﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace Twp.Test
{
	partial class ControlsForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
		    this.booleanBoxLabel = new System.Windows.Forms.Label();
		    this.sliderLabel = new System.Windows.Forms.Label();
		    this.inputBoxLabel = new System.Windows.Forms.Label();
		    this.TabRow = new Twp.Controls.TabRow();
		    this.tab1 = new Twp.Controls.TabRow.Tab();
		    this.tabRobLabel = new System.Windows.Forms.Label();
		    this.sidePanel = new System.Windows.Forms.Panel();
		    this.propertyGrid = new System.Windows.Forms.PropertyGrid();
		    this.selecctedLabel = new System.Windows.Forms.Label();
		    this.line1 = new Twp.Controls.Line();
		    this.ToolBox = new Twp.Controls.ToolBox();
		    this.TabControl = new Twp.Controls.TabControl();
		    this.tabPage1 = new System.Windows.Forms.TabPage();
		    this.toolBoxContainer1 = new Twp.Controls.ToolBoxContainer();
		    this.toolBox1 = new Twp.Controls.ToolBox();
		    this.toolBox2 = new Twp.Controls.ToolBox();
		    this.tabPage2 = new System.Windows.Forms.TabPage();
		    this.FlagsListBox = new Twp.Controls.FlagsListBox();
		    this.BooleanBox = new Twp.Controls.BooleanBox();
		    this.Slider = new Twp.Controls.Slider();
		    this.InputBox = new Twp.Controls.InputBox();
		    this.tabRow1 = new Twp.Controls.TabRow();
		    this.tab2 = new Twp.Controls.TabRow.Tab();
		    this.tab3 = new Twp.Controls.TabRow.Tab();
		    this.line2 = new Twp.Controls.Line();
		    this.progressIndicator1 = new Twp.Controls.ProgressIndicator();
		    this.vLabel1 = new Twp.Controls.VLabel();
		    this.sidePanel.SuspendLayout();
		    this.ToolBox.SuspendLayout();
		    this.TabControl.SuspendLayout();
		    this.tabPage1.SuspendLayout();
		    this.toolBoxContainer1.SuspendLayout();
		    this.toolBox1.SuspendLayout();
		    this.toolBox2.SuspendLayout();
		    this.SuspendLayout();
		    // 
		    // booleanBoxLabel
		    // 
		    this.booleanBoxLabel.AutoSize = true;
		    this.booleanBoxLabel.Location = new System.Drawing.Point(12, 16);
		    this.booleanBoxLabel.Name = "booleanBoxLabel";
		    this.booleanBoxLabel.Size = new System.Drawing.Size(67, 13);
		    this.booleanBoxLabel.TabIndex = 4;
		    this.booleanBoxLabel.Text = "BooleanBox:";
		    // 
		    // sliderLabel
		    // 
		    this.sliderLabel.AutoSize = true;
		    this.sliderLabel.Location = new System.Drawing.Point(12, 43);
		    this.sliderLabel.Name = "sliderLabel";
		    this.sliderLabel.Size = new System.Drawing.Size(36, 13);
		    this.sliderLabel.TabIndex = 5;
		    this.sliderLabel.Text = "Slider:";
		    // 
		    // inputBoxLabel
		    // 
		    this.inputBoxLabel.AutoSize = true;
		    this.inputBoxLabel.Location = new System.Drawing.Point(12, 69);
		    this.inputBoxLabel.Name = "inputBoxLabel";
		    this.inputBoxLabel.Size = new System.Drawing.Size(52, 13);
		    this.inputBoxLabel.TabIndex = 6;
		    this.inputBoxLabel.Text = "InputBox:";
		    // 
		    // TabRow
		    // 
		    this.TabRow.Location = new System.Drawing.Point(85, 92);
		    this.TabRow.Name = "TabRow";
		    this.TabRow.SelectedTab = this.tab1;
		    this.TabRow.Size = new System.Drawing.Size(263, 20);
		    this.TabRow.TabIndex = 7;
		    // 
		    // tab1
		    // 
		    this.tab1.Text = "tab1";
		    // 
		    // tabRobLabel
		    // 
		    this.tabRobLabel.AutoSize = true;
		    this.tabRobLabel.Location = new System.Drawing.Point(12, 96);
		    this.tabRobLabel.Name = "tabRobLabel";
		    this.tabRobLabel.Size = new System.Drawing.Size(51, 13);
		    this.tabRobLabel.TabIndex = 8;
		    this.tabRobLabel.Text = "TabRow:";
		    // 
		    // sidePanel
		    // 
		    this.sidePanel.Controls.Add(this.propertyGrid);
		    this.sidePanel.Controls.Add(this.selecctedLabel);
		    this.sidePanel.Controls.Add(this.line1);
		    this.sidePanel.Dock = System.Windows.Forms.DockStyle.Right;
		    this.sidePanel.Location = new System.Drawing.Point(508, 0);
		    this.sidePanel.Name = "sidePanel";
		    this.sidePanel.Size = new System.Drawing.Size(193, 438);
		    this.sidePanel.TabIndex = 10;
		    // 
		    // propertyGrid
		    // 
		    this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
		    this.propertyGrid.Location = new System.Drawing.Point(10, 23);
		    this.propertyGrid.Name = "propertyGrid";
		    this.propertyGrid.Size = new System.Drawing.Size(183, 415);
		    this.propertyGrid.TabIndex = 2;
		    this.propertyGrid.ToolbarVisible = false;
		    // 
		    // selecctedLabel
		    // 
		    this.selecctedLabel.Dock = System.Windows.Forms.DockStyle.Top;
		    this.selecctedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		    this.selecctedLabel.Location = new System.Drawing.Point(10, 0);
		    this.selecctedLabel.Name = "selecctedLabel";
		    this.selecctedLabel.Size = new System.Drawing.Size(183, 23);
		    this.selecctedLabel.TabIndex = 5;
		    this.selecctedLabel.Text = "{object}";
		    this.selecctedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		    // 
		    // line1
		    // 
		    this.line1.Dock = System.Windows.Forms.DockStyle.Left;
		    this.line1.Location = new System.Drawing.Point(0, 0);
		    this.line1.Name = "line1";
		    this.line1.Orientation = System.Windows.Forms.Orientation.Vertical;
		    this.line1.Size = new System.Drawing.Size(10, 438);
		    this.line1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
		    // 
		    // ToolBox
		    // 
		    this.ToolBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
		    		    		    | System.Windows.Forms.AnchorStyles.Left) 
		    		    		    | System.Windows.Forms.AnchorStyles.Right)));
		    this.ToolBox.Controls.Add(this.TabControl);
		    this.ToolBox.Controls.Add(this.FlagsListBox);
		    this.ToolBox.Location = new System.Drawing.Point(12, 134);
		    this.ToolBox.Name = "ToolBox";
		    this.ToolBox.Size = new System.Drawing.Size(490, 292);
		    this.ToolBox.TabIndex = 11;
		    this.ToolBox.Text = "ToolBox";
		    // 
		    // TabControl
		    // 
		    this.TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
		    		    		    | System.Windows.Forms.AnchorStyles.Left) 
		    		    		    | System.Windows.Forms.AnchorStyles.Right)));
		    this.TabControl.Controls.Add(this.tabPage1);
		    this.TabControl.Controls.Add(this.tabPage2);
		    this.TabControl.ItemSize = new System.Drawing.Size(0, 17);
		    this.TabControl.Location = new System.Drawing.Point(133, 25);
		    this.TabControl.Name = "TabControl";
		    this.TabControl.SelectedIndex = 0;
		    this.TabControl.Size = new System.Drawing.Size(350, 260);
		    this.TabControl.TabIndex = 1;
		    // 
		    // tabPage1
		    // 
		    this.tabPage1.Controls.Add(this.toolBoxContainer1);
		    this.tabPage1.Location = new System.Drawing.Point(4, 21);
		    this.tabPage1.Name = "tabPage1";
		    this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
		    this.tabPage1.Size = new System.Drawing.Size(342, 235);
		    this.tabPage1.TabIndex = 0;
		    this.tabPage1.Text = "tabPage1";
		    this.tabPage1.UseVisualStyleBackColor = true;
		    // 
		    // toolBoxContainer1
		    // 
		    this.toolBoxContainer1.Controls.Add(this.toolBox1);
		    this.toolBoxContainer1.Controls.Add(this.toolBox2);
		    this.toolBoxContainer1.Dock = System.Windows.Forms.DockStyle.Left;
		    this.toolBoxContainer1.FillLast = false;
		    this.toolBoxContainer1.Location = new System.Drawing.Point(3, 3);
		    this.toolBoxContainer1.Name = "toolBoxContainer1";
		    this.toolBoxContainer1.Size = new System.Drawing.Size(196, 229);
		    this.toolBoxContainer1.SplitterWidth = 4;
		    this.toolBoxContainer1.TabIndex = 0;
		    // 
		    // toolBox1
		    // 
		    this.toolBox1.AllowCollapse = true;
		    this.toolBox1.Controls.Add(this.vLabel1);
		    this.toolBox1.Location = new System.Drawing.Point(2, 2);
		    this.toolBox1.Name = "toolBox1";
		    this.toolBox1.Size = new System.Drawing.Size(192, 92);
		    this.toolBox1.TabIndex = 0;
		    this.toolBox1.Text = "DockedToolBox1";
		    // 
		    // toolBox2
		    // 
		    this.toolBox2.AllowCollapse = true;
		    this.toolBox2.Controls.Add(this.progressIndicator1);
		    this.toolBox2.Location = new System.Drawing.Point(2, 98);
		    this.toolBox2.Name = "toolBox2";
		    this.toolBox2.Size = new System.Drawing.Size(192, 87);
		    this.toolBox2.TabIndex = 1;
		    this.toolBox2.Text = "DockedToolBox2";
		    // 
		    // tabPage2
		    // 
		    this.tabPage2.Location = new System.Drawing.Point(4, 21);
		    this.tabPage2.Name = "tabPage2";
		    this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
		    this.tabPage2.Size = new System.Drawing.Size(342, 235);
		    this.tabPage2.TabIndex = 1;
		    this.tabPage2.Text = "tabPage2";
		    this.tabPage2.UseVisualStyleBackColor = true;
		    // 
		    // FlagsListBox
		    // 
		    this.FlagsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
		    		    		    | System.Windows.Forms.AnchorStyles.Left)));
		    this.FlagsListBox.CheckOnClick = true;
		    this.FlagsListBox.FormattingEnabled = true;
		    this.FlagsListBox.IntegralHeight = false;
		    this.FlagsListBox.Location = new System.Drawing.Point(7, 25);
		    this.FlagsListBox.Name = "FlagsListBox";
		    this.FlagsListBox.Size = new System.Drawing.Size(120, 260);
		    this.FlagsListBox.TabIndex = 0;
		    // 
		    // BooleanBox
		    // 
		    this.BooleanBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
		    		    		    | System.Windows.Forms.AnchorStyles.Right)));
		    this.BooleanBox.AutoSize = false;
		    this.BooleanBox.FalseText = "";
		    this.BooleanBox.Location = new System.Drawing.Point(85, 12);
		    this.BooleanBox.Name = "BooleanBox";
		    this.BooleanBox.Size = new System.Drawing.Size(417, 21);
		    this.BooleanBox.TabIndex = 12;
		    this.BooleanBox.TrueText = "";
		    // 
		    // Slider
		    // 
		    this.Slider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
		    		    		    | System.Windows.Forms.AnchorStyles.Right)));
		    this.Slider.Location = new System.Drawing.Point(85, 39);
		    this.Slider.Name = "Slider";
		    this.Slider.Size = new System.Drawing.Size(417, 21);
		    this.Slider.TabIndex = 13;
		    // 
		    // InputBox
		    // 
		    this.InputBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
		    		    		    | System.Windows.Forms.AnchorStyles.Right)));
		    this.InputBox.Location = new System.Drawing.Point(85, 66);
		    this.InputBox.Name = "InputBox";
		    this.InputBox.Size = new System.Drawing.Size(417, 20);
		    this.InputBox.TabIndex = 14;
		    // 
		    // tabRow1
		    // 
		    this.tabRow1.Location = new System.Drawing.Point(85, 92);
		    this.tabRow1.Name = "tabRow1";
		    this.tabRow1.SelectedTab = this.tab2;
		    this.tabRow1.Size = new System.Drawing.Size(263, 20);
		    this.tabRow1.TabIndex = 15;
		    this.tabRow1.Tabs.Add(this.tab2);
		    this.tabRow1.Tabs.Add(this.tab3);
		    // 
		    // tab2
		    // 
		    this.tab2.Text = "";
		    // 
		    // tab3
		    // 
		    this.tab3.Text = "tab2";
		    // 
		    // line2
		    // 
		    this.line2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
		    		    		    | System.Windows.Forms.AnchorStyles.Right)));
		    this.line2.Location = new System.Drawing.Point(0, 118);
		    this.line2.Name = "line2";
		    this.line2.Size = new System.Drawing.Size(512, 10);
		    this.line2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
		    // 
		    // progressIndicator1
		    // 
		    this.progressIndicator1.AutoStart = true;
		    this.progressIndicator1.Circles = ((uint)(8u));
		    this.progressIndicator1.Location = new System.Drawing.Point(7, 25);
		    this.progressIndicator1.Name = "progressIndicator1";
		    this.progressIndicator1.Size = new System.Drawing.Size(50, 50);
		    this.progressIndicator1.TabIndex = 0;
		    this.progressIndicator1.Text = "progressIndicator1";
		    this.progressIndicator1.VisibleCircles = ((uint)(8u));
		    // 
		    // vLabel1
		    // 
		    this.vLabel1.BackColor = System.Drawing.Color.Transparent;
		    this.vLabel1.Location = new System.Drawing.Point(7, 25);
		    this.vLabel1.Name = "vLabel1";
		    this.vLabel1.Size = new System.Drawing.Size(17, 53);
		    this.vLabel1.TabIndex = 0;
		    this.vLabel1.Text = "vLabel1";
		    // 
		    // ControlsForm
		    // 
		    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		    this.ClientSize = new System.Drawing.Size(701, 438);
		    this.Controls.Add(this.line2);
		    this.Controls.Add(this.tabRow1);
		    this.Controls.Add(this.InputBox);
		    this.Controls.Add(this.Slider);
		    this.Controls.Add(this.BooleanBox);
		    this.Controls.Add(this.ToolBox);
		    this.Controls.Add(this.sidePanel);
		    this.Controls.Add(this.tabRobLabel);
		    this.Controls.Add(this.TabRow);
		    this.Controls.Add(this.inputBoxLabel);
		    this.Controls.Add(this.sliderLabel);
		    this.Controls.Add(this.booleanBoxLabel);
		    this.Name = "ControlsForm";
		    this.Text = "ControlsForm";
		    this.sidePanel.ResumeLayout(false);
		    this.ToolBox.ResumeLayout(false);
		    this.TabControl.ResumeLayout(false);
		    this.tabPage1.ResumeLayout(false);
		    this.toolBoxContainer1.ResumeLayout(false);
		    this.toolBox1.ResumeLayout(false);
		    this.toolBox1.PerformLayout();
		    this.toolBox2.ResumeLayout(false);
		    this.ResumeLayout(false);
		    this.PerformLayout();
        }
		private Twp.Controls.VLabel vLabel1;
		private Twp.Controls.ProgressIndicator progressIndicator1;
		private Twp.Controls.ToolBox toolBox2;
		private Twp.Controls.ToolBox toolBox1;
		private Twp.Controls.ToolBoxContainer toolBoxContainer1;
		private Twp.Controls.FlagsListBox FlagsListBox;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private Twp.Controls.TabControl TabControl;
		private Twp.Controls.ToolBox ToolBox;
		private System.Windows.Forms.Label selecctedLabel;
		private System.Windows.Forms.Panel sidePanel;
        private System.Windows.Forms.Label tabRobLabel;
		private Twp.Controls.TabRow.Tab tab1;
		private Twp.Controls.TabRow TabRow;
		private System.Windows.Forms.Label inputBoxLabel;
		private System.Windows.Forms.Label sliderLabel;
        private System.Windows.Forms.Label booleanBoxLabel;
        private Twp.Controls.Line line1;
        private System.Windows.Forms.PropertyGrid propertyGrid;
        private Controls.BooleanBox BooleanBox;
        private Controls.Slider Slider;
        private Controls.InputBox InputBox;
        private Controls.TabRow tabRow1;
        private Controls.TabRow.Tab tab2;
        private Controls.TabRow.Tab tab3;
        private Controls.Line line2;
	}
}
