﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Test
{
	/// <summary>
	/// Description of ControlsForm.
	/// </summary>
	public partial class ControlsForm : Form
	{
		public ControlsForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            foreach( Control control in this.Controls )
            {
    		    if( control is Label )
    		        continue;
    		    
    		    if( control == this.sidePanel )
    		        continue;
    		    
    		    control.Enter += new EventHandler( this.OnControlEntered );
            }
            
            foreach( Control control in this.ToolBox.Controls )
            {
                control.Enter += new EventHandler( this.OnControlEntered );
            }
        }

        private void OnControlEntered( object sender, EventArgs e )
        {
            Control control = (Control) sender;
            if( control == null )
                return;
            
	        Twp.Utilities.Log.Debug( "[ControlsForm.OnControlEntered]: {0}.Enter triggerd.", control.Name );
	        this.propertyGrid.SelectedObject = control;
	        this.selecctedLabel.Text = control.Name;
        }
	}
}
