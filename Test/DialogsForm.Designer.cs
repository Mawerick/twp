﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace Twp.Test
{
	partial class DialogsForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
		    this.verticalPanel1 = new Twp.Controls.VerticalPanel();
		    this.aboutDialog = new System.Windows.Forms.Button();
		    this.exceptionDialog = new System.Windows.Forms.Button();
		    this.inputDialogButton = new System.Windows.Forms.Button();
		    this.inputTypePanel = new System.Windows.Forms.Panel();
		    this.inputTypeBox = new System.Windows.Forms.ComboBox();
		    this.inputTypeLabel = new System.Windows.Forms.Label();
		    this.textFileDialogButton = new System.Windows.Forms.Button();
		    this.consoleWindowButton = new System.Windows.Forms.Button();
		    this.progressListFormButton = new System.Windows.Forms.Button();
		    this.progressFormButton = new System.Windows.Forms.Button();
		    this.verticalPanel1.SuspendLayout();
		    this.inputTypePanel.SuspendLayout();
		    this.SuspendLayout();
		    // 
		    // verticalPanel1
		    // 
		    this.verticalPanel1.Controls.Add(this.aboutDialog);
		    this.verticalPanel1.Controls.Add(this.exceptionDialog);
		    this.verticalPanel1.Controls.Add(this.inputDialogButton);
		    this.verticalPanel1.Controls.Add(this.inputTypePanel);
		    this.verticalPanel1.Controls.Add(this.textFileDialogButton);
		    this.verticalPanel1.Controls.Add(this.consoleWindowButton);
		    this.verticalPanel1.Controls.Add(this.progressListFormButton);
		    this.verticalPanel1.Controls.Add(this.progressFormButton);
		    this.verticalPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
		    this.verticalPanel1.HorizontalAlign = Twp.Controls.HorizontalLayoutAlignment.Stretch;
		    this.verticalPanel1.Location = new System.Drawing.Point(0, 0);
		    this.verticalPanel1.Name = "verticalPanel1";
		    this.verticalPanel1.Padding = new System.Windows.Forms.Padding(3);
		    this.verticalPanel1.Size = new System.Drawing.Size(184, 210);
		    this.verticalPanel1.TabIndex = 0;
		    // 
		    // aboutDialog
		    // 
		    this.aboutDialog.AutoSize = true;
		    this.aboutDialog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.aboutDialog.Location = new System.Drawing.Point(3, 3);
		    this.aboutDialog.Name = "aboutDialog";
		    this.aboutDialog.Size = new System.Drawing.Size(178, 23);
		    this.aboutDialog.TabIndex = 4;
		    this.aboutDialog.Text = "AboutDialog";
		    this.aboutDialog.UseVisualStyleBackColor = true;
		    this.aboutDialog.Click += new System.EventHandler(this.AboutDialogClick);
		    // 
		    // exceptionDialog
		    // 
		    this.exceptionDialog.AutoSize = true;
		    this.exceptionDialog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.exceptionDialog.Location = new System.Drawing.Point(3, 29);
		    this.exceptionDialog.Name = "exceptionDialog";
		    this.exceptionDialog.Size = new System.Drawing.Size(178, 23);
		    this.exceptionDialog.TabIndex = 3;
		    this.exceptionDialog.Text = "ExceptionDialog";
		    this.exceptionDialog.UseVisualStyleBackColor = true;
		    this.exceptionDialog.Click += new System.EventHandler(this.ExceptionDialogClick);
		    // 
		    // inputDialogButton
		    // 
		    this.inputDialogButton.AutoSize = true;
		    this.inputDialogButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.inputDialogButton.Location = new System.Drawing.Point(3, 55);
		    this.inputDialogButton.Name = "inputDialogButton";
		    this.inputDialogButton.Size = new System.Drawing.Size(178, 23);
		    this.inputDialogButton.TabIndex = 2;
		    this.inputDialogButton.Text = "InputDialog";
		    this.inputDialogButton.UseVisualStyleBackColor = true;
		    this.inputDialogButton.Click += new System.EventHandler(this.InputDialogButtonClick);
		    // 
		    // inputTypePanel
		    // 
		    this.inputTypePanel.Controls.Add(this.inputTypeBox);
		    this.inputTypePanel.Controls.Add(this.inputTypeLabel);
		    this.inputTypePanel.Location = new System.Drawing.Point(3, 81);
		    this.inputTypePanel.Name = "inputTypePanel";
		    this.inputTypePanel.Size = new System.Drawing.Size(178, 21);
		    this.inputTypePanel.TabIndex = 6;
		    // 
		    // inputTypeBox
		    // 
		    this.inputTypeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
		    this.inputTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
		    this.inputTypeBox.FormattingEnabled = true;
		    this.inputTypeBox.Items.AddRange(new object[] {
            "Boolean",
            "Item",
            "Number",
            "Text"});
		    this.inputTypeBox.Location = new System.Drawing.Point(88, 0);
		    this.inputTypeBox.Name = "inputTypeBox";
		    this.inputTypeBox.Size = new System.Drawing.Size(90, 21);
		    this.inputTypeBox.TabIndex = 6;
		    // 
		    // inputTypeLabel
		    // 
		    this.inputTypeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
		    this.inputTypeLabel.AutoSize = true;
		    this.inputTypeLabel.Location = new System.Drawing.Point(3, 3);
		    this.inputTypeLabel.Name = "inputTypeLabel";
		    this.inputTypeLabel.Size = new System.Drawing.Size(57, 13);
		    this.inputTypeLabel.TabIndex = 7;
		    this.inputTypeLabel.Text = "Input type:";
		    this.inputTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
		    // 
		    // textFileDialogButton
		    // 
		    this.textFileDialogButton.AutoSize = true;
		    this.textFileDialogButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.textFileDialogButton.Location = new System.Drawing.Point(3, 105);
		    this.textFileDialogButton.Name = "textFileDialogButton";
		    this.textFileDialogButton.Size = new System.Drawing.Size(178, 23);
		    this.textFileDialogButton.TabIndex = 0;
		    this.textFileDialogButton.Text = "TextFileDialog";
		    this.textFileDialogButton.UseVisualStyleBackColor = true;
		    this.textFileDialogButton.Click += new System.EventHandler(this.TextFileDialogButtonClick);
		    // 
		    // consoleWindowButton
		    // 
		    this.consoleWindowButton.AutoSize = true;
		    this.consoleWindowButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.consoleWindowButton.Location = new System.Drawing.Point(3, 131);
		    this.consoleWindowButton.Name = "consoleWindowButton";
		    this.consoleWindowButton.Size = new System.Drawing.Size(178, 23);
		    this.consoleWindowButton.TabIndex = 7;
		    this.consoleWindowButton.Text = "ConsoleWindow";
		    this.consoleWindowButton.UseVisualStyleBackColor = true;
		    this.consoleWindowButton.Click += new System.EventHandler(this.consoleWindowButtonClick);
		    // 
		    // progressListFormButton
		    // 
		    this.progressListFormButton.AutoSize = true;
		    this.progressListFormButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.progressListFormButton.Location = new System.Drawing.Point(3, 157);
		    this.progressListFormButton.Name = "progressListFormButton";
		    this.progressListFormButton.Size = new System.Drawing.Size(178, 23);
		    this.progressListFormButton.TabIndex = 8;
		    this.progressListFormButton.Text = "ProgressListForm";
		    this.progressListFormButton.UseVisualStyleBackColor = true;
		    this.progressListFormButton.Click += new System.EventHandler(this.ProgressListFormButtonClick);
		    // 
		    // progressFormButton
		    // 
		    this.progressFormButton.AutoSize = true;
		    this.progressFormButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
		    this.progressFormButton.Location = new System.Drawing.Point(3, 183);
		    this.progressFormButton.Name = "progressFormButton";
		    this.progressFormButton.Size = new System.Drawing.Size(178, 23);
		    this.progressFormButton.TabIndex = 1;
		    this.progressFormButton.Text = "ProgressForm";
		    this.progressFormButton.UseVisualStyleBackColor = true;
		    this.progressFormButton.Click += new System.EventHandler(this.ProgressFormButtonClick);
		    // 
		    // DialogsForm
		    // 
		    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		    this.ClientSize = new System.Drawing.Size(184, 210);
		    this.Controls.Add(this.verticalPanel1);
		    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
		    this.Name = "DialogsForm";
		    this.Text = "DialogsForm";
		    this.verticalPanel1.ResumeLayout(false);
		    this.verticalPanel1.PerformLayout();
		    this.inputTypePanel.ResumeLayout(false);
		    this.inputTypePanel.PerformLayout();
		    this.ResumeLayout(false);

		}
		private System.Windows.Forms.Label inputTypeLabel;
		private System.Windows.Forms.Panel inputTypePanel;
		private System.Windows.Forms.ComboBox inputTypeBox;
		private System.Windows.Forms.Button inputDialogButton;
		private System.Windows.Forms.Button exceptionDialog;
		private System.Windows.Forms.Button aboutDialog;
		private System.Windows.Forms.Button progressFormButton;
		private System.Windows.Forms.Button textFileDialogButton;
		private Twp.Controls.VerticalPanel verticalPanel1;
        private System.Windows.Forms.Button consoleWindowButton;
        private System.Windows.Forms.Button progressListFormButton;
	}
}
