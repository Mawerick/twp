﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Twp.Controls;

namespace Twp.Test
{
    /// <summary>
    /// Description of DialogsForm.
    /// </summary>
    public partial class DialogsForm : Form
    {
        public DialogsForm()
        {
            InitializeComponent();

            inputTypeBox.SelectedIndex = 0;
        }

        void AboutDialogClick( object sender, EventArgs e )
        {
            using( AboutDialog about = new AboutDialog() )
            {
                about.ShowDialog();
            }
        }

        void ExceptionDialogClick( object sender, EventArgs e )
        {
            ExceptionDialog.Show( "ExceptionDialog Test", new Twp.Utilities.IniParseException( "This is a test" ) );
        }

        void InputDialogButtonClick( object sender, EventArgs e )
        {
            string type = (string) inputTypeBox.SelectedItem;
            switch( type )
            {
                case "Text":
                    string text = InputDialog.GetText( "InputDialog.GetText", "Type something:" );
                    Console.WriteLine( "InputDialog.GetText returned " + text );
                    break;
                case "Number":
                    decimal number = InputDialog.GetNumber( "InputDialog.GetNumber", "Pick a number, any number!" );
                    Console.WriteLine( "InputDialog.GetNumber returned " + number.ToString() );
                    break;
                case "Item":
                    string item = InputDialog.GetItem( "InputDialog.GetItem", "Pick a color:", new string[]
                    {
                        "Blue",
                        "Red",
                        "Green",
                        "Five"
                    } );
                    Console.WriteLine( "InputDialog.GetItem returned " + item );
                    if( item == "Five" )
                        Console.WriteLine( "Five is not a color. Please contact support." );
                    break;
                case "Boolean":
                    bool boolean = InputDialog.GetBoolean( "InputDialog.GetBoolean", "This is awesome!" );
                    Console.WriteLine( "InputDialog.GetBoolean returned " + boolean.ToString() );
                    break;
                default:
                    Console.WriteLine( "InputDialog: Invalid type " + type );
                    break;
            }
        }

        void ProgressFormButtonClick( object sender, EventArgs e )
        {
            ProgressForm progress = new ProgressForm();
            progress.AutoCloseDelay = 10;
            progress.Status = "Performing test.";
            progress.Show();
            Application.DoEvents();
            Random rng = new Random();
            for( int i = 0; i < 10; i++ )
            {
                int count = (int) ( rng.NextDouble() * 20 ) + 10;
                ProgressTask task = progress.AddTask( "Copying files:", count, 10 );
                for( int n = 0; n < count; n++ )
                {
                    task.Value++;
                    Thread.Sleep( 100 );
                    Application.DoEvents();
                }
                task.Finish();
            }
            progress.Finish();
        }

        void ProgressListFormButtonClick( object sender, EventArgs e )
        {
            ProgressListForm progress = new ProgressListForm();
            progress.AutoCloseDelay = 10;
            progress.Status = "ProgressForm Test.";
            progress.Show();
            Application.DoEvents();
            Random rng = new Random();
            for( int i = 0; i < 10; i++ )
            {
                int count = (int) ( rng.NextDouble() * 20 ) + 10;
                ProgressTask task = progress.AddTask( "Copying files", count, 10 );
                Thread thread = new Thread( DoTask );
                thread.Start( task );
            }
        }

        void DoTask( object data )
        {
            ProgressTask task = (ProgressTask) data;
            for( int n = 0; n < task.Maximum; n++ )
            {
                task.Value++;
                Thread.Sleep( 500 );
            }
            task.Finish();
        }

        void TextFileDialogButtonClick( object sender, EventArgs e )
        {
            TextFileDialog.ShowFile( "Twp.Test.log" );
        }

        void consoleWindowButtonClick( object sender, EventArgs e )
        {
            ConsoleWindow console = new ConsoleWindow();
            console.Show();
        }
    }
}
