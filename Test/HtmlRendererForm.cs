﻿using System;
using System.Windows.Forms;
using Twp.Controls.RichText;

namespace Twp.Test
{
    public partial class HtmlRendererForm : Form
    {
        public HtmlRendererForm()
        {
            InitializeComponent();
            this.textBox.Text = "A <font color=\"red\">List</font>:\r\n<ul>\r\n<li>Test #1</li>\r\n<li>Test #2</li>\r\n</ul>\r\n<strong>How's that?</strong><br>A <a href=\"http://www.wrongplace.net/\">link</a>";
        }
        
        private bool loaded = false;
        
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad(e);
            this.loaded = true;
            this.UpdateContent();
        }

        private void textBox1_TextChanged( object sender, EventArgs e )
        {
            this.UpdateContent();
        }
        
        private void UpdateContent()
        {
            if( !this.loaded )
                return;
            
            this.htmlLabel.Text = this.textBox.Text;
            this.htmlPanel.Text = this.textBox.Text;
        }
        
        private void OnLinkClicked( object sender, LinkClickedEventArgs e )
        {
            System.Diagnostics.Process.Start( e.LinkText );
        }
    }
}
