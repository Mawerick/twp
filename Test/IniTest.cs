﻿using System;
using System.Runtime.Serialization;
using System.IO;

using Twp.Utilities;
using System.Reflection;

namespace Twp.Test
{
    public class IniTest
    {
        public static void Run()
        {
            IniTest test = new IniTest();
            Log.Debug( "[IniTest.Run] IniData structure:" );
            Log.Debug( "[IniTest.Run] ------------------" );
            test.ReadDebug( test.data, "[IniTest.Run] " );
            test.Write();
            test.Read();
        }

        private IniData data = new IniData();

        public void Read()
        {
            using( FileStream stream = new FileStream( "IniTest.ini", FileMode.Open ) )
            {
                //StreamReader reader = new StreamReader( stream );
                //Log.Debug( reader.ReadToEnd() );
                try
                {
                    IniFormatter formatter = new IniFormatter();
                    this.data = (IniData) formatter.Deserialize( stream );
                }
                catch( SerializationException e )
                {
                    Log.Debug( "[IniTest.Read] An error occured: {0}", e.Message );
                }
                finally
                {
                    stream.Close();
                }
            }

            Log.Debug( "[IniTest.Read] IniData structure:" );
            Log.Debug( "[IniTest.Read] ------------------" );
            this.ReadDebug( this.data, "[IniTest.Read] " );
            Log.Debug( "[IniTest.Read]" );
        }

        public void ReadDebug( object obj, string indent )
        {
            Log.Debug( indent + obj.GetType().Name + " {" );
            foreach( PropertyInfo pi in obj.GetType().GetProperties() )
            {
                if( pi.PropertyType.IsValueType || pi.PropertyType == typeof( String ) )
                    Log.Debug( indent + "  {0} = {1}", pi.Name, pi.GetValue( obj, null ) );
                else
                    this.ReadDebug( pi.GetValue( obj, null ), indent + "  " );
            }
            Log.Debug( indent + "}" );
        }

        public void Write()
        {
            using( FileStream stream = new FileStream( "IniTest.ini", FileMode.Create ) )
            {
                try
                {
                    IniFormatter formatter = new IniFormatter();
                    formatter.Serialize( stream, this.data );
                }
                catch( SerializationException e )
                {
                    Log.Debug( "[IniTest.Write] An error occured: {0}", e.Message );
                }
                finally
                {
                    stream.Close();
                }
            }

            Log.Debug( "[IniTest.Write] IniTest.ini:" );
            Log.Debug( "[IniTest.Write] ------------" );
            foreach( string line in File.ReadAllLines( "IniTest.ini" ) )
            {
                Log.Debug( "[IniTest.Write] {0}", line );
            }
            Log.Debug( "[IniTest.Write]" );
        }
    }

    [Serializable]
    public class IniData
    {
        public int Number
        {
            get { return this.number; }
            set { this.number = value; }
        }
        private int number = 13;

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        private string text = "This is a string";

        public MoreIniData MoreData
        {
            get { return this.moreData; }
        }
        private MoreIniData moreData = new MoreIniData();

        [INIIgnore]
        public bool Ignored
        {
            get { return this.ignored; }
            set { this.ignored = value; }
        }
        private bool ignored = true;
    }

    [Serializable]
    public class MoreIniData
    {
        private int number = 42;
        public int Number
        {
            get { return this.number; }
            set { this.number = value; }
        }

        private string text = "This is another string";
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        private EvenMoreIniData evenMoreData = new EvenMoreIniData();
        public EvenMoreIniData EvenMoreData
        {
            get { return this.evenMoreData; }
        }

        private bool ignored = true;
        [INIIgnore]
        public bool Ignored
        {
            get { return this.ignored; }
            set { this.ignored = value; }
        }
    }

    [Serializable]
    public class EvenMoreIniData
    {
        private int number = 666;
        public int Number
        {
            get { return this.number; }
            set { this.number = value; }
        }

        private string text = "This is an evil string";
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
    }
}
