﻿namespace Twp.Test
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prefsButton = new System.Windows.Forms.Button();
            this.parserButton = new System.Windows.Forms.Button();
            this.iniButton = new System.Windows.Forms.Button();
            this.diffButton = new System.Windows.Forms.Button();
            this.richTextButton = new System.Windows.Forms.Button();
            this.htmlRendererButton = new System.Windows.Forms.Button();
            this.consoleView1 = new Twp.Controls.ConsoleView();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.horizontalPanel1 = new Twp.Controls.HorizontalPanel();
            this.dataTestsBox = new Twp.Controls.Design.VerticalGroupBox();
            this.mmdbButton = new System.Windows.Forms.Button();
            this.guiTestsBox = new Twp.Controls.Design.VerticalGroupBox();
            this.controlsButton = new System.Windows.Forms.Button();
            this.dialogsButton = new System.Windows.Forms.Button();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.horizontalPanel1.SuspendLayout();
            this.dataTestsBox.SuspendLayout();
            this.guiTestsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // prefsButton
            // 
            this.prefsButton.AutoSize = true;
            this.prefsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.prefsButton.Location = new System.Drawing.Point(3, 39);
            this.prefsButton.Name = "prefsButton";
            this.prefsButton.Size = new System.Drawing.Size(194, 23);
            this.prefsButton.TabIndex = 1;
            this.prefsButton.Text = "AO Prefs";
            this.prefsButton.UseVisualStyleBackColor = true;
            this.prefsButton.Click += new System.EventHandler(this.prefsButton_Click);
            // 
            // parserButton
            // 
            this.parserButton.AutoSize = true;
            this.parserButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.parserButton.Location = new System.Drawing.Point(3, 65);
            this.parserButton.Name = "parserButton";
            this.parserButton.Size = new System.Drawing.Size(194, 23);
            this.parserButton.TabIndex = 2;
            this.parserButton.Text = "Parsers";
            this.parserButton.UseVisualStyleBackColor = true;
            this.parserButton.Click += new System.EventHandler(this.parserButton_Click);
            // 
            // iniButton
            // 
            this.iniButton.AutoSize = true;
            this.iniButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.iniButton.Location = new System.Drawing.Point(3, 91);
            this.iniButton.Name = "iniButton";
            this.iniButton.Size = new System.Drawing.Size(194, 23);
            this.iniButton.TabIndex = 3;
            this.iniButton.Text = "INI Formatter";
            this.iniButton.UseVisualStyleBackColor = true;
            this.iniButton.Click += new System.EventHandler(this.iniButton_Click);
            // 
            // diffButton
            // 
            this.diffButton.AutoSize = true;
            this.diffButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.diffButton.Location = new System.Drawing.Point(3, 117);
            this.diffButton.Name = "diffButton";
            this.diffButton.Size = new System.Drawing.Size(194, 23);
            this.diffButton.TabIndex = 4;
            this.diffButton.Text = "Diff test";
            this.diffButton.UseVisualStyleBackColor = true;
            this.diffButton.Click += new System.EventHandler(this.diffButton_Click);
            // 
            // richTextButton
            // 
            this.richTextButton.AutoSize = true;
            this.richTextButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.richTextButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextButton.Location = new System.Drawing.Point(3, 65);
            this.richTextButton.Name = "richTextButton";
            this.richTextButton.Size = new System.Drawing.Size(194, 23);
            this.richTextButton.TabIndex = 2;
            this.richTextButton.Text = "RichText";
            this.richTextButton.UseVisualStyleBackColor = true;
            this.richTextButton.Click += new System.EventHandler(this.richTextButton_Click);
            // 
            // htmlRendererButton
            // 
            this.htmlRendererButton.AutoSize = true;
            this.htmlRendererButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.htmlRendererButton.Location = new System.Drawing.Point(3, 91);
            this.htmlRendererButton.Name = "htmlRendererButton";
            this.htmlRendererButton.Size = new System.Drawing.Size(194, 23);
            this.htmlRendererButton.TabIndex = 3;
            this.htmlRendererButton.Text = "HtmlRenderer";
            this.htmlRendererButton.UseVisualStyleBackColor = true;
            this.htmlRendererButton.Click += new System.EventHandler(this.htmlRendererButton_Click);
            // 
            // consoleView1
            // 
            this.consoleView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consoleView1.Location = new System.Drawing.Point(0, 0);
            this.consoleView1.Name = "consoleView1";
            this.consoleView1.ReadOnly = true;
            this.consoleView1.Size = new System.Drawing.Size(784, 405);
            this.consoleView1.TabIndex = 0;
            this.consoleView1.Text = "Failed to set flags with hr=0x8013131d\r\nFailed to set flags with hr=0x8013131d\r\nF" +
            "ailed to set flags with hr=0x8013131d\r\nFailed to set flags with hr=0x8013131d\r\n";
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.horizontalPanel1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.consoleView1);
            this.splitContainer.Size = new System.Drawing.Size(784, 562);
            this.splitContainer.SplitterDistance = 153;
            this.splitContainer.TabIndex = 0;
            // 
            // horizontalPanel1
            // 
            this.horizontalPanel1.Controls.Add(this.dataTestsBox);
            this.horizontalPanel1.Controls.Add(this.guiTestsBox);
            this.horizontalPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.horizontalPanel1.Location = new System.Drawing.Point(0, 0);
            this.horizontalPanel1.Name = "horizontalPanel1";
            this.horizontalPanel1.Padding = new System.Windows.Forms.Padding(3);
            this.horizontalPanel1.Size = new System.Drawing.Size(784, 153);
            this.horizontalPanel1.TabIndex = 0;
            this.horizontalPanel1.VerticalAlign = Twp.Controls.VerticalLayoutAlignment.Stretch;
            // 
            // dataTestsBox
            // 
            this.dataTestsBox.Controls.Add(this.mmdbButton);
            this.dataTestsBox.Controls.Add(this.prefsButton);
            this.dataTestsBox.Controls.Add(this.parserButton);
            this.dataTestsBox.Controls.Add(this.iniButton);
            this.dataTestsBox.Controls.Add(this.diffButton);
            this.dataTestsBox.Location = new System.Drawing.Point(3, 3);
            this.dataTestsBox.Name = "dataTestsBox";
            this.dataTestsBox.Size = new System.Drawing.Size(200, 147);
            this.dataTestsBox.TabIndex = 0;
            this.dataTestsBox.TabStop = false;
            this.dataTestsBox.Text = "Data Tests";
            // 
            // mmdbButton
            // 
            this.mmdbButton.AutoSize = true;
            this.mmdbButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mmdbButton.Location = new System.Drawing.Point(3, 13);
            this.mmdbButton.Name = "mmdbButton";
            this.mmdbButton.Size = new System.Drawing.Size(194, 23);
            this.mmdbButton.TabIndex = 0;
            this.mmdbButton.Text = "MMDB";
            this.mmdbButton.UseVisualStyleBackColor = true;
            this.mmdbButton.Click += new System.EventHandler(this.mmdbButtom_Click);
            // 
            // guiTestsBox
            // 
            this.guiTestsBox.Controls.Add(this.controlsButton);
            this.guiTestsBox.Controls.Add(this.dialogsButton);
            this.guiTestsBox.Controls.Add(this.richTextButton);
            this.guiTestsBox.Controls.Add(this.htmlRendererButton);
            this.guiTestsBox.Location = new System.Drawing.Point(206, 3);
            this.guiTestsBox.Name = "guiTestsBox";
            this.guiTestsBox.Size = new System.Drawing.Size(200, 147);
            this.guiTestsBox.TabIndex = 1;
            this.guiTestsBox.TabStop = false;
            this.guiTestsBox.Text = "GUI Tests";
            // 
            // controlsButton
            // 
            this.controlsButton.AutoSize = true;
            this.controlsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.controlsButton.Location = new System.Drawing.Point(3, 13);
            this.controlsButton.Name = "controlsButton";
            this.controlsButton.Size = new System.Drawing.Size(194, 23);
            this.controlsButton.TabIndex = 0;
            this.controlsButton.Text = "Controls";
            this.controlsButton.UseVisualStyleBackColor = true;
            this.controlsButton.Click += new System.EventHandler(this.ControlsButtonClick);
            // 
            // dialogsButton
            // 
            this.dialogsButton.AutoSize = true;
            this.dialogsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.dialogsButton.Location = new System.Drawing.Point(3, 39);
            this.dialogsButton.Name = "dialogsButton";
            this.dialogsButton.Size = new System.Drawing.Size(194, 23);
            this.dialogsButton.TabIndex = 1;
            this.dialogsButton.Text = "Dialogs";
            this.dialogsButton.UseVisualStyleBackColor = true;
            this.dialogsButton.Click += new System.EventHandler(this.dialogsButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.splitContainer);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.OnLoad);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            this.horizontalPanel1.ResumeLayout(false);
            this.dataTestsBox.ResumeLayout(false);
            this.dataTestsBox.PerformLayout();
            this.guiTestsBox.ResumeLayout(false);
            this.guiTestsBox.PerformLayout();
            this.ResumeLayout(false);
        }
        private System.Windows.Forms.Button mmdbButton;

        private System.Windows.Forms.SplitContainer splitContainer;
        private Twp.Controls.HorizontalPanel horizontalPanel1;
        private Twp.Controls.Design.VerticalGroupBox dataTestsBox;
        private System.Windows.Forms.Button prefsButton;
        private System.Windows.Forms.Button parserButton;
        private System.Windows.Forms.Button iniButton;
        private System.Windows.Forms.Button diffButton;
        private Twp.Controls.Design.VerticalGroupBox guiTestsBox;
        private System.Windows.Forms.Button controlsButton;
        private System.Windows.Forms.Button dialogsButton;
        private System.Windows.Forms.Button richTextButton;
        private System.Windows.Forms.Button htmlRendererButton;
        private Twp.Controls.ConsoleView consoleView1;

        #endregion
    }
}