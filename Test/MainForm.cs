﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Test
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void OnLoad( object sender, EventArgs e )
        {
            Console.WriteLine( "Redirected Console..." );
        }
                
        private void mmdbButtom_Click( object sender, EventArgs e )
        {
            MmdbForm form = new MmdbForm();
            form.Show();
        }

        private void prefsButton_Click( object sender, EventArgs e )
        {
            this.prefsButton.Enabled = false;
            string path = Twp.Utilities.Path.Combine( "D:", "Games", "Anarchy Online" );
            if( Twp.FC.AO.GamePath.Confirm( ref path ) )
                Log.Debug( "Valid install: {0}", path );
            PrefsTest.Run( path );
            this.prefsButton.Enabled = true;
        }

        private void iniButton_Click( object sender, EventArgs e )
        {
            this.iniButton.Enabled = false;
            IniTest.Run();
            this.iniButton.Enabled = true;
        }

        private void diffButton_Click( object sender, EventArgs e )
        {
            this.diffButton.Enabled = false;
            
            string s1 = "<b>This is a test.</b>";
            string s2 = "<b>This is a Test.</b>";
            
            Diff.GetDiffTreeFromBacktrackMatrix( Diff.GetLongestCommonSubsequenceMatrix( s1, s2 ), s1, s2, s1.Length, s2.Length );
            
            this.diffButton.Enabled = true;
        }

        private void parserButton_Click( object sender, EventArgs e )
        {
            this.parserButton.Enabled = false;
            ParserTest.Run();
            this.parserButton.Enabled = true;
        }
                
        private void ControlsButtonClick( object sender, EventArgs e )
        {
            ControlsForm form = new ControlsForm();
            form.Show();
        }

        private void dialogsButton_Click( object sender, EventArgs e )
        {
        	DialogsForm form = new DialogsForm();
        	form.Show();
        }

        private void richTextButton_Click( object sender, EventArgs e )
        {
            RichTextForm form = new RichTextForm();
            form.Show();
        }

        private void htmlRendererButton_Click( object sender, EventArgs e )
        {
            HtmlRendererForm form = new HtmlRendererForm();
            form.Show();
        }
    }
}
