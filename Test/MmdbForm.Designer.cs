﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace Twp.Test
{
    partial class MmdbForm
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            Twp.Controls.TreeListColumn treeListColumn1 = new Twp.Controls.TreeListColumn("fieldname1", "ID");
            Twp.Controls.TreeListColumn treeListColumn2 = new Twp.Controls.TreeListColumn("fieldname2", "Text");
            this.treeListView = new Twp.Controls.TreeListView();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListView
            // 
            treeListColumn1.CellFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            treeListColumn1.Width = 150;
            treeListColumn2.AutoSize = true;
            this.treeListView.Columns.AddRange(new Twp.Controls.TreeListColumn[] {
                                    treeListColumn1,
                                    treeListColumn2});
            this.treeListView.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.treeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView.ItemHeight = 20;
            this.treeListView.Location = new System.Drawing.Point(0, 0);
            this.treeListView.Name = "treeListView";
            this.treeListView.Size = new System.Drawing.Size(580, 470);
            this.treeListView.TabIndex = 0;
            // 
            // MmdbForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 470);
            this.Controls.Add(this.treeListView);
            this.Name = "MmdbForm";
            this.Text = "CompareMdbForm";
            ((System.ComponentModel.ISupportInitialize)(this.treeListView)).EndInit();
            this.ResumeLayout(false);
        }
        private Twp.Controls.TreeListView treeListView;
    }
}
