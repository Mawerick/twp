﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Twp.Data.AO;
using Twp.Controls;
using Twp.Utilities;

namespace Twp.Test
{
    /// <summary>
    /// Description of CompareMdbForm.
    /// </summary>
    public partial class MmdbForm : Form
    {
        public MmdbForm()
        {
            InitializeComponent();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            string path = Twp.Utilities.Path.Combine( "D:", "Games", "Anarchy Online" );
            if( Twp.FC.AO.GamePath.Confirm( ref path ) )
                Log.Debug( "Valid install: {0}", path );
            this.Run( path );
        }

        private void Run( string path )
        {
            Mmdb mmdb = new Mmdb();
            if( !mmdb.Open( path ) )
            {
                Log.Debug( "Failed to open MMDB file." );
                return;
            }
            
            this.treeListView.BeginUpdate();
            this.treeListView.Nodes.Clear();
            foreach( Mmdb.Category cat in mmdb.Categories )
            {
                Node node = this.treeListView.Nodes.Add( cat.Id.ToString() );
                foreach( Mmdb.Entry ent in cat.Entries )
                {
                    string text = ent.Text.Replace( "\r\n", "" ).Replace( "\n", "" );
                    node.Nodes.Add( ent.Id.ToString(), text );
                    Application.DoEvents();
                }
                Application.DoEvents();
            }
            this.treeListView.EndUpdate();
        }
    }
}
