﻿using System;
using System.Collections.Generic;
using Twp.Controls.RichText;
using Twp.Utilities;

namespace Twp.Test
{
    public class Parser
    {
        public static void Run()
        {
            Parser p = new Parser();
            p.ParseAoml();
            p.ParseBBCode();
        }

        public void ParseAoml()
        {
            string aoml = "This is a test... <font color='blue'>Blue text...</font><font color=red>Red text...</font>";
            Log.Debug( "[ParseAoml] Text: {0}", aoml );
            Log.Debug( "[ParseAoml] Creating parser" );
            AomlParser parser = new AomlParser();
            Log.Debug( "[ParseAoml] Parsing..." );
            ISegment root = parser.Parse( aoml );

            Log.Debug( "[ParseAoml] Parser output:" );
            this.DebugSegment( root, "" );
        }

        public void ParseBBCode()
        {
            string bbcode = "This is a test... [b]bold text...[/b][font color=red]Red text...[/font]";
            Log.Debug( "[ParseBBCode] Text: {0}", bbcode );
            Log.Debug( "[ParseBBCode] Creating parser" );
            BBCodeParser parser = new BBCodeParser();
            Log.Debug( "[ParseBBCode] Parsing..." );
            ISegment root = parser.Parse( bbcode );

            Log.Debug( "[ParseBBCode] Parser output:" );
            this.DebugSegment( root, "" );
        }

        private void DebugSegment( ISegment segment, string indent )
        {
            this.DebugTagSegment( segment as TagSegment, indent );
            this.DebugTextSegment( segment as TextSegment, indent );

            if( segment.HasChildren )
            {
                foreach( ISegment child in segment.Children )
                {
                    this.DebugSegment( child, indent + "\t" );
                }
            }
        }

        private void DebugTagSegment( TagSegment segment, string indent )
        {
            if( segment == null )
                return;

            Console.Write( indent + "<" );
            if( segment.IsEndTag )
                Console.Write( "/" );
            Console.Write( segment.Type );
            if( segment.HasAttributes )
            {
                foreach( KeyValuePair<string, object> attr in segment.Attributes )
                {
                    Console.Write( " {0}={1}", attr.Key, attr.Value );
                }
            }
            Console.WriteLine( ">" );
        }

        private void DebugTextSegment( TextSegment segment, string indent )
        {
            if( segment == null )
                return;

            Console.Write( indent );
            foreach( Word word in segment.Words )
            {
                Console.Write( word.Text + " " );
            }
            Console.WriteLine();
        }
    }
}
