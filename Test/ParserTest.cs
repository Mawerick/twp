﻿using System;
using System.Collections.Generic;
using Twp.Controls.RichText;
using Twp.Utilities;

namespace Twp.Test
{
    public class ParserTest
    {
        public static void Run()
        {
            ParseAoml();
            ParseBBCode();
        }

        public static void ParseAoml()
        {
            string aoml = "This is a test... <font color='blue'>Blue text...</font><font color=red>Red text...</font>";
            Log.Debug( "[ParseAoml] Text: {0}", aoml );
            Log.Debug( "[ParseAoml] Creating parser" );
            AomlParser parser = new AomlParser();
            Log.Debug( "[ParseAoml] Parsing..." );
            Tag root = parser.Parse( aoml );

            Log.Debug( "[ParseAoml] Parser output:" );
            DebugTag( root, "" );
        }

        public static void ParseBBCode()
        {
            string bbcode = "This is a test... [b]bold text...[/b][font color=red]Red text...[/font]";
            Log.Debug( "[ParseBBCode] Text: {0}", bbcode );
            Log.Debug( "[ParseBBCode] Creating parser" );
            BBCodeParser parser = new BBCodeParser();
            Log.Debug( "[ParseBBCode] Parsing..." );
            Tag root = parser.Parse( bbcode );

            Log.Debug( "[ParseBBCode] Parser output:" );
            DebugTag( root, "" );
        }

        private static void DebugTag( Tag tag, string indent )
        {
            if( tag == null )
                return;
            
            if( tag.Type == TagType.Text )
            {
                Console.Write( indent );
                foreach( Word word in tag.Words )
                {
                    Console.Write( word.Text + " " );
                }
                Console.WriteLine();
            }
            else
            {
                Console.Write( indent + "<" );
                if( tag.IsEndTag )
                    Console.Write( "/" );
                Console.Write( tag.Type );
                if( tag.HasAttributes )
                {
                    foreach( KeyValuePair<string, object> attr in tag.Attributes )
                    {
                        Console.Write( " {0}={1}", attr.Key, attr.Value );
                    }
                }
                Console.WriteLine( ">" );
            }

            if( tag.HasChildren )
            {
                foreach( Tag child in tag.Children )
                {
                    DebugTag( child, indent + "\t" );
                }
            }
        }
    }
}
