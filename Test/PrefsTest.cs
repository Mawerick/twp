﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Twp.FC;
using Twp.FC.AO;
using Twp.Utilities;

namespace Twp.Test
{
    public class PrefsTest
    {
        static public void Run( string path )
        {
            Stopwatch sw = new Stopwatch();
            Stopwatch act_sw = new Stopwatch();
            Stopwatch char_sw = new Stopwatch();

            if( !Prefs.IsValid( path ) )
            {
                Log.Debug( "Prefs don't exist." );
                return;
            }

            sw.Start();
            XmlFile prefsFile = Prefs.Read( path );
            //DebugXml( prefsFile.Root );
            sw.Stop();
            Log.Debug( "Prefs.xml parsing: {0}", sw.Elapsed );
            sw.Reset();

            sw.Start();
            List<Account> accounts = Account.List( path );
            Log.Debug( "Accounts found: {0}", accounts.Count );
            foreach( Account account in accounts )
            {
                Log.Debug( "Account name: {0}", account.Name );

                act_sw.Start();
                if( account.Preferences == null )
                    Log.Debug( "--- Prefs.Xml parsing failed!" );
                act_sw.Stop();
                Log.Debug( "--- Prefs.Xml parsing: {0}", act_sw.Elapsed );
                act_sw.Reset();

                act_sw.Start();
                if( account.Config == null )
                    Log.Debug( "--- Login.cfg parsing failed!" );
                act_sw.Stop();
                Log.Debug( "--- Login.cfg parsing: {0}", act_sw.Elapsed );
                act_sw.Reset();

                Log.Debug( "--- Characters:" );
                act_sw.Start();

                List<Character> characters = account.Characters;
                Log.Debug( "    Characters: {0}", characters.Count );
                foreach( Character character in characters )
                {
                    Log.Debug( "    ID: {0}", character.ID );

                    char_sw.Start();
                    if( character.Prefs == null )
                        Log.Debug( "    --- Prefs.Xml parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- Prefs.Xml parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();

                    char_sw.Start();
                    if( character.Config == null )
                        Log.Debug( "    --- Char.cfg parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- Char.cfg parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();

                    char_sw.Start();
                    if( character.Chat == null )
                        Log.Debug( "    --- Chat parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- Chat parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();

                    char_sw.Start();
                    if( character.Containers == null )
                        Log.Debug( "    --- Container parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- Container parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();

                    char_sw.Start();
                    if( character.DockAreas == null )
                        Log.Debug( "    --- DockArea parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- DockArea parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();

                    char_sw.Start();
                    if( character.IgnoreList == null )
                        Log.Debug( "    --- IgnoreList parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- IgnoreList parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();

                    char_sw.Start();
                    if( character.TextMacros == null )
                        Log.Debug( "    --- TextMacros parsing failed!" );
                    char_sw.Stop();
                    Log.Debug( "    --- TextMacros parsing: {0}", char_sw.Elapsed );
                    char_sw.Reset();
                }
                act_sw.Stop();
                Log.Debug( "    Parsing time: {0}", act_sw.Elapsed );
                act_sw.Reset();
            }
            sw.Stop();
            Log.Debug( "Parsing time: {0}", sw.Elapsed );
            sw.Reset();

            try
            {
                Log.Debug( "" );
                Log.Debug( "Statistics for Mawerick:" );
                Log.Debug( "------------------------" );
                Character maw = accounts[0].Characters[0];
                Log.Debug( "There are {0} bags, of which {1} are old (more than 30 days)",
                          maw.Containers.Bags.Count, maw.Containers.OldBags );
                Log.Debug( "There are {0} DockAreas, of which {1} are old (more than 30 days)",
                          maw.DockAreas.Items.Count, maw.DockAreas.OldAreas );
                
                DockArea invDA = maw.DockAreas.Items[maw.Containers.Inventory.DockArea];
                Log.Debug( "Inventory is in {0}", invDA.Name );
                Log.Debug( "{0} is a {1}", invDA.Name, invDA.Type );
                Log.Debug( "Items in {0}: {1}", invDA.Name, String.Join( ", ", invDA.Identities.ToArray() ) );
                int i = 1;
                foreach( Container bag in maw.Containers.Bags.Values )
                {
                    if( i == 5 )
                    {
                        Log.Debug( "The 5th bag is named {0}, and is in {1}", bag.Name, bag.DockArea );
                        DockArea bagDA = maw.DockAreas.Items[bag.DockArea];
                        Log.Debug( "{0} is a {1}", bagDA.Name, bagDA.Type );
                        Log.Debug( "Items in {0}: {1}", bagDA.Name, String.Join( ", ", bagDA.Identities.ToArray() ) );
                        break;
                    }
                    i++;
                }
                DockArea rollupArea = maw.DockAreas.RollupArea;
                Log.Debug( "{0} is a {1}", rollupArea.Name, rollupArea.Type );
                Log.Debug( "Items in {0}: {1}", rollupArea.Name, String.Join( ", ", rollupArea.Identities.ToArray() ) );
                Log.Debug( "There are {0} macros.", maw.TextMacros.Macros.Count );
            }
            catch( Exception ex )
            {
                Twp.Controls.ExceptionDialog.Show( ex );
            }
        }

        //static private void DebugXml( IAOElement element, string indent = "" )
        //{
        //    Log.Debug( "{0}{1}", indent, element );
        //    if( element.HasChildren )
        //    {
        //        string subindent = indent + "  ";
        //        if( element is RootElement )
        //        {
        //            foreach( IAOElement node in ( element as RootElement ).Items )
        //            {
        //                DebugXml( node, subindent );
        //            }
        //        }
        //        else if( element is ArchiveElement )
        //        {
        //            foreach( IAOElement item in ( element as ArchiveElement ).Items )
        //            {
        //                DebugXml( item, subindent );
        //            }
        //        }
        //    }
        //}
    }
}
