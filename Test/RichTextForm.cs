﻿using System;
using System.Windows.Forms;
using Twp.Controls.RichText;

namespace Twp.Test
{
    public partial class RichTextForm : Form
    {
        public RichTextForm()
        {
            InitializeComponent();
            this.parserModeBox.DataSource = Enum.GetValues( typeof( RichTextMode ) );
            this.parserModeBox.SelectedItem = RichTextMode.Html;
//            this.textBox.Text = "A [font color=\"red\"]List[/font]:\r\n[ulist]\r\n[*]Test #1\r\n[*]Test #2\r\n[/ulist]\r\n[b]How's that?[/b][br]A [a href=\"http://www.wrongplace.net/\"]link[/a]";
//            this.textBox.Text = "A <font color=\"red\">List</font>:\r\n<ul>\r\n<li>Test #1</li>\r\n<li>Test #2</li>\r\n</ul>\r\n<b>How's that?</b><br>A <a href=\"http://www.wrongplace.net/\">link</a>";
//            this.textBox.Text = "<html>\r\n" +
//                "<body>\r\n" +
//                "<strong><u>Changing your chat colors:</u></strong>\r\n" +
//                "<ul>\r\n" +
//                "<li>Change the colors you want.</li>\r\n" +
//                "<li>Click <strong><font color='navy'>Save</font></strong> to save the data.</li>\r\n" +
//                "<li>Launch Anarchy Online.</li>\r\n" +
//                "</ul>\r\n" +
//                "<hr>\r\n" +
//                "<u>If you already have AO running, do this to see your changes:</u>\r\n" +
//                "<ul>\r\n" +
//                "<li>Press <strong><font color='navy'>CTRL + SHIFT + F7</font></strong>.</li>\r\n" +
//                "<li>Make the <strong><font color='navy'>Actions</font></strong> tab active by clicking on it.</li>\r\n" +
//                "<li>Click <strong><font color='navy'>Reload menus</font></strong>.</li>\r\n" +
//                "</ul>\r\n" +
//                "</body>\r\n" +
//                "</html>";
            this.textBox.Text = "<html>\r\n" +
                  "<body>\r\n" +
                  "<p>AOcrayon is a third-party tool that was made with the intent to help players of Funcom MMOs (like Anarchy Online and Age of Conan) change their chat text colors easily, without mucking about with 'complex' XML file editing.</p>\r\n" +
                  "<p>Get the latest version at: <a href='http://www.wrongplace.net/'>http://www.wrongplace.net/</a></p>\r\n" +
                  "<p><b><u>Credits:</u></b> Special thanks to Oofbig for suggesting the name :)</p>\r\n" +
                  "<p><b>Note:</b> This <u>only</u> changes what <u>you</u> see.<br>\r\n" +
                  "It has nothing to do with what anyone else sees.<br>\r\n" +
                  "If you want to make your chat colorful for others, this is not the tool for you.</p>\r\n" +
                  "</body>\r\n" +
                  "</html>";
        }

        private bool loaded = false;
        
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            this.loaded = true;
            this.UpdateContent( "On Load" );
        }

        private void parserModeBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.richTextLabel.Mode = (RichTextMode) this.parserModeBox.SelectedItem;
            this.UpdateContent( "Mode Changed" );
        }

        private void textBox1_TextChanged( object sender, EventArgs e )
        {
            this.UpdateContent( "Text Changed" );
        }
        
        private void UpdateContent( string caller )
        {
            if( !this.loaded )
                return;
            
            this.richTextLabel.Text = this.textBox.Text;
            this.treeView.BeginUpdate();
            this.treeView.Nodes.Add( caller );
            this.treeView.Nodes.Clear();
            foreach( Tag tag in this.richTextLabel.Document.Segments )
            {
                AddNode( tag, null );
            }
            this.treeView.ExpandAll();
            this.treeView.EndUpdate();
        }
        
        private void AddNode( Tag tag, TreeNode parent )
        {
            TreeNode node = new TreeNode();
            node.Text = tag.ToString();
            node.Tag = tag;
            if( tag.HasChildren )
            {
                foreach( Tag child in tag.Children )
                {
                    AddNode( child, node );
                }
            }
            if( parent == null )
                this.treeView.Nodes.Add( node );
            else
                parent.Nodes.Add( node );
        }
        
        private void OnLinkClicked( object sender, LinkClickedEventArgs e )
        {
            System.Diagnostics.Process.Start( e.LinkText );
        }
    }
}
