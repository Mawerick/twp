﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Twp.Test
{
    public class TextBoxWriter : TextWriter
    {
        private TextBox textBox = null;

        public TextBoxWriter( TextBox textBox )
        {
            this.textBox = textBox;

            this.scrollTimer.Interval = 30;
            this.scrollTimer.Tick += new EventHandler( this.OnTick );
        }

        private bool autoScroll = false;
        public bool AutoScroll
        {
            get { return this.autoScroll; }
            set
            {
                this.autoScroll = value;
                this.scrollTimer.Enabled = value;
            }
        }

        private delegate void InvokeDelegate( char value );

        public override void Write( char value )
        {
            base.Write( value );
            if( this.textBox.InvokeRequired )
            {
                this.textBox.BeginInvoke( new InvokeDelegate( AppendText ), value );
            }
            else
                this.AppendText( value );
        }

        private void AppendText( char value )
        {
            this.textBox.Text += value;
            //this.textBox.SelectionStart = this.textBox.Text.Length;
            //this.textBox.ScrollToCaret();
            Application.DoEvents();
            this.scrollTimer.Enabled = this.autoScroll;
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }

        [DllImport( "user32.dll" )]
        [return: MarshalAs( UnmanagedType.Bool )]
        static extern bool GetScrollInfo( IntPtr hwnd, int fnBar, ref SCROLLINFO lpsi );

        [DllImport( "user32.dll" )]
        static extern int SetScrollInfo( IntPtr hwnd, int fnBar, [In] ref SCROLLINFO lpsi, bool fRedraw );

        [DllImport( "user32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage" )]
        static extern IntPtr SendMessage( IntPtr hwnd, uint msg, IntPtr wParam, IntPtr lParam );

        struct SCROLLINFO
        {
            public uint cbSize;
            public uint fMask;
            public int nMin;
            public int nMax;
            public uint nPage;
            public int nPos;
            public int nTrackPos;
        };

        enum ScrollBarDirection
        {
            SB_HORZ = 0,
            SB_VERT = 1,
            SB_CTL = 2,
            SB_BOTH = 3
        }

        enum ScrollInfoMask
        {
            SIF_RANGE = 0x1,
            SIF_PAGE = 0x2,
            SIF_POS = 0x4,
            SIF_DISABLENOSCROLL = 0x8,
            SIF_TRACKPOS = 0x10,
            SIF_ALL = SIF_RANGE + SIF_PAGE + SIF_POS + SIF_TRACKPOS
        }

        const int WM_VSCROLL = 277;
        const int SB_LINEUP = 0;
        const int SB_LINEDOWN = 1;
        const int SB_THUMBPOSITION = 4;
        const int SB_THUMBTRACK = 5;
        const int SB_TOP = 6;
        const int SB_BOTTOM = 7;
        const int SB_ENDSCROLL = 8;

        private Timer scrollTimer = new Timer();

        void OnTick( object sender, EventArgs e )
        {
            this.Scroll( this.textBox.Handle, 1 );
        }

        // Scrolls a given textbox. handle: an handle to our textbox. pixels: number of pixels to scroll.
        void Scroll( IntPtr handle, int pixels )
        {
            IntPtr ptrLparam = new IntPtr( 0 );
            IntPtr ptrWparam;

            // Get current scroller posion
            SCROLLINFO si = new SCROLLINFO();
            si.cbSize = (uint) Marshal.SizeOf( si );
            si.fMask = (uint) ScrollInfoMask.SIF_ALL;
            GetScrollInfo( handle, (int) ScrollBarDirection.SB_VERT, ref si );

            // Increase posion by pixles
            if( si.nPos < (si.nMax - si.nPage) )
                si.nPos += pixels;
            else
            {
                ptrWparam = new IntPtr( SB_ENDSCROLL );
                scrollTimer.Enabled = false;
                SendMessage( handle, WM_VSCROLL, ptrWparam, ptrLparam );
            }

            // Reposition scroller
            SetScrollInfo( handle, (int) ScrollBarDirection.SB_VERT, ref si, true );

            // Send a WM_VSCROLL scroll message using SB_THUMBTRACK as wParam
            // SB_THUMBTRACK: low-order word of wParam, si.nPos high-order word of wParam
            ptrWparam = new IntPtr( SB_THUMBTRACK + 0x10000 * si.nPos );
            SendMessage( handle, WM_VSCROLL, ptrWparam, ptrLparam );
        }
    }
}
