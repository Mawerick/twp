﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Controls
{
	/// <summary>
	/// Description of Document.
	/// </summary>
	public class Document
	{
		private List<ISegment> lineSegmentCollection;
		public List<ISegment> LineSegmentCollection
		{
			get { return lineSegmentCollection; }
		}

		private AomlParser parser;
		public AomlParser Parser
		{
			get { return parser; }
		}

		public string GetText( int offset, int length )
		{
			return String.Empty;
		}
		
		public void Clear()
		{
			this.lineSegmentCollection.Clear();
		}

		public void Append( string text )
		{
//			this.lineSegmentCollection.Add( this.parser.Parse( text ) );
		}

		public Document()
		{
			this.lineSegmentCollection = new List<ISegment>();
			this.parser = new AomlParser();
		}
	}
}
