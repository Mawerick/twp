﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml;

namespace Twp.Utilities
{
	/// <summary>
	/// Description of HtmlLabel.
	/// </summary>
	[DefaultProperty( "Html" )]
	[DefaultEvent( "LinkClicked" )]
	public class HtmlLabel : UserControl
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="HtmlLabel"/> class.
		/// </summary>
		public HtmlLabel()
		{
			InitializeComponent();
			SetStyle( ControlStyles.DoubleBuffer, true );
			SetStyle( ControlStyles.AllPaintingInWmPaint, true );
			SetStyle( ControlStyles.UserPaint, true );
		}
		
		#region Designer required code

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if( components != null )
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "HtmlLabel";
		}

		#endregion

		/// <summary>
		/// Gets or sets the html content of the label.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The html text content of the label." )]
		[DefaultValue( "" )]
		[Editor( typeof( MultilineStringEditor ), typeof( UITypeEditor ) )]
		public string Html
		{
			get { return this.html; }
			set
			{
				if( this.html != value )
				{
					this.html = value;
					this.Invalidate();
				}
			}
		}
		private string html = String.Empty;

		/// <summary>
		/// Gets or sets the number of pixels to indent the content of a list.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The number of pixels to indent the content of a list." )]
		[DefaultValue( 15 )]
		public int ListIndent
		{
			get { return this.listIndent; }
			set
			{
				if( this.listIndent != value )
				{
					this.listIndent = value;
					this.Invalidate();
				}
			}
		}
		private int listIndent = 15;

		private int position = 0;
		private int verticalPosition = 0;

		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
		/// </summary>
		/// <param name="e">A <see cref="System.Windows.Forms.PaintEventArgs"/> that contains the event data.</param>
		protected override void OnPaint( PaintEventArgs e )
		{
			base.OnPaint( e );

			this.position = 0;
			this.verticalPosition = 0;
			this.links.Clear();
			this.currentLink = null;

			try
			{
				e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
				HtmlParser parser = new HtmlParser( this.html, Font );
				foreach( XmlNode node in parser.DocumentElement.ChildNodes )
				{
					this.DrawNode( node, e.Graphics, this.ForeColor, this.Font, false );
				}
			}
			catch( Exception ex )
			{
				Log.Debug( "[HtmlLabel] [OnPaint]", ex.Message );
			}
		}
		
		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.MouseMove"/> event.
		/// </summary>
		/// <param name="e">A <see cref="System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
		protected override void OnMouseMove( MouseEventArgs e )
		{
			base.OnMouseMove( e );
			bool onLink = false;
			foreach( HtmlLabel.Link link in this.links )
			{
				if( link.Region.IsVisible( e.Location ) )
				{
					onLink = true;
				}
			}
			if( onLink == true )
				this.Cursor = Cursors.Hand;
			else
				this.Cursor = this.DefaultCursor;
		}

		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.MouseClick"/> event.
		/// </summary>
		/// <param name="e">A <see cref="System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
		protected override void OnMouseClick( MouseEventArgs e )
		{
			base.OnMouseClick( e );
			foreach( HtmlLabel.Link link in this.links )
			{
				if( link.Region.IsVisible( e.Location ) )
				{
					if( e.Button == MouseButtons.Left )
					{
						this.OnLinkClicked( new LinkClickedEventArgs( link.LinkText ) );
					}
					else if( e.Button == MouseButtons.Right )
					{
						this.hoverLink = link.LinkText;
						ContextMenu cm = new ContextMenu();
						cm.MenuItems.Add( "Copy link location", OnCopyLinkLocation );
						cm.Show( this, e.Location );
						cm.Dispose();
					}
					return;
				}
			}
		}

		private void DrawNode( XmlNode node, Graphics g, Color currentColor, Font currentFont, bool inList )
		{
			Color color = currentColor;
			Font font = currentFont;
			bool nextLine = false;
			bool isList = inList;
			int indent = 0;
			string link = null;
			TextFormatFlags flags = TextFormatFlags.NoPadding;

			string text  = HtmlParser.ParseNode( node, ref font, ref color,
			                                    ref nextLine, ref isList, ref link );
			int stringHeight = (int) g.MeasureString( "X", font ).Height + 2;

			if( isList )
				indent = this.listIndent;

			if( isList && !inList )
			{
				this.position = indent;
				this.verticalPosition += stringHeight;
			}

			if( String.IsNullOrEmpty( text ) == false )
			{
				if( text == "<li>" )
				{
					int x = this.listIndent - 10;
					int y = this.verticalPosition + (stringHeight / 2) - 1;

					Brush brush = new SolidBrush( currentColor );
					Point[] points = new Point[] {
						new Point( x - 2, y ),
						new Point( x, y - 2 ),
						new Point( x + 2, y ),
						new Point( x, y + 2 ),
					};
					g.FillPolygon( brush, points );
					brush.Dispose();
				}
				else
				{
					Rectangle clip = Rectangle.Truncate( this.ClientRectangle );
					string[] words = text.Split( new char[] { ' ' } );
					for( int n = 0; n < words.Length; ++n )
					{
						string word = words[n];
						if( n < words.Length - 1 )
							word += " ";
						int stringWidth = TextRenderer.MeasureText( g, word, font,
						                                           clip.Size, flags ).Width;

						if( this.position + stringWidth > this.ClientRectangle.Width )
						{
							this.position = indent;
							this.verticalPosition += stringHeight;
						}

						if( this.position <= clip.Right &&
						   this.verticalPosition <= clip.Bottom &&
						   this.verticalPosition >= clip.Top )
						{
							Rectangle rect = new Rectangle( this.position, this.verticalPosition,
							                               stringWidth, stringHeight );
							TextRenderer.DrawText( g, word, font, rect, color,
							                      TextFormatFlags.NoPadding );
							
							if( this.currentLink != null )
								this.currentLink.Region.Union( rect );
						}

						this.position += (int) stringWidth;
					}
				}
			}

			if( String.IsNullOrEmpty( link ) == false )
			{
				this.currentLink = new HtmlLabel.Link( link );
			}

			if( node.HasChildNodes )
			{
				foreach( XmlNode child in node.ChildNodes )
				{
					DrawNode( child, g, color, font, isList );
				}
			}

			if( String.IsNullOrEmpty( link ) == false )
			{
				this.links.Add( this.currentLink );
				this.currentLink = null;
			}

			if( nextLine )
			{
				this.position = indent;
				this.verticalPosition += stringHeight;
				nextLine = false;
			}
		}
		
		private List<HtmlLabel.Link> links = new List<HtmlLabel.Link>();
		private HtmlLabel.Link currentLink = null;
		private string hoverLink = null;

		/// <summary>
		/// Occurs when a link is clicked within the control.
		/// </summary>
		[Category( "Action" )]
		[Description( "Occurs when a link is clicked within the control." )]
		public event LinkClickedEventHandler LinkClicked;

		/// <summary>
		/// Raises the <see cref="LinkClicked"/> event.
		/// </summary>
		/// <param name="e">A <see cref="LinkClickedEventArgs"/> that contains the event data.</param>
		protected virtual void OnLinkClicked( LinkClickedEventArgs e )
		{
			if( this.LinkClicked != null )
				this.LinkClicked( this, e );
		}

		internal void OnCopyLinkLocation( object sender, EventArgs e )
		{
			Clipboard.SetText( this.hoverLink );
		}

		/// <summary>
		/// Represents a link within a <see cref="HtmlLabel"/> control.
		/// </summary>
		public class Link
		{
			/// <summary>
			/// Gets or sets the text associated with the link.
			/// </summary>
			public string LinkText
			{
				get { return this.linkText; }
				set { this.linkText = value; }
			}
			private string linkText;

			/// <summary>
			/// Gets the clipping region the link occupies.
			/// </summary>
			public Region Region
			{
				get { return this.region; }
			}
			private Region region;

			/// <summary>
			/// Initializes a new instance of the <see cref="HtmlLabel.Link"/> class with the specified link text.
			/// </summary>
			/// <param name="linkText">The associated link text.</param>
			public Link( string linkText )
			{
				this.linkText = linkText;
				this.region = new Region();
				this.region.MakeEmpty();
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="HtmlLabel.Link"/> class with the specified link text and clipping region.
			/// </summary>
			/// <param name="linkText">The associated link text.</param>
			/// <param name="region">The clipping region the link occupies.</param>
			public Link( string linkText, Region region )
			{
				this.linkText = linkText;
				this.region = region;
			}
		}

	}
}
