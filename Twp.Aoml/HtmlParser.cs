// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.IO;
using System.Xml;

namespace Twp.Utilities
{
	/// <summary>
	/// Summary description for HtmlParser.
	/// </summary>
	public class HtmlParser : XmlDocument
	{
		private Color currentColor = Color.Black;
		private Font currentFont;

		/// <summary>
		/// Initializes a new instance of the <see cref="HtmlParser"/> class.
		/// </summary>
		/// <param name="html">The html-encoded text to display.</param>
		/// <param name="font">The base font to use.</param>
		public HtmlParser( string html, Font font )
		{
			this.currentFont = font;
			try
			{
				html = html.Replace( "\r", "" );
				html = html.Replace( "\n", "" );
				html = html.Replace( "&nbsp;", " " );
				TextReader reader = new System.IO.StringReader( "<root>" + html + "</root>" );
				this.Load( XmlReader.Create( reader ) );
			}
			catch( XmlException ex )
			{
				Log.Debug( "[HtmlParser]", "Parser error: {0}", ex.Message );
			}
		}

		/// <summary>
		/// Parses an <see cref="XmlNode"/> from the html input and extracts the style and content for drawing.
		/// </summary>
		/// <param name="node">The <see cref="XmlNode"/> to parse.</param>
		/// <param name="font">The font for this node.</param>
		/// <param name="color">The color for this node.</param>
		/// <param name="nextLine">True if a new line should be added to the output, otherwise, false.</param>
		/// <param name="isList">True if the node is part of a list, otherwise, false.</param>
		/// <param name="link">The link text of a link node.</param>
		/// <returns></returns>
		public static string ParseNode( XmlNode node, ref Font font, ref Color color,
		                               ref bool nextLine, ref bool isList, ref string link )
		{
			if( node == null )
			{
				return null;
			}

			FontStyle fontStyle = font.Style;
			float fontSize = font.SizeInPoints;

			string text = String.Empty;

			switch( node.Name.ToLowerInvariant() )
			{
				case "#text":
					return node.InnerText;

				case "br":
					nextLine = true;
					break;

				case "b":
				case "strong":
					fontStyle |= FontStyle.Bold;
					break;

				case "i":
				case "em":
				case "italic":
					fontStyle |= FontStyle.Italic;
					break;

				case "u":
					fontStyle |= FontStyle.Underline;
					break;

				case "s":
				case "strike":
					fontStyle |= FontStyle.Strikeout;
					break;

				case "ul":
					nextLine = true;
					isList = true;
					break;

				case "li":
					text = "<li>";
					nextLine = true;
					break;

				case "a":
					fontStyle |= FontStyle.Underline;
					color = Color.Blue;
					foreach( XmlAttribute attribute in node.Attributes )
					{
						switch( attribute.Name.ToLowerInvariant() )
						{
							case "href":
								link = attribute.Value;
								break;

							default:
								break;
						}
					}
					break;

				case "font":
					foreach( XmlAttribute attribute in node.Attributes )
					{
						switch( attribute.Name.ToLowerInvariant() )
						{
							case "size":
								if( attribute.Value.StartsWith( "+" ) )
									fontSize += (float) Convert.ToInt32( attribute.Value );
								else if( attribute.Value.StartsWith( "-" ) )
									fontSize -= (float) Convert.ToInt32( attribute.Value );
								else
									fontSize = (float) Convert.ToInt32( attribute.Value );
								break;

							case "color":
								color = ColorTranslator.FromHtml( attribute.Value );
								break;

							default:
								break;
						}
					}
					break;

				default:
					Log.Debug( "[HtmlParser]", "Unhandled node '{0}'", node.Name );
					break;
			}

			foreach( XmlAttribute attribute in node.Attributes )
			{
				switch( attribute.Name.ToLowerInvariant() )
				{
					case "style":
						ParseStyleAttribute( attribute.Value, ref fontStyle, ref color );
						break;

					default:
						break;
				}
			}

			font = new Font( font.FontFamily, fontSize, fontStyle, GraphicsUnit.Point );
			return text;
		}

		private static void ParseStyleAttribute( string value, ref FontStyle fontStyle, ref Color color )
		{
			if( String.IsNullOrEmpty( value ) )
				return;

			string[] nodes = value.Split( new char[] { ';' } );
			foreach( string node in nodes )
			{
				string[] parts = node.Split( new char[] { ':' } );
				if( parts.Length != 2 )
					break;

				switch( parts[0].Trim().ToLowerInvariant() )
				{
					case "text-decoration":
						switch( parts[1].Trim().ToLowerInvariant() )
						{
							case "none":
								fontStyle ^= FontStyle.Underline;
								fontStyle ^= FontStyle.Strikeout;
								break;

							case "underline":
								fontStyle |= FontStyle.Underline;
								break;

							case "line-through":
								fontStyle |= FontStyle.Strikeout;
								break;

							default:
								break;
						}
						break;

					case "color":
						color = ColorTranslator.FromHtml( value.Trim() );
						break;

					default:
						break;
				}
			}
		}
	}
}
