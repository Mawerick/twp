﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Utilities
{
	/// <summary>
	/// Description of Line.
	/// </summary>
	[DefaultProperty( "LineStyle" )]
	[ToolboxItem( "System.Windows.Forms.Design.AutoSizeToolboxItem,System.Design" )]
	[ToolboxBitmap( typeof( Twp.Utilities.Line ) )]
	public class Line : UserControl
	{
		/// <summary>
		/// Gets or sets a value representing the orientation of the line.
		/// The default is Horizontal.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The orientation of the line." )]
		[DefaultValue( Orientation.Horizontal )]
		public Orientation Orientation
		{
			get { return this.orientation; }
			set
			{
				if( this.orientation != value )
				{
					this.orientation = value;
					this.Invalidate();
				}
			}
		}
		private Orientation orientation = Orientation.Horizontal;

		/// <summary>
		/// Gets or sets a value representing the style of the line.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The style of the line." )]
		[DefaultValue( LineStyle.Sunken )]
		public LineStyle LineStyle
		{
			get { return this.lineStyle; }
			set
			{
				if( this.lineStyle != value )
				{
					this.lineStyle = value;
					this.Invalidate();
				}
			}
		}
		private LineStyle lineStyle;

		/// <returns>The default <see cref="System.Drawing.Size"/> of the line.</returns>
		protected override Size DefaultSize
		{
			get { return new Size( 150, 10 ); }
		}

		/// <summary>
		/// Gets the space, in pixels, that is specified by default between controls.
		/// </summary>
		protected override Padding DefaultMargin
		{
			get { return new Padding( 0 ); }
		}

		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPaint( PaintEventArgs e )
		{
//			base.OnPaint( e );
			Point start = Point.Empty;
			Point end = Point.Empty;
			Color light = Color.Empty;
			Color dark = Color.Empty;
			switch( this.lineStyle )
			{
				case LineStyle.Sunken:
					light = Color.FromKnownColor( KnownColor.ButtonHighlight );
					dark = Color.FromKnownColor( KnownColor.ButtonShadow );
					break;
				case LineStyle.Raised:
					light = Color.FromKnownColor( KnownColor.ButtonShadow );
					dark = Color.FromKnownColor( KnownColor.ButtonHighlight );
					break;
				case LineStyle.Flat:
					dark = Color.FromKnownColor( KnownColor.ButtonShadow );
					break;
				default:
					return;
			}
			switch( this.orientation )
			{
				case Orientation.Horizontal:
					start = new Point( this.ClientRectangle.X,
					                  this.ClientRectangle.Y + (this.ClientRectangle.Height / 2) - 1 );
					end = new Point( start.X + this.ClientRectangle.Width, start.Y );
					e.Graphics.DrawLine( new Pen( dark ), start, end );
					if( light != Color.Empty )
					{
						start.Offset( 0, 1 );
						end.Offset( 0, 1 );
						e.Graphics.DrawLine( new Pen( light ), start, end );
					}
					break;
				case Orientation.Vertical:
					start = new Point( this.ClientRectangle.X + (this.ClientRectangle.Width / 2) - 1,
					                  this.ClientRectangle.Y );
					end = new Point( start.X, start.Y + this.ClientRectangle.Height );
					e.Graphics.DrawLine( new Pen( dark ), start, end );
					if( light != Color.Empty )
					{
						start.Offset( 1, 0 );
						end.Offset( 1, 0 );
						e.Graphics.DrawLine( new Pen( light ), start, end );
					}
					break;
			}
		}

		#region Designer required code

		/// <summary>
		///
		/// </summary>
		public Line()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.Name = "Line";
		}

		#endregion
	}

	/// <summary>
	/// Specifies the style used to paint a line.
	/// </summary>
	public enum LineStyle
	{
		/// <summary>The line has a sunken look.</summary>
		Sunken,
		/// <summary>The line has a raised look.</summary>
		Raised,
		/// <summary>The line has a flat look.</summary>
		Flat,
	}
}
