// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace Twp.Controls
{
	public class TextSegment : ISegment
	{
		private List<Word> words;
		public List<Word> Words
		{
			get { return this.words; }
			set { this.words = value; }
		}

		public Word GetWord( int column )
		{
			int curColumn = 0;
			foreach( Word word in this.words )
			{
				if( column < curColumn + word.Length )
				{
					return word;
				}
				curColumn += word.Length;
			}
			return null;
		}
		
		public int Offset
		{
			get { throw new NotImplementedException(); }
		}
		
		public int Length
		{
			get { throw new NotImplementedException(); }
		}
	}
}
