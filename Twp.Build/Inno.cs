﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Twp.Build
{
	public class Inno : ICompiler
	{
		private string path;
		private string executable;

		public Inno( string path, string executable )
		{
            if( path != null && executable != null )
            {
                this.path = Path.GetFullPath( path );
                this.executable = executable;
            }
            else
            {
                this.executable = Path.Combine( "Inno Setup 5", "iscc.exe" );
                this.path = Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles ) + Path.DirectorySeparatorChar;
                if( !this.IsInstalled )
                    this.path = "C:/Program Files/";
                if( !this.IsInstalled )
                    this.path = "C:/Program Files (x86)/";
            }
		}

		public bool IsInstalled
		{
			get { return File.Exists( this.FullPath ); }
		}

		public string FullPath
		{
			get { return Path.Combine( this.path, this.executable ); }
		}

		private string errorMessage = null;
		public string ErrorMessage
		{
			get { return this.errorMessage; }
		}

		private bool quiet = false;
		public bool Quiet
		{
			get { return this.quiet; }
			set { this.quiet = value; }
		}
        
		public bool Run( string workingDirectory, string script )
		{
			// Check if Inno Setup is installed
			if( !this.IsInstalled )
			{
				this.errorMessage = "Unable to locate Inno Setup. Cannot create installer.";
				return false;
			}
			// Some preperations
			script = Path.GetFullPath( script );
			workingDirectory = Path.GetFullPath( workingDirectory );
			StringBuilder args = new StringBuilder();
			if( this.quiet )
				args.Append( "/Q " );
			args.Append( "\"" + script + "\"" );
			this.WriteLine( "Arguments: " + args.ToString() );

			// Prepare process
			ProcessStartInfo info = new ProcessStartInfo( this.FullPath, args.ToString() );
			info.WorkingDirectory = workingDirectory;
			info.UseShellExecute = false;
			info.RedirectStandardOutput = true;

			// Start process
			Process process = Process.Start( info );
			Console.WriteLine( process.StandardOutput.ReadToEnd() );
			process.WaitForExit();
			int exitCode = process.ExitCode;
			process.Dispose();
			if( exitCode == 0 )
				return true;

			// Report error
			this.errorMessage = String.Format( "Inno Setup terminated with error code {0}", exitCode );
			return false;
		}

		private void WriteLine( string format, params object[] args )
		{
			if( this.quiet )
				return;

			Console.WriteLine( "Twp.Build: [Inno] " + format, args );
		}
	}
}
