﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Twp.Build
{
    public class Parser : ICompiler
    {
        private FileInfo fileInfo;
        private string workingDirectory;
        private ParserInfo info = new ParserInfo();
        private Dictionary<string,List<FileInfo>> files = new Dictionary<string,List<FileInfo>>();
        private List<FileInfo> output;
        private List<ShortCut> shortCuts;

        private bool quiet = false;
        public bool Quiet
        {
            get { return this.quiet; }
            set { this.quiet = value; }
        }

        private string errorMessage = null;
        public string ErrorMessage
        {
            get { return this.errorMessage; }
        }

        public bool Run( string workingDirectory, string script )
        {
            this.fileInfo = new FileInfo( Path.Combine( workingDirectory, script ) );
            this.workingDirectory = workingDirectory;
            if( !this.fileInfo.Exists )
            {
                this.errorMessage = String.Format( "Script File not found: {0}", this.fileInfo );
                return false;
            }

            using( StreamReader sr = this.fileInfo.OpenText() )
            {
                while( !sr.EndOfStream )
                {
                    string line = this.ReadLine( sr );

                    if( String.IsNullOrEmpty( line ) || line.StartsWith( "#" ) )
                        continue;

                    string[] parts = line.Split( '=' );
                    if( parts.Length != 2 )
                        this.WriteLine( "Failed to parse line: {0}", line );
                    else
                        this.ParseToken( parts[0], parts[1].Trim() );
                }
                sr.Close();
            }

            foreach( FileInfo outFile in this.output )
            {
                if( !this.ProcessFile( outFile ) )
                    return false;
            }

            return true;
        }

        private string ReadLine( StreamReader sr )
        {
            string line = sr.ReadLine();
            if( String.IsNullOrEmpty( line ) )
                return null;

            if( line.EndsWith( "\\" ) )
            {
                line = line.TrimEnd( '\\' );
                line = line.TrimEnd();
                line += " " + this.ReadLine( sr );
            }
            return line.Trim();
        }

        private void ParseToken( string token, string value )
        {
            string trimmed = value.Trim( '"', '\'' );
            switch( token )
            {
                case "PRODUCT_NAME":
                    this.info.Product = trimmed;
                    this.WriteLine( "Product Name: {0}", this.info.Product );
                    break;

                case "PRODUCT_VERSION":
                    this.info.Version = new VersionInfo( trimmed );
                    this.WriteLine( "Version: {0}", this.info.Version );
                    break;

                case "PRODUCT_DESCRIPTION":
                    this.info.Description = trimmed;
                    this.WriteLine( "Description: {0}", this.info.Description );
                    break;

                case "PRODUCT_COMPANY":
                    this.info.Company = trimmed;
                    this.WriteLine( "Company: {0}", this.info.Company );
                    break;

                case "PRODUCT_COPYRIGHT":
                    this.info.Copyright = trimmed;
                    this.WriteLine( "Copyright: {0}", this.info.Copyright );
                    break;

                case "PRODUCT_LICENSE":
                    this.info.License = trimmed;
                    this.WriteLine( "License: {0}", this.info.License );
                    break;

                case "PRODUCT_URL":
                    this.info.URL = trimmed;
                    this.WriteLine( "URL: {0}", this.info.URL );
                    break;

                case "FILES":
                    this.WriteLine( "Files:" );
                    if( this.files.ContainsKey( "default" ) )
                        this.files["default"].AddRange( this.ParseFiles( value ) );
                    else
                        this.files.Add( "default", this.ParseFiles( value ) );
                    break;

                case "SHORTCUTS":
                    this.WriteLine( "Shortcuts:" );
                    this.ParseShortCuts( trimmed );
                    break;

                case "OUTPUT":
                    this.WriteLine( "Output Files:" );
                    this.output = this.ParseOutputFiles( value );
                    break;

                default:
                    if( token.StartsWith( "FILES_" ) )
                    {
                        string section = token.Substring( 6 );
                        this.WriteLine( "Files ({0}):", section );
                        if( this.files.ContainsKey( section ) )
                            this.files[section].AddRange( this.ParseFiles( value ) );
                        else
                            this.files.Add( section, this.ParseFiles( value ) );
                    }
                    else
                        this.WriteLine( "Unknown token: {0} (with value {1})", token, value );
                    break;
            }
        }

        private List<FileInfo> ParseFiles( string input )
        {
            List<FileInfo> files = new List<FileInfo>();
            string[] fileNames = input.Split( ' ' );
            foreach( string fileName in fileNames )
            {
                string trimmed = fileName.Trim( '"', '\'' );
                FileInfo file = new FileInfo( Path.Combine( this.workingDirectory, trimmed ) );
                this.WriteLine( "FileName: {0}", file.FullName );
                files.Add( file );
            }
            return files;
        }

        private List<FileInfo> ParseOutputFiles( string input )
        {
            List<FileInfo> files = new List<FileInfo>();
            string[] fileNames = input.Split( ' ' );
            foreach( string fileName in fileNames )
            {
                string trimmed = fileName.Trim( '"', '\'' );
                FileInfo file = new FileInfo( Path.Combine( this.fileInfo.Directory.Parent.FullName, trimmed ) );
                this.WriteLine( "FileName: {0}", file.FullName );
                files.Add( file );
            }
            return files;
        }

        private void ParseShortCuts( string input )
        {
            this.shortCuts = new List<ShortCut>();
            string[] files = input.Split( ' ' );
            foreach( string file in files )
            {
                string[] parts = file.Split( '|' );
                if( parts.Length < 2 )
                {
                    this.WriteLine( "Failed to parse shortcut: {0}", file );
                }
                else
                {
                    ShortCut sc = new ShortCut();
                    sc.FileName = parts[0];
                    sc.Link = parts[1].Replace( '_', ' ' );

                    if( parts.Length > 2 && parts[2] == "true" )
                    {
                        sc.Startup = true;
                        if( parts.Length > 3 )
                            sc.Args = parts[3];
                    }

                    this.WriteLine( "Shortcut: {0}", sc );
                    this.shortCuts.Add( sc );
                }
            }
        }

        private string currentExt = "";

        private bool ProcessFile( FileInfo file )
        {
            FileInfo input = new FileInfo( file.FullName + ".in" );
            if( !input.Exists )
            {
                this.errorMessage = String.Format( "OUTPUT ERROR! File not found: {0}", input.FullName );
                return false;
            }

            if( file.Exists )
            {
                file.Delete();
            }

            this.currentExt = file.Extension;
            this.fileIndex = 0;

            this.WriteLine( "Processing {0}...", input.FullName );
            using( StreamWriter sw = file.CreateText() )
            {
                using( StreamReader sr = input.OpenText() )
                {
                    while( !sr.EndOfStream )
                    {
                        string line = sr.ReadLine();
                        line = this.ProcessLine( line );
                        if( line.Contains( "\n" ) )
                        {
                            string[] lines = line.Split( '\n' );
                            foreach( string subLine in lines )
                            {
                                sw.WriteLine( subLine );
                            }
                        }
                        else
                        {
                            sw.WriteLine( line );
                        }
                    }
                    sr.Close();
                }
                sw.Close();
            }

            return true;
        }

        private string ProcessLine( string line )
        {
            return Regex.Replace( line, @"@([A-Z_]\w+)?@", new MatchEvaluator( ProcessToken ) );
        }

        private string ProcessToken( Match match )
        {
            string value = "";
            switch( match.Value )
            {
                case "@PRODUCT@":
                    value = this.info.Product;
                    break;

                case "@VERSION@":
                    value = this.info.Version.ToString();
                    break;

                case "@VERSION_SHORT@":
                    value = this.info.Version.Short;
                    break;

                case "@MAJOR@":
                    value = this.info.Version.Major;
                    break;

                case "@MINOR@":
                    value = this.info.Version.Minor;
                    break;

                case "@REVISION@":
                    value = this.info.Version.Revision;
                    break;

                case "@BUILD@":
                    value = this.info.Version.Build;
                    break;

                case "@DESCRIPTION@":
                    value = this.info.Description;
                    break;

                case "@COMPANY@":
                    value = this.info.Company;
                    break;

                case "@COPYRIGHT@":
                    value = this.info.Copyright;
                    break;

                case "@URL@":
                    value = this.info.URL;
                    break;

                case "@CWD@":
                    value = this.workingDirectory;
                    break;

                case "@LICENSE@":
                    FileInfo file = new FileInfo( Path.Combine( this.workingDirectory, this.info.License ) );
                    value = file.FullName;
                    break;

                case "@FILES@":
                    value = ProcessFiles( match.Index, "default" );
                    break;

                case "@SHORTCUTS@":
                    value = ProcessShortCuts( match.Index );
                    break;

                case "@STARTUP@":
                    value = ProcessStartup( match.Index );
                    break;

                case "@UNINST_STARTUP@":
                    value = ProcessUninstStartup( match.Index );
                    break;

                default:
                    if( match.Value.StartsWith( "@FILES_" ) )
                    {
                        string section = match.Value.Substring( 7, match.Value.Length - 8 );
                        value = ProcessFiles( match.Index, section );
                    }
                    break;
            }
            return value;
        }

        private int fileIndex = 0;

        private string ProcessFiles( int pad, string section )
        {
            string files = null;
            if( !this.files.ContainsKey( section ) )
            {
                return null;
            }
            foreach( FileInfo file in this.files[section] )
            {
                if( files != null )
                    files += "\n" + new String( ' ', pad );
                switch( this.currentExt )
                {
                    case ".nsi":
                        files += "File \"" + file.FullName + "\"";
                        break;

                    case ".iss":
                        files += String.Format( "Source: \"{0}\"; DestDir: \"{{app}}\"; Flags: ignoreversion", file.FullName );
                        break;
                        
                    case ".ini":
                        files += String.Format( "File{0} = {1}", this.fileIndex++, file.FullName );
                        break;

                    default:
                        files += file.FullName;
                        break;
                }
            }
            return files;
        }

        private string ProcessShortCuts( int pad )
        {
            string text = null;
            foreach( ShortCut sc in this.shortCuts )
            {
                if( text != null )
                    text += "\n" + new String( ' ', pad );
                switch( this.currentExt )
                {
                    case ".nsi":
                        text += String.Format( "CreateShortCut \"$SMPROGRAMS\\$StartMenuFolder\\{0}.lnk\" \"$INSTDIR\\{1}\"", sc.Link, sc.FileName );
                        break;

                    case ".iss":
                        text += String.Format( "Name: \"{{group}}\\{0}\"; Filename: \"{{app}}\\{1}\"", sc.Link, sc.FileName );
                        break;

                    default:
                        text += sc.ToString();
                        break;
                }
            }
            return text;
        }

        private string ProcessStartup( int pad )
        {
            string text = null;
            foreach( ShortCut sc in this.shortCuts )
            {
                if( !sc.Startup )
                    continue;

                if( text != null )
                    text += "\n" + new String( ' ', pad );
                switch( this.currentExt )
                {
                    case ".nsi":
                        text += String.Format( "CreateShortCut \"$SMSTARTUP\\{0}.lnk\" \"$INSTDIR\\{1}\" \"{2}\"", sc.Link, sc.FileName, sc.Args );
                        break;
                    
                    case ".iss":
                        text += String.Format( "Name: \"{{userstartup}}\\{0}\"; Filename: \"{{app}}\\{1}\"", sc.Link, sc.FileName );
                        break;

                    default:
                        text += sc.ToString();
                        break;
                }
            }
            return text;
        }

        private string ProcessUninstStartup( int pad )
        {
            string text = null;
            foreach( ShortCut sc in this.shortCuts )
            {
                if( !sc.Startup )
                    continue;

                if( text != null )
                    text += "\n" + new String( ' ', pad );
                switch( this.currentExt )
                {
                    case ".nsi":
                        text += String.Format( "Delete \"$SMSTARTUP\\{0}.lnk\"", sc.Link );
                        break;

                    case ".iss":
                        break;

                    default:
                        text += sc.ToString();
                        break;
                }
            }
            return text;
        }

        private void WriteLine( string format, params object[] args )
        {
            if( this.quiet )
                return;

            Console.WriteLine( "Twp.Build: [Parser] " + format, args );
        }
    }
}
