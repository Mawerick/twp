﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Build
{
    internal class ParserInfo
    {
        private string product;
        public string Product
        {
            get { return this.product; }
            set { this.product = value; }
        }

        private string description;
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        private string company;
        public string Company
        {
            get { return this.company; }
            set { this.company = value; }
        }

        private string copyright;
        public string Copyright
        {
            get { return this.copyright; }
            set { this.copyright = value; }
        }

        private string license;
        public string License
        {
            get { return this.license; }
            set { this.license = value; }
        }

        private string url;
        public string URL
        {
            get { return this.url; }
            set { this.url = value; }
        }

        private VersionInfo version;
        public VersionInfo Version
        {
            get { return this.version; }
            set { this.version = value; }
        }
    }

    internal class VersionInfo
    {
        public VersionInfo( string version )
        {
            string[] parts = version.Split( '.', ',' );
            if( parts.Length != 4 )
                throw new ArgumentException( "Version Format must be ##.##.##.##!" );

            this.major = parts[0].Trim();
            this.minor = parts[1].Trim();
            this.revision = parts[2].Trim();
            this.build = parts[3].Trim();
        }

        private string major;
        public string Major
        {
            get { return this.major; }
        }

        private string minor;
        public string Minor
        {
            get { return this.minor; }
        }

        private string revision;
        public string Revision
        {
            get { return this.revision; }
        }

        private string build;
        public string Build
        {
            get { return this.build; }
        }

        public string Short
        {
            get { return string.Format( "{0}.{1}.{2}", this.major, this.minor, this.revision ); }
        }

        public override string ToString()
        {
            return string.Format( "{0}.{1}.{2}.{3}", this.major, this.minor, this.revision, this.build );
        }
    }
}
