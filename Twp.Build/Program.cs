﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;

namespace Twp.Build
{
	public class Program
	{
	    [STAThread]
		public static void Main( string[] args )
		{
            Program program = new Program();
            program.Init( args );
            program.Run();
        }

        private enum Mode {
            Build,
            Inno,
            NSIS,
            Zip,
        };

        private Mode mode;
        private ZipMode zipMode = ZipMode.Binary;
        private string script;

        private string path;
        private string executable;

        private Arguments arguments;
        private string workingDirectory;
        private bool quiet;

        private void Init( string[] args )
        {
            this.arguments = new Arguments( args );
            if( this.arguments["help"] != null || this.arguments["h"] != null || args.Length == 0 )
            {
                this.ShowHelp();
                Environment.Exit( 0 );
            }

            if( this.arguments["quiet"] != null || this.arguments["q"] != null )
                this.quiet = true;

            if( this.arguments["mode"] != null )
            {
                try
                {
                    this.mode = (Mode) Enum.Parse( typeof( Mode ), this.arguments["mode"], true );
                }
                catch( Exception )
                {
                    Console.WriteLine( "Invalid mode {0}. Aborting.", this.arguments["mode"] );
                    Environment.Exit( 1 );
                }
#if DEBUG
                if( this.mode != Mode.Build )
                {
                    this.WriteLine( "Installer creating is not available in Debug mode." );
                    Environment.Exit( 0 );
                }
#endif
                this.Debug( "Mode: {0}", this.mode );
            }
            else
            {
                Console.WriteLine( "Error: Mode not set." );
                this.ShowHelp();
            }

            if( this.arguments["script"] != null )
            {
                this.script = this.arguments["script"];
                this.Debug( "Script: " + script );
                if( !File.Exists( this.script ) )
                {
                    this.WriteLine( "Script file not found at {0}.", this.script );
                    Environment.Exit( 2 );
                }
            }
            else
            {
                this.WriteLine( "No script file provided. Aborting." );
                Environment.Exit( 1 );
            }

            this.workingDirectory = Environment.CurrentDirectory;
            if( this.arguments["wd"] != null )
            {
                this.workingDirectory = this.arguments["wd"];
            }
            else if( this.arguments["workingDirectory"] != null )
            {
                this.workingDirectory = this.arguments["workingDirectory"];
            }
            this.Debug( "Working directory: " + workingDirectory );

            if( this.arguments["path"] != null && this.arguments["executable"] != null )
            {
                this.path = this.arguments["path"];
                this.executable = this.arguments["executable"];
                this.Debug( "Executable: {0} (in: {1})", this.executable, this.path );
            }

            if( this.arguments["zipmode"] != null )
            {
                switch( this.arguments["zipmode"].ToLower() )
                {
                    case "binary":
                        this.zipMode = ZipMode.Binary;
                        break;
                        
                    case "source":
                        this.zipMode = ZipMode.Source;
                        break;
                        
                    default:
                        Console.WriteLine( "Invalid Zip mode {0}. Aborting.", this.arguments["zipmode"] );
                        Environment.Exit( 1 );
                    	break;
                }
            }
        }

        private void Run()
        {
            ICompiler compiler = null;
            switch( this.mode )
            {
                case Mode.Build:
                    compiler = new Parser();
                    break;
                case Mode.Inno:
                    compiler = new Inno( this.path, this.executable );
                    break;
                case Mode.NSIS:
                    compiler = new NSIS( this.path, this.executable );
                    break;
                case Mode.Zip:
                    compiler = new Zip( this.zipMode, this.path, this.executable );
                    break;
            }
            compiler.Quiet = this.quiet;
            bool success = compiler.Run( this.workingDirectory, this.script );
            if( !success )
            {
                this.WriteLine( "Compiler error: {0}", compiler.ErrorMessage );
                Environment.Exit( 3 );
            }
        }
        
        private void ShowHelp()
        {
            Console.WriteLine( "Twp.Build is a utility for pre- and post-build events." );
            Console.WriteLine();
            Console.WriteLine( "Available arguments:" );
            Console.WriteLine( "  -help (-h)" );
            Console.WriteLine( "    Optional. Displays this output" );
            Console.WriteLine();
            Console.WriteLine( "  -mode" );
            Console.WriteLine( "    Required. Sets the operating mode. Valid modes are: Build, Inno, NSIS, Zip" );
            Console.WriteLine();
            Console.WriteLine( "  -script=\"...\"" );
            Console.WriteLine( "    Required. The script file that should be run" );
            Console.WriteLine();
            Console.WriteLine( "  -quiet (-q)" );
            Console.WriteLine( "    Optional. Suppress non-critical output" );
            Console.WriteLine();
            Console.WriteLine( "  -workingDirectory=\"...\" (-wd)" );
            Console.WriteLine( "    Optional. Sets the working directory to start the installer script in" );
            Console.WriteLine();
            Console.WriteLine( "Optional arguments for Inno/NSIS:" );
            Console.WriteLine( "  -executable=\"...\"" );
            Console.WriteLine( "    Defines the script compiler that needs to be run (only works if path is also defined)" );
            Console.WriteLine();
            Console.WriteLine( "  -path=\"...\"" );
            Console.WriteLine( "    Defines the path of the script compiler" );
            Console.WriteLine();
            Console.WriteLine( "Optional arguments for Zip:" );
            Console.WriteLine( "  -zipmode" );
            Console.WriteLine( "    Sets the operating mode for the zip compiler. Valid modes are: Binary (default), Source" );
            Console.WriteLine();
            Console.WriteLine( "  -executable=\"...\"" );
            Console.WriteLine( "    Defines the svn client used to export a clean copy of the source tree (only works if path is also defined)" );
            Console.WriteLine();
            Console.WriteLine( "  -path=\"...\"" );
            Console.WriteLine( "    Defines the path of the svn client" );
        }

        private void Debug( string format, params object[] args )
        {
            if( this.quiet )
                return;

            this.WriteLine( format, args );
        }

        private void WriteLine( string format, params object[] args )
		{
			Console.WriteLine( "Twp.Build: " + format, args );
		}
	}
}
