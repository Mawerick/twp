﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Build
{
	public class ShortCut
	{
		private string fileName;
		public string FileName
		{
			get { return this.fileName; }
			set { this.fileName = value; }
		}

		private string link;
		public string Link
		{
			get { return this.link; }
			set { this.link = value; }
		}

		private bool startup = false;
		public bool Startup
		{
			get { return this.startup; }
			set { this.startup = value; }
		}

		private string args;
		public string Args
		{
			get { return this.args; }
			set { this.args = value; }
		}

		public override string ToString()
		{
			string text = String.Format( "{0} => {1}", this.link, this.fileName );
			if( this.startup )
				text += String.Format( " (+startup, args: {0})", this.args );
			return text;
		}
	}
}
