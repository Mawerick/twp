﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32;

namespace Twp.Build
{
    public enum ZipMode {
        Source,
        Binary
    }

    /// <summary>
    /// Description of Zip.
    /// </summary>
    public class Zip : ICompiler
    {
        public Zip( ZipMode mode, string path, string executable )
        {
            this.mode = mode;
            if( path != null && executable != null )
            {
                this.path = Path.GetFullPath( path );
                this.executable = executable;
            }
            else
            {
                this.executable = "svn.exe";
                this.path = this.GetRegistryPath();
                if( !this.IsInstalled )
                    this.path = Twp.Utilities.Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles ), "TortoiseSVN", "bin" );
                if( !this.IsInstalled )
                    this.path = "C:\\Program Files\\TortoiseSVN\\bin";
                if( !this.IsInstalled )
                    this.path = "C:\\Program Files (x86)\\TortoiseSVN\\bin";
            }
        }

        private string errorMessage = null;
        public string ErrorMessage
        {
            get { return this.errorMessage; }
        }

        private bool quiet = false;
        public bool Quiet
        {
            get { return this.quiet; }
            set { this.quiet = value; }
        }

        private ZipMode mode = ZipMode.Binary;
        public ZipMode Mode
        {
            get { return this.mode; }
            set { this.mode = value; }
        }

        private string path;
        private string executable;

        private string workingDirectory;
        private FileInfo scriptInfo;
        private Twp.Utilities.IniDocument scriptDoc = new Twp.Utilities.IniDocument();
        private FileInfo outputFile;
        private DirectoryInfo outputDir;
        private DirectoryInfo tempDir;

        private string GetRegistryPath()
        {
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey( "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{6B13A3F1-F66A-42FB-9E62-98952D582187}" );
            if( regKey == null )
            {
                regKey = Registry.LocalMachine.OpenSubKey( "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{6B13A3F1-F66A-42FB-9E62-98952D582187}" );
                if( regKey == null )
                    return "";
            }

            string path = (string) regKey.GetValue( "InstallLocation" );
            if( String.IsNullOrEmpty( path ) )
                return "";

            return path;
        }

        public bool IsInstalled
        {
            get { return File.Exists( this.FullPath ); }
        }

        public string FullPath
        {
            get { return Path.Combine( this.path, this.executable ); }
        }

        private bool SetupBinary()
        {
            this.outputFile = new FileInfo( Path.Combine( this.outputDir.FullName, this.scriptDoc["Output"]["BinaryFile"] ) );
            this.tempDir = new DirectoryInfo( Path.Combine( this.workingDirectory, Path.GetFileNameWithoutExtension( this.outputFile.Name ) ) );

            // Create temp folder
            if( !this.tempDir.Exists )
                this.tempDir.Create();

            // Copy files to temp folder
            this.WriteLine( "Copying files to temp folder {0}:", this.tempDir );
            foreach( KeyValuePair<string, Twp.Utilities.IniSection> section in this.scriptDoc.Sections )
            {
                if( !section.Key.StartsWith( "Files" ) )
                    continue;

                DirectoryInfo dir = this.tempDir;
                string[] parts = section.Key.Split( '_' );
                if( parts.Length > 1 )
                {
                    for( int i = 1; i < parts.Length; i++ )
                    {
                        dir = new DirectoryInfo( Path.Combine( dir.FullName, parts[i] ) );
                        if( !dir.Exists )
                            dir.Create();
                    }
                }

                foreach( string file in section.Value.Values )
                {
                    FileInfo fi = new FileInfo( file );
                    if( fi.Exists )
                    {
                        try
                        {
                            this.WriteLine( "File: {0}", fi );
                            fi.CopyTo( Path.Combine( dir.FullName, fi.Name ), true );
                        }
                        catch
                        {
                            this.tempDir.Delete( true );
                            this.errorMessage = "Failed to copy file to temp folder: " + fi.Name;
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private bool SetupSource()
        {
            if( !this.IsInstalled )
            {
                this.errorMessage = "Unable to locate SVN files";
                return false;
            }

            // Export svn code
            this.WriteLine( "SVN Path: {0}", this.FullPath );

            this.outputFile = new FileInfo( Path.Combine( this.outputDir.FullName, this.scriptDoc["Output"]["SourceFile"] ) );
            this.tempDir = new DirectoryInfo( Path.Combine( this.workingDirectory, Path.GetFileNameWithoutExtension( this.outputFile.Name ) ) );

            if( this.scriptDoc.Sections.ContainsKey( "Sources" ) )
            {
                foreach( KeyValuePair<string, string> source in this.scriptDoc["Sources"] )
                {
                    if( !this.tempDir.Exists )
                        this.tempDir.Create();
                    bool ok = this.SetupSource( source.Key, source.Value );
                    if( !ok )
                        return false;
                }
                return true;
            }
            else
            {
                return this.SetupSource( null, null );
            }
        }

        private bool SetupSource( string name, string path )
        {
            this.WriteLine( "Name: {0} path: {1}", name, path );
            
            DirectoryInfo dir = this.tempDir;
            if( !String.IsNullOrEmpty( name ) )
            {
                string[] parts = name.Split( '_' );
                for( int i = 0; i < parts.Length; i++ )
                {
                    dir = new DirectoryInfo( Path.Combine( dir.FullName, parts[i] ) );
                    if( i < parts.Length - 1 && !dir.Exists )
                        dir.Create();
                }
            }
            if( String.IsNullOrEmpty( path ) )
                path = this.workingDirectory;

            // Remove temp folder if it exists
            if( dir.Exists )
                dir.Delete( true );

            // Some preperations
            StringBuilder args = new StringBuilder();
            args.Append( "export " );
            if( this.quiet )
                args.Append( "-q " );
            args.Append( path );
            args.Append( " " );
            args.Append( dir.FullName );
            this.WriteLine( "Arguments: " + args.ToString() );

            // Prepare process
            ProcessStartInfo info = new ProcessStartInfo( this.FullPath, args.ToString() );
            info.WorkingDirectory = this.workingDirectory;
            info.UseShellExecute = false;
            info.RedirectStandardOutput = true;

            // Start process
            Process process = Process.Start( info );
            Console.WriteLine( process.StandardOutput.ReadToEnd() );
            process.WaitForExit();
            int exitCode = process.ExitCode;
            process.Dispose();
            if( exitCode != 0 )
            {
                // Report error
                this.errorMessage = String.Format( "SVN terminated with error code {0}", exitCode );
                return false;
            }

            return true;
        }

        public bool Run( string workingDirectory, string script )
        {
            this.workingDirectory = workingDirectory;
            
            // Read input script
            this.scriptInfo = new FileInfo( Path.Combine( this.workingDirectory, script ) );
            if( !this.scriptInfo.Exists )
            {
                this.errorMessage = String.Format( "Script File not found: {0}", this.scriptInfo );
                return false;
            }

            try
            {
                this.scriptDoc.Read( this.scriptInfo.FullName );
            }
            catch( Exception ex )
            {
                this.errorMessage = "Failed to parse script file: " + ex.ToString();
                return false;
            }

            this.outputDir = new DirectoryInfo( this.scriptDoc["Output"]["Path"] );
            if( !this.outputDir.Exists )
            {
                this.errorMessage = String.Format( "Output Directory not found: {0}", this.outputDir );
                return false;
            }

            bool ok = true;
            switch( this.mode )
            {
                case ZipMode.Source:
                    ok = this.SetupSource();
                    break;
                case ZipMode.Binary:
                    ok = this.SetupBinary();
                    break;
                default:
                    this.errorMessage = String.Format( "Invalid ZipMode: ", this.mode );
                    return false;
            }
            if( !ok )
                return false;

            try
            {
                // Create zip file
                FileInfo[] files = this.tempDir.GetFiles();
                this.WriteLine( "Creating output file {0}", this.outputFile );
                using( ZipOutputStream stream = new ZipOutputStream( this.outputFile.Create() ) )
                {
                    stream.SetLevel( 9 );

                    this.CompressFolder( this.tempDir, stream, this.tempDir.Parent.FullName.Length );

                    stream.Finish();
                    stream.Close();
                }
                
                // Clean up
                this.WriteLine( "Cleaning up." );
                this.tempDir.Delete( true );
            }
            catch( Exception ex )
            {
                this.tempDir.Delete( true );
                this.errorMessage = "Zip operation error: " + ex.ToString();
                return false;
            }
            
            return true;
        }
        
        private void CompressFolder( DirectoryInfo dir, ZipOutputStream stream, int offset )
        {
            FileInfo[] files = dir.GetFiles();
            foreach( FileInfo file in files )
            {
                string entryName = file.FullName.Substring( offset );
                entryName = ZipEntry.CleanName( entryName );
                this.WriteLine( "Zipping file: {0}", entryName );
                ZipEntry entry = new ZipEntry( entryName );
                entry.DateTime = file.LastWriteTime;
                stream.PutNextEntry( entry );

                byte[] buffer = new byte[4096];
                using( FileStream fs = file.OpenRead() )
                {
                    StreamUtils.Copy( fs, stream, buffer );
                }
                stream.CloseEntry();
            }

            DirectoryInfo[] subDirs = dir.GetDirectories();
            foreach( DirectoryInfo subDir in subDirs )
            {
                this.CompressFolder( subDir, stream, offset );
            }
        }

        private void WriteLine( string format, params object[] args )
        {
            if( this.quiet )
                return;

            string prefix = String.Format( "Twp.Build: [Zip|{0}] ", this.mode );
            Console.WriteLine( prefix + format, args );
        }
    }
}
