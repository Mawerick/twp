﻿// $Id$
//
//  Copyright (C) 2014 Mawerick, WrongPlace.Net
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct( "Twp.Controls.HtmlRenderer" )]
[assembly: AssemblyVersion( "1.5.0.*" )]

[assembly: AssemblyTitle( "Twp.Controls.HtmlRenderer" )]
[assembly: AssemblyDescription( "Html Rendering controls. Based on HtmlRenderer from CodePlex." )]
[assembly: AssemblyCompany( "WrongPlace.Net" )]
[assembly: AssemblyCopyright( "� Mawerick, WrongPlace.Net 2014" )]

[assembly: ComVisible( false )]

[assembly: Guid("ec8a9e7e-9a9d-43c3-aa97-f6f505b1d3ed")]
