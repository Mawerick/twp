﻿// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
// 
// - Sun Tsu,
// "The Art of War"

using System;
using System.Collections.Generic;
using System.Drawing;

namespace Twp.Controls.HtmlRenderer.Utils
{
    /// <summary>
    /// Utils for colors handling.
    /// </summary>
    internal static class ColorsUtils
    {
        #region Fields and Consts
        
        /// <summary>
        /// cache of all the colors used not to create same color again and again
        /// </summary>
        private static readonly Dictionary<string, Color> _colorsCache = new Dictionary<string, Color>();

        #endregion

        /// <summary>
        /// Check if the given color exists by name
        /// </summary>
        /// <param name="color">the color to check</param>
        /// <returns>true - color exists by given name, false - otherwise</returns>
        public static bool IsColorExists(string name)
        {
            return _colorsCache.ContainsKey(name);
        }

        /// <summary>
        /// Get cached font instance for the given font properties.<br/>
        /// Improve performance not to create same font multiple times.
        /// </summary>
        /// <returns>cached font instance</returns>
        public static Color GetCachedColor(string name)
        {
            var color = TryGetColor(name);
            if (color == Color.Empty)
            {
                color = Color.FromName(name);
                if(color.A > 0)
                    _colorsCache[name] = color;
            }
            return color;
        }

        /// <summary>
        /// Adds a color to be used.
        /// </summary>
        /// <param name="fontFamily">The font family to add.</param>
        public static void AddColor(string name, Color color)
        {
            ArgChecker.AssertArgNotNull(name, "name");
            ArgChecker.AssertArgNotNull(color, "color");

            _colorsCache[name.ToLower()] = color;
        }

        #region Private methods

        /// <summary>
        /// Get cached color if it exists in cache or <see cref="Color.Empty" /> if it is not.
        /// </summary>
        private static Color TryGetColor(string name)
        {
            Color color = Color.Empty;
            if (_colorsCache.ContainsKey(name))
            {
                color = _colorsCache[name];
            }
            return color;
        }

        #endregion
    }
}