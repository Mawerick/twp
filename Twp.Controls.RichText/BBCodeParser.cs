//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Collections.Generic;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls.RichText
{
    public class BBCodeParser : ParserBase
    {
        public BBCodeParser()
        {
            m_validTags = new ValidTag[] {
                new ValidTag( TagType.LineBreak, "br" ),
                new ValidTag( TagType.Italic, "i" ),
                new ValidTag( TagType.Bold, "b" ),
                new ValidTag( TagType.Underlined, "u" ),
                new ValidTag( TagType.Strikeout, "s" ),
                new ValidTag( TagType.Link, "a", new string[] { "href" } ),
                new ValidTag( TagType.Font, "font", new string[] { "color", "style" } ),
                new ValidTag( TagType.List, "ulist" ),
                new ValidTag( TagType.ListItem, "*", TagType.List ),
                new ValidTag( TagType.Indent, "-" ),
            };
        }

        ValidTag[] m_validTags;

        protected override char TagStart
        {
            get { return '['; }
        }

        protected override char TagEnd
        {
            get { return ']'; }
        }

        protected override char TagBreak
        {
            get { return '/'; }
        }

        protected override IList<ValidTag> ValidTags
        {
            get { return m_validTags; }
        }

        protected override void Debug( string fmt, params object[] args )
        {
            Log.Debug( "[BBCodeParser." + fmt, args );
            Application.DoEvents();
        }
    }
}
