﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Controls.RichText
{
    /// <summary>
    /// Description of Document.
    /// </summary>
    public class Document
    {
        #region Constructors

        public Document()
        {
        }

        #endregion

        #region Private fields

        List<Tag> m_tags = new List<Tag>();
        List<Tag> m_links = new List<Tag>();
        List<string> m_lines = new List<string>();
        string m_text;
        IParser m_parser;
        Renderer m_renderer;

        #endregion

        #region Properties

        public List<Tag> Segments
        {
            get { return m_tags; }
        }

        public List<Tag> Links
        {
            get { return m_links; }
        }

        public List<string> Lines
        {
            get { return m_lines; }
        }

        public string Text
        {
            get { return m_text; }
        }

        public IParser Parser
        {
            get { return m_parser; }
            set
            {
                if( m_parser != value )
                {
                    m_parser = value;
                    OnParserChanged();
                }
            }
        }

        public Renderer Renderer
        {
            get { return m_renderer; }
            set
            {
                if( m_renderer != value )
                {
                    m_renderer = value;
                    OnRendererChanged();
                }
            }
        }

        #endregion

        #region Methods

        protected virtual void OnParserChanged()
        {
            m_tags.Clear();
            m_text = null;
            if( m_parser != null )
            {
                foreach( string line in m_lines )
                {
                    m_tags.Add( m_parser.Parse( line ) );
                }
            }
        }

        protected virtual void OnRendererChanged()
        {
            
        }

        public string GetText( int offset, int length )
        {
            return String.Empty;
        }

        public void Append( string text )
        {
            if( m_parser == null )
                throw new NullReferenceException( "No Parser is defined!" );

            m_lines.Add( text );
            Tag tag = m_parser.Parse( text );
            m_tags.Add( tag );
            ProcessTag( tag );
        }

        void ProcessTag( Tag tag )
        {
            if( tag == null )
                return;

            if( tag.Type == TagType.Link && !tag.IsEndTag )
                m_links.Add( tag );

            if( tag.HasChildren )
            {
                foreach( Tag childTag in tag.Children )
                {
                    ProcessTag( childTag );
                }
            }
        }

        public void Clear()
        {
            m_tags.Clear();
            m_lines.Clear();
            m_links.Clear();
            m_text = null;
        }

        public void PerformLayout()
        {
            if( m_renderer == null )
                throw new NullReferenceException( "No Renderer is defined!" );

            foreach( Tag tag in m_tags )
            {
                m_renderer.MeasureTag( tag );
            }
        }

        public void Render()
        {
            if( m_renderer == null )
                throw new NullReferenceException( "No Renderer is defined!" );

            foreach( Tag tag in m_tags )
            {
                m_renderer.DrawTag( tag );
            }
        }

        #endregion
    }
}
