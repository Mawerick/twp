//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Twp.Controls.RichText
{
    public abstract class ParserBase : IParser
    {
        protected abstract char TagStart { get; }
        protected abstract char TagEnd { get; }
        protected abstract char TagBreak { get; }
        protected abstract IList<ValidTag> ValidTags { get; }

        string m_text;
        string m_plainText;
        StringReader m_reader;
        Tag m_root;
        Tag m_current;

        public string PlainText
        {
            get { return m_plainText; }
        }
        
        public Tag Root
        {
            get { return m_root; }
        }

        public Tag Parse( string text )
        {
            if( text == null )
                return null;

            m_text = text;
            m_plainText = String.Empty;

            // Create root segment.
            m_root = new Tag( TagType.None );
            m_current = m_root;

            // Create the string reader
            m_reader = new StringReader( text );

            // Start the parsing loop.
            while( true )
            {
                int c = m_reader.Peek();
                if( c == '\0' || c == -1 )
                    break;
                if( c == TagStart )
                    ParseTag();
                else
                    ParseText();
            }
            // If the first tag we found is a root tag (<html>), make it the real root.
            if( m_root.Children.Count == 1 && m_root.Children[0].Type == TagType.None )
            {
                m_current = m_root.Children[0];
                m_root.Children.Clear();
                m_root = m_current;
                m_root.Parent = null;
            }

            // Dispose of the reader.
            m_reader.Close();
            m_reader.Dispose();
            m_reader = null;

            // Trim the plaintext buffer...
            m_plainText = m_plainText.Trim();

            return m_root;
        }

        void ParseTag()
        {
            // Create a new tag, and add it into the tree
            Tag tag = new Tag();
            m_current.Children.Add( tag );
            m_current = tag;

            // Skip past the tag starter character
            if( m_reader.Peek() == TagStart )
                m_reader.Read();

            // Is this an end tag?
            if( m_reader.Peek() == TagBreak )
            {
                m_reader.Read();
                tag.IsEndTag = true;
            }

            // Parse start tag.
            ValidTag vTag = ParseTagType();
            if( vTag == null )
                return;

            tag.Type = vTag.Type;

            // Parse the rest of the tag for attributes.
            while( true )
            {
                int c = m_reader.Peek();
                // Have we run out of data?
                if( c == '\0' || c == -1 || c == TagEnd )
                    break;

                // Is this an empty tag, like <br/>?
                if( c == TagBreak && tag.Type != TagType.None )
                    break;

                // Move into the attributes
                if( Char.IsWhiteSpace( (char) c ) )
                {
                    m_reader.Read();
                    ParseTagAttributes( ref tag );
                }
            }

            if( m_reader.Peek() == TagBreak )
            {
                // It's an empty tag, not expecting an endtag, or content...
                m_reader.Read();
                if( m_reader.Peek() == TagEnd )
                    m_reader.Read();
                return;
            }

            // Skip past the tag end character.
            if( m_reader.Peek() == TagEnd )
                m_reader.Read();

            if( tag.IsEndTag )
            {
                // An end-tag needs to be reparented. Look for the nearest matching starter tag.
                Tag temp = tag.Parent;
                while( temp != null )
                {
                    if( temp != null && temp.Type == vTag.Type && !temp.IsEndTag )
                    {
                        // We found our match. Grab it's parent.
                        temp = temp.Parent;
                        break;
                    }
                    temp = temp.Parent;
                }
                if( temp != null )
                {
                    // Reparenting gets done inside the Add method.
                    temp.Children.Add( tag );
                    m_current = temp;
                }
                m_current.Children.Remove( tag );
                return;
            }

            // Does this tag require a certain parent? (i.e. list items)
            if( vTag.Parent != TagType.None )
            {
                // Look for the nearest matching parent tag.
                Tag temp = tag.Parent;
                while( temp != null )
                {
                    if( temp != null && temp.Type == vTag.Parent )
                    {
                        // We found our match. Grab it and go.
                        break;
                    }
                    temp = temp.Parent;
                }
                if( temp != null )
                {
                    temp.Children.Add( tag );
                }
            }

            // Skip further parsing if it's a linebreak tag...
            if( vTag.Type == TagType.LineBreak )
            {
                m_current = tag.Parent;
                if( IsParentOrLastTagAList( true ) || IsParentTagType( TagType.None ) || m_current.Children.Count == 1 )
                    m_current.Children.Remove( tag );
                return;
            }
            if( vTag.Type == TagType.HLine )
            {
                m_current = tag.Parent;
                return;
            }

            // Parse content, wait for end-tag
            while( true )
            {
                int c = m_reader.Peek();
                // Check for end-of-data
                if( c == '\0' || c == -1 )
                    break;
                // Do we have a new tag? or just text?
                if( c == TagStart )
                    ParseTag();
                else
                    ParseText();
            }

            // Reset our tree pointer.
            m_current = tag.Parent;
        }

        ValidTag ParseTagType()
        {
            string name = String.Empty;
            while( true )
            {
                int c = m_reader.Peek();
                // Check for end-of-data
                if( c == '\0' || c == -1 || c == TagEnd )
                    break;

                // Check for simple tag ends (tags like <this/>)
                if( c == TagBreak && !String.IsNullOrEmpty( name ) )
                    break;

                if( Char.IsWhiteSpace( (char) c ) )
                    break;

                name += (char) m_reader.Read();
            }

            foreach( ValidTag tag in ValidTags )
            {
                if( tag.Name == name )
                {
                    return tag;
                }
            }
            Debug( "ParseTag] Invalid tag name: {0}", name );
            return null;
        }

        void ParseTagAttributes( ref Tag tag )
        {
            string key = String.Empty;
            string value = String.Empty;

            bool onKey = true;

            while( true )
            {
                int c = m_reader.Peek();
                // Check for end-of-data
                if( c == '\0' || (int) c == -1 )
                    break;

                if( c == TagBreak || c == TagEnd )
                    break;

                if( Char.IsWhiteSpace( (char) c ) )
                {
                    onKey = true;
                    tag.Attributes.Add( key, ParseTagAttributeValue( key, value ) );
                    m_reader.Read();
                    key = String.Empty;
                    continue;
                }

                if( c == '=' )
                {
                    onKey = false;
                    m_reader.Read();
                    continue;
                }

                if( onKey == true )
                    key += (char) m_reader.Read();
                else
                    value = GetQuotedString();
            }

            if( !String.IsNullOrEmpty( key ) )
                tag.Attributes.Add( key, ParseTagAttributeValue( key, value ) );
        }

        object ParseTagAttributeValue( string key, string value )
        {
            if( String.IsNullOrEmpty( key ) || String.IsNullOrEmpty( value ) )
                return 0;

            switch( key.ToLowerInvariant() )
            {
                case "style":
                    return ParseStyle( value );

                case "size":
                    return Convert.ToInt32( value );

                case "color":
                    return value[0] == '#' ? ColorTranslator.FromHtml( value ) : Color.FromName( value );

                default:
                    return value;
            }
        }

        Dictionary<string, string> ParseStyle( string data )
        {
            Dictionary<string, string> styles = new Dictionary<string, string>();
            string[] lines = data.ToLowerInvariant().Split( ';' );
            foreach( string line in lines )
            {
                string[] style = line.Split( ':' );
                if( style.Length == 2 )
                    styles.Add( style[0].Trim(), style[1].Trim() );
            }
            return styles;
        }

        void ParseText()
        {
            Tag tag = new Tag( TagType.Text );
            m_current.Children.Add( tag );

            Word word = new Word();
            while( true )
            {
                int c = m_reader.Peek();
                if( c == (int) '\0' || c == -1 || c == (int) TagStart )
                    break;
                if( c == (int) '\r' )
                {
                    m_reader.Read();
                    continue;
                }

                if( Char.IsWhiteSpace( (char) c ) )
                {
                    word.Trim();
                    if( !String.IsNullOrEmpty( word.Text ) )
                    {
                        tag.Words.Add( word );
                        tag.Words.Add( new Word( " " ) );
                        word = new Word();
                    }
                    if( tag.Words.Count == 0 )
                    {
                        if( tag.Parent != m_root && tag.Parent.Children.Count > 1 )
                        {
                            Tag previous = tag.Previous;
							if( previous == null || previous.Type != TagType.LineBreak )
								tag.Words.Add( new Word( " " ) );
                        }
                    }
                    m_reader.Read();
                    continue;
                }
                word.Text += (char) m_reader.Read();
            }
            word.Trim();
            if( !String.IsNullOrEmpty( word.Text ) )
                tag.Words.Add( word );

            if( String.IsNullOrEmpty( tag.Text ) )
            {
                m_current.Children.Remove( tag );
                return;
            }

            if( IsParentOrLastTagAList( false ) )
            {
                m_current.Children.Remove( tag );
                return;
            }

            foreach( Word w in tag.Words )
                m_plainText += w.Text;
        }

        bool IsParentOrLastTagAList( bool alsoListItem )
        {
            if( m_current != null && m_current.Type == TagType.List )
                return true;
            if( alsoListItem && m_current.Type == TagType.ListItem )
                return true;

            if( m_current.Children.Count > 1 )
            {
                Tag tag = m_current.Previous;
                if( tag != null && tag.Type == TagType.List && tag.IsEndTag )
                    return true;
            }
            return false;
        }

        bool IsParentTagType( params TagType[] types )
        {
            foreach( TagType type in types )
            {
                if( m_current.Type == type )
                    return true;
            }
            return false;
        }

        string GetQuotedString()
        {
            string text = String.Empty;

            while( true )
            {
                int c = m_reader.Peek();
                // Check for end-of-data
                if( c == '\0' || c == -1 )
                    break;

                if( Char.IsWhiteSpace( (char) c ) || c == TagEnd )
                    break;

                if( c == TagBreak )
                {
                    c = m_reader.Read();
                    int c2 = m_reader.Peek();
                    if( c2 == TagEnd )
                        break;
                    text += (char) c;
                }

                if( c == '\'' || c == '"' )
                {
                    m_reader.Read();
                    Stack<int> stack = new Stack<int>();
                    stack.Push( c );
                    text += (char) c;

                    while( stack.Count > 0 )
                    {
                        int cq = m_reader.Read();
                        if( cq == '\0' || cq == -1 )
                            break;

                        if( cq == '\'' || cq == '"' )
                        {
                            if( cq == stack.Peek() )
                                stack.Pop();
                            else
                                stack.Push( cq );
                        }
                        text += (char) cq;
                    }
                    break;
                }
                text += (char) m_reader.Read();
            }

            return text.Trim( '\'', '"' );
        }

        protected abstract void Debug( string fmt, params object[] args );
    }
}
