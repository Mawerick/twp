﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2011 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct( "Twp.Controls.RichText" )]
[assembly: AssemblyVersion( "1.1.0.*" )]

[assembly: AssemblyTitle( "Twp.Controls.RichText" )]
[assembly: AssemblyDescription( "RichText Controls library." )]
[assembly: AssemblyCompany( "WrongPlace.Net" )]
[assembly: AssemblyCopyright( "� Mawerick, WrongPlace.Net 2011 - 2014" )]

[assembly: ComVisible( false )]

[assembly: Guid( "9cc5d2b7-125c-4ebc-b436-cb1935ea299f" )]
