﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls.RichText
{
    public class Renderer
    {
#region Constructor

        public Renderer( Graphics graphics, Rectangle bounds, Font font, Color color )
        {
            m_graphics = graphics;
            m_bounds = bounds;
            m_location = bounds.Location;

            m_fonts.Push( font );
            m_colors.Push( color );
        }

#endregion

#region Private fields

        int m_listIndent = 15;
        bool m_renderOverflow;

        readonly Graphics m_graphics;
        Rectangle m_bounds;
        Point m_location;

        readonly Stack<Font> m_fonts = new Stack<Font>();
        readonly Stack<Color> m_colors = new Stack<Color>();
        TextFormatFlags m_tff = TextFormatFlags.Default | TextFormatFlags.NoPadding;

        int m_indent;
        bool m_singleIndent;

        int LineHeight
        {
            get { return (int) m_graphics.MeasureString( "|", m_fonts.Peek() ).Height; }
        }

#endregion

#region Properties

        public int ListIndent
        {
            get { return m_listIndent; }
            set { m_listIndent = value; }
        }

        public bool RenderOverflow
        {
            get { return m_renderOverflow; }
            set { m_renderOverflow = value; }
        }

#endregion

#region Tag processing

        void BeginProcessTag( Tag tag )
        {
            if( tag == null || tag.IsEndTag )
                return;

            //Log.Debug( "[Renderer.BeginProcessTag] Tag: {0}", tag.Name );

            switch( tag.Type )
            {
                case TagType.List:
                    m_indent += (int) Math.Ceiling( m_listIndent / 2f );
                    m_location.X = m_bounds.Left + m_indent;
                    Tag previous = tag.Previous;
                    if( previous != null && previous.Type == TagType.LineBreak )
                        m_location.Y += LineHeight / 2;
                    else
                        m_location.Y += (int) ( (float) LineHeight * 1.5 );
                    break;

                case TagType.ListItem:
                    m_indent += (int) Math.Floor( m_listIndent / 2f );
                    m_location.X = m_bounds.Left + m_indent;
                    break;

                case TagType.Link:
                    if( tag.Attributes.ContainsKey( "style" ) )
                    {
                        Dictionary<string,string> styles = (Dictionary<string,string>) tag.Attributes["style"];
                        if( styles.ContainsKey( "text-decoration" ) )
                        {
                            switch( styles["text-decoration"] )
                            {
                                case "none":
                                    break;

                                case "underline":
                                    BeginProcessFont( "style", FontStyle.Underline );
                                    break;
                            }
                        }
                        else
                            BeginProcessFont( "style", FontStyle.Underline );
                    }
                    else
                        BeginProcessFont( "style", FontStyle.Underline );
                    BeginProcessFont( "color", Color.Blue );
                    break;

                case TagType.LineBreak:
                    m_location.X = m_bounds.Left + m_indent;
                    m_location.Y += LineHeight;
                    break;

                case TagType.Indent:
                    if( !m_singleIndent )
                    {
                        m_singleIndent = true;
                        m_indent += m_listIndent;
                        m_location.X = m_bounds.Left + m_indent;
                    }
                    break;

                case TagType.Italic:
                    BeginProcessFont( "style", FontStyle.Italic );
                    break;

                case TagType.Bold:
                    BeginProcessFont( "style", FontStyle.Bold );
                    break;

                case TagType.Underlined:
                    BeginProcessFont( "style", FontStyle.Underline );
                    break;

                case TagType.Strikeout:
                    BeginProcessFont( "style", FontStyle.Strikeout );
                    break;

                case TagType.Font:
                    foreach( KeyValuePair<string, object> kvp in tag.Attributes )
                        BeginProcessFont( kvp.Key, kvp.Value );
                    break;
            }
        }

        void EndProcessTag( Tag tag )
        {
            if( tag == null || tag.IsEndTag )
                return;

            //Log.Debug( "[Renderer.EndProcessTag] Tag: {0}", tag.Name );

            switch( tag.Type )
            {
                case TagType.List:
                    m_indent -= (int) Math.Ceiling( m_listIndent / 2f );
                    m_location.X = m_bounds.Left + m_indent;
                    m_location.Y += LineHeight / 2;
                    break;

                case TagType.ListItem:
                    m_indent -= (int) Math.Floor( m_listIndent / 2f );
                    m_location.X = m_bounds.Left + m_indent;
                    m_location.Y += LineHeight;
                    break;

                case TagType.HLine:
                    m_location.Y += LineHeight;
                    break;

                case TagType.Link:
                    EndProcessFont( "color" );
                    if( tag.Attributes.ContainsKey( "style" ) )
                    {
                        Dictionary<string,string> styles = (Dictionary<string,string>) tag.Attributes["style"];
                        if( styles.ContainsKey( "text-decoration" ) )
                        {
                            switch( styles["text-decoration"] )
                            {
                                case "none":
                                    break;

                                case "underline":
                                    EndProcessFont( "style" );
                                    break;
                            }
                        }
                        else
                            EndProcessFont( "style" );
                    }
                    else
                        EndProcessFont( "style" );
                    break;

                case TagType.Italic:
                case TagType.Bold:
                case TagType.Underlined:
                case TagType.Strikeout:
                    EndProcessFont( "style" );
                    break;

                case TagType.Font:
                    foreach( string key in tag.Attributes.Keys )
                        EndProcessFont( key );
                    break;

                case TagType.Box:
                    m_location.X = m_bounds.Left + m_indent;
                    m_location.Y += LineHeight * 2;
                    break;
            }
        }

        void BeginProcessFont( string key, object value )
        {
            //Log.Debug( "[Renderer.BeginProcessFont] Key: {0} Value: {1}", key, value );
            switch( key )
            {
                case "color":
                    m_colors.Push( (Color) value );
                    break;

                case "style":
                    m_fonts.Push( new Font( m_fonts.Peek(), m_fonts.Peek().Style | (FontStyle) value ) );
                    break;
            }
        }

        void EndProcessFont( string key )
        {
            //Log.Debug( "[Renderer.EndProcessFont] Key: {0}", key );
            switch( key )
            {
                case "color":
                    m_colors.Pop();
                    break;

                case "style":
                    m_fonts.Pop();
                    break;
            }
        }

#endregion

#region Measuring

        public void MeasureTag( Tag tag )
        {
            //Log.Debug( "[Renderer.MeasureTag] Segment: {0}", segment );
            if( tag == null )
                return;

            tag.Location = m_location;
            tag.Region.MakeEmpty();

            // Update drawing parameters
            //Log.Debug( "[Renderer.MeasureTag] Trying BeginProcessTag" );
            BeginProcessTag( tag );

            // Measure text content, if available
            if( tag.Type == TagType.Text )
            {
                tag.Location = m_location;
                foreach( Word word in tag.Words )
                {
                    Size size = TextRenderer.MeasureText( m_graphics, word.Text, m_fonts.Peek(), m_bounds.Size, m_tff );
                    if( size.Width + m_location.X > m_bounds.Right )
                    {
                        if( m_singleIndent )
                        {
                            m_singleIndent = false;
                            m_indent -= m_listIndent;
                        }
                        m_location.X = m_bounds.Left + m_indent;
                        m_location.Y += size.Height;
                        // Skip empty words on newline...
                        if( word.Text.Trim().Length == 0 )
                            size.Width = 0;
                    }
                    word.Bounds = new Rectangle( m_location, size );
                    tag.Region.Union( word.Bounds );
                    m_location.X += size.Width;
                }
            }

            // Process children
            if( tag.HasChildren )
            {
                //Log.Debug( "[Renderer.MeasureTag] Processing Children" );
                foreach( Tag child in tag.Children )
                {
                    MeasureTag( child );
                    tag.Region.Union( child.Region );
                }
            }

            // Update drawing parameters
            //Log.Debug( "[Renderer.MeasureTag] Trying EndProcessTag" );
            EndProcessTag( tag );
        }

#endregion

#region Drawing

        public void DrawTag( Tag tag )
        {
            //Log.Debug( "[Renderer.DrawTag] Tag: {0}", tag );
            if( tag == null )
                return;

            // Update drawing parameters
            BeginProcessTag( tag );

            // Draw content
            int y = tag.Location.Y + ( LineHeight / 2 );
            switch( tag.Type )
            {
                case TagType.HLine:
                    m_graphics.DrawLine( SystemPens.ButtonShadow, m_bounds.Left + 3, y - 1, m_bounds.Right - 3, y - 1 );
                    m_graphics.DrawLine( SystemPens.ButtonHighlight, m_bounds.Left + 3, y, m_bounds.Right - 3, y );
                    break;

                case TagType.ListItem:
                    using( Brush brush = new SolidBrush( m_colors.Peek() ) )
                    {
                        Point[] points = {
                            new Point( tag.Location.X - 2, y ),
                            new Point( tag.Location.X, y - 2 ),
                            new Point( tag.Location.X + 2, y ),
                            new Point( tag.Location.X, y + 2 ),
                        };
                        //Log.Debug( "[Renderer.DrawTagGraphics] Points: {0} {1} {2} {3}", points[0], points[1], points[2], points[3] );
                        m_graphics.FillPolygon( brush, points );
                    }
                    break;

                case TagType.Image:
                    m_graphics.DrawRectangle( Pens.Red, Rectangle.Round( tag.Region.GetBounds( m_graphics ) ) );
                    break;
                    
                case TagType.Text:
                    foreach( Word word in tag.Words )
                    {
                        if( word.Bounds.Bottom <= m_bounds.Bottom || m_renderOverflow )
                        {
                            if( word.Bounds.Width > 0 )
                            {
                                TextRenderer.DrawText( m_graphics, word.Text, m_fonts.Peek(), word.Bounds.Location, m_colors.Peek(), m_tff );
                            }
                        }
                    }
                    break;
            }

            // Process children
            if( tag.HasChildren )
            {
                //Log.Debug( "[Renderer.DrawTag] Processing Children" );
                foreach( Tag child in tag.Children )
                {
                    DrawTag( child );
                }
            }

            // Update drawing parameters
            //Log.Debug( "[Renderer.DrawTag] Trying EndProcessTag" );
            EndProcessTag( tag );
        }

#endregion
    }
}
