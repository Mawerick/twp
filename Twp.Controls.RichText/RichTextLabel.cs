﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Controls.RichText
{
    /// <summary>
    /// Description of RichTextLabel.
    /// </summary>
    [DefaultProperty( "Text" )]
    [DefaultEvent( "LinkClicked" )]
    public class RichTextLabel : Control
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RichTextLabel"/> class.
        /// </summary>
        public RichTextLabel()
        {
            m_contextMenu = new ContextMenu();
            m_contextMenu.MenuItems.Add( "Copy link location", OnCopyLinkLocation );

            SetStyle( ControlStyles.DoubleBuffer |
                      ControlStyles.AllPaintingInWmPaint |
                      ControlStyles.ResizeRedraw |
                      ControlStyles.UserPaint, true );

            Name = "RichTextLabel";
            base.Padding = new Padding( 3 );
            m_document.Parser = new AomlParser();
            ResizeRedraw = true;
        }

        string m_text = String.Empty;
        RichTextMode m_mode = RichTextMode.Html;
        Document m_document = new Document();
        ContextMenu m_contextMenu;
        string m_hoverLink = null;

        /// <summary>
        /// Gets or sets the html content of the label.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The rich text content of the label." )]
        [DefaultValue( "" )]
        [Editor( typeof( MultilineStringEditor ), typeof( UITypeEditor ) )]
        public override string Text
        {
            get { return m_text; }
            set
            {
                if( m_text != value )
                {
                    m_text = value;
                    m_document.Clear();
                    m_document.Append( m_text );
                    Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value representing the text parsing mode.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "The parsing mode to use." )]
        [DefaultValue( RichTextMode.Html )]
        public RichTextMode Mode
        {
            get { return m_mode; }
            set
            {
                if( m_mode != value )
                {
                    m_mode = value;
                    switch( m_mode )
                    {
                        case RichTextMode.Html:
                            m_document.Parser = new AomlParser();
                            break;
    
                        case RichTextMode.BBCode:
                            m_document.Parser = new BBCodeParser();
                            break;
    
                        default:
                            m_document.Parser = new DefaultParser();
                            break;
                    }
                    Invalidate();
                }
            }
        }

        [DefaultValue( typeof( Padding ), "3, 3, 3, 3" )]
        public new Padding Padding
        {
            get { return base.Padding; }
            set
            {
                if( base.Padding != value )
                {
                    base.Padding = value;
                    Invalidate();
                }
            }
        }

        public override Rectangle DisplayRectangle
        {
            get
            {
                Rectangle rect = base.DisplayRectangle;
                rect.Inflate( -Padding.Horizontal, -Padding.Vertical );
                rect.Offset( -Padding.Left, -Padding.Top );
                return rect;
            }
        }

        [Browsable( false )]
        public Document Document
        {
            get { return m_document; }
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
        /// </summary>
        /// <param name="e">A <see cref="System.Windows.Forms.PaintEventArgs"/> that contains the event data.</param>
        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );

            //if( DesignMode )
            //    return;

            try
            {
                e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                m_document.Renderer = new Renderer( e.Graphics, DisplayRectangle, Font, ForeColor );
                m_document.Renderer.RenderOverflow = true;
                m_document.PerformLayout();
                m_document.Render();
            }
            catch( Exception ex )
            {
                Log.Debug( "[RichTextLabel.OnPaint] {0}", ex );
            }
        }

#region Link handling

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseMove"/> event.
        /// </summary>
        /// <param name="e">A <see cref="System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            bool onLink = false;
            foreach( Tag tag in m_document.Links )
            {
                if( tag.Region.IsVisible( e.Location ) )
                {
                    onLink = true;
                }
            }
            Cursor = onLink == true ? Cursors.Hand : DefaultCursor;
        }

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.MouseClick"/> event.
        /// </summary>
        /// <param name="e">A <see cref="System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseClick( MouseEventArgs e )
        {
            base.OnMouseClick( e );
            foreach( Tag tag in m_document.Links )
            {
                if( tag.Region.IsVisible( e.Location ) )
                {
                    if( e.Button == MouseButtons.Left )
                    {
                        OnLinkClicked( new LinkClickedEventArgs( tag.Attributes["href"].ToString() ) );
                    }
                    else if( e.Button == MouseButtons.Right )
                    {
                        m_hoverLink = tag.Attributes["href"].ToString();
                        m_contextMenu.Show( this, e.Location );
                    }
                    return;
                }
            }
        }

        /// <summary>
        /// Occurs when a link is clicked within the control.
        /// </summary>
        [Category( "Action" )]
        [Description( "Occurs when a link is clicked within the control." )]
        public event LinkClickedEventHandler LinkClicked;

        /// <summary>
        /// Raises the <see cref="LinkClicked"/> event.
        /// </summary>
        /// <param name="e">A <see cref="LinkClickedEventArgs"/> that contains the event data.</param>
        protected virtual void OnLinkClicked( LinkClickedEventArgs e )
        {
            if( LinkClicked != null )
                LinkClicked( this, e );
        }

        internal void OnCopyLinkLocation( object sender, EventArgs e )
        {
            Log.Debug( "[RichTextLabel.OnCopyLinkLocation] Link: {0}", m_hoverLink );
            Clipboard.SetText( m_hoverLink );
        }

#endregion
    }
}
