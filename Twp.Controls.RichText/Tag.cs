﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Twp.Controls.RichText
{
    public class Tag
    {
#region Constructors

        public Tag()
        {
            m_children = new TagCollection( this );
        }

        public Tag( TagType type )
        {
            m_type = type;
            m_children = new TagCollection( this );
        }

#endregion

#region Private fields

        TagType m_type = TagType.None;
        Dictionary<string, object> m_attributes = new Dictionary<string, object>();
        List<Word> m_words = new List<Word>();
        Region m_region = new Region();
        Point m_location = Point.Empty;
        bool m_isEndTag;
        Tag m_parent;
        TagCollection m_owner;
        TagCollection m_children;

#endregion

#region Properties

        public TagType Type
        {
            get { return m_type; }
            set { m_type = value; }
        }

        public Dictionary<string, object> Attributes
        {
            get { return m_attributes; }
            set { m_attributes = value; }
        }

        public Region Region
        {
            get { return m_region; }
            set { m_region = value; }
        }

        public Point Location
        {
            get { return m_location; }
            set { m_location = value; }
        }

        public bool IsEndTag
        {
            get { return m_isEndTag; }
            set { m_isEndTag = value; }
        }

        public Tag Parent
        {
            get { return m_parent; }
            set { m_parent = value; }
        }

        public TagCollection Owner
        {
            get { return m_owner; }
            set { m_owner = value; }
        }

        public TagCollection Children
        {
            get { return m_children; }
            set { m_children = value; }
        }

        public bool HasAttributes
        {
            get { return m_attributes.Count > 0; }
        }

        public bool HasChildren
        {
            get { return m_children.Count > 0; }
        }

        public Tag Previous
        {
            get
            {
                int index = m_owner.IndexOf( this ) - 1;
                return ( index < 0 ? null : m_owner[index] );
            }
        }

        public Tag Next
        {
            get
            {
                int index = m_owner.IndexOf( this ) + 1;
                return ( index < 0 || index > m_owner.Count - 1 ) ? null : m_owner[index];
            }
        }

        public List<Word> Words
        {
            get { return m_words; }
        }

        public string Text
        {
            get
            {
                string text = String.Empty;
                foreach( Word word in m_words )
                {
                    text += word.Text;
                }
                return text.Trim();
            }
        }

        public string Hex
        {
            get
            {
                string hex = "{ ";
                foreach( Word word in m_words )
                {
                    hex += word.Hex + "; ";
                }
                return hex.TrimEnd( ';', ' ' ) + " }";
            }
        }

#endregion

#region Methods

        public override string ToString()
        {
            string extra = String.Empty;
            if( HasAttributes )
            {
                extra = ", Attributes=[";
                string attr = String.Empty;
                foreach( KeyValuePair<string, object> kvp in m_attributes )
                {
                    attr += String.Format( " {0}={1}", kvp.Key, kvp.Value );
                }
                extra += attr.Trim() + "]";
            }
            if( m_type == TagType.Text )
                extra += ", Text=\"" + Text + "\"";
            if( IsEndTag )
                extra += ", IsEndTag";
            return string.Format( "[Tag Type={0}{1}]", m_type, extra.Trim() );
        }

#endregion
    }
}
