﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Controls.RichText
{
    /// <summary>
    /// Description of TagCollection.
    /// </summary>
    public class TagCollection : List<Tag>
    {
        public TagCollection( Tag parent )
        {
            m_parent = parent;
        }

        readonly Tag m_parent;

        public Tag Parent
        {
            get { return m_parent; }
        }

        public new void Add( Tag item )
        {
            if( item.Parent != null )
                item.Parent.Children.Remove( item );
            item.Parent = m_parent;
            item.Owner = this;
            base.Add( item );
        }
    }
}
