//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Twp.Controls.RichText
{
    public class TextTag : Tag
    {
#region Private fields

        List<Word> m_words = new List<Word>();

#endregion

#region Properties

        public List<Word> Words
        {
            get { return m_words; }
        }

        public string Text
        {
            get
            {
                string text = String.Empty;
                foreach( Word word in m_words )
                {
                    text += word.Text;
                }
                return text.Trim();
            }
        }

        public string Hex
        {
            get
            {
                string hex = "{ ";
                foreach( Word word in m_words )
                {
                    hex += word.Hex + "; ";
                }
                return hex.TrimEnd( ';', ' ' ) + " }";
            }
        }

#endregion

#region Methods

        public override string ToString()
        {
            return string.Format( "[TextSegment Text=\"{0}\" {1}:{2}]", Text, m_words.Count, Hex );
        }

#endregion
    }
}
