﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;

namespace Twp.Controls.RichText
{
    /// <summary>
    /// Description of ValidTag.
    /// </summary>
    public class ValidTag
    {
#region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidTag"/> class with the specified <paramref name="type"/> and <paramref name="name"/>.
        /// </summary>
        /// <param name="type">A <see cref="TagType"/> representing the tag type.</param>
        /// <param name="name">The name of the tag.</param>
        public ValidTag( TagType type, string name )
            : this( type, name, TagType.None, null )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidTag"/> class with the specified <paramref name="type"/>, <paramref name="name"/>
        /// and <paramref name="parent"/>.
        /// </summary>
        /// <param name="type">A <see cref="TagType"/> representing the tag type.</param>
        /// <param name="name">The name of the tag.</param>
        /// <param name="parent">A <see cref="TagType"/> representing the parent tag's type.</param>
        public ValidTag( TagType type, string name, TagType parent )
            : this( type, name, parent, null )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidTag"/> class with the specified <paramref name="type"/>, <paramref name="name"/>
        /// and <paramref name="attributes"/>.
        /// </summary>
        /// <param name="type">A <see cref="TagType"/> representing the tag type.</param>
        /// <param name="name">The name of the tag.</param>
        /// <param name="attributes">A string array containing valid attributes.</param>
        public ValidTag( TagType type, string name, string[] attributes )
            : this( type, name, TagType.None, attributes )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidTag"/> class with the specified <paramref name="type"/>, <paramref name="name"/>,
        /// <paramref name="parent"/> and <paramref name="attributes"/>.
        /// </summary>
        /// <param name="type">A <see cref="TagType"/> representing the tag type.</param>
        /// <param name="name">The name of the tag.</param>
        /// <param name="parent">A <see cref="TagType"/> representing the parent tag's type.</param>
        /// <param name="attributes">A string array containing valid attributes.</param>
        public ValidTag( TagType type, string name, TagType parent, string[] attributes )
        {
            m_type = type;
            m_name = name;
            m_parent = parent;
            m_attributes = attributes;
        }

#endregion

#region Private fields

        TagType m_type;
        string m_name;
        TagType m_parent;
        string[] m_attributes;

#endregion

#region Properties

        /// <summary>
        /// Gets the type of the tag.
        /// </summary>
        public TagType Type
        {
            get { return m_type; }
        }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        public string Name
        {
            get { return m_name; }
        }

        /// <summary>
        /// Gets the type of the parent tag.
        /// </summary>
        public TagType Parent
        {
            get { return m_parent; }
        }

        /// <summary>
        /// Gets the array of valid attributes.
        /// </summary>
        public string[] Attributes
        {
            get { return m_attributes; }
        }

#endregion

#region Methods

        public override string ToString()
        {
            return string.Format( "[ValidTag Type={0}, Name={1}, Parent={2}, Attributes={3}]", this.m_type, this.m_name, this.m_parent, this.m_attributes );
        }

#endregion
    }
}
