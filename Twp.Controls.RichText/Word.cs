﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;

namespace Twp.Controls.RichText
{
    /// <summary>
    /// Description of Word.
    /// </summary>
    public class Word
    {
#region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Word"/> class.
        /// </summary>
        public Word()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Word"/> class with the given text.
        /// </summary>
        /// <param name="text">The text content of the word.</param>
        public Word( string text )
        {
            m_text = text;
        }

#endregion

#region Private fields

        string m_text = String.Empty;
        Rectangle m_bounds = Rectangle.Empty;

#endregion

#region Properties

        public string Text
        {
            get { return m_text; }
            set { m_text = value; }
        }

        public Rectangle Bounds
        {
            get { return m_bounds; }
            set { m_bounds = value; }
        }

        public string Hex
        {
            get
            {
                string hex = String.Empty;
                foreach( Char c in m_text )
                {
                    hex += String.Format( " 0x{0:X2}", (int) c );
                }
                return hex.Trim();
            }
        }

#endregion

#region Methods

        public void Trim()
        {
            if( String.IsNullOrEmpty( m_text ) )
                return;

            string text = m_text.Trim();
            m_text = text.Trim( '\r', '\n' );
        }

#endregion
    }
}
