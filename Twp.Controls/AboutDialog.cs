﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    /// <summary>
    /// Represents a Dialog that shows information about a program.
    /// </summary>
    [DefaultProperty( "VersionFormat" )]
    public partial class AboutDialog : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AboutDialog"/> class.
        /// </summary>
        public AboutDialog()
        {
            this.InitializeComponent();
        }

        private VersionFormat versionFormat = VersionFormat.Full;
        private Assembly assembly = null;

        /// <summary>
        /// Gets or sets a value indicating the website url for the program.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The website URL for the program." )]
        public string Url
        {
            get { return this.urlBox.Text; }
            set { this.urlBox.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating the contact e-mail for the program.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The main contact e-mail for the program." )]
        public string Email
        {
            get { return this.emailBox.Text; }
            set { this.emailBox.Text = value; }
        }

        /// <summary>
        /// Gets or sets an image representing the program icon.
        /// This should be a 64x64 PNG file.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The program's icon. This should be a 64x64 PNG file." )]
        public System.Drawing.Image Image
        {
            get { return this.iconBox.Image; }
            set { this.iconBox.Image = value; }
        }

        /// <summary>
        /// Gets or sets an image representing the license for the program.
        /// This should be a 88x33 PNG file.
        /// </summary>
        [Category( "Apperance" )]
        [Description( "An image representing the license for the program. This should be a 88x33 PNG file." )]
        public System.Drawing.Image LicenseImage
        {
            get { return this.licenseIconBox.Image; }
            set { this.licenseIconBox.Image = value; }
        }

        /// <summary>
        /// Gets or sets a value representing the format of the version string.
        /// The default is <see cref="VersionFormat.Full"/>.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "A value representing the format of the version string." )]
        [DefaultValue( VersionFormat.Full )]
        public VersionFormat VersionFormat
        {
            get { return this.versionFormat; }
            set
            {
                this.versionFormat = value;
                this.UpdateVersion();
            }
        }

        /// <summary>
        /// Raises the <see cref="Form.Load"/> event, and fills in product info based on the entry assembly.
        /// </summary>
        /// <param name="e">a <see cref="EventArgs"/> containing the event data.</param>
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );

            if( this.DesignMode )
                return;

            this.assembly = Assembly.GetEntryAssembly();
            this.Text = "About " + ( this.assembly.GetCustomAttributes( typeof( AssemblyTitleAttribute ), false )[0] as AssemblyTitleAttribute ).Title;
            this.productLabel.Text = ( this.assembly.GetCustomAttributes( typeof( AssemblyTitleAttribute ), false )[0] as AssemblyTitleAttribute ).Title;

            this.UpdateVersion();

            this.descriptionLabel.Text = ( this.assembly.GetCustomAttributes( typeof( AssemblyDescriptionAttribute ), false )[0] as AssemblyDescriptionAttribute ).Description;
            this.copyrightLabel.Text = ( this.assembly.GetCustomAttributes( typeof( AssemblyCopyrightAttribute ), false )[0] as AssemblyCopyrightAttribute ).Copyright;
        }

        private void UpdateVersion()
        {
            if( this.DesignMode )
                return;

            if( this.assembly == null )
                return;

            this.versionLabel.Text = "v" + VersionString.Format( this.assembly, this.versionFormat );
        }

        private void OnLinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            Process.Start( e.Link.LinkData as string );
        }
    }
}
