﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace Twp.Controls
{
	partial class AboutDialog
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.iconBox = new System.Windows.Forms.PictureBox();
			this.productLabel = new System.Windows.Forms.Label();
			this.versionLabel = new System.Windows.Forms.Label();
			this.descriptionLabel = new System.Windows.Forms.Label();
			this.copyrightLabel = new System.Windows.Forms.Label();
			this.licenseIconBox = new System.Windows.Forms.PictureBox();
			this.urlBox = new System.Windows.Forms.LinkLabel();
			this.emailBox = new System.Windows.Forms.LinkLabel();
			this.okButton = new System.Windows.Forms.Button();
			this.urlLabel = new System.Windows.Forms.Label();
			this.emailLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.iconBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.licenseIconBox)).BeginInit();
			this.SuspendLayout();
			// 
			// iconBox
			// 
			this.iconBox.Location = new System.Drawing.Point(8, 8);
			this.iconBox.Name = "iconBox";
			this.iconBox.Size = new System.Drawing.Size(88, 64);
			this.iconBox.TabIndex = 0;
			this.iconBox.TabStop = false;
			// 
			// productLabel
			// 
			this.productLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.productLabel.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
			this.productLabel.Location = new System.Drawing.Point(102, 8);
			this.productLabel.Name = "productLabel";
			this.productLabel.Size = new System.Drawing.Size(254, 24);
			this.productLabel.TabIndex = 1;
			this.productLabel.Text = "[ProductName]";
			// 
			// versionLabel
			// 
			this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.versionLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
			this.versionLabel.Location = new System.Drawing.Point(102, 32);
			this.versionLabel.Name = "versionLabel";
			this.versionLabel.Size = new System.Drawing.Size(254, 16);
			this.versionLabel.TabIndex = 2;
			this.versionLabel.Text = "v0.0.0.0";
			// 
			// descriptionLabel
			// 
			this.descriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.descriptionLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.descriptionLabel.Location = new System.Drawing.Point(102, 56);
			this.descriptionLabel.Name = "descriptionLabel";
			this.descriptionLabel.Size = new System.Drawing.Size(254, 26);
			this.descriptionLabel.TabIndex = 3;
			this.descriptionLabel.Text = "[Description]";
			// 
			// copyrightLabel
			// 
			this.copyrightLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.copyrightLabel.Location = new System.Drawing.Point(102, 82);
			this.copyrightLabel.Name = "copyrightLabel";
			this.copyrightLabel.Size = new System.Drawing.Size(254, 16);
			this.copyrightLabel.TabIndex = 4;
			this.copyrightLabel.Text = "[Copyright]";
			// 
			// licenseIconBox
			// 
			this.licenseIconBox.Location = new System.Drawing.Point(8, 78);
			this.licenseIconBox.Name = "licenseIconBox";
			this.licenseIconBox.Size = new System.Drawing.Size(88, 64);
			this.licenseIconBox.TabIndex = 6;
			this.licenseIconBox.TabStop = false;
			// 
			// urlBox
			// 
			this.urlBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.urlBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.urlBox.Location = new System.Drawing.Point(173, 108);
			this.urlBox.Name = "urlBox";
			this.urlBox.Size = new System.Drawing.Size(183, 16);
			this.urlBox.TabIndex = 6;
			this.urlBox.TabStop = true;
			this.urlBox.Text = "[url]";
			this.urlBox.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnLinkClicked);
			// 
			// emailBox
			// 
			this.emailBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.emailBox.Location = new System.Drawing.Point(173, 126);
			this.emailBox.Name = "emailBox";
			this.emailBox.Size = new System.Drawing.Size(183, 16);
			this.emailBox.TabIndex = 8;
			this.emailBox.TabStop = true;
			this.emailBox.Text = "[email]";
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(135, 148);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(75, 23);
			this.okButton.TabIndex = 0;
			this.okButton.Text = "&Ok";
			this.okButton.UseVisualStyleBackColor = true;
			// 
			// urlLabel
			// 
			this.urlLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.urlLabel.Location = new System.Drawing.Point(103, 108);
			this.urlLabel.Name = "urlLabel";
			this.urlLabel.Size = new System.Drawing.Size(64, 16);
			this.urlLabel.TabIndex = 5;
			this.urlLabel.Text = "Website:";
			// 
			// emailLabel
			// 
			this.emailLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.emailLabel.Location = new System.Drawing.Point(103, 126);
			this.emailLabel.Name = "emailLabel";
			this.emailLabel.Size = new System.Drawing.Size(64, 16);
			this.emailLabel.TabIndex = 7;
			this.emailLabel.Text = "E-mail:";
			// 
			// AboutDialog
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(364, 180);
			this.Controls.Add(this.emailLabel);
			this.Controls.Add(this.urlLabel);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.emailBox);
			this.Controls.Add(this.urlBox);
			this.Controls.Add(this.licenseIconBox);
			this.Controls.Add(this.copyrightLabel);
			this.Controls.Add(this.descriptionLabel);
			this.Controls.Add(this.versionLabel);
			this.Controls.Add(this.productLabel);
			this.Controls.Add(this.iconBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutDialog";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "About ";
			((System.ComponentModel.ISupportInitialize)(this.iconBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.licenseIconBox)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label productLabel;
		private System.Windows.Forms.Label versionLabel;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Label emailLabel;
		private System.Windows.Forms.Label urlLabel;
		private System.Windows.Forms.LinkLabel emailBox;
		private System.Windows.Forms.Label descriptionLabel;
		private System.Windows.Forms.Label copyrightLabel;
		private System.Windows.Forms.PictureBox licenseIconBox;
		private System.Windows.Forms.LinkLabel urlBox;
		private System.Windows.Forms.PictureBox iconBox;
	}
}
