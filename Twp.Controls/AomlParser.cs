﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2012
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using Twp.Utilities;

namespace Twp.Controls
{
	public class AomlParser
	{
		private string aoml;
		public string Aoml
		{
			get { return this.aoml; }
		}

		private Font font;
		public Font Font
		{
			get { return this.font; }
		}

		public AomlParser( string aoml, Font font )
		{
			this.aoml = aoml;
			this.font = font;
		}

		public static AomlString[] Parse( string aoml, Font font )
		{
			AomlParser p = new AomlParser( aoml, font );
			AomlString[] a = p.Parse();
			return a;
		}

		public AomlString[] Parse()
		{
			List<AomlString> words = new List<AomlString>();
			List<AomlString> tags = new List<AomlString>();
			AomlString word = new AomlString();

			Stopwatch sw = new Stopwatch();
			sw.Start();
			CharEnumerator ie = this.aoml.GetEnumerator();
			while( ie.MoveNext() )
			{
				if( Char.IsWhiteSpace( ie.Current ) )
				{
					// We got whitespace.
					// Add the word to the list (unless we already did).
					// this trims away excess whitespace.
					if( !String.IsNullOrEmpty( word.Text ) )
					{
						word.Text += " ";
						words.Add( word );
						word = new AomlString();
					}
				}
				else if( ie.Current == '<' )
				{
					if( !String.IsNullOrEmpty( word.Text ) )
					{
						words.Add( word );
						word = new AomlString();
					}
					AomlString tag = this.ParseTag( ie );
					Log.Debug( "[AomlParser.Parse] Tag: {0}", tag );
					if( tag.Text[0] == '/' )
					{
						tags.RemoveAt( tags.Count - 1 );
						tag = tags[tags.Count - 1];
					}
					else
					{
						tags.Add( tag );
					}
					word.Color = tag.Color;
					word.FontFamily = tag.FontFamily;
					word.Size = tag.Size;
					word.Style = tag.Style;
				}
				else
				{
					// We got a normal letter.
					word.Text += ie.Current;
				}
			}
			sw.Stop();
			Log.Debug( "[AomlParser.Parse] Elapsed: {0}", sw.Elapsed );
			return words.ToArray();
		}

		private AomlString ParseTag( CharEnumerator ie )
		{
			AomlString tag = new AomlString();
			bool hasAttr = false;
			string attr = String.Empty;
			bool hasValue = false;
			string value = String.Empty;

			while( ie.MoveNext() )
			{
				if( Char.IsWhiteSpace( ie.Current ) )
				{
					// we may have attributes...
					hasAttr = true;
				}
				if( ie.Current == '>' )
				{
					break;
				}
				else if( ie.Current == '=' )
				{
					hasValue = true;
				}
				else
				{
					if( hasValue )
					{
						value += ie.Current;
					}
					else if( hasAttr )
					{
						attr += ie.Current;
					}
					else
					{
						tag.Text += ie.Current;
					}
				}
			}
			return tag;
		}
	}

	public class AomlString
	{
		private string text;
		public string Text
		{
			get { return this.text; }
			set { this.text = value; }
		}

		private FontStyle style = FontStyle.Regular;
		public FontStyle Style
		{
			get { return this.style; }
			set { this.style = value; }
		}

		private FontFamily fontFamily = null;
		public FontFamily FontFamily
		{
			get { return this.fontFamily; }
			set { this.fontFamily = value; }
		}

		private float size = 0;
		public float Size
		{
			get { return this.size; }
			set { this.size = value; }
		}

		private Color color = Color.Empty;
		public Color Color
		{
			get { return this.color; }
			set { this.color = value; }
		}

		public override string ToString()
		{
			string output = '"' + this.text + '"';
			if( this.style != FontStyle.Regular )
				output += " Style=" + this.style.ToString();
			if( this.fontFamily != null )
				output += " FontFamily=" + this.fontFamily.ToString();
			if( this.size != 0 )
				output += " Size=" + this.size.ToString();
			if( this.color != Color.Empty )
				output += " Color=" + this.color.ToString();
			return output;
		}
	}
}
