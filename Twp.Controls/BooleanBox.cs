﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Layout;
using System.Windows.Forms.VisualStyles;

namespace Twp.Controls
{
    public enum BooleanDisplayFormat
    {
        TrueFalse,
        YesNo,
        OnOff,
        EnableDisable,
        Custom,
    }

    /// <summary>
    /// Description of BooleanBox.
    /// </summary>
    [Description( "Provides a simple control for editing a boolean value, as an alternative to a checkbox." )]
    [Designer( typeof( Design.BooleanBoxDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
//    [ToolboxBitmap( typeof( Line ), "Resources.Line.bmp" )]
    [DefaultProperty( "DisplayFormat" )]
    [DefaultEvent( "ValueChanged" )]
    public class BooleanBox : Control, INotifyPropertyChanged
    {
        #region Constructor

        public BooleanBox()
        {
            this.SetStyle( 
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.SupportsTransparentBackColor,
                true );
            
            // Ensure proper rendering in default state...
            this.trueButton.Checked = true;
        }

        #endregion
        
        #region Properties
        
        [Category( "Appearance" )]
        [Description( "The value of the BooleanBox" )]
        [DefaultValue( true )]
        public bool Value
        {
            get { return this.value; }
            set
            {
                if( this.value != value )
                {
        		    this.value = value;
        		    this.trueButton.Checked = value;
        		    this.falseButton.Checked = !value;
        		    this.Invalidate();
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Value" ) );
                    this.OnValueChanged();
                }
            }
        }
        private bool value = true;

        [Category( "Appearance" )]
        [Description( "Defines what the true and false buttons should display" )]
        [DefaultValue( BooleanDisplayFormat.TrueFalse )]
        public BooleanDisplayFormat DisplayFormat
        {
            get { return this.displayFormat; }
            set
            {
                if( this.displayFormat != value )
                {
                    this.displayFormat = value;
                    switch( this.displayFormat )
                    {
                        case BooleanDisplayFormat.TrueFalse:
                            this.trueButton.Text = "True";
                            this.falseButton.Text = "False";
                            break;
                        case BooleanDisplayFormat.YesNo:
                            this.trueButton.Text = "Yes";
                            this.falseButton.Text = "No";
                            break;
                        case BooleanDisplayFormat.OnOff:
                            this.trueButton.Text = "On";
                            this.falseButton.Text = "Off";
                            break;
                        case BooleanDisplayFormat.EnableDisable:
                            this.trueButton.Text = "Enable";
                            this.falseButton.Text = "Disable";
                            break;
                        case BooleanDisplayFormat.Custom:
                            this.trueButton.Text = this.trueText;
                            this.falseButton.Text = this.falseText;
                            break;
                        default:
                            throw new Exception( "Invalid value for DisplayFormat" );
                    }
                    this.ResizeContent();
                    this.Invalidate();
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "DisplayFormat" ) );
                }
            }
        }
        private BooleanDisplayFormat displayFormat = BooleanDisplayFormat.TrueFalse;
        
        [Category( "Appearance" )]
        [Description( "Determins the text of the 'true' button. Only valid if DisplayFormat is set to Custom." )]
        public string TrueText
        {
            get { return this.trueText; }
            set
            {
                if( this.trueText != value )
                {
                    this.trueText = value;
                    if( this.displayFormat == BooleanDisplayFormat.Custom )
                    {
                        this.trueButton.Text = value;
                        this.ResizeContent();
                        this.Invalidate();
                    }
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "TrueText" ) );
                }
            }
        }
        private string trueText = String.Empty;
        
        [Category( "Appearance" )]
        [Description( "Determins the text of the 'false' button. Only valid if DisplayFormat is set to Custom." )]
        public string FalseText
        {
            get { return this.falseText; }
            set
            {
                if( this.falseText != value )
                {
                    this.falseText = value;
                    if( this.displayFormat == BooleanDisplayFormat.Custom )
                    {
                        this.falseButton.Text = value;
                        this.ResizeContent();
                        this.Invalidate();
                    }
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "FalseText" ) );
                }
            }
        }
        private string falseText = String.Empty;
        
        [Category( "Appearance" )]
        [Description( "Determins the location of the buttons, in relation to the box." )]
        [DefaultValue( HorizontalAlignment.Left )]
        public HorizontalAlignment ButtonAlign
        {
        	get { return this.buttonAlign; }
        	set
        	{
        		if( this.buttonAlign != value )
        		{
        			this.buttonAlign = value;
        			this.ResizeContent();
        			this.Invalidate();
        			this.OnPropertyChanged( new PropertyChangedEventArgs( "ButtonAlign" ) );
        		}
        	}
        }
        private HorizontalAlignment buttonAlign = HorizontalAlignment.Left;
        
        [Category( "Appearance" )]
        [Description( "Defines the width of the space between the buttons (including labels)." )]
        [DefaultValue( 4 )]
        public int Spacing
        {
            get { return this.spacing; }
            set
            {
                if( this.spacing != value )
                {
                    this.spacing = value;
                    this.ResizeContent();
                    this.Invalidate();
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Spacing" ) );
                }
            }
        }
        private int spacing = 4;
        
        protected override Padding DefaultPadding
        {
            get { return new Padding( 3 ); }
        }

        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
		public override bool AutoSize
		{
			get { return this.autoSize; }
			set
			{
			    if( this.autoSize != value )
			    {
    			    this.autoSize = value;
    			    if( this.autoSize == true )
    			        this.Size = FitSize;
    			    this.OnPropertyChanged( new PropertyChangedEventArgs( "AutoSize" ) );
			    }
			}
		}
        private bool autoSize = true;
        
        public new Size Size
        {
            get { return base.Size; }
            set
            {
                this.Width = value.Width;
                this.Height = FitSize.Height;
            }
        }
        
        private Size FitSize
        {
            get
            {
                int width = this.Padding.Horizontal + this.trueButton.Bounds.Width + this.falseButton.Bounds.Width + this.spacing;
                int height = this.Padding.Vertical + Math.Max( this.trueButton.Bounds.Height, this.falseButton.Bounds.Height ) - BooleanButton.TextPadding;
                return new Size( width, height );
            }
        }

        [Browsable( false )]
        public int TextBaseline
        {
            get { return this.trueButton.TextRect.Bottom - BooleanButton.TextPadding - 1; }
        }

        private bool leftButtonDown = false;

        #endregion
        
        #region Fields
        
        private BooleanButton trueButton = new BooleanButton( "True" );
        private BooleanButton falseButton = new BooleanButton( "False" );
        private Rectangle focusRect = Rectangle.Empty;

        #endregion

        #region Events
        
		public event PropertyChangedEventHandler PropertyChanged;
		public event EventHandler ValueChanged;
		
        #endregion
        
        #region Methods

        protected virtual void OnValueChanged()
		{
		    if( this.ValueChanged != null )
		        this.ValueChanged( this, EventArgs.Empty );
		}

		protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			if( this.PropertyChanged != null )
				this.PropertyChanged( this, e );
		}
		
        protected override void OnLostFocus( EventArgs e )
        {
            this.Invalidate();
            base.OnLostFocus( e );
        }

        protected override void OnGotFocus( EventArgs e )
        {
            this.Invalidate();
            base.OnGotFocus( e );
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            this.SetHot( false, false );
            this.Invalidate();
            base.OnMouseLeave( e );
        }

        protected override void OnClick( EventArgs e )
        {
            this.Focus();
            this.Invalidate();
            base.OnClick( e );
        }

        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            bool result = true;

            const int WM_KEYDOWN = 0x0100;
            const int WM_KEYUP = 0x0104;

            if( msg.Msg == WM_KEYDOWN || msg.Msg == WM_KEYUP )
            {
                switch( keyData )
                {
                    case Keys.Left:
                    case Keys.Up:
                        this.Value = true;
                        break;

                    case Keys.Right:
                    case Keys.Down:
                        this.Value = false;
                        break;
                        
                    case Keys.Space:
                        this.Value = !this.Value;
                        break;

                    default:
                        result = base.ProcessCmdKey( ref msg, keyData );
                        break;
                }
            }

            return result;
        }

        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );
            if( this.trueButton.Bounds.Contains( e.Location ) )
            {
                this.SetHot( true, false );
                if( !this.leftButtonDown )
                {
                    this.leftButtonDown = true;
                    this.Capture = true;
                }
            }
            else if( this.falseButton.Bounds.Contains( e.Location ) )
            {
                this.SetHot( false, true );
                if( !this.leftButtonDown )
                {
                    this.leftButtonDown = true;
                    this.Capture = true;
                }
            }
            else
                this.SetHot( false, false );

            this.Invalidate();
        }

        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );
            this.leftButtonDown = false;
            this.Capture = false;

            if( this.trueButton.Hot && this.trueButton.Bounds.Contains( e.Location ) )
                this.Value = true;
            else if( this.falseButton.Hot && this.falseButton.Bounds.Contains( e.Location ) )
                this.Value = false;
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            PointF currentPoint = new PointF( e.X, e.Y );

            if( this.trueButton.Bounds.Contains( e.Location ) )
                this.SetHot( true, false );
            else if( this.falseButton.Bounds.Contains( e.Location ) )
                this.SetHot( false, true );
            else
                this.SetHot( false, false );

            this.Invalidate();
        }
        
        private void SetHot( bool trueHot, bool falseHot )
        {
            this.trueButton.Hot = trueHot;
            this.falseButton.Hot = falseHot;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.ResizeContent();
            this.Invalidate();
        }
		
		private void ResizeContent()
		{
		    using( Graphics g = this.CreateGraphics() )
		    {
    		    TextFormatFlags tff = TextFormatFlags.Left | TextFormatFlags.VerticalCenter;

    		    Size trueSize = TextRenderer.MeasureText( g, this.trueButton.Text, this.Font, this.ClientSize, tff );
		        Size falseSize = TextRenderer.MeasureText( g, this.falseButton.Text, this.Font, this.ClientSize, tff );

		        int maxHeight = Math.Max( BooleanButton.Height, Math.Max( trueSize.Height, falseSize.Height ) );
		        trueSize.Height = maxHeight;
		        falseSize.Height = maxHeight;

		        this.trueButton.TextRect = new Rectangle( 0, (this.Height - trueSize.Height) / 2, trueSize.Width, trueSize.Height );
		        this.falseButton.TextRect = new Rectangle( 0, (this.Height - falseSize.Height) / 2, falseSize.Width, falseSize.Height );
		    }

		    this.trueButton.ButtonRect = new Rectangle( 0, (this.Height - BooleanButton.Height) / 2, BooleanButton.Width, BooleanButton.Height );
	        this.falseButton.ButtonRect = new Rectangle( 0, (this.Height - BooleanButton.Height) / 2, BooleanButton.Width, BooleanButton.Height );
		    
		    switch( this.buttonAlign )
		    {
		        case HorizontalAlignment.Left:
		            this.trueButton.SetX( this.Padding.Left );
		            this.falseButton.SetX( this.trueButton.Bounds.Right + this.spacing );
		            break;
		        case HorizontalAlignment.Right:
		            this.falseButton.SetX( this.Width - this.Padding.Right - this.falseButton.Bounds.Width );
		            this.trueButton.SetX( this.falseButton.Bounds.Left - this.trueButton.Bounds.Width - this.spacing );
		            break;
		        case HorizontalAlignment.Center:
		            int width = this.falseButton.Bounds.Width + this.trueButton.Bounds.Width + this.spacing;
		            this.trueButton.SetX( (this.Width - width) / 2 );
		            this.falseButton.SetX( this.trueButton.Bounds.Right + this.spacing );
		            break;
		    }
		}
		
		#endregion
		
		#region Painting
        
        protected override void OnPaint( PaintEventArgs e )
        {
            using( Brush brush = new SolidBrush( this.BackColor ) )
            {
                e.Graphics.FillRectangle( brush, this.ClientRectangle );
            }
            if( this.Focused )
            {
    		    if( this.value == true )
    		        this.focusRect = this.trueButton.TextRect;
    		    else
    		        this.focusRect = this.falseButton.TextRect;
    		    this.focusRect.Inflate( -BooleanButton.TextPadding, -BooleanButton.TextPadding );
                ControlPaint.DrawFocusRectangle( e.Graphics, this.focusRect, SystemColors.ControlDark, this.BackColor );
            }
            
            this.DrawButton( e.Graphics, this.trueButton );
            this.DrawButton( e.Graphics, this.falseButton );
        }
        
        private void DrawButton( Graphics g, BooleanButton button )
        {
            TextFormatFlags tff = TextFormatFlags.Left | TextFormatFlags.VerticalCenter;
            Color color = this.ForeColor;
            RadioButtonState state;

            if( !this.Enabled )
            {
                color = SystemColors.GrayText;
                state = button.Checked ? RadioButtonState.CheckedDisabled : RadioButtonState.UncheckedDisabled;
            }
            else
            {
                if( button.Hot )
                {
                    if( this.leftButtonDown )
                        state = button.Checked ? RadioButtonState.CheckedPressed : RadioButtonState.UncheckedPressed;
                    else
                        state = button.Checked ? RadioButtonState.CheckedHot : RadioButtonState.UncheckedHot;
                }
                else
                    state = button.Checked ? RadioButtonState.CheckedNormal : RadioButtonState.UncheckedNormal;
            }

            ButtonRenderer.DrawRadioButton( g, button.ButtonRect, state );

            TextRenderer.DrawText( g, button.Text, this.Font, button.TextRect, color, tff );
        }
        
        #endregion
        
        #region Helper classes
        
        internal class BooleanButton
        {
            public const int TextPadding = 2;
            public const int Height = 13;
            public const int Width = 12;
            
            public BooleanButton( string text )
            {
                this.text = text;
            }
            
            public Rectangle Bounds
            {
                get
                {
                    Rectangle rect = new Rectangle();
                    rect.X = this.buttonRect.X;
                    rect.Y = Math.Min( this.buttonRect.Y, this.textRect.Y );
                    rect.Height = Math.Max( this.buttonRect.Height, this.textRect.Height );
                    rect.Width = this.buttonRect.Width + this.textRect.Width;
                    return rect;
                }
            }
            
            private Rectangle buttonRect = Rectangle.Empty;
            public Rectangle ButtonRect
            {
                get { return this.buttonRect; }
                set { this.buttonRect = value; }
            }
            
            private Rectangle textRect = Rectangle.Empty;
            public Rectangle TextRect
            {
                get
                {
                    return this.textRect;
                }
                set
                {
                    this.textRect = value;
                    this.textRect.Inflate( TextPadding, TextPadding );
//                    this.textRect.Offset( TextPadding, TextPadding );
                }
            }
            
            private string text = string.Empty;
            public string Text
            {
                get { return this.text; }
                set { this.text = value; }
            }
            
            public void SetX( int x )
            {
                this.buttonRect.X = x;
                this.textRect.X = this.buttonRect.Right + TextPadding;
            }
            
            private bool check = false;
            public bool Checked
            {
                get { return this.check; }
                set { this.check = value; }
            }
            
            private bool hot = false;
            public bool Hot
            {
                get { return this.hot; }
                set { this.hot = value; }
            }
        }
        #endregion
    }
}
