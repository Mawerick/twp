﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Twp.Utilities;

namespace Twp.Controls
{
    public static class ButtonRenderer
    {
        public static void DrawButton( Graphics g, Rectangle rect, PushButtonState state )
        {
            if( Application.RenderWithVisualStyles )
            {
                System.Windows.Forms.ButtonRenderer.DrawButton( g, rect, state );
            }
            else
            {
                ButtonState bState = ButtonState.Normal;
                switch( state )
                {
                    case PushButtonState.Disabled:
                        bState = ButtonState.Inactive;
                        break;
                    case PushButtonState.Pressed:
                        bState = ButtonState.Pushed;
                        break;
                }
                ControlPaint.DrawButton( g, rect, bState );
            }
        }

        public static void DrawRadioButton( Graphics g, Rectangle rect, RadioButtonState state )
        {
            if( Application.RenderWithVisualStyles )
            {
                System.Windows.Forms.RadioButtonRenderer.DrawRadioButton( g, rect.Location, state );
            }
            else
            {
                ButtonState bState = ButtonState.Normal;
                switch( state )
                {
                    case RadioButtonState.UncheckedNormal:
                        bState = ButtonState.Normal;
                        break;
                    case RadioButtonState.UncheckedHot:
                        bState = ButtonState.Normal | ButtonState.Pushed;
                        break;
                    case RadioButtonState.UncheckedPressed:
                        bState = ButtonState.Pushed;
                        break;
                    case RadioButtonState.UncheckedDisabled:
                        bState = ButtonState.Inactive;
                        break;
                    case RadioButtonState.CheckedNormal:
                        bState = ButtonState.Checked | ButtonState.Normal;
                        break;
                    case RadioButtonState.CheckedHot:
                        bState = ButtonState.Checked | ButtonState.Normal | ButtonState.Pushed;
                        break;
                    case RadioButtonState.CheckedPressed:
                        bState = ButtonState.Checked | ButtonState.Pushed;
                        break;
                    case RadioButtonState.CheckedDisabled:
                        bState = ButtonState.Checked | ButtonState.Inactive;
                        break;
                }
                rect.Offset( 1, 1 );
                ControlPaint.DrawRadioButton( g, rect, bState );
            }
        }

        public static void DrawArrow( Graphics g, Rectangle rect, Color color, ArrowDirection direction )
        {
            Point center = new Point( rect.X + (rect.Width / 2), rect.Y + (rect.Height / 2) );
            Point[] points = new Point[3];
            switch( direction )
            {
                case ArrowDirection.Up:
                    points[0] = new Point( center.X, center.Y - 3 );
                    points[1] = new Point( center.X + 5, center.Y + 2 );
                    points[2] = new Point( center.X - 4, center.Y + 2 );
                    break;
                case ArrowDirection.Down:
                    points[0] = new Point( center.X - 3, center.Y - 2 );
                    points[1] = new Point( center.X + 4, center.Y - 2 );
                    points[2] = new Point( center.X, center.Y + 2 );
                    break;
                case ArrowDirection.Left:
                    points[0] = new Point( center.X + 2, center.Y - 4 );
                    points[1] = new Point( center.X + 2, center.Y + 4 );
                    points[2] = new Point( center.X - 2, center.Y );
                    break;
                case ArrowDirection.Right:
                    points[0] = new Point( center.X - 2, center.Y - 4 );
                    points[1] = new Point( center.X - 2, center.Y + 4 );
                    points[2] = new Point( center.X + 2, center.Y );
                    break;
            }
            using( Brush brush = new SolidBrush( color ) )
            {
                g.FillPolygon( brush, points );
            }
        }

        public static void DrawEndArrow( Graphics g, Rectangle rect, Color color, ArrowDirection direction )
        {
            DrawArrow( g, rect, color, direction );
            Point center = new Point( rect.X + (rect.Width / 2), rect.Y + (rect.Height / 2) );
            Point[] points = new Point[2];
            switch( direction )
            {
                case ArrowDirection.Up:
                    points[0] = new Point( center.X - 3, center.Y - 3 );
                    points[1] = new Point( center.X + 3, center.Y - 3 );
                    break;
                case ArrowDirection.Down:
                    points[0] = new Point( center.X - 3, center.Y + 2 );
                    points[1] = new Point( center.X + 3, center.Y + 2 );
                    break;
                case ArrowDirection.Left:
                    points[0] = new Point( center.X - 3, center.Y - 3 );
                    points[1] = new Point( center.X - 3, center.Y + 3 );
                    break;
                case ArrowDirection.Right:
                    points[0] = new Point( center.X + 2, center.Y - 3 );
                    points[1] = new Point( center.X + 2, center.Y + 3 );
                    break;
            }
            using( Pen pen = new Pen( color ) )
            {
                g.DrawLine( pen, points[0], points[1] );
            }
        }
    }
}
