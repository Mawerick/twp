﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Twp.Controls
{
	/// <summary>
	/// Description of ConsoleView.
	/// </summary>
	[Designer( typeof( Design.ConsoleViewDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
	public partial class ConsoleView : TextBox
	{
		public ConsoleView()
		{
			this.ResetFont();
			this.BackColor = Color.Black;
			this.ForeColor = Color.Lime;
			this.Multiline = true;
			this.ReadOnly = true;
			this.ScrollBars = ScrollBars.Vertical;

			if( this.DesignMode )
				return;

			ConsoleViewWriter.Register( this );

			this.updateTimer.Interval = 250;
			this.updateTimer.Tick += new EventHandler( OnTick );
			this.updateTimer.Start();
		}

		private StringBuilder stringBuilder = new StringBuilder();
		private Timer updateTimer = new Timer();

		[Category( "Behavior" )]
		[Description( "Determins the time, in milliseconds, between updates." )]
		[DefaultValue( 250 )]
		public int UpdateInterval
		{
			get { return this.updateTimer.Interval; }
			set { this.updateTimer.Interval = value; }
		}

		#region Font

		private static new Font DefaultFont = new Font( "Consolas", 8F, FontStyle.Regular, GraphicsUnit.Point, (byte) 0 );

		public override void ResetFont()
		{
			base.Font = DefaultFont;
		}

		private bool ShouldSerializeFont()
		{
			return !this.Font.Equals( DefaultFont );
		}

		public override Font Font
		{
			get { return base.Font; }
			set
			{
				if( value == null )
					ResetFont();
				else
					base.Font = value;
			}
		}

		#endregion

		[DefaultValue( typeof( Color ), "Black" )]
		public override Color BackColor
		{
			get { return base.BackColor; }
			set { base.BackColor = value; }
		}

		[DefaultValue( typeof( Color ), "Lime" )]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override bool Multiline
		{
			get { return base.Multiline; }
			set { base.Multiline = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new bool ReadOnly
		{
			get { return base.ReadOnly; }
			set { base.ReadOnly = value; }
		}

		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public new ScrollBars ScrollBars
		{
			get { return base.ScrollBars; }
			set { base.ScrollBars = value; }
		}

		protected override Size DefaultSize
		{
			get { return new Size( 200, 100 ); }
		}

		public void AppendChar( char c )
		{
			this.stringBuilder.Append( c, 1 );
		}

		internal void OnTick( object sender, EventArgs e )
		{
			MethodInvoker method = delegate
			{
				if( this.IsDisposed )
					return;
				this.AppendText( this.stringBuilder.ToString() );
				this.stringBuilder = new StringBuilder();
				Application.DoEvents();
			};
			
			if( this.InvokeRequired )
				this.BeginInvoke( method );
			else
				method.Invoke();
		}

		protected override void Dispose(bool disposing)
		{
			ConsoleViewWriter.UnRegister( this );
			base.Dispose( disposing );
		}

		private class ConsoleViewWriter : TextWriter
		{
			private static ConsoleViewWriter instance;
			private static object dummy = new object();

			private List<ConsoleView> outputs = new List<ConsoleView>();

			private ConsoleViewWriter()
			{
				Console.SetOut( this );
			}

			public static ConsoleViewWriter Instance
			{
				get
				{
					if( instance == null )
					{
						lock( dummy )
						{
							instance = new ConsoleViewWriter();
						}
					}
					return instance;
				}
			}

			public static void Register( ConsoleView output )
			{
				Twp.Utilities.Log.Debug( "[ConsoleViewWriter.Register] Registering output control: {0}", output.Name );
				Instance.outputs.Add( output );
			}

			public static void UnRegister( ConsoleView output )
			{
				Twp.Utilities.Log.Debug( "[ConsoleViewWriter.UnRegister] Unregistering output control: {0}", output.Name );
				Instance.outputs.Remove( output );
			}

			public override void Write( char value )
			{
				base.Write( value );
				foreach( ConsoleView output in this.outputs )
				{
					output.AppendChar( value );
				}
			}

			public override Encoding Encoding
			{
				get { return Encoding.UTF8; }
			}
		}
	}
}
