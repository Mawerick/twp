﻿namespace Twp.Controls
{
    partial class ConsoleWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsoleWindow));
            this.panel = new System.Windows.Forms.Panel();
            this.consoleView = new Twp.Controls.ConsoleView();
            this.inputBox = new Twp.Controls.InputBox();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel.Controls.Add(this.consoleView);
            this.panel.Controls.Add(this.inputBox);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(624, 362);
            this.panel.TabIndex = 2;
            // 
            // consoleView
            // 
            this.consoleView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.consoleView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consoleView.Location = new System.Drawing.Point(0, 0);
            this.consoleView.Name = "consoleView";
            this.consoleView.ReadOnly = true;
            this.consoleView.Size = new System.Drawing.Size(620, 338);
            this.consoleView.TabIndex = 0;
            // 
            // inputBox
            // 
            this.inputBox.BackColor = System.Drawing.Color.Black;
            this.inputBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.inputBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.inputBox.ForeColor = System.Drawing.Color.Gold;
            this.inputBox.Location = new System.Drawing.Point(0, 338);
            this.inputBox.Name = "inputBox";
            this.inputBox.Size = new System.Drawing.Size(620, 20);
            this.inputBox.TabIndex = 1;
            this.inputBox.EnterPressed += new System.EventHandler(this.inputBox_EnterPressed);
            // 
            // ConsoleWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 362);
            this.Controls.Add(this.panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConsoleWindow";
            this.Text = "ConsoleWindow";
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private ConsoleView consoleView;
        private InputBox inputBox;
        private System.Windows.Forms.Panel panel;
    }
}