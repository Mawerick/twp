﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Twp.Controls
{
    public partial class ConsoleWindow : Form
    {
        public ConsoleWindow()
        {
            InitializeComponent();

            this.stdin = Console.OpenStandardInput();
        }

        private Stream stdin;

        private void inputBox_EnterPressed( object sender, EventArgs e )
        {
            byte[] bytes = Encoding.UTF8.GetBytes( this.inputBox.Text );
            this.stdin.Write( bytes, 0, bytes.Length );
            this.inputBox.Text = "";
        }
    }
}
