﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Controls
{
    /// <summary>
    /// Description of ControlRenderer.
    /// </summary>
    public static class ControlRenderer
    {
        #region DrawBorder

        /// <summary>
        /// Draws a three-dimensional style border on the specified graphics surface and withind the specified area of a control.
        /// </summary>
        /// <param name="graphics">The <see cref="System.Drawing.Graphics"/> to draw on.</param>
        /// <param name="x">The x-coordinate of the top left corner of the border rectangle.</param>
        /// <param name="x">The x-coordinate of the top left corner of the border rectangle.</param>
        /// <param name="width">The width of the border rectangle.</param>
        /// <param name="height">The height of the border rectangle.</param>
        /// <param name="baseColor">The <see cref="System.Drawing.Color"/> that represents the base color of the border.</param>
        public static void DrawBorder( Graphics graphics, int x, int y, int width, int height, Color baseColor )
        {
            DrawBorder( graphics, new Rectangle( x, y, width, height ), baseColor );
        }

        /// <summary>
        /// Draws a three-dimensional style border with the specified style, on the specified graphics surface, and withind the specified area of a control.
        /// </summary>
        /// <param name="graphics">The <see cref="System.Drawing.Graphics"/> to draw on.</param>
        /// <param name="x">The x-coordinate of the top left corner of the border rectangle.</param>
        /// <param name="x">The x-coordinate of the top left corner of the border rectangle.</param>
        /// <param name="width">The width of the border rectangle.</param>
        /// <param name="height">The height of the border rectangle.</param>
        /// <param name="baseColor">The <see cref="System.Drawing.Color"/> that represents the base color of the border.</param>
        /// <param name="style">One of the <see cref="System.Windows.Forms.Border3DStyle"/> values that specify the style of the border.</param>
        public static void DrawBorder( Graphics graphics, int x, int y, int width, int height, Color baseColor, Border3DStyle style )
        {
            DrawBorder( graphics, new Rectangle( x, y, width, height ), baseColor, style );
        }

        /// <summary>
        /// Draws a three-dimensional style border with the specified style, on the specified graphics surface and sides, and withind the specified area of a control.
        /// </summary>
        /// <param name="graphics">The <see cref="System.Drawing.Graphics"/> to draw on.</param>
        /// <param name="x">The x-coordinate of the top left corner of the border rectangle.</param>
        /// <param name="x">The x-coordinate of the top left corner of the border rectangle.</param>
        /// <param name="width">The width of the border rectangle.</param>
        /// <param name="height">The height of the border rectangle.</param>
        /// <param name="baseColor">The <see cref="System.Drawing.Color"/> that represents the base color of the border.</param>
        /// <param name="style">One of the <see cref="System.Windows.Forms.Border3DStyle"/> values that specify the style of the border.</param>
        /// <param name="sides">One of the <see cref="System.Windows.Forms.Border3DSide"/> values that specify the side of the rectangle to draw the border on.</param>
        public static void DrawBorder( Graphics graphics, int x, int y, int width, int height, Color baseColor, Border3DStyle style, Border3DSide sides )
        {
            DrawBorder( graphics, new Rectangle( x, y, width, height ), baseColor, style, sides );
        }

        /// <summary>
        /// Draws a three-dimensional style border on the specified graphics surface and withind the specified area of a control.
        /// </summary>
        /// <param name="graphics">The <see cref="System.Drawing.Graphics"/> to draw on.</param>
        /// <param name="rectangle">The <see cref="System.Drawing.Rectangle"/> that represents the dimensions of the border.</param>
        /// <param name="baseColor">The <see cref="System.Drawing.Color"/> that represents the base color of the border.</param>
        public static void DrawBorder( Graphics graphics, Rectangle rectangle, Color baseColor )
        {
            DrawBorder( graphics, rectangle, baseColor, Border3DStyle.Etched, Border3DSide.All );
        }

        /// <summary>
        /// Draws a three-dimensional style border with the specified style, on the specified graphics surface, and withind the specified area of a control.
        /// </summary>
        /// <param name="graphics">The <see cref="System.Drawing.Graphics"/> to draw on.</param>
        /// <param name="rectangle">The <see cref="System.Drawing.Rectangle"/> that represents the dimensions of the border.</param>
        /// <param name="baseColor">The <see cref="System.Drawing.Color"/> that represents the base color of the border.</param>
        /// <param name="style">One of the <see cref="System.Windows.Forms.Border3DStyle"/> values that specify the style of the border.</param>
        public static void DrawBorder( Graphics graphics, Rectangle rectangle, Color baseColor, Border3DStyle style )
        {
            DrawBorder( graphics, rectangle, baseColor, style, Border3DSide.All );
        }

        /// <summary>
        /// Draws a three-dimensional style border with the specified style, on the specified graphics surface and sides, and withind the specified area of a control.
        /// </summary>
        /// <param name="graphics">The <see cref="System.Drawing.Graphics"/> to draw on.</param>
        /// <param name="rectangle">The <see cref="System.Drawing.Rectangle"/> that represents the dimensions of the border.</param>
        /// <param name="baseColor">The <see cref="System.Drawing.Color"/> that represents the base color of the border.</param>
        /// <param name="style">One of the <see cref="System.Windows.Forms.Border3DStyle"/> values that specify the style of the border.</param>
        /// <param name="sides">One of the <see cref="System.Windows.Forms.Border3DSide"/> values that specify the side of the rectangle to draw the border on.</param>
        public static void DrawBorder( Graphics graphics, Rectangle rectangle, Color baseColor, Border3DStyle style, Border3DSide sides )
        {
            if( graphics == null )
                throw new ArgumentNullException( "graphics" );

            Color color1 = Color.Empty;
            Color color2 = Color.Empty;
            Color color3 = Color.Empty;
            Color color4 = Color.Empty;

            switch( style )
            {
                case Border3DStyle.Adjust:
                    break;
                case Border3DStyle.Bump:
                    color1 = ControlPaint.Light( baseColor );
                    color2 = ControlPaint.DarkDark( baseColor );
                    color3 = ControlPaint.Light( baseColor );
                    color4 = ControlPaint.DarkDark( baseColor );
                    break;
                case Border3DStyle.Etched:
                    color1 = ControlPaint.Dark( baseColor );
                    color2 = ControlPaint.LightLight( baseColor );
                    color3 = ControlPaint.Dark( baseColor );
                    color4 = ControlPaint.LightLight( baseColor );
                    break;
                case Border3DStyle.Flat:
                    if( baseColor.GetBrightness() > 0.5F )
                    {
                        color1 = ControlPaint.Dark( baseColor );
                        color4 = ControlPaint.Dark( baseColor );
                    }
                    else
                    {
                        color1 = ControlPaint.Light( baseColor );
                        color4 = ControlPaint.Light( baseColor );
                    }
                    break;
                case Border3DStyle.Raised:
                    color1 = ControlPaint.Light( baseColor );
                    color2 = ControlPaint.LightLight( baseColor );
                    color3 = ControlPaint.Dark( baseColor );
                    color4 = ControlPaint.DarkDark( baseColor );
                    break;
                case Border3DStyle.RaisedInner:
                    color1 = ControlPaint.LightLight( baseColor );
                    color4 = ControlPaint.Dark( baseColor );
                    break;
                case Border3DStyle.RaisedOuter:
                    color1 = ControlPaint.Light( baseColor );
                    color4 = ControlPaint.DarkDark( baseColor );
                    break;
                case Border3DStyle.Sunken:
                    color1 = ControlPaint.Dark( baseColor );
                    color2 = ControlPaint.DarkDark( baseColor );
                    color3 = ControlPaint.Light( baseColor );
                    color4 = ControlPaint.LightLight( baseColor );
                    break;
                case Border3DStyle.SunkenInner:
                    color1 = ControlPaint.DarkDark( baseColor );
                    color4 = ControlPaint.Light( baseColor );
                    break;
                case Border3DStyle.SunkenOuter:
                    color1 = ControlPaint.Dark( baseColor );
                    color4 = ControlPaint.LightLight( baseColor );
                    break;
            }

            // Left and Top, outer line.
            Point p1 = new Point( rectangle.Left, rectangle.Bottom - 1 );
            Point p2 = new Point( rectangle.Left, rectangle.Top );
            Point p3 = new Point( rectangle.Right - 1, rectangle.Top );
            if( Flag.IsSet( sides, Border3DSide.Left ) )
                DrawLine( graphics, color1, p1, p2 );
            if( Flag.IsSet( sides, Border3DSide.Top ) )
                DrawLine( graphics, color1, p2, p3 );

            // Left and Top, inner line.
            p1.Offset( 1, -1 );
            p2.Offset( 1, 1 );
            p3.Offset( -1, 1 );
            if( Flag.IsSet( sides, Border3DSide.Left ) )
                DrawLine( graphics, color2, p1, p2 );
            if( Flag.IsSet( sides, Border3DSide.Top ) )
                DrawLine( graphics, color2, p2, p3 );

            // Bottom and Right, inner line.
            p1 = new Point( rectangle.Left + 1, rectangle.Bottom - 1 );
            p2 = new Point( rectangle.Right - 1, rectangle.Bottom - 1 );
            p3 = new Point( rectangle.Right - 1, rectangle.Top + 1 );
            if( Flag.IsSet( sides, Border3DSide.Bottom ) )
                DrawLine( graphics, color3, p1, p2 );
            if( Flag.IsSet( sides, Border3DSide.Right ) )
                DrawLine( graphics, color3, p2, p3 );

            // Bottom and Right, outer line.
            p1.Offset( 1, -1 );
            p2.Offset( -1, -1 );
            p3.Offset( -1, 1 );
            if( Flag.IsSet( sides, Border3DSide.Bottom ) )
                DrawLine( graphics, color4, p1, p2 );
            if( Flag.IsSet( sides, Border3DSide.Right ) )
                DrawLine( graphics, color4, p2, p3 );
        }

        #endregion

        #region DrawLine

        public static void DrawLine( Graphics graphics, Color color, int x1, int y1, int x2, int y2 )
        {
            DrawLine( graphics, color, new Point( x1, y2 ), new Point( x2, y2 ) );
        }

        public static void DrawLine( Graphics graphics, Color color, Point p1, Point p2 )
        {
            if( graphics == null )
                throw new ArgumentNullException( "graphics" );

            if( color == Color.Empty )
                return;

            using( Pen pen = new Pen( color ) )
            {
                graphics.DrawLine( pen, p1, p2 );
            }
        }

        #endregion

        #region DrawLines

        public static void DrawLines( Graphics graphics, Color color, Point[] points )
        {
            if( graphics == null )
                throw new ArgumentNullException( "graphics" );

            if( color == Color.Empty )
                return;

            using( Pen pen = new Pen( color ) )
            {
                graphics.DrawLines( pen, points );
            }
        }

        #endregion
    }
}
