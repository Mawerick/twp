﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;

namespace Twp.Controls.Design
{
	internal class BooleanBoxDesigner : ControlDesigner
	{
	    private BooleanBox box;
	    
		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
			this.box = (BooleanBox) component;
		}
		
		public override SelectionRules SelectionRules
		{
			get
			{
//				if( (bool) DesignerExtras.GetProperty( this.Component, "AutoSize" ) == true )
			    if( this.box.AutoSize )
					return SelectionRules.Moveable | SelectionRules.Visible;
				else
					return ( base.SelectionRules & ~SelectionRules.TopSizeable ) & ~SelectionRules.BottomSizeable;
			}
		}

		public override IList SnapLines
		{
			get
			{
				IList snapLines = base.SnapLines;
				snapLines.Add( new SnapLine( SnapLineType.Baseline, (int) DesignerExtras.GetProperty( this.Component, "TextBaseline" ) ) );
				return snapLines;
			}
		}

		protected override void PostFilterProperties( IDictionary properties )
		{
			properties.Remove( "Text" );
			properties.Remove( "BackgroundImage" );
			properties.Remove( "BackgroundImageLayout" );

			properties.Remove( "AllowDrop" );
			properties.Remove( "ContextMenuStrip" );

			base.PostFilterProperties( properties );
		}

		protected override void PostFilterEvents( IDictionary events )
		{
			//Actions
			events.Remove( "Click" );
			events.Remove( "DoubleClick" );

			//Appearence
			events.Remove( "Paint" );

			//Behavior
			events.Remove( "ChangeUICues" );
			events.Remove( "ImeModeChanged" );
			events.Remove( "QueryAccessibilityHelp" );
			events.Remove( "StyleChanged" );
			events.Remove( "SystemColorsChanged" );

			//Drag Drop
			events.Remove( "DragDrop" );
			events.Remove( "DragEnter" );
			events.Remove( "DragLeave" );
			events.Remove( "DragOver" );
			events.Remove( "GiveFeedback" );
			events.Remove( "QueryContinueDrag" );
			events.Remove( "DragDrop" );

			//Layout
			events.Remove( "Layout" );

			//Property Changed
			events.Remove( "BackgroundImageChanged" );
			events.Remove( "BindingContextChanged" );
			events.Remove( "CausesValidationChanged" );
			events.Remove( "CursorChanged" );
			events.Remove( "SizeChanged" );
			events.Remove( "TextChanged" );

			base.PostFilterEvents( events );
		}
	}
}
