﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;

namespace Twp.Controls.Design
{
	/// <summary>
	/// Description of ConsoleViewDesigner.
	/// </summary>
	public class ConsoleViewDesigner : ControlDesigner
	{
		private DesignerActionListCollection actionLists;
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if( this.actionLists == null )
				{
					this.actionLists = new DesignerActionListCollection();
					this.actionLists.Add( new ConsoleViewActionList( this ) );
				}
				return this.actionLists;
			}
		}

		protected override void PostFilterProperties( IDictionary properties )
		{
			properties.Remove( "Multiline" );
			properties.Remove( "PasswordChar" );
			properties.Remove( "UseSystemPasswordChar" );
			
			base.PostFilterProperties( properties );
		}
	}
	
	internal class ConsoleViewActionList : DesignerActionList
	{
		public ConsoleViewActionList( ConsoleViewDesigner designer ) : base( designer.Component )
		{
		}
		
		public bool DockInParent
		{
			get { return ((ConsoleView)base.Component).Dock == DockStyle.Fill; }
			set { DesignerExtras.SetProperty( base.Component, "Dock", value ? DockStyle.Fill : DockStyle.None ); }
		}
		
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add( new DesignerActionPropertyItem( "DockInParent", "Dock in parent" ) );
			return items;
		}
	}
}
