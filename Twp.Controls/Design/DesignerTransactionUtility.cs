﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel.Design;

namespace Twp.Controls.Design
{
	public delegate object TransactionAwareParammedMethod( IDesignerHost host, object param );

	public abstract class DesignerTransactionUtility
	{
		public static object DoInTransaction( IDesignerHost host, string name, TransactionAwareParammedMethod method, object param )
		{
			DesignerTransaction transaction = null;
			object value = null;

			try
			{
				transaction = host.CreateTransaction( name );
				value = method( host, param );
			}
			catch( CheckoutException ex )
			{
				if( ex != CheckoutException.Canceled )
					throw ex;
			}
			catch
			{
				if( transaction != null )
				{
					transaction.Cancel();
					transaction = null;
				}
				throw;
			}
			finally
			{
				if( transaction != null )
					transaction.Commit();
			}

			return value;
		}
	}
}
