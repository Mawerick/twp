﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Globalization;

namespace Twp.Controls.Design
{
    /// <summary>
    /// Description of DictionaryEditor.
    /// </summary>
    public class DictionaryEditor<TKey, TValue> : CollectionEditor
    {
        public DictionaryEditor( Type type ) : base( type )
        {
        }
        
        private DictionaryEditorAttribute editorAttribute;
        private CollectionEditor.CollectionForm form;
        
        protected override CollectionEditor.CollectionForm CreateCollectionForm()
        {
            this.form = base.CreateCollectionForm();
            this.form.Text = this.editorAttribute.Title ?? "Dictinary Editor";
            
            Type formType = this.form.GetType();
            PropertyInfo pi = formType.GetProperty( "CollectionEditable", BindingFlags.NonPublic | BindingFlags.Instance );
            pi.SetValue( this.form, true, null );
            
            return this.form;
        }
        
        public override object EditValue( ITypeDescriptorContext context, IServiceProvider provider, object value )
        {
            if( context == null )
                throw new ArgumentNullException( "context" );
            
            DictionaryEditorAttribute attribute = context.PropertyDescriptor.Attributes[typeof(DictionaryEditorAttribute)] as DictionaryEditorAttribute;
            if( attribute != null )
            {
                this.editorAttribute = attribute;
                
                if( this.editorAttribute.KeyDefaultProviderType == null )
                    this.editorAttribute.KeyDefaultProviderType = typeof( DefaultProvider<TKey> );
                if( this.editorAttribute.ValueDefaultProviderType == null )
                    this.editorAttribute.ValueDefaultProviderType = typeof( DefaultProvider<TValue> );
            }
            else
            {
                this.editorAttribute = new DictionaryEditorAttribute();
                this.editorAttribute.KeyDefaultProviderType = typeof( DefaultProvider<TKey> );
                this.editorAttribute.ValueDefaultProviderType = typeof( DefaultProvider<TValue> );
            }
            
            return base.EditValue( context, provider, value );
        }
        
        protected override object CreateInstance( Type itemType )
        {
            TKey key;
            TValue value;
            
            DefaultProvider<TKey> keyDefaultProvider = Activator.CreateInstance( this.editorAttribute.KeyDefaultProviderType ) as DefaultProvider<TKey>;
            if( keyDefaultProvider != null )
                key = keyDefaultProvider.GetDefault( DefaultUsage.Key );
            else
                key = default(TKey);
            
            DefaultProvider<TValue> valueDefaultProvider = Activator.CreateInstance( this.editorAttribute.ValueDefaultProviderType ) as DefaultProvider<TValue>;
            if( valueDefaultProvider != null )
                value = valueDefaultProvider.GetDefault( DefaultUsage.Value );
            else
                value = default(TValue);
            
            return new EditableKeyValuePair<TKey, TValue>( key, value, this.editorAttribute );
        }
        
        protected override object[] GetItems( object editValue )
        {
            if( editValue == null )
                return new object[0];
            
            IDictionary<TKey, TValue> dictionary = editValue as IDictionary<TKey, TValue>;
            if( dictionary == null )
                throw new ArgumentNullException( "editValue" );
            
            object[] objArray = new object[dictionary.Count];
            int i = 0;
            foreach( KeyValuePair<TKey, TValue> entry in dictionary )
            {
                EditableKeyValuePair<TKey, TValue> entry2 = new EditableKeyValuePair<TKey, TValue>( entry.Key, entry.Value, this.editorAttribute );
                objArray[i++] = entry2;
            }
            return objArray;
        }
        
        protected override object SetItems( object editValue, object[] value )
        {
            if( value == null )
                throw new ArgumentNullException( "value" );
            
            IDictionary<TKey, TValue> dictionary = editValue as IDictionary<TKey, TValue>;
            if( dictionary == null )
                throw new ArgumentNullException( "editValue" );
            
            dictionary.Clear();
            foreach( EditableKeyValuePair<TKey, TValue> entry in value )
            {
                dictionary.Add( new KeyValuePair<TKey, TValue>( entry.Key, entry.Value ) );
            }
            return dictionary;
        }
        
        protected override string GetDisplayText( object value )
        {
            EditableKeyValuePair<TKey, TValue> entry = value as EditableKeyValuePair<TKey, TValue>;
            if( entry != null )
                return String.Format( CultureInfo.CurrentCulture, "{0}={1}", entry.Key, entry.Value );
            else
                return base.GetDisplayText( value );
        }
    }
}
