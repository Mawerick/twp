﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;

namespace Twp.Controls.Design
{
    /// <summary>
    /// Description of DictionaryEditorAttribute.
    /// </summary>
    [AttributeUsage( AttributeTargets.Property, AllowMultiple=false)]
    public sealed class DictionaryEditorAttribute : Attribute
    {
        public DictionaryEditorAttribute()
        {
            this.keyDisplayName = "Key";
            this.valueDisplayName = "Value";
        }
        
        private Type keyDefaultProviderType;
        public Type KeyDefaultProviderType
        {
            get { return this.keyDefaultProviderType; }
            set { this.keyDefaultProviderType = value; }
        }

        private Type keyConverterType;
        public Type KeyConverterType
        {
            get { return this.keyConverterType; }
            set { this.keyConverterType = value; }
        }

        private Type keyEditorType;
        public Type KeyEditorType
        {
            get { return this.keyEditorType; }
            set { this.keyEditorType = value; }
        }

        private Type keyAttributeProviderType;
        public Type KeyAttributeProviderType
        {
            get { return this.keyAttributeProviderType; }
            set { this.keyAttributeProviderType = value; }
        }

        private Type valueDefaultProviderType;
        public Type ValueDefaultProviderType
        {
            get { return this.valueDefaultProviderType; }
            set { this.valueDefaultProviderType = value; }
        }

        private Type valueConverterType;
        public Type ValueConverterType
        {
            get { return this.valueConverterType; }
            set { this.valueConverterType = value; }
        }

        private Type valueEditorType;
        public Type ValueEditorType
        {
            get { return this.valueEditorType; }
            set { this.valueEditorType = value; }
        }

        private Type valueAttributeProviderType;
        public Type ValueAttributeProviderType
        {
            get { return this.valueAttributeProviderType; }
            set { this.valueAttributeProviderType = value; }
        }

        private string title;
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }

        private string keyDisplayName;
        public string KeyDisplayName
        {
            get { return this.keyDisplayName; }
            set { this.keyDisplayName = value; }
        }

        private string valueDisplayName;
        public string ValueDisplayName
        {
            get { return this.valueDisplayName; }
            set { this.valueDisplayName = value; }
        }
    }
}
