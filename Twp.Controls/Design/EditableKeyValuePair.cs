﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace Twp.Controls.Design
{
    /// <summary>
    /// Description of EditableKeyValuePair.
    /// </summary>
    internal class EditableKeyValuePair<TKey, TValue> : CustomTypeDescriptor
    {
        private TKey key;
        public TKey Key
        {
            get { return this.key; }
            set { this.key = value; }
        }
        
        private TValue value;
        public TValue Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
        
        public EditableKeyValuePair( TKey key, TValue value, DictionaryEditorAttribute editorAttribute )
        {
            this.Key = key;
            this.Value = value;
            
            if( editorAttribute == null )
                throw new ArgumentNullException( "editorAttribute" );
            
            this.editorAttribute = editorAttribute;
        }
        
        private DictionaryEditorAttribute editorAttribute;
        public DictionaryEditorAttribute EditorAttribute
        {
            get { return this.editorAttribute; }
            set { this.editorAttribute = value; }
        }
        
        public override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            return this.GetProperties();
        }
        
        public override PropertyDescriptorCollection GetProperties()
        {
            List<PropertyDescriptor> properties = new List<PropertyDescriptor>();
            
            KeyValueDescriptor keyDescriptor = new KeyValueDescriptor( TypeDescriptor.CreateProperty( this.GetType(), "Key", typeof( TKey ) ),
                                                                      this.editorAttribute.KeyConverterType, this.editorAttribute.KeyEditorType,
                                                                      this.editorAttribute.KeyAttributeProviderType, this.editorAttribute.KeyDisplayName );
            properties.Add( keyDescriptor );

            KeyValueDescriptor valueDescriptor = new KeyValueDescriptor( TypeDescriptor.CreateProperty( this.GetType(), "Value", typeof( TValue ) ),
                                                                      this.editorAttribute.ValueConverterType, this.editorAttribute.ValueEditorType,
                                                                      this.editorAttribute.ValueAttributeProviderType, this.editorAttribute.ValueDisplayName );
            properties.Add( valueDescriptor );
            
            return new PropertyDescriptorCollection( properties.ToArray() );
        }
        
        public override object GetPropertyOwner( PropertyDescriptor pd )
        {
            return this;
        }
    }
}
