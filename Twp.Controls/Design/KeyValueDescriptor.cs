﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.ComponentModel;
using System.Drawing.Design;

namespace Twp.Controls.Design
{
    /// <summary>
    /// Description of KeyValueDescriptor.
    /// </summary>
    internal class KeyValueDescriptor : PropertyDescriptor
    {
        private PropertyDescriptor pd;
        private Type converterType;
        private Type editorType;
        private Type attributeProviderType;
        private string displayName;
        
        public KeyValueDescriptor( PropertyDescriptor pd, Type converterType, Type editorType, Type attributeProviderType, string displayName )
            : base( pd )
        {
            this.pd = pd;
            this.converterType = converterType;
            this.editorType = editorType;
            this.attributeProviderType = attributeProviderType;
            this.displayName = displayName;
        }
        
        public override string DisplayName
        {
            get { return this.displayName; }
        }
        
        public override bool CanResetValue( object component )
        {
            return this.pd.CanResetValue( component );
        }
        
        public override Type ComponentType
        {
            get { return this.pd.ComponentType; }
        }
        
        public override object GetValue( object component )
        {
            return this.pd.GetValue( component );
        }
        
        public override bool IsReadOnly
        {
            get { return this.pd.IsReadOnly; }
        }
        
        public override Type PropertyType
        {
            get { return this.pd.PropertyType; }
        }
        
        public override void ResetValue( object component )
        {
            this.pd.ResetValue( component );
        }
        
        public override void SetValue( object component, object value )
        {
            this.pd.SetValue( component, value );
        }
        
        public override bool ShouldSerializeValue( object component )
        {
            return this.pd.ShouldSerializeValue( component );
        }
        
        public override TypeConverter Converter
        {
            get
            {
                if( this.converterType != null )
                    return Activator.CreateInstance( this.converterType ) as TypeConverter;
                else
                    return TypeDescriptor.GetConverter( this.PropertyType );
            }
        }
        
        public override object GetEditor( Type editorBaseType )
        {
            if( this.editorType != null )
                return Activator.CreateInstance( this.editorType ) as UITypeEditor;
            else
                return TypeDescriptor.GetEditor( this.PropertyType, typeof( UITypeEditor ) );
        }
        
        public override AttributeCollection Attributes
        {
            get
            {
                if( this.attributeProviderType != null )
                    return (Activator.CreateInstance( this.attributeProviderType ) as AttributeProvider).GetAttributes( this.PropertyType );
                else
                    return TypeDescriptor.GetAttributes( this.PropertyType );
            }
        }
        
    }
}
