﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;

namespace Twp.Controls.Design
{
    internal class SliderDesigner : ControlDesigner
    {
        public override void Initialize( IComponent component )
        {
            base.Initialize( component );
        }

        public override SelectionRules SelectionRules
        {
            get
            {
                bool autoSize = (bool) DesignerExtras.GetProperty( this.Component, "AutoSize" );
                if( autoSize )
                {
                    Orientation orientation = (Orientation) DesignerExtras.GetProperty( this.Component, "Orientation" );
                    if( orientation == Orientation.Horizontal )
                        return (base.SelectionRules & ~SelectionRules.TopSizeable) & ~SelectionRules.BottomSizeable;
                    else
                        return (base.SelectionRules & ~SelectionRules.LeftSizeable) & ~SelectionRules.RightSizeable;
                }
                else
                    return base.SelectionRules;
            }
        }

        public override IList SnapLines
        {
            get
            {
                IList snapLines = base.SnapLines;
                int baseLine = (int) DesignerExtras.GetProperty( this.Component, "TextBaseline" );
                snapLines.Add( new SnapLine( SnapLineType.Baseline, baseLine ) );
                return snapLines;
            }
        }

        protected override void PostFilterProperties( IDictionary properties )
        {
            properties.Remove( "AllowDrop" );
            properties.Remove( "BackgroundImage" );
			properties.Remove( "BackgroundImageLayout" );
            properties.Remove( "ContextMenuStrip" );

            properties.Remove( "Text" );
            properties.Remove( "TextAlign" );
            properties.Remove( "RightToLeft" );

			base.PostFilterProperties( properties );
        }

        protected override void PostFilterEvents( IDictionary events )
        {
            //Actions
            events.Remove( "Click" );
            events.Remove( "DoubleClick" );

            //Appearence
            events.Remove( "Paint" );

            //Behavior
            events.Remove( "ChangeUICues" );
            events.Remove( "ImeModeChanged" );
            events.Remove( "QueryAccessibilityHelp" );
            events.Remove( "StyleChanged" );
            events.Remove( "SystemColorsChanged" );

            //Drag Drop
            events.Remove( "DragDrop" );
            events.Remove( "DragEnter" );
            events.Remove( "DragLeave" );
            events.Remove( "DragOver" );
            events.Remove( "GiveFeedback" );
            events.Remove( "QueryContinueDrag" );
            events.Remove( "DragDrop" );

            //Layout
            events.Remove( "Layout" );
            events.Remove( "Move" );
            events.Remove( "Resize" );

            //Property Changed
            events.Remove( "BackColorChanged" );
            events.Remove( "BackgroundImageChanged" );
            events.Remove( "BindingContextChanged" );
            events.Remove( "CausesValidationChanged" );
            events.Remove( "CursorChanged" );
            events.Remove( "FontChanged" );
            events.Remove( "ForeColorChanged" );
            events.Remove( "RightToLeftChanged" );
            events.Remove( "SizeChanged" );
            events.Remove( "TextChanged" );

            base.PostFilterEvents( events );
        }
    }
}
