﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
    internal class TabRowDesigner : ControlDesigner
    {
        private TabRow TabRow
        {
            get { return (TabRow) this.Control; }
        }

        public override void Initialize( IComponent component )
        {
            base.Initialize( component );

            ISelectionService s = (ISelectionService) this.GetService( typeof( ISelectionService ) );
            IComponentChangeService c = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
            s.SelectionChanged += new EventHandler( OnSelectionChanged );
            c.ComponentRemoving += new ComponentEventHandler( OnComponentRemoving );
        }

        private void OnSelectionChanged( object sender, EventArgs e )
        {
            this.TabRow.OnTabSelected();
        }

        private void OnComponentRemoving( object sender, ComponentEventArgs e )
        {
            IComponentChangeService c = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
            IDesignerHost h = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
            TabRow.Tab tab;

            if( e.Component is TabRow.Tab )
            {
                tab = (TabRow.Tab) e.Component;
                if( this.TabRow.Tabs.Contains( tab ) )
                {
                    c.OnComponentChanging( this.TabRow, null );
                    this.TabRow.Tabs.Remove( tab );
                    c.OnComponentChanged( this.TabRow, null, null, null );
                    return;
                }
            }

            if( e.Component == this.TabRow )
            {
                for( int i = this.TabRow.Tabs.Count - 1; i >= 0; i-- )
                {
                    tab = this.TabRow.Tabs[i];
                    c.OnComponentChanging( this.TabRow, null );
                    this.TabRow.Tabs.Remove( tab );
                    h.DestroyComponent( tab );
                    c.OnComponentChanged( this.TabRow, null, null, null );
                }
            }
        }

        protected override void Dispose( bool disposing )
        {
            ISelectionService s = (ISelectionService) this.GetService( typeof( ISelectionService ) );
            IComponentChangeService c = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
            s.SelectionChanged -= new EventHandler( OnSelectionChanged );
            c.ComponentRemoving -= new ComponentEventHandler( OnComponentRemoving );
            base.Dispose( disposing );
        }

        public override ICollection AssociatedComponents
        {
            get
            {
                return this.TabRow.Tabs;
            }
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                DesignerVerbCollection v = new DesignerVerbCollection();
                v.Add( new DesignerVerb( "&Add Tab", new EventHandler( OnAddTab ) ) );
                v.Add( new DesignerVerb( "&Remove Tab", new EventHandler( OnRemoveTab ) ) );
                return v;
            }
        }

        private void OnAddTab( object sender, EventArgs e )
        {
            IDesignerHost h = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
            IComponentChangeService c = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
            DesignerTransaction dt = h.CreateTransaction( "Add Tab" );
            TabRow.Tab tab = (TabRow.Tab) h.CreateComponent( typeof( TabRow.Tab ) );
            c.OnComponentChanging( this.TabRow, null );
            this.TabRow.Tabs.Add( tab );
            tab.Text = "tab" + this.TabRow.Tabs.Count.ToString();
            c.OnComponentChanged( this.TabRow, null, null, null );
            dt.Commit();
        }

        private void OnRemoveTab( object sender, EventArgs e )
        {
            IDesignerHost h = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
            IComponentChangeService c = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
            TabRow.Tab tab = this.TabRow.SelectedTab;
            if( this.TabRow.Tabs.Contains( tab ) )
            {
                DesignerTransaction dt = h.CreateTransaction( "Remove Tab" );
                c.OnComponentChanging( this.TabRow, null );
                this.TabRow.Tabs.Remove( tab );
                h.DestroyComponent( tab );
                c.OnComponentChanged( this.TabRow, null, null, null );
                dt.Commit();
            }
        }

        protected override bool GetHitTest( Point point )
        {
            point = this.TabRow.PointToClient( point );
            foreach( TabRow.Tab tab in this.TabRow.Tabs )
            {
                if( tab.Bounds.Contains( point ) )
                    return true;
            }
            return false;
        }

        public override SelectionRules SelectionRules
        {
            get
            {
                if( this.TabRow.AutoSize )
                {
                    switch( this.TabRow.Alignment )
                    {
                        case System.Windows.Forms.TabAlignment.Bottom:
                        case System.Windows.Forms.TabAlignment.Top:
                            return (base.SelectionRules & ~SelectionRules.TopSizeable) & ~SelectionRules.BottomSizeable;
                        default:
                            return (base.SelectionRules & ~SelectionRules.LeftSizeable) & ~SelectionRules.RightSizeable;
                    }
                }
                else
                    return base.SelectionRules;
            }
        }

        protected override void PostFilterProperties( IDictionary properties )
        {
            properties.Remove( "AllowDrop" );
            properties.Remove( "BackgroundImage" );
            properties.Remove( "BackgroundImageLayout" );
            properties.Remove( "ContextMenuStrip" );

            properties.Remove( "Text" );
            properties.Remove( "TextAlign" );
            properties.Remove( "RightToLeft" );

            base.PostFilterProperties( properties );
        }

        protected override void PostFilterEvents( IDictionary events )
        {
            //Actions
            events.Remove( "Click" );
            events.Remove( "DoubleClick" );

            //Appearence
            events.Remove( "Paint" );

            //Behavior
            events.Remove( "ChangeUICues" );
            events.Remove( "ImeModeChanged" );
            events.Remove( "QueryAccessibilityHelp" );
            events.Remove( "StyleChanged" );
            events.Remove( "SystemColorsChanged" );

            //Drag Drop
            events.Remove( "DragDrop" );
            events.Remove( "DragEnter" );
            events.Remove( "DragLeave" );
            events.Remove( "DragOver" );
            events.Remove( "GiveFeedback" );
            events.Remove( "QueryContinueDrag" );
            events.Remove( "DragDrop" );

            //Layout
            events.Remove( "Layout" );
            events.Remove( "Move" );
            events.Remove( "Resize" );

            //Property Changed
            events.Remove( "BackColorChanged" );
            events.Remove( "BackgroundImageChanged" );
            events.Remove( "BindingContextChanged" );
            events.Remove( "CausesValidationChanged" );
            events.Remove( "CursorChanged" );
            events.Remove( "FontChanged" );
            events.Remove( "ForeColorChanged" );
            events.Remove( "RightToLeftChanged" );
            events.Remove( "SizeChanged" );
            events.Remove( "TextChanged" );

            base.PostFilterEvents( events );
        }
    }
}
