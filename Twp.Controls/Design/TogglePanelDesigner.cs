﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
	public class TogglePanelDesigner : ParentControlDesigner
	{
		DesignerActionListCollection actionLists;

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
		}

		protected override void OnPaintAdornments( PaintEventArgs pe )
		{
			base.OnPaintAdornments( pe );

			Graphics g = pe.Graphics;
			using( Pen pen = new Pen( Color.Gray, 1 ) )
			{
				pen.DashStyle = DashStyle.Dash;
				Rectangle rect = (Rectangle) DesignerExtras.GetProperty( this.Component, "DisplayRectangle" );
				rect.Width -= 1;
				rect.Height -= 1;
				g.DrawRectangle( pen, rect );
			}
		}
        
		public override DesignerActionListCollection ActionLists
		{
		    get
		    {
				if( this.actionLists == null )
		        {
		            this.actionLists = new DesignerActionListCollection();
		            this.actionLists.Add( new TogglePanelActionList( this.Component ) );
		        }
		        return this.actionLists;
		    }
		}

		internal class TogglePanelActionList : DesignerActionList
		{
			public TogglePanelActionList( IComponent component ) : base( component )
			{
			}

			public void ToggleDockStyle()
			{
			    DockStyle dock = (DockStyle) DesignerExtras.GetProperty( this.Component, "Dock" );
				if( dock != DockStyle.Fill )
					DesignerExtras.SetProperty( this.Component, "Dock", DockStyle.Fill );
				else
					DesignerExtras.SetProperty( this.Component, "Dock", DockStyle.None );
			}

			private string GetDockStyleText()
			{
			    DockStyle dock = (DockStyle) DesignerExtras.GetProperty( this.Component, "Dock" );
				if( dock != DockStyle.Fill )
					return "Undock in parent container";
				else
					return "Dock in parent container";
			}

			public override DesignerActionItemCollection GetSortedActionItems()
			{
				DesignerActionItemCollection items = new DesignerActionItemCollection();
				items.Add( new DesignerActionMethodItem( this, "ToggleDockStyle", this.GetDockStyleText() ) );
				return items;
			}
		}
	}
}
