﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
    public class ToolBoxContainerDesigner : ParentControlDesigner
    {
        private ToolBoxContainer Container
        {
            get { return (ToolBoxContainer) this.Control; }
        }

        public override void Initialize( IComponent component )
        {
            base.Initialize( component );
        }

        protected override void OnPaintAdornments( PaintEventArgs e )
        {
            base.OnPaintAdornments( e );

            using( Pen pen = new Pen( Color.Gray, 1 ) )
            {
                pen.DashStyle = DashStyle.Dash;
                e.Graphics.DrawRectangle( pen, 0, 0, this.Container.Width - 1, this.Container.Height - 1 );
            }
        }

        private object AddPageTransaction( IDesignerHost host, object param )
        {
            ToolBox toolBox = (ToolBox) host.CreateComponent( typeof( ToolBox ) );
            MemberDescriptor member = TypeDescriptor.GetProperties( this.Container )["Controls"];
            this.RaiseComponentChanging( member );
            this.Container.Controls.Add( toolBox );
            this.RaiseComponentChanged( member, null, null );
            return null;
        }

        private void AddPageHandler( object sender, EventArgs e )
        {
            IDesignerHost host = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
            if( host == null )
                return;

            DesignerTransactionUtility.DoInTransaction( host, "ToolBoxContainerAddToolBox", new TransactionAwareParammedMethod( this.AddPageTransaction ), null );
        }

        public override bool CanParent( Control control )
        {
            if( control is ToolBox )
                return !this.Container.Contains( control );
            else
                return false;
        }

        private DesignerVerbCollection verbs;
        public override DesignerVerbCollection Verbs
        {
            get
            {
                if( this.verbs == null )
                {
                    this.verbs = new DesignerVerbCollection();
                    this.verbs.Add( new DesignerVerb( "Add ToolBox", new EventHandler( this.AddPageHandler ) ) );
                }
                return this.verbs;
            }
        }

        private DesignerActionListCollection actionLists;
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if( this.actionLists == null )
                {
                    this.actionLists = new DesignerActionListCollection();
                    this.actionLists.Add( new ToolBoxContainerActionList( this.Container ) );
                }
                return this.actionLists;
            }
        }

        internal class ToolBoxContainerActionList : DesignerActionList
        {
            public ToolBoxContainerActionList( IComponent component )
                : base( component )
            {
            }

            public void ToggleDockStyle()
            {
			    DockStyle dock = (DockStyle) DesignerExtras.GetProperty( this.Component, "Dock" );
				if( dock != DockStyle.Fill )
                    DesignerExtras.SetProperty( this.Component, "Dock", DockStyle.Fill );
                else
                    DesignerExtras.SetProperty( this.Component, "Dock", DockStyle.None );
            }

            private string GetDockStyleText()
            {
			    DockStyle dock = (DockStyle) DesignerExtras.GetProperty( this.Component, "Dock" );
				if( dock != DockStyle.Fill )
                    return "Undock from parent container";
                else
                    return "Dock in parent container";
            }

            public override DesignerActionItemCollection GetSortedActionItems()
            {
                DesignerActionItemCollection items = new DesignerActionItemCollection();
                items.Add( new DesignerActionMethodItem( this, "ToggleDockStyle", this.GetDockStyleText() ) );
                return items;
            }
        }
    }
}
