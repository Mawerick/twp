﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Twp.Controls.Design
{
	[Serializable]
	public class ToolBoxContainerToolboxItem : ToolboxItem
	{
		public ToolBoxContainerToolboxItem()
			: base( typeof( ToolBoxContainer ) )
		{
		}

		public ToolBoxContainerToolboxItem( SerializationInfo info, StreamingContext context )
		{
			this.Deserialize( info, context );
		}

		protected override IComponent[] CreateComponentsCore( IDesignerHost host )
		{
			return DesignerTransactionUtility.DoInTransaction(
				host,
				"ToolBoxContainerToolboxItem_CreateComponentsCore",
				new TransactionAwareParammedMethod( CreateContainerWithOneToolBox ),
				null
				) as IComponent[];
		}

		public object CreateContainerWithOneToolBox( IDesignerHost host, object param )
		{
			ToolBoxContainer container = (ToolBoxContainer) host.CreateComponent( typeof( ToolBoxContainer ) );
			ToolBox toolBox1 = (ToolBox) host.CreateComponent( typeof( ToolBox ) );
			container.Controls.Add( toolBox1 );
			return new IComponent[] { container };
		}
	}
}
