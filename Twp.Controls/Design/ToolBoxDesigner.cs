﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;
using System.Collections;

namespace Twp.Controls.Design
{
	public class ToolBoxDesigner : ParentControlDesigner
	{
		DesignerActionListCollection actionLists;

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
		}
		
		protected override void OnPaintAdornments( PaintEventArgs pe )
		{
			base.OnPaintAdornments( pe );

			BorderStyle bs = (BorderStyle) DesignerExtras.GetProperty( this.Component, "BorderStyle" );
			ToolBoxRenderingMode rm = (ToolBoxRenderingMode) DesignerExtras.GetProperty( this.Component, "RenderingMode" );
			if( bs == BorderStyle.None && rm != ToolBoxRenderingMode.Styled )
			{
				Graphics g = pe.Graphics;
				using( Pen pen = new Pen( Color.Gray, 1 ) )
				{
					pen.DashStyle = DashStyle.Dash;
					Rectangle rect = (Rectangle) DesignerExtras.GetProperty( this.Component, "DisplayRectangle" );
					rect.Width -= 1;
					rect.Height -= 1;
					g.DrawRectangle( pen, rect );
				}
			}
		}

		private bool IsContained
		{
			get
			{
			    Control parent = (Control) DesignerExtras.GetProperty( this.Component, "Parent" );
			    return parent is ToolBoxContainer;
			}
		}

		public override bool CanParent( Control control )
		{
			if( control is ToolBox )
				return false;
			else if( control is ToolBoxContainer )
				return false;
			else
                return base.CanParent( control );
		}

		public override SelectionRules SelectionRules
		{
			get
			{
				if( this.IsContained )
					return SelectionRules.TopSizeable | SelectionRules.BottomSizeable | SelectionRules.Visible;
				else
					return base.SelectionRules;
			}
		}

		public override DesignerActionListCollection ActionLists
		{
		    get
		    {
				if( this.IsContained )
					return base.ActionLists;

				if( this.actionLists == null )
		        {
		            this.actionLists = new DesignerActionListCollection();
		            this.actionLists.Add( new ToolBoxActionList( this.Component ) );
		        }
		        return this.actionLists;
		    }
		}

        public override IList SnapLines
        {
            get
            {
                ArrayList snapLines = (ArrayList) base.SnapLines;
                this.AddPaddingSnapLines( ref snapLines );
                return snapLines;
            }
        }

		internal class ToolBoxActionList : DesignerActionList
		{
			public ToolBoxActionList( IComponent component )
				: base( component )
			{
			}

			public void ToggleDockStyle()
			{
			    DockStyle dock = (DockStyle) DesignerExtras.GetProperty( this.Component, "Dock" );
				if( dock != DockStyle.None )
					DesignerExtras.SetProperty( this.Component, "Dock", DockStyle.None );
				else
					DesignerExtras.SetProperty( this.Component, "Dock", DockStyle.Fill );
			}

			private string GetDockStyleText()
			{
			    DockStyle dock = (DockStyle) DesignerExtras.GetProperty( this.Component, "Dock" );
				if( dock != DockStyle.None )
					return "Undock in parent container";
				else
					return "Dock in parent container";
			}

			public override DesignerActionItemCollection GetSortedActionItems()
			{
				DesignerActionItemCollection items = new DesignerActionItemCollection();
				items.Add( new DesignerActionMethodItem( this, "ToggleDockStyle", this.GetDockStyleText() ) );
				return items;
			}
		}
	}
}
