﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using Twp.Utilities;

namespace Twp.Controls.Design
{
    public class ColumnCollectionEditor : CollectionEditor
    {
        public ColumnCollectionEditor(Type type)
            : base(type)
        {
        }

        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        protected override Type CreateCollectionItemType()
        {
            return base.CreateCollectionItemType();
        }

        protected override object CreateInstance(Type itemType)
        {
            TreeListView owner = this.Context.Instance as TreeListView;
            // create new default fieldname
            string fieldname;
            string caption;
            int cnt = owner.Columns.Count;
            do
            {
                cnt++;
                fieldname = "fieldname" + cnt.ToString();
                caption = "Column_" + cnt.ToString();
            }
            while (owner.Columns[fieldname] != null);
            return new TreeListColumn(fieldname, caption);
        }

        protected override string GetDisplayText(object value)
        {
            TreeListColumn col = (TreeListColumn)value;
            if (col.Caption.Length > 0)
                return string.Format("{0} (\"{1}\")", col.FieldName, col.Caption);
            return base.GetDisplayText(value);
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (provider == null)
                throw new ArgumentNullException("provider");
            if (value == null)
                throw new ArgumentNullException("value");

            object result = base.EditValue(context, provider, value);
            TreeListView owner = this.Context.Instance as TreeListView;
            if (owner != null)
                owner.Invalidate();
            return result;
        }
    }

    internal class ColumnConverter : ExpandableObjectConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(InstanceDescriptor))
                return true;
            else
                return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            TreeListColumn col = value as TreeListColumn;
            if (col != null && destinationType == typeof(InstanceDescriptor))
            {
                if (col.Width != 50)
                {
                    ConstructorInfo cinfo = typeof(TreeListColumn).GetConstructor(new Type[] { typeof(string), typeof(string), typeof(int) });
                    return new InstanceDescriptor(cinfo, new object[] { col.FieldName, col.Caption, col.Width }, false);
                }
                else if (col.AutoSize)
                {
                    ConstructorInfo cinfo = typeof(TreeListColumn).GetConstructor(new Type[] { typeof(string), typeof(string), typeof(bool) });
                    return new InstanceDescriptor(cinfo, new object[] { col.FieldName, col.Caption, col.AutoSize }, false);
                }
                else
                {
                    ConstructorInfo cinfo = typeof(TreeListColumn).GetConstructor(new Type[] { typeof(string), typeof(string) });
                    return new InstanceDescriptor(cinfo, new object[] { col.FieldName, col.Caption }, false);
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    internal class ColumnsTypeConverter : ExpandableObjectConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(TreeListColumnCollection))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
                return "(Columns Collection)";
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    internal class TreeListViewDesigner : ControlDesigner, ITypeDescriptorContext, IWindowsFormsEditorService
    {
        private IDesignerHost hostSvc;
        private IComponentChangeService compSvc;
        private PropertyDescriptor columnsPD;
        private DesignerActionListCollection actionLists;
        private DesignerVerbCollection verbs;

        public override void Initialize(IComponent component)
        {
            base.Initialize(component);

            this.hostSvc = (IDesignerHost)this.GetService(typeof(IDesignerHost));
            this.compSvc = (IComponentChangeService)this.GetService(typeof(IComponentChangeService));
            if (this.compSvc != null)
                this.compSvc.ComponentChanged += this.OnComponentChanged;
            this.columnsPD = TypeDescriptor.GetProperties(component)["Columns"];

            TreeListView tree = this.Control as TreeListView;
            if (tree != null)
                tree.AfterResizeColumn += this.OnAfterResizingColumn;
        }

        private void OnAfterResizingColumn(object sender, MouseEventArgs e)
        {
            this.RaiseComponentChanged(null, null, null);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private void OnComponentChanged(object sender, ComponentChangedEventArgs e)
        {
            if (this.Control != null && !this.Control.IsDisposed)
                this.Control.Invalidate();
        }

        protected override bool GetHitTest(Point point)
        {
            TreeListView tree = this.Control as TreeListView;
            if (tree == null)
                return false;

            point = tree.PointToClient(point);

            Node node = tree.CalcHitNode(point);
            if (node != null)
                return true;

            HitInfo info = tree.CalcColumnHit(point);
            if (Flag.IsSet(info.HitType, HitInfo.HitTypes.ColumnHeader))
                return true;

            if (tree.HitTestScrollbar(point))
                return true;

            return base.GetHitTest(point);
        }

        protected override void PostFilterProperties(IDictionary properties)
        {
            properties.Remove("Text");
            base.PostFilterProperties(properties);
        }

        private void OnEditColumns(object sender, EventArgs e)
        {
            UITypeEditor editor = (UITypeEditor)this.columnsPD.GetEditor(typeof(UITypeEditor));
            editor.EditValue(this, this, ((TreeListView)this.Control).Columns);
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                if (this.verbs == null)
                {
                    this.verbs = new DesignerVerbCollection();
                    DesignerVerb verb = new DesignerVerb("Edit Columns...", this.OnEditColumns);
                    verb.Description = "Edit the columns for this control.";
                    this.verbs.Add(verb);
                }
                return this.verbs;
            }
        }

        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (this.actionLists == null)
                {
                    this.actionLists = new DesignerActionListCollection();
                    this.actionLists.Add(new TreeListViewActionList(this.Component));
                }
                return this.actionLists;
            }
        }

        #region ITypeDescriptorContext

        public IContainer Container
        {
            get { return this.Component.Site.Container; }
        }

        public object Instance
        {
            get { return this.Component; }
        }

        public PropertyDescriptor PropertyDescriptor
        {
            get { return this.columnsPD; }
        }

        public bool OnComponentChanging()
        {
            this.compSvc.OnComponentChanging(this.Component, this.columnsPD);
            return true;
        }

        public void OnComponentChanged()
        {
            object value = ((TreeListView)this.Component).Columns;
            this.compSvc.OnComponentChanged(this.Component, this.columnsPD, value, value);
        }

        #endregion

        #region IServiceProvider

        object IServiceProvider.GetService(Type serviceType)
        {
            if (serviceType.Equals(typeof(IWindowsFormsEditorService)))
                return this;
            else
                return this.hostSvc.GetService(serviceType);
        }

        #endregion

        #region IWindowsFormsEditorService

        void IWindowsFormsEditorService.CloseDropDown()
        {
            throw new NotImplementedException();
        }

        void IWindowsFormsEditorService.DropDownControl(Control control)
        {
            throw new NotImplementedException();
        }

        DialogResult IWindowsFormsEditorService.ShowDialog(Form dialog)
        {
            return dialog.ShowDialog();
        }

        #endregion

        internal class TreeListViewActionList : DesignerActionList, ITypeDescriptorContext, IWindowsFormsEditorService
        {
            private IDesignerHost hostSvc;
            private IComponentChangeService compSvc;
            private PropertyDescriptor columnsPD;

            public TreeListViewActionList(IComponent component)
                : base(component)
            {
                this.hostSvc = component.Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
                this.compSvc = component.Site.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
                this.columnsPD = TypeDescriptor.GetProperties(component)["Columns"];
            }

            public override DesignerActionItemCollection GetSortedActionItems()
            {
                DesignerActionItemCollection items = new DesignerActionItemCollection();
                items.Add(new DesignerActionMethodItem(this, "EditColumns", "Edit columns..."));
                items.Add(new DesignerActionMethodItem(this, "ToggleDockStyle", this.GetDockStyleText()));
                return items;
            }

            public void EditColumns()
            {
                UITypeEditor editor = (UITypeEditor)this.columnsPD.GetEditor(typeof(UITypeEditor));
                editor.EditValue(this, this, ((TreeListView)this.Component).Columns);
            }

            public void ToggleDockStyle()
            {
                DockStyle dock = (DockStyle)DesignerExtras.GetProperty(this.Component, "Dock");
                if (dock != DockStyle.None)
                    DesignerExtras.SetProperty(this.Component, "Dock", DockStyle.None);
                else
                    DesignerExtras.SetProperty(this.Component, "Dock", DockStyle.Fill);
            }

            private string GetDockStyleText()
            {
                DockStyle dock = (DockStyle)DesignerExtras.GetProperty(this.Component, "Dock");
                if (dock != DockStyle.None)
                    return "Undock in parent container";
                else
                    return "Dock in parent container";
            }

            #region ITypeDescriptorContext

            public IContainer Container
            {
                get { return this.Component.Site.Container; }
            }

            public object Instance
            {
                get { return this.Component; }
            }

            public PropertyDescriptor PropertyDescriptor
            {
                get { return this.columnsPD; }
            }

            public bool OnComponentChanging()
            {
                this.compSvc.OnComponentChanging(this.Component, this.columnsPD);
                return true;
            }

            public void OnComponentChanged()
            {
                object value = ((TreeListView)this.Component).Columns;
                this.compSvc.OnComponentChanged(this.Component, this.columnsPD, value, value);
            }

            #endregion

            #region IServiceProvider

            object IServiceProvider.GetService(Type serviceType)
            {
                if (serviceType.Equals(typeof(IWindowsFormsEditorService)))
                    return this;
                else
                    return this.hostSvc.GetService(serviceType);
            }

            #endregion

            #region IWindowsFormsEditorService

            void IWindowsFormsEditorService.CloseDropDown()
            {
                throw new NotImplementedException();
            }

            void IWindowsFormsEditorService.DropDownControl(Control control)
            {
                throw new NotImplementedException();
            }

            DialogResult IWindowsFormsEditorService.ShowDialog(Form dialog)
            {
                return dialog.ShowDialog();
            }

            #endregion
        }
    }
}
