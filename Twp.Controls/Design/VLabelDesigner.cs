﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;

namespace Twp.Controls.Design
{
	internal class VLabelDesigner : ControlDesigner
	{
		private VLabel Label
		{
			get { return (VLabel) this.Control; }
		}

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
		}

		public override SelectionRules SelectionRules
		{
			get
			{
				if( this.Label.AutoSize )
					return SelectionRules.Moveable | SelectionRules.Visible;
				else
					return base.SelectionRules;
			}
		}

		public override IList SnapLines
		{
			get
			{
				IList snapLines = base.SnapLines;
				snapLines.Add( new SnapLine( SnapLineType.Baseline, this.Label.Height - 3 ) );
				return snapLines;
			}
		}
	}
}
