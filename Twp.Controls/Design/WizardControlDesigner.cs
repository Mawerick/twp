﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
    public class WizardControlDesigner : ParentControlDesigner
    {
        #region Fields
        private DesignerVerbCollection verbs = null;
        private DesignerVerb addVerb = null;
        private DesignerVerb removeVerb = null;
        #endregion

        #region Properties
        protected WizardControl WizardControl
        {
            get { return this.Control as WizardControl; }
        }

        public override bool ParticipatesWithSnapLines
        {
            get
            {
                bool result;
                WizardPageDesigner designer = this.GetSelectedPageDesigner();
                if( designer != null )
                    result = designer.ParticipatesWithSnapLines;
                else
                    result = true;
                
                return result;
            }
        }

        public override DesignerVerbCollection Verbs
        {
            get
            {
                if( this.verbs == null )
                {
                    this.verbs = new DesignerVerbCollection();
                    this.addVerb = new DesignerVerb( "Add page", this.OnAddPage );
                    this.addVerb.Description = "Add a new WizardPage to the parent controler.";
                    this.removeVerb = new DesignerVerb( "Remove page", this.OnRemovePage );
                    this.removeVerb.Description = "Remove the currently selected WizardPage from the parent control.";
                    this.verbs.Add( this.addVerb );
                    this.verbs.Add( this.removeVerb );
                }
                return this.verbs;
            }
        }
        #endregion

        #region Initialize & dispose
        public override void Initialize( IComponent component )
        {
//                WizardControl wizardControl;
            
            base.Initialize( component );
            
            ISelectionService ss = (ISelectionService) this.GetService( typeof( ISelectionService ) );
            if( ss != null )
                ss.SelectionChanged += this.OnSelectionChanged;
            
            IComponentChangeService ccs = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
            if( ccs != null )
                ccs.ComponentChanged += this.OnComponentChanged;
            
//                wizardControl = component as WizardControl;
//                if( wizardControl != null )
        }
                    
        public override void InitializeNewComponent(System.Collections.IDictionary defaultValues)
        {
            base.InitializeNewComponent( defaultValues );
            
            this.AddWizardPage( WizardPageStyle.Welcome );
            this.AddWizardPage( WizardPageStyle.Standard );
            this.AddWizardPage( WizardPageStyle.Finish );
            this.WizardControl.SelectedIndex = 0;
        }
        
        protected override void Dispose(bool disposing)
        {
            if( disposing )
            {
//                    WizardControl wizardControl;
                
                ISelectionService ss = (ISelectionService) this.GetService( typeof( ISelectionService ) );
                if( ss != null )
                    ss.SelectionChanged -= this.OnSelectionChanged;
                
                IComponentChangeService ccs = (IComponentChangeService) this.GetService( typeof( IComponentChangeService ) );
                if( ccs != null )
                    ccs.ComponentChanged -= this.OnComponentChanged;
            
//                    wizardControl = component as WizardControl;
//                    if( wizardControl != null )
            }
            base.Dispose(disposing);
        }
        #endregion
        
        #region Event handlers
        private void OnSelectionChanged( object sender, EventArgs e )
        {
            ISelectionService ss = (ISelectionService) this.GetService( typeof( ISelectionService  ));
            if( ss != null )
            {
                WizardControl control = this.WizardControl;
                foreach( object component in ss.GetSelectedComponents() )
                {
                    WizardPage ownedPage = this.GetComponentOwner( component );
                    if( ownedPage != null && ownedPage.Parent == control )
                    {
                        control.SelectedPage = ownedPage;
                        break;
                    }
                }
            }
        }
        
        private void OnComponentChanged( object sender, ComponentChangedEventArgs e )
        {
            ISelectionService ss = (ISelectionService) this.GetService( typeof( ISelectionService ) );
            if( ss != null )
            {
                ss.SetSelectedComponents( new object[] { this.Control } );
            }
        }

        private void OnAddPage( object sender, EventArgs e )
        {
            this.AddWizardPage( WizardPageStyle.Standard );
        }
        
        private void OnRemovePage( object sender, EventArgs e )
        {
            this.RemoveSelectedWizardPage();
        }
        #endregion
        
        #region Methods
        protected virtual void AddWizardPage( WizardPageStyle style )
        {
            WizardControl control = this.WizardControl;
            IDesignerHost host = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
            if( host != null )
            {
                using( DesignerTransaction transaction = host.CreateTransaction( String.Format( "Add WizardPage to '{0}'", control.Name ) ) )
                {
                    try
                    {
                        WizardPage page = (WizardPage) host.CreateComponent( typeof( WizardPage ) );
                        MemberDescriptor controlsProperty = TypeDescriptor.GetProperties( control )["Controls"];
                        
                        this.RaiseComponentChanging( controlsProperty );
                        
                        page.Title = page.Name;
                        page.Style = style;
                        
                        control.Controls.Add( page );
                        control.Pages.Add( page );
                        control.SelectedIndex = control.Pages.Count - 1;

                        this.RaiseComponentChanged( controlsProperty, null, null );
                        
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Cancel();
                        throw;
                    }
                }
            }
        }
        
        protected virtual void RemoveSelectedWizardPage()
        {
            WizardControl control = this.WizardControl;
            if( control != null && control.Pages.Count != 0 )
            {
                IDesignerHost host = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
                if( host != null )
                {
                    using( DesignerTransaction transaction = host.CreateTransaction( String.Format( "Remove WizardPage from '{0}'", control.Name ) ) )
                    {
                        try
                        {
                            MemberDescriptor controlsProperty = TypeDescriptor.GetProperties( control )["Controls"];
                            
                            this.RaiseComponentChanging( controlsProperty );
                            
                            WizardPage page = control.SelectedPage;
                            int index = control.SelectedIndex;
                            control.Pages.Remove( page );
                            host.DestroyComponent( page );
                            if( index == control.Pages.Count )
                                control.SelectedIndex = index - 1;
                            
                            this.RaiseComponentChanged( controlsProperty, null, null );
                            
                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Cancel();
                            throw;
                        }
                    }
                }
            }
        }
        
        protected override IComponent[] CreateToolCore( System.Drawing.Design.ToolboxItem tool, int x, int y, int width, int height, bool hasLocation, bool hasSize )
        {
            WizardControl control = this.WizardControl;
            
            if( control.SelectedPage == null )
                throw new ArgumentException( String.Format( "Cannot add control '{0}', no page is selected.", tool.DisplayName ) );
            
            IDesignerHost host = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
            if( host != null )
            {
                ParentControlDesigner childDesigner = (ParentControlDesigner) host.GetDesigner( control.SelectedPage );
                ParentControlDesigner.InvokeCreateTool( childDesigner, tool );
            }

            return null;
        }
                
        public override bool CanParent( Control control )
        {
            return control is WizardControl && !this.Control.Contains( control );
        }
//
//        protected override bool GetHitTest( Point point )
//        {
////            WizardControl control = this.WizardControl;
////            Point location = control.PointToClient( point );
//            return this.WizardControl.HitTest( point );
//        }
        
        private WizardPage GetComponentOwner( object component )
        {
            WizardPage result;
            
            if( component is Control )
            {
                Control parent = (Control)component;
                while( parent != null && !(parent is WizardPage) )
                    parent = parent.Parent;
                
                result = (WizardPage) parent;
            }
            else
                result = null;
            
            return result;
        }
        
        private WizardPageDesigner GetSelectedPageDesigner()
        {
            WizardPageDesigner designer = null;
            WizardPage selectedPage = this.WizardControl.SelectedPage;
            
            if( selectedPage != null )
            {
                IDesignerHost host = (IDesignerHost) this.GetService( typeof( IDesignerHost ) );
                if( host != null )
                    designer = host.GetDesigner( selectedPage ) as WizardPageDesigner;
            }
            
            return designer;
        }

        protected override void WndProc( ref Message msg )
        {
            if( msg.Msg == NativeMethods.WM_LBUTTONDOWN || msg.Msg == NativeMethods.WM_LBUTTONDBLCLK )
            {
                ISelectionService ss = (ISelectionService) this.GetService( typeof( ISelectionService ) );
                if( ss.PrimarySelection is WizardControl )
                {
                    WizardControl wizard = (WizardControl) ss.PrimarySelection;

                    int xPos = (short)((uint) msg.LParam & 0x0000FFFF);
                    int yPos = (short)(((uint) msg.LParam & 0xFFFF0000) >> 16);
                    Point mousePos = new Point( xPos, yPos );

                    wizard.HitTest( mousePos, msg.HWnd );
//                    if( msg.HWnd == wizard.nextButton.Handle )
//                    {
//                        if( wizard.nextButton.Enabled && wizard.nextButton.ClientRectangle.Contains( mousePos ) )
//                            wizard.Next();
//                    }
//                    else if( msg.HWnd == wizard.backButton.Handle )
//                    {
//                        if( wizard.backButton.Enabled && wizard.backButton.ClientRectangle.Contains( mousePos ) )
//                            wizard.Back();
//                    }
                    return;
                }
            }

            base.WndProc( ref msg );
        }
        #endregion
    }
}
