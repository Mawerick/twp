﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms.Design.Behavior;

namespace Twp.Controls.Design
{
	/// <summary>
	/// Description of WizardPageDesigner.
	/// </summary>
	internal class WizardPageDesigner : ParentControlDesigner
	{
		public override void Initialize(IComponent component)
		{
			base.Initialize( component );
		}
		
		public override SelectionRules SelectionRules
		{
			get { return SelectionRules.Visible | SelectionRules.Locked; }
		}
		
		public override IList SnapLines
		{
			get
			{
				IList snapLines = base.SnapLines;
				
				WizardPage page = this.Control as WizardPage;
				if( page == null )
					return snapLines;
				
				snapLines.Add( new SnapLine( SnapLineType.Top, WizardPage.HeaderAreaHeight ) );

				if( page.Style == WizardPageStyle.Welcome || page.Style == WizardPageStyle.Finish )
					snapLines.Add( new SnapLine( SnapLineType.Left, WizardPage.WelcomeGlyphSize ) );

				return snapLines;
			}
		}
		
		public override bool CanBeParentedTo(IDesigner parentDesigner)
		{
			return parentDesigner != null && parentDesigner.Component is WizardControl;
		}
	}
}
