﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Reflection;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Represents a Dialog that shows an <see cref="Exception"/> message with stack trace.
    /// </summary>
    public partial class ExceptionDialog : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionDialog"/> class with the specified exception.
        /// </summary>
        public ExceptionDialog( Exception ex )
        {
            InitializeComponent();
            this.Text = Assembly.GetEntryAssembly().GetName().Name;
            this.Exception = ex;
        }

        /// <summary>
        /// Gets or sets the <see cref="Exception"/> whose message and stack trace is shown.
        /// </summary>
        public Exception Exception
        {
            get { return this.exception; }
            set
            {
                if( this.exception != value )
                {
                    this.exception = value;
                    this.errorLabel.Text = this.exception.Message;
                    this.detailsBox.Text = this.exception.StackTrace;
                }
            }
        }
        private Exception exception = null;

        /// <summary>
        /// Gets or sets the text displayed above the exception.
        /// </summary>
        public string Title
        {
            get { return this.titleLabel.Text; }
            set { this.titleLabel.Text = value; }
        }

        /// <summary>
        /// Displays a <see cref="ExceptionDialog"/> with the specified <see cref="Exception"/>.
        /// </summary>
        /// <param name="ex">The <see cref="Exception"/> to display.</param>
        public static void Show( Exception ex )
        {
            using( ExceptionDialog ed = new ExceptionDialog( ex ) )
            {
                ed.ShowDialog();
            }
        }

        /// <summary>
        /// Displays a <see cref="ExceptionDialog"/> with the specified <see cref="Exception"/>.
        /// </summary>
        /// <param name="title">The text to show above the exception.</param>
        /// <param name="ex">The <see cref="Exception"/> to display.</param>
        public static void Show( string title, Exception ex )
        {
            using( ExceptionDialog ed = new ExceptionDialog( ex ) )
            {
                ed.Title = title;
                ed.ShowDialog();
            }
        }
    }
}
