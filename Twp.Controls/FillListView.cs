﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Twp.Controls
{
    [DefaultProperty( "StretchIndex" )]
    [ToolboxBitmap( typeof( ListView ) )]
    public class FillListView : ListView
    {
        [Category( "Behavior" )]
        [Description( "Specifies a column which will get stretched to fill out the with of the control. This will also disable column interaction. Set to -1 to disable." )]
        [DefaultValue( 0 )]
        public int StretchIndex
        {
            get { return this.stretchIndex; }
            set
            {
                if( this.stretchIndex != value )
                {
                    this.stretchIndex = value;
                    this.OnSizeChanged( EventArgs.Empty );
                }
            }
        }
        private int stretchIndex = 0;

        private HeaderControl headerControl = null;

        protected override void OnSizeChanged( EventArgs e )
        {
            base.OnSizeChanged( e );

            if( this.stretchIndex < 0 || this.stretchIndex >= this.Columns.Count )
                return;

            int extra = 0;
            for( int i = 0; i < this.Columns.Count; i++ )
            {
                if( i != this.stretchIndex )
                    extra += this.Columns[i].Width;
            }
            this.Columns[this.stretchIndex].Width = this.ClientSize.Width - extra;

            NativeMethods.ShowScrollBar( this.Handle, NativeMethods.SB_HORZ, false );
        }

        protected override void WndProc( ref Message msg )
        {
            if( this.stretchIndex == -1 )
                return;

            const int HDN_FIRST = (0 - 300);
            const int HDN_BEGINTRACKA = (HDN_FIRST - 6);
            const int HDN_BEGINTRACKW = (HDN_FIRST - 26);
            bool callBase = true;

            switch( msg.Msg )
            {
                case NativeMethods.WM_NOTIFY:
                    NativeMethods.NMHDR nmhdr = (NativeMethods.NMHDR) msg.GetLParam( typeof( NativeMethods.NMHDR ) );
                    switch( nmhdr.code )
                    {
                        case HDN_BEGINTRACKA:
                        case HDN_BEGINTRACKW:
                            msg.Result = (IntPtr) 1;
                            callBase = false;
                            break;
                    }
                    break;
            }

            if( callBase )
            {
                base.WndProc( ref msg );
            }
        }

        protected override void OnKeyDown( KeyEventArgs e )
        {
            if( this.stretchIndex == -1 )
                return;

            if( e.KeyValue == 107 && e.Modifiers == Keys.Control )
                e.Handled = true;
            else
                base.OnKeyDown( e );
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            headerControl = new HeaderControl( this );
        }

        private class HeaderControl : NativeWindow
        {
            private FillListView parentListView = null;

            public HeaderControl( FillListView flv )
            {
                parentListView = flv;

                //Get the header control handle
                IntPtr header = NativeMethods.SendMessage( flv.Handle, NativeMethods.LVM_GETHEADER, IntPtr.Zero, IntPtr.Zero );
                this.AssignHandle( header );
            }

            protected override void WndProc( ref Message msg )
            {
                if( this.parentListView.StretchIndex == -1 )
                    return;

                bool callBase = true;

                switch( msg.Msg )
                {
                    case NativeMethods.WM_LBUTTONDBLCLK:
                    case NativeMethods.WM_SETCURSOR:
                        msg.Result = (IntPtr) 1;
                        callBase = false;
                        break;
                }

                if( callBase )
                {
                    base.WndProc( ref msg );
                }
            }
        }
    }
}
