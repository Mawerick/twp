﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Description of FlagsListBox.
    /// </summary>
    [DefaultEvent( "ValueChanged" )]
    public class FlagsListBox : CheckedListBox
    {
        Container components = null;

        public FlagsListBox()
        {
            InitializeComponent();
        }

        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if( this.components != null )
                    this.components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code
        private void InitializeComponent()
        {
            //
            // FlagsListBox
            // 
            this.CheckOnClick = true;
        }
        #endregion
        
        [Category( "Behavior" )]
        [Description( "Should a Null value for the flags be displayed?" )]
        [DefaultValue( true )]
        public bool ShowNull
        {
            get { return this.showNull; }
            set { this.showNull = value; }
        }
        private bool showNull = true;
        
        [Category( "Behavior" )]
        [Description( "Should an All value for the flags be displayed?" )]
        [DefaultValue( true )]
        public bool ShowAll
        {
            get { return this.showAll; }
            set { this.showAll = value; }
        }
        private bool showAll = true;

        public FlagsListBoxItem Add( int v, string c )
        {
            FlagsListBoxItem item = new FlagsListBoxItem( v, c );
            this.Items.Add( item );
            return item;
        }

        public FlagsListBoxItem Add( FlagsListBoxItem item )
        {
            this.Items.Add( item );
            return item;
        }

        protected override void OnItemCheck( ItemCheckEventArgs e )
        {
            base.OnItemCheck( e );

            if( this.isUpdatingCheckStates )
                return;

            FlagsListBoxItem item = Items[e.Index] as FlagsListBoxItem;
            this.UpdateCheckedItems( item, e.NewValue );

            if( this.ValueChanged != null )
                this.ValueChanged( this, EventArgs.Empty );
        }

        protected void UpdateCheckedItems( int value )
        {
            this.isUpdatingCheckStates = true;

            for( int i = 0; i < this.Items.Count; i++ )
            {
                FlagsListBoxItem item = this.Items[i] as FlagsListBoxItem;
                if( item.Value == 0 )
                {
                    this.SetItemChecked( i, value == 0 );
                }
                else
                {
                    if( (item.Value & value) == item.Value )
                        this.SetItemChecked( i, true );
                    else
                        this.SetItemChecked( i, false );
                }
            }

            this.isUpdatingCheckStates = false;
        }

        protected void UpdateCheckedItems( FlagsListBoxItem composite, CheckState state )
        {
            if( composite.Value == 0 )
                UpdateCheckedItems( 0 );

            int sum = this.GetCurrentValue();

            if( state == CheckState.Unchecked )
                sum = sum & (~composite.Value);
            else
                sum |= composite.Value;

            UpdateCheckedItems( sum );
        }

        private bool isUpdatingCheckStates = false;

        public int GetCurrentValue()
        {
            int sum = 0;
            for( int i = 0; i < this.Items.Count; i++ )
            {
                if( this.GetItemChecked( i ) )
                {
                    FlagsListBoxItem item = this.Items[i] as FlagsListBoxItem;
                    sum |= item.Value;
                }
            }
            return sum;
        }

        Type enumType;
        Enum enumValue;

        private void FillEnumMembers()
        {
            this.Items.Clear();
            foreach( object val in Enum.GetValues( this.enumType ) )
            {
                int intVal = (int) Convert.ChangeType( val, typeof( int ) );
                string name = TypeDescriptor.GetConverter( this.enumType ).ConvertToString( val );
                if( !this.showNull && intVal == 0 )
                    continue;
                if( !this.showAll && name.ToLower() == "all" )
                    continue;
                this.Add( intVal, name );
            }
        }

        private void ApplyEnumValue()
        {
            int intVal = (int) Convert.ChangeType( this.enumValue, typeof( int ) );
            this.UpdateCheckedItems( intVal );
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Enum Value
        {
            get
            {
                object e = Enum.ToObject( this.enumType, this.GetCurrentValue() );
                return (Enum) e;
            }
            set
            {
                if( this.enumValue != value )
                {
                    this.enumValue = value;
                    this.enumType = value.GetType();
                    this.FillEnumMembers();
                    this.ApplyEnumValue();
                }
            }
        }
        
        [Category( "Behavior" )]
        [Description( "Occurs when the flag selection is changed" )]
        public event EventHandler ValueChanged;
    }

    public class FlagsListBoxItem
    {
        public FlagsListBoxItem( int value, string name )
        {
            this.value = value;
            this.name = name;
        }

        private int value;
        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        private string name;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public bool IsFlag
        {
            get
            {
                return ((this.value & (this.value - 1)) == 0);
            }
        }

        public bool IsMemberFlag( FlagsListBoxItem composite )
        {
            return (this.IsFlag && ((this.value & composite.value) == value));
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
