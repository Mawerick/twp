﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Twp.Controls
{
    public enum GradientMode
    {
        Diagonal,
        Horizontal,
        Vertical,
    };

    /// <summary>
    /// Description of GradientBrush.
    /// </summary>
    public class GradientBrush
    {
        public static LinearGradientBrush Create( Rectangle bounds, Color color1, Color color2, GradientMode mode )
        {
            Point end = Point.Empty;
            switch( mode )
            {
                case GradientMode.Diagonal:
                    end = new Point( bounds.Right, bounds.Bottom );
                    break;
                case GradientMode.Horizontal:
                    end = new Point( bounds.Right, bounds.Top );
                    break;
                case GradientMode.Vertical:
                    end = new Point( bounds.Left, bounds.Bottom );
                    break;
            }
            return new LinearGradientBrush( bounds.Location, end, color1, color2 );
        }
    }
}
