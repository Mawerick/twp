﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace Twp.Controls
{
    public enum HorizontalLayoutStyle
    {
        LeftToRight,
        RightToLeft,
        Fill,
        Spaced,
    }

    public enum VerticalLayoutAlignment
    {
        Top,
        Bottom,
        Center,
        Stretch,
    }

    public class HorizontalLayout : LayoutEngine
    {
        #region Constructors

        public HorizontalLayout() : base()
        {
            this.contentOffset = new Point( 0, 0 );
        }

        public HorizontalLayout( Point contentOffset ) : base()
        {
            this.contentOffset = contentOffset;
        }

        #endregion

        #region Private Fields

        private int width = 0;
        private int spacing = 3;
        private bool autoSpacing = true;
        private bool includeHidden = false;
        private HorizontalLayoutStyle layoutStyle = HorizontalLayoutStyle.LeftToRight;
        private VerticalLayoutAlignment verticalAlign = VerticalLayoutAlignment.Top;
        private Point contentOffset;
        private Hashtable originalSize = new Hashtable();

        #endregion

        #region Properties

        /// <summary>
        /// Returns a value equal to the full width used by the controls in the container.
        /// </summary>
        public int Width
        {
            get { return this.width; }
        }

        /// <summary>
        /// Gets or sets a value representing the width of the space between controls.
        /// </summary>
        public int Spacing
        {
            get { return this.spacing; }
            set { this.spacing = value; }
        }

        /// <summary>
        /// Gets or sets whether or not the spacing should be automatic (uses a control's left or right margin, whichever it higher).
        /// </summary>
        public bool AutoSpacing
        {
            get { return this.autoSpacing; }
            set { this.autoSpacing = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not hidden controls are included by the LayoutEngine.
        /// </summary>
        public bool IncludeHidden
        {
            get { return this.includeHidden; }
            set { this.includeHidden = value; }
        }

        /// <summary>
        /// Gets or sets a value representing the style used for the layout.
        /// </summary>
        public HorizontalLayoutStyle LayoutStyle
        {
            get { return this.layoutStyle; }
            set { this.layoutStyle = value; }
        }

        /// <summary>
        /// Gets or sets a value representing the vertical alignment use for the layout.
        /// </summary>
        public VerticalLayoutAlignment VerticalAlign
        {
            get { return this.verticalAlign; }
            set { this.verticalAlign = value; }
        }

        #endregion

        #region Methods

        public override bool Layout( object container, System.Windows.Forms.LayoutEventArgs layoutEventArgs )
        {
            if( container == null )
                throw new ArgumentNullException( "container" );

            Control parent = container as Control;
            if( parent == null )
                throw new ArgumentException( "container is not a control object!", "container" );

            foreach( Control control in parent.Controls )
            {
                if( !this.originalSize.Contains( control ) )
                {
                    this.originalSize.Add( control, control.Size );
                    control.Resize += delegate
                    {
                        this.originalSize[control] = control.Size;
                    };
                }
            }

            switch( this.layoutStyle )
            {
                case HorizontalLayoutStyle.LeftToRight:
                    this.LayoutLeftToRight( parent );
                    break;

                case HorizontalLayoutStyle.RightToLeft:
                    this.LayoutRightToLeft( parent );
                    break;

                case HorizontalLayoutStyle.Fill:
                    this.LayoutFill( parent );
                    break;

                case HorizontalLayoutStyle.Spaced:
                    this.LayoutSpaced( parent );
                    break;

                default:
                    throw new Exception( "Invalid HorizontalLayoutStyle value: " + this.layoutStyle.ToString() );
            }

            return true;
        }

        private void LayoutLeftToRight( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( parent.Padding.Left, parent.Padding.Top );
            nextLocation.Offset( this.contentOffset );

            int lastMargin = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                
                if( this.originalSize.Contains( control ) )
                    control.Size = (Size) this.originalSize[control];

                if( this.autoSpacing && lastMargin != 0 )
                {
                    if( lastMargin < control.Margin.Left )
                        nextLocation.X += control.Margin.Left - lastMargin;
                }

                nextLocation.Y = this.GetAlignmentOffset( control, parent );
                control.Location = nextLocation;

                if( this.autoSpacing )
                {
                    nextLocation.X += control.Width + control.Margin.Right;
                    lastMargin = control.Margin.Right;
                }
                else
                    nextLocation.X += control.Width + this.spacing;
            }

            this.width = nextLocation.X + parent.Padding.Right;
        }

        private void LayoutRightToLeft( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( pClientRect.Width - parent.Padding.Right, parent.Padding.Top );
            nextLocation.Offset( this.contentOffset );

            int lastMargin = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                
                if( this.originalSize.Contains( control ) )
                    control.Size = (Size) this.originalSize[control];

                if( this.autoSpacing && lastMargin != 0 )
                {
                    if( lastMargin < control.Margin.Right )
                        nextLocation.X -= control.Margin.Right - lastMargin;
                }

                nextLocation.Y = this.GetAlignmentOffset( control, parent );
                nextLocation.X -= control.Width;
                control.Location = nextLocation;

                if( this.autoSpacing )
                {
                    nextLocation.X -= control.Margin.Left;
                    lastMargin = control.Margin.Left;
                }
                else
                    nextLocation.X -= this.spacing;
            }

            this.width = pClientRect.Width - (nextLocation.X + parent.Padding.Left);
        }
        
        private void LayoutFill( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( parent.Padding.Left, parent.Padding.Top );
            nextLocation.Offset( this.contentOffset );
            
            int count = 0;
            int staticWidth = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                                
                if( this.originalSize.Contains( control ) )
                    control.Size = (Size) this.originalSize[control];

                if( control.AutoSize )
                    staticWidth += control.Width + this.spacing;
                else
                    count++;
            }

            int width = pClientRect.Width - parent.Padding.Horizontal - this.contentOffset.X - staticWidth;
            width -= this.spacing;
            width /= count;

            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;

                nextLocation.Y = this.GetAlignmentOffset( control, parent );
                control.Location = nextLocation;
                
                if( !control.AutoSize )
                    control.Width = width;

                nextLocation.X += control.Width + this.spacing;
            }

            this.width = nextLocation.X + parent.Padding.Right;
        }
        
        private void LayoutSpaced( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( parent.Padding.Left, parent.Padding.Top );
            nextLocation.Offset( this.contentOffset );
            
            int count = 0;
            int staticWidth = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                                
                if( this.originalSize.Contains( control ) )
                    control.Size = (Size) this.originalSize[control];

                if( control.AutoSize )
                    staticWidth += control.Width + this.spacing;
                else
                    count++;
            }

            int width = pClientRect.Width - parent.Padding.Horizontal - this.contentOffset.X - staticWidth;
            width -= this.spacing;
            width /= count;

            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;

                nextLocation.Y = this.GetAlignmentOffset( control, parent );
                control.Location = nextLocation;
                
                if( !control.AutoSize )
                    control.Width = width;

                nextLocation.X += control.Width + this.spacing;
            }

            this.width = nextLocation.X + parent.Padding.Right;
        }
        
        private int GetAlignmentOffset( Control control, Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            switch( this.verticalAlign )
            {
                case VerticalLayoutAlignment.Bottom:
                    return pClientRect.Height - control.Height - parent.Padding.Bottom;

                case VerticalLayoutAlignment.Center:
                    return (pClientRect.Height - control.Height) / 2;
                    
                case VerticalLayoutAlignment.Stretch:
                    control.Height = pClientRect.Height - parent.Padding.Vertical;
                    return (pClientRect.Height - control.Height) / 2;
            }
            return parent.Padding.Top;
        }

        #endregion
    }
}
