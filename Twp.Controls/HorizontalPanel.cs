﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace Twp.Controls
{
    [Description( "Provides a Panel with a LayoutEngine that positions the content horizontally." )]
    [ToolboxBitmap( typeof( HorizontalPanel ) )]
    public class HorizontalPanel : Panel
    {
        public HorizontalPanel() : base()
        {
            this.layoutEngine = new HorizontalLayout();
        }

        /// <summary>
        /// Gets or sets a value representing the width of the space between controls.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Determins the spacing between controls. If AutoSpacing is true, or LayoutStyle is Fill, this value is ignored." )]
        [DefaultValue( typeof( uint ), "3" )]
        public uint Spacing
        {
            get { return (uint) this.layoutEngine.Spacing; }
            set
            {
                if( this.layoutEngine.Spacing != value )
                {
                    this.layoutEngine.Spacing = (int) value;
                    this.PerformLayout();
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets wether or not the spacing should be automatic (uses a control's left or right margin, whichever it higher).
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Determins whether or not spacing is automatically calculated.  If LayoutStyle is Fill, this value is ignored." )]
        [DefaultValue( true )]
        public bool AutoSpacing
        {
            get { return this.layoutEngine.AutoSpacing; }
            set
            {
                if( this.layoutEngine.AutoSpacing != value )
                {
                    this.layoutEngine.AutoSpacing = value;
                    this.PerformLayout();
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not hidden controls are included by the LayoutEngine.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Indicates whether or not hidden controls are included when controls are laid out." )]
        [DefaultValue( false )]
        public bool IncludeHidden
        {
            get { return this.layoutEngine.IncludeHidden; }
            set
            {
                if( this.layoutEngine.IncludeHidden != value )
                {
                    this.layoutEngine.IncludeHidden = value;
                    this.PerformLayout();
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value representing the style used for the layout.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Determins how the content is laid out horizontally." )]
        [DefaultValue( HorizontalLayoutStyle.LeftToRight )]
        public HorizontalLayoutStyle LayoutStyle
        {
            get { return this.layoutEngine.LayoutStyle; }
            set
            {
                if( this.layoutEngine.LayoutStyle != value )
                {
                    this.layoutEngine.LayoutStyle = value;
                    this.PerformLayout();
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value representing the style used for the layout.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Determins how the content is laid out vertically." )]
        [DefaultValue( VerticalLayoutAlignment.Top )]
        public VerticalLayoutAlignment VerticalAlign
        {
            get { return this.layoutEngine.VerticalAlign; }
            set
            {
                if( this.layoutEngine.VerticalAlign != value )
                {
                    this.layoutEngine.VerticalAlign = value;
                    this.PerformLayout();
                    this.Refresh();
                }
            }
        }

        private HorizontalLayout layoutEngine;
        public override LayoutEngine LayoutEngine
        {
            get { return this.layoutEngine; }
        }
    }
}
