﻿namespace Twp.Controls
{
	partial class HtmlEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( HtmlEditor ) );
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.boldToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.italicToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.underlineToolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.splitContainer = new System.Windows.Forms.SplitContainer();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.textBox = new Twp.Controls.InputBox();
			this.preview = new Twp.Controls.HtmlLabel();
			this.toolStrip.SuspendLayout();
			this.splitContainer.Panel1.SuspendLayout();
			this.splitContainer.Panel2.SuspendLayout();
			this.splitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripButton,
            this.toolStripSeparator,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator1,
            this.boldToolStripButton,
            this.italicToolStripButton,
            this.underlineToolStripButton,
            this.toolStripSeparator2} );
			this.toolStrip.Location = new System.Drawing.Point( 0, 0 );
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size( 575, 25 );
			this.toolStrip.TabIndex = 0;
			this.toolStrip.Text = "toolStrip";
			// 
			// saveToolStripButton
			// 
			this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.saveToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "saveToolStripButton.Image" ) ) );
			this.saveToolStripButton.Name = "saveToolStripButton";
			this.saveToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.saveToolStripButton.Text = "&Save";
			this.saveToolStripButton.Click += new System.EventHandler( this.OnSaveClicked );
			// 
			// toolStripSeparator
			// 
			this.toolStripSeparator.Name = "toolStripSeparator";
			this.toolStripSeparator.Size = new System.Drawing.Size( 6, 25 );
			// 
			// cutToolStripButton
			// 
			this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.cutToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "cutToolStripButton.Image" ) ) );
			this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.cutToolStripButton.Name = "cutToolStripButton";
			this.cutToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.cutToolStripButton.Text = "C&ut";
			this.cutToolStripButton.Click += new System.EventHandler( this.OnCutClicked );
			// 
			// copyToolStripButton
			// 
			this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.copyToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "copyToolStripButton.Image" ) ) );
			this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.copyToolStripButton.Name = "copyToolStripButton";
			this.copyToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.copyToolStripButton.Text = "&Copy";
			this.copyToolStripButton.Click += new System.EventHandler( this.OnCopyClicked );
			// 
			// pasteToolStripButton
			// 
			this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.pasteToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "pasteToolStripButton.Image" ) ) );
			this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.pasteToolStripButton.Name = "pasteToolStripButton";
			this.pasteToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.pasteToolStripButton.Text = "&Paste";
			this.pasteToolStripButton.Click += new System.EventHandler( this.OnPasteClicked );
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size( 6, 25 );
			// 
			// boldToolStripButton
			// 
			this.boldToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.boldToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "boldToolStripButton.Image" ) ) );
			this.boldToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.boldToolStripButton.Name = "boldToolStripButton";
			this.boldToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.boldToolStripButton.Text = "&Bold";
			this.boldToolStripButton.Click += new System.EventHandler( this.OnBoldClicked );
			// 
			// italicToolStripButton
			// 
			this.italicToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.italicToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "italicToolStripButton.Image" ) ) );
			this.italicToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.italicToolStripButton.Name = "italicToolStripButton";
			this.italicToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.italicToolStripButton.Text = "&Italic";
			this.italicToolStripButton.Click += new System.EventHandler( this.OnItalicClicked );
			// 
			// underlineToolStripButton
			// 
			this.underlineToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.underlineToolStripButton.Image = ( (System.Drawing.Image) ( resources.GetObject( "underlineToolStripButton.Image" ) ) );
			this.underlineToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.underlineToolStripButton.Name = "underlineToolStripButton";
			this.underlineToolStripButton.Size = new System.Drawing.Size( 23, 22 );
			this.underlineToolStripButton.Text = "&Underline";
			this.underlineToolStripButton.Click += new System.EventHandler( this.OnUnderlineClicked );
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size( 6, 25 );
			// 
			// splitContainer
			// 
			this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer.Location = new System.Drawing.Point( 0, 25 );
			this.splitContainer.Name = "splitContainer";
			this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer.Panel1
			// 
			this.splitContainer.Panel1.Controls.Add( this.textBox );
			// 
			// splitContainer.Panel2
			// 
			this.splitContainer.Panel2.Controls.Add( this.preview );
			this.splitContainer.Panel2.Controls.Add( this.statusStrip );
			this.splitContainer.Size = new System.Drawing.Size( 575, 276 );
			this.splitContainer.SplitterDistance = 164;
			this.splitContainer.TabIndex = 1;
			// 
			// statusStrip
			// 
			this.statusStrip.Location = new System.Drawing.Point( 0, 86 );
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size( 575, 22 );
			this.statusStrip.TabIndex = 1;
			this.statusStrip.Text = "statusStrip1";
			// 
			// textBox
			// 
			this.textBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox.Location = new System.Drawing.Point( 0, 0 );
			this.textBox.Multiline = true;
			this.textBox.Name = "textBox";
			this.textBox.Size = new System.Drawing.Size( 575, 164 );
			this.textBox.TabIndex = 0;
			this.textBox.EnterPressed += new System.EventHandler( this.OnEnterPressed );
			this.textBox.TextChanged += new System.EventHandler( this.OnTextChanged );
			// 
			// preview
			// 
			this.preview.BackColor = System.Drawing.SystemColors.Info;
			this.preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.preview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.preview.ForeColor = System.Drawing.SystemColors.InfoText;
			this.preview.Location = new System.Drawing.Point( 0, 0 );
			this.preview.Name = "preview";
			this.preview.Size = new System.Drawing.Size( 575, 86 );
			this.preview.TabIndex = 0;
			// 
			// HtmlEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 575, 301 );
			this.Controls.Add( this.splitContainer );
			this.Controls.Add( this.toolStrip );
			this.Name = "HtmlEditor";
			this.Text = "HtmlEditor";
			this.toolStrip.ResumeLayout( false );
			this.toolStrip.PerformLayout();
			this.splitContainer.Panel1.ResumeLayout( false );
			this.splitContainer.Panel1.PerformLayout();
			this.splitContainer.Panel2.ResumeLayout( false );
			this.splitContainer.Panel2.PerformLayout();
			this.splitContainer.ResumeLayout( false );
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer;
		private HtmlLabel preview;
		private InputBox textBox;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton saveToolStripButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
		private System.Windows.Forms.ToolStripButton cutToolStripButton;
		private System.Windows.Forms.ToolStripButton copyToolStripButton;
		private System.Windows.Forms.ToolStripButton pasteToolStripButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton boldToolStripButton;
		private System.Windows.Forms.ToolStripButton italicToolStripButton;
		private System.Windows.Forms.ToolStripButton underlineToolStripButton;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
	}
}