﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2012
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Twp.Controls
{
	/// <summary>
	/// Represents a simple HTML editor form.
	/// </summary>
	[DefaultProperty( "Html" )]
	public partial class HtmlEditor : Form
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="HtmlEditor"/> class.
		/// </summary>
		public HtmlEditor()
		{
			this.InitializeComponent();
		}

		/// <summary>
		/// Gets or sets the HTML code content of the input box.
		/// </summary>
		public string Html
		{
			get { return this.textBox.Text; }
			set { this.textBox.Text = value; }
		}

		private void OnSaveClicked( object sender, EventArgs e )
		{
			this.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Close();
		}

		private void OnCutClicked( object sender, EventArgs e )
		{
			this.textBox.Cut();
		}

		private void OnCopyClicked( object sender, EventArgs e )
		{
			this.textBox.Copy();
		}

		private void OnPasteClicked( object sender, EventArgs e )
		{
			this.textBox.Paste();
		}

		private void OnBoldClicked( object sender, EventArgs e )
		{
			this.AddTagToSelection( "strong" );
		}

		private void OnItalicClicked( object sender, EventArgs e )
		{
			this.AddTagToSelection( "i" );
		}

		private void OnUnderlineClicked( object sender, EventArgs e )
		{
			this.AddTagToSelection( "u" );
		}

		private void OnEnterPressed( object sender, EventArgs e )
		{
			int pos = this.textBox.SelectionStart;
			this.textBox.Text = this.textBox.Text.Insert( pos, "<br/>" );
			this.textBox.SelectionStart = pos + 5;
		}

		private void AddTagToSelection( string tag )
		{
			string text = this.textBox.Text;
			int end = this.textBox.SelectionStart + this.textBox.SelectionLength;
			text = text.Insert( end, "</" + tag + ">" );
			text = text.Insert( this.textBox.SelectionStart, "<" + tag + ">" );
			this.textBox.Text = text;
			this.textBox.SelectionStart = (end + tag.Length + 2); // place cursor at end of text inside tag
		}

		private void OnTextChanged( object sender, EventArgs e )
		{
			this.preview.Html = this.textBox.Text;
		}
	}
}
