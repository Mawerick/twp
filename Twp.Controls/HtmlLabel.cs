﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2012
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Xml;

namespace Twp.Controls
{
	/// <summary>
	/// Description of HtmlLabel.
	/// </summary>
	[DefaultProperty( "Html" )]
	[DefaultEvent( "LinkClicked" )]
	[Designer( typeof( HtmlLabelDesigner ), typeof( IDesigner ) )]
	public class HtmlLabel : UserControl
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="HtmlLabel"/> class.
		/// </summary>
		public HtmlLabel()
		{
			SetStyle( ControlStyles.OptimizedDoubleBuffer, true );
			SetStyle( ControlStyles.AllPaintingInWmPaint, true );
			SetStyle( ControlStyles.UserPaint, true );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "HtmlLabel";
		}

		/// <summary>
		/// Gets or sets the html content of the label.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The html text content of the label." )]
		[DefaultValue( "" )]
		[Editor( typeof( MultilineStringEditor ), typeof( UITypeEditor ) )]
		public string Html
		{
			get { return this.html; }
			set
			{
				if( this.html != value )
				{
					this.html = value;
					this.Invalidate();
				}
			}
		}
		private string html = String.Empty;

		/// <summary>
		/// Gets or sets the number of pixels to indent the content of a list.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The number of pixels to indent the content of a list." )]
		[DefaultValue( 15 )]
		public int ListIndent
		{
			get { return this.listIndent; }
			set
			{
				if( this.listIndent != value )
				{
					this.listIndent = value;
					this.Invalidate();
				}
			}
		}
		private int listIndent = 15;

		private int position = 0;
		private int verticalPosition = 0;

		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.MouseMove"/> event.
		/// </summary>
		/// <param name="e">A <see cref="System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
		protected override void OnMouseMove( MouseEventArgs e )
		{
			base.OnMouseMove( e );
			bool onLink = false;
			foreach( HtmlLabel.Link link in this.links )
			{
				if( link.Region.IsVisible( e.Location ) )
				{
					onLink = true;
				}
			}
			if( onLink == true )
				this.Cursor = Cursors.Hand;
			else
				this.Cursor = this.DefaultCursor;
		}

		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.MouseClick"/> event.
		/// </summary>
		/// <param name="e">A <see cref="System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
		protected override void OnMouseClick( MouseEventArgs e )
		{
			base.OnMouseClick( e );
			foreach( HtmlLabel.Link link in this.links )
			{
				if( link.Region.IsVisible( e.Location ) )
				{
					if( e.Button == MouseButtons.Left )
					{
						this.OnLinkClicked( new LinkClickedEventArgs( link.LinkText ) );
					}
					else if( e.Button == MouseButtons.Right )
					{
						this.hoverLink = link.LinkText;
						ContextMenu cm = new ContextMenu();
						cm.MenuItems.Add( "Copy link location", OnCopyLinkLocation );
						cm.Show( this, e.Location );
						cm.Dispose();
					}
					return;
				}
			}
		}

        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
        /// </summary>
        /// <param name="e">A <see cref="System.Windows.Forms.PaintEventArgs"/> that contains the event data.</param>
        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );

            this.position = this.Padding.Left;
            this.verticalPosition = this.Padding.Top;
            this.links.Clear();
            this.currentLink = null;

            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            HtmlParser parser = new HtmlParser( this.html, Font );
            foreach( XmlNode node in parser.DocumentElement.ChildNodes )
            {
                this.DrawNode( node, e.Graphics, this.ForeColor, this.Font, false );
            }
        }

		private void DrawNode( XmlNode node, Graphics g, Color currentColor, Font currentFont, bool inList )
		{
			Color color = currentColor;
			Font font = currentFont;
			bool nextLine = false;
			bool isList = inList;
			int indent = this.Padding.Left;
			string link = null;
			TextFormatFlags flags = TextFormatFlags.NoPadding;

			string text = HtmlParser.ParseNode( node, ref font, ref color, ref nextLine, ref isList, ref link );
			int stringHeight = (int) g.MeasureString( "|", font ).Height + 2;

            if( isList )
                indent += this.listIndent;

			if( isList && !inList )
			{
				this.position = indent;
				this.verticalPosition += stringHeight;
			}

            if( this.position == this.Padding.Left )
                this.position += indent;

			if( String.IsNullOrEmpty( text ) == false )
			{
				if( text == "<li>" )
				{
					int x = this.Padding.Left + ( this.listIndent / 2 );
					int y = this.verticalPosition + ( stringHeight / 2 ) + 1;

					Brush brush = new SolidBrush( currentColor );
					Point[] points = new Point[] {
						new Point( x - 2, y ),
						new Point( x, y - 2 ),
						new Point( x + 2, y ),
						new Point( x, y + 2 ),
					};
					g.FillPolygon( brush, points );
					brush.Dispose();
				}
				else
				{
					Rectangle clip = Rectangle.Truncate( this.ClientRectangle );
					string[] words = text.Split( new char[] { ' ' } );
					for( int n = 0; n < words.Length; ++n )
					{
						string word = words[n];
						if( n < words.Length - 1 )
							word += " ";
						int stringWidth = TextRenderer.MeasureText( g, word, font, clip.Size, flags ).Width;

						if( this.position + stringWidth > clip.Width - 4 )
						{
							this.position = indent;
							this.verticalPosition += stringHeight;
						}

						if( this.position <= clip.Right - 4 &&
							this.verticalPosition <= clip.Bottom &&
							this.verticalPosition >= clip.Top )
						{
							Rectangle rect = new Rectangle( this.position, this.verticalPosition,
															stringWidth, stringHeight );
							rect.Offset( 2, 2 );
							TextRenderer.DrawText( g, word, font, rect, color, flags );

							if( this.currentLink != null )
								this.currentLink.Region.Union( rect );
						}
						else
							Twp.Utilities.Log.Debug( "[HtmlLabel.DrawNode] Word: {0} Pos: {1} VPos: {2} Clip: {3}", word, this.position, this.verticalPosition, clip );


						this.position += (int) stringWidth;
					}
				}
			}

			if( String.IsNullOrEmpty( link ) == false )
			{
				this.currentLink = new HtmlLabel.Link( link );
			}

			if( node.HasChildNodes )
			{
				foreach( XmlNode child in node.ChildNodes )
				{
					DrawNode( child, g, color, font, isList );
				}
			}

			if( String.IsNullOrEmpty( link ) == false )
			{
				this.links.Add( this.currentLink );
				this.currentLink = null;
			}

			if( nextLine )
			{
				this.position = this.Padding.Left;
				this.verticalPosition += stringHeight;
				nextLine = false;
			}
		}

		protected override void OnResize( EventArgs e )
		{
			base.OnResize( e );
			this.Invalidate();
		}

		private List<HtmlLabel.Link> links = new List<HtmlLabel.Link>();
		private HtmlLabel.Link currentLink = null;
		private string hoverLink = null;

		/// <summary>
		/// Occurs when a link is clicked within the control.
		/// </summary>
		[Category( "Action" )]
		[Description( "Occurs when a link is clicked within the control." )]
		public event LinkClickedEventHandler LinkClicked;

		/// <summary>
		/// Raises the <see cref="LinkClicked"/> event.
		/// </summary>
		/// <param name="e">A <see cref="LinkClickedEventArgs"/> that contains the event data.</param>
		protected virtual void OnLinkClicked( LinkClickedEventArgs e )
		{
			if( this.LinkClicked != null )
				this.LinkClicked( this, e );
		}

		internal void OnCopyLinkLocation( object sender, EventArgs e )
		{
			Clipboard.SetText( this.hoverLink );
		}

		/// <summary>
		/// Represents a link within a <see cref="HtmlLabel"/> control.
		/// </summary>
		public class Link
		{
			/// <summary>
			/// Gets or sets the text associated with the link.
			/// </summary>
			public string LinkText
			{
				get { return this.linkText; }
				set { this.linkText = value; }
			}
			private string linkText;

			/// <summary>
			/// Gets the clipping region the link occupies.
			/// </summary>
			public Region Region
			{
				get { return this.region; }
			}
			private Region region;

			/// <summary>
			/// Initializes a new instance of the <see cref="HtmlLabel.Link"/> class with the specified link text.
			/// </summary>
			/// <param name="linkText">The associated link text.</param>
			public Link( string linkText )
			{
				this.linkText = linkText;
				this.region = new Region();
				this.region.MakeEmpty();
			}

			/// <summary>
			/// Initializes a new instance of the <see cref="HtmlLabel.Link"/> class with the specified link text and clipping region.
			/// </summary>
			/// <param name="linkText">The associated link text.</param>
			/// <param name="region">The clipping region the link occupies.</param>
			public Link( string linkText, Region region )
			{
				this.linkText = linkText;
				this.region = region;
			}
		}

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // HtmlLabel
            // 
            this.Name = "HtmlLabel";
            this.Size = new System.Drawing.Size(65, 26);
            this.ResumeLayout(false);

        }

	}

	class HtmlLabelDesigner : ControlDesigner
	{
		private HtmlLabel label;

		public HtmlLabelDesigner()
		{
		}

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
			this.label = (HtmlLabel) component;
		}

		public override SelectionRules SelectionRules
		{
			get
			{
				if( this.label.AutoSize == true )
				{
					return SelectionRules.Moveable | SelectionRules.Visible;
				}
				else
				{
					return SelectionRules.Moveable | SelectionRules.Visible | SelectionRules.AllSizeable;
				}
			}
		}
	}
}
