// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2012
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Xml;

namespace Twp.Controls
{
	/// <summary>
	/// Summary description for HtmlParser.
	/// </summary>
	public class HtmlParser : XmlDocument
	{
		private Color currentColor = Color.Black;
		private Font currentFont;

		/// <summary>
		/// Initializes a new instance of the <see cref="HtmlParser"/> class.
		/// </summary>
		/// <param name="html">The html-encoded text to display.</param>
		/// <param name="font">The base font to use.</param>
		public HtmlParser( string html, Font font )
		{
			try
			{
				this.currentFont = font;
				html = html.Replace( "\r", "" );
				html = html.Replace( "\n", "" );
				html = html.Replace( "&nbsp;", " " );
				TextReader reader = new System.IO.StringReader( "<root>" + html + "</root>" );
				this.Load( XmlReader.Create( reader ) );
			}
			catch( XmlException )
			{
				XmlElement body = this.CreateElement( "body" );
				XmlText text = this.CreateTextNode( html );
				body.AppendChild( text );
				this.AppendChild( body );
			}
		}

		/// <summary>
		/// Parses an <see cref="XmlNode"/> from the html input and extracts the style and content for drawing.
		/// </summary>
		/// <param name="node">The <see cref="XmlNode"/> to parse.</param>
		/// <param name="font">The font for this node.</param>
		/// <param name="color">The color for this node.</param>
		/// <param name="nextLine">True if a new line should be added to the output, otherwise, false.</param>
		/// <param name="isList">True if the node is part of a list, otherwise, false.</param>
		/// <param name="link">The link text of a link node.</param>
		/// <returns></returns>
		public static string ParseNode( XmlNode node, ref Font font, ref Color color, 
										ref bool nextLine, ref bool isList, ref string link )
		{
			if( node == null )
				return null;

			FontFamily fontFamily = font.FontFamily;
			FontStyle fontStyle = font.Style;
			float fontSize = font.SizeInPoints;

			string text = String.Empty;

			switch( node.Name.ToLowerInvariant() )
			{
				case "body":
				case "span":
					// These are ignored here. If they contain "style" attributes,
					// that will be parsed below.
					break;

				case "#text":
					return node.InnerText;

				case "br":
					nextLine = true;
					break;

				case "b":
				case "strong":
					fontStyle |= FontStyle.Bold;
					break;

				case "i":
				case "em":
				case "italic":
					fontStyle |= FontStyle.Italic;
					break;

				case "u":
					fontStyle |= FontStyle.Underline;
					break;

				case "s":
				case "strike":
					fontStyle |= FontStyle.Strikeout;
					break;

				case "ul":
					nextLine = true;
					isList = true;
					break;

				case "li":
					text = "<li>";
					nextLine = true;
					break;

				case "a":
					fontStyle |= FontStyle.Underline;
					color = Color.Blue;
					link = TakeAttribute( "href", node.Attributes );
					break;

				case "font":
					ParseFontSize( TakeAttribute( "size", node.Attributes ), ref fontSize );
					string colorText = TakeAttribute( "color", node.Attributes );
					if( !String.IsNullOrEmpty( colorText ) )
						color = ColorTranslator.FromHtml( colorText );
					break;

				default:
					//throw new XmlException( String.Format( "Unhandled node '{0}' in {1}", node.Name, node.InnerXml ) );
					Twp.Utilities.Log.Debug( "[HtmlParser.ParseNode] Unhandled element {0} in node {1}", node.Name, node.OuterXml );
					break;
			}

			foreach( XmlAttribute attribute in node.Attributes )
			{
				switch( attribute.Name.ToLowerInvariant() )
				{
					case "style":
						ParseStyleAttribute( attribute.Value, ref fontFamily, ref fontStyle, ref fontSize, ref color );
						break;

					default:
						//throw new XmlException( String.Format( "Unhandled attribute '{0}' in node '{1}'", attribute.Name, node.OuterXml ) );
						Twp.Utilities.Log.Debug( "[HtmlParser.ParseNode] Unhandled attribute {0} in node {1}", attribute.Name, node.OuterXml );
						break;
				}
			}

			font = new Font( fontFamily, fontSize, fontStyle, GraphicsUnit.Point );
			return text;
		}

		private static string TakeAttribute( string name, XmlAttributeCollection attributes )
		{
			if( attributes == null )
				return null;

			XmlAttribute attribute = attributes[name];
			if( attribute == null )
				return null;

			attributes.Remove( attribute );
			return attribute.Value;
		}

		private static void ParseStyleAttribute( string value, ref FontFamily fontFamily, ref FontStyle fontStyle,
												 ref float fontSize, ref Color color )
		{
			if( String.IsNullOrEmpty( value ) )
				return;

			string[] nodes = value.Split( new char[] { ';' } );
			foreach( string node in nodes )
			{
				string[] parts = node.Split( new char[] { ':' } );
				if( parts.Length != 2 )
					break;

				string name = parts[0].Trim().ToLowerInvariant();
				switch( name )
				{
					case "text-decoration":
						switch( parts[1].Trim().ToLowerInvariant() )
						{
							case "none":
								fontStyle ^= FontStyle.Underline;
								fontStyle ^= FontStyle.Strikeout;
								break;

							case "underline":
								fontStyle |= FontStyle.Underline;
								break;

							case "line-through":
								fontStyle |= FontStyle.Strikeout;
								break;

							default:
								break;
						}
						break;

					case "color":
						color = ColorTranslator.FromHtml( parts[1].Trim() );
						break;

					case "font-family":
						ParseFontFamily( parts[1].Trim(), ref fontFamily );
						break;

					case "font-size":
						ParseFontSize( parts[1].Trim(), ref fontSize );
						break;

					default:
						throw new XmlException( String.Format( "Unhandled Style attribute '{0}'", name ) );
				}
			}
		}
		
		private static void ParseFontFamily( string value, ref FontFamily fontFamily )
		{
			if( String.IsNullOrEmpty( value ) )
				return;

			switch( value.ToLowerInvariant() )
			{
				case "serif":
					fontFamily = FontFamily.GenericSerif;
					break;
						
				case "sans-serif":
					fontFamily = FontFamily.GenericSansSerif;
					break;
						
				case "monospace":
					fontFamily = FontFamily.GenericMonospace;
					break;
						
				default:
					fontFamily = new FontFamily( value );
					break;
			}
		}
		
		private static void ParseFontSize( string value, ref float fontSize )
		{
			if( String.IsNullOrEmpty( value ) )
				return;
			try
			{
				CultureInfo ci = CultureInfo.InvariantCulture;
				if( value.EndsWith( "em" ) )
					fontSize *= (float) Convert.ToDouble( value.Substring( 0, value.Length - 2 ), ci.NumberFormat );
				else if( value.EndsWith( "pt" ) )
					fontSize = (float) Convert.ToInt32( value.Substring( 0, value.Length - 2 ), ci.NumberFormat );
				else if( value.EndsWith( "px" ) )
					fontSize = (float) Convert.ToInt32( value.Substring( 0, value.Length - 2 ), ci.NumberFormat );
				else if( value.StartsWith( "+" ) )
					fontSize += (float) Convert.ToInt32( value.Substring( 1 ), ci.NumberFormat );
				else if( value.StartsWith( "-" ) )
					fontSize -= (float) Convert.ToInt32( value.Substring( 1 ), ci.NumberFormat );
				else
					fontSize = (float) Convert.ToInt32( value, ci.NumberFormat );
			}
			catch( FormatException )
			{
			}
			catch( OverflowException )
			{
			}
		}
	}
}
