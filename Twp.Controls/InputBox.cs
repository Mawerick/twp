﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
	/// <summary>
	/// Derived from <see cref="TextBox"/>. Triggers an event when the Enter key is pressed.
	/// </summary>
    [Description( "A TextBox, that triggers an event when the Enter key is pressed." )]
    [ToolboxBitmap( typeof( TextBox ) )]
	[DefaultEvent( "EnterPressed" )]
	public class InputBox : TextBox
	{
		/// <summary>
		/// Occurs when the Enter key is pressed.
		/// </summary>
		[Category( "Behavior" )]
		[Description( "Occurs when the user presses the Enter key in the input field." )]
		public event EventHandler EnterPressed;

		private bool keyHandled = false;

		/// <summary>
		/// Checks if the Enter key was pressed. Then calls base.OnKeyDown()
		/// </summary>
		/// <param name="e">a <see cref="KeyEventArgs"/> that contains the event data.</param>
		protected override void OnKeyDown( KeyEventArgs e )
		{
			if( e.KeyCode == Keys.Enter )
			{
				if( this.EnterPressed != null )
					this.EnterPressed( this, EventArgs.Empty );

				e.Handled = true;
				this.keyHandled = true;
			}
			base.OnKeyDown( e );
		}

		/// <summary>
		/// Checks if the Enter key was pressed. Then calls base.OnKeyPress()
		/// </summary>
		/// <param name="e">a <see cref="KeyPressEventArgs"/> that contains the event data.</param>
		protected override void OnKeyPress( KeyPressEventArgs e )
		{
			if( this.keyHandled == true )
			{
				e.Handled = true;
				this.keyHandled = false;
			}
			base.OnKeyPress( e );
		}
	}
}
