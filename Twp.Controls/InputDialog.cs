// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// 
    /// </summary>
	public class InputDialog : Form
	{
	    #region GetText

		/// <summary>
		/// Shows a dialog asking the user to input a text string. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
	    public static string GetText( string title, string label )
		{
			return GetText( title, label, null, false, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a text string. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="hidden">If true, the input field is treated as a password field; otherwise the input is a normal textfield.</param>
		public static string GetText( string title, string label, bool hidden )
		{
			return GetText( title, label, null, hidden, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a text string. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="text">A string representing the default value.</param>
		public static string GetText( string title, string label, string text )
		{
			return GetText( title, label, text, false, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a text string. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="text">A string representing the default value.</param>
		/// <param name="hidden">If true, the input field is treated as a password field; otherwise the input is a normal textfield.</param>
		public static string GetText( string title, string label, string text, bool hidden )
		{
			return GetText( title, label, text, hidden, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a text string. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="text">A string representing the default value.</param>
		/// <param name="hidden">If true, the input field is treated as a password field; otherwise the input is a normal textfield.</param>
		/// <param name="size">Defines the size of the dialog.</param>
		public static string GetText( string title, string label, string text, bool hidden, Size size )
		{
			using( TextBox input = new TextBox() )
			{
				input.Text = text;
				input.UseSystemPasswordChar = hidden;
				using( InputDialog dialog = new InputDialog( input, title, label ) )
				{
					if( !size.IsEmpty )
						dialog.ClientSize = size;
					if( dialog.ShowDialog() == DialogResult.OK )
						return input.Text;
				}
			}
			return text;
		}

		#endregion

		#region GetNumber

		/// <summary>
		/// Shows a dialog asking the user to input a number. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		public static decimal GetNumber( string title, string label )
		{
			return GetNumber( title, label, 0, 0, 100, 1, 0, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a number. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="decimals">An integer representing the number of decimal places to display.</param>
		public static decimal GetNumber( string title, string label, int decimals )
		{
			return GetNumber( title, label, 0, 0, 100, 1, decimals, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a number. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="value">A <see cref="Decimal"/> representing the default value.</param>
		/// <param name="minimum">An integer representing the minimum value allowed.</param>
		/// <param name="maximum">An integer representing the maximum value allowed.</param>
		/// <param name="increment">An integer representing the incremental value of the input box.</param>
		public static decimal GetNumber( string title, string label, decimal value,
		                                 int minimum, int maximum, int increment )
		{
			return GetNumber( title, label, value, minimum, maximum, increment,
			                  0, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a number. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="value">A <see cref="Decimal"/> representing the default value.</param>
		/// <param name="minimum">An integer representing the minimum value allowed.</param>
		/// <param name="maximum">An integer representing the maximum value allowed.</param>
		/// <param name="increment">An integer representing the incremental value of the input box.</param>
		/// <param name="decimals">An integer representing the number of decimal places to display.</param>
		public static decimal GetNumber( string title, string label, decimal value,
		                                 int minimum, int maximum, int increment,
		                                 int decimals )
		{
			return GetNumber( title, label, value, minimum, maximum, increment,
			                  decimals, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to input a number. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="value">A <see cref="Decimal"/> representing the default value.</param>
		/// <param name="minimum">An integer representing the minimum value allowed.</param>
		/// <param name="maximum">An integer representing the maximum value allowed.</param>
		/// <param name="increment">An integer representing the incremental value of the input box.</param>
		/// <param name="decimals">An integer representing the number of decimal places to display.</param>
		/// <param name="size">Defines the size of the dialog.</param>
		public static decimal GetNumber( string title, string label, decimal value,
		                                 int minimum, int maximum, int increment,
		                                 int decimals, Size size )
		{
			using( NumericUpDown input = new NumericUpDown() )
			{
				input.Value = value;
				input.Minimum = minimum;
				input.Maximum = maximum;
				input.Increment = increment;
				input.DecimalPlaces = decimals;
				using( InputDialog dialog = new InputDialog( input, title, label ) )
				{
					if( !size.IsEmpty )
						dialog.ClientSize = size;
					if( dialog.ShowDialog() == DialogResult.OK )
						return input.Value;
				}
			}
			return value;
		}

		#endregion

		#region GetBoolean

		/// <summary>
		/// Shows a dialog asking the user to select a boolean value. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		public static bool GetBoolean( string title, string label )
		{
			return GetBoolean( title, label, true, BooleanDisplayFormat.TrueFalse, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a boolean value. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="format">A <see cref="BooleanDisplayFormat"/> defining the format of the input.</param>
		public static bool GetBoolean( string title, string label, BooleanDisplayFormat format )
		{
			return GetBoolean( title, label, true, format, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a boolean value. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="value">A boolean representing the default value.</param>
		public static bool GetBoolean( string title, string label, bool value )
		{
			return GetBoolean( title, label, value, BooleanDisplayFormat.TrueFalse, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a boolean value. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="value">A boolean representing the default value.</param>
		/// <param name="format">A <see cref="BooleanDisplayFormat"/> defining the format of the input.</param>
		public static bool GetBoolean( string title, string label, bool value, BooleanDisplayFormat format )
		{
			return GetBoolean( title, label, value, format, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a boolean value. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="value">A boolean representing the default value.</param>
		/// <param name="format">A <see cref="BooleanDisplayFormat"/> defining the format of the input.</param>
		/// <param name="size">Defines the size of the dialog.</param>
		public static bool GetBoolean( string title, string label, bool value, BooleanDisplayFormat format, Size size )
		{
			using( BooleanBox input = new BooleanBox() )
			{
				input.Value = value;
				input.DisplayFormat = format;
				input.ButtonAlign = HorizontalAlignment.Right;
				using( InputDialog dialog = new InputDialog( input, title, label ) )
				{
					if( !size.IsEmpty )
						dialog.ClientSize = size;
					if( dialog.ShowDialog() == DialogResult.OK )
						return input.Value;
				}
			}
			return value;
		}

		#endregion

		#region GetItem(T)

		/// <summary>
		/// Shows a dialog asking the user to select a value from a list. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="items">An array of items from which to make a selection.</param>
		public static T GetItem<T>( string title, string label, T[] items )
		{
			return GetItem( title, label, items, 0, false, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a value from a list. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="items">An array of items from which to make a selection.</param>
		/// <param name="current">An integer representing the index of the default selection.</param>
		public static T GetItem<T>( string title, string label, T[] items, int current )
		{
			return GetItem( title, label, items, current, false, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a value from a list. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="items">An array of items from which to make a selection.</param>
		/// <param name="current">An integer representing the index of the default selection.</param>
		/// <param name="editable">If true, the input can be edited, making it possible to select something not in the provided list;
        /// otherwise only the provided items are available for selection.</param>
		public static T GetItem<T>( string title, string label, T[] items, int current, bool editable )
		{
			return GetItem( title, label, items, current, editable, Size.Empty );
		}

		/// <summary>
		/// Shows a dialog asking the user to select a value from a list. 
		/// </summary>
		/// <param name="title">The title of the dialog.</param>
		/// <param name="label">The text to display, to describe what the input is for.</param>
		/// <param name="items">An array of items from which to make a selection.</param>
		/// <param name="current">An integer representing the index of the default selection.</param>
		/// <param name="editable">If true, the input can be edited, making it possible to select something not in the provided list;
        /// otherwise only the provided items are available for selection.</param>
		/// <param name="size">Defines the size of the dialog.</param>
        public static T GetItem<T>( string title, string label, T[] items, int current, bool editable, Size size )
        {
            using( ComboBox input = new ComboBox() )
            {
                if( editable )
                    input.DropDownStyle = ComboBoxStyle.DropDown;
                else
                    input.DropDownStyle = ComboBoxStyle.DropDownList;
                foreach( T item in items )
                {
                    input.Items.Add( item );
                }
                input.SelectedIndex = current;
                using( InputDialog dialog = new InputDialog( input, title, label ) )
                {
                    if( !size.IsEmpty )
                        dialog.ClientSize = size;
                    if( dialog.ShowDialog() == DialogResult.OK )
                        return (T) input.SelectedItem;
                }
            }
            return items[current];
        }

        #endregion
        
        #region Base class

        private System.ComponentModel.IContainer components = null;

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		public InputDialog( Control inputControl, string title, string label )
		{
			Debug.Assert( inputControl != null );

			this.label = new Label();
			this.input = inputControl;
			this.okButton = new Button();
			this.cancelButton = new Button();
			this.line = new Line();
			this.SuspendLayout();

			this.label.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			this.label.Location = new Point( 8, 8 );
			this.label.Name = "label";
			this.label.Size = new Size( 235, 16 );
			this.label.TabIndex = 0;
			this.label.Text = label;

			this.input.Anchor = AnchorStyles.Bottom | AnchorStyles.Left
				| AnchorStyles.Right;
			this.input.Location = new Point( 8, 28 );
			this.input.Name = "input";
			this.input.Size = new Size( 235, 20 );
			this.input.TabIndex = 1;

			this.line.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			this.line.Location = new Point( 0, 54 );
			this.line.Name = "line";
			this.line.Size = new Size( 250, 2 );
			this.line.TabIndex = 2;

			this.okButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.okButton.DialogResult = DialogResult.OK;
			this.okButton.Location = new Point( 88, 67 );
			this.okButton.Name = "okButton";
			this.okButton.Size = new Size( 75, 25 );
			this.okButton.TabIndex = 3;
			this.okButton.Text = "&OK";

			this.cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.cancelButton.DialogResult = DialogResult.Cancel;
			this.cancelButton.Location = new Point( 168, 67 );
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new Size( 75, 25 );
			this.cancelButton.TabIndex = 4;
			this.cancelButton.Text = "&Cancel";

			this.AcceptButton = this.okButton;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new Size( 250, 100 );
			this.Controls.Add( this.cancelButton );
			this.Controls.Add( this.okButton );
			this.Controls.Add( this.line );
			this.Controls.Add( this.input );
			this.Controls.Add( this.label );
			this.FormBorderStyle = FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "InputDialog";
			this.StartPosition = FormStartPosition.CenterParent;
			this.Text = title;
			this.ResumeLayout( false );
			this.PerformLayout();
		}

		private Label label;
		private Control input;
		private Line line;
		private Button okButton;
		private Button cancelButton;

		#endregion
	}
}
