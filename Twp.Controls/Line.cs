﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Specifies the style used to paint a line.
    /// </summary>
    public enum LineStyle
    {
        /// <summary>The line has a sunken look.</summary>
        Sunken,
        /// <summary>The line has a raised look.</summary>
        Raised,
        /// <summary>The line has a flat look.</summary>
        Flat,
    }

    /// <summary>
    /// Description of Line.
    /// </summary>
    [Description( "Provides a simple horizontal or vertical line. Purely cosmetic." )]
    [Designer( typeof( Design.LineDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
    [ToolboxBitmap( typeof( Line ) )]
    [DefaultProperty( "LineStyle" )]
    [DefaultEvent( "OrientationChanged" )]
    public class Line : Control, INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes a new instance of the Line class with default settings.
        /// </summary>
        public Line()
            : base()
        {
            this.SetStyle( ControlStyles.AllPaintingInWmPaint |
            ControlStyles.ResizeRedraw |
            ControlStyles.OptimizedDoubleBuffer |
            ControlStyles.SupportsTransparentBackColor,
                           true );
            this.BackColor = Color.Transparent;
            this.TabStop = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            if( this.PropertyChanged != null )
                this.PropertyChanged( this, e );
        }

        /// <summary>
        /// Gets or sets a value representing the orientation of the line.
        /// The default is Horizontal.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The orientation of the line." )]
        [DefaultValue( Orientation.Horizontal )]
        public Orientation Orientation
        {
            get { return this.orientation; }
            set
            {
                if( this.orientation != value )
                {
                    this.orientation = value;
                    if( this.orientation == Orientation.Horizontal )
                    {
                        if( this.Width < this.Height )
                        {
                            int temp = this.Width;
                            this.Width = this.Height;
                            this.Height = temp;
                        }
                    }
                    else
                    {
                        if( this.Width > this.Height )
                        {
                            int temp = this.Width;
                            this.Width = this.Height;
                            this.Height = temp;
                        }
                    }

                    this.Size = FitSize;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Orientation" ) );
                }
            }
        }
        private Orientation orientation = Orientation.Horizontal;

        public new Size Size
        {
            get { return base.Size; }
            set
            {
                if( this.orientation == Orientation.Horizontal )
                {
                    this.Width = value.Width;
                    this.Height = FitSize.Height;
                }
                else
                {
                    this.Width = FitSize.Width;
                    this.Height = value.Height;
                }
            }
        }

        private Size FitSize
        {
            get
            {
                if( this.orientation == Orientation.Horizontal )
                    return new Size( this.ClientSize.Width, ( this.linePadding * 2 ) + 2 );
                else
                    return new Size( ( this.linePadding * 2 ) + 2, this.ClientSize.Height );
            }
        }

        /// <summary>
        /// Gets or sets a value representing the style of the line.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The style of the line." )]
        [DefaultValue( LineStyle.Sunken )]
        public LineStyle LineStyle
        {
            get { return this.lineStyle; }
            set
            {
                if( this.lineStyle != value )
                {
                    this.lineStyle = value;
                    this.Invalidate();
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "LineStyle" ) );
                }
            }
        }
        private LineStyle lineStyle = LineStyle.Sunken;

        /// <summary>
        /// Gets or sets a value representing the padding on either side of the line.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The padding in either side of the line" )]
        [DefaultValue( 4 )]
        public int LinePadding
        {
            get { return this.linePadding; }
            set
            {
                if( this.linePadding != value )
                {
                    this.linePadding = value;
                    this.Size = FitSize;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "LinePadding" ) );
                }
            }
        }
        private int linePadding = 4;
		
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get { return this.text; }
            set
            {
                if( this.text != value )
                {
                    this.text = value;
                    this.Invalidate();
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Text" ) );
                }
            }
        }
        private string text = String.Empty;
		
        [Category( "Appearance" )]
        [Description( "" )]
        public HorizontalAlignment TextAlign
        {
            get { return this.textAlign; }
            set
            {
                if( this.textAlign != value )
                {
                    this.textAlign = value;
                    this.Invalidate();
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "TextAlign" ) );
                }
            }
        }
        private HorizontalAlignment textAlign = HorizontalAlignment.Left;

        private const int textSpacing = 4;
		
        /// <summary>
        /// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint( PaintEventArgs e )
        {
            Color light = Color.Empty;
            Color dark = Color.Empty;
            switch( this.lineStyle )
            {
                case LineStyle.Sunken:
                    light = SystemColors.ButtonHighlight;
                    dark = SystemColors.ButtonShadow;
                    break;
                case LineStyle.Raised:
                    light = SystemColors.ButtonShadow;
                    dark = SystemColors.ButtonHighlight;
                    break;
                case LineStyle.Flat:
					// light remains empty, only dark gets rendered.
                    dark = SystemColors.ButtonShadow;
                    break;
                default:
                    return;
            }
            Rectangle rect = this.ClientRectangle;
			
            if( String.IsNullOrEmpty( this.text ) || this.orientation == Orientation.Vertical )
            {
                Point start = Point.Empty;
                Point end = Point.Empty;
                switch( this.orientation )
                {
                    case Orientation.Horizontal:
                        start = new Point( rect.X, rect.Y + this.linePadding );
                        end = new Point( rect.X + rect.Width, rect.Y + this.linePadding );
                        break;
                    case Orientation.Vertical:
                        start = new Point( rect.X + this.linePadding, rect.Y );
                        end = new Point( rect.X + this.linePadding, rect.Y + rect.Height );
                        break;
                }

                this.DrawLine( e.Graphics, start, end, light, dark );
            }
            else
            {
                Point start1 = Point.Empty;
                Point start2 = Point.Empty;
                Point end1 = Point.Empty;
                Point end2 = Point.Empty;
                TextFormatFlags tff = TextFormatFlags.VerticalCenter | TextFormatFlags.Left | TextFormatFlags.NoPadding;
                Size textSize = TextRenderer.MeasureText( e.Graphics, this.text, this.Font, rect.Size, tff );
                switch( this.textAlign )
                {
                    case HorizontalAlignment.Left:
                        start1 = new Point( rect.X, rect.Y + this.linePadding );
                        end1 = new Point( start1.X + textSpacing, start1.Y );
                        start2 = new Point( end1.X + textSize.Width + textSpacing * 2, start1.Y );
                        end2 = new Point( rect.X + rect.Width, start1.Y );
                        break;
                    case HorizontalAlignment.Right:
                        start1 = new Point( rect.X, rect.Y + this.linePadding );
                        end2 = new Point( rect.X + rect.Width, start1.Y );
                        start2 = new Point( end2.X - textSpacing, start1.Y );
                        end1 = new Point( start2.X - textSize.Width - textSpacing * 2, start1.Y );
                        break;
                    case HorizontalAlignment.Center:
                        start1 = new Point( rect.X, rect.Y + this.linePadding );
                        end1 = new Point( rect.X + ( rect.Width / 2 ) - ( textSize.Width / 2 ) - textSpacing, start1.Y );
                        start2 = new Point( rect.X + ( rect.Width / 2 ) + ( textSize.Width / 2 ) + textSpacing, start1.Y );
                        end2 = new Point( rect.X + rect.Width, start1.Y );
                        break;
                }
                this.DrawLine( e.Graphics, start1, end1, light, dark );
                this.DrawLine( e.Graphics, start2, end2, light, dark );
                int vPos = rect.Y + (int) Math.Floor( (double) ( rect.Height - textSize.Height ) / 2.0 );
                Rectangle textRect = new Rectangle( end1.X + textSpacing, vPos, textSize.Width, textSize.Height );
                TextRenderer.DrawText( e.Graphics, this.text, this.Font, textRect, this.ForeColor, tff );
            }
        }
		
        private void DrawLine( Graphics g, Point start, Point end, Color light, Color dark )
        {
            using( Pen darkPen = new Pen( dark ) )
            {
                g.DrawLine( darkPen, start, end );
            }
            if( light != Color.Empty )
            {
                switch( this.orientation )
                {
                    case Orientation.Horizontal:
                        start.Offset( 0, 1 );
                        end.Offset( 0, 1 );
                        break;
                    case Orientation.Vertical:
                        start.Offset( 1, 0 );
                        end.Offset( 1, 0 );
                        break;
                }
                using( Pen lightPen = new Pen( light ) )
                {
                    g.DrawLine( lightPen, start, end );
                }
            }
        }
		
        protected override void OnResize( EventArgs e )
        {
            this.Invalidate();
        }
    }
}
