﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using Twp.Utilities;

namespace Twp.Controls
{
	[ToolboxItem( true )]
	[DesignTimeVisible( true )]
	[DefaultProperty( "MaximumLines" )]
	public class LogWindow : Component, INotifyPropertyChanged
	{
		public LogWindow()
		{
			InitializeComponent();
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBox = new ListBox();
            this.clearButton = new Button();
            this.form = new Form();
			this.form.SuspendLayout();
            // 
			// listBox
			// 
			this.listBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBox.Font = new System.Drawing.Font( System.Drawing.FontFamily.GenericMonospace, this.listBox.Font.Size );
			this.listBox.FormattingEnabled = true;
			this.listBox.HorizontalScrollbar = true;
            this.listBox.IntegralHeight = false;
			this.listBox.Location = new System.Drawing.Point( 0, 0 );
			this.listBox.Name = "listBox";
			this.listBox.Size = new System.Drawing.Size( 435, 89 );
			this.listBox.TabIndex = 0;
            //
            // clearButton
            //
            this.clearButton.Click += new EventHandler( OnClear );
            this.clearButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clearButton.Location = new System.Drawing.Point( 0, 0 );
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size( 75, 20 );
            this.clearButton.TabIndex = 1;
            this.clearButton.Text = "Clear";
            // 
			// form
			// 
			this.form.ControlBox = false;
			this.form.Controls.Add( this.listBox );
            this.form.Controls.Add( this.clearButton );
//            this.form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.form.Name = "LogWindow";
            this.form.ShowInTaskbar = false;
            this.form.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.form.Text = "Log";
			this.form.ResumeLayout( false );
		}

        private Button clearButton;
		private ListBox listBox;
		private Form form;

		#endregion

        #region Properties

        [Category( "Behavior" )]
		[Description( "The maximum number of lines to display in the log window." )]
		[DefaultValue( 100 )]
		public int MaximumLines
		{
			get { return this.maximumLines; }
			set
			{
				if( this.maximumLines != value )
				{
					this.maximumLines = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "MaximumLines" ) );
				}
			}
		}
		private int maximumLines = 100;

		[Browsable( false )]
		public Size Size
		{
			get { return this.form.Size; }
			set { this.form.Size = value; }
		}

		[Browsable( false )]
		public int Height
		{
			get { return this.form.Height; }
			set { this.form.Height = value; }
		}

		[Browsable( false )]
		public int Width
		{
			get { return this.form.Width; }
			set { this.form.Width = value; }
		}

		[Browsable( false )]
		public Point Location
		{
			get { return this.form.Location; }
			set { this.form.Location = value; }
		}
		
		[Browsable( false )]
		private Form Owner
		{
		    get { return this.form.Owner; }
		    set { this.form.Owner = value; }
		}

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			if( this.PropertyChanged != null )
				this.PropertyChanged( this, e );
		}

		public void AddMessage( string message )
		{
            if( this.listBox.IsDisposed )
                return;
			this.listBox.Items.Add( message );
			if( this.listBox.Items.Count > this.maximumLines )
			{
				this.listBox.Items.RemoveAt( 0 );
			}
			this.listBox.SelectedIndex = this.listBox.Items.Count - 1;
			this.listBox.ClearSelected();
		}

		public void Show()
		{
			if( !this.DesignMode )
				this.form.Show();
		}

        public void Init( Form parent )
        {
            this.Owner = parent;
            this.Owner.Move += new EventHandler( this.OnMoved );
            this.Owner.Shown += new EventHandler( this.OnShown );
            Log.Logged += new LoggedEventHandler( this.OnLogged );
        }

        #endregion

        #region Event handlers

        private void OnClear( object sender, EventArgs e )
        {
            this.listBox.Items.Clear();
        }

        private void OnLogged( LoggedEventArgs e )
        {
            this.AddMessage( e.Message );
        }

        private void OnMoved( object sender, EventArgs e )
        {
            if( this.Owner == null )
                return;
            System.Drawing.Point location = this.Owner.Location;
            location.X += this.Owner.Width + 8;
            this.Location = location;
            this.Height = this.Owner.Height;
        }

        private void OnShown( object sender, EventArgs e )
        {
            this.Show();
            if( this.Owner != null )
                this.Owner.Focus();
        }

        #endregion
    }
}
