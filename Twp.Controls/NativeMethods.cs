﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Twp.Controls
{
    public static class NativeMethods
    {
        public const int WM_NOTIFY = 0x004E,
                         WM_NCHITTEST = 0x0084,
                         WM_NCACTIVATE = 0x0086,
                         WM_PRINT = 0x317,
                         WM_USER = 0x0400,
                         WM_REFLECT = WM_USER + 0x1C00,
                         WM_COMMAND = 0x0111,
                         WM_GETMINMAXINFO = 0x0024,
                         WM_SETFONT = 0x30,
                         WM_FONTCHANGE = 0x1d,

                         HTTRANSPARENT = -1,
                         HTLEFT = 10,
                         HTRIGHT = 11,
                         HTTOP = 12,
                         HTTOPLEFT = 13,
                         HTTOPRIGHT = 14,
                         HTBOTTOM = 15,
                         HTBOTTOMLEFT = 16,
                         HTBOTTOMRIGHT = 17,

                         LVM_GETHEADER = 0x101f,
                         CBN_DROPDOWN = 7,

                         // For ShowScrollBar
                         SB_HORZ = 0,
                         SB_VERT = 1,
                         SB_CTL = 2,
                         SB_BOTH = 3,

                         // Mouse stuff
                         WM_LBUTTONDOWN = 0x0201,
                         WM_LBUTTONDBLCLK = 0x0203,
                         WM_SETCURSOR = 0x0020;

        public const uint WS_BORDER = 0x00800000,
                          WS_EX_TRANSPARENT = 0x00000020,
                          WS_EX_TOOLWINDOW  = 0x00000080,
                          WS_EX_CLIENTEDGE  = 0x00000200,
                          WS_EX_LAYERED     = 0x00080000,
                          WS_EX_NOACTIVATE  = 0x08000000;

        public static int HIWORD( int n )
        {
            return (n >> 16) & 0xffff;
        }

        public static int HIWORD( IntPtr n )
        {
            return HIWORD( unchecked( (int) (long) n ) );
        }

        public static int LOWORD( int n )
        {
            return n & 0xffff;
        }

        public static int LOWORD( IntPtr n )
        {
            return LOWORD( unchecked( (int) (long) n ) );
        }

        [StructLayout( LayoutKind.Sequential )]
        public struct MINMAXINFO
        {
            public Point reserved;
            public Size maxSize;
            public Point maxPosition;
            public Size minTrackSize;
            public Size maxTrackSize;
        }

        /// <summary>
        /// Notify message header structure.
        /// </summary>
        [StructLayout( LayoutKind.Sequential )]
        public struct NMHDR
        {
            public IntPtr hwndFrom;
            public int idFrom;
            public int code;
        }

        [DllImport( "user32", CallingConvention=CallingConvention.Winapi )]
        static public extern IntPtr SendMessage( IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam );

        [DllImport( "user32", CallingConvention=CallingConvention.Winapi )]
        [return:MarshalAs(UnmanagedType.Bool)]
        static public extern bool ShowScrollBar( System.IntPtr hWnd, int wBar, [MarshalAs(UnmanagedType.Bool)] bool bShow );
    }
}