﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Controls
{
    /// <summary>
    /// Description of Node.
    /// </summary>
    public class Node
    {
        #region Constructor

        public Node()
        {
            this.data = new object[1];
        }

        public Node(string text)
        {
            this.data = new object[1] { text };
        }

        public Node(params object[] fields)
        {
            this.SetData(fields);
        }

        #endregion

        #region Private fields

        private NodeCollection owner = null;
        private Node prevSibling = null;
        private Node nextSibling = null;
        private NodeCollection children = null;
        private bool hasChildren = false;
        private bool expanded = false;
        private CheckState checkState = CheckState.Unchecked;
        private int imageId = -1;
        private string imageKey = null;
        private int id = -1;
        private object[] data = null;
        private Color color = Color.Empty;
        private object tag = null;
        private int childVisibleCount = 0;

        #endregion

        #region Properties

        public Node Parent
        {
            get
            {
                if (this.owner != null)
                    return this.owner.Owner;
                return null;
            }
        }

        public Node PrevSibling
        {
            get { return this.prevSibling; }
            internal set { this.prevSibling = value; }
        }

        public Node NextSibling
        {
            get { return this.nextSibling; }
            internal set { this.nextSibling = value; }
        }

        public bool HasChildren
        {
            get
            {
                if (this.children != null && this.children.IsEmpty == false)
                    return true;
                return this.hasChildren;
            }
            set
            {
                this.hasChildren = value;
            }
        }

        public int NodeIndex
        {
            get { return this.id; }
        }

        internal int Id
        {
            get
            {
                if (this.owner == null)
                    return -1;
                this.owner.UpdateChildIds(false);
                return this.id;
            }
            set { this.id = value; }
        }

        public int ImageId
        {
            get { return this.imageId; }
            set { this.imageId = value; }
        }

        public string ImageKey
        {
            get { return this.imageKey; }
            set { this.imageKey = value; }
        }

        public virtual NodeCollection Owner
        {
            get { return this.owner; }
        }

        public virtual NodeCollection Nodes
        {
            get
            {
                if (this.children == null)
                    this.children = new NodeCollection(this);
                return this.children;
            }
        }

        public int Fields
        {
            get { return this.data.Length; }
        }

        public bool Expanded
        {
            get { return this.expanded; }
            set
            {
                if (this.expanded == value)
                    return;

                NodeCollection root = this.GetRootCollection();
                if (root != null)
                    root.NotifyBeforeExpand(this, value);

                int oldCount = this.VisibleNodeCount;
                this.expanded = value;
                if (this.expanded)
                    this.UpdateOwnerTotalCount(1, this.VisibleNodeCount);
                else
                    this.UpdateOwnerTotalCount(oldCount, 1);

                if (root != null)
                    root.NotifyAfterExpand(this, value);
            }
        }

        public CheckState CheckState
        {
            get { return this.checkState; }
            set
            {
                this.checkState = value;
                this.OnCheckedChanged();
            }
        }

        public bool Checked
        {
            get { return this.checkState != CheckState.Unchecked; }
            set { this.CheckState = value ? CheckState.Checked : CheckState.Unchecked; }
        }

        public object this[string fieldName]
        {
            get { return this[Owner.FieldIndex(fieldName)]; }
            set { this[Owner.FieldIndex(fieldName)] = value; }
        }

        public object this[int index]
        {
            get
            {
                if (index < 0 || index >= this.data.Length)
                    return null;
                return this.data[index];
            }
            set
            {
                if (index >= 0 && index < 50)
                {
                    if (this.data == null || index >= this.data.Length)
                    {
                        object[] newData = new object[index + 1];
                        if (this.data != null)
                            this.data.CopyTo(newData, 0);
                        this.data = newData;
                    }
                }
                AssertData(index);
                this.data[index] = value;
            }
        }

        public int VisibleNodeCount
        {
            get
            {
                if (this.expanded)
                    return this.childVisibleCount + 1;
                return 1;
            }
        }

        public Color Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        public object Tag
        {
            get { return this.tag; }
            set { this.tag = value; }
        }

        #endregion

        #region Events

        public event EventHandler CheckedChanged;

        #endregion

        #region Public methods

        protected virtual void OnCheckedChanged()
        {
            if (this.CheckedChanged != null)
                this.CheckedChanged(this, EventArgs.Empty);
        }

        public void SetData(params object[] data)
        {
            this.data = new object[data.Length];
            data.CopyTo(this.data, 0);
        }

        public int CompareTo(Node other, int field)
        {
            if (field < 0)
                throw new ArgumentOutOfRangeException("field");

            if (field >= this.data.Length && field >= other.data.Length)
                return 0;
            else if (field >= this.data.Length)
                return -1;
            else if (field >= other.data.Length)
                return 1;

            try
            {
                return Extensions.Compare(this.data[field], other.data[field]);
            }
            catch
            {
                if (this.data[field] != null && other.data[field] != null)
                    return Extensions.Compare(this.data[field].ToString(), other.data[field].ToString());
                else
                    return 1;
            }
        }

        public void MakeVisible()
        {
            Node parent = this.Parent;
            while (parent != null)
            {
                parent.Expanded = true;
                parent = parent.Parent;
            }
        }

        public bool IsVisible()
        {
            Node parent = this.Parent;
            while (parent != null)
            {
                if (parent.Expanded == false)
                    return false;
                if (parent.Owner == null)
                    return false;
                parent = parent.Parent;
            }
            return true;
        }

        public void Collapse()
        {
            this.Expanded = false;
        }

        public void CollapseAll()
        {
            this.Expanded = false;
            if (this.HasChildren)
                foreach (Node node in this.Nodes)
                    node.CollapseAll();
        }

        public void Expand()
        {
            this.Expanded = true;
        }

        public void ExpandAll()
        {
            this.Expanded = true;
            if (this.HasChildren)
            {
                foreach (Node node in this.Nodes)
                    node.ExpandAll();
            }
        }

        public Node GetRoot()
        {
            Node parent = this;
            while (parent.Parent != null)
                parent = parent.Parent;
            return parent;
        }

        public NodeCollection GetRootCollection()
        {
            return this.GetRoot().Owner;
        }

        public string GetId()
        {
            StringBuilder sb = new StringBuilder(32);
            Node node = this;
            while (node != null)
            {
                node.Owner.UpdateChildIds(false);
                if (node.Parent != null)
                    sb.Insert(0, "." + node.id.ToString());
                else
                    sb.Insert(0, node.id.ToString());
                node = node.Parent;
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            string text = this.id.ToString();
            text += " { ";
            if (this.data != null && this.data.Length > 0)
            {
                foreach (object obj in this.data)
                {
                    string field = '"' + Convert.ToString(obj) + '"';
                    if (!String.IsNullOrEmpty(field))
                        text += field + ", ";
                }
                text = text.TrimEnd(',', ' ');
            }
            else
                text += "(empty)";
            text += " }";
            return text;
        }

        #endregion

        #region Internal methods

        internal void InsertBefore(Node insertBefore, NodeCollection owner)
        {
            this.owner = owner;
            Node next = insertBefore;
            Node prev = null;
            if (next != null)
            {
                prev = next.PrevSibling;
                next.prevSibling = this;
            }
            if (prev != null)
                prev.nextSibling = this;

            this.nextSibling = next;
            this.prevSibling = prev;
            this.UpdateOwnerTotalCount(0, this.VisibleNodeCount);
        }

        internal void InsertAfter(Node insertAfter, NodeCollection owner)
        {
            this.owner = owner;
            Node prev = insertAfter;
            Node next = null;
            if (prev != null)
            {
                next = prev.NextSibling;
                prev.nextSibling = this;
                this.prevSibling = prev;
            }
            if (next != null)
                next.prevSibling = this;

            this.nextSibling = next;
            this.UpdateOwnerTotalCount(0, this.VisibleNodeCount);
        }

        internal void Remove()
        {
            Node prev = this.PrevSibling;
            Node next = this.NextSibling;
            if (prev != null)
                prev.nextSibling = next;
            if (next != null)
                next.prevSibling = prev;

            this.prevSibling = null;
            this.nextSibling = null;
            this.UpdateOwnerTotalCount(this.VisibleNodeCount, 0);
            this.owner = null;
            this.id = -1;
        }

        private void UpdateTotalCount(int oldValue, int newValue)
        {
            int old = this.VisibleNodeCount;
            this.childVisibleCount += (newValue - oldValue);
            this.UpdateOwnerTotalCount(old, this.VisibleNodeCount);
        }

        private void UpdateOwnerTotalCount(int oldValue, int newValue)
        {
            if (this.Owner != null)
                this.Owner.InternalUpdateNodeCount(oldValue, newValue);
            if (this.Parent != null)
                this.Parent.UpdateTotalCount(oldValue, newValue);
        }

        private void AssertData(int index)
        {
            System.Diagnostics.Debug.Assert(index >= 0, "index >= 0");
            System.Diagnostics.Debug.Assert(index < this.data.Length, "index < data.Length");
        }

        #endregion
    }

    public class NodeComparer : IComparer<Node>
    {
        public NodeComparer(SortOrder order, int field)
        {
            this.order = order;
            this.field = field;
        }

        private readonly SortOrder order = SortOrder.Ascending;
        private readonly int field = 0;

        public int Compare(Node x, Node y)
        {
            if (x == null)
            {
                if (y == null)
                    return 0;
                else
                    return -1;
            }
            else
            {
                if (y == null)
                    return 1;
                else
                {
                    int retVal;
                    if (this.order == SortOrder.Ascending)
                        retVal = x.CompareTo(y, this.field);
                    else
                        retVal = y.CompareTo(x, this.field);

                    if (retVal != 0)
                        return retVal;

                    for (int i = 0; i < x.Fields; i++)
                    {
                        if (i == this.field)
                            continue;

                        if (i >= y.Fields)
                            return 1;

                        if (this.order == SortOrder.Ascending)
                            retVal = x.CompareTo(y, i);
                        else
                            retVal = y.CompareTo(x, i);

                        if (retVal != 0)
                            break;
                    }

                    return retVal;
                }
            }
        }
    }
}
