﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace Twp.Controls
{
    public enum SortOrder
    {
        None,
        Ascending,
        Descending,
    }

    /// <summary>
    /// Description of TreeListNodeCollection.
    /// </summary>
    public class NodeCollection : IEnumerable
    {
        #region Constructor

        public NodeCollection( Node owner )
        {
            this.owner = owner;
        }

        #endregion

        #region Private Fields

        private int nextId = 0;
        private int dirtyId = 0;
        private int totalNodeCount = 0;
        private int count = 0;

        private Node[] internalNodes = null;
        private Node owner = null;
        private Node firstNode = null;
        private Node lastNode = null;

        #endregion

        #region Properties

        public Node Owner
        {
            get { return this.owner; }
        }

        public Node FirstNode
        {
            get { return this.firstNode; }
        }

        public Node LastNode
        {
            get { return this.lastNode; }
        }

        public bool IsEmpty
        {
            get { return this.firstNode == null; }
        }

        public int Count
        {
            get { return this.count; }
        }

        public virtual int VisibleNodeCount
        {
            get { return this.totalNodeCount; }
        }

        public Node this[int index]
        {
            get
            {
                Debug.Assert( index >= 0 && index < this.count, "Index out of range." );
                if( index >= this.count )
                    throw new IndexOutOfRangeException( String.Format( "Node this [{0}], Collection Count {1}", index, this.count ) );
                this.EnsureInternalArray();
                return this.internalNodes[index];
            }
        }

        internal CheckState CheckState
        {
            get
            {
                bool check = false;
                bool mixed = false;
                this.EnsureInternalArray();
                foreach( Node node in this.internalNodes )
                {
                    if( node.Checked )
                        check = true;
                    else if( check )
                        mixed = true;
                }
                
                if( mixed )
                    return CheckState.Indeterminate;
                else if( check )
                    return CheckState.Checked;
                else
                    return CheckState.Unchecked;
            }
        }
        
        #endregion
        
        #region Public Methods

        public virtual void Clear()
        {
            while( this.firstNode != null )
            {
                Node node = this.firstNode;
                this.firstNode = node.NextSibling;
                node.Remove();
            }
            this.firstNode = null;
            this.lastNode = null;
            this.count = 0;
            this.totalNodeCount = 0;
            this.dirtyId = 0;
            this.nextId = 0;
            this.ClearInternalArray();
            if( this.owner != null )
                this.owner.HasChildren = false;
        }

        public virtual Node Add( string text )
        {
            if( text == null )
                throw new ArgumentNullException( "text" );

            return Add( new Node( text ) );
        }

        public virtual Node Add( params object[] data )
        {
            return Add( new Node( data ) );
        }

        public virtual Node Add( Node node )
        {
            if( node == null )
                throw new ArgumentNullException( "node" );

            this.ClearInternalArray();
            node.InsertAfter( this.lastNode, this );
            this.lastNode = node;
            if( this.firstNode == null )
                this.firstNode = node;
            node.Id = this.nextId++;
            this.count++;
            return node;
        }

        public void Remove( Node node )
        {
            if( node == null )
                throw new ArgumentNullException( "node" );

            if( this.lastNode == null )
                return;

            this.ClearInternalArray();
            Node prev = node.PrevSibling;
            Node next = node.NextSibling;
            node.Remove();
            
            if( prev == null )
                this.firstNode = next;
            if( next == null )
                this.lastNode = prev;
            this.dirtyId++;
            this.count--;
            if( this.owner != null )
                this.owner.HasChildren = this.count != 0;
        }

        public virtual void Sort( SortOrder order, int field )
        {
            if( this.count <= 0 )
                return;

            this.EnsureInternalArray();
            List<Node> nodes = new List<Node>( this.internalNodes );
            this.ClearInternalArray();

            nodes.Sort( new NodeComparer( order, field ) );

            // Fix links.
            this.firstNode = nodes[0];
            nodes[0].PrevSibling = null;
            if( nodes.Count == 1 )
            {
                nodes[0].NextSibling = null;
                this.lastNode = nodes[0];
            }
            else
            {
                nodes[0].NextSibling = nodes[1];
                int last = nodes.Count - 1;
                for( int i = 1; i < last; i++ )
                {
                    nodes[i - 1].NextSibling = nodes[i];
                    nodes[i].PrevSibling = nodes[i - 1];
                    nodes[i].NextSibling = nodes[i + 1];
                    nodes[i + 1].PrevSibling = nodes[i];
                }
                nodes[last].PrevSibling = nodes[last - 1];
                nodes[last].NextSibling = null;

                this.lastNode = nodes[last];
            }
            this.dirtyId = 0;
            this.nextId = 0;
            foreach( Node node in nodes )
            {
                node.Id = this.nextId++;
                node.Nodes.Sort( order, field );
            }
        }

        public void InsertAfter( Node node, Node insertAfter )
        {
            this.ClearInternalArray();
            if( insertAfter == null )
            {
                node.InsertBefore( this.firstNode, this );
                this.firstNode = node;
            }
            else
            {
                node.InsertAfter( insertAfter, this );
            }
            if( this.lastNode == insertAfter )
            {
                this.lastNode = node;
                node.Id = this.nextId++;
            }
            else
                this.dirtyId++;
            this.count++;
        }

        public Node NodeAtIndex( int index )
        {
            Node node = this.FirstNode;
            while( index-- > 0 && node != null )
                node = node.NextSibling;
            return node;
        }

        public int GetNodeIndex( Node node )
        {
            int index = 0;
            Node tmp = this.FirstNode;
            while( tmp != null && tmp != node )
            {
                tmp = tmp.NextSibling;
                index++;
            }
            if( tmp == null )
                return -1;
            return index;
        }

        public virtual int FieldIndex( string fieldName )
        {
            NodeCollection rootCollection = this;
            while( rootCollection.Owner != null && rootCollection.Owner.Owner != null )
                rootCollection = rootCollection.Owner.Owner;
            return rootCollection.GetFieldIndex( fieldName );
        }

        public Node FirstVisibleNode()
        {
            return this.FirstNode;
        }

        public Node LastVisibleNode( bool recursive )
        {
            if( recursive )
                return FindNodesBottomLeaf( this.LastNode, true );
            return this.LastNode;
        }

        public Node SlowGetNodeFromVisibleIndex( int index )
        {
            int startIndex = index;
            RecursiveNodesEnumerator iterator = new RecursiveNodesEnumerator( this.firstNode, true );
            while( iterator.MoveNext() )
            {
                index--;
                if( index < 0 )
                    return iterator.Current as Node;
            }
            return null;
        }

        public IEnumerator GetEnumerator()
        {
            return new TreeListNodesEnumerator( this.firstNode );
        }

        public virtual int SlowTotalRowCount( bool mustBeVisible )
        {
            int count = 0;
            RecursiveNodesEnumerator iterator = new RecursiveNodesEnumerator( this, mustBeVisible );
            while( iterator.MoveNext() )
                count++;
            return count;
        }

        public virtual void NotifyBeforeExpand( Node nodeToExpand, bool expanding )
        {
        }

        public virtual void NotifyAfterExpand( Node nodeToExpand, bool expanding )
        {
        }

        #endregion

        #region Internal Methods

        internal void UpdateChildIds( bool recursive )
        {
            if( recursive == false && this.dirtyId == 0 )
                return;
            
            this.dirtyId = 0;
            this.nextId = 0;
            foreach( Node node in this )
            {
                node.Id = this.nextId++;
                if( node.HasChildren && recursive )
                    node.Nodes.UpdateChildIds( true );
            }
        }

        protected virtual int GetFieldIndex( string fieldName )
        {
            return -1;
        }

        private void EnsureInternalArray()
        {
            if( this.internalNodes != null )
            {
                Debug.Assert( this.internalNodes.Length == this.Count, "internalNodes.Length == Count" );
                return;
            }

            this.internalNodes = new Node[this.Count];
            int index = 0;
            foreach( Node node in this )
                this.internalNodes[index++] = node;
        }

        private void ClearInternalArray()
        {
            this.internalNodes = null;
        }
        
        protected virtual void UpdateNodeCount( int oldValue, int newValue )
        {
            this.totalNodeCount += (newValue - oldValue);
        }
        
        internal void InternalUpdateNodeCount( int oldValue, int newValue )
        {
            this.UpdateNodeCount( oldValue, newValue );
        }
        
        #endregion

        #region Static Methods

        public static Node GetNextNode( Node startingNode, int searchOffset )
        {
            if( searchOffset == 0 )
                return startingNode;
            
            if( searchOffset > 0 )
            {
                ForwardNodeEnumerator iterator = new ForwardNodeEnumerator( startingNode, true );
                while( searchOffset >= 0 && iterator.MoveNext() )
                    searchOffset--;
                return iterator.Current;
            }
            if( searchOffset < 0 )
            {
                ReverseNodeEnumerator iterator = new ReverseNodeEnumerator( startingNode, true );
                while( searchOffset <= 0 && iterator.MoveNext() )
                    searchOffset++;
                return iterator.Current;
            }
            return null;
        }
        
        public static IEnumerable ReverseNodeIterator( Node firstNode, Node lastNode, bool mustBeVisible )
        {
            bool done = false;
            ReverseNodeEnumerator iterator = new ReverseNodeEnumerator( firstNode, mustBeVisible );
            while( iterator.MoveNext() )
            {
                if( done )
                    break;
                
                if( iterator.Current == lastNode )
                    done = true;
                
                yield return iterator.Current;
            }
        }

        public static IEnumerable ForwardNodeIterator( Node firstNode, Node lastNode, bool mustBeVisible )
        {
            bool done = false;
            ForwardNodeEnumerator iterator = new ForwardNodeEnumerator( firstNode, mustBeVisible );
            while( iterator.MoveNext() )
            {
                if( done )
                    break;
                
                if( iterator.Current == lastNode )
                    done = true;
                
                yield return iterator.Current;
            }
        }
        
        public static IEnumerable ForwardNodeIterator( Node firstNode, bool mustBeVisible )
        {
            ForwardNodeEnumerator iterator = new ForwardNodeEnumerator( firstNode, mustBeVisible );
            while( iterator.MoveNext() )
                yield return iterator.Current;
        }
        
        public static int GetVisibleNodeIndex( Node node )
        {
            if( node == null || node.IsVisible() == false || node.GetRootCollection() == null )
                return -1;
            
            int count = -node.VisibleNodeCount;
            while( node != null )
            {
                count += node.VisibleNodeCount;
                if( node.PrevSibling != null )
                    node = node.PrevSibling;
                else
                {
                    node = node.Parent;
                    if( node != null )
                        count -= node.VisibleNodeCount - 1;
                }
            }
            return count;
        }
        
        public static Node FindNodesBottomLeaf( Node node, bool mustBeVisible )
        {
            if( mustBeVisible && node.Expanded == false )
                return node;
            if( node.HasChildren == false || node.Nodes.LastNode == null )
                return node;
            node = node.Nodes.LastNode;
            return FindNodesBottomLeaf( node, mustBeVisible );
        }
        
        #endregion

        #region NodesEnumerator class

        internal class TreeListNodesEnumerator : IEnumerator
        {
            private Node firstNode;
            private Node current = null;

            public TreeListNodesEnumerator( Node firstNode )
            {
                this.firstNode = firstNode;
            }

            public object Current
            {
                get { return this.current; }
            }

            public bool MoveNext()
            {
                if( this.firstNode == null )
                    return false;
                if( this.current == null )
                {
                    this.current = this.firstNode;
                    return true;
                }
                this.current = this.current.NextSibling;
                return this.current != null;
            }

            public void Reset()
            {
                this.current = null;
            }
        }
        
        #endregion
        
        #region RecursiveNodesEnumerator class
        
        internal class RecursiveNodesEnumerator: IEnumerator<Node>
        {
            private class NodeCollectionIterator : IEnumerator<Node>
            {
                private Node firstNode;
                private Node current;
                private bool visible;
                
                public NodeCollectionIterator( NodeCollection collection, bool mustBeVisible )
                {
                    this.firstNode = collection.FirstNode;
                    this.visible = mustBeVisible;
                }
                
                public Node Current
                {
                    get { return this.current; }
                }
                
                object IEnumerator.Current
                {
                    get { return this.current; }
                }
                
                public bool MoveNext()
                {
                    if( this.firstNode == null )
                        return false;
                    if( this.current == null )
                    {
                        this.current = this.firstNode;
                        return true;
                    }
                    if( this.current.HasChildren && this.current.Nodes.FirstNode != null )
                    {
                        if( this.visible == false || this.current.Expanded )
                        {
                            this.current = this.current.Nodes.FirstNode;
                            return true;
                        }
                    }
                    if( this.current == this.firstNode )
                    {
                        this.firstNode = this.firstNode.NextSibling;
                        this.current = this.firstNode;
                        return this.current != null;
                    }
                    if( this.current.NextSibling != null )
                    {
                        this.current = this.current.NextSibling;
                        return true;
                    }
                    
                    while( this.current.Parent != null )
                    {
                        this.current = this.current.Parent;
                        if( this.current == this.firstNode )
                        {
                            this.firstNode = this.firstNode.NextSibling;
                            this.current = this.firstNode;
                            return this.current != null;
                        }
                        if( this.current.NextSibling != null )
                        {
                            this.current = this.current.NextSibling;
                            return true;
                        }
                    }
                    this.current = null;
                    return false;
                }
                
                public void Reset()
                {
                    this.current = null;
                }
                
                public void Dispose()
                {
                    throw new NotImplementedException();
                }
            }
            
            IEnumerator<Node> enumerator = null;
            
            public RecursiveNodesEnumerator( Node firstNode, bool mustBeVisible )
            {
                this.enumerator = new ForwardNodeEnumerator( firstNode, mustBeVisible );
            }

            public RecursiveNodesEnumerator( NodeCollection collection, bool mustBeVisible )
            {
                this.enumerator = new NodeCollectionIterator( collection, mustBeVisible );
            }

            public Node Current
            {
                get { return this.enumerator.Current; }
            }
            
            object IEnumerator.Current
            {
                get { return this.enumerator.Current; }
            }

            public bool MoveNext()
            {
                return this.enumerator.MoveNext();
            }
            
            public void Reset()
            {
                this.enumerator.Reset();
            }

            public void Dispose()
            {
                this.enumerator.Dispose();
            }
        }

        #endregion
        
        #region ForwardNodeEnumerator class
        
        internal class ForwardNodeEnumerator : IEnumerator<Node>
        {
            private Node firstNode;
            private Node current;
            private bool visible;
            
            public ForwardNodeEnumerator( Node firstNode, bool mustBeVisible )
            {
                this.firstNode = firstNode;
                this.visible = mustBeVisible;
            }
            
            public Node Current
            {
                get { return this.current; }
            }
            
            object IEnumerator.Current
            {
                get { return this.current; }
            }
            
            public void Dispose()
            {
            }
            
            public bool MoveNext()
            {
                if( this.firstNode == null )
                    return false;

                if( this.current == null )
                {
                    this.current = this.firstNode;
                    return true;
                }
                if( this.current.HasChildren && this.current.Nodes.FirstNode != null )
                {
                    if( this.visible == false || this.current.Expanded )
                    {
                        this.current = this.current.Nodes.FirstNode;
                        return true;
                    }
                }
                if( this.current.NextSibling != null )
                {
                    this.current = this.current.NextSibling;
                    return true;
                }
                
                while( this.current.Parent != null && this.current.Parent.NextSibling == null )
                {
                    this.current = this.current.Parent;
                }

                if( this.current.Parent != null && this.current.Parent.NextSibling != null )
                {
                    this.current = this.current.Parent.NextSibling;
                    return true;
                }
                this.current = null;
                return false;
            }
            
            public void Reset()
            {
                this.current = this.firstNode;
            }
        }
        #endregion
        
        #region ReverseNodeEnumerator class
        
        internal class ReverseNodeEnumerator : IEnumerator<Node>
        {
            private Node firstNode;
            private Node current;
            private bool visible;
            
            public ReverseNodeEnumerator( Node firstNode, bool mustBeVisible )
            {
                this.firstNode = firstNode;
                this.visible = mustBeVisible;
            }

            public Node Current
            {
                get { return this.current; }
            }

            public void Dispose()
            {
            }

            object IEnumerator.Current
            {
                get { return this.current; }
            }

            public bool MoveNext()
            {
                if( this.firstNode == null )
                    return false;

                if( this.current == null )
                {
                    this.current = this.firstNode;
                    return true;
                }
                if( this.current.PrevSibling != null )
                {
                    this.current = FindNodesBottomLeaf( this.current.PrevSibling, visible );
                    return true;
                }
                if( this.current.Parent != null )
                {
                    this.current = this.current.Parent;
                    return true;
                }
                this.current = null;
                return false;
            }
            public void Reset()
            {
                this.current = this.firstNode;
            }
        }

        #endregion
    }
}
