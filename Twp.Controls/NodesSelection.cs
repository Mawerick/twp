﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Twp.Utilities;

namespace Twp.Controls
{
    /// <summary>
    /// Description of NodesSelection.
    /// </summary>
    public class NodesSelection : IEnumerable
    {
        private List<Node> nodes = new List<Node>();
        private Dictionary<Node,int> nodesMap = new Dictionary<Node, int>();
        
        public void Clear()
        {
            this.nodes.Clear();
            this.nodesMap.Clear();
        }
        
        public IEnumerator GetEnumerator()
        {
            return this.nodes.GetEnumerator();
        }
        
        public Node this[int index]
        {
            get { return this.nodes[index]; }
        }
        
        public int Count
        {
            get { return this.nodes.Count; }
        }
        
        public void Add( Node node )
        {
            this.nodes.Add( node );
            this.nodesMap.Add( node, 0 );
        }
        
        public void Remove( Node node )
        {
            this.nodes.Remove( node );
            this.nodesMap.Remove( node );
        }
        
        public bool Contains( Node node )
        {
            return this.nodesMap.ContainsKey( node );
        }
        
        public IList<Node> GetSortedNodes()
        {
            SortedList<string, Node> list = new SortedList<string, Node>();
            foreach( Node node in this.nodes )
                list.Add( node.GetId(), node );
            return list.Values;
        }
    }
}
