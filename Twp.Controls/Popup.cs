﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    public enum PopupDirection
    {
        Down,
        Left,
        Right,
        Up,
        DownLeft,
        DownRight,
        UpLeft,
        UpRight,
    }

    public class Popup : ToolStripDropDown
    {
        #region Constructor
        
        public Popup()
        {
            this.AutoSize = false;
            this.DoubleBuffered = true;
            this.ResizeRedraw = true;
        }

        public Popup( Control content )
        {
            if( content == null )
                throw new ArgumentNullException( "content" );

            this.AutoSize = false;
            this.DoubleBuffered = true;
            this.ResizeRedraw = true;

            this.Content = content;
        }

        #endregion

        #region Private fields

        private Control content;
        private ToolStripControlHost host;
        private Popup ownerPopup;
        private Popup childPopup;
        private Control owner;

        private bool focusOnOpen = true;
        private bool nonInteractive = false;

        private bool resizable = false;
        private bool resizeLocked = false;
        private bool resizableTop = false;
        private bool resizableLeft = false;

        private Size minimumSize;
        private Size maximumSize;

        private PopupDirection direction = PopupDirection.Down;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the content of the popup.
        /// </summary>
        public Control Content
        {
            get { return this.content; }
            set
            {
                if( this.content != value )
                {
                    if( this.content != null )
                        this.ClearContent();
                    this.content = value;
                    if( this.content != null )
                        this.SetContent();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the content should receive the focus after the popup has been opened.
        /// </summary>
        /// <value><c>true</c> if the content should be focused after the popup has been opened; otherwise, <c>false</c>.</value>
        public bool FocusOnOpen
        {
            get { return this.focusOnOpen; }
            set { this.focusOnOpen = value; }
        }

        public bool NonInteractive
        {
            get { return this.nonInteractive; }
            set
            {
                if( this.nonInteractive != value )
                {
                    this.nonInteractive = value;
                    if( this.IsHandleCreated )
                        this.RecreateHandle();
                }
            }
        }

        public bool Resizable
        {
            get { return (this.resizable && !this.resizeLocked); }
            set { this.resizable = value; }
        }

        public new Size MinimumSize
        {
            get { return this.minimumSize; }
            set { this.minimumSize = value; }
        }

        public new Size MaximumSize
        {
            get { return this.maximumSize; }
            set { this.maximumSize = value; }
        }

        public PopupDirection Direction
        {
            get { return this.direction; }
            set { this.direction = value; }
        }

        protected override CreateParams CreateParams
        {
            [SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= (int) NativeMethods.WS_EX_NOACTIVATE;
                if( this.nonInteractive )
                    cp.ExStyle |= (int) NativeMethods.WS_EX_TRANSPARENT
                        | (int) NativeMethods.WS_EX_LAYERED
                        | (int) NativeMethods.WS_EX_TOOLWINDOW;
                return cp;
            }
        }

        #endregion

        #region Methods

        private void OnContentDisposed( object sender, EventArgs e )
        {
            this.content = null;
            this.Dispose( true );
        }

        private void OnContentRegionChanged( object sender, EventArgs e )
        {
            this.UpdateRegion();
        }

        private void OnContentPaint( object sender, PaintEventArgs e )
        {
            this.PaintSizeGrip( e );
        }

        private void SetContent()
        {
            if( this.content == null )
                return;

            this.host = new ToolStripControlHost( this.content );

            Size contentSize = new Size( this.content.Width - 3, this.content.Height - 4 );
            this.minimumSize = this.content.MinimumSize;
            this.maximumSize = this.content.MaximumSize;
            this.Size = this.content.Size;

            this.content.MinimumSize = contentSize;
            this.content.MaximumSize = contentSize;
            this.content.Size = contentSize;

            this.content.Location = Point.Empty;
            this.Items.Add( host );

            this.content.Disposed += this.OnContentDisposed;
            this.content.RegionChanged += this.OnContentRegionChanged;
            this.content.Paint += this.OnContentPaint;

            this.UpdateRegion();
        }

        private void ClearContent()
        {
            if( this.host != null )
            {
                this.Items.Remove( this.host );
                this.host.Dispose();
                this.host = null;
            }
            if( this.content != null )
            {
                this.content.Disposed -= this.OnContentDisposed;
                this.content.RegionChanged -= this.OnContentRegionChanged;
                this.content.Paint -= this.OnContentPaint;
            }
            this.UpdateRegion();
        }

        protected void UpdateRegion()
        {
            if( this.Region != null )
            {
                this.Region.Dispose();
                this.Region = null;
            }
            if( this.content != null && this.content.Region != null )
            {
                this.Region = this.content.Region.Clone();
            }
        }

        public void Show( Control control )
        {
            if( control == null )
                throw new ArgumentNullException( "control" );

            this.Show( control, control.ClientRectangle );
        }

        public void Show( Control control, Rectangle rect )
        {
            if( control == null )
                throw new ArgumentNullException( "control" );

            this.owner = control;
            this.SetOwnerItem( control );
            this.ShowContent( control, rect );
        }
        
        public void ShowContent( Control control, Rectangle rect )
        {
            if( this.content == null )
                throw new FieldAccessException( "Popup content is null!" );

            Point location;
            ToolStripDropDownDirection realDirection;
            int offset = (this.content.Width - rect.Width) / 2;

            switch( this.direction )
            {
                case PopupDirection.Down:
                    location = control.PointToScreen( new Point( rect.Left - offset, rect.Bottom ) );
                    realDirection = ToolStripDropDownDirection.BelowRight;
                    break;
                case PopupDirection.Left:
                    location = control.PointToScreen( new Point( rect.Left, rect.Top ) );
                    realDirection = ToolStripDropDownDirection.Left;
                    break;
                case PopupDirection.Right:
                    location = control.PointToScreen( new Point( rect.Right, rect.Top ) );
                    realDirection = ToolStripDropDownDirection.Right;
                    break;
                case PopupDirection.Up:
                    location = control.PointToScreen( new Point( rect.Left - offset, rect.Top ) );
                    realDirection = ToolStripDropDownDirection.AboveRight;
                    break;
                case PopupDirection.DownLeft:
                    location = control.PointToScreen( new Point( rect.Right, rect.Bottom ) );
                    realDirection = ToolStripDropDownDirection.BelowLeft;
                    break;
                case PopupDirection.DownRight:
                    location = control.PointToScreen( new Point( rect.Left, rect.Bottom ) );
                    realDirection = ToolStripDropDownDirection.BelowRight;
                    break;
                case PopupDirection.UpLeft:
                    location = control.PointToScreen( new Point( rect.Right, rect.Top ) );
                    realDirection = ToolStripDropDownDirection.AboveLeft;
                    break;
                case PopupDirection.UpRight:
                    location = control.PointToScreen( new Point( rect.Left, rect.Top ) );
                    realDirection = ToolStripDropDownDirection.AboveRight;
                    break;
                default:
                    throw new Exception( String.Format( "Unknown PopupDirection {0}", this.direction ) );
            }

            Rectangle screen = Screen.FromControl( control ).WorkingArea;
            if( location.X < screen.Left )
            {
                location.X = screen.Left;
            }
            else if( location.X + this.Width > screen.Right )
            {
                this.resizableLeft = true;
                location.X = screen.Right - this.Width;
            }
            if( location.Y < screen.Top )
            {
                location.Y = screen.Top;
            }
            else if( location.Y + this.Height > screen.Bottom )
            {
                this.resizableTop = true;
                location.Y = screen.Bottom - this.Height;
            }
            this.Show( control, control.PointToClient( location ), realDirection );
        }

        private void SetOwnerItem( Control control )
        {
            if( control == null )
                return;

            Popup popup = control as Popup;
            if( popup != null )
            {
                this.ownerPopup = popup;
                this.ownerPopup.childPopup = this;
                this.OwnerItem = popup.Items[0];
            }
            else if( control.Parent != null )
            {
                this.SetOwnerItem( control.Parent );
                return;
            }
            Form form = control.FindForm();
            if( form != null )
                form.Move += new EventHandler(form_Move);
        }

        private void form_Move( object sender, EventArgs e )
        {
            if( this.Visible )
                this.ShowContent( this.owner, this.owner.ClientRectangle );
        }

        protected override void OnSizeChanged( EventArgs e )
        {
            if( this.content != null )
            {
                this.content.MinimumSize = this.Size;
                this.content.MaximumSize = this.Size;
                this.content.Size = this.Size;
                this.content.Location = Point.Empty;
            }
            base.OnSizeChanged( e );
        }

        protected override void OnOpening( CancelEventArgs e )
        {
            if( this.content.IsDisposed || this.content.Disposing )
            {
                if( e != null )
                    e.Cancel = true;
                return;
            }
            this.UpdateRegion();
            base.OnOpening( e );
        }

        protected override void OnOpened( EventArgs e )
        {
            if( this.ownerPopup != null )
                this.ownerPopup.resizeLocked = true;
            if( this.focusOnOpen )
                this.content.Focus();
            base.OnOpened( e );
        }

        protected override void OnClosed( ToolStripDropDownClosedEventArgs e )
        {
            if( this.ownerPopup != null )
                this.ownerPopup.resizeLocked = false;
            base.OnClosed( e );
        }

        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if( this.content != null )
                {
                    this.content.Dispose();
                    this.content = null;
                }
                this.ownerPopup = null;
                this.childPopup = null;
            }
            base.Dispose( disposing );
        }

        #endregion

        #region Resize support

        [SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        protected override void WndProc( ref Message m )
        {
            if( this.InternalProcessResizing( ref m, false ) )
                return;
            base.WndProc( ref m );
        }

        [SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        public bool ProcessResizing( ref Message m )
        {
            return this.InternalProcessResizing( ref m, true );
        }

        [SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        private bool InternalProcessResizing( ref Message m, bool contentControl )
        {
            if( m.Msg == NativeMethods.WM_NCACTIVATE && m.WParam != IntPtr.Zero && this.childPopup != null && this.childPopup.Visible )
                this.childPopup.Hide();
            if( !this.resizable && !this.nonInteractive )
                return false;
            if( m.Msg == NativeMethods.WM_NCHITTEST )
                return this.OnNcHitTest( ref m, contentControl );
            else if( m.Msg == NativeMethods.WM_GETMINMAXINFO )
                return this.OnGetMinMaxInfo( ref m );
            return false;
        }

        [SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        private bool OnGetMinMaxInfo( ref Message m )
        {
            NativeMethods.MINMAXINFO minMax = (NativeMethods.MINMAXINFO) Marshal.PtrToStructure( m.LParam, typeof( NativeMethods.MINMAXINFO ) );
            if( !this.MaximumSize.IsEmpty )
                minMax.maxTrackSize = this.MaximumSize;
            minMax.minTrackSize = this.MinimumSize;
            Marshal.StructureToPtr( minMax, m.LParam, false );
            return true;
        }

        private bool OnNcHitTest( ref Message m, bool contentControl )
        {
            if( this.nonInteractive )
            {
                m.Result = (IntPtr) NativeMethods.HTTRANSPARENT;
                return true;
            }

            int x = NativeMethods.LOWORD( m.LParam );
            int y = NativeMethods.HIWORD( m.LParam );
            Point clientLocation = this.PointToClient( new Point( x, y ) );

            GripBounds gripBounds = new GripBounds( contentControl ? this.content.ClientRectangle : this.ClientRectangle );
            IntPtr transparent = new IntPtr( NativeMethods.HTTRANSPARENT );

            if( this.resizableTop )
            {
                if( this.resizableLeft && gripBounds.TopLeft.Contains( clientLocation ) )
                {
                    m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTTOPLEFT;
                    return true;
                }
                if( !this.resizableLeft && gripBounds.TopRight.Contains( clientLocation ) )
                {
                    m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTTOPRIGHT;
                    return true;
                }
                if( gripBounds.Top.Contains( clientLocation ) )
                {
                    m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTTOP;
                    return true;
                }
            }
            else
            {
                if( this.resizableLeft && gripBounds.BottomLeft.Contains( clientLocation ) )
                {
                    m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTBOTTOMLEFT;
                    return true;
                }
                if( !this.resizableLeft && gripBounds.BottomRight.Contains( clientLocation ) )
                {
                    m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTBOTTOMRIGHT;
                    return true;
                }
                if( gripBounds.Bottom.Contains( clientLocation ) )
                {
                    m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTBOTTOM;
                    return true;
                }
            }
            if( this.resizableLeft && gripBounds.Left.Contains( clientLocation ) )
            {
                m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTLEFT;
                return true;
            }
            if( !this.resizableLeft && gripBounds.Right.Contains( clientLocation ) )
            {
                m.Result = contentControl ? transparent : (IntPtr) NativeMethods.HTRIGHT;
                return true;
            }
            return false;
        }

        private System.Windows.Forms.VisualStyles.VisualStyleRenderer sizeGripRenderer;

        public void PaintSizeGrip( PaintEventArgs e )
        {
            if( e == null || e.Graphics == null || !this.resizable )
            {
                return;
            }

            Size clientSize = this.content.ClientSize;

            using( Bitmap gripImage = new Bitmap( 0x10, 0x10 ) )
            {
                using( Graphics g = Graphics.FromImage( gripImage ) )
                {
                    if( Application.RenderWithVisualStyles )
                    {
                        if( this.sizeGripRenderer == null )
                            this.sizeGripRenderer = new System.Windows.Forms.VisualStyles.VisualStyleRenderer( System.Windows.Forms.VisualStyles.VisualStyleElement.Status.Gripper.Normal );
                        this.sizeGripRenderer.DrawBackground( g, new Rectangle( 0, 0, 0x10, 0x10 ) );
                    }
                    else
                    {
                        ControlPaint.DrawSizeGrip( g, this.childPopup.BackColor, 0, 0, 0x10, 0x10 );
                    }
                }
                GraphicsState gs = e.Graphics.Save();
                e.Graphics.ResetTransform();
                if( this.resizableTop )
                {
                    if( this.resizableLeft )
                    {
                        e.Graphics.RotateTransform( 180 );
                        e.Graphics.TranslateTransform( -clientSize.Width, -clientSize.Height );
                    }
                    else
                    {
                        e.Graphics.ScaleTransform( 1, -1 );
                        e.Graphics.TranslateTransform( 0, -clientSize.Height );
                    }
                }
                else if( this.resizableLeft )
                {
                    e.Graphics.ScaleTransform( -1, 1 );
                    e.Graphics.TranslateTransform( -clientSize.Width, 0 );
                }
                e.Graphics.DrawImage( gripImage, clientSize.Width - 0x10, clientSize.Height - 0x10 );
                e.Graphics.Restore( gs );
            }
        }

        #endregion

        #region Helper classes

        internal class GripBounds
        {
            private const int GripSize = 6;
            private const int CornerGripSize = GripSize << 1;

            public GripBounds( Rectangle clientRectangle )
            {
                this.clientRectangle = clientRectangle;
            }

            private Rectangle clientRectangle;
            public Rectangle ClientRectangle
            {
                get { return this.clientRectangle; }
            }

            public Rectangle Bottom
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Y = rect.Bottom - GripSize + 1;
                    rect.Height = GripSize;
                    return rect;
                }
            }

            public Rectangle Left
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Width = GripSize;
                    return rect;
                }
            }

            public Rectangle Right
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.X = rect.Right - GripSize + 1;
                    rect.Height = GripSize;
                    return rect;
                }
            }

            public Rectangle Top
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Height = GripSize;
                    return rect;
                }
            }

            public Rectangle BottomLeft
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Y = rect.Bottom - CornerGripSize + 1;
                    rect.Height = CornerGripSize;
                    rect.Width = CornerGripSize;
                    return rect;
                }
            }

            public Rectangle BottomRight
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Y = rect.Bottom - CornerGripSize + 1;
                    rect.Height = CornerGripSize;
                    rect.X = rect.Right - CornerGripSize + 1;
                    rect.Width = CornerGripSize;
                    return rect;
                }
            }

            public Rectangle TopLeft
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Height = CornerGripSize;
                    rect.Width = CornerGripSize;
                    return rect;
                }
            }

            public Rectangle TopRight
            {
                get
                {
                    Rectangle rect = this.clientRectangle;
                    rect.Height = CornerGripSize;
                    rect.X = rect.Right - CornerGripSize + 1;
                    rect.Width = CornerGripSize;
                    return rect;
                }
            }
        }
        #endregion
    }
}
