﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Controls
{
    [DefaultProperty( "Orientation" )]
    public class PopupButton : Button
    {
        #region Constructor

        public PopupButton()
        {
            //
            // popup
            //
            this.popup = new Popup();
            this.popup.AutoClose = false;
            this.popup.Closed += delegate { this.Open = false; };
            this.popup.Opened += delegate { this.Open = true; };
        }

        #endregion

        #region Private fields

        private Popup popup;
        private bool open = false;

        #endregion

        #region Properties

        [Category( "Appearance" )]
        [Description( "Defines in which direction the popup opens." )]
        [DefaultValue( PopupDirection.Down )]
        public PopupDirection PopupDirection
        {
            get { return this.popup.Direction; }
            set { this.popup.Direction = value; }
        }

        [Browsable( false )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Control Content
        {
            get { return this.popup.Content; }
            set { this.popup.Content = value; }
        }

        [Browsable( false )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool Open
        {
            get { return this.open; }
            set { this.open = value; }
        }

        #endregion

        #region Methods

        protected override void OnClick( EventArgs e )
        {
            base.OnClick( e );
            Log.Debug( "[PopupButton.OnClick] Are we open? {0}", this.open );
            if( !this.open )
                this.popup.Show( this );
            else
                this.popup.Close();
        }

        #endregion
    }
}
