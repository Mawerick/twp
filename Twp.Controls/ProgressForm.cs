﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Description of the ProgressForm class.
    /// </summary>
    public partial class ProgressForm : Form
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressForm"/> class.
        /// </summary>
        public ProgressForm()
        {
            InitializeComponent();
            _overall.PropertyChanged += OnOverallPropertyChanged;
        }

        #endregion

        #region Fields

        int _autoCloseDelay = 0;
        int _autoCloseRemaining = 0;
        bool _cancelled = false;
        bool _finished = false;

        ProgressTask _task;
        ProgressTask _overall = new ProgressTask( "Overall progress", 100, 100, false );
        double _pastProgress = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value representing how long the <see cref="ProgressForm"/> will remain on screen before closing itself once all tasks are finished.
        /// </summary>
        /// <remarks>
        /// If this value is 0, auto-closing is disabled. This is the default behavior.
        /// </remarks>
        [Category( "Behavior" )]
        [Description( "Defines how long the ProgressForm will remain on-screen before closing itself, once all tasks are finished." )]
        [DefaultValue( 0 )]
        public int AutoCloseDelay
        {
            get { return _autoCloseDelay; }
            set { _autoCloseDelay = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Cancel button is enabled.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Sets whether or not the cancel button is enabled." )]
        [DefaultValue( false )]
        public bool CanBeCancelled
        {
            get { return cancelButton.Enabled; }
            set { cancelButton.Enabled = value; }
        }

        /// <summary>
        /// Returns true if the user has clicked the 'cancel' button. Otherwise, returns false.
        /// </summary>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool Cancelled
        {
            get { return _cancelled; }
        }

        public string Status
        {
            get { return statusLabel.Text; }
            set { statusLabel.Text = value; }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the user presses the Cancel button.
        /// </summary>
        [Category( "Action" )]
        [Description( "Occurs when the user presses the Cancel button." )]
        public event EventHandler Cancel;

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new task.
        /// </summary>
        /// <param name="text">The text that should be displayed, describing the current task.</param>
        /// <param name="maximum">The maximum value of the task.</param>
        /// <param name="percent">The overall percentage of this task.</param>
        public ProgressTask AddTask( string text, int maximum, double percent )
        {
            ProgressTask task = new ProgressTask( text, maximum, percent );
            AddTask( task );
            return task;
        }

        /// <summary>
        /// Adds a new task.
        /// </summary>
        /// <param name="text">The text that should be displayed, describing the current task.</param>
        /// <param name="maximum">The maximum value of the task.</param>
        /// <param name="percent">The overall percentage of this task.</param>
        /// <param name="showStepCount"></param>
        public ProgressTask AddTask( string text, int maximum, double percent, bool showStepCount )
        {
            ProgressTask task = new ProgressTask( text, maximum, percent, showStepCount );
            AddTask( task );
            return task;
        }

        /// <summary>
        /// Adds a new task.
        /// </summary>
        /// <param name="task">The <see cref="ProgressTask"/> to add.</param>
        public void AddTask( ProgressTask task )
        {
            if( _task != null )
            {
                if( !_task.Finished )
                    _task.Finish();
                _task.PropertyChanged -= OnTaskPropertyChanged;
            }
            _task = task;
            if( _task != null )
            {
                task.PropertyChanged += OnTaskPropertyChanged;
            }
            taskLabel.Text = _task.Text;
            taskProgressBar.Value = 0;
            taskProgressBar.Maximum = _task.Maximum;
        }

        public void Finish()
        {
            _finished = true;
            statusLabel.Text = "Done.";
            cancelButton.Text = "Close";
            if( _autoCloseDelay > 0 )
            {
                cancelButton.Text += " (" + _autoCloseDelay + ")";
                autoCloseTimer.Start();
            }
        }

        void OnTaskPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( InvokeRequired )
            {
                BeginInvoke( new PropertyChangedEventHandler( OnTaskPropertyChanged ), sender, e );
                return;
            }

            if( IsDisposed )
                return;

            taskLabel.Text = _task.FullText;
            taskProgressBar.Value = _task.Value;
            if( _task.Finished )
            {
                _pastProgress += _task.Percent;
                _overall.Value = (int) _pastProgress;
            }
            else
                _overall.Value = (int) ( _pastProgress + ( (double) _task.Value * _task.Percent / 100.0f ) );
        }

        void OnOverallPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( InvokeRequired )
            {
                BeginInvoke( new PropertyChangedEventHandler( OnOverallPropertyChanged ), sender, e );
                return;
            }

            if( IsDisposed )
                return;

            progressBar.Value = _overall.Value;
            progressLabel.Text = _overall.FullText + _overall.PercentText;
        }

        void OnCancel( object sender, EventArgs e )
        {
            if( _finished )
            {
                autoCloseTimer.Stop();
                Close();
            }
            else
            {
                _cancelled = true;
                if( Cancel != null )
                    Cancel( this, EventArgs.Empty );
            }
        }

        void OnAutoCloseTimerTick( object sender, EventArgs e )
        {
            cancelButton.Text = String.Format( "Close ({0})", --_autoCloseRemaining );
            if( _autoCloseRemaining <= 0 )
                Close();
        }

        #endregion
    }
}
