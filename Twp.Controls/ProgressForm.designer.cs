﻿namespace Twp.Controls
{
	partial class ProgressForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		    this.components = new System.ComponentModel.Container();
		    this.progressBar = new System.Windows.Forms.ProgressBar();
		    this.progressLabel = new System.Windows.Forms.Label();
		    this.taskProgressBar = new System.Windows.Forms.ProgressBar();
		    this.taskLabel = new System.Windows.Forms.Label();
		    this.cancelButton = new System.Windows.Forms.Button();
		    this.statusLabel = new System.Windows.Forms.Label();
		    this.line1 = new Twp.Controls.Line();
		    this.line2 = new Twp.Controls.Line();
		    this.line3 = new Twp.Controls.Line();
		    this.autoCloseTimer = new System.Windows.Forms.Timer(this.components);
		    this.SuspendLayout();
		    // 
		    // progressBar
		    // 
		    this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
		    this.progressBar.Location = new System.Drawing.Point(12, 111);
		    this.progressBar.Name = "progressBar";
		    this.progressBar.Size = new System.Drawing.Size(470, 20);
		    this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
		    this.progressBar.TabIndex = 3;
		    // 
		    // progressLabel
		    // 
		    this.progressLabel.Location = new System.Drawing.Point(12, 93);
		    this.progressLabel.Name = "progressLabel";
		    this.progressLabel.Size = new System.Drawing.Size(470, 15);
		    this.progressLabel.TabIndex = 2;
		    this.progressLabel.Text = "Overall progress: 0%";
		    // 
		    // taskProgressBar
		    // 
		    this.taskProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
		    this.taskProgressBar.Location = new System.Drawing.Point(12, 56);
		    this.taskProgressBar.Maximum = 10;
		    this.taskProgressBar.Name = "taskProgressBar";
		    this.taskProgressBar.Size = new System.Drawing.Size(470, 20);
		    this.taskProgressBar.Step = 1;
		    this.taskProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
		    this.taskProgressBar.TabIndex = 1;
		    // 
		    // taskLabel
		    // 
		    this.taskLabel.Location = new System.Drawing.Point(12, 38);
		    this.taskLabel.Name = "taskLabel";
		    this.taskLabel.Size = new System.Drawing.Size(470, 15);
		    this.taskLabel.TabIndex = 0;
		    this.taskLabel.Text = "...";
		    // 
		    // cancelButton
		    // 
		    this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		    this.cancelButton.Enabled = false;
		    this.cancelButton.Location = new System.Drawing.Point(210, 149);
		    this.cancelButton.Name = "cancelButton";
		    this.cancelButton.Size = new System.Drawing.Size(75, 23);
		    this.cancelButton.TabIndex = 4;
		    this.cancelButton.Text = "&Cancel";
		    this.cancelButton.UseVisualStyleBackColor = true;
		    this.cancelButton.Click += new System.EventHandler(this.OnCancel);
		    // 
		    // statusLabel
		    // 
		    this.statusLabel.Location = new System.Drawing.Point(12, 9);
		    this.statusLabel.Name = "statusLabel";
		    this.statusLabel.Size = new System.Drawing.Size(470, 15);
		    this.statusLabel.TabIndex = 5;
		    this.statusLabel.Text = "...";
		    // 
		    // line1
		    // 
		    this.line1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
		    this.line1.Location = new System.Drawing.Point(0, 25);
		    this.line1.Name = "line1";
		    this.line1.Size = new System.Drawing.Size(494, 10);
		    this.line1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
		    // 
		    // line2
		    // 
		    this.line2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
		    this.line2.Location = new System.Drawing.Point(0, 80);
		    this.line2.Name = "line2";
		    this.line2.Size = new System.Drawing.Size(494, 10);
		    this.line2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
		    // 
		    // line3
		    // 
		    this.line3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
		    this.line3.Location = new System.Drawing.Point(0, 135);
		    this.line3.Name = "line3";
		    this.line3.Size = new System.Drawing.Size(494, 10);
		    this.line3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
		    // 
		    // timer
		    // 
		    this.autoCloseTimer.Interval = 1000;
		    this.autoCloseTimer.Tick += new System.EventHandler(this.OnAutoCloseTimerTick);
		    // 
		    // ProgressForm
		    // 
		    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		    this.ClientSize = new System.Drawing.Size(494, 181);
		    this.Controls.Add(this.line3);
		    this.Controls.Add(this.line2);
		    this.Controls.Add(this.line1);
		    this.Controls.Add(this.statusLabel);
		    this.Controls.Add(this.cancelButton);
		    this.Controls.Add(this.taskLabel);
		    this.Controls.Add(this.taskProgressBar);
		    this.Controls.Add(this.progressLabel);
		    this.Controls.Add(this.progressBar);
		    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
		    this.Name = "ProgressForm";
		    this.ShowInTaskbar = false;
		    this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
		    this.Text = "Progress...";
		    this.TopMost = true;
		    this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Label progressLabel;
		private System.Windows.Forms.ProgressBar taskProgressBar;
		private System.Windows.Forms.Label taskLabel;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label statusLabel;
		private Line line1;
		private Line line2;
		private Line line3;
		private System.Windows.Forms.Timer autoCloseTimer;
	}
}