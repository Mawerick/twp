﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Controls
{
    public partial class ProgressIndicator : Control
    {
        #region Constructor

        public ProgressIndicator()
        {
            InitializeComponent();
            this.SetStyle( ControlStyles.OptimizedDoubleBuffer |
                           ControlStyles.AllPaintingInWmPaint |
                           ControlStyles.UserPaint |
                           ControlStyles.ResizeRedraw |
                           ControlStyles.SupportsTransparentBackColor, true );

            if( this.AutoStart )
                this.timer.Start();
        }
        
        #endregion
        
        #region Private Fields

        private Color circleColor = Color.FromArgb( 20, 20, 20 );
        private float circleSize = 1.0F;
        private uint circles = 8;
        private uint visibleCircles = 8;

        private int value = 1;
        private int interval = 100;
        private bool autoStart = false;
        private bool stopped = true;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the base color for the circles.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The base color for the circles." )]
        [DefaultValue( typeof( Color ), "20, 20, 20" )]
        public Color CircleColor
        {
            get { return this.circleColor; }
            set
            {
                if( this.circleColor != value )
                {
                    this.circleColor = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the scale of the circles, ranging from 0.1 to 1.0.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "The scale of the circles, ranging from 0.1 to 1.0" )]
        [DefaultValue( 1.0F )]
        public float CircleSize
        {
            get { return this.circleSize; }
            set
            {
                if( this.circleSize != value )
                {
                    if( value < 0.1F )
                        this.circleSize = 0.1F;
                    else if( value > 1.0F )
                        this.circleSize = 1.0F;
                    else
                        this.circleSize = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of circles used in the animation.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "The number of circles used in the animation." )]
        [DefaultValue( 8 )]
        public uint Circles
        {
            get { return this.circles; }
            set
            {
                if( this.circles != value )
                {
                    this.circles = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of visible circles used in the animation.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "The number of visible circles used in the animation." )]
        [DefaultValue( 8 )]
        public uint VisibleCircles
        {
            get { return this.visibleCircles; }
            set
            {
                if( this.visibleCircles != value )
                {
                    if( value > this.circles )
                        throw new ArgumentOutOfRangeException( "value", "Number of visible circles cannot be higher than Circles." );

                    this.visibleCircles = value;
                    this.Invalidate();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if the animation should start automatically.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Indicates if the animation should start automatically." )]
        [DefaultValue( false )]
        public bool AutoStart
        {
            get { return this.autoStart; }
            set
            {
                if( this.autoStart != value )
                {
                    this.autoStart = value;
                    if( this.autoStart && !this.DesignMode )
                        this.Start();
                    else
                        this.Stop();
                }
            }
        }

        /// <summary>
        /// Gets or sets the animation speed.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "The animation speed." )]
        [DefaultValue( 75 )]
        public int AnimationSpeed
        {
            get { return (-this.interval + 400) / 4; }
            set
            {
                checked
                {
                    int i = 400 - (value * 4);
                    if( i < 10 )
                        this.interval = 10;
                    else if( i > 400 )
                        this.interval = 400;
                    else
                        this.interval = i;

                    this.timer.Interval = this.interval;
                }
            }
        }

        #endregion

        #region Methods

        public void Start()
        {
            this.timer.Interval = this.interval;
            this.stopped = false;
            this.timer.Start();
        }

        public void Stop()
        {
            this.timer.Stop();
            this.value = 1;
            this.stopped = true;
            this.Invalidate();
        }

        #endregion

        #region Overrides

        protected override void OnPaint( PaintEventArgs e )
        {
            float angle = 360.0F / this.circles;
            GraphicsState state = e.Graphics.Save();
            
            e.Graphics.TranslateTransform( this.Width / 2.0F, this.Height / 2.0F );
            e.Graphics.RotateTransform( angle * this.value );
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            for( int i = 1; i <= this.circles; i++ )
            {
                int alphaValue = (int)(255.0F * (i / (float) this.visibleCircles));
                if( alphaValue > 255 )
                    alphaValue = 0;
                int alpha = this.stopped ? (int)(255.0F / 8.0F) : alphaValue;

                Color drawColor = Color.FromArgb( alpha, this.circleColor );
                using( SolidBrush brush = new SolidBrush( drawColor ) )
                {
                    float sizeRate = 4.5F / this.circleSize;
                    float size = this.Width / sizeRate;
                    float diff = (this.Width / 4.5F) - size;
                    float x = (this.Width / 9.0F) + diff;
                    float y = (this.Height / 9.0F) + diff;
                    e.Graphics.FillEllipse( brush, x, y, size, size );
                    e.Graphics.RotateTransform( angle );
                }
            }
            
            e.Graphics.Restore( state );
            base.OnPaint( e );
        }

        protected override void OnResize( EventArgs e )
        {
            this.SetNewSize();
            base.OnResize(e);
        }
        
        protected override void OnSizeChanged( EventArgs e )
        {
            this.SetNewSize();
            base.OnSizeChanged( e );
        }

        #endregion

        #region Private Methods

        private void SetNewSize()
        {
            int size = Math.Max( this.Width, this.Height );
            this.Size = new Size( size, size );
        }

        private void IncreaseValue()
        {
            if( this.value + 1 <= this.circles )
                this.value++;
            else
                this.value = 1;
        }
        
        private void OnTimerTick( object sender, EventArgs e )
        {
            if( !this.DesignMode )
            {
                this.IncreaseValue();
                this.Invalidate();
            }
        }

        #endregion
    }
}
