﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    /// <summary>
    /// Description of ProgressListForm.
    /// </summary>
    public partial class ProgressListForm : Form
    {
        public ProgressListForm()
        {
            InitializeComponent();
        }

        #region Fields

        int _autoCloseDelay = 0;
        int _autoCloseRemaining = 0;
        bool _cancelled = false;
        bool _finished = false;
        bool _manualFinish = false;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value representing how long the <see cref="ProgressListForm"/> will remain on screen before closing itself once all tasks are finished.
        /// </summary>
        /// <remarks>
        /// If this value is 0, auto-closing is disabled. This is the default behavior.
        /// </remarks>
        [Category( "Behavior" )]
        [Description( "Defines how long the ProgressListForm will remain on-screen before closing itself, once all tasks are finished." )]
        [DefaultValue( 0 )]
        public int AutoCloseDelay
        {
            get { return _autoCloseDelay; }
            set { _autoCloseDelay = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Cancel button is enabled.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Sets whether or not the cancel button is enabled." )]
        [DefaultValue( false )]
        public bool CanBeCancelled
        {
            get { return cancelButton.Enabled; }
            set { cancelButton.Enabled = value; }
        }

        /// <summary>
        /// Returns true if the user has clicked the 'cancel' button. Otherwise, returns false.
        /// </summary>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool Cancelled
        {
            get { return _cancelled; }
        }

        /// <summary>
        /// Gets or sets a value determining if the <see cref="ProgressListForm.Finish"/> method must be
        /// called manually.
        /// If false, <see cref="ProgressListForm.Finish"/> will be called automatically when all added
        /// tasks are complete.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Determins whether or not the Finish method must be called manually." )]
        [DefaultValue( false )]
        public bool ManualFinish
        {
            get { return _manualFinish; }
            set { _manualFinish = value; }
        }

        public string Status
        {
            get { return statusLabel.Text; }
            set { statusLabel.Text = value; }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the user presses the Cancel button.
        /// </summary>
        [Category( "Action" )]
        [Description( "Occurs when the user presses the Cancel button." )]
        public event EventHandler Cancel;

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new task.
        /// </summary>
        /// <param name="text">The text that should be displayed in the top label, describing the current task.</param>
        /// <param name="maximum">The maximum value of the task.</param>
        /// <param name="percent">The overall percentage of this task.</param>
        public ProgressTask AddTask( string text, int maximum, double percent )
        {
            ProgressTask task = new ProgressTask( text, maximum, percent );
            AddTask( task );
            return task;
        }

        /// <summary>
        /// Adds a new task.
        /// </summary>
        /// <param name="text">The text that should be displayed in the top label, describing the current task.</param>
        /// <param name="maximum">The maximum value of the task.</param>
        /// <param name="percent">The overall percentage of this task.</param>
        /// <param name="showStepCount"></param>
        public ProgressTask AddTask( string text, int maximum, double percent, bool showStepCount )
        {
            ProgressTask task = new ProgressTask( text, maximum, percent, showStepCount );
            AddTask( task );
            return task;
        }

        public void AddTask( ProgressTask task )
        {
            int index = tasksBox.Tasks.Add( task );
            tasksBox.EnsureVisible( index );
            task.PropertyChanged += OnTaskPropertyChanged;
            UpdateTasks();
        }

        public void Finish()
        {
            if( _finished )
                return;

            _finished = true;
            statusLabel.Text = "Done.";
            cancelButton.Text = "&Close";
            cancelButton.Enabled = true;
            if( _autoCloseDelay > 0 )
            {
                _autoCloseRemaining = _autoCloseDelay;
                cancelButton.Text += " (" + _autoCloseDelay + ")";
                autoCloseTimer.Start();
            }
        }

        public void UpdateTasks()
        {
            int tasks = 0;
            double value = 0.0f;

            foreach( ProgressTask task in tasksBox.Tasks )
            {
                if( task.Finished )
                    value += task.Percent;
                else
                {
                    tasks++;
                    value += ( (double) task.Value / (double) task.Maximum ) * task.Percent;
                }
            }
            
            if( value > 100.0f )
                value = 100.0f;

            progressLabel.Text = String.Format( "{0}%", (int) value );
            progressBar.Value = (int) value;

            if( tasks == 0 )
            {
                if( !_manualFinish )
                    Finish();
            }
        }

        void OnTaskPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if( InvokeRequired )
            {
                BeginInvoke( new PropertyChangedEventHandler( OnTaskPropertyChanged ), sender, e );
                return;
            }

            if( IsDisposed )
                return;

            int index = tasksBox.Tasks.IndexOf( (ProgressTask) sender );
            if( index < 0 )
                return;

            tasksBox.InvalidateTask( index );

            UpdateTasks();
        }

        void OnCancelClicked( object sender, EventArgs e )
        {
            if( _finished )
            {
                autoCloseTimer.Stop();
                Close();
            }
            else
            {
                _cancelled = true;
                if( Cancel != null )
                    Cancel( this, EventArgs.Empty );
            }
        }

        void OnAutoCloseTimerTick( object sender, EventArgs e )
        {
            cancelButton.Text = String.Format( "Close ({0})", --_autoCloseRemaining );
            if( _autoCloseRemaining <= 0 )
                Close();
        }

        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            tasksBox.Refresh();
        }

        #endregion
    }

    
}
