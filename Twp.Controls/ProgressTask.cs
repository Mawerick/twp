﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    public class ProgressTask : INotifyPropertyChanged
    {
        #region Constructor

        public ProgressTask( string text, int maximum, double percent )
            : this( text, maximum, percent, true )
        {
        }

        public ProgressTask( string text, int maximum, double percent, bool showStepCount )
        {
            Log.Debug( "[ProgressTask] Text: {0} Maximum: {1} Percent: {2} ShowStepCount: {3}",
                       text, maximum, percent, showStepCount );
            _text = text;
            _value = 0;
            if( maximum <= 0 )
                _maximum = 1;
            else
                _maximum = maximum;
            _percent = percent;
            _showStepCount = showStepCount;
            _finished = false;
        }

        #endregion

        #region Fields

        string _text;
        int _value;
        int _maximum;
        readonly double _percent;
        readonly bool _showStepCount;
        bool _finished;

        #endregion

        #region Properties

        public string Text
        {
            get { return _text; }
            set
            {
                if( _text != value )
                {
                    _text = value;
                    OnPropertyChanged( new PropertyChangedEventArgs( "Text" ) );
                }
            }
        }

        public int Value
        {
            get { return _value; }
            set
            {
                if( _value != value && value > 0 )
                {
                    _value = value;
                    if( _value > _maximum )
                        _value = _maximum;
                    OnPropertyChanged( new PropertyChangedEventArgs( "Value" ) );
                }
            }
        }

        public int Maximum
        {
            get { return _maximum; }
            set
            {
                if( _maximum != value && value > 0 )
                {
                    _maximum = value;
                    if( _value > _maximum )
                        _value = _maximum;
                    OnPropertyChanged( new PropertyChangedEventArgs( "Maximum" ) );
                    _finished = false;
                }
            }
        }

        public double Percent
        {
            get { return _percent; }
        }

        internal string FullText
        {
            get
            {
                string text = _text;
                if( _showStepCount )
                {
                    text += ": ";
                    if( _finished )
                        text += " Done.";
                    else if( _maximum > 0 )
                        text += _value + " / " + _maximum;
                }
                return text;
            }
        }

        internal string PercentText
        {
            get
            {
                int pct = (int) ( (double) _value / (double) _maximum * 100.0f );
                return pct + "%";
            }
        }

        public bool Finished
        {
            get { return _finished; }
            internal set
            {
                if( _finished != value )
                {
                    _finished = value;
                    OnPropertyChanged( new PropertyChangedEventArgs( "Finished" ) );
                }
            }
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            if( PropertyChanged != null )
                PropertyChanged( this, e );
        }

        #endregion

        #region Methods
        
        public void Finish()
        {
            if( _finished )
                return;

            _value = _maximum;
            Finished = true;
        }

        #endregion
    }
}
