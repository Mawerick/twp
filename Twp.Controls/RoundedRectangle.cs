﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Twp.Controls
{
	public enum RectangleCorners
	{
		None = 0,
		TopLeft = 1,
		TopRight = 2,
		BottomLeft = 4,
		BottomRight = 8,
		Top = TopLeft | TopRight,
		Bottom = BottomLeft | BottomRight,
		Left = TopLeft | BottomLeft,
		Right = TopRight | BottomRight,
		All = TopLeft | TopRight | BottomLeft | BottomRight
	}

	public static class RoundedRectangle
	{
		public static GraphicsPath Create( float x, float y, float width, float height, float radius )
		{
			return Create( new RectangleF( x, y, width, height ), radius, RectangleCorners.All );
		}

		public static GraphicsPath Create( float x, float y, float width, float height, float radius, RectangleCorners corners )
		{
			return Create( new RectangleF( x, y, width, height ), radius, corners );
		}

		public static GraphicsPath Create( int x, int y, int width, int height, int radius )
		{
			return Create( Convert.ToSingle( x ), Convert.ToSingle( y ),
						   Convert.ToSingle( width ), Convert.ToSingle( height ),
						   Convert.ToSingle( radius ), RectangleCorners.All );
		}

		public static GraphicsPath Create( int x, int y, int width, int height, int radius, RectangleCorners corners )
		{
			return Create( Convert.ToSingle( x ), Convert.ToSingle( y ),
						   Convert.ToSingle( width ), Convert.ToSingle( height ),
						   Convert.ToSingle( radius ), corners );
		}

		public static GraphicsPath Create( Rectangle rect, int radius )
		{
			return Create( rect.X, rect.Y, rect.Width, rect.Height, radius, RectangleCorners.All );
		}

		public static GraphicsPath Create( Rectangle rect, int radius, RectangleCorners corners )
		{
			return Create( rect.X, rect.Y, rect.Width, rect.Height, radius, corners );
		}

		public static GraphicsPath Create( RectangleF rect, float radius )
		{
			return Create( rect, radius, RectangleCorners.All );
		}

		public static GraphicsPath Create( RectangleF rect, float radius, RectangleCorners corners )
		{
			GraphicsPath path = new GraphicsPath();
			if( radius <= 0.0F || corners == RectangleCorners.None )
			{
				path.AddRectangle( rect );
			}
			else
			{
				float diameter = radius * 2.0F;
				SizeF sizeF = new SizeF( diameter, diameter );
				RectangleF arc = new RectangleF( rect.Location, sizeF );

				if( ( corners & RectangleCorners.TopLeft ) != 0 )
				{
					path.AddArc( arc, 180, 90 );
				}
				else
				{
                    path.AddLine( arc.Left, arc.Bottom + radius, arc.Left, arc.Top );
                    path.AddLine( arc.Left, arc.Top, arc.Right - radius, arc.Top );
				}
				arc.X = rect.Right - diameter;
				if( ( corners & RectangleCorners.TopRight ) != 0 )
				{
					path.AddArc( arc, 270, 90 );
				}
				else
				{
                    path.AddLine( arc.Left + radius, arc.Top, arc.Right, arc.Top );
                    path.AddLine( arc.Right, arc.Top, arc.Right, arc.Bottom - radius );
				}
				arc.Y = rect.Bottom - diameter;
				if( ( corners & RectangleCorners.BottomRight ) != 0 )
				{
					path.AddArc( arc, 0, 90 );
				}
				else
				{
                    path.AddLine( arc.Right, arc.Top + radius, arc.Right, arc.Bottom );
                    path.AddLine( arc.Right, arc.Bottom, arc.Left + radius, arc.Bottom );
				}
				arc.X = rect.Left;
				if( ( corners & RectangleCorners.BottomLeft ) != 0 )
				{
					path.AddArc( arc, 90, 90 );
				}
				else
				{
                    path.AddLine( arc.Right - radius, arc.Bottom, arc.Left, arc.Bottom );
                    path.AddLine( arc.Left, arc.Bottom, arc.Left, arc.Top + radius );
				}
			}
			path.CloseFigure();
			return path;
		}
	}
}
