﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Description of SimpleProgressForm.
    /// </summary>
    public partial class SimpleProgressForm : Form
    {
        public SimpleProgressForm()
        {
            InitializeComponent();
        }

        public string Label
        {
            get { return this.label.Text; }
            set { this.label.Text = value; }
        }

        public Color ProgressColor
        {
            get { return this.progressIndicator.CircleColor; }
            set { this.progressIndicator.CircleColor = value; }
        }

        public bool AutoStart
        {
            get { return this.progressIndicator.AutoStart; }
            set { this.progressIndicator.AutoStart = value; }
        }

        public void Start()
        {
            this.progressIndicator.Start();
        }

        public void Stop()
        {
            this.progressIndicator.Stop();
        }

        public new void Show( IWin32Window owner )
        {
            base.Show( owner );
            if( this.Owner != null && this.StartPosition == FormStartPosition.CenterParent )
            {
                this.Location = new Point( this.Owner.Location.X + (this.Owner.Width / 2) - (this.Width / 2),
                                           this.Owner.Location.Y + (this.Owner.Height / 2) - (this.Height / 2) );
            }
        }

        private void OnPaint( object sender, PaintEventArgs e )
        {
            ControlRenderer.DrawBorder( e.Graphics, this.DisplayRectangle, this.BackColor, Border3DStyle.Raised );
        }
    }
}
