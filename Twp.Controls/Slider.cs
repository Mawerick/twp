﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Twp.Controls
{
    /// <summary>
    /// Specifies the position of the label in relation to the slider.
    /// </summary>
    public enum LabelPosition
    {
        /// <summary>
        /// The label is positioned before the slider.
        /// </summary>
        LabelBeforeSlider,
        /// <summary>
        /// The label is positioned after the slider.
        /// </summary>
        SliderBeforeLabel,
        /// <summary>
        /// The label is hidden.
        /// </summary>
        Disabled,
    }

    /// <summary>
    /// 
    /// </summary>
    [Description( "Slider represents an advanced trackbar." )]
    [Designer( typeof( Design.SliderDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
    [ToolboxBitmap( typeof( Slider ) )]
    [DefaultProperty( "Value" )]
    [DefaultEvent( "ValueChanged" )]
    public class Slider : Control
    {

#region Constructor

        public Slider()
        {
            this.SetStyle( 
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.SupportsTransparentBackColor,
                true );
        }

#endregion

#region Private fields

        private decimal value = 0m;
        private decimal minimum = 0m;
        private decimal maximum = 100m;

        private decimal step = 1m;
        private decimal pageStep = 10m;

        private decimal tickFrequency = 5m;
        private int decimalPlaces = 0;
        private Orientation orientation = Orientation.Horizontal;
        private int spacing = 4;
        private bool autoSize = true;
        private LabelPosition labelPosition = LabelPosition.SliderBeforeLabel;
        private bool showButtons = true;

        private Rectangle labelRect = Rectangle.Empty;
        private Rectangle focusRect = Rectangle.Empty;
        private SliderTracker tracker = new SliderTracker();
        private SliderButton minButton = new SliderButton();
        private SliderButton maxButton = new SliderButton();

        private bool leftButtonDown = false;
        private float mouseStartPos = -1;
        private MouseHoverElements mouseHoverElement = MouseHoverElements.None;

#endregion

#region Properties

        [Category( "Behavior" )]
        [Description( "The value for the position of the slider" )]
        [DefaultValue( typeof( decimal ), "0" )]
        public decimal Value
        {
            get { return this.value; }
            set
            {
                if( this.value != value )
                {
                    if( value < this.minimum )
                        this.value = this.minimum;
                    else if( value > this.maximum )
                        this.value = this.maximum;
                    else
                        this.value = value;

                    this.OnValueChanged();
                    this.ResizeTrackerButton();
                    this.Invalidate();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "The minimum value for the position of the slider" )]
        [DefaultValue( typeof( decimal ), "0" )]
        public decimal Minimum
        {
            get { return this.minimum; }
            set
            {
                if( this.minimum != value )
                {
                    this.minimum = value;
                    if( this.minimum > this.maximum )
                        this.maximum = this.minimum;
                    if( this.minimum > this.value )
                        this.value = this.minimum;

                    this.Invalidate();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "The maximum value for the position of the slider" )]
        [DefaultValue( typeof( decimal ), "100" )]
        public decimal Maximum
        {
            get { return this.maximum; }
            set
            {
                if( this.maximum != value )
                {
                    this.maximum = value;
                    if( this.maximum < this.value )
                        this.value = this.maximum;
                    if( this.maximum < this.minimum )
                        this.minimum = this.maximum;

                    this.ResizeContent();
                    this.Invalidate();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "The number of positions the slider moves in response to keyboard input (arrow keys)" )]
        [DefaultValue( typeof( decimal ), "1" )]
        public decimal Step
        {
            get { return this.step; }
            set
            {
                if( this.step != value )
                {
                    this.step = value;
                    if( this.step < 1 )
                        this.step = 1;

                    this.Invalidate();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "The number of positions the slider moves in response to mouse clicks or the PAGE UP and PAGE DOWN keys" )]
        [DefaultValue( typeof( decimal ), "10" )]
        public decimal PageStep
        {
            get { return this.pageStep; }
            set
            {
                if( this.pageStep != value )
                {
                    this.pageStep = value;
                    if( this.pageStep < 1 )
                        this.pageStep = 1;

                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "The frequency at which to place ticks by the slider." )]
        [DefaultValue( typeof( decimal ), "5" )]
        public decimal TickFrequency
        {
            get { return this.tickFrequency; }
            set
            {
                if( this.tickFrequency != value )
                {
                    this.tickFrequency = value;
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "The number of decimal places used on the value label" )]
        [DefaultValue( 0 )]
        public int DecimalPlaces
        {
            get { return this.decimalPlaces; }
            set
            {
                if( this.decimalPlaces != value )
                {
                    this.decimalPlaces = value;
                    this.ResizeContent();
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "The orientation of the control" )]
        [DefaultValue( Orientation.Horizontal )]
        public Orientation Orientation
        {
            get { return this.orientation; }
            set
            {
                if( this.orientation != value )
                {
                    this.orientation = value;
                    if( this.orientation == Orientation.Horizontal )
                    {
                        if( this.Width < this.Height )
                        {
                            int temp = this.Width;
                            this.Width = this.Height;
                            this.Height = temp;
                        }
                    }
                    else
                    {
                        if( this.Width > this.Height )
                        {
                            int temp = this.Width;
                            this.Width = this.Height;
                            this.Height = temp;
                        }
                    }
                    if( this.autoSize == true )
                        this.Size = FitSize;

                    this.ResizeContent();
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "The amount of space between elements in the slider" )]
        [DefaultValue( 4 )]
        public int Spacing
        {
            get { return this.spacing; }
            set
            {
                if( this.spacing != value )
                {
                    this.spacing = value;
                    if( this.autoSize == true )
                        this.Size = FitSize;
                    this.ResizeContent();
                    this.Invalidate();
                }
            }
        }

        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
        public override bool AutoSize
        {
            get { return this.autoSize; }
            set
            {
                if( this.autoSize != value )
                {
                    this.autoSize = value;
                    if( this.autoSize == true )
                        this.Size = FitSize;
                }
            }
        }

        public new Size Size
        {
            get { return base.Size; }
            set
            {
                if( this.orientation == Orientation.Horizontal )
                {
                    this.Width = value.Width;
                    this.Height = FitSize.Height;
                }
                else
                {
                    this.Width = FitSize.Width;
                    this.Height = value.Height;
                }
            }
        }

        private Size FitSize
        {
            get
            {
                Size fitSize;

                Size textSize;
                if( this.labelPosition == LabelPosition.Disabled )
                    textSize = Size.Empty;
                else
                {
                    textSize = this.MeasureDecimal( this.maximum, TextFormatFlags.Default );
                    textSize.Width += 4;
                    textSize.Height += 4;
                }
                if( this.orientation == Orientation.Horizontal )
                {
                    fitSize = new Size( this.ClientRectangle.Width, SliderButton.Height );
                    if( fitSize.Height < textSize.Height )
                        fitSize.Height = textSize.Height;
                }
                else
                {
                    fitSize = new Size( SliderButton.Height, this.ClientRectangle.Height );
                    if( fitSize.Width < textSize.Width )
                        fitSize.Width = textSize.Width;
                }

                return fitSize;
            }
        }

        [Category( "Appearance" )]
        [Description( "Determins where the label displaying the value will be positioned" )]
        [DefaultValue( LabelPosition.SliderBeforeLabel )]
        public LabelPosition LabelPosition
        {
            get { return this.labelPosition; }
            set
            {
                if( this.labelPosition != value )
                {
                    this.labelPosition = value;
                    if( this.autoSize == true )
                        this.Size = FitSize;
                    this.ResizeContent();
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "Determins whether or not the minimum/maximum buttons are shown." )]
        [DefaultValue( true )]
        public bool ShowButtons
        {
            get { return this.showButtons; }
            set
            {
                if( this.showButtons != value )
                {
                    this.showButtons = value;
                    this.ResizeContent();
                    this.Invalidate();
                }
            }
        }

        [Browsable( false )]
        public int TextBaseline
        {
            get { return this.labelRect.Bottom - 3; }
        }

        private MouseHoverElements MouseHoverElement
        {
            get { return this.mouseHoverElement; }
            set
            {
                if( this.leftButtonDown )
                    return;
                if( this.mouseHoverElement != value )
                {
                    this.mouseHoverElement = value;
                    this.Invalidate();
                }
            }
        }

#endregion

#region Enums

        private enum MouseHoverElements
        {
            None,
            MinButton,
            MaxButton,
            Tracker,
        }

#endregion

#region Events

        public event EventHandler ValueChanged;
        public event EventHandler Scroll;

#endregion

#region Methods
        
        protected virtual void OnValueChanged()
        {
            this.DoValueChanged();
        }

        protected virtual void OnScroll()
        {
            this.DoScroll();
        }

        private void DoValueChanged()
        {
            if( this.ValueChanged != null )
                this.ValueChanged( this, EventArgs.Empty );
        }
        
        private void DoScroll()
        {
            if( this.Scroll != null )
                this.Scroll( this, EventArgs.Empty );
        }

        public void Increment( decimal value )
        {
            if( this.value < this.maximum )
            {
                this.value += value;
                if( this.value > this.maximum )
                    this.value = this.maximum;
            }
            else
                this.value = this.maximum;

            this.OnValueChanged();
            this.ResizeTrackerButton();
            this.Invalidate();
        }

        public void Decrement( decimal value )
        {
            if( this.value > this.minimum )
            {
                this.value -= value;
                if( this.value < this.minimum )
                    this.value = this.minimum;
            }
            else
                this.value = this.minimum;

            this.OnValueChanged();
            this.ResizeTrackerButton();
            this.Invalidate();
        }

        public void SetRange( decimal minimum, decimal maximum )
        {
            this.minimum = minimum;
            if( this.minimum > this.value )
                this.value = this.minimum;

            this.maximum = maximum;
            if( this.maximum < this.value )
                this.value = this.maximum;
            if( this.maximum < this.minimum )
                this.minimum = this.maximum;

            this.ResizeContent();
            this.Invalidate();
        }

        protected override void OnLostFocus( EventArgs e )
        {
            this.Invalidate();
            base.OnLostFocus( e );
        }

        protected override void OnGotFocus( EventArgs e )
        {
            this.Invalidate();
            base.OnGotFocus( e );
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            this.MouseHoverElement = MouseHoverElements.None;
            this.Invalidate();
            base.OnMouseLeave( e );
        }

        protected override void OnClick( EventArgs e )
        {
            this.Focus();
            this.Invalidate();
            base.OnClick( e );
        }

        protected override bool ProcessCmdKey( ref Message msg, Keys keyData )
        {
            bool result = true;

            const int WM_KEYDOWN = 0x0100;
            const int WM_KEYUP = 0x0104;

            if( msg.Msg == WM_KEYDOWN || msg.Msg == WM_KEYUP )
            {
                switch( keyData )
                {
                    case Keys.Left:
                    case Keys.Down:
                        this.Decrement( this.step );
                        break;

                    case Keys.Right:
                    case Keys.Up:
                        this.Increment( this.step );
                        break;

                    case Keys.PageDown:
                        this.Decrement( this.pageStep );
                        break;

                    case Keys.PageUp:
                        this.Increment( this.pageStep );
                        break;

                    case Keys.Home:
                        this.Value = this.minimum;
                        break;

                    case Keys.End:
                        this.Value = this.maximum;
                        break;

                    default:
                        result = base.ProcessCmdKey( ref msg, keyData );
                        break;
                }
            }

            return result;
        }

        protected override void OnMouseWheel( MouseEventArgs e )
        {
            base.OnMouseWheel( e );
            if( e.Delta > 0 )
                this.Increment( this.step * 3 );
            else
                this.Decrement( this.step * 3 );
        }

        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );
            if( this.showButtons && this.minButton.Enabled && this.minButton.Rectangle.Contains( e.Location ) )
            {
                this.MouseHoverElement = MouseHoverElements.MinButton;
                if( !this.leftButtonDown )
                {
                    this.leftButtonDown = true;
                    this.Capture = true;
                }
            }
            else if( this.showButtons && this.maxButton.Enabled && this.maxButton.Rectangle.Contains( e.Location ) )
            {
                this.MouseHoverElement = MouseHoverElements.MaxButton;
                if( !this.leftButtonDown )
                {
                    this.leftButtonDown = true;
                    this.Capture = true;
                }
            }
            else if( this.tracker.Button.Contains( e.Location ) )
            {
                this.MouseHoverElement = MouseHoverElements.Tracker;
                if( !this.leftButtonDown )
                {
                    this.leftButtonDown = true;
                    this.Capture = true;
                    PointF currentPoint = new PointF( e.X, e.Y );
                    switch( this.orientation )
                    {
                        case Orientation.Horizontal:
                            this.mouseStartPos = currentPoint.X - this.tracker.Button.X + this.tracker.Bounds.Left;
                            break;

                        case Orientation.Vertical:
                            this.mouseStartPos = currentPoint.Y - this.tracker.Button.Y + this.tracker.Bounds.Top;
                            break;
                    }
                }
            }
            else if( this.tracker.Bounds.Contains( e.Location ) )
            {
                PointF currentPoint = new PointF( e.X - this.tracker.Bounds.X, e.Y - this.tracker.Bounds.Y );
                this.MouseHoverElement = MouseHoverElements.None;
                decimal offsetValue = 0;
                
                switch( this.orientation )
                {
                    case Orientation.Horizontal:
                        if( ( currentPoint.X + SliderTracker.Width / 2 ) >= this.tracker.Bounds.Width )
                            offsetValue = this.maximum - this.minimum;
                        else if( ( currentPoint.X - SliderTracker.Width / 2 ) <= 0 )
                            offsetValue = 0;
                        else
                            offsetValue = ( ( (decimal) ( currentPoint.X - SliderTracker.Width / 2 ) * ( this.maximum - this.minimum ) ) / ( this.tracker.Bounds.Width - SliderTracker.Width ) + 0.5m );
                        break;

                    case Orientation.Vertical:
                        if( currentPoint.Y + SliderTracker.Width / 2 >= this.tracker.Bounds.Height )
                            offsetValue = 0;
                        else if( currentPoint.Y - SliderTracker.Width / 2 <= 0 )
                            offsetValue = this.maximum - this.minimum;
                        else
                            offsetValue = ( ( (decimal) ( currentPoint.Y - SliderTracker.Width / 2 ) * ( this.maximum - this.minimum ) ) / ( this.tracker.Bounds.Height - SliderTracker.Width ) + 0.5m );
                        break;
                }
                
                decimal oldValue = this.value;
                this.value = this.minimum + offsetValue;

                if( oldValue != this.value )
                {
                    this.OnScroll();
                    this.OnValueChanged();
                    this.ResizeTrackerButton();
                }
            }
            this.Invalidate();
        }

        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );
            this.leftButtonDown = false;
            this.Capture = false;

            if( this.showButtons )
            {
                if( this.minButton.Enabled && this.MouseHoverElement == MouseHoverElements.MinButton && this.minButton.Rectangle.Contains( e.Location ) )
                    this.Value = this.minimum;
                else if( this.maxButton.Enabled && this.MouseHoverElement == MouseHoverElements.MaxButton && this.maxButton.Rectangle.Contains( e.Location ) )
                    this.Value = this.maximum;
            }
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            PointF currentPoint = new PointF( e.X, e.Y );

            if( this.showButtons && this.minButton.Enabled && this.minButton.Rectangle.Contains( e.Location ) )
            {
                this.MouseHoverElement = MouseHoverElements.MinButton;
            }
            else if( this.showButtons && this.maxButton.Enabled && this.maxButton.Rectangle.Contains( e.Location ) )
            {
                this.MouseHoverElement = MouseHoverElements.MaxButton;
            }
            else if( this.tracker.Bounds.Contains( e.Location ) )
            {
                if( this.leftButtonDown && this.MouseHoverElement == MouseHoverElements.Tracker )
                {
                    decimal offsetValue = 0;
                    try
                    {
                        switch( this.orientation )
                        {
                            case Orientation.Horizontal:
                                if( ( currentPoint.X + SliderTracker.Width - this.mouseStartPos ) >= this.tracker.Bounds.Width )
                                    offsetValue = this.maximum - this.minimum;
                                else if( currentPoint.X - this.mouseStartPos <= 0 )
                                    offsetValue = 0;
                                else
                                    offsetValue = ( ( (decimal) ( currentPoint.X - this.mouseStartPos ) * ( this.maximum - this.minimum ) ) / ( this.tracker.Bounds.Width - SliderTracker.Width ) + 0.5m );
                                break;

                            case Orientation.Vertical:
                                if( ( currentPoint.Y + SliderTracker.Width - this.mouseStartPos ) >= this.tracker.Bounds.Height )
                                    offsetValue = 0;
                                else if( currentPoint.Y - this.mouseStartPos <= 0 )
                                    offsetValue = this.maximum - this.minimum;
                                else
                                    offsetValue = this.maximum - ( ( (decimal) ( currentPoint.Y - this.mouseStartPos ) * ( this.maximum - this.minimum ) ) / ( this.tracker.Bounds.Height - SliderTracker.Width ) + 0.5m );
                                break;
                        }
                    }
                    catch( Exception )
                    {
                    }
                    finally
                    {
                        decimal oldValue = this.value;
                        this.value = this.minimum + offsetValue;

                        if( oldValue != this.value )
                        {
                            this.OnScroll();
                            this.OnValueChanged();
                            this.ResizeTrackerButton();
                        }
                    }
                }
                else
                    this.MouseHoverElement = MouseHoverElements.Tracker;
            }
            this.Invalidate();
        }

        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.ResizeContent();
            this.Invalidate();
        }

        private Size MeasureDecimal( decimal value, TextFormatFlags tff )
        {
            Size size = Size.Empty;
            using( Graphics g = this.CreateGraphics() )
            {
                size = TextRenderer.MeasureText( g, value.ToString( "N" + this.decimalPlaces.ToString() ), this.Font, this.ClientSize, tff );
            }
            return size;
        }

        private void ResizeContent()
        {
            TextFormatFlags tff;
            int textOffset = 0;
            if( this.orientation == Orientation.Horizontal )
            {
                if( this.showButtons )
                {
                    this.minButton.Rectangle = new Rectangle( 0, ( this.ClientSize.Height - SliderButton.Height ) / 2, SliderButton.Width, SliderButton.Height );
                    this.minButton.Arrow = ArrowDirection.Left;

                    this.maxButton.Rectangle = new Rectangle( this.ClientSize.Width - SliderButton.Width, ( this.ClientSize.Height - SliderButton.Height ) / 2, SliderButton.Width, SliderButton.Height );
                    this.maxButton.Arrow = ArrowDirection.Right;
                }

                // Text label
                if( this.labelPosition != LabelPosition.Disabled )
                {
                    tff = TextFormatFlags.Right | TextFormatFlags.VerticalCenter;
                    this.labelRect = new Rectangle( this.ClientRectangle.Location, this.MeasureDecimal( this.maximum, tff ) );
                    this.labelRect.Inflate( 2, 2 );
                    if( this.labelPosition == LabelPosition.SliderBeforeLabel )
                        this.labelRect.X = this.ClientSize.Width - this.labelRect.Width;
                    else
                        this.labelRect.X = 0;
                    this.labelRect.Y = ( this.ClientSize.Height - this.labelRect.Height ) / 2;
                    textOffset = this.labelRect.Width + this.spacing;

                    if( this.showButtons )
                    {
                        if( this.labelPosition == LabelPosition.LabelBeforeSlider )
                            this.minButton.Offset( textOffset, 0 );
                        else if( this.labelPosition == LabelPosition.SliderBeforeLabel )
                            this.maxButton.Offset( -textOffset, 0 );
                    }
                }

                if( this.showButtons )
                {
                    this.tracker.Bounds = new Rectangle( this.minButton.Rectangle.Right + this.spacing, ( this.ClientSize.Height - SliderTracker.Height ) / 2,
                                                         this.maxButton.Rectangle.Left - this.minButton.Rectangle.Right - ( this.spacing * 2 ), SliderTracker.Height );
                }
                else
                {
                    Rectangle rect = new Rectangle( 0, ( this.ClientSize.Height - SliderTracker.Height ) / 2, this.ClientSize.Width, SliderTracker.Height );
                    if( this.labelPosition == LabelPosition.LabelBeforeSlider )
                    {
                        rect.X += textOffset;
                        rect.Width -= textOffset;
                    }
                    else if( this.labelPosition == LabelPosition.SliderBeforeLabel )
                    {
                        rect.Width -= textOffset;
                    }
                    this.tracker.Bounds = rect;
                }
                this.tracker.Ticks = new Rectangle( this.tracker.Bounds.Left + ( SliderTracker.Width / 2 ), this.tracker.Bounds.Height - 5,
                                                    this.tracker.Bounds.Width - SliderTracker.Width, 3 );
                this.tracker.Bar = new Rectangle( this.tracker.Bounds.Left + ( SliderTracker.Width / 2 ), ( this.tracker.Bounds.Height / 2 ) - 4,
                                                  this.tracker.Bounds.Width - SliderTracker.Width, 4 );

                this.focusRect = this.tracker.Bounds;
                this.focusRect.X -= 2;
                this.focusRect.Width += 3;
            }
            else
            {
                if( this.showButtons )
                {
                    this.minButton.Rectangle = new Rectangle( ( this.ClientSize.Width - SliderButton.Height ) / 2, this.ClientSize.Height - SliderButton.Width, SliderButton.Height, SliderButton.Width );
                    this.minButton.Arrow = ArrowDirection.Down;

                    this.maxButton.Rectangle = new Rectangle( ( this.ClientSize.Width - SliderButton.Height ) / 2, 0, SliderButton.Height, SliderButton.Width );
                    this.maxButton.Arrow = ArrowDirection.Up;
                }

                if( this.labelPosition != LabelPosition.Disabled )
                {
                    tff = TextFormatFlags.HorizontalCenter | TextFormatFlags.Bottom;
                    this.labelRect = new Rectangle( this.ClientRectangle.Location, this.MeasureDecimal( this.maximum, tff ) );
                    this.labelRect.Inflate( 2, 2 );
                    this.labelRect.X = ( this.ClientSize.Width - this.labelRect.Width ) / 2;
                    if( this.labelPosition == LabelPosition.SliderBeforeLabel )
                        this.labelRect.Y = this.ClientSize.Height - this.labelRect.Height;
                    else
                        this.labelRect.Y = 0;
                    textOffset = this.labelRect.Height + this.spacing;

                    if( this.showButtons )
                    {
                        if( this.labelPosition == LabelPosition.SliderBeforeLabel )
                            this.minButton.Offset( 0, -textOffset );
                        else if( this.labelPosition == LabelPosition.LabelBeforeSlider )
                            this.maxButton.Offset( 0, textOffset );
                    }
                }

                if( this.showButtons )
                {
                    this.tracker.Bounds = new Rectangle( ( this.ClientSize.Width - SliderTracker.Height ) / 2, this.maxButton.Rectangle.Bottom + this.spacing,
                                                         SliderTracker.Height, this.minButton.Rectangle.Top - this.maxButton.Rectangle.Bottom - ( this.spacing * 2 ) );
                }
                else
                {
                    Rectangle rect = new Rectangle( ( this.ClientSize.Width - SliderTracker.Height ) / 2, 0, SliderTracker.Height, this.ClientSize.Height );
                    if( this.labelPosition == LabelPosition.LabelBeforeSlider )
                    {
                        rect.Y += textOffset;
                        rect.Height -= textOffset;
                    }
                    else if( this.labelPosition == LabelPosition.SliderBeforeLabel )
                    {
                        rect.Height -= textOffset;
                    }
                    this.tracker.Bounds = rect;
                }
                this.tracker.Ticks = new Rectangle( this.tracker.Bounds.Left + 1, this.tracker.Bounds.Top + ( SliderTracker.Width / 2 ),
                                                    3, this.tracker.Bounds.Height - SliderTracker.Width );
                this.tracker.Bar = new Rectangle( this.tracker.Bounds.Left + 2 + ( this.tracker.Bounds.Width / 2 ), this.tracker.Bounds.Top + ( SliderTracker.Width / 2 ),
                                                  4, this.tracker.Bounds.Height - SliderTracker.Width );

                this.focusRect = this.tracker.Bounds;
                this.focusRect.Y -= 2;
                this.focusRect.Height += 3;
            }
            this.ResizeTrackerButton();
        }

        private void ResizeTrackerButton()
        {
            if( this.orientation == Orientation.Horizontal )
            {
                double currentTrackerPos;
                if( this.maximum == this.minimum )
                    currentTrackerPos = this.tracker.Bounds.Left;
                else
                    currentTrackerPos = Decimal.ToDouble( ( this.tracker.Bounds.Width - SliderTracker.Width ) * ( this.value - this.minimum ) / ( this.maximum - this.minimum ) + this.tracker.Bounds.Left );
                this.tracker.Button = new Rectangle( (int) currentTrackerPos, this.tracker.Bounds.Top, SliderTracker.Width, SliderTracker.Height - 4 );
            }
            else
            {
                double currentTrackerPos;
                if( this.maximum == this.minimum )
                    currentTrackerPos = this.tracker.Bounds.Bottom;
                else
                    currentTrackerPos = Decimal.ToDouble( ( this.tracker.Bounds.Height - SliderTracker.Width ) * ( this.maximum - this.value ) / ( this.maximum - this.minimum ) + this.tracker.Bounds.Top );
                this.tracker.Button = new Rectangle( this.tracker.Bounds.Left + 4 + ( this.tracker.Bounds.Width - SliderTracker.Height ) / 2, (int) currentTrackerPos, SliderTracker.Height - 4, SliderTracker.Width );
            }
        }

        protected override void Dispose( bool disposing )
        {
            base.Dispose( disposing );
        }

#endregion

#region Painting

        protected override void OnPaint( PaintEventArgs e )
        {
            // Draw background
            using( Brush brush = new SolidBrush( this.BackColor ) )
            {
                e.Graphics.FillRectangle( brush, this.ClientRectangle );
            }
            if( this.Focused )
            {
                ControlPaint.DrawFocusRectangle( e.Graphics, this.focusRect, SystemColors.ControlDark, this.BackColor );
            }

            this.DrawValueText( e.Graphics );
            this.DrawButton( e.Graphics, this.minButton, this.minimum, MouseHoverElements.MinButton );
            this.DrawButton( e.Graphics, this.maxButton, this.maximum, MouseHoverElements.MaxButton );
            this.DrawTrackBar( e.Graphics );
        }

        private void DrawValueText( Graphics g )
        {
            if( this.labelPosition == LabelPosition.Disabled )
                return;

            TextFormatFlags tff;
            if( this.orientation == Orientation.Horizontal )
                tff = TextFormatFlags.Right | TextFormatFlags.VerticalCenter;
            else
                tff = TextFormatFlags.HorizontalCenter | TextFormatFlags.Bottom;

            Rectangle rect = this.labelRect;
            SliderRenderer.DrawTextBox( g, rect );

            rect.Inflate( -2, -2 );
            Color color = this.ForeColor;
            if( !this.Enabled )
                color = SystemColors.GrayText;
            TextRenderer.DrawText( g, this.value.ToString( "N" + this.decimalPlaces.ToString() ), this.Font, rect, color, tff );
        }

        private void DrawButton( Graphics g, SliderButton button, decimal matchValue, MouseHoverElements element )
        {
            if( !this.showButtons )
                return;

            if( !this.Enabled || this.value == matchValue )
                button.State = PushButtonState.Disabled;
            else if( this.mouseHoverElement == element )
            {
                if( this.leftButtonDown )
                    button.State = PushButtonState.Pressed;
                else
                    button.State = PushButtonState.Hot;
            }
            else
                button.State = PushButtonState.Normal;

            SliderRenderer.DrawButton( g, button.Rectangle, this.ForeColor, button.State, button.Arrow );
        }

        private void DrawTrackBar( Graphics g )
        {
            TrackBarThumbState state = TrackBarThumbState.Normal;
            if( !this.Enabled || this.maximum == this.minimum )
                state = TrackBarThumbState.Disabled;
            else if( this.MouseHoverElement == MouseHoverElements.Tracker )
            {
                if( this.leftButtonDown )
                    state = TrackBarThumbState.Pressed;
                else
                    state = TrackBarThumbState.Hot;
            }
            if( this.orientation == Orientation.Horizontal )
            {
                if( this.tickFrequency > 0 )
                    SliderRenderer.DrawHorizontalTicks( g, this.tracker.Ticks, Decimal.ToInt32( ( this.maximum - this.minimum ) / this.tickFrequency ) );
                SliderRenderer.DrawHorizontalTrack( g, this.tracker.Bar );
                SliderRenderer.DrawBottomPointingThumb( g, this.tracker.Button, state );
            }
            else
            {
                if( this.tickFrequency > 0 )
                    SliderRenderer.DrawVerticalTicks( g, this.tracker.Ticks, Decimal.ToInt32( ( this.maximum - this.minimum ) / this.tickFrequency ) );
                SliderRenderer.DrawVerticalTrack( g, this.tracker.Bar );
                SliderRenderer.DrawLeftPointingThumb( g, this.tracker.Button, state );
            }
        }

        private void DebugBorder( Graphics g, Rectangle rect )
        {
            rect.Width -= 1;
            rect.Height -= 1;
            g.DrawRectangle( Pens.Red, rect );
        }

#endregion

#region Helper classes

        internal class SliderTracker
        {
            public static int Width
            {
                get { return 9; }
            }

            public static int Height
            {
                get { return 21; }
            }

            private Rectangle bounds = Rectangle.Empty;
            public Rectangle Bounds
            {
                get { return this.bounds; }
                set { this.bounds = value; }
            }

            private Rectangle ticks = Rectangle.Empty;
            public Rectangle Ticks
            {
                get { return this.ticks; }
                set { this.ticks = value; }
            }

            private Rectangle bar = Rectangle.Empty;
            public Rectangle Bar
            {
                get { return this.bar; }
                set { this.bar = value; }
            }

            private Rectangle tracker = Rectangle.Empty;
            public Rectangle Button
            {
                get { return this.tracker; }
                set { this.tracker = value; }
            }
        }

        internal class SliderButton
        {
            public static int Width
            {
                get { return 14; }
            }

            public static int Height
            {
                get { return 21; }
            }

            private Rectangle rectangle = Rectangle.Empty;
            public Rectangle Rectangle
            {
                get { return this.rectangle; }
                set { this.rectangle = value; }
            }

            private PushButtonState state = PushButtonState.Normal;
            public PushButtonState State
            {
                get { return this.state; }
                set { this.state = value; }
            }

            private ArrowDirection arrow = ArrowDirection.Left;
            public ArrowDirection Arrow
            {
                get { return this.arrow; }
                set { this.arrow = value; }
            }

            public bool Enabled
            {
                get { return this.state != PushButtonState.Disabled; }
            }

            // Tweak to make button offseting easier
            public void Offset( int x, int y )
            {
                this.rectangle.Offset( x, y );
            }
        }

#endregion
    }
}
