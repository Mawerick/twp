﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Twp.Controls
{
    public static class SliderRenderer
    {
        private const int lineWidth = 2;

        public static void DrawHorizontalTicks( Graphics g, Rectangle rect, int numTicks )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawHorizontalTicks( g, rect, numTicks, EdgeStyle.Etched );
            else
            {
                if( numTicks == 1 )
                {
                    ControlPaint.DrawBorder3D( g, new Rectangle( rect.X, rect.Y, lineWidth, rect.Height ), Border3DStyle.Etched, Border3DSide.Left );
                    return;
                }

                float inc = ((float) rect.Width - lineWidth) / ((float) numTicks - 1);

                while( numTicks > 0 )
                {
                    float x = rect.X + ((float) (numTicks - 1)) * inc;
                    ControlPaint.DrawBorder3D( g, new Rectangle( (int) Math.Round( x ), rect.Y, lineWidth, rect.Height ), Border3DStyle.Etched, Border3DSide.Left );
                    numTicks--;
                }
            }
        }

        public static void DrawHorizontalTrack( Graphics g, Rectangle rect )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawHorizontalTrack( g, rect );
            else
                ControlPaint.DrawBorder3D( g, rect, Border3DStyle.SunkenOuter );
        }

        public static void DrawVerticalTicks( Graphics g, Rectangle rect, int numTicks )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawVerticalTicks( g, rect, numTicks, EdgeStyle.Etched );
            else
            {
                if( numTicks == 1 )
                {
                    ControlPaint.DrawBorder3D( g, new Rectangle( rect.X, rect.Y, rect.Width, lineWidth ), Border3DStyle.Etched, Border3DSide.Top );
                    return;
                }

                float inc = ((float) rect.Height - lineWidth) / ((float) numTicks - 1);

                while( numTicks > 0 )
                {
                    float y = rect.Y + ((float) (numTicks - 1)) * inc;
                    ControlPaint.DrawBorder3D( g, new Rectangle( rect.X, (int) Math.Round( y ), rect.Width, lineWidth ), Border3DStyle.Etched, Border3DSide.Top );
                    numTicks--;
                }
            }
        }

        public static void DrawVerticalTrack( Graphics g, Rectangle rect )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawVerticalTrack( g, rect );
            else
                ControlPaint.DrawBorder3D( g, rect, Border3DStyle.SunkenOuter );
        }

        public static void DrawLeftPointingThumb( Graphics g, Rectangle rect, TrackBarThumbState state )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawLeftPointingThumb( g, rect, state );
            else
                DrawHorizontalThumb( g, rect, state );
        }

        public static void DrawRightPointingThumb( Graphics g, Rectangle rect, TrackBarThumbState state )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawRightPointingThumb( g, rect, state );
            else
                DrawHorizontalThumb( g, rect, state );
        }

        public static void DrawTopPointingThumb( Graphics g, Rectangle rect, TrackBarThumbState state )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawTopPointingThumb( g, rect, state );
            else
                DrawVerticalThumb( g, rect, state );
        }

        public static void DrawBottomPointingThumb( Graphics g, Rectangle rect, TrackBarThumbState state )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawBottomPointingThumb( g, rect, state );
            else
                DrawVerticalThumb( g, rect, state );
        }

        public static void DrawHorizontalThumb( Graphics g, Rectangle rect, TrackBarThumbState state )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawHorizontalThumb( g, rect, state );
            else
            {
                ButtonState bState = ButtonState.Normal;
                switch( state )
                {
                    case TrackBarThumbState.Disabled:
                        bState = ButtonState.Inactive;
                        break;
                    case TrackBarThumbState.Pressed:
                        bState = ButtonState.Pushed;
                        break;
                }
                rect.Height -= 1;
                rect.Width -= 1;
                ControlPaint.DrawButton( g, rect, bState );
            }
        }

        public static void DrawVerticalThumb( Graphics g, Rectangle rect, TrackBarThumbState state )
        {
            if( TrackBarRenderer.IsSupported )
                TrackBarRenderer.DrawVerticalThumb( g, rect, state );
            else
            {
                ButtonState bState = ButtonState.Normal;
                switch( state )
                {
                    case TrackBarThumbState.Disabled:
                        bState = ButtonState.Inactive;
                        break;
                    case TrackBarThumbState.Pressed:
                        bState = ButtonState.Pushed;
                        break;
                }
                rect.Height -= 1;
                rect.Width -= 1;
                ControlPaint.DrawButton( g, rect, bState );
            }
        }

        public static void DrawButton( Graphics g, Rectangle rect, Color color, PushButtonState state, ArrowDirection direction )
        {
            ButtonRenderer.DrawButton( g, rect, state );
            if( state == PushButtonState.Disabled )
                color = SystemColors.GrayText;
            ButtonRenderer.DrawEndArrow( g, rect, color, direction );
        }

        public static void DrawTextBox( Graphics g, Rectangle rect )
        {
            g.FillRectangle( SystemBrushes.Window, rect );
            if( Application.RenderWithVisualStyles )
            {
                rect.Width -= 1;
                rect.Height -= 1;
                ControlPaint.DrawVisualStyleBorder( g, rect );
            }
            else
                ControlPaint.DrawBorder3D( g, rect, Border3DStyle.SunkenOuter );
        }
    }
}
