﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Twp.Controls
{
    //[Designer( typeof( Design.TabControlDesigner ), typeof( IDesigner ) )]
    [ToolboxBitmap( typeof( System.Windows.Forms.TabControl ) )]
    public class TabControl : System.Windows.Forms.TabControl
	{
		public TabControl() : base()
		{
			this.SetStyle( ControlStyles.UserPaint
				| ControlStyles.AllPaintingInWmPaint
				| ControlStyles.Opaque
                //| ControlStyles.ResizeRedraw
                | ControlStyles.OptimizedDoubleBuffer
				| ControlStyles.SupportsTransparentBackColor, true );
            //this.ResizeRedraw = true;
			this.ItemSize = new Size( 0, this.Font.Height + 4 );
			this.Padding = new Point( 6, 0 );
		}

		[Browsable( false )]
		[EditorBrowsable( EditorBrowsableState.Never )]
		public new Size ItemSize
		{
			get { return base.ItemSize; }
			set { base.ItemSize = value; }
		}

		[Browsable( false )]
		[EditorBrowsable( EditorBrowsableState.Never )]
		public new Point Padding
		{
			get { return base.Padding; }
			set { base.Padding = value; }
		}

		public new TabSizeMode SizeMode
		{
			get { return base.SizeMode; }
			set
			{
				base.SizeMode = value;
				this.SetItemSize();
			}
		}

		private void SetItemSize()
		{
			Size itemSize;
			if( this.SizeMode == TabSizeMode.Fixed && this.TabCount > 0 )
            {
                switch( this.Alignment )
                {
                    case TabAlignment.Left:
                    case TabAlignment.Right:
                        itemSize = new Size( (this.ClientSize.Height - 3) / this.TabCount, this.ItemSize.Height );
                        break;
                    default:
                        itemSize = new Size( (this.ClientSize.Width - 3) / this.TabCount, this.ItemSize.Height );
                        break;
                }
            }
            else
                itemSize = new Size( 0, this.ItemSize.Height );
            this.ItemSize = itemSize;
		}

		private bool InResize;
		protected override void OnResize( EventArgs e )
		{
			if( !InResize )
			{
				InResize = true;
				this.SetItemSize();
				base.OnResize( e );
				InResize = false;
			}
		}

		protected override void OnPaintBackground( PaintEventArgs e )
		{
			if( this.DesignMode )
			{
                using( LinearGradientBrush brush = new LinearGradientBrush( this.Bounds,
					SystemColors.Control, SystemColors.ControlLight,
					LinearGradientMode.Vertical ) )
				{
					e.Graphics.FillRectangle( brush, this.Bounds );
				}
			}
			else
			{
				this.PaintBackground( e.Graphics, this.ClientRectangle );
			}
		}

		private void PaintBackground( Graphics g, Rectangle rect )
		{
            if( this.Parent != null )
			{
                rect.Offset( this.Location );
				PaintEventArgs e = new PaintEventArgs( g, rect );
				GraphicsState state = g.Save();
				g.SmoothingMode = SmoothingMode.HighSpeed;
				try
				{
					g.TranslateTransform( (float) -this.Location.X, (float) -this.Location.Y );
					this.InvokePaintBackground( this.Parent, e );
					this.InvokePaint( this.Parent, e );
				}
				finally
				{
					g.Restore( state );
					rect.Offset( -this.Location.X, -this.Location.Y );
				}
			}
			else
			{
                using( LinearGradientBrush brush = new LinearGradientBrush( this.Bounds,
					SystemColors.Control, SystemColors.ControlLight,
					LinearGradientMode.Vertical ) )
				{
					g.FillRectangle( brush, this.Bounds );
				}
			}
        }

		protected override void OnPaint( PaintEventArgs e )
		{
            this.PaintBackground( e.Graphics, this.ClientRectangle );
			if( this.TabCount > 0 )
			{
				// Paint Tabs
                for( int index = 0; index < this.TabCount; index++ )
				{
					this.PaintTab( e.Graphics, index, false );
				}
                if( this.SelectedIndex >= 0 )
					this.PaintTab( e.Graphics, this.SelectedIndex, true );
            }
            this.PaintTabPage( e.Graphics );
        }

		private void PaintTab( Graphics g, int index, bool selected )
		{
			Rectangle rect = this.GetTabRect( index );

			if( rect.Width == 0 )
			{
				rect.Width = 1;
				rect.X += 2;
			}

            TabState state = TabState.Normal;
            if( selected )
            {
                state = TabState.Selected;
                rect.Inflate( 1, 1 );
                switch( this.Alignment )
                {
                    case TabAlignment.Bottom:
                        rect.Offset( -1, 0 );
                        break;
                    case TabAlignment.Top:
                    case TabAlignment.Left:
                        rect.Offset( -1, -1 );
                        break;
                    case TabAlignment.Right:
                        rect.Offset( 0, -1 );
                        break;
                }
            }
            else
            {
                switch( this.Alignment )
                {
                    case TabAlignment.Bottom:
                    case TabAlignment.Top:
                        rect.Offset( -1, 0 );
                        break;
                    case TabAlignment.Left:
                        rect.Offset( 0, -1 );
                        break;
                    case TabAlignment.Right:
                        rect.Offset( -1, -1 );
                        break;
                }
            }

            TabRenderer.DrawTab( g, rect, this.TabPages[index].Text, this.Font, state, this.Alignment );
		}

		private void PaintTabPage( Graphics g )
		{
			// Paint Tab page border
			Rectangle rect;
			if( this.TabCount > 0 )
			{
				int index = 0;
				if( this.SelectedIndex > 0 )
					index = this.SelectedIndex;
				rect = this.TabPages[index].Bounds;
                switch( this.Alignment )
                {
                    case TabAlignment.Top:
                        rect.Inflate( 4, 3 );
                        rect.Height += 1;
                        break;
                    case TabAlignment.Bottom:
                        rect.Inflate( 4, 4 );
                        rect.Height -= 2;
                        break;
                    case TabAlignment.Left:
				        rect.Inflate( 2, 4 );
				        rect.Width += 2;
                        break;
                    case TabAlignment.Right:
				        rect.Inflate( 4, 4 );
				        rect.Width -= 2;
                        break;
                }
			}
			else
			{
				rect = this.ClientRectangle;
                switch( this.Alignment )
                {
                    case TabAlignment.Top:
                        rect.Inflate( -4, -2 );
                        rect.Y += this.ItemSize.Height;
                        rect.Height -= this.ItemSize.Height;
                        break;
                    case TabAlignment.Bottom:
                        rect.Inflate( -4, -2 );
                        rect.Height -= this.ItemSize.Height;
                        break;
                    case TabAlignment.Left:
                        rect.Inflate( -2, -4 );
                        rect.X += this.ItemSize.Width;
                        rect.Width -= this.ItemSize.Width;
                        break;
                    case TabAlignment.Right:
                        rect.Inflate( -4, -2 );
                        rect.Width -= this.ItemSize.Width;
                        break;
                }
			}
			rect.Height -= 1;
			rect.Width -= 1;

            RectangleCorners corners;
            RectangleCorners extra;
            switch( this.Alignment )
            {
                case TabAlignment.Left:
                    corners = RectangleCorners.Right;
                    extra = RectangleCorners.BottomLeft;
                    break;
                case TabAlignment.Right:
                    corners = RectangleCorners.Left;
                    extra = RectangleCorners.BottomRight;
                    break;
                case TabAlignment.Bottom:
                    corners = RectangleCorners.Top;
                    extra = RectangleCorners.BottomRight;
                    break;
                default:
                    corners = RectangleCorners.Bottom;
                    extra = RectangleCorners.TopRight;
                    break;
            }
            if( this.SizeMode == TabSizeMode.Normal )
                corners |= extra;

            g.SmoothingMode = SmoothingMode.HighQuality;
			using( GraphicsPath path = RoundedRectangle.Create( rect, 3, corners ) )
			{
				g.FillPath( SystemBrushes.Control, path );
				g.DrawPath( SystemPens.ControlDarkDark, path );
			}

			// Fix border below selected Tab
			if( this.SelectedIndex >= 0 )
			{
			    g.SmoothingMode = SmoothingMode.None;
				rect = this.GetTabRect( this.SelectedIndex );
                Point pt1, pt2;
                switch( this.Alignment )
                {
                    case TabAlignment.Top:
                        pt1 = new Point( rect.Left - 1, rect.Bottom - 1 );
                        pt2 = new Point( rect.Right - 1, rect.Bottom - 1 );
                        break;
                    case TabAlignment.Bottom:
                        pt1 = new Point( rect.Left - 1, rect.Top - 1 );
                        pt2 = new Point( rect.Right - 1, rect.Top - 1 );
                        break;
                    case TabAlignment.Left:
                        pt1 = new Point( rect.Right, rect.Top - 1 );
                        pt2 = new Point( rect.Right, rect.Bottom - 1 );
                        break;
                    case TabAlignment.Right:
                        pt1 = new Point( rect.Left - 1, rect.Top - 1 );
                        pt2 = new Point( rect.Left - 1, rect.Bottom - 1 );
                        break;
                    default:
                        return;
                }
				g.DrawLine( SystemPens.Control, pt1, pt2 );
                g.SmoothingMode = SmoothingMode.HighQuality;
			}
		}

		protected override void OnCreateControl()
		{
		    base.OnCreateControl();
		    this.OnFontChanged( EventArgs.Empty );
		}

		protected override void OnFontChanged( EventArgs e )
		{
		    base.OnFontChanged( e );
		    IntPtr hFont = this.Font.ToHfont();
		    NativeMethods.SendMessage( this.Handle, NativeMethods.WM_SETFONT, hFont, (IntPtr) ( -1 ) );
		    NativeMethods.SendMessage( this.Handle, NativeMethods.WM_FONTCHANGE, IntPtr.Zero, IntPtr.Zero );
		    this.UpdateStyles();
			this.ItemSize = new Size( 0, this.Font.Height + 4 );
			this.SetItemSize();
		}
	}
}
