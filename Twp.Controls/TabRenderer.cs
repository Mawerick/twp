﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace Twp.Controls
{
    public enum TabState
    {
        Normal,
        Hot,
        Selected,
        Disabled,
    };

    public enum TabArrow
    {
        Up,
        Down,
        Left,
        Right,
    };

    public static class TabRenderer
    {
        #region Public methods

        public static void DrawTab( Graphics g, Rectangle bounds, TabState state )
        {
            DrawTabBackground( g, bounds, state, TabAlignment.Top );
        }

        public static void DrawTab( Graphics g, Rectangle bounds, TabState state, TabAlignment align )
        {
            DrawTabBackground( g, bounds, state, align );
        }

        public static void DrawTab( Graphics g, Rectangle bounds, string text, Font font, TabState state )
        {
            DrawTabBackground( g, bounds, state, TabAlignment.Top );
            DrawTabText( g, bounds, text, font, state, TabAlignment.Top);
        }

        public static void DrawTab( Graphics g, Rectangle bounds, string text, Font font, TabState state, TabAlignment align )
        {
            DrawTabBackground( g, bounds, state, align );
            DrawTabText( g, bounds, text, font, state, align );
        }

        public static void DrawArrow( Graphics g, Rectangle bounds, bool active, bool hover, TabArrow type )
        {
            if( hover )
            {
                Rectangle rect = bounds;
                rect.Width -= 1;
                rect.Height -= 1;
                g.FillRectangle( SystemBrushes.ControlLightLight, rect );
                g.DrawRectangle( SystemPens.ControlDark, rect );
            }

            Point center = new Point( bounds.X + (bounds.Width / 2), bounds.Y + (bounds.Height / 2) );
            Point[] points = new Point[3];
            switch( type )
            {
                case TabArrow.Up:
                    points[0] = new Point( center.X, center.Y - 3 );
                    points[1] = new Point( center.X + 5, center.Y + 2 );
                    points[2] = new Point( center.X - 4, center.Y + 2 );
                    break;
                case TabArrow.Down:
                    points[0] = new Point( center.X - 3, center.Y - 2 );
                    points[1] = new Point( center.X + 4, center.Y - 2 );
                    points[2] = new Point( center.X, center.Y + 2 );
                    break;
                case TabArrow.Left:
                    points[0] = new Point( center.X + 2, center.Y - 4 );
                    points[1] = new Point( center.X + 2, center.Y + 4 );
                    points[2] = new Point( center.X - 2, center.Y );
                    break;
                case TabArrow.Right:
                    points[0] = new Point( center.X - 1, center.Y - 4 );
                    points[1] = new Point( center.X - 1, center.Y + 4 );
                    points[2] = new Point( center.X + 3, center.Y );
                    break;
            }
            Brush brush = active ? SystemBrushes.ControlDarkDark : SystemBrushes.ControlDark;
            g.FillPolygon( brush, points );
        }

        #endregion

        #region Private methods

        private static void DrawTabBackground( Graphics g, Rectangle bounds, TabState state, TabAlignment align )
        {
            Color topGradient = SystemColors.ControlDark;
            Color bottomGradient = SystemColors.ControlDarkDark;
            Color border = SystemColors.ControlDarkDark;
            int radius = 3;

            switch( state )
            {
                case TabState.Disabled:
                    topGradient = SystemColors.Control;
                    bottomGradient = SystemColors.ControlDark;
                    break;
                case TabState.Selected:
                    topGradient = SystemColors.ControlLightLight;
                    bottomGradient = SystemColors.Control;
                    radius = 5;
                    break;
                case TabState.Hot:
                    border = Color.LightBlue;
                    break;
            }

            RectangleCorners corners;
            LinearGradientMode mode;
            Color tmp = Color.Empty;
            switch( align )
            {
                case TabAlignment.Left:
                    corners = RectangleCorners.Left;
                    mode = LinearGradientMode.Horizontal;
                    break;
                case TabAlignment.Right:
                    corners = RectangleCorners.Right;
                    mode = LinearGradientMode.Horizontal;
                    tmp = topGradient;
                    topGradient = bottomGradient;
                    bottomGradient = tmp;
                    break;
                case TabAlignment.Bottom:
                    corners = RectangleCorners.Bottom;
                    mode = LinearGradientMode.Vertical;
                    tmp = topGradient;
                    topGradient = bottomGradient;
                    bottomGradient = tmp;
                    break;
                default: // TabAlignment.Top
                    corners = RectangleCorners.Top;
                    mode = LinearGradientMode.Vertical;
                    break;
            }

            SmoothingMode sm = g.SmoothingMode;
            g.SmoothingMode = SmoothingMode.HighQuality;
            using( GraphicsPath path = RoundedRectangle.Create( bounds, radius, corners ) )
            {
                using( LinearGradientBrush brush = new LinearGradientBrush( bounds, topGradient, bottomGradient, mode ) )
                {
                    g.FillPath( brush, path );
                }
                using( Pen pen = new Pen( border ) )
                {
                    g.DrawPath( pen, path );
                }
            }
            g.SmoothingMode = sm;
        }

        private static void DrawTabText( Graphics g, Rectangle bounds, string text, Font font, TabState state, TabAlignment align )
        {
            if( String.IsNullOrEmpty( text ) )
                return;

            Color textColor = SystemColors.ControlLightLight;

            switch( state )
            {
                case TabState.Disabled:
                    textColor = SystemColors.ControlDarkDark;
                    break;
                case TabState.Selected:
                    textColor = SystemColors.ControlText;
                    break;
                case TabState.Hot:
                    textColor = Color.LightBlue;
                    break;
            }

            switch( align )
            {
                case TabAlignment.Left:
                    DrawTabTextVertical( g, bounds, text, font, textColor, RotateFlipType.Rotate270FlipNone );
                    break;
                case TabAlignment.Right:
                    DrawTabTextVertical( g, bounds, text, font, textColor, RotateFlipType.Rotate90FlipNone );
                    break;
                default:
                    DrawTabTextHorizontal( g, bounds, text, font, textColor );
                    break;
            }
        }

        private static void DrawTabTextHorizontal( Graphics g, Rectangle bounds, string text, Font font, Color textColor )
        {
            TextFormatFlags tff = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding;
            TextRenderer.DrawText( g, text, font, bounds, textColor, tff );
        }

        private static void DrawTabTextVertical( Graphics g, Rectangle bounds, string text, Font font, Color textColor, RotateFlipType rotateFlipType )
        {
            // Render text normally onto a bitmap, then rotate the bitmap and render that onto the actual background.
            using( Bitmap tmpBpm = new Bitmap( bounds.Height, bounds.Width ) )
            {
                using( Graphics gfx = Graphics.FromImage( tmpBpm ) )
                {
                    gfx.PageUnit = GraphicsUnit.Point;
                    gfx.TextRenderingHint = TextRenderingHint.AntiAlias;
                    gfx.SmoothingMode = SmoothingMode.HighQuality;

                    Rectangle rect = new Rectangle( 0, 0, bounds.Height, bounds.Width );
                    TextFormatFlags tff = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding;
                    TextRenderer.DrawText( gfx, text, font, rect, textColor, tff );
                    gfx.Flush();
                }
                tmpBpm.RotateFlip( rotateFlipType );
                g.DrawImageUnscaled( tmpBpm, bounds );
            }
        }

        #endregion
    }
}
