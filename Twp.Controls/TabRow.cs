﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    [Description( "Provides a row of tabs, just like TabControl but without any directly linked content." )]
    [Designer( typeof( Design.TabRowDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
    [ToolboxBitmap( typeof( System.Windows.Forms.TabControl ) )]
    [DefaultProperty( "ItemSize" )]
    [DefaultEvent( "TabSelected" )]
    public class TabRow : Control
    {
        #region Constructor

        public TabRow() : base()
		{
			this.SetStyle( ControlStyles.UserPaint
				| ControlStyles.AllPaintingInWmPaint
				| ControlStyles.FixedHeight
				| ControlStyles.OptimizedDoubleBuffer
				| ControlStyles.ResizeRedraw
				| ControlStyles.SupportsTransparentBackColor, true );
			this.ResizeRedraw = true;
            this.tabs.ListChanged += new ListChangedEventHandler( OnTabsChanged );
		}

        #endregion

        #region Properties

        [Category( "Appearance" )]
        [Description( "Indicates whether hottracking is enabled." )]
        [DefaultValue( true )]
        public bool HotTrack
        {
            get { return this.hotTrack; }
            set
            {
                if( this.hotTrack != value )
                {
                    this.hotTrack = value;
                }
            }
        }
        private bool hotTrack = true;

        [Category( "Appearance" )]
        [Description( "Determins the width of fixed-width tabs, and the height of all tabs." )]
        [DefaultValue( typeof( Size ), "60, 18" )]
		public Size ItemSize
		{
			get { return this.itemSize; }
			set
			{
				if( this.itemSize != value )
				{
					this.itemSize = value;
					this.ResizeTabs();
				}
			}
		}
        private Size itemSize = new Size( 60, 18 );

        [Category( "Appearance" )]
        [Description( "Indicates how much extra space should be added around the text in the tab." )]
        [DefaultValue( typeof( Point ), "9, 2" )]
		public new Point Padding
		{
			get { return this.padding; }
			set
			{
				if( this.padding != value )
				{
					this.padding = value;
					this.ResizeTabs();
				}
			}
		}
        private Point padding = new Point( 9, 2 );

        [Category( "Appearance" )]
        [Description( "Indicates how Tabs are sized." )]
        [DefaultValue( TabSizeMode.Normal )]
        public TabSizeMode SizeMode
        {
            get { return this.sizeMode; }
            set
            {
                if( this.sizeMode != value )
                {
                    this.sizeMode = value;
                    this.ResizeTabs();
                }
            }
        }
        private TabSizeMode sizeMode = TabSizeMode.Normal;

        [Category( "Appearance" )]
        [Description( "Indicates the position (and orientation) of the Tabs." )]
        [DefaultValue( TabAlignment.Top )]
        public TabAlignment Alignment
        {
            get { return this.alignment; }
            set
            {
                if( this.alignment != value )
                {
                    this.alignment = value;
                    switch( this.alignment )
                    {
                        case TabAlignment.Top:
                        case TabAlignment.Bottom:
                            if( this.Width < this.Height )
                            {
                                int temp = this.Width;
                                this.Width = this.Height;
                                this.Height = temp;
                            }
                            break;

                        default: // Left/Right
                            if( this.Width > this.Height )
                            {
                                int temp = this.Width;
                                this.Width = this.Height;
                                this.Height = temp;
                            }
                            break;
                    }
                    if( this.autoSize == true )
                        this.Size = FitSize;
                    this.ResizeTabs();
                }
            }
        }
        private TabAlignment alignment = TabAlignment.Top;
        
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
        public override bool AutoSize
        {
            get { return this.autoSize; }
            set
            {
                if( this.autoSize != value )
                {
                    this.autoSize = value;
                    if( this.autoSize == true )
                        this.Size = FitSize;
                }
            }
        }
        private bool autoSize = true;

        public new Size Size
        {
            get { return base.Size; }
            set
            {
                switch( this.alignment )
                {
                    case TabAlignment.Bottom:
                    case TabAlignment.Top:
                        this.Width = value.Width;
                        this.Height = FitSize.Height;
                        break;
                    default:
                        this.Width = FitSize.Width;
                        this.Height = value.Height;
                        break;
                }
            }
        }

        private Size FitSize
        {
            get
            {
                Size fitSize;
                switch( this.alignment )
                {
                    case TabAlignment.Bottom:
                    case TabAlignment.Top:
                        fitSize = new Size( this.ClientSize.Width, this.itemSize.Height + 2 );
                        if( this.itemSize.Height == 0 )
                            fitSize.Height = this.ClientSize.Height;
                        break;

                    default:
                        fitSize = new Size( this.itemSize.Height + 2, this.ClientSize.Height );
                        if( this.itemSize.Height == 0 )
                            fitSize.Width = this.ClientSize.Width;
                        break;
                }
                return fitSize;
            }
        }

        [Category( "Behavior" )]
        [Description( "The Tabs in the TabRow" )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public TabCollection Tabs
        {
            get { return this.tabs; }
        }
        private TabCollection tabs = new TabCollection();

        [Browsable( false )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        public Tab SelectedTab
        {
            get { return this.selectedTab; }
            set
            {
                if( this.selectedTab != value )
                {
                    this.SelectTab( value );
                    this.OnTabSelected();
                }
            }
        }
        private Tab selectedTab = null;
        private Tab hotTab = null;
        private int firstTab = 0;
        private int lastTab = 0;

        private bool hoverBack = false;
        private bool hoverForward = false;

        private Rectangle BackArrowRectangle
        {
            get
            {
                if( this.backArrowRectangle == Rectangle.Empty )
                {
                    this.backArrowRectangle = this.GetArrowRect( true );
                }
                return this.backArrowRectangle;
            }
        }
        private Rectangle backArrowRectangle = Rectangle.Empty;

        private Rectangle ForwardArrowRectangle
        {
            get
            {
                if( this.forwardArrowRectangle == Rectangle.Empty )
                {
                    this.forwardArrowRectangle = this.GetArrowRect( false );
                }
                return this.forwardArrowRectangle;
            }
        }
        private Rectangle forwardArrowRectangle = Rectangle.Empty;

        #endregion

        #region Events

        [Category( "Behavior" )]
        [Description( "Occurs when a tab is selected." )]
        public event EventHandler TabSelected;

        #endregion

        #region Methods

        private Rectangle GetArrowRect( bool offset )
        {
            int height = this.itemSize.Height;
            if( height == 0 )
                height = this.GetTextSize( "|" ).Height;
            height++;

            Size size;
            Point location;
            switch( this.alignment )
            {
                case TabAlignment.Left:
                    size = new Size( height, height / 2 );
                    location = new Point( 1, this.ClientSize.Height - size.Height );
                    if( offset )
                        location.Y -= size.Height;
                    break;
                case TabAlignment.Right:
                    size = new Size( height, height / 2 );
                    location = new Point( this.ClientSize.Width - size.Width - 1, this.ClientSize.Height - size.Height );
                    if( offset )
                        location.Y -= size.Height;
                    break;
                case TabAlignment.Top:
                    size = new Size( height / 2, height );
                    location = new Point( this.ClientSize.Width - size.Width, 1 );
                    if( offset )
                        location.X -= size.Width;
                    break;
                default: // Bottom
                    size = new Size( height / 2, height );
                    location = new Point( this.ClientSize.Width - size.Width, this.ClientSize.Height - size.Height - 1 );
                    if( offset )
                        location.X -= size.Width;
                    break;
            }
            return new Rectangle( location, size );
        }

        protected override void OnDockChanged( EventArgs e )
        {
            base.OnDockChanged( e );
            switch( this.Dock )
            {
                case DockStyle.Top:
                    this.Alignment = TabAlignment.Top;
                    break;
                case DockStyle.Bottom:
                    this.Alignment = TabAlignment.Bottom;
                    break;
                case DockStyle.Left:
                    this.Alignment = TabAlignment.Left;
                    break;
                case DockStyle.Right:
                    this.Alignment = TabAlignment.Right;
                    break;
            }
        }

        internal void OnTabSelected()
        {
            if( this.TabSelected != null )
                this.TabSelected( this, EventArgs.Empty );
        }

        private void OnTabsChanged( object sender, ListChangedEventArgs e )
        {
            switch( e.ListChangedType )
            {
                case ListChangedType.ItemAdded:
                    if( this.selectedTab == null )
                        this.SelectTab( this.tabs[e.NewIndex] );
                    if( e.NewIndex < this.firstTab )
                        this.firstTab = e.NewIndex;
                    if( e.NewIndex >= this.lastTab )
                        this.firstTab -= e.NewIndex - this.lastTab;
                    break;
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.ItemDeleted:
                    if( !this.tabs.Contains( this.selectedTab ) )
                    {
                        if( this.tabs.Count > 0 )
                            this.SelectTab( this.tabs[this.tabs.Count - 1] );
                        else
                            this.SelectTab( null );
                    }
                    if( this.lastTab > this.tabs.Count )
                    {
                        this.firstTab--;
                        this.lastTab = this.tabs.Count;
                    }
                    break;
                case ListChangedType.Reset:
                    this.firstTab = 0;
                    this.lastTab = this.tabs.Count;
                    if( this.tabs.Count > 0 )
                        this.SelectTab( this.tabs[0] );
                    break;
                default:
                    break;
            }
            this.ResizeTabs();
        }

        public void SelectTab( Tab selected )
        {
            if( selected != null )
            {
                if( this.tabs.Count == 0 )
                    selected.State = TabState.Selected;
                else
                {
                    for( int i = 0; i < this.tabs.Count; i++ )
                    {
                        Tab tab = this.tabs[i];
                        if( tab.State == TabState.Disabled )
                            continue;

                        if( selected == tab )
                        {
                            tab.State = TabState.Selected;
                            if( i < this.firstTab )
                                this.firstTab = i;
                            if( i >= this.lastTab )
                                this.firstTab += i - this.lastTab + 1;
                        }
                        else
                            tab.State = TabState.Normal;
                    }
                }
            }
            this.selectedTab = selected;
            this.ResizeTabs();
        }

        private void HotTab( Point location )
        {
            this.hotTab = null;
            for( int i = this.firstTab; i < this.lastTab; i++ )
            {
                Tab tab = this.tabs[i];
                if( tab.State == TabState.Selected )
                    continue;

                if( tab.State == TabState.Disabled )
                    continue;

                if( location != Point.Empty && tab.Bounds.Contains( location ) )
                {
                    tab.State = TabState.Hot;
                    this.hotTab = tab;
                }
                else
                    tab.State = TabState.Normal;
            }
        }

		private void ResizeTabs()
		{
			Point location = this.ClientRectangle.Location;
            this.backArrowRectangle = Rectangle.Empty;
            this.forwardArrowRectangle = Rectangle.Empty;
            bool overflow = false;
            if( this.firstTab < 0 )
                this.firstTab = 0;

            int i = this.firstTab;
            for( i = this.firstTab; i < this.tabs.Count; i++ )
            {
                Tab tab = this.tabs[i];
                switch( this.alignment )
                {
                    case TabAlignment.Left:
                    case TabAlignment.Right:
				        tab.Bounds = this.GetTabRectVertical( location, tab.Text );
				        location.Offset( 0, tab.Bounds.Height );
                        if( tab.Bounds.Bottom > this.ClientSize.Height - tab.Bounds.Width )
                            overflow = true;
                        break;

                    default:
                        tab.Bounds = this.GetTabRectHorizontal( location, tab.Text );
                        location.Offset( tab.Bounds.Width, 0 );
                        if( tab.Bounds.Right > this.ClientSize.Width - tab.Bounds.Height )
                            overflow = true;
                        break;
                }

                if( overflow )
                    break;
			}
            this.lastTab = i;
            this.Invalidate();
		}

        private Rectangle GetTabRectHorizontal( Point location, string text )
        {
            Size itemSize;

            if( this.sizeMode == TabSizeMode.Fixed )
                itemSize = new Size( (this.ClientSize.Width - 3) / this.tabs.Count, this.itemSize.Height );
            else
                itemSize = this.itemSize;

            Rectangle rect = new Rectangle( location, itemSize );
            if( rect.Width == 0 || rect.Height == 0 )
            {
                Size textSize = this.GetTextSize( text );

                if( rect.Width == 0 )
                    rect.Width = (int) textSize.Width;

                if( rect.Height == 0 )
                    rect.Height = (int) textSize.Height;
            }

            rect.X += 2;
            if( this.alignment == TabAlignment.Top )
                rect.Y = this.ClientSize.Height - rect.Height;
            else
                rect.Y = 0;
            return rect;
        }

        private Rectangle GetTabRectVertical( Point location, string text )
        {
            Size itemSize;
            if( this.sizeMode == TabSizeMode.Fixed )
                itemSize = new Size( this.itemSize.Height, (this.ClientSize.Height - 3) / this.tabs.Count );
            else
                itemSize = new Size( this.itemSize.Height, this.itemSize.Width );

            Rectangle rect = new Rectangle( location, itemSize );
            if( rect.Width == 0 || rect.Height == 0 )
            {
                Size textSize = this.GetTextSize( text );

                if( rect.Width == 0 )
                    rect.Width = (int) textSize.Height;

                if( rect.Height == 0 )
                    rect.Height = (int) textSize.Width;
            }

            rect.Y += 2;
            if( this.alignment == TabAlignment.Left )
                rect.X = this.ClientSize.Width - rect.Width;
            else
                rect.X = 0;
            return rect;
        }

        private Size GetTextSize( string text )
        {
            Size textSize = Size.Empty;
            using( Graphics g = this.CreateGraphics() )
            {
                TextFormatFlags tff = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding;
                textSize = TextRenderer.MeasureText( g, text, this.Font, this.itemSize, tff );
                textSize.Width += this.padding.X * 2;
                textSize.Height += this.padding.Y * 2;
            }
            return textSize;
        }
        
        protected override void OnMouseMove( MouseEventArgs e )
        {
            if( this.firstTab > 0 && this.BackArrowRectangle.Contains( e.Location ) )
                this.hoverBack = true;
            else
                this.hoverBack = false;

            if( this.lastTab < this.tabs.Count && this.ForwardArrowRectangle.Contains( e.Location ) )
                this.hoverForward = true;
            else
                this.hoverForward = false;

            if( this.hotTrack )
                this.HotTab( e.Location );

            this.Invalidate();
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            if( this.hotTrack )
                this.HotTab( Point.Empty );

            this.hoverBack = false;
            this.hoverForward = false;

            this.Invalidate();
        }

        protected override void OnMouseClick( MouseEventArgs e )
        {
            this.Focus();
            if( e.Button == MouseButtons.Left )
            {
                if( this.firstTab > 0 && this.BackArrowRectangle.Contains( e.Location ) )
                {
                    this.firstTab--;
                    this.lastTab--;
                    this.ResizeTabs();
                    return;
                }
                if( this.lastTab < this.tabs.Count && this.ForwardArrowRectangle.Contains( e.Location ) )
                {
                    this.firstTab++;
                    this.lastTab++;
                    this.ResizeTabs();
                    return;
                }

                if( this.hotTrack )
                {
                    if( this.hotTab == null )
                        this.HotTab( e.Location );
                    if( this.hotTab != null )
                    {
                        this.SelectedTab = this.hotTab;
                        return;
                    }
                }

                for( int i = this.firstTab; i < this.lastTab; i++ )
                {
                    Tab tab = this.tabs[i];
                    if( tab.Bounds.Contains( e.Location ) )
                        this.SelectedTab = tab;
                }
            }
        }

        protected override void OnResize( EventArgs e )
        {
            this.ResizeTabs();
            base.OnResize( e );
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            try
            {
                Log.DebugIf( this.selectedTab == null, "[TabRow.OnPaint] Tabs: {0} (First: {1} Last: {2})", this.tabs.Count, this.firstTab, this.lastTab );
                Log.DebugIf( this.selectedTab != null, "[TabRow.OnPaint] Tabs: {0} (First: {1} Last: {2} Selected: {3})", this.tabs.Count, this.firstTab, this.lastTab, this.tabs.IndexOf( this.selectedTab ) );
                if( this.tabs.Count > 0 )
                {
                    for( int i = this.firstTab; i < this.lastTab; i++ )
                    {
                        Tab tab = this.tabs[i];
                        if( tab == this.selectedTab )
                            continue;
                        if( this.hotTrack && tab == this.hotTab )
                            continue;
                        this.PaintTab( e.Graphics, tab );
                    }
                    if( this.hotTrack && this.hotTab != null )
                        this.PaintTab( e.Graphics, this.hotTab );
                    if( this.selectedTab != null )
                    {
                        int index = this.tabs.IndexOf( this.selectedTab );
                        if( index >= this.firstTab && index < this.lastTab )
                            this.PaintTab( e.Graphics, this.selectedTab );
                    }
                }
                if( this.firstTab > 0 || this.lastTab < this.tabs.Count )
                {
                    switch( this.alignment )
                    {
                        case TabAlignment.Left:
                        case TabAlignment.Right:
                            TabRenderer.DrawArrow( e.Graphics, this.BackArrowRectangle, this.firstTab > 0, this.hoverBack, TabArrow.Up );
                            TabRenderer.DrawArrow( e.Graphics, this.ForwardArrowRectangle, this.lastTab < this.tabs.Count, this.hoverForward, TabArrow.Down );
                            break;

                        default:
                            TabRenderer.DrawArrow( e.Graphics, this.BackArrowRectangle, this.firstTab > 0, this.hoverBack, TabArrow.Left );
                            TabRenderer.DrawArrow( e.Graphics, this.ForwardArrowRectangle, this.lastTab < this.tabs.Count, this.hoverForward, TabArrow.Right );
                            break;
                    }
                }
            }
            catch( Exception ex )
            {
                ExceptionDialog.Show( ex );
                throw;
            }
        }

        private void PaintTab( Graphics g, Tab tab )
        {
            Rectangle rect = tab.Bounds;
            if( tab.State == TabState.Selected )
            {
                rect.Inflate( 1, 1 );
                switch( this.alignment )
                {
                    case TabAlignment.Bottom:
                        rect.Offset( -1, 0 );
                        break;
                    case TabAlignment.Right:
                        rect.Offset( 0, -1 );
                        break;
                    default:
                        rect.Offset( -1, -1 );
                        break;
                }
            }
            else
            {
                switch( this.alignment )
                {
                    case TabAlignment.Left:
                        rect.Offset( 0, -1 );
                        break;
                    case TabAlignment.Right:
                        rect.Offset( -1, -1 );
                        break;
                    default:
                        rect.Offset( -1, 0 );
                        break;
                }
            }

            TabRenderer.DrawTab( g, rect, tab.Text, this.Font, tab.State, this.alignment );
        }

        #endregion

        #region Internal classes

        [DefaultProperty( "Text" )]
        [TypeConverter( typeof( TabConverter ) ), DesignTimeVisible( false ), ToolboxItem( false )]
        public class Tab : Component, IEquatable<Tab>, INotifyPropertyChanged
		{
            public Tab()
            {
            }

			public Tab( string text )
			{
				this.text = text;
			}

			[Category( "Appearance" )]
			[Description( "The text displayed on this Tab" )]
			public string Text
			{
				get { return this.text; }
				set
				{
					if( this.text != value )
					{
						this.text = value;
						this.OnPropertyChanged( new PropertyChangedEventArgs( "Text" ) );
					}
				}
			}
			private string text = String.Empty;

			[Browsable( false )]
			[EditorBrowsable( EditorBrowsableState.Never )]
			[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
            public TabState State
			{
				get { return this.state; }
				set
                {
                    if( this.state != value )
                    {
                        this.state = value;
                        this.OnPropertyChanged( new PropertyChangedEventArgs( "State" ) );
                    }
                }
			}
			private TabState state = TabState.Normal;

			[Browsable( false )]
			[EditorBrowsable( EditorBrowsableState.Never )]
			[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
			public Rectangle Bounds
			{
				get { return this.bounds; }
				set { this.bounds = value; }
			}
			private Rectangle bounds = Rectangle.Empty;

            [Browsable( false )]
            [EditorBrowsable( EditorBrowsableState.Never )]
			[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
            public object Tag
            {
                get { return this.tag; }
                set { this.tag = value; }
            }
            private object tag;

            public override string ToString()
            {
                if( String.IsNullOrEmpty( this.text ) )
                    return base.ToString();
                else
                    return this.text;
            }

			#region INotifyPropertyChanged Members

			public event PropertyChangedEventHandler PropertyChanged;
			protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
			{
				if( this.PropertyChanged != null )
					this.PropertyChanged( this, e );
			}

			#endregion

			#region IEquatable

			public override int GetHashCode()
			{
				return this.text.GetHashCode();
			}

			public bool Equals( Tab other )
			{
				if( other == null )
					return false;
				else
					return this.text.Equals( other.text );
			}

			public override bool Equals( object obj )
			{
				Tab other = obj as Tab;
				if( other != null )
					return this.Equals( other );
				else
					return false;
			}

			public static bool operator ==( Tab tab1, Tab tab2 )
			{
				if( object.ReferenceEquals( tab1, tab2 ) )
					return true;
				if( object.ReferenceEquals( tab1, null ) )
					return false;
				if( object.ReferenceEquals( tab2, null ) )
					return false;

				return tab1.Equals( tab2 );
			}

			public static bool operator !=( Tab tab1, Tab tab2 )
			{
				if( object.ReferenceEquals( tab1, tab2 ) )
					return false;
				if( object.ReferenceEquals( tab1, null ) )
					return true;
				if( object.ReferenceEquals( tab2, null ) )
					return true;

				return !tab1.Equals( tab2 );
			}

			#endregion
		}

		public class TabCollection : BindingList<Tab>
		{
            public TabCollection() : base()
            {
            }

			public Tab this[string text]
			{
				get
				{
					foreach( Tab tab in this )
					{
						if( tab.Text == text )
							return tab;
					}
					return null;
				}
			}

			public bool Contains( string text )
			{
				foreach( Tab tab in this )
				{
					if( tab.Text == text )
						return true;
				}
				return false;
			}

            protected override void OnAddingNew( AddingNewEventArgs e )
            {
                Tab tab = (Tab) e.NewObject;
                if( tab != null && String.IsNullOrEmpty( tab.Text ) )
                {
			         tab.Text = "tab" + (this.Count + 1).ToString();
                }
                base.OnAddingNew( e );
            }
			
			public Tab Add( string text )
			{
			    if( String.IsNullOrEmpty( text ) )
			        text = "tab" + (this.Count + 1).ToString();
				Tab tab = new Tab( text );
				this.Add( tab );
				return tab;
			}

			public void Remove( string text )
			{
				Tab tab = this[text];
				this.Remove( tab );
			}

            public Tab GetByTag( object tag )
            {
                foreach( Tab tab in this )
                {
                    if( tab.Tag == tag )
                        return tab;
                }
                return null;
            }
		}

        internal class TabConverter : TypeConverter
        {
            public override bool CanConvertTo( ITypeDescriptorContext context, Type destType )
            {
                if( destType == typeof( InstanceDescriptor ) )
                    return true;

                return base.CanConvertTo( context, destType );
            }

            public override object ConvertTo( ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destType )
            {
                if( destType == typeof( InstanceDescriptor ) )
                {
                    System.Reflection.ConstructorInfo ci = typeof( Tab ).GetConstructor( System.Type.EmptyTypes );

                    return new InstanceDescriptor( ci, null, false );
                }

                return base.ConvertTo( context, culture, value, destType );
            }
        }

		#endregion
	}
}
