﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    public class TaskCollection : IEnumerable
    {
        public TaskCollection( TaskListBox owner )
        {
            _owner = owner;
        }

        #region Fields

        TaskListBox _owner;
        List<ProgressTask> _tasks = new List<ProgressTask>();
        List<int> _heights = new List<int>();

        #endregion

        #region Properties

        public ProgressTask this[int index]
        {
            get { return _tasks[index]; }
        }

        public int Count
        {
            get { return _tasks.Count; }
        }

        internal int Height
        {
            get
            {
                int height = 0;
                foreach( int h in _heights )
                    height += h;
                return height;
            }
        }

        #endregion

        #region Methods

        internal int GetTaskHeight( int index )
        {
            if( index < 0 || index >= _heights.Count )
                throw new ArgumentOutOfRangeException( "index" );

            return _heights[index];
        }

        internal void SetTaskHeight( int index, int height )
        {
            if( index < 0 || index >= _heights.Count )
                throw new ArgumentOutOfRangeException( "index" );

            _heights[index] = height;
        }

        internal int GetOffset( int index )
        {
            int offset = 0;
            for( int i = 0; i < index; i++ )
                offset += _heights[i];
            return offset;
        }

        public int Add( ProgressTask task )
        {
            int index = _tasks.Count;
            _tasks.Add( task );
            _heights.Add( 0 );
            _owner.InvalidateTask( index );
            return index;
        }

        public void Remove( ProgressTask task )
        {
            RemoveAt( _tasks.IndexOf( task ) );
        }

        public void RemoveAt( int index )
        {
            _tasks.RemoveAt( index );
            _heights.RemoveAt( index );
        }

        public int IndexOf( ProgressTask task )
        {
            return _tasks.IndexOf( task );
        }

        #endregion

        #region IEnumerable implementation

        public IEnumerator GetEnumerator()
        {
            return _tasks.GetEnumerator();
        }

        #endregion
    }
}


