﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    public class TaskListBox : Control
    {
        #region Constructor

        public TaskListBox()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.ResizeRedraw |
                ControlStyles.Selectable |
                ControlStyles.UserMouse |
                ControlStyles.UserPaint, true );

            _tasks = new TaskCollection( this );
            
            _scroll = new VScrollBar();
            _scroll.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            _scroll.Size = new Size( 20, Height - 2 );
            _scroll.Location = new Point( Width - 21, 1 );
            _scroll.Scroll += OnScroll;
            Controls.Add( _scroll );
        }

        #endregion

        #region Fields

        TaskCollection _tasks;
        VScrollBar _scroll;

        #endregion

        #region Properties

        public TaskCollection Tasks
        {
            get { return _tasks; }
        }

        public new Rectangle ClientRectangle
        {
            get
            {
                Rectangle rect = base.ClientRectangle;
                rect.Inflate( -2, -2 );
                if( _scroll.Visible )
                    rect.Width -= _scroll.Width;
                return rect;
            }
        }

        #endregion

        #region Methods
        
        void OnScroll( object sender, ScrollEventArgs e )
        {
            Invalidate();
        }
        
        void SetScrollValue( int value )
        {
            if( value < 0 )
                value = 0;
            int max = _scroll.Maximum - _scroll.LargeChange;
            if( value > max )
                value = max;

            if( ( value >= 0 && value <= max ) && ( value != _scroll.Value ) )
            {
                ScrollEventArgs e = new ScrollEventArgs( ScrollEventType.ThumbPosition, _scroll.Value, value, ScrollOrientation.VerticalScroll );
                _scroll.Value = value;
                OnScroll( _scroll, e );
            }
        }

        int GetScrollValue()
        {
            if( _scroll.Visible )
                return _scroll.Value;
            return 0;
        }

        void UpdateScrollbar()
        {
            if( this.ClientRectangle.Width < 0 )
                return;

            if( _tasks.Height <= ClientRectangle.Height )
            {
                _scroll.Visible = false;
                SetScrollValue( 0 );
            }
            else
            {
                int maxScrollValue = _tasks.Height - ClientRectangle.Height;
                _scroll.Visible = true;
                _scroll.SmallChange = 1;
                _scroll.LargeChange = 20;
                _scroll.Minimum = 0;
                _scroll.Maximum = maxScrollValue + _scroll.LargeChange;

                if( _scroll.Value > maxScrollValue )
                    SetScrollValue( maxScrollValue );
            }
        }

        public void EnsureVisible( int index )
        {
            if( !_scroll.Visible )
                return;

            int offset = _tasks.GetOffset( index );
            SetScrollValue( offset );
        }

        public Rectangle GetTaskRectangle( int index )
        {
            int offset = ClientRectangle.Top - _scroll.Value + _tasks.GetOffset( index );
            return new Rectangle( ClientRectangle.Left, offset, ClientRectangle.Width, _tasks.GetTaskHeight( index ) );
        }

        public void InvalidateTask( int index )
        {
            // FIXME: Configurable item height?
            MeasureTaskEventArgs e = new MeasureTaskEventArgs( CreateGraphics(), index, 21 );
            OnMeasureTask( e );
            _tasks.SetTaskHeight( index, e.Height );
            UpdateScrollbar();
            Invalidate();
        }

        protected virtual void OnMeasureTask( MeasureTaskEventArgs e )
        {
            ProgressTask task = _tasks[e.Index];
            Size proposedSize = new Size( ClientRectangle.Width, e.Height );
            Size size = TextRenderer.MeasureText( e.Graphics, task.FullText, Font, proposedSize );
            size.Height += 8;
            if( !task.Finished )
                size.Height *= 2;
            e.Height = size.Height;
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );
            using( SolidBrush brush = new SolidBrush( BackColor ) )
                e.Graphics.FillRectangle( brush, DisplayRectangle );
            ControlPaint.DrawBorder3D( e.Graphics, DisplayRectangle, Border3DStyle.SunkenInner );
            
            int bottom = 0;
            for( int i = 0; i < _tasks.Count; i++ )
            {
                Rectangle bounds = GetTaskRectangle( i );
                if( e.ClipRectangle.IntersectsWith( bounds ) )
                    OnDrawTask( new DrawTaskEventArgs( e.Graphics, bounds, i ) );
                bottom = bounds.Bottom;
            }
            if( bottom < ClientRectangle.Bottom )
            {
                Rectangle filler = new Rectangle( ClientRectangle.Left, bottom, ClientRectangle.Width, ClientRectangle.Bottom - bottom );
                using( SolidBrush brush = new SolidBrush( BackColor ) )
                    e.Graphics.FillRectangle( brush, filler );
            }
        }

        protected virtual void OnDrawTask( DrawTaskEventArgs e )
        {
            ProgressTask task = _tasks[e.Index];
            Rectangle bounds = e.Bounds;
            bounds.Inflate( -1, -1 );
            e.Graphics.DrawRectangle( SystemPens.ActiveBorder, bounds );
            bounds.Inflate( -3, -3 );
            TextRenderer.DrawText( e.Graphics, task.FullText, Font, bounds, ForeColor, TextFormatFlags.Left );
            TextRenderer.DrawText( e.Graphics, task.PercentText, Font, bounds, ForeColor, TextFormatFlags.Right );
            if( !task.Finished )
            {
                Rectangle barRect = new Rectangle( bounds.X, bounds.Y + ( bounds.Height / 2 ),
                                        bounds.Width, ( bounds.Height / 2 ) );
                ProgressBarRenderer.DrawHorizontalBar( e.Graphics, barRect );
                barRect.Inflate( -1, -1 );
                barRect.Width = (int) ( (double) task.Value / (double) task.Maximum * (double) barRect.Width );
                ProgressBarRenderer.DrawHorizontalChunks( e.Graphics, barRect );
            }
        }

        protected override void OnSizeChanged( EventArgs e )
        {
            base.OnSizeChanged( e );
            if( ClientRectangle.Width > 0 && ClientRectangle.Height > 0 )
                UpdateScrollbar();
        }

        protected override void OnMouseWheel( MouseEventArgs e )
        {
            int value = _scroll.Value - ( e.Delta * SystemInformation.MouseWheelScrollLines / 120 );
            if( _scroll.Visible )
                SetScrollValue( value );
            base.OnMouseWheel( e );
        }

        #endregion
    }
    
    public class MeasureTaskEventArgs : EventArgs
    {
        public MeasureTaskEventArgs( Graphics graphics, int index, int height )
        {
            _graphics = graphics;
            _index = index;
            _height = height;
        }

        readonly Graphics _graphics;
        readonly int _index;
        int _height;

        public Graphics Graphics
        {
            get { return _graphics; }
        }

        public int Index
        {
            get { return _index; }
        }
        
        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }
    }

    public class DrawTaskEventArgs : EventArgs
    {
        public DrawTaskEventArgs( Graphics graphics, Rectangle bounds, int index )
        {
            _graphics = graphics;
            _bounds = bounds;
            _index = index;
        }

        readonly Graphics _graphics;
        readonly Rectangle _bounds;
        readonly int _index;

        public Graphics Graphics
        {
            get { return _graphics; }
        }

        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public int Index
        {
            get { return _index; }
        }
    }
}
