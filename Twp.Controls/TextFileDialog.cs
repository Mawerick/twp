﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Description of TextFileDialog.
    /// </summary>
    public partial class TextFileDialog : Form
    {
        public TextFileDialog()
        {
            InitializeComponent();
        }
        
        private string fileName;
        public string FileName
        {
            get { return this.fileName; }
            set
            {
                if( this.fileName != value )
                {
                    this.fileName = value;
                    this.ReadFile();
                }
            }
        }
        
        private void ReadFile()
        {
            if( !File.Exists( this.fileName ) )
            {
                this.Text = "File not found";
                this.textBox.Text = "File not found: " + this.fileName;
                return;
            }

            FileInfo fi = new FileInfo( this.fileName );
            this.Text = fi.Name;
            using( TextReader reader = new StreamReader( this.fileName, Encoding.UTF8 ) )
            {
                this.textBox.Text = reader.ReadToEnd();
                reader.Close();
            }
        }

        public static void ShowFile( string fileName )
        {
            using( TextFileDialog dialog = new TextFileDialog() )
            {
                dialog.FileName = fileName;
                dialog.ShowDialog();
            }
        }
    }
}
