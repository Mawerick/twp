﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Twp.Utilities;
using System.Drawing.Drawing2D;

namespace Twp.Controls
{
    [Designer( typeof( Design.TogglePanelDesigner ), typeof( IDesigner ) )]
    public class TogglePanel : Control, INotifyPropertyChanged
    {
        #region Constructor

        public TogglePanel() : base()
        {
            this.Size = new Size( 100, 100 );
            this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ContainerControl |
                ControlStyles.Opaque |
                ControlStyles.ResizeRedraw |
                ControlStyles.UserPaint |
                ControlStyles.SupportsTransparentBackColor,
                true );
            this.BackColor = Color.Transparent;
        }

        #endregion

        #region Properties

        [Category( "Behavior" )]
        [DefaultValue( true )]
        public bool Collapsed
        {
            get { return this.collapsed; }
            set
            {
                if( this.collapsed != value )
                {
                    this.collapsed = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Collapsed" ) );
                    this.UpdateRegion();
                    this.Invalidate();
                }
            }
        }
        private bool collapsed = true;

        [Category( "Appearance" )]
        [DefaultValue( TabAlignment.Bottom )]
        public TabAlignment Alignment
        {
            get { return this.alignment; }
            set
            {
                if( this.alignment != value )
                {
                    this.alignment = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Alignment" ) );
                    this.ResizeButton();
                    this.UpdateRegion();
                    this.Invalidate();
                }
            }
        }
        private TabAlignment alignment = TabAlignment.Bottom;

        /// <summary>
        /// Gets the rectangle that represents the display area of the control.
        /// </summary>
        public override Rectangle DisplayRectangle
        {
            get
            {
                Rectangle rect = base.DisplayRectangle;
                switch( this.alignment )
                {
                    case TabAlignment.Bottom:
                        rect.Height -= ToggleButton.Height;
                        break;
                    case TabAlignment.Left:
                        rect.Width -= ToggleButton.Height;
                        rect.Offset( ToggleButton.Height, 0 );
                        break;
                    case TabAlignment.Right:
                        rect.Width -= ToggleButton.Height;
                        break;
                    case TabAlignment.Top:
                        rect.Height -= ToggleButton.Height;
                        rect.Offset( 0, ToggleButton.Height ); 
                        break;
                    default:
                        throw new Exception( String.Format( "Unknown Alignment {0}", this.alignment ) );
                }
                //rect.Size -= this.Padding.Size;
                //rect.Offset( this.Padding.Left, this.Padding.Top );
                return rect;
            }
        }

        private ToggleButton button = new ToggleButton();

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            if( this.PropertyChanged != null )
                this.PropertyChanged( this, e );
        }

        #endregion

        #region Methods

        private void ResizeButton()
        {
            Point p;
            Size s;
            switch( this.alignment )
            {
                case TabAlignment.Bottom:
                    p = new Point( (this.Width - ToggleButton.Width) / 2, this.Height - ToggleButton.Height );
                    s = new Size( ToggleButton.Width, ToggleButton.Height );
                    break;
                case TabAlignment.Left:
                    p = new Point( 0, (this.Height - ToggleButton.Width) / 2 );
                    s = new Size( ToggleButton.Height, ToggleButton.Width );
                    break;
                case TabAlignment.Right:
                    p = new Point( this.Width - ToggleButton.Height, (this.Height - ToggleButton.Width) / 2 );
                    s = new Size( ToggleButton.Height, ToggleButton.Width );
                    break;
                case TabAlignment.Top:
                    p = new Point( (this.Width - ToggleButton.Width) / 2, 0 );
                    s = new Size( ToggleButton.Width, ToggleButton.Height );
                    break;
                default:
                    throw new Exception( String.Format( "Unknown Alignment {0}", this.alignment ) );
            }
            this.button.Rectangle = new Rectangle( p, s );
        }

        private void UpdateRegion()
        {
            Region region = new Region( this.button.Rectangle );
            if( this.collapsed == false || this.DesignMode )
                region.Union( this.DisplayRectangle );
            this.Region = region;
        }

        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.ResizeButton();
            this.UpdateRegion();
        }

        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );

            if( e.Button == System.Windows.Forms.MouseButtons.Left )
            {
                if( this.button.Rectangle.Contains( e.Location ) )
                {
                    this.button.State = PushButtonState.Pressed;
                    this.Capture = true;
                    this.Invalidate();
                }
            }
        }

        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );

            if( this.Capture )
            {
                this.Capture = false;

                if( this.button.Rectangle.Contains( e.Location ) )
                {
                    this.button.State = PushButtonState.Hot;
                    this.Collapsed = !this.collapsed;
                }
                else
                    this.button.State = PushButtonState.Normal;
                this.Invalidate();
            }
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            if( this.button.Rectangle.Contains( e.Location ) )
            {
                if( e.Button == System.Windows.Forms.MouseButtons.Left )
                    this.button.State = PushButtonState.Pressed;
                else
                    this.button.State = PushButtonState.Hot;
            }
            else
                this.button.State = PushButtonState.Normal;
            this.Invalidate();
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );

            this.button.State = PushButtonState.Normal;
            this.Invalidate();
        }

        protected override void OnPaintBackground( PaintEventArgs e )
        {
            if( this.DesignMode )
            {
                using( Brush brush = new SolidBrush( this.Parent.BackColor ) )
                {
                    e.Graphics.FillRectangle( brush, this.button.Rectangle );
                }
            }
            else
            {
                this.PaintBackground( e.Graphics, this.button.Rectangle );
            }
        }

        private void PaintBackground( Graphics g, Rectangle rect )
        {
            if( this.Parent != null )
            {
                rect.Offset( this.Location );
                PaintEventArgs pe = new PaintEventArgs( g, rect );
                GraphicsState state = g.Save();
                g.SmoothingMode = SmoothingMode.HighSpeed;
                try
                {
                    g.TranslateTransform( (float) -this.Location.X, (float) -this.Location.Y );
                    this.InvokePaintBackground( this.Parent, pe );
                    this.InvokePaint( this.Parent, pe );
                }
                finally
                {
                    g.Restore( state );
                    rect.Offset( -this.Location.X, -this.Location.Y );
                }
            }
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );

            ButtonRenderer.DrawButton( e.Graphics, this.button.Rectangle, this.button.State );

            ArrowDirection direction;
            switch( this.alignment )
            {
                case TabAlignment.Bottom:
                    direction = this.collapsed ? ArrowDirection.Up :  ArrowDirection.Down;
                    break;
                case TabAlignment.Left:
                    direction = this.collapsed ? ArrowDirection.Right :  ArrowDirection.Left;
                    break;
                case TabAlignment.Right:
                    direction = this.collapsed ? ArrowDirection.Left :  ArrowDirection.Right;
                    break;
                case TabAlignment.Top:
                    direction = this.collapsed ? ArrowDirection.Down :  ArrowDirection.Up;
                    break;
                default:
                    throw new Exception( String.Format( "Unknown Alignment {0}", this.alignment ) );
            }
            Point[] arrowPoints = ToggleButton.GetArrow( this.button.Rectangle, direction );
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.FillPolygon( SystemBrushes.ControlLightLight, arrowPoints );
            e.Graphics.DrawPolygon( SystemPens.ControlDarkDark, arrowPoints );
        }

        #endregion

        #region Helper class

        internal class ToggleButton
        {
            public const int Width = 60;
            public const int Height = 12;

            private Rectangle rectangle = Rectangle.Empty;
            public Rectangle Rectangle
            {
                get { return this.rectangle; }
                set { this.rectangle = value; }
            }

            private PushButtonState state = PushButtonState.Normal;
            public PushButtonState State
            {
                get { return this.state; }
                set { this.state = value; }
            }

            public static Point[] GetArrow( Rectangle rect, ArrowDirection direction )
            {
                Point[] points = new Point[14];
                Point start = rect.Location;
                switch( direction )
                {
                    case ArrowDirection.Up:
                        start.Offset( (ToggleButton.Width / 2) - 15, (ToggleButton.Height / 2) - 3 );
                        points[0] = new Point( start.X, start.Y + 3 );
                        points[1] = new Point( start.X + 5, start.Y );
                        points[2] = new Point( start.X + 10, start.Y + 3 );
                        points[3] = new Point( start.X + 15, start.Y );
                        points[4] = new Point( start.X + 20, start.Y + 3 );
                        points[5] = new Point( start.X + 25, start.Y );
                        points[6] = new Point( start.X + 30, start.Y + 3 );

                        points[7] = new Point( start.X + 30, start.Y + 5 );
                        points[8] = new Point( start.X + 25, start.Y + 2 );
                        points[9] = new Point( start.X + 20, start.Y + 5 );
                        points[10] = new Point( start.X + 15, start.Y + 2 );
                        points[11] = new Point( start.X + 10, start.Y + 5 );
                        points[12] = new Point( start.X + 5, start.Y + 2 );
                        points[13] = new Point( start.X, start.Y + 5 );
                        break;
                    case ArrowDirection.Down:
                        start.Offset( (ToggleButton.Width / 2) - 15, (ToggleButton.Height / 2) - 3 );
                        points[0] = new Point( start.X, start.Y );
                        points[1] = new Point( start.X + 5, start.Y + 3 );
                        points[2] = new Point( start.X + 10, start.Y );
                        points[3] = new Point( start.X + 15, start.Y + 3 );
                        points[4] = new Point( start.X + 20, start.Y );
                        points[5] = new Point( start.X + 25, start.Y + 3 );
                        points[6] = new Point( start.X + 30, start.Y );

                        points[7] = new Point( start.X + 30, start.Y + 2 );
                        points[8] = new Point( start.X + 25, start.Y + 5 );
                        points[9] = new Point( start.X + 20, start.Y + 2 );
                        points[10] = new Point( start.X + 15, start.Y + 5 );
                        points[11] = new Point( start.X + 10, start.Y + 2 );
                        points[12] = new Point( start.X + 5, start.Y + 5 );
                        points[13] = new Point( start.X, start.Y + 2 );
                        break;
                    case ArrowDirection.Left:
                        start.Offset( (ToggleButton.Height / 2) - 3, (ToggleButton.Width / 2) - 15 );
                        points[0] = new Point( start.X + 3, start.Y );
                        points[1] = new Point( start.X, start.Y + 5 );
                        points[2] = new Point( start.X + 3, start.Y + 10 );
                        points[3] = new Point( start.X, start.Y + 15 );
                        points[4] = new Point( start.X + 3, start.Y + 20 );
                        points[5] = new Point( start.X, start.Y + 25 );
                        points[6] = new Point( start.X + 3, start.Y + 30 );

                        points[7] = new Point( start.X + 5, start.Y + 30 );
                        points[8] = new Point( start.X + 2, start.Y + 25 );
                        points[9] = new Point( start.X + 5, start.Y + 20 );
                        points[10] = new Point( start.X + 2, start.Y + 15 );
                        points[11] = new Point( start.X + 5, start.Y + 10 );
                        points[12] = new Point( start.X + 2, start.Y + 5 );
                        points[13] = new Point( start.X + 5, start.Y );
                        break;
                    case ArrowDirection.Right:
                        start.Offset( (ToggleButton.Height / 2) - 3, (ToggleButton.Width / 2) - 15 );
                        points[0] = new Point( start.X, start.Y );
                        points[1] = new Point( start.X + 3, start.Y + 5 );
                        points[2] = new Point( start.X, start.Y + 10 );
                        points[3] = new Point( start.X + 3, start.Y + 15 );
                        points[4] = new Point( start.X, start.Y + 20 );
                        points[5] = new Point( start.X + 3, start.Y + 25 );
                        points[6] = new Point( start.X, start.Y + 30 );

                        points[7] = new Point( start.X + 2, start.Y + 30 );
                        points[8] = new Point( start.X + 5, start.Y + 25 );
                        points[9] = new Point( start.X + 2, start.Y + 20 );
                        points[10] = new Point( start.X + 5, start.Y + 15 );
                        points[11] = new Point( start.X + 2, start.Y + 10 );
                        points[12] = new Point( start.X + 5, start.Y + 5 );
                        points[13] = new Point( start.X + 2, start.Y );
                        break;
                    default:
                        throw new ArgumentException( String.Format( "Unknown ArrowDirection {0}", direction ), "direction" );
                }
                return points;
            }
        }
        #endregion
    }
}
