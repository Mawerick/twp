﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Twp.Controls
{
    public enum ToolBoxRenderingMode
    {
        Styled,
        StyledBox,
        Gradient,
        Simple,
    };

    /// <summary>
    /// The ToolBox class.
    /// </summary>
    [DefaultProperty( "Text" )]
    [DefaultEvent( "CollapsedChanged" )]
    [Designer( typeof( Design.ToolBoxDesigner ), typeof( IDesigner ) )]
    public class ToolBox : Control
    {
        #region Constructor

        /// <summary>
        /// Creates a new instance of the ToolBox class.
        /// </summary>
        public ToolBox() : base()
        {
            this.SetStyle( ControlStyles.ContainerControl |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ResizeRedraw |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.SupportsTransparentBackColor,
                true );
        }

        #endregion

        #region Events

        public delegate void PaintTitlebarEventHandler( object sender, PaintTitlebarEventArgs e );
        public delegate void PaintCollapseButtonEventHandler( object sender, PaintCollapseButtonEventArgs e );
        public delegate void PaintBackgroundEventHandler( object sender, PaintBackgroundEventArgs e );

        public event PaintTitlebarEventHandler PaintTitlebar;
        public event PaintCollapseButtonEventHandler PaintCollapseButton;
        public event PaintBackgroundEventHandler PaintBackground;
        public event EventHandler CollapsedChanged;
        public event EventHandler AllowCollapseChanged;

        #endregion Events

        #region Properties

        [Category( "Colors" )]
        [Description( "The color of the titlebar text." )]
        [DefaultValue( typeof( Color ), "ControlLight" )]
        public Color TitlebarForeColor
        {
            get { return this.titlebarForeColor; }
            set
            {
                if( this.titlebarForeColor != value )
                {
                    this.titlebarForeColor = value;
                    this.Invalidate();
                }
            }
        }
        private Color titlebarForeColor = SystemColors.ControlLight;

        [Category( "Colors" )]
        [Description( "The background color of the titlebar." )]
        [DefaultValue( typeof( Color ), "ControlDark" )]
        public Color TitlebarBackColor
        {
            get { return this.titlebarBackColor; }
            set
            {
                if( this.titlebarBackColor != value )
                {
                    this.titlebarBackColor = value;
                    this.Invalidate();
                }
            }
        }
        private Color titlebarBackColor = SystemColors.ControlDark;

        [Category( "Colors" )]
        [Description( "The background gradient color of the titlebar." )]
        [DefaultValue( typeof( Color ), "ControlDarkDark" )]
        public Color TitlebarGradientColor
        {
            get { return this.titlebarGradientColor; }
            set
            {
                if( this.titlebarGradientColor != value )
                {
                    this.titlebarGradientColor = value;
                    this.Invalidate();
                }
            }
        }
        private Color titlebarGradientColor = SystemColors.ControlDarkDark;

        [Category( "Colors" )]
        [Description( "The border color of the titlebar." )]
        [DefaultValue( typeof( Color ), "ControlDarkDark" )]
        public Color TitlebarBorderColor
        {
            get { return this.titlebarBorderColor; }
            set
            {
                if( this.titlebarBorderColor != value )
                {
                    this.titlebarBorderColor = value;
                    this.Invalidate();
                }
            }
        }
        private Color titlebarBorderColor = SystemColors.ControlDarkDark;

        [Category( "Colors" )]
        [Description( "The background color of the content area." )]
        [DefaultValue( typeof( Color ), "ControlLightLight" )]
        public Color ContentBackColor
        {
            get { return this.contentBackColor; }
            set
            {
                if( this.contentBackColor != value )
                {
                    this.contentBackColor = value;
                    this.Invalidate();
                }
            }
        }
        private Color contentBackColor = SystemColors.ControlLightLight;

        [Category( "Colors" )]
        [Description( "The background gradient color of the content area." )]
        [DefaultValue( typeof( Color ), "ControlLight" )]
        public Color GradientColor
        {
            get { return this.gradientColor; }
            set
            {
                if( this.gradientColor != value )
                {
                    this.gradientColor = value;
                    this.Invalidate();
                }
            }
        }
        private Color gradientColor = SystemColors.ControlLight;

        [Category( "Appearance" )]
        [Description( "The Rendering mode used to draw the toolbox." )]
        [DefaultValue( ToolBoxRenderingMode.Styled )]
        public ToolBoxRenderingMode RenderingMode
        {
            get { return this.renderingMode; }
            set
            {
                if( this.renderingMode != value )
                {
                    this.renderingMode = value;
                    this.Invalidate();
                    this.PerformLayout();
                }
            }
        }
        private ToolBoxRenderingMode renderingMode = ToolBoxRenderingMode.Styled;

        /// <summary>
        /// Gets or sets the internal border style of the ToolBox.
        /// </summary>
        /// <remarks>
        /// Ignored if <see cref="RenderingMode"/> is <see cref="ToolBoxRenderingMode.Styled"/>.
        /// </remarks>
        [Category( "Appearance" )]
        [Description( "The internal border style. Not used if RenderingMode is Styled" )]
        [DefaultValue( BorderStyle.FixedSingle )]
        public BorderStyle BorderStyle
        {
            get { return this.borderStyle; }
            set
            {
                if( this.borderStyle != value )
                {
                    this.borderStyle = value;
                    this.Invalidate();
                    this.PerformLayout();
                }
            }
        }
        private BorderStyle borderStyle = BorderStyle.FixedSingle;

        /// <summary>
        /// Gets or sets whether to show a border around the titlebar.
        /// </summary>
        [Category( "Appearance" )]
        [Description( "Sets whether to show a border around the titlebar." )]
        [DefaultValue( true )]
        public bool TitlebarBorder
        {
            get { return this.titlebarBorder; }
            set
            {
                if( this.titlebarBorder != value )
                {
                    this.titlebarBorder = value;
                    this.Invalidate();
                }
            }
        }
        private bool titlebarBorder = true;
        
        [Category( "Appearance" )]
        [Description( "Defines the alignment of the text in the titlebar." )]
        [DefaultValue( HorizontalAlignment.Left )]
        public HorizontalAlignment TextAlign
        {
            get { return this.textAlign; }
            set
            {
                if( this.textAlign != value )
                {
                    this.textAlign = value;
                    this.Invalidate();
                }
            }
        }
        private HorizontalAlignment textAlign = HorizontalAlignment.Left;

        [Category( "Appearance" )]
        [Description( "Indicates how the titlebar and background should be rendered." )]
        [DefaultValue( GradientMode.Vertical )]
        public GradientMode GradientMode
        {
            get { return this.gradientMode; }
            set
            {
                if( this.gradientMode != value )
                {
                    this.gradientMode = value;
                    this.Invalidate();
                }
            }
        }
        private GradientMode gradientMode = GradientMode.Vertical;

        /// <summary>
        /// Gets the rectangle that represents the display area of the control.
        /// </summary>
        public override Rectangle DisplayRectangle
        {
            get
            {
                Rectangle rect = base.DisplayRectangle;
                rect.Height -= this.TitlebarHeight;
                rect.Offset( 0, this.TitlebarHeight );
                switch( this.renderingMode )
                {
                    case ToolBoxRenderingMode.Styled:
                    case ToolBoxRenderingMode.StyledBox:
                        rect.Inflate( -1, -1 );
                        break;

                    default:
                        rect.Height -= 1;
                        rect.Offset( 0, 1 );
                        switch( this.borderStyle )
                        {
                            case BorderStyle.None:
                                break;
                            case BorderStyle.FixedSingle:
                            case BorderStyle.Fixed3D:
                                rect.Inflate( -2, -2 );
                                break;
                        }
                        break;
                }
                // Apply padding
                rect.Size -= this.Padding.Size;
                rect.Offset( this.Padding.Left, this.Padding.Top );
                return rect;
            }
        }

        /// <summary>
        /// Gets or sets the title text of the ToolBox.
        /// </summary>
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [Description( "The title to display in the titlebar of the ToolBox" )]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if( base.Text != value )
                {
                    base.Text = value;
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "Indicates whether the panel is in the collapsed state." )]
        [DefaultValue( false )]
        public bool Collapsed
        {
            get { return this.collapsed; }
            set
            {
                if( this.collapsed != value )
                {
                    this.collapsed = value;
                    this.OnCollapsedChanged();
                }
            }
        }
        private bool collapsed = false;

        [Category( "Appearance" )]
        [Description( "Indicates whether the toolbox is allowed to collapse." )]
        [DefaultValue( false )]
        public bool AllowCollapse
        {
            get { return this.allowCollapse; }
            set
            {
                if( this.allowCollapse != value )
                {
                    this.allowCollapse = value;
                    this.OnAllowCollapseChanged();
                }
            }
        }
        private bool allowCollapse = false;

        private int TitlebarHeight
        {
            get { return SystemInformation.ToolWindowCaptionHeight; }
        }

        protected override Size DefaultSize
        {
            get { return new Size( 100, 100 ); }
        }

        protected override Padding DefaultPadding
        {
            get { return new Padding( 3 ); }
        }

        #endregion Properties

        #region Collapse / Expand
        
        protected override void OnClick(EventArgs e)
        {
            this.Focus();
            base.OnClick(e);
        }

        protected virtual void OnCollapsedChanged()
        {
            if( this.CollapsedChanged != null )
                this.CollapsedChanged( this, EventArgs.Empty );

            this.CollapseExpand();
        }

        protected virtual void OnAllowCollapseChanged()
        {
            if( this.AllowCollapseChanged != null )
                this.AllowCollapseChanged( this, EventArgs.Empty );

            this.CollapseExpand();
        }

        private void CollapseExpand()
        {
            bool collapsed = this.collapsed;
            if( !this.allowCollapse )
                collapsed = false;

            if( collapsed )
                this.Height = this.TitlebarHeight;
            else
                this.Height = this.ExpandedHeight;

            this.Invalidate();
        }

        private int ExpandedHeight
        {
            get
            {
                int height = this.TitlebarHeight + 3;
                if( this.Controls.Count > 0 )
                {
                    height = 0;
                    int top = Int32.MaxValue;
                    foreach( Control c in this.Controls )
                    {
                        if( height < c.Bottom )
                            height = c.Bottom;
                        if( top > c.Top )
                            top = c.Top;
                    }
                    height += top - this.TitlebarHeight;
                }
                return height;
            }
        }

        private Rectangle CollapseBox
        {
            get
            {
                Rectangle rect = new Rectangle();
                int height = this.TitlebarHeight;

                if( height <= 17 )
                {
                    rect.Size = new Size( height, height );
                    rect.Location = new Point( this.Width - height, 0 );
                }
                else
                {
                    int y = (height - 17) / 2;
                    rect.Size = new Size( 17, 17 );
                    rect.Location = new Point( this.Width - 17 - y, y );
                }
                return rect;
            }
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            if( this.allowCollapse )
            {
                if( this.CollapseBox.Contains( e.Location ) )
                    this.Hover = true;
                else
                    this.Hover = false;
            }
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            if( this.allowCollapse )
            {
                if( this.hover )
                    this.Hover = false;
            }
        }

        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );
            if( this.allowCollapse )
            {
                if( this.CollapseBox.Contains( e.Location ) )
                    this.Pressed = true;
            }
        }

        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );
            if( this.allowCollapse )
            {
                if( this.CollapseBox.Contains( e.Location ) )
                {
                    this.Pressed = false;
                    this.Collapsed = !this.Collapsed;
                }
            }
        }

        protected override void OnMouseDoubleClick( MouseEventArgs e )
        {
            base.OnMouseDoubleClick( e );
            if( this.allowCollapse )
            {
                Rectangle rect = new Rectangle( 0, 0, this.Width, this.TitlebarHeight );
                if( rect.Contains( e.Location ) && !this.CollapseBox.Contains( e.Location ) )
                {
                    this.Collapsed = !this.Collapsed;
                }
            }
        }

        private bool Hover
        {
            get { return this.hover; }
            set
            {
                if( this.hover != value )
                {
                    this.hover = value;
                    this.SetCursor();
                    this.Refresh();
                }
            }
        }
        private bool hover = false;

        private bool Pressed
        {
            get { return this.pressed; }
            set
            {
                this.pressed = value;
                this.Refresh();
            }
        }
        private bool pressed = false;

        private void SetCursor()
        {
            if( this.hover )
                this.Cursor = Cursors.Hand;
            else
                this.Cursor = Cursors.Default;
        }

        #endregion Collapse / Expand

        #region Painting

        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );

            Rectangle bounds = base.DisplayRectangle;
            bounds.Height = this.TitlebarHeight;
            this.OnPaintTitlebar( new PaintTitlebarEventArgs( e.Graphics, e.ClipRectangle, bounds, this.Text, this.collapsed ) );

            if( this.allowCollapse )
            {
                this.OnPaintCollapseButton( new PaintCollapseButtonEventArgs( e.Graphics, e.ClipRectangle, this.CollapseBox, this.collapsed, this.pressed, this.hover ) );
            }
            if( !this.allowCollapse || !this.collapsed )
            {
                bounds.Offset( 0, this.TitlebarHeight );
                bounds.Height = base.DisplayRectangle.Height - this.TitlebarHeight;
                this.OnPaintBackground( new PaintBackgroundEventArgs( e.Graphics, e.ClipRectangle, bounds ) );
            }
        }

        protected virtual void OnPaintTitlebar( PaintTitlebarEventArgs e )
        {
            if( this.PaintTitlebar != null )
                this.PaintTitlebar( this, e );

            e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            Rectangle rect = e.Bounds;

            // Draw titlebar background.
            Brush brush = null;
            RectangleCorners corners = RectangleCorners.None;
            switch( this.renderingMode )
            {
                case ToolBoxRenderingMode.Styled:
                case ToolBoxRenderingMode.StyledBox:
                    if( collapsed )
                        corners = RectangleCorners.All;
                    else
                        corners = RectangleCorners.Top;
                    rect.Height -= 1;
                    rect.Width -= 1;
                    brush = GradientBrush.Create( rect, this.titlebarBackColor, this.titlebarGradientColor, this.gradientMode );
                    break;
                case ToolBoxRenderingMode.Gradient:
                    brush = GradientBrush.Create( rect, this.titlebarBackColor, this.titlebarGradientColor, this.gradientMode );
                    break;
                case ToolBoxRenderingMode.Simple:
                    brush = new SolidBrush( this.titlebarBackColor );
                    break;
            }
            
            Pen pen = new Pen( this.titlebarBorderColor );

            if( corners == RectangleCorners.None )
            {
                e.Graphics.FillRectangle( brush, rect );
                if( this.titlebarBorder )
                {
                    rect.Height -= 1;
                    rect.Width -= 1;
                    e.Graphics.DrawRectangle( pen, rect );
                }
            }
            else
            {
                using( GraphicsPath path = RoundedRectangle.Create( rect, 3, corners ) )
                {
                    e.Graphics.FillPath( brush, path );
                    e.Graphics.DrawPath( pen, path );
                }
            }

            pen.Dispose();
            brush.Dispose();

            // Draw titlebar text
            if( !String.IsNullOrEmpty( e.Text ) )
            {
                TextFormatFlags tff = TextFormatFlags.VerticalCenter | TextFormatFlags.LeftAndRightPadding;
                switch( this.textAlign )
                {
                    case HorizontalAlignment.Left:
                        tff |= TextFormatFlags.Left;
                        break;
                        
                    case HorizontalAlignment.Right:
                        tff |= TextFormatFlags.Right;
                        break;
                        
                    case HorizontalAlignment.Center:
                        tff |= TextFormatFlags.HorizontalCenter;
                        break;
                }
                TextRenderer.DrawText( e.Graphics, e.Text, this.Font, e.Bounds, this.titlebarForeColor, tff );
            }
        }

        protected void OnPaintCollapseButton( PaintCollapseButtonEventArgs e )
        {
            if( this.PaintCollapseButton != null )
                this.PaintCollapseButton( this, e );

            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            Rectangle rect = e.Bounds;
            rect.Inflate( -1, -1 );

            if( this.renderingMode == ToolBoxRenderingMode.Styled )
            {
                rect.Height -= 1;
                rect.Width -= 1;

                Color color = this.titlebarBackColor;
                if( pressed )
                {
                    color = this.titlebarGradientColor;
                }

                if( hover )
                {
                    using( GraphicsPath path = RoundedRectangle.Create( rect, 3 ) )
                    {
                        using( SolidBrush brush = new SolidBrush( color ) )
                        {
                            e.Graphics.FillPath( brush, path );
                        }
                        using( Pen pen = new Pen( this.titlebarBorderColor ) )
                        {
                            e.Graphics.DrawPath( pen, path );
                        }
                    }
                }

                using( Pen pen = new Pen( this.titlebarForeColor, 1.6F ) )
                {
                    rect.Inflate( -3, -3 );
                    int middle = rect.Top + rect.Height / 2;
                    Point[] top, bottom;
                    if( collapsed )
                    {
                        top = new Point[] {
                            new Point( rect.Left, rect.Top ),
                            new Point( rect.Left + rect.Width / 2, middle ),
                            new Point( rect.Right, rect.Top )
                        };
                        bottom = new Point[] {
                            new Point( rect.Left, middle ),
                            new Point( rect.Left + rect.Width / 2, rect.Bottom ),
                            new Point( rect.Right, middle )
                        };
                    }
                    else
                    {
                        top = new Point[] {
                            new Point( rect.Left, rect.Bottom ),
                            new Point( rect.Left + rect.Width / 2, middle ),
                            new Point( rect.Right, rect.Bottom )
                        };
                        bottom = new Point[] {
                            new Point( rect.Left, middle ),
                            new Point( rect.Left + rect.Width / 2, rect.Top ),
                            new Point( rect.Right, middle )
                        };
                    }
                    e.Graphics.DrawLines( pen, top );
                    e.Graphics.DrawLines( pen, bottom );
                }
            }
            else
            {
                if( Application.RenderWithVisualStyles )
                {
                    VisualStyleRenderer renderer;
                    if( pressed )
                        renderer = new VisualStyleRenderer( VisualStyleElement.Button.PushButton.Pressed );
                    else if( hover )
                        renderer = new VisualStyleRenderer( VisualStyleElement.Button.PushButton.Hot );
                    else
                        renderer = new VisualStyleRenderer( VisualStyleElement.Button.PushButton.Normal );
                    renderer.DrawBackground( e.Graphics, rect );
                }
                else
                {
                    rect.Inflate( -1, -1 );
                    if( pressed )
                        ControlPaint.DrawButton( e.Graphics, rect, ButtonState.Pushed );
                    else if( hover )
                        ControlPaint.DrawButton( e.Graphics, rect, ButtonState.Normal );
                    else
                        ControlPaint.DrawButton( e.Graphics, rect, ButtonState.Flat );
                }

                using( Pen pen = new Pen( SystemColors.ControlText, 1.25F ) )
                {
                    rect.Inflate( -4, -4 );
                    rect.Height -= 1;
                    rect.Width -= 1;
                    if( collapsed )
                    {
                        int hMiddle = rect.Left + rect.Width / 2;
                        e.Graphics.DrawLine( pen, hMiddle, rect.Top, hMiddle, rect.Bottom );
                    }
                    int vMiddle = rect.Top + rect.Height / 2;
                    e.Graphics.DrawLine( pen, rect.Left, vMiddle, rect.Right, vMiddle );
                }
            }
        }

        protected virtual void OnPaintBackground( PaintBackgroundEventArgs e )
        {
            if( this.PaintBackground != null )
                this.PaintBackground( this, e );

            Rectangle rect = e.Bounds;
            switch( this.renderingMode )
            {
                case ToolBoxRenderingMode.Styled:
                    rect.Width -= 1;
                    rect.Height -= 1;

                    using( GraphicsPath path = RoundedRectangle.Create( rect, 2, RectangleCorners.Bottom ) )
                    {
                        using( Brush brush = GradientBrush.Create( rect, this.contentBackColor, this.gradientColor, this.gradientMode ) )
                        {
                            e.Graphics.FillPath( brush, path );
                        }
                        using( Pen pen = new Pen( this.gradientColor ) )
                        {
                            e.Graphics.DrawPath( pen, path );
                        }
                    }
                    break;

                case ToolBoxRenderingMode.StyledBox:
                    rect.Width -= 1;
                    rect.Height -= 1;

                    using( GraphicsPath path = RoundedRectangle.Create( rect, 3, RectangleCorners.Bottom ) )
                    {
                        using( Brush brush = new SolidBrush( this.BackColor ) )
                        {
                            e.Graphics.FillPath( brush, path );
                        }
                        using( Pen pen = new Pen( this.titlebarBorderColor ) )
                        {
                            e.Graphics.DrawPath( pen, path );
                        }
                    }
                    break;

                default:
                    rect.Height -= 1;
                    rect.Offset( 0, 1 );
                    switch( this.borderStyle )
                    {
                        case BorderStyle.None:
                            break;

                        case BorderStyle.FixedSingle:
                            ControlPaint.DrawBorder( e.Graphics, rect, this.titlebarBorderColor, ButtonBorderStyle.Solid );
                            break;

                        case BorderStyle.Fixed3D:
                            ControlPaint.DrawBorder3D( e.Graphics, rect, Border3DStyle.Sunken );
                            break;
                    }
                    break;
            }
        }

        #endregion Painting

        #region EventArgs

        public class PaintTitlebarEventArgs : PaintBackgroundEventArgs
        {
            public PaintTitlebarEventArgs( Graphics g, Rectangle clipRect, Rectangle bounds, string text, bool collapsed )
                : base( g, clipRect, bounds )
            {
                this.text = text;
                this.collapsed = collapsed;
            }

            private string text;
            public string Text
            {
                get { return this.text; }
            }

            private bool collapsed;
            public bool Collapsed
            {
                get { return this.collapsed; }
            }
        }

        public class PaintCollapseButtonEventArgs : PaintBackgroundEventArgs
        {
            public PaintCollapseButtonEventArgs( Graphics g, Rectangle clipRect, Rectangle bounds, bool collapsed, bool hover, bool pressed )
                : base( g, clipRect, bounds )
            {
                this.collapsed = collapsed;
                this.hover = hover;
                this.pressed = pressed;
            }

            private bool collapsed;
            public bool Collapsed
            {
                get { return this.collapsed; }
            }

            private bool hover;
            public bool Hover
            {
                get { return this.hover; }
            }

            private bool pressed;
            public bool Pressed
            {
                get { return this.pressed; }
            }
        }

        public class PaintBackgroundEventArgs : PaintEventArgs
        {
            public PaintBackgroundEventArgs( Graphics g, Rectangle clipRect, Rectangle bounds )
                : base( g, clipRect )
            {
                this.bounds = bounds;
            }

            private Rectangle bounds;
            public Rectangle Bounds
            {
                get { return this.bounds; }
            }
        }

        #endregion EventArgs
    }
}
