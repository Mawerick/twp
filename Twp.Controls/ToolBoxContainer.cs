﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace Twp.Controls
{
    [ToolboxItem(typeof( Design.ToolBoxContainerToolboxItem ))]
    [Designer( typeof( Design.ToolBoxContainerDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
    public class ToolBoxContainer : Control
    {
        public ToolBoxContainer()
        {
            this.SetStyle( ControlStyles.ResizeRedraw |
                          ControlStyles.ContainerControl  |
                          ControlStyles.OptimizedDoubleBuffer |
                          ControlStyles.SupportsTransparentBackColor, true );
            this.BackColor = Color.Transparent;
            this.LayoutEngine.InitLayout( this.Controls, BoundsSpecified.All );
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        [Category( "Appearance" )]
        [Description( "The background color of the component." )]
        [DefaultValue( typeof( Color ), "Transparent" )]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value;  }
        }

        [Category( "Appearance" )]
        [Description( "The padding of the container area." )]
        [DefaultValue( typeof( Padding ), "2, 2, 2, 2" )]
        public new Padding Padding
        {
            get { return this.padding; }
            set
            {
                if( this.padding != value )
                {
                    this.padding = value;
                    this.Invalidate();
                }
            }
        }
        private Padding padding = new Padding( 2, 2, 2, 2 );

        public override Rectangle DisplayRectangle
        {
            get
            {
                Rectangle paddedRect = base.DisplayRectangle;
                paddedRect.Offset( this.padding.Left, this.padding.Top );
                paddedRect.Width -= this.padding.Horizontal;
                paddedRect.Height -= this.padding.Vertical;
                return paddedRect;
            }
        }

        [Category( "Behavior" )]
        [Description( "" )]
        [DefaultValue( true )]
        public bool FillLast
        {
            get { return this.fillLast; }
            set
            {
                if( this.fillLast != value )
                {
                    this.fillLast = value;
                    this.PerformLayout();
                }
            }
        }
        private bool fillLast = true;

        public override LayoutEngine LayoutEngine
        {
            get
            {
                if( this.layoutEngine == null )
                    this.layoutEngine = new ToolBoxLayout();
                return this.layoutEngine;
            }
        }
        private ToolBoxLayout layoutEngine;

        protected override void InitLayout()
        {
            base.InitLayout();
            this.ResumeLayout( true );
        }

        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.PerformLayout();
        }

        #region Splitter support

        Rectangle activeSplitter;
        Point lastPoint;
        bool isDrawn = false;

        bool mouseDown = false;
        Rectangle startSplitter = Rectangle.Empty;

        [Category( "Behavior" )]
        [Description( "Defines whether or not the user will be allowed to resize the toolboxes in this container." )]
        [DefaultValue( true )]
        public bool AllowResize
        {
            get { return this.allowResize; }
            set
            {
                if( this.allowResize != value )
                {
                    this.allowResize = value;
                }
            }
        }
        private bool allowResize = true;

        [Category( "Behavior" )]
        [Description( "The width of the splitter area." )]
        [DefaultValue( 2 )]
        public int SplitterWidth
        {
            get { return this.splitterWidth; }
            set
            {
                if( this.splitterWidth != value )
                {
                    this.splitterWidth = value;
                    this.Invalidate();
                }
            }
        }
        protected int splitterWidth = 2;

        protected override void OnMouseDown( MouseEventArgs e )
        {
            base.OnMouseDown( e );
            if( !this.allowResize )
                return;

            this.SuspendLayout();
            if( e.Button == System.Windows.Forms.MouseButtons.Left )
            {
                if( this.DisplayRectangle.Contains( e.Location ) )
                {
                    this.mouseDown = true;
                    Size size = new Size( this.Width - this.padding.Horizontal, 2 );
                    Point pt = new Point( this.Location.X, e.Y );
                    this.startSplitter = new Rectangle( this.PointToScreen( pt ), size );
                    this.activeSplitter = this.startSplitter;
                    this.DrawMoveIndicator( this.startSplitter, true );
                }
            }
            this.ResumeLayout();
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );
            if( !this.allowResize )
                return;

            if( this.mouseDown )
            {
                if( e.Location == this.lastPoint )
                    return;
                this.lastPoint = e.Location;

                Twp.Utilities.Log.Debug( "[TBC.MouseMove] loc: {0}", e.Location );
                this.DrawMoveIndicator( this.activeSplitter, false );
                Point pt = new Point( this.Location.X, e.Y );
                if( this.ClientRectangle.Contains( pt ) )
                {
                    this.activeSplitter.Location = this.PointToScreen( pt );
                    this.DrawMoveIndicator( this.activeSplitter, true );
                }
            }
            else
            {
                if( this.DisplayRectangle.Contains( e.Location ) )
                    this.Cursor = Cursors.HSplit;
            }
        }

        protected override void OnMouseUp( MouseEventArgs e )
        {
            base.OnMouseUp( e );
            if( !this.allowResize )
                return;

            Twp.Utilities.Log.Debug( "[TBC.MouseUp] calling DrawMove" );
            this.DrawMoveIndicator( this.startSplitter, false );
            this.DrawMoveIndicator( this.activeSplitter, false );

            this.Invalidate( this.startSplitter );
            this.Invalidate( this.activeSplitter );

            int height = this.activeSplitter.Y - this.startSplitter.Y;
            Twp.Utilities.Log.Debug( "Height: {0}", height );

            Point pt = this.PointToClient( this.startSplitter.Location );
            pt.X += this.padding.Left;
            pt.Y -= this.splitterWidth + this.padding.Vertical;
            Twp.Utilities.Log.Debug( "Point: {0}", pt );
            Control control = this.GetChildAtPoint( pt );
            Twp.Utilities.Log.Debug( "Control above: {0} ({1})", control, (control != null ) ? control.Name : "null" );
            if( control != null )
            {
                Twp.Utilities.Log.Debug( "Control area: {0}", control.Bounds );
                Twp.Utilities.Log.Debug( "Height before: {0}", control.Height );
                control.Height += height;
                Twp.Utilities.Log.Debug( "Height after: {0}", control.Height );
            }

            pt = this.PointToClient( this.startSplitter.Location );
            pt.X += this.padding.Left;
            pt.Y += this.splitterWidth + height + this.padding.Vertical;
            Twp.Utilities.Log.Debug( "Point: {0}", pt );
            control = this.GetChildAtPoint( pt );
            Twp.Utilities.Log.Debug( "Control below: {0} ({1})", control, ( control != null ) ? control.Name : "null" );
            if( control != null )
            {
                Twp.Utilities.Log.Debug( "Control area: {0}", control.Bounds );
                Twp.Utilities.Log.Debug( "Height before: {0}", control.Height );
                control.Height -= height;
                Twp.Utilities.Log.Debug( "Height after: {0}", control.Height );
            }

            this.mouseDown = false;
            this.startSplitter = Rectangle.Empty;
            this.activeSplitter = Rectangle.Empty;
            this.ResumeLayout( true );
        }

        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            if( !this.allowResize )
                return;

            if( !this.mouseDown )
                this.Cursor = Cursors.Arrow;
        }

        private void DrawMoveIndicator( Rectangle rect, bool draw )
        {
            if( !this.allowResize )
                return;

            if( this.isDrawn == draw && rect == this.activeSplitter )
                return;
            this.isDrawn = draw;

            Twp.Utilities.Log.Debug( "[TBC.DrawMove] R:{0} Draw:{1}", rect, draw );

            ControlPaint.FillReversibleRectangle( rect, Color.Black );
        }

        #endregion
    }

    internal class ToolBoxLayout : LayoutEngine
    {
        public override bool Layout( object container, LayoutEventArgs layoutEventArgs )
        {
            ToolBoxContainer parent = container as ToolBoxContainer;
            if( parent == null )
            {
                base.Layout( container, layoutEventArgs );
            }
            else
            {
                Rectangle parentRect = parent.DisplayRectangle;
                Point nextLocation = parentRect.Location;
                //Padding padding = parent.Padding;
                ToolBox last = null;
                foreach( ToolBox toolBox in parent.Controls )
                {
                    if( !toolBox.Visible )
                        continue;

                    //nextLocation.Offset( padding.Left, padding.Top );
                    toolBox.Location = nextLocation;
                    toolBox.Width = parentRect.Width; // - padding.Horizontal;

                    if( !toolBox.Collapsed )
                        if( toolBox.Bottom >= parentRect.Height /*- padding.Bottom*/ )
                            toolBox.Height -= ( toolBox.Bottom - parentRect.Height /*+ padding.Bottom*/ );
                    nextLocation.X = parentRect.X;
                    nextLocation.Y += toolBox.Height + parent.SplitterWidth /*+ parent.Padding.Bottom*/;
                    last = toolBox;
                }
                if( last != null && parent.FillLast )
                {
                    int index = parent.Controls.GetChildIndex( last );
                    int offset = parent.Padding.Bottom;
                    for( int n = index; n >= 0; n-- )
                    {
                        last = parent.Controls[n] as ToolBox;
                        if( last.Collapsed )
                        {
                            offset += last.Height + parent.SplitterWidth /*+ padding.Vertical*/;
                            continue;
                        }

                        if( last.Bottom < parentRect.Height - offset )
                        {
                            last.Height += ( parentRect.Height - last.Bottom - offset );
                        }
                        break;
                    }
                }
            }
            return false;
        }
    }
}
