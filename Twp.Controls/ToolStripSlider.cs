﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls
{
    /// <summary>
    /// Description of ToolStripSlider.
    /// </summary>
    [DefaultEvent( "ValueChanged" )]
    [DefaultProperty( "Value" )]
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.All )]
    public class ToolStripSlider : ToolStripControlHost
    {
        public ToolStripSlider() : base( new Slider() )
        {
            this.Control.Width = 100;
        }
        
        #region Properties

        [Browsable( false )]
        public Slider SliderControl
        {
            get
            {
                return this.Control as Slider;
            }
        }
        
        [Category( "Behavior" )]
        [Description( "The value for the position of the slider" )]
        [DefaultValue( 0 )]
        public decimal Value
        {
            get { return this.SliderControl.Value; }
            set { this.SliderControl.Value = value; }
        }

        [Category( "Appearance" )]
        [Description( "The number of decimal places used on the value label" )]
        [DefaultValue( 0 )]
        public int DecimalPlaces
        {
            get { return this.SliderControl.DecimalPlaces; }
            set { this.SliderControl.DecimalPlaces = value; }
        }

        [Category( "Behavior" )]
        [Description( "The minimum value for the position of the slider" )]
        [DefaultValue( 0 )]
        public decimal Minimum
        {
            get { return this.SliderControl.Minimum; }
            set { this.SliderControl.Minimum = value; }
        }

        [Category( "Behavior" )]
        [Description( "The maximum value for the position of the slider" )]
        [DefaultValue( 100 )]
        public decimal Maximum
        {
            get { return this.SliderControl.Maximum; }
            set { this.SliderControl.Maximum = value; }
        }

        [Category( "Behavior" )]
        [Description( "The number of positions the slider moves in response to keyboard input (arrow keys)" )]
        [DefaultValue( 1 )]
        public decimal Step
        {
            get { return this.SliderControl.Step; }
            set { this.SliderControl.Step = value; }
        }

        [Category( "Behavior" )]
        [Description( "The number of positions the slider moves in response to mouse clicks or the PAGE UP and PAGE DOWN keys" )]
        [DefaultValue( 10 )]
        public decimal PageStep
        {
            get { return this.SliderControl.PageStep; }
            set { this.SliderControl.PageStep = value; }
        }

        [Category( "Appearance" )]
        [Description( "The frequency at which to place ticks by the slider." )]
        [DefaultValue( 5 )]
        public decimal TickFrequency
        {
            get { return this.SliderControl.TickFrequency; }
            set { this.SliderControl.TickFrequency = value; }
        }

        [Category( "Appearance" )]
        [Description( "The amount of space between elements in the slider" )]
        [DefaultValue( 4 )]
        public int Spacing
        {
            get { return this.SliderControl.Spacing; }
            set { this.SliderControl.Spacing = value; }
        }

        [Category( "Appearance" )]
        [Description( "Determins where the label displaying the value will be positioned" )]
        [DefaultValue( LabelPosition.SliderBeforeLabel )]
        public LabelPosition LabelPosition
        {
            get { return this.SliderControl.LabelPosition; }
            set { this.SliderControl.LabelPosition = value; }
        }

        [Category( "Appearance" )]
        [Description( "Determins whether or not the minimum/maximum buttons are shown." )]
        [DefaultValue( true )]
        public bool ShowButtons
        {
            get { return this.SliderControl.ShowButtons; }
            set { this.SliderControl.ShowButtons = value; }
        }

        #endregion
        
        #region Events

        public event EventHandler ValueChanged
        {
            add { this.SliderControl.ValueChanged += value; }
            remove { this.SliderControl.ValueChanged -= value; }
        }

        public event EventHandler Scroll
        {
            add { this.SliderControl.Scroll += value; }
            remove { this.SliderControl.Scroll -= value; }
        }

        #endregion
        
        #region Methods
        
        protected override void OnSubscribeControlEvents( Control control )
        {
            base.OnSubscribeControlEvents( control );
        }
        
        protected override void OnUnsubscribeControlEvents( Control control )
        {
            base.OnUnsubscribeControlEvents( control );
        }
        
        #endregion
    }
}
