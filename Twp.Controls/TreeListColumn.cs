﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Diagnostics;

using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using Twp.Utilities;

namespace Twp.Controls
{
    public class HitInfo
    {
        [Flags]
        public enum HitTypes
        {
            ColumnHeader = 0x0001,
            ColumnHeaderResize = 0x0002,
        }

        public HitTypes HitType = 0;
        public TreeListColumn Column = null;
    }

    [DesignTimeVisible(false)]
    [TypeConverter(typeof(Design.ColumnConverter))]
    public class TreeListColumn
    {
        #region Constructor

        public TreeListColumn(string fieldName)
            : this(fieldName, fieldName)
        {
        }

        public TreeListColumn(string fieldName, int width)
            : this(fieldName, fieldName, width)
        {
        }

        public TreeListColumn(string fieldName, bool autoSize)
            : this(fieldName, fieldName, autoSize)
        {
        }

        public TreeListColumn(string fieldName, string caption)
            : this(fieldName, caption, 50)
        {
        }

        public TreeListColumn(string fieldName, string caption, int width)
        {
            this.fieldName = fieldName;
            this.caption = caption;
            this.width = width;
            this.ResetHeaderFont();
            this.ResetCellFont();
        }

        public TreeListColumn(string fieldName, string caption, bool autoSize)
        {
            this.fieldName = fieldName;
            this.caption = caption;
            this.autoSize = autoSize;
            this.ResetHeaderFont();
            this.ResetCellFont();
        }

        #endregion

        #region Private Fields

        private ContentAlignment headerAlignment = ContentAlignment.MiddleLeft;
        private Font headerFont = SystemFonts.DefaultFont;
        private Color headerForeColor = SystemColors.ControlText;
        private Color headerBackColor = Color.Transparent;
        private Padding headerPadding = new Padding(5, 0, 5, 0);

        private ContentAlignment cellAlignment = ContentAlignment.MiddleLeft;
        private Font cellFont = SystemFonts.DefaultFont;
        private Color cellForeColor = SystemColors.ControlText;
        private Color cellBackColor = Color.Transparent;
        private Padding cellPadding = new Padding(0, 0, 0, 0);

        private int width = 50;
        private bool autoSize = false;
        private float autoSizeRatio = 100;
        private int autoSizeMinSize = 50;
        private string caption = String.Empty;
        private string fieldName = String.Empty;

        private TreeListColumnCollection owner = null;
        private Rectangle calculatedRect;
        private int calculatedAutoSize;
        private int visibleIndex = -1;
        private int columnIndex = -1;
        private bool isHot = false;

        private bool sortAllowed = true;
        private SortOrder sortOrder = SortOrder.None;

        #endregion

        #region Internal Properties

        [Browsable(false)]
        internal TreeListColumnCollection Owner
        {
            get { return this.owner; }
            set { this.owner = value; }
        }

        [Browsable(false)]
        internal TextFormatFlags HeaderFormatFlags
        {
            get { return GetFormatting(this.headerAlignment); }
        }

        [Browsable(false)]
        internal TextFormatFlags CellFormatFlags
        {
            get { return GetFormatting(this.cellAlignment); }
        }

        [Browsable(false)]
        internal Rectangle InternalCalculatedRect
        {
            get { return this.calculatedRect; }
            set { this.calculatedRect = value; }
        }

        [Browsable(false)]
        internal int InternalVisibleIndex
        {
            get { return this.visibleIndex; }
            set { this.visibleIndex = value; }
        }

        [Browsable(false)]
        internal int InternalIndex
        {
            get { return this.columnIndex; }
            set { this.columnIndex = value; }
        }

        [Browsable(false)]
        internal int CalculatedAutoSize
        {
            get { return this.calculatedAutoSize; }
            set { this.calculatedAutoSize = value; }
        }

        #endregion

        #region Properties

        [Category("Appearance")]
        [DisplayName("TextAlignment")]
        [DefaultValue(ContentAlignment.MiddleLeft)]
        public ContentAlignment HeaderAlignment
        {
            get { return this.headerAlignment; }
            set { this.headerAlignment = value; }
        }

        [Category("Appearance")]
        [DisplayName("Font")]
        public Font HeaderFont
        {
            get { return this.headerFont; }
            set
            {
                if (value == null)
                    this.ResetCellFont();
                else
                    this.headerFont = value;
            }
        }

        [Category("Appearance")]
        [DisplayName("ForeColor")]
        [DefaultValue(typeof(Color), "ControlText")]
        public Color HeaderForeColor
        {
            get { return this.headerForeColor; }
            set { this.headerForeColor = value; }
        }

        [Category("Appearance")]
        [DisplayName("BackColor")]
        [DefaultValue(typeof(Color), "Transparent")]
        public Color HeaderBackColor
        {
            get { return this.headerBackColor; }
            set { this.headerBackColor = value; }
        }

        [Category("Layout")]
        [DisplayName("Header Padding")]
        [DefaultValue(typeof(Padding), "5, 0, 5, 0")]
        public Padding HeaderPadding
        {
            get { return this.headerPadding; }
            set { this.headerPadding = value; }
        }

        [Category("Cell Appearance")]
        [DisplayName("TextAlignment")]
        [DefaultValue(ContentAlignment.MiddleLeft)]
        public ContentAlignment CellAlignment
        {
            get { return this.cellAlignment; }
            set { this.cellAlignment = value; }
        }

        [Category("Cell Appearance")]
        [DisplayName("Font")]
        public Font CellFont
        {
            get { return this.cellFont; }
            set
            {
                if (value == null)
                    this.ResetHeaderFont();
                else
                    this.cellFont = value;
            }
        }

        [Category("Cell Appearance")]
        [DisplayName("ForeColor")]
        [DefaultValue(typeof(Color), "ControlText")]
        public Color CellForeColor
        {
            get { return this.cellForeColor; }
            set { this.cellForeColor = value; }
        }

        [Category("Cell Appearance")]
        [DisplayName("BackColor")]
        [DefaultValue(typeof(Color), "Transparent")]
        public Color CellBackColor
        {
            get { return this.cellBackColor; }
            set { this.cellBackColor = value; }
        }

        [Category("Layout")]
        [DisplayName("Cell Padding")]
        [DefaultValue(typeof(Padding), "0, 0, 0, 0")]
        public Padding CellPadding
        {
            get { return this.cellPadding; }
            set { this.cellPadding = value; }
        }

        [Category("Layout")]
        [DefaultValue(50)]
        public int Width
        {
            get { return this.width; }
            set
            {
                if (this.width == value)
                    return;
                this.width = value;
                if (this.owner != null && this.owner.DesignMode)
                    this.owner.RecalcVisibleColumnsRect();
            }
        }

        [Category("Layout")]
        [DefaultValue(false)]
        public bool AutoSize
        {
            get { return this.autoSize; }
            set { this.autoSize = value; }
        }

        [Category("Layout")]
        [DefaultValue(100f)]
        public float AutoSizeRatio
        {
            get { return this.autoSizeRatio; }
            set { this.autoSizeRatio = value; }
        }

        [Category("Layout")]
        [DefaultValue(50)]
        public int AutoSizeMinSize
        {
            get { return this.autoSizeMinSize; }
            set { this.autoSizeMinSize = value; }
        }

        [Category("Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get { return this.caption; }
            set
            {
                this.caption = value;
                if (this.owner != null)
                    this.owner.Invalidate();
            }
        }

        [Category("Design")]
        [DisplayName("(Name)")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FieldName
        {
            get { return this.fieldName; }
            set
            {
                if (this.owner == null || this.owner.DesignMode == false)
                    throw new Exception("Fieldname can only be set at design time. Use Constructor to set programatically.");
                if (value.Length == 0)
                    throw new Exception("Empty fieldname not allowed.");
                if (this.owner[value] != null)
                    throw new Exception("Fieldname must be unique.");
                this.fieldName = value;
            }
        }

        [Category("Behavior")]
        [DefaultValue(true)]
        public bool SortAllowed
        {
            get { return this.sortAllowed; }
            set { this.sortAllowed = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public SortOrder SortOrder
        {
            get { return this.sortOrder; }
            set { this.sortOrder = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Rectangle CalculatedRect
        {
            get { return this.calculatedRect; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TreeListView TreeList
        {
            get
            {
                if (this.owner == null)
                    return null;
                return this.owner.Owner;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int VisibleIndex
        {
            get { return this.visibleIndex; }
            set
            {
                if (this.owner != null)
                    this.owner.SetVisibleIndex(this, value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get { return this.columnIndex; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsHot
        {
            get { return this.isHot; }
            set { this.isHot = value; }
        }

        #endregion

        #region Methods

        public void ResetHeaderFont()
        {
            this.headerFont = SystemFonts.MenuFont;
        }

        public void ResetCellFont()
        {
            this.cellFont = SystemFonts.DefaultFont;
        }

        private bool ShouldSerializeHeaderFont()
        {
            return !this.headerFont.Equals(SystemFonts.MenuFont);
        }

        private bool ShouldSerializeCellFont()
        {
            return !this.cellFont.Equals(SystemFonts.DefaultFont);
        }

        public virtual void Draw(Graphics g, ColumnHeaderRenderer renderer, Rectangle rect)
        {
            renderer.DrawHeader(g, rect, this, this.isHot);
        }

        internal static TextFormatFlags GetFormatting(System.Drawing.ContentAlignment alignment)
        {
            TextFormatFlags flags = 0;
            switch (alignment)
            {
                case System.Drawing.ContentAlignment.TopLeft:
                    flags = TextFormatFlags.Top | TextFormatFlags.Left;
                    break;
                case System.Drawing.ContentAlignment.TopCenter:
                    flags = TextFormatFlags.Top | TextFormatFlags.HorizontalCenter;
                    break;
                case System.Drawing.ContentAlignment.TopRight:
                    flags = TextFormatFlags.Top | TextFormatFlags.Right;
                    break;
                case System.Drawing.ContentAlignment.MiddleLeft:
                    flags = TextFormatFlags.VerticalCenter | TextFormatFlags.Left;
                    break;
                case System.Drawing.ContentAlignment.MiddleCenter:
                    flags = TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter;
                    break;
                case System.Drawing.ContentAlignment.MiddleRight:
                    flags = TextFormatFlags.VerticalCenter | TextFormatFlags.Right;
                    break;
                case System.Drawing.ContentAlignment.BottomLeft:
                    flags = TextFormatFlags.Bottom | TextFormatFlags.Left;
                    break;
                case System.Drawing.ContentAlignment.BottomCenter:
                    flags = TextFormatFlags.Bottom | TextFormatFlags.HorizontalCenter;
                    break;
                case System.Drawing.ContentAlignment.BottomRight:
                    flags = TextFormatFlags.Bottom | TextFormatFlags.Right;
                    break;
            }
            return flags;
        }

        #endregion
    }

    [Description("This is the columns collection")]
    [Editor(typeof(Design.ColumnCollectionEditor), typeof(System.Drawing.Design.UITypeEditor))]
    public class TreeListColumnCollection : IList<TreeListColumn>, IList
    {
        #region Constructor

        public TreeListColumnCollection(TreeListView owner)
        {
            this.owner = owner;
        }

        #endregion

        #region Private Fields

        private ColumnHeaderRenderer renderer = new ColumnHeaderRenderer();
        private TreeListView owner;
        private List<TreeListColumn> columns = new List<TreeListColumn>();
        private List<TreeListColumn> visibleColumns = new List<TreeListColumn>();
        private Dictionary<string, TreeListColumn> columnsMap = new Dictionary<string, TreeListColumn>();

        private bool isInitializing = false;

        #endregion

        #region Properties

        [Browsable(false)]
        public ColumnHeaderRenderer Renderer
        {
            get { return this.renderer; }
        }

        [Browsable(false)]
        public TreeListView Owner
        {
            get { return this.owner; }
        }

        [Browsable(false)]
        public TreeListColumn[] VisibleColumns
        {
            get { return this.visibleColumns.ToArray(); }
        }

        [Browsable(false)]
        public int ColumnsWidth
        {
            get
            {
                int width = 0;
                foreach (TreeListColumn col in this.visibleColumns)
                {
                    if (col.AutoSize)
                        width += col.CalculatedAutoSize;
                    else
                        width += col.Width;
                }
                return width;
            }
        }

        public TreeListColumn this[int index]
        {
            get { return this.columns[index]; }
            set { this.columns[index] = value; }
        }

        public TreeListColumn this[string fieldName]
        {
            get
            {
                TreeListColumn col;
                this.columnsMap.TryGetValue(fieldName, out col);
                return col;
            }
        }

        public int Count
        {
            get { return this.columns.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        internal bool DesignMode
        {
            get
            {
                if (this.owner != null)
                    return this.owner.DesignMode;
                return false;
            }
        }

        #endregion

        #region Public Methods

        public void SetVisibleIndex(TreeListColumn col, int index)
        {
            this.visibleColumns.Remove(col);
            if (index >= 0)
            {
                if (index < this.visibleColumns.Count)
                    this.visibleColumns.Insert(index, col);
                else
                    this.visibleColumns.Add(col);
            }
            this.RecalcVisibleColumnsRect();
        }

        public HitInfo CalcHitInfo(Point point, int horzOffset)
        {
            HitInfo info = new HitInfo();
            info.Column = this.CalcHitColumn(point, horzOffset);
            if (info.Column != null && point.Y < this.owner.HeaderHeight)
            {
                info.HitType |= HitInfo.HitTypes.ColumnHeader;
                int right = info.Column.CalculatedRect.Right - horzOffset;
                if (info.Column.AutoSize == false)
                {
                    if (point.X >= right - 4 && point.X <= right)
                        info.HitType |= HitInfo.HitTypes.ColumnHeaderResize;
                }
            }
            return info;
        }

        public TreeListColumn CalcHitColumn(Point point, int horzOffset)
        {
            if (point.X < this.owner.LeftMargin)
                return null;
            foreach (TreeListColumn col in this.visibleColumns)
            {
                int left = col.CalculatedRect.Left - horzOffset;
                int right = col.CalculatedRect.Right - horzOffset;
                if (point.X >= left && point.X <= right)
                    return col;
            }
            return null;
        }

        public void RecalcVisibleColumnsRect()
        {
            this.RecalcVisibleColumnsRect(false);
        }

        public void RecalcVisibleColumnsRect(bool isDoingColumnResize)
        {
            if (this.isInitializing)
                return;

            int x = 0;
            if (this.owner.ShowRowHeader)
                x = this.owner.RowHeaderWidth;
            int y = 0;
            int height = this.owner.HeaderHeight;
            int index = 0;
            foreach (TreeListColumn col in this.columns)
            {
                col.InternalVisibleIndex = -1;
                col.InternalIndex = index++;
            }

            int widthFixedColumns = 0;
            int widthAutoSizeColumns = 0;
            float totalRatio = 0;
            foreach (TreeListColumn col in this.visibleColumns)
            {
                if (col.AutoSize)
                {
                    widthAutoSizeColumns += col.AutoSizeMinSize;
                    totalRatio += col.AutoSizeRatio;
                }
                else
                    widthFixedColumns += col.Width;
            }

            int clientWidth = this.owner.ClientRectangle.Width - this.owner.InternalRowHeaderWidth;
            float remainingWidth = clientWidth - (widthFixedColumns + widthAutoSizeColumns);
            float ratioUnit = 0;
            if (totalRatio > 0 && remainingWidth > 0)
                ratioUnit = remainingWidth / totalRatio;

            for (index = 0; index < this.visibleColumns.Count; index++)
            {
                TreeListColumn col = this.visibleColumns[index];
                int width = col.Width;
                if (col.AutoSize)
                {
                    if (isDoingColumnResize)
                        width = col.CalculatedAutoSize;
                    else
                        width = col.AutoSizeMinSize + (int)(ratioUnit * col.AutoSizeRatio) - 1; ;
                    col.CalculatedAutoSize = width;
                }
                col.InternalCalculatedRect = new Rectangle(x, y, width, height);
                col.InternalVisibleIndex = index;
                x += width;
            }

            this.Invalidate();
        }

        public void Draw(Graphics g, Rectangle rect, int horzOffset)
        {
            Rectangle colRect = Rectangle.Empty;
            foreach (TreeListColumn col in this.visibleColumns)
            {
                colRect = col.CalculatedRect;
                colRect.X -= horzOffset;
                if (colRect.Left > rect.Right)
                    break;
                col.Draw(g, this.renderer, colRect);
            }
            if (colRect.IsEmpty || colRect.Right < this.owner.ClientRectangle.Right)
            {
                int offset = 0;
                if (!colRect.IsEmpty)
                    offset = colRect.Right;
                colRect = new Rectangle(offset, 0, this.owner.ClientRectangle.Width - offset, this.owner.HeaderHeight);
                this.renderer.DrawHeader(g, colRect);
            }

            if (this.owner.ShowRowHeader)
            {
                Rectangle fillerRect = new Rectangle(0, 0, this.owner.RowHeaderWidth, this.owner.HeaderHeight);
                this.renderer.DrawHeaderFiller(g, fillerRect);
            }
        }

        public void AddRange(IEnumerable<TreeListColumn> columns)
        {
            foreach (TreeListColumn col in columns)
                this.Add(col);
        }

        public void AddRange(TreeListColumn[] columns)
        {
            foreach (TreeListColumn col in columns)
                this.Add(col);
        }

        public void Add(TreeListColumn item)
        {
            bool designMode = this.owner.DesignMode;
            if (!designMode)
            {
                Debug.Assert(this.columnsMap.ContainsKey(item.FieldName) == false);
                Debug.Assert(item.Owner == null, "column.Owner == null");
            }
            else
            {
                this.columns.Remove(item);
                this.visibleColumns.Remove(item);
            }

            item.Owner = this;
            this.columns.Add(item);
            this.visibleColumns.Add(item);
            this.columnsMap[item.FieldName] = item;
            this.RecalcVisibleColumnsRect();
        }

        public void Clear()
        {
            this.columnsMap.Clear();
            this.columns.Clear();
            this.visibleColumns.Clear();
        }

        public bool Contains(TreeListColumn item)
        {
            return this.columns.Contains(item);
        }

        #endregion

        #region Internal Methods

        internal void ToggleSort(TreeListColumn column)
        {
            foreach (TreeListColumn col in this.columns)
            {
                if (col == column && col.SortAllowed)
                {
                    switch (col.SortOrder)
                    {
                        case SortOrder.None:
                        case SortOrder.Descending:
                            col.SortOrder = SortOrder.Ascending;
                            break;
                        case SortOrder.Ascending:
                            col.SortOrder = SortOrder.Descending;
                            break;
                    }
                }
                else
                    col.SortOrder = SortOrder.None;
            }
        }

        internal void Invalidate()
        {
            if (this.owner != null)
                this.owner.Invalidate();
        }

        internal void BeginInit()
        {
            this.isInitializing = true;
        }

        internal void EndInit()
        {
            this.isInitializing = false;
            this.RecalcVisibleColumnsRect();
        }

        #endregion

        #region IList<TreeListColumn> Members

        public int IndexOf(TreeListColumn item)
        {
            return this.columns.IndexOf(item);
        }

        public void Insert(int index, TreeListColumn item)
        {
            this.columns.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            this.columns.RemoveAt(index);
        }

        #endregion

        #region ICollection<TreeListColumn> Members

        public void CopyTo(TreeListColumn[] array, int arrayIndex)
        {
            this.columns.CopyTo(array, arrayIndex);
        }

        public bool Remove(TreeListColumn item)
        {
            return this.columns.Remove(item);
        }

        #endregion

        #region IEnumerable<TreeListColumn> Members

        public IEnumerator<TreeListColumn> GetEnumerator()
        {
            return this.columns.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IList Members

        int IList.Add(object value)
        {
            this.Add((TreeListColumn)value);
            return Count - 1;
        }

        bool IList.Contains(object value)
        {
            return this.Contains((TreeListColumn)value);
        }

        int IList.IndexOf(object value)
        {
            return this.IndexOf((TreeListColumn)value);
        }

        void IList.Insert(int index, object value)
        {
            this.Insert(index, (TreeListColumn)value);
        }

        bool IList.IsFixedSize
        {
            get { return false; }
        }

        void IList.Remove(object value)
        {
            this.Remove((TreeListColumn)value);
        }

        object IList.this[int index]
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        #endregion

        #region ICollection Members

        public void CopyTo(Array array, int index)
        {
            TreeListColumn[] cols = new TreeListColumn[this.columns.Count - index];
            this.columns.CopyTo(cols, index);
            array = (Array)cols;
        }

        public bool IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        public object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }

        int ICollection.Count
        {
            get { return this.columns.Count; }
        }

        #endregion
    }
}
