﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

using Twp.Utilities;

namespace Twp.Controls
{
    public class VisualStyleItemBackground
    {
        [StructLayout( LayoutKind.Sequential )]
        public class RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
            
            public RECT()
            {
            }
            
            public RECT( Rectangle r )
            {
                this.left = r.X;
                this.top = r.Y;
                this.right = r.Right;
                this.bottom = r.Bottom;
            }
        }
        
        [DllImport( "uxtheme.dll", CharSet = CharSet.Auto )]
        public static extern int DrawThemeBackground( IntPtr hTheme, IntPtr hdc, int partId, int stateId, [In] RECT pRect, [In] RECT pClipRect );
        
        [DllImport( "uxtheme.dll", CharSet = CharSet.Auto )]
        public static extern IntPtr OpenThemeData( IntPtr hwnd, [MarshalAs(UnmanagedType.LPWStr)] string pszClassList );
        
        [DllImport("uxtheme.dll", CharSet=CharSet.Auto)]
        public static extern int CloseThemeData( IntPtr hTheme );
        
        enum ITEMSTATES
        {
            LBPSI_HOT = 1,
            LBPSI_HOTSELECTED = 2,
            LBPSI_SELECTED = 3,
            LBPSI_SELECTEDNOTFOCUS = 4,
        };

        enum LISTBOXPARTS
        {
            LBCP_BORDER_HSCROLL = 1,
            LBCP_BORDER_HVSCROLL = 2,
            LBCP_BORDER_NOSCROLL = 3,
            LBCP_BORDER_VSCROLL = 4,
            LBCP_ITEM = 5,
        };

        public enum Style
        {
            Normal,
            Selected,
            Hot,
            Focused, // Selected and Hot
        }
        
        private Style style;

        public VisualStyleItemBackground( Style style )
        {
            this.style = style;
        }

        public void DrawBackground( Control owner, Graphics g, Rectangle rect )
        {
            IntPtr themeHandle = OpenThemeData( owner.Handle, "Explorer" );
            if( themeHandle != IntPtr.Zero )
            {
                DrawThemeBackground( themeHandle, g.GetHdc(), (int)LISTBOXPARTS.LBCP_ITEM, (int)ITEMSTATES.LBPSI_SELECTED, new RECT(rect), new RECT(rect) );
                g.ReleaseHdc();
                CloseThemeData( themeHandle );
                return;
            }

            Color selectedColor;
            switch( this.style )
            {
                case Style.Selected:
                    selectedColor = Color.FromArgb( 150, 220, 250 );
                    break;
                case Style.Focused:
                    selectedColor = Color.FromArgb( 100, 200, 250 );
                    break;
                case Style.Hot:
                    selectedColor = Color.FromArgb( 170, 225, 250 );
                    break;
                default:
                    selectedColor = Color.FromArgb( 217, 217, 217 );
                    break;
            }
            
            SmoothingMode sm = g.SmoothingMode;
            g.SmoothingMode = SmoothingMode.HighQuality;
            if( Extensions.IsOdd( rect.Height ) )
                rect.Height -= 1;
            using( Pen pen = new Pen( selectedColor ) )
            {
                using( GraphicsPath path = RoundedRectangle.Create( rect.X, rect.Y, rect.Width, rect.Height, 2 ) )
                {
                    g.DrawPath( pen, path );

                    rect.Inflate( -1, -1 );
                    using( LinearGradientBrush brush = new LinearGradientBrush( rect, Color.White, Color.FromArgb( 90, selectedColor ), 90 ) )
                    {
                        g.FillRectangle( brush, rect );
                    }
                    g.DrawLine( Pens.White, rect.Left + 1, rect.Top, rect.Right - 1, rect.Top );
                }
            }
            g.SmoothingMode = sm;
        }
    }

    internal class TreeListRenderer
    {
        #region Static Fields

        private static Pen gridLinePen = SystemPens.Control;
        private static VisualStyleRenderer headerRenderer;
        private static VisualStyleRenderer hotHeaderRenderer;
        private static VisualStyleItemBackground itemRenderer;
        private static VisualStyleItemBackground hotItemRenderer;
        private static VisualStyleItemBackground focusedItemRenderer;
        private static VisualStyleItemBackground selectedItemRenderer;
        private static VisualStyleRenderer collapsedRenderer;
        private static VisualStyleRenderer expandedRenderer;

        #endregion
        
        #region Static Properties
        
        internal static Pen GridLinePen
        {
            get { return gridLinePen; }
        }
        
        internal static VisualStyleRenderer HeaderRenderer
        {
            get
            {
                if( headerRenderer == null )
                {
                    VisualStyleElement element = VisualStyleElement.Header.Item.Normal;
                    if( VisualStyleRenderer.IsElementDefined( element ) )
                        headerRenderer = new VisualStyleRenderer( element );
                }
                return headerRenderer;
            }
        }

        internal static VisualStyleRenderer HotHeaderRenderer
        {
            get
            {
                if( hotHeaderRenderer == null )
                {
                    VisualStyleElement element = VisualStyleElement.Header.Item.Hot;
                    if( VisualStyleRenderer.IsElementDefined( element ) )
                        hotHeaderRenderer = new VisualStyleRenderer( element );
                }
                return hotHeaderRenderer;
            }
        }

        internal static VisualStyleItemBackground ItemRenderer
        {
            get
            {
                if( itemRenderer == null )
                    itemRenderer = new VisualStyleItemBackground( VisualStyleItemBackground.Style.Normal );
                return itemRenderer;
            }
        }
        
        internal static VisualStyleItemBackground HotItemRenderer
        {
            get
            {
                if( hotItemRenderer == null )
                    hotItemRenderer = new VisualStyleItemBackground( VisualStyleItemBackground.Style.Hot );
                return hotItemRenderer;
            }
        }
        
        internal static VisualStyleItemBackground FocusedItemRenderer
        {
            get
            {
                if( focusedItemRenderer == null )
                    focusedItemRenderer = new VisualStyleItemBackground( VisualStyleItemBackground.Style.Focused );
                return focusedItemRenderer;
            }
        }
        
        internal static VisualStyleItemBackground SelectedItemRenderer
        {
            get
            {
                if( selectedItemRenderer == null )
                    selectedItemRenderer = new VisualStyleItemBackground( VisualStyleItemBackground.Style.Selected );
                return selectedItemRenderer;
            }
        }
        
        internal static VisualStyleRenderer CollapsedRenderer
        {
            get
            {
                if( collapsedRenderer == null )
                {
                    VisualStyleElement element = VisualStyleElement.TreeView.Glyph.Closed;
                    if( VisualStyleRenderer.IsElementDefined( element ) )
                        collapsedRenderer = new VisualStyleRenderer( element );
                }
                return collapsedRenderer;
            }
        }
        
        internal static VisualStyleRenderer ExpandedRenderer
        {
            get
            {
                if( expandedRenderer == null )
                {
                    VisualStyleElement element = VisualStyleElement.TreeView.Glyph.Opened;
                    if( VisualStyleRenderer.IsElementDefined( element ) )
                        expandedRenderer = new VisualStyleRenderer( element );
                }
                return expandedRenderer;
            }
        }
        
        #endregion
    }
    
    public class CellRenderer
    {
        #region Constructor
        
        public CellRenderer( TreeListView owner )
        {
            this.owner = owner;
        }
        
        #endregion
        
        #region Private Fields

        protected TreeListView owner;
        
        #endregion
        
        #region Public Methods

        public virtual void DrawSelectionBackground( Graphics g, Rectangle rect, Node node )
        {
            rect.Height--;
            rect.Width--;
            if( this.owner.Focused && this.owner.HotRow == node )
            {
                if( Application.RenderWithVisualStyles )
                    TreeListRenderer.HotItemRenderer.DrawBackground( this.owner, g, rect );
            }
            if( this.owner.NodesSelection.Contains( node ) || this.owner.FocusedNode == node )
            {
                if( Application.RenderWithVisualStyles )
                {
                    if( this.owner.Focused )
                        TreeListRenderer.SelectedItemRenderer.DrawBackground( this.owner, g, rect );
                    else
                        TreeListRenderer.ItemRenderer.DrawBackground( this.owner, g, rect );
                }
                else
                {
                    g.FillRectangle( SystemBrushes.Highlight, rect );
                }
            }
            if( this.owner.Focused && this.owner.FocusedNode == node && this.owner.MultiSelect == true )
            {
                if( Application.RenderWithVisualStyles )
                    TreeListRenderer.FocusedItemRenderer.DrawBackground( this.owner, g, rect );
                else
                    ControlPaint.DrawFocusRectangle( g, rect );
            }
            rect.Height++;
            rect.Width++;
        }

        public virtual void PaintCell( Graphics g, Rectangle rect, Node node, TreeListColumn column, object data )
        {
            if( column.CellBackColor != Color.Transparent )
            {
                Rectangle cellRect = rect;
                cellRect.X = column.CalculatedRect.X;
                cellRect.Width = column.CalculatedRect.Width;
                using( SolidBrush brush = new SolidBrush( column.CellBackColor ) )
                {
                    g.FillRectangle( brush, cellRect );
                }
            }
            if( data != null )
            {
                rect.X += column.CellPadding.Left;
                rect.Width -= column.CellPadding.Horizontal;
                rect.Y += column.CellPadding.Top;
                rect.Height -= column.CellPadding.Vertical;

                if( data is Image )
                {
                    g.DrawImageUnscaledAndClipped( (Image) data, rect );
                }
                else
                {
                    Color color = column.CellForeColor;
                    if( this.owner.FocusedNode == node && !Application.RenderWithVisualStyles )
                        color = SystemColors.HighlightText;
                    else if( this.owner.HotRow == node && !Application.RenderWithVisualStyles )
                        color = SystemColors.HotTrack;
                    if( node.Color.IsEmpty == false )
                        color = node.Color;
                    TextFormatFlags flags = TextFormatFlags.NoPrefix | TextFormatFlags.EndEllipsis | column.CellFormatFlags;
                    TextRenderer.DrawText( g, data.ToString(), column.CellFont, rect, color, flags );
                }
            }
        }

        public virtual void PaintCellPlusMinus( Graphics g, Rectangle rect, Node node )
        {
            if( Application.RenderWithVisualStyles )
            {
                if( !Extensions.IsOdd( rect.Width ) )
                    rect.X += 1;
                if( !Extensions.IsOdd( rect.Height ) )
                    rect.Y += 1;

                if( node.Expanded )
                    TreeListRenderer.ExpandedRenderer.DrawBackground( g, rect );
                else
                    TreeListRenderer.CollapsedRenderer.DrawBackground( g, rect );
            }
            else
            {
                rect.Inflate( -1, -1 );
                g.FillRectangle( Brushes.White, rect );
                g.DrawRectangle( Pens.DarkSlateGray, rect );
                g.DrawLine( Pens.Black, rect.Left + 2, rect.Top + 4, rect.Right - 2, rect.Top + 4 );
                if( !node.Expanded )
                    g.DrawLine( Pens.Black, rect.Left + 4, rect.Top + 2, rect.Left + 4, rect.Bottom - 2 );
            }
        }
        
        public virtual void PaintCellCheckBox( Graphics g, Rectangle rect, Node node, bool isHot )
        {
            CheckBoxState state;
            switch( node.CheckState )
            {
                case CheckState.Checked:
                    state = isHot ? CheckBoxState.CheckedHot : CheckBoxState.CheckedNormal;
                    break;
                case CheckState.Unchecked:
                    state = isHot ? CheckBoxState.UncheckedHot : CheckBoxState.UncheckedNormal;
                    break;
                default:
                    state = isHot ? CheckBoxState.MixedHot : CheckBoxState.MixedNormal;
                    break;
            }
            CheckBoxRenderer.DrawCheckBox( g, rect.Location, state );
        }
        
        #endregion
    }

    public class ColumnHeaderRenderer
    {
        public virtual void DrawHeaderFiller( Graphics g, Rectangle rect )
        {
            if( Application.RenderWithVisualStyles )
            {
                rect.Height++;
                TreeListRenderer.HeaderRenderer.DrawBackground( g, rect );
                rect.Height--;
            }
            else
                ControlPaint.DrawButton( g, rect, ButtonState.Flat );
        }

        public virtual void DrawHeader( Graphics g, Rectangle rect )
        {
            this.DrawHeader( g, rect, null, false );
        }

        public virtual void DrawHeader( Graphics g, Rectangle rect, TreeListColumn column, bool isHot )
        {
            if( Application.RenderWithVisualStyles )
            {
                rect.Height++;
                if( isHot )
                    TreeListRenderer.HotHeaderRenderer.DrawBackground( g, rect );
                else
                    TreeListRenderer.HeaderRenderer.DrawBackground( g, rect );
                rect.Height--;
            }
            else
                ControlPaint.DrawButton( g, rect, ButtonState.Flat );
            
            if( column == null )
                return;

            if( column.HeaderBackColor != Color.Transparent )
            {
                using( SolidBrush brush = new SolidBrush( column.HeaderBackColor ) )
                {
                    g.FillRectangle( brush, rect );
                }
            }

            if( column.SortOrder != SortOrder.None && rect.Width > 17 )
            {
                Rectangle sortRect = new Rectangle( rect.Right - 13, rect.Y, 9, rect.Height );
                if( column.SortOrder == SortOrder.Ascending )
                    this.DrawSortingArrow( g, sortRect, column.HeaderForeColor, ArrowDirection.Down );
                else
                    this.DrawSortingArrow( g, sortRect, column.HeaderForeColor, ArrowDirection.Up );
                rect.Width -= 13;
            }

            rect.X += column.HeaderPadding.Left;
            rect.Width -= column.HeaderPadding.Horizontal;
            rect.Y += column.HeaderPadding.Top;
            rect.Height -= column.HeaderPadding.Vertical;
            
            Color color = column.HeaderForeColor;
            TextFormatFlags flags = TextFormatFlags.EndEllipsis | column.HeaderFormatFlags;
            TextRenderer.DrawText( g, column.Caption, column.HeaderFont, rect, color, flags );
        }

        public virtual void DrawSortingArrow( Graphics g, Rectangle rect, Color color, ArrowDirection direction )
        {
            Point center = new Point( rect.X + (rect.Width / 2), rect.Y + (rect.Height / 2) );
            Point[] arrowPoints = new Point[3];
            switch( direction )
            {
                case ArrowDirection.Up:
                    arrowPoints[0] = new Point( center.X, center.Y - 3 );
                    arrowPoints[1] = new Point( center.X + 5, center.Y + 2 );
                    arrowPoints[2] = new Point( center.X - 4, center.Y + 2 );
                    break;

                case ArrowDirection.Down:
                    arrowPoints[0] = new Point( center.X - 3, center.Y - 2 );
                    arrowPoints[1] = new Point( center.X + 4, center.Y - 2 );
                    arrowPoints[2] = new Point( center.X, center.Y + 2 );
                    break;
                default:
                    throw new ArgumentException( "Unsupported ArrowDirection." );
            }
            using( Brush brush = new SolidBrush( color ) )
            {
                g.FillPolygon( brush, arrowPoints );
            }
        }

        public virtual void DrawVerticalGridLines( TreeListColumnCollection columns, Graphics g, Rectangle rect, int hScrollOffset )
        {
            foreach( TreeListColumn col in columns.VisibleColumns )
            {
                int rightPos = col.CalculatedRect.Right - hScrollOffset;
                if( rightPos < 0 )
                    continue;
                g.DrawLine( TreeListRenderer.GridLinePen, rightPos, rect.Top, rightPos, rect.Bottom );
            }
        }
    }

    public class RowRenderer
    {
        public void DrawHeader( Graphics g, Rectangle rect, bool isHot )
        {
            if( Application.RenderWithVisualStyles )
            {
                if( isHot )
                    TreeListRenderer.HotHeaderRenderer.DrawBackground( g, rect );
                else
                    TreeListRenderer.HeaderRenderer.DrawBackground( g, rect );
            }
            else
                ControlPaint.DrawButton( g, rect, ButtonState.Flat );
        }

        public void DrawHorizontalGridLine(Graphics g, Rectangle rect )
        {
            rect.Height -= 1;
            g.DrawLine( TreeListRenderer.GridLinePen, rect.Left, rect.Bottom, rect.Right, rect.Bottom );
        }
    }
}
