﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Controls
{
    /// <summary>
    /// Description of TreeListView.
    /// </summary>
    [Designer(typeof(Design.TreeListViewDesigner), typeof(IDesigner))]
    public class TreeListView : Control, ISupportInitialize
    {
        #region Constructor

        public TreeListView()
        {
            this.DoubleBuffered = true;
            this.BackColor = SystemColors.Window;
            this.TabStop = true;

            this.rowRenderer = new RowRenderer();
            this.cellRenderer = new CellRenderer(this);

            this.nodes = new TreeListViewNodes(this);
            this.columns = new TreeListColumnCollection(this);

            // Add scroll bars
            this.vScroll = new VScrollBar();
            this.vScroll.Scroll += new ScrollEventHandler(this.OnVScroll);
            this.vScroll.Dock = DockStyle.Right;
            this.Controls.Add(this.vScroll);

            this.hScroll = new HScrollBar();
            this.hScroll.Scroll += new ScrollEventHandler(this.OnHScroll);
            this.hScroll.Dock = DockStyle.Fill;

            this.hScrollFiller = new Panel();
            this.hScrollFiller.BackColor = SystemColors.Control;
            this.hScrollFiller.Size = new Size(this.vScroll.Width - 1, this.hScroll.Height);
            this.hScrollFiller.Dock = DockStyle.Right;

            this.hScrollPanel = new Panel();
            this.hScrollPanel.Height = this.hScroll.Height;
            this.hScrollPanel.Dock = DockStyle.Bottom;
            this.hScrollPanel.Controls.Add(this.hScroll);
            this.hScrollPanel.Controls.Add(this.hScrollFiller);
            this.Controls.Add(this.hScrollPanel);
        }

        #endregion

        #region Events

        public delegate void NotifyBeforeExpandHandler(Node node, bool isExpanding);
        public delegate void NotifyAfterExpandHandler(Node node, bool isExpanding);

        [Category("Behavior")]
        public event EventHandler AfterSelect;

        [Category("Behavior")]
        public event NotifyBeforeExpandHandler NotifyBeforeExpand;

        [Category("Behavior")]
        public event NotifyAfterExpandHandler NotifyAfterExpand;

        internal event MouseEventHandler AfterResizeColumn;

        #endregion

        #region Event Handlers

        protected virtual void OnAfterSelect(Node node)
        {
            this.DoAfterSelect(node);
        }

        protected virtual void DoAfterSelect(Node node)
        {
            if (this.AfterSelect != null)
                this.AfterSelect(this, EventArgs.Empty);
        }

        public virtual void OnNotifyBeforeExpand(Node node, bool isExpanding)
        {
            this.DoNotifyBeforeExpand(node, isExpanding);
        }

        protected virtual void DoNotifyBeforeExpand(Node node, bool isExpanding)
        {
            if (this.NotifyBeforeExpand != null)
                this.NotifyBeforeExpand(node, isExpanding);
        }

        public virtual void OnNotifyAfterExpand(Node node, bool isExpanding)
        {
            this.DoNotifyAfterExpand(node, isExpanding);
        }

        protected virtual void DoNotifyAfterExpand(Node node, bool isExpanding)
        {
            if (this.NotifyAfterExpand != null)
                this.NotifyAfterExpand(node, isExpanding);
        }

        #endregion

        #region Private Fields

        private BorderStyle borderStyle = BorderStyle.Fixed3D;
        private const int lineIndent = 10;
        private int indent = 16;
        private bool showLines = true;
        private bool showPlusMinus = true;
        private bool showGridLines = false;
        private bool showRowHeader = false;
        private bool showCheckBoxes = false;
        private int leftMargin = 5;
        private int headerHeight = 25;
        private int rowHeaderWidth = 15;
        private int itemHeight = 21;
        private bool multiSelect = true;
        private ImageList images = null;
        private TreeListColumnCollection columns;

        private TreeListViewNodes nodes;
        private NodesSelection nodesSelection = new NodesSelection();
        private Node focusedNode = null;

        private VScrollBar vScroll;
        private HScrollBar hScroll;
        private Panel hScrollFiller;
        private Panel hScrollPanel;
        private Node firstVisibleNode = null;

        private RowRenderer rowRenderer;
        private CellRenderer cellRenderer;

        private TreeListColumn resizingColumn = null;
        private int resizingColumnScrollOffset = 0;

        private int hotRow = -1;
        private TreeListColumn hotColumn = null;

        #endregion

        #region Properties

        [Category("Appearance")]
        [DefaultValue(BorderStyle.Fixed3D)]
        public BorderStyle BorderStyle
        {
            get { return this.borderStyle; }
            set
            {
                if (this.borderStyle != value)
                {
                    this.borderStyle = value;
                    this.InternalUpdateStyles();
                    this.Invalidate();
                }
            }
        }

        [Category("Appearance")]
        [DefaultValue(typeof(Color), "Window")]
        public new Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }

        [Category("Behavior")]
        [DefaultValue(false)]
        public bool ShowRowHeader
        {
            get { return this.showRowHeader; }
            set
            {
                if (this.showRowHeader == value)
                    return;
                this.showRowHeader = value;
                this.columns.RecalcVisibleColumnsRect();
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(5)]
        public int LeftMargin
        {
            get { return this.leftMargin; }
            set
            {
                this.leftMargin = value;
                this.columns.RecalcVisibleColumnsRect();
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(25)]
        public int HeaderHeight
        {
            get { return this.headerHeight; }
            set
            {
                this.headerHeight = value;
                this.columns.RecalcVisibleColumnsRect();
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(15)]
        public int RowHeaderWidth
        {
            get { return this.rowHeaderWidth; }
            set
            {
                if (this.rowHeaderWidth == value)
                    return;
                this.rowHeaderWidth = value;
                this.columns.RecalcVisibleColumnsRect();
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(21)]
        public int ItemHeight
        {
            get { return this.itemHeight; }
            set
            {
                this.itemHeight = value;
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(16)]
        public int Indent
        {
            get { return this.indent; }
            set
            {
                this.indent = value;
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(true)]
        public bool ShowLines
        {
            get { return this.showLines; }
            set
            {
                this.showLines = value;
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(true)]
        public bool ShowPlusMinus
        {
            get { return this.showPlusMinus; }
            set
            {
                this.showPlusMinus = value;
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(false)]
        public bool ShowGridLines
        {
            get { return this.showGridLines; }
            set
            {
                this.showGridLines = value;
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(false)]
        public bool ShowCheckBoxes
        {
            get { return this.showCheckBoxes; }
            set
            {
                this.showCheckBoxes = value;
                this.Invalidate();
            }
        }

        [Category("Behavior")]
        [DefaultValue(true)]
        public bool MultiSelect
        {
            get { return this.multiSelect; }
            set { this.multiSelect = value; }
        }

        [Category("Behavior")]
        [DefaultValue(null)]
        public ImageList Images
        {
            get { return this.images; }
            set { this.images = value; }
        }

        [Category("Behavior")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public TreeListColumnCollection Columns
        {
            get { return this.columns; }
        }

        [Browsable(false)]
        public TreeListViewNodes Nodes
        {
            get { return this.nodes; }
        }

        [Browsable(false)]
        public NodesSelection NodesSelection
        {
            get { return this.nodesSelection; }
        }

        [Browsable(false)]
        public CellRenderer CellRenderer
        {
            get { return this.cellRenderer; }
            set { this.cellRenderer = value; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Node FocusedNode
        {
            get { return this.focusedNode; }
            set
            {
                Node currentNode = this.focusedNode;
                if (object.ReferenceEquals(currentNode, value))
                    return;
                if (this.multiSelect == false)
                    this.nodesSelection.Clear();

                int oldrow = NodeCollection.GetVisibleNodeIndex(currentNode);
                int newrow = NodeCollection.GetVisibleNodeIndex(value);

                this.focusedNode = value;
                this.OnAfterSelect(value);
                this.InvalidateRow(oldrow);
                this.InvalidateRow(newrow);
                this.EnsureVisible(this.focusedNode);
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~(int)NativeMethods.WS_BORDER;
                cp.ExStyle &= ~(int)NativeMethods.WS_EX_CLIENTEDGE;
                switch (this.borderStyle)
                {
                    case BorderStyle.Fixed3D:
                        cp.ExStyle |= (int)NativeMethods.WS_EX_CLIENTEDGE;
                        break;
                    case BorderStyle.FixedSingle:
                        cp.Style |= (int)NativeMethods.WS_BORDER;
                        break;
                    default:
                        break;
                }
                return cp;
            }
        }

        protected override Size DefaultSize
        {
            get { return new Size(150, 150); }
        }

        public new Rectangle ClientRectangle
        {
            get
            {
                Rectangle rect = base.ClientRectangle;
                if (this.vScroll.Visible)
                    rect.Width -= this.vScroll.Width - 1;
                if (this.hScroll.Visible)
                    rect.Height -= this.hScroll.Height - 1;
                return rect;
            }
        }

        internal Node HotRow
        {
            get
            {
                if (this.hotRow >= 0)
                    return NodeCollection.GetNextNode(this.firstVisibleNode, this.hotRow - this.VScrollValue());
                else
                    return null;
            }
        }

        internal new bool DesignMode
        {
            get { return base.DesignMode; }
        }

        #endregion

        #region Public Methods

        public void RecalcLayout()
        {
            if (this.firstVisibleNode == null)
                this.firstVisibleNode = this.nodes.FirstNode;

            if (this.nodes.Count == 0)
                this.firstVisibleNode = null;

            this.UpdateScrollBars();

            int vscroll = this.VScrollValue();
            if (vscroll == 0)
                this.firstVisibleNode = this.nodes.FirstNode;
            else
                this.firstVisibleNode = NodeCollection.GetNextNode(this.nodes.FirstNode, vscroll);
            this.Invalidate();
        }

        public void EnsureVisible(Node node)
        {
            int screenVisible = this.MaxVisibleRows() - 1;
            int visibleIndex = NodeCollection.GetVisibleNodeIndex(node);
            if (visibleIndex < this.VScrollValue())
                this.SetVScrollValue(visibleIndex);
            if (visibleIndex > this.VScrollValue() + screenVisible)
                this.SetVScrollValue(visibleIndex - screenVisible);
        }

        public Node CalcHitNode(Point mousePoint)
        {
            int hitRow = this.CalcHitRow(mousePoint);
            if (hitRow < 0)
                return null;
            return NodeCollection.GetNextNode(this.firstVisibleNode, hitRow);
        }

        public Node GetHitNode()
        {
            return this.CalcHitNode(this.PointToClient(Control.MousePosition));
        }

        public HitInfo CalcColumnHit(Point mousePoint)
        {
            return this.columns.CalcHitInfo(mousePoint, this.HScrollValue());
        }

        public bool HitTestScrollbar(Point mousePoint)
        {
            if (this.hScroll.Visible && mousePoint.Y >= this.ClientRectangle.Height - this.hScroll.Height)
                return true;
            return false;
        }

        public void BeginUpdate()
        {
            this.nodes.BeginUpdate();
        }

        public void EndUpdate()
        {
            this.nodes.EndUpdate();
            this.RecalcLayout();
        }

        #endregion

        #region Internal Methods

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (this.ClientRectangle.Width > 0 && this.ClientRectangle.Height > 0)
                this.UpdateScrollBars();
        }

        protected virtual void BeforeShowContextMenu()
        {
        }

        protected void InvalidateRow(int absoluteRowIndex)
        {
            int visibleRowIndex = absoluteRowIndex - this.VScrollValue();
            Rectangle rect = this.CalcRowRectangle(visibleRowIndex);
            if (rect != Rectangle.Empty)
            {
                rect.Inflate(1, 1);
                this.Invalidate(rect);
            }
        }

        private void OnVScroll(object sender, ScrollEventArgs e)
        {
            int diff = e.NewValue - e.OldValue;
            if (e.NewValue == 0)
            {
                this.firstVisibleNode = this.nodes.FirstNode;
                diff = 0;
            }
            this.firstVisibleNode = NodeCollection.GetNextNode(this.firstVisibleNode, diff);
            this.Invalidate();
        }

        private void OnHScroll(object sender, ScrollEventArgs e)
        {
            this.Invalidate();
        }

        private void SetVScrollValue(int value)
        {
            if (value < 0)
                value = 0;
            int max = this.vScroll.Maximum - this.vScroll.LargeChange + 1;
            if (value > max)
                value = max;

            if ((value >= 0 && value <= max) && (value != this.vScroll.Value))
            {
                ScrollEventArgs e = new ScrollEventArgs(ScrollEventType.ThumbPosition, this.vScroll.Value, value, ScrollOrientation.VerticalScroll);
                this.vScroll.Value = value;
                this.OnVScroll(this.vScroll, e);
            }
        }

        private int VScrollValue()
        {
            if (this.vScroll.Visible == false)
                return 0;
            return this.vScroll.Value;
        }

        private int HScrollValue()
        {
            if (this.hScroll.Visible == false)
                return 0;
            return this.hScroll.Value;
        }

        private void UpdateScrollBars()
        {
            if (this.ClientRectangle.Width < 0)
                return;

            int maxVisibleRows = this.MaxVisibleRows();
            int totalRows = this.nodes.VisibleNodeCount;
            if (totalRows <= 0 || maxVisibleRows > totalRows)
            {
                this.vScroll.Visible = false;
                this.SetVScrollValue(0);
            }
            else
            {
                this.vScroll.Visible = true;
                this.vScroll.SmallChange = 1;
                this.vScroll.LargeChange = maxVisibleRows;
                this.vScroll.Minimum = 0;
                this.vScroll.Maximum = totalRows - 1;

                int maxScrollValue = this.vScroll.Maximum - this.vScroll.LargeChange;
                if (maxScrollValue < this.vScroll.Value)
                    this.SetVScrollValue(maxScrollValue);
            }

            this.columns.RecalcVisibleColumnsRect();
            if (this.ClientRectangle.Width > this.MinWidth())
            {
                this.hScrollPanel.Visible = false;
                this.hScroll.Value = 0;
            }
            else
            {
                this.hScroll.Minimum = 0;
                this.hScroll.Maximum = this.MinWidth();
                this.hScroll.SmallChange = 5;
                this.hScroll.LargeChange = this.ClientRectangle.Width;
                this.hScrollFiller.Visible = this.vScroll.Visible;
                this.hScrollPanel.Visible = true;
            }
        }

        private int CalcHitRow(Point mousePoint)
        {
            if (mousePoint.X <= this.rowHeaderWidth && this.showRowHeader)
                return -1;
            if (mousePoint.Y <= this.headerHeight)
                return -1;
            return (mousePoint.Y - this.headerHeight) / this.itemHeight;
        }

        private int VisibleRowToYPoint(int visibleRowIndex)
        {
            return this.headerHeight + (visibleRowIndex * this.itemHeight);
        }

        private Rectangle CalcRowRectangle(int visibleRowIndex)
        {
            Rectangle rect = this.ClientRectangle;
            rect.Y = this.VisibleRowToYPoint(visibleRowIndex);
            if (rect.Top < this.headerHeight || rect.Top > this.ClientRectangle.Height)
                return Rectangle.Empty;
            rect.Height = this.itemHeight;
            return rect;
        }

        private void MultiSelectAdd(Node clickedNode, Keys modifierKeys)
        {
            if (modifierKeys == Keys.None)
            {
                foreach (Node node in this.nodesSelection)
                {
                    int newRow = NodeCollection.GetVisibleNodeIndex(node);
                    this.InvalidateRow(newRow);
                }
                this.nodesSelection.Clear();
                this.nodesSelection.Add(clickedNode);
            }

            if (modifierKeys == Keys.Shift)
            {
                if (this.nodesSelection.Count == 0)
                    this.nodesSelection.Add(clickedNode);
                else
                {
                    int startRow = NodeCollection.GetVisibleNodeIndex(this.nodesSelection[0]);
                    int currentRow = NodeCollection.GetVisibleNodeIndex(clickedNode);
                    if (currentRow > startRow)
                    {
                        Node startNode = this.nodesSelection[0];
                        this.nodesSelection.Clear();
                        foreach (Node node in NodeCollection.ForwardNodeIterator(startNode, clickedNode, true))
                            this.nodesSelection.Add(node);
                        this.Invalidate();
                    }
                    if (currentRow < startRow)
                    {
                        Node startNode = this.nodesSelection[0];
                        this.nodesSelection.Clear();
                        foreach (Node node in NodeCollection.ReverseNodeIterator(startNode, clickedNode, true))
                            this.nodesSelection.Add(node);
                        this.Invalidate();
                    }
                }
            }

            if (modifierKeys == Keys.Control)
            {
                if (this.nodesSelection.Contains(clickedNode))
                    this.nodesSelection.Remove(clickedNode);
                else
                    this.nodesSelection.Add(clickedNode);
            }

            this.InvalidateRow(NodeCollection.GetVisibleNodeIndex(clickedNode));
            this.FocusedNode = clickedNode;
        }

        private void ToggleCheckBox(Node node)
        {
            node.Checked = !node.Checked;
            if (node.Nodes != null && node.Nodes.Count > 0)
            {
                foreach (Node child in node.Nodes)
                {
                    child.Checked = node.Checked;
                }
            }
            Node parent = node.Parent;
            while (parent != null)
            {
                parent.CheckState = parent.Nodes.CheckState;
                parent = parent.Parent;
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                HitInfo info = this.columns.CalcHitInfo(e.Location, this.HScrollValue());
                if (Flag.IsSet(info.HitType, HitInfo.HitTypes.ColumnHeader) &&
                   !Flag.IsSet(info.HitType, HitInfo.HitTypes.ColumnHeaderResize))
                {
                    this.columns.ToggleSort(info.Column);
                    if (info.Column.SortAllowed)
                        this.nodes.Sort(info.Column.SortOrder, info.Column.Index);
                    else
                        this.Invalidate();
                }
                Node clickedNode = this.CalcHitNode(e.Location);
                if (clickedNode != null && this.columns.Count > 0)
                {
                    int clickedRow = this.CalcHitRow(e.Location);
                    Rectangle glyphRect = this.GetPlusMinusRectangle(clickedNode, this.columns.VisibleColumns[0], clickedRow);
                    if (clickedNode.HasChildren && glyphRect != Rectangle.Empty && glyphRect.Contains(e.Location))
                        clickedNode.Expanded = !clickedNode.Expanded;
                    glyphRect = this.GetCheckBoxRectangle(clickedNode, this.columns.VisibleColumns[0], clickedRow);
                    if (glyphRect != Rectangle.Empty && glyphRect.Contains(e.Location))
                        this.ToggleCheckBox(clickedNode);

                    if (this.multiSelect)
                        this.MultiSelectAdd(clickedNode, Control.ModifierKeys);
                    else
                        this.FocusedNode = clickedNode;
                }
            }
            base.OnMouseClick(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            Node clickedNode = this.CalcHitNode(e.Location);
            if (clickedNode != null && clickedNode.HasChildren)
                clickedNode.Expanded = !clickedNode.Expanded;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            this.Focus();
            if (e.Button == MouseButtons.Right)
            {
                Node clickedNode = this.CalcHitNode(e.Location);
                if (clickedNode != null)
                {
                    if (this.multiSelect)
                    {
                        if (this.nodesSelection.Contains(clickedNode) == false)
                            this.MultiSelectAdd(clickedNode, Control.ModifierKeys);
                    }
                    this.FocusedNode = clickedNode;
                    this.Invalidate();
                }
                this.BeforeShowContextMenu();
            }

            if (e.Button == MouseButtons.Left)
            {
                HitInfo info = this.columns.CalcHitInfo(e.Location, this.HScrollValue());
                if (Flag.IsSet(info.HitType, HitInfo.HitTypes.ColumnHeaderResize))
                {
                    this.resizingColumn = info.Column;
                    this.resizingColumnScrollOffset = this.HScrollValue();
                    return;
                }
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this.SetHotRow(-1);
            this.SetHotColumn(null);
            this.Invalidate();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.resizingColumn != null)
            {
                int left = this.resizingColumn.CalculatedRect.Left - this.resizingColumnScrollOffset;
                int width = e.X - left;
                if (width < 10)
                    width = 10;
                this.resizingColumn.Width = width;
                this.columns.RecalcVisibleColumnsRect(true);
                this.Invalidate();
                return;
            }

            TreeListColumn hotCol = null;
            HitInfo info = this.columns.CalcHitInfo(e.Location, this.HScrollValue());
            if (Flag.IsSet(info.HitType, HitInfo.HitTypes.ColumnHeader))
                hotCol = info.Column;
            if (Flag.IsSet(info.HitType, HitInfo.HitTypes.ColumnHeaderResize))
                this.Cursor = Cursors.VSplit;
            else
                this.Cursor = Cursors.Arrow;

            this.SetHotColumn(hotCol);

            int vScrollOffset = this.VScrollValue();

            int newHotRow = -1;
            if (hotCol == null)
            {
                int row = (e.Y - this.headerHeight) / this.itemHeight;
                newHotRow = row + vScrollOffset;
            }
            this.SetHotRow(newHotRow);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (this.resizingColumn != null)
            {
                this.resizingColumn = null; ;
                this.UpdateScrollBars();
                this.Invalidate();
                if (this.AfterResizeColumn != null)
                    this.AfterResizeColumn(this, e);
            }
            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            int value = this.vScroll.Value - (e.Delta * SystemInformation.MouseWheelScrollLines / 120);
            if (this.vScroll.Visible)
                this.SetVScrollValue(value);
            base.OnMouseWheel(e);
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            this.Invalidate();
        }

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            this.Invalidate();
        }

        private void SetHotColumn(TreeListColumn column)
        {
            int scrollOffset = this.HScrollValue();
            if (column != this.hotColumn)
            {
                if (this.hotColumn != null)
                {
                    this.hotColumn.IsHot = false;
                    Rectangle rect = this.hotColumn.CalculatedRect;
                    rect.X -= scrollOffset;
                    this.Invalidate(rect);
                }
                this.hotColumn = column;
                if (this.hotColumn != null)
                {
                    this.hotColumn.IsHot = true;
                    Rectangle rect = this.hotColumn.CalculatedRect;
                    rect.X -= scrollOffset;
                    this.Invalidate(rect);
                }
            }
        }

        private void SetHotRow(int row)
        {
            if (row != this.hotRow)
            {
                int oldHotRow = this.hotRow;
                this.hotRow = row;
                if (oldHotRow >= 0)
                    this.InvalidateRow(oldHotRow);
                if (this.hotRow >= 0)
                    this.InvalidateRow(this.hotRow);
            }
        }

        internal int InternalRowHeaderWidth
        {
            get
            {
                if (this.showRowHeader)
                    return this.rowHeaderWidth;
                return 0;
            }
        }

        private int MinWidth()
        {
            return this.InternalRowHeaderWidth + this.columns.ColumnsWidth;
        }

        private int MaxVisibleRows(out int unused)
        {
            unused = 0;
            if (this.ClientRectangle.Height < 0)
                return 0;

            int height = this.ClientRectangle.Height - this.headerHeight;
            unused = height % this.itemHeight;
            return height / this.itemHeight;
        }

        private int MaxVisibleRows()
        {
            int unused;
            return this.MaxVisibleRows(out unused);
        }

        private object GetDataDesignMode(Node node, TreeListColumn column)
        {
            string id = String.Empty;
            while (node != null)
            {
                id = node.Owner.GetNodeIndex(node).ToString() + ":" + id;
                node = node.Parent;
            }
            return "<temp>" + id;
        }

        protected virtual object GetData(Node node, TreeListColumn column)
        {
            if (node[column.Index] != null)
                return node[column.Index];
            return null;
        }

        protected virtual void PaintCell(Graphics g, Rectangle rect, Node node, TreeListColumn column)
        {
            if (this.DesignMode)
                this.cellRenderer.PaintCell(g, rect, node, column, GetDataDesignMode(node, column));
            else
                this.cellRenderer.PaintCell(g, rect, node, column, GetData(node, column));
        }

        protected virtual void PaintImage(Graphics g, Rectangle rect, Node node, Image image)
        {
            if (image != null)
                g.DrawImageUnscaled(image, rect);
        }

        protected virtual void PaintNode(Graphics g, Rectangle rect, Node node, TreeListColumn[] visibleColumns, int visibleRowIndex)
        {
            this.cellRenderer.DrawSelectionBackground(g, rect, node);
            foreach (TreeListColumn col in visibleColumns)
            {
                if (col.CalculatedRect.Right - this.HScrollValue() < this.InternalRowHeaderWidth)
                    continue;

                Rectangle cellRect = rect;
                cellRect.X = col.CalculatedRect.X - this.HScrollValue();
                cellRect.Width = col.CalculatedRect.Width;

                if (col.VisibleIndex == 0)
                {
                    cellRect.X += this.leftMargin;
                    cellRect.Width -= this.leftMargin;

                    int indentSize = this.GetIndentSize(node) + 5;
                    cellRect.X += indentSize;
                    cellRect.Width -= indentSize;

                    if (this.showLines)
                        this.PaintLines(g, cellRect, node);
                    cellRect.X += lineIndent;
                    cellRect.Width -= lineIndent;

                    Rectangle glyphRect = this.GetPlusMinusRectangle(node, col, visibleRowIndex);
                    if (glyphRect != Rectangle.Empty && this.showPlusMinus)
                        this.cellRenderer.PaintCellPlusMinus(g, glyphRect, node);

                    if (!this.showLines && !this.showPlusMinus)
                    {
                        cellRect.X -= (lineIndent + 5);
                        cellRect.Width += (lineIndent + 5);
                    }

                    if (this.showCheckBoxes)
                    {
                        Rectangle checkRect = this.GetCheckBoxRectangle(node, col, visibleRowIndex);
                        if (checkRect != Rectangle.Empty)
                        {
                            this.cellRenderer.PaintCellCheckBox(g, checkRect, node, false);
                            cellRect.X = checkRect.Right;
                        }
                    }

                    Image icon = this.GetNodeImage(node);
                    if (icon != null)
                    {
                        glyphRect.Y = cellRect.Y + (cellRect.Height / 2) - (icon.Height / 2);
                        glyphRect.X = cellRect.X;
                        glyphRect.Width = icon.Width;
                        glyphRect.Height = icon.Height;

                        this.PaintImage(g, glyphRect, node, icon);
                        cellRect.X += (glyphRect.Width + 2);
                        cellRect.Width -= (glyphRect.Width + 2);
                    }

                    this.PaintCell(g, cellRect, node, col);
                }
                else
                {
                    this.PaintCell(g, cellRect, node, col);
                }
            }
        }

        protected virtual void PaintLines(Graphics g, Rectangle rect, Node node)
        {
            using (Pen pen = new Pen(Color.Gray))
            {
                pen.DashStyle = DashStyle.Dot;

                int halfPoint = rect.Top + (rect.Height / 2);

                if (node.Parent == null && node.PrevSibling == null)
                {
                    rect.Y += (rect.Height / 2);
                    rect.Height -= (rect.Height / 2);
                }
                if (node.NextSibling != null)
                    g.DrawLine(pen, rect.X, rect.Top, rect.X, rect.Bottom);
                else
                    g.DrawLine(pen, rect.X, rect.Top, rect.X, halfPoint);
                g.DrawLine(pen, rect.X, halfPoint, rect.X + 10, halfPoint);

                Node parent = node.Parent;
                while (parent != null)
                {
                    rect.X -= this.indent;
                    if (parent.NextSibling != null)
                        g.DrawLine(pen, rect.X, rect.Top, rect.X, rect.Bottom);
                    parent = parent.Parent;
                }
            }
        }

        protected virtual int GetIndentSize(Node node)
        {
            int indent = 0;
            Node parent = node.Parent;
            while (parent != null)
            {
                indent += this.indent;
                parent = parent.Parent;
            }
            return indent;
        }

        protected virtual Rectangle GetPlusMinusRectangle(Node node, TreeListColumn firstColumn, int visibleRowIndex)
        {
            if (node.HasChildren == false)
                return Rectangle.Empty;

            int hScrollOffset = this.HScrollValue();
            if (firstColumn.CalculatedRect.Right - hScrollOffset < this.InternalRowHeaderWidth)
                return Rectangle.Empty;

            System.Diagnostics.Debug.Assert(firstColumn.VisibleIndex == 0);

            Rectangle glyphRect = firstColumn.CalculatedRect;
            glyphRect.X -= hScrollOffset;
            glyphRect.X += this.GetIndentSize(node);
            glyphRect.X += this.leftMargin;
            glyphRect.Width = 10;
            glyphRect.Y = this.VisibleRowToYPoint(visibleRowIndex);
            glyphRect.Y += (this.itemHeight - 10) / 2;
            glyphRect.Height = 10;
            return glyphRect;
        }

        protected virtual Rectangle GetCheckBoxRectangle(Node node, TreeListColumn firstColumn, int visibleRowIndex)
        {
            int hScrollOffset = this.HScrollValue();
            if (firstColumn.CalculatedRect.Right - hScrollOffset < this.InternalRowHeaderWidth)
                return Rectangle.Empty;

            System.Diagnostics.Debug.Assert(firstColumn.VisibleIndex == 0);

            Rectangle glyphRect = firstColumn.CalculatedRect;
            glyphRect.X -= hScrollOffset;
            glyphRect.X += this.GetIndentSize(node);
            glyphRect.X += this.leftMargin;
            if (this.showLines || this.showPlusMinus)
                glyphRect.X += lineIndent + 3;
            glyphRect.Width = 12;
            glyphRect.Y = this.VisibleRowToYPoint(visibleRowIndex);
            glyphRect.Y += (this.itemHeight - 12) / 2;
            glyphRect.Height = 12;
            return glyphRect;
        }

        protected virtual Image GetNodeImage(Node node)
        {
            if (this.images != null)
            {
                if (node.ImageId >= 0 && node.ImageId < this.images.Images.Count)
                    return this.images.Images[node.ImageId];
                if (!String.IsNullOrEmpty(node.ImageKey) && this.images.Images.ContainsKey(node.ImageKey))
                    return this.images.Images[node.ImageKey];
            }
            return null;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            int hScrollOffset = this.HScrollValue();
            int unused = 0;
            int visibleRows = this.MaxVisibleRows(out unused);
            if (unused > 0)
                visibleRows++;

            bool drawColumnHeaders = true;
            if (drawColumnHeaders)
            {
                Rectangle headerRect = e.ClipRectangle;
                this.columns.Draw(e.Graphics, headerRect, hScrollOffset);
            }

            if (this.showGridLines)
            {
                int remainRows = this.nodes.VisibleNodeCount - this.vScroll.Value;
                if (visibleRows < remainRows)
                    visibleRows = remainRows;

                Rectangle fullRect = this.ClientRectangle;
                if (drawColumnHeaders)
                    fullRect.Y += this.headerHeight;
                fullRect.Height = visibleRows * this.itemHeight;
                this.columns.Renderer.DrawVerticalGridLines(this.columns, e.Graphics, fullRect, hScrollOffset);
            }

            int visibleRowIndex = 0;
            TreeListColumn[] visibleColumns = this.columns.VisibleColumns;
            int columnsWidth = this.columns.ColumnsWidth;
            foreach (Node node in NodeCollection.ForwardNodeIterator(this.firstVisibleNode, true))
            {
                Rectangle rowRect = this.CalcRowRectangle(visibleRowIndex);
                if (rowRect == Rectangle.Empty || rowRect.Bottom <= e.ClipRectangle.Top || rowRect.Top >= e.ClipRectangle.Bottom)
                {
                    if (visibleRowIndex > visibleRows)
                        break;
                    visibleRowIndex++;
                    continue;
                }

                rowRect.X = this.InternalRowHeaderWidth - hScrollOffset;
                rowRect.Width = columnsWidth;

                if (this.showGridLines)
                {
                    Rectangle rect = rowRect;
                    rect.X = this.InternalRowHeaderWidth;
                    rect.Width = columnsWidth - hScrollOffset;
                    this.rowRenderer.DrawHorizontalGridLine(e.Graphics, rect);
                }

                this.PaintNode(e.Graphics, rowRect, node, visibleColumns, visibleRowIndex);

                if (this.showRowHeader)
                {
                    Rectangle headerRect = rowRect;
                    headerRect.Height += 1;
                    headerRect.X = 0;
                    headerRect.Width = this.InternalRowHeaderWidth;
                    int absoluteRowIndex = visibleRowIndex + this.VScrollValue();
                    this.rowRenderer.DrawHeader(e.Graphics, headerRect, absoluteRowIndex == this.hotRow);
                }

                visibleRowIndex++;
            }
        }

        protected override bool IsInputKey(Keys keyData)
        {
            if (Flag.IsSet(keyData, Keys.Shift))
                return true;

            switch (keyData)
            {
                case Keys.Left:
                case Keys.Right:
                case Keys.Up:
                case Keys.Down:
                case Keys.PageUp:
                case Keys.PageDown:
                case Keys.Home:
                case Keys.End:
                    return true;
            }
            return false;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            Node newNode = null;
            if (e.KeyCode == Keys.PageUp)
            {
                int unusued;
                int diff = this.MaxVisibleRows(out unusued) - 1;
                newNode = NodeCollection.GetNextNode(this.focusedNode, -diff);
                if (newNode == null)
                    newNode = this.nodes.FirstVisibleNode();
            }
            if (e.KeyCode == Keys.PageDown)
            {
                int unusued;
                int diff = this.MaxVisibleRows(out unusued) - 1;
                newNode = NodeCollection.GetNextNode(this.focusedNode, diff);
                if (newNode == null)
                    newNode = this.nodes.LastVisibleNode(true);
            }

            if (e.KeyCode == Keys.Down)
                newNode = NodeCollection.GetNextNode(this.focusedNode, 1);
            if (e.KeyCode == Keys.Up)
                newNode = NodeCollection.GetNextNode(this.focusedNode, -1);
            if (e.KeyCode == Keys.Home)
                newNode = this.nodes.FirstNode;
            if (e.KeyCode == Keys.End)
                newNode = this.nodes.LastVisibleNode(true);

            if (e.KeyCode == Keys.Left)
            {
                if (this.focusedNode != null)
                {
                    if (this.focusedNode.Expanded)
                    {
                        this.focusedNode.Collapse();
                        this.EnsureVisible(this.focusedNode);
                        return;
                    }
                    if (this.focusedNode.Parent != null)
                    {
                        this.FocusedNode = this.focusedNode.Parent;
                        this.EnsureVisible(this.focusedNode);
                    }
                }
            }

            if (e.KeyCode == Keys.Right)
            {
                if (this.focusedNode != null)
                {
                    if (this.focusedNode.Expanded == false && this.focusedNode.HasChildren)
                    {
                        this.focusedNode.Expand();
                        this.EnsureVisible(this.focusedNode);
                        return;
                    }
                    if (this.focusedNode.Expanded == true && this.focusedNode.HasChildren)
                    {
                        this.FocusedNode = this.focusedNode.Nodes.FirstNode;
                        this.EnsureVisible(this.focusedNode);
                    }
                }
            }

            if (newNode != null)
            {
                if (this.multiSelect)
                {
                    if (Control.ModifierKeys == Keys.Control)
                        this.FocusedNode = newNode;
                    else
                        this.MultiSelectAdd(newNode, Control.ModifierKeys);
                }
                else
                    this.FocusedNode = newNode;
                this.EnsureVisible(this.focusedNode);
            }

            base.OnKeyDown(e);
        }

        internal void InternalUpdateStyles()
        {
            base.UpdateStyles();
        }

        #endregion

        #region ISupportInitialize

        public void BeginInit()
        {
            this.columns.BeginInit();
        }

        public void EndInit()
        {
            this.columns.EndInit();
        }

        #endregion
    }

    public class TreeListViewNodes : NodeCollection
    {
        #region Constructor

        public TreeListViewNodes(TreeListView owner)
            : base(null)
        {
            this.tree = owner;
        }

        #endregion

        #region Private Fields

        private TreeListView tree;
        private bool isUpdating;

        #endregion

        #region Public Methods

        public void BeginUpdate()
        {
            this.isUpdating = true;
        }

        public void EndUpdate()
        {
            this.isUpdating = false;
        }

        public void ExpandAll()
        {
            Node node = this.FirstNode;
            while (node != null)
            {
                node.ExpandAll();
                node = node.NextSibling;
            }
        }

        public void CollapseAll()
        {
            Node node = this.FirstNode;
            while (node != null)
            {
                node.CollapseAll();
                node = node.NextSibling;
            }
        }

        public override Node Add(Node node)
        {
            Node nodeOut = base.Add(node);
            if (this.isUpdating == false)
                this.tree.RecalcLayout();
            nodeOut.CheckedChanged += delegate
            {
                this.tree.Invalidate();
            };
            return nodeOut;
        }

        public override void Clear()
        {
            base.Clear();
            this.tree.RecalcLayout();
        }

        public override void NotifyBeforeExpand(Node nodeToExpand, bool expanding)
        {
            if (!tree.DesignMode)
                this.tree.OnNotifyBeforeExpand(nodeToExpand, expanding);
        }

        public override void NotifyAfterExpand(Node nodeToExpand, bool expanding)
        {
            this.tree.OnNotifyAfterExpand(nodeToExpand, expanding);
        }

        public override void Sort(SortOrder order, int field)
        {
            base.Sort(order, field);
            this.tree.RecalcLayout();
        }

        #endregion

        #region Internal Methods

        protected override int GetFieldIndex(string fieldName)
        {
            TreeListColumn col = this.tree.Columns[fieldName];
            if (col != null)
                return col.Index;
            return -1;
        }

        protected override void UpdateNodeCount(int oldValue, int newValue)
        {
            base.UpdateNodeCount(oldValue, newValue);
            if (!this.isUpdating)
                this.tree.RecalcLayout();
        }

        #endregion
    }
}
