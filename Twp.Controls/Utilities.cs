﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
    public static class Utilities
    {
        public static TextFormatFlags AlignFormatFlags(ContentAlignment alignment)
        {
            switch (alignment)
            {
                case ContentAlignment.TopLeft:
                    return TextFormatFlags.Top | TextFormatFlags.Left;
                case ContentAlignment.TopCenter:
                    return TextFormatFlags.Top | TextFormatFlags.HorizontalCenter;
                case ContentAlignment.TopRight:
                    return TextFormatFlags.Top | TextFormatFlags.Right;
                case ContentAlignment.MiddleLeft:
                    return TextFormatFlags.VerticalCenter | TextFormatFlags.Left;
                case ContentAlignment.MiddleCenter:
                    return TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter;
                case ContentAlignment.MiddleRight:
                    return TextFormatFlags.VerticalCenter | TextFormatFlags.Right;
                case ContentAlignment.BottomLeft:
                    return TextFormatFlags.Bottom | TextFormatFlags.Left;
                case ContentAlignment.BottomCenter:
                    return TextFormatFlags.Bottom | TextFormatFlags.HorizontalCenter;
                case ContentAlignment.BottomRight:
                    return TextFormatFlags.Bottom | TextFormatFlags.Right;
                default:
                    throw new Exception("Invalid value for ContentAlignment");
            }
        }

        /// <summary>
        /// Creates a new color with adjusted brighness.
        /// </summary>
        /// <param name="color">The <see cref="Color"/> to adjust.</param>
        /// <param name="adjustment">The brighness adjustment factor. Must be between -1 and 1.
        /// Negative values gives a darker color, positive values gives a lighter color.</param>
        /// <returns>
        /// Adjusted <see cref="Color"/> structure.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException"/>
        public static Color AdjustColor(Color color, float adjustment)
        {
            if (adjustment < -1 || adjustment > 1)
                throw new ArgumentOutOfRangeException("adjustment", "Adjustment must be between -1 and 1");
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;

            if(adjustment < 0)
            {
                adjustment = 1 + adjustment;
                red *= adjustment;
                green *= adjustment;
                blue *= adjustment;
            }
            else
            {
                red = (255 - red) * adjustment + red;
                green = (255 - green) * adjustment + green;
                blue = (255 - blue) * adjustment + blue;
            }

            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }
    }
}

