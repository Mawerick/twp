﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace Twp.Controls
{
    public enum VLabelRenderer
    {
        Plain,
        Default,
        TabRenderer,
    }

    [Description( "Provides vertically aligned text." )]
    [Designer( typeof( Design.VLabelDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
    [ToolboxBitmap( typeof( VLabel ) )]
    public class VLabel : Control
    {
        public VLabel()
        {
            SetStyle( ControlStyles.AllPaintingInWmPaint |
                     ControlStyles.ResizeRedraw |
                     ControlStyles.OptimizedDoubleBuffer |
                     ControlStyles.SupportsTransparentBackColor,
                     true );
            this.BackColor = Color.Transparent;
        }

        private VLabelRenderer renderer = VLabelRenderer.Default;
        private ContentAlignment textAlign = ContentAlignment.MiddleLeft;
        private bool autoSize = true;

        [Category( "Appearance" )]
        [Description( "The Renderer used when painting the control." )]
        [DefaultValue( VLabelRenderer.Default )]
        public VLabelRenderer Renderer
        {
            get { return this.renderer; }
            set
            {
                if( this.renderer != value )
                {
                    this.renderer = value;
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "" )]
        [DefaultValue( ContentAlignment.MiddleLeft )]
        public ContentAlignment TextAlign
        {
            get { return this.textAlign; }
            set
            {
                if( this.textAlign != value )
                {
                    this.textAlign = value;
                    this.Invalidate();
                }
            }
        }

        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
        public override bool AutoSize
        {
            get { return this.autoSize; }
            set
            {
                if( this.autoSize != value )
                {
                    this.autoSize = value;
                    if( this.autoSize == true )
                        this.Size = FitSize;
                }
            }
        }

        private Size FitSize
        {
            get
            {
                Size fitSize;
                using( Graphics g = this.CreateGraphics() )
                {
                    SizeF textSize = g.MeasureString( this.Text, this.Font );
                    fitSize = new Size( (int) textSize.Height + 4, (int) textSize.Width + 10 );
                }
                return fitSize;
            }
        }

        protected override void OnFontChanged( EventArgs e )
        {
            base.OnFontChanged( e );
            if( this.autoSize == true )
                this.Size = FitSize;
        }

        protected override void OnTextChanged( EventArgs e )
        {
            base.OnTextChanged( e );
            if( this.autoSize == true )
                this.Size = FitSize;
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );
            switch( this.renderer )
            {
                case VLabelRenderer.Default:
                    this.PaintDefault( e.Graphics );
                    break;
                case VLabelRenderer.TabRenderer:
                    this.PaintWithTabRenderer( e.Graphics );
                    break;
                case VLabelRenderer.Plain:
                    this.PaintPlain( e.Graphics );
                    break;
            }
        }

        private void PaintWithTabRenderer( Graphics g )
        {
            Rectangle bounds = this.ClientRectangle;
            bounds.Height -= 1;
            TabRenderer.DrawTab( g, bounds, this.Text, this.Font, TabState.Selected, TabAlignment.Left );
        }

        private void PaintDefault( Graphics g )
        {
            // Draw background
            using( Brush brush = new SolidBrush( this.BackColor ) )
            {
                g.FillRectangle( brush, this.ClientRectangle );
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TranslateTransform( 0, this.ClientSize.Height );
            g.RotateTransform( -90 );

            this.PaintTitlebar( g );
            this.PaintText( g );
        }

        private void PaintPlain( Graphics g )
        {
            // Draw background
            using( Brush brush = new SolidBrush( this.BackColor ) )
            {
                g.FillRectangle( brush, this.ClientRectangle );
            }

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.TranslateTransform( 0, this.ClientSize.Height );
            g.RotateTransform( -90 );

            this.PaintText( g );
        }

        private void PaintTitlebar( Graphics g )
        {
            Rectangle rect = new Rectangle( 1, 1, this.ClientSize.Height - 1, 6 );
            RectangleCorners corners = RectangleCorners.Top;
            Point start = new Point( rect.X, rect.Top );
            Point end = new Point( rect.X, rect.Bottom );
            using( LinearGradientBrush brush = new LinearGradientBrush( start, end, SystemColors.Highlight, SystemColors.InactiveBorder ) )
            {
                using( GraphicsPath path = RoundedRectangle.Create( rect, 3, corners ) )
                {
                    g.FillPath( brush, path );
                    g.DrawPath( SystemPens.ControlDarkDark, path );
                }
            }

            rect = new Rectangle( 1, 6, this.ClientSize.Height - 1, this.ClientSize.Width - 1 );
            g.FillRectangle( SystemBrushes.InactiveBorder, rect );

            start = new Point( rect.X, rect.Top );
            end = new Point( rect.X, rect.Bottom );
            g.DrawLine( SystemPens.ControlDarkDark, start, end );

            start.Offset( rect.Width, 0 );
            end.Offset( rect.Width, 0 );
            g.DrawLine( SystemPens.ControlDarkDark, start, end );
        }

        private void PaintText( Graphics g )
        {
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            using( Brush brush = new SolidBrush( this.ForeColor ) )
            {
                RectangleF rect = new RectangleF( this.ClientRectangle.Y, this.ClientRectangle.X, this.ClientSize.Height, this.ClientSize.Width );
                rect.Inflate( -4, -2 );
                rect.Offset( 0, 2 );
                StringFormat sf = new StringFormat( StringFormatFlags.NoClip );
                if( this.autoSize )
                {
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                }
                else
                {
                    switch( this.textAlign )
                    {
                        case ContentAlignment.BottomCenter:
                            sf.Alignment = StringAlignment.Center;
                            sf.LineAlignment = StringAlignment.Far;
                            break;
                        case ContentAlignment.BottomLeft:
                            sf.Alignment = StringAlignment.Near;
                            sf.LineAlignment = StringAlignment.Far;
                            break;
                        case ContentAlignment.BottomRight:
                            sf.Alignment = StringAlignment.Far;
                            sf.LineAlignment = StringAlignment.Far;
                            break;
                        case ContentAlignment.MiddleCenter:
                            sf.Alignment = StringAlignment.Center;
                            sf.LineAlignment = StringAlignment.Center;
                            break;
                        case ContentAlignment.MiddleLeft:
                            sf.Alignment = StringAlignment.Near;
                            sf.LineAlignment = StringAlignment.Center;
                            break;
                        case ContentAlignment.MiddleRight:
                            sf.Alignment = StringAlignment.Far;
                            sf.LineAlignment = StringAlignment.Center;
                            break;
                        case ContentAlignment.TopCenter:
                            sf.Alignment = StringAlignment.Center;
                            sf.LineAlignment = StringAlignment.Near;
                            break;
                        case ContentAlignment.TopLeft:
                            sf.Alignment = StringAlignment.Near;
                            sf.LineAlignment = StringAlignment.Near;
                            break;
                        case ContentAlignment.TopRight:
                            sf.Alignment = StringAlignment.Far;
                            sf.LineAlignment = StringAlignment.Near;
                            break;
                    }
                }
                g.DrawString( this.Text, this.Font, brush, rect, sf );
            }
        }
    }
}
