﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Layout;
using Twp.Utilities;

namespace Twp.Controls
{
	public enum VerticalLayoutStyle
	{
	    TopToBottom,
		BottomToTop,
		Fill,
	}
	
	public enum HorizontalLayoutAlignment
	{
	    Left,
	    Right,
	    Center,
	    Stretch,
	}

	public class VerticalLayout : LayoutEngine
    {
	    #region Constructors

	    public VerticalLayout() : base()
	    {
	        this.contentOffset = new Point( 0, 0 );
	    }
	    
	    public VerticalLayout( Point contentOffset ) : base()
	    {
	        this.contentOffset = contentOffset;
	    }
	    
	    #endregion

	    #region Properties

        /// <summary>
        /// Returns a value equal to the full height used by the controls in the container.
        /// </summary>
        public int Height
        {
            get { return this.height; }
        }
        private int height = 0;

        /// <summary>
        /// Gets or sets a value representing the height of the space between controls.
        /// </summary>
        public uint Spacing
        {
            get { return this.spacing; }
            set { this.spacing = value; }
        }
        private uint spacing = 3;

        /// <summary>
        /// Gets or sets whether or not the spacing should be automatic (uses a control's top or bottom margin, whichever it higher).
        /// </summary>
        public bool AutoSpacing
        {
            get { return this.autoSpacing; }
            set { this.autoSpacing = value; }
        }
        private bool autoSpacing = true;

        /// <summary>
        /// Gets or sets a value indicating whether or not hidden controls are included by the LayoutEngine.
        /// </summary>
        public bool IncludeHidden
        {
            get { return this.includeHidden; }
            set { this.includeHidden = value; }
        }
        private bool includeHidden = false;
        
        /// <summary>
        /// Gets or sets a value indicating whether or not the last control will be stretched to fill the remaining space.
        /// </summary>
        public bool StretchLast
        {
            get { return this.stretchLast; }
            set { this.stretchLast = value; }
        }
        private bool stretchLast = false;

        /// <summary>
        /// Gets or sets a value representing the style used for the layout.
        /// </summary>
        public VerticalLayoutStyle LayoutStyle
        {
        	get { return this.layoutStyle; }
        	set { this.layoutStyle = value; }
        }
        private VerticalLayoutStyle layoutStyle = VerticalLayoutStyle.TopToBottom;
        
        /// <summary>
        /// Gets or sets a value representing the horizontal alignment use for the layout.
        /// </summary>
        public HorizontalLayoutAlignment HorizontalAlign
        {
            get { return this.horizontalAlign; }
            set { this.horizontalAlign = value; }
        }
        private HorizontalLayoutAlignment horizontalAlign = HorizontalLayoutAlignment.Left;
        
        private Point contentOffset;

        #endregion

        #region Methods

        public override bool Layout( object container, System.Windows.Forms.LayoutEventArgs layoutEventArgs )
        {
            if( container == null )
                throw new ArgumentNullException( "container" );
            
            Control parent = container as Control;
            if( parent == null )
                throw new ArgumentException( "container is not a control object!", "container" );
           
            switch( this.layoutStyle )
            {
            	case VerticalLayoutStyle.TopToBottom:
            		this.LayoutTopToBottom( parent );
            		break;
            		
            	case VerticalLayoutStyle.BottomToTop:
            		this.LayoutBottomToTop( parent );
            		break;
            		
            	case VerticalLayoutStyle.Fill:
            		this.LayoutFill( parent );
            		break;
            		
            	default:
            		throw new Exception( "Invalid VerticalLayoutStyle value: " + this.layoutStyle.ToString() );
            }
            
            return true;
        }
        
        private void LayoutTopToBottom( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( parent.Padding.Left, parent.Padding.Top );
            nextLocation.Offset( this.contentOffset );

            int lastMargin = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                
                if( this.autoSpacing && lastMargin != 0 )
                {
                    if( lastMargin < control.Margin.Top )
                        nextLocation.Y += control.Margin.Top - lastMargin;
                }

                nextLocation.X = this.GetAlignmentOffset( control, parent );
                control.Location = nextLocation;

                if( this.autoSpacing )
                {
                    nextLocation.Y += control.Height + control.Margin.Bottom;
                    lastMargin = control.Margin.Bottom;
                }
                else
                    nextLocation.Y += control.Height + (int) this.spacing;
            }
            if( this.stretchLast && parent.Controls.Count > 0 )
            {
                Control control = parent.Controls[parent.Controls.Count - 1];
                control.Height = pClientRect.Height - control.Top;
            }

            this.height = nextLocation.Y + parent.Padding.Bottom;
        }
        
        private void LayoutBottomToTop( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( parent.Padding.Left, pClientRect.Height - parent.Padding.Bottom );
            nextLocation.Offset( this.contentOffset );

            int lastMargin = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;

                if( this.autoSpacing && lastMargin != 0 )
                {
                    if( lastMargin < control.Margin.Bottom )
                        nextLocation.Y -= control.Margin.Bottom - lastMargin;
                }

                nextLocation.X = this.GetAlignmentOffset( control, parent );
                nextLocation.Y -= control.Height;
                control.Location = nextLocation;

                if( this.autoSpacing )
                {
                	nextLocation.Y -= control.Margin.Top;
                	lastMargin = control.Margin.Top;
                }
                else
                    nextLocation.Y -= (int) this.spacing;
            }
            if( this.stretchLast && parent.Controls.Count > 0 )
            {
                Control control = parent.Controls[0];
                int height = control.Bottom;
                control.Location = new Point( pClientRect.X + parent.Padding.Left, pClientRect.Y + parent.Padding.Top );
                control.Height = height - parent.Padding.Top;
            }

            this.height = pClientRect.Height - (nextLocation.Y + parent.Padding.Top);
        }
        
        private void LayoutFill( Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            Point nextLocation = pClientRect.Location;
            nextLocation.Offset( parent.Padding.Left, parent.Padding.Top );
            nextLocation.Offset( this.contentOffset );

            int contentHeight = 0;
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                
                contentHeight += control.Height;
            }
            
            int contentCount = parent.Controls.Count - 1;
            if( contentCount <= 0 )
                return;
            int fillSpacing = (pClientRect.Height - parent.Padding.Vertical - contentHeight) / contentCount;
            
            foreach( Control control in parent.Controls )
            {
                if( !control.Visible && !this.includeHidden )
                    continue;
                
                nextLocation.X = this.GetAlignmentOffset( control, parent );
                control.Location = nextLocation;

                nextLocation.Y += control.Height + fillSpacing;
            }

            this.height = nextLocation.Y + parent.Padding.Bottom;
        }
        
        private int GetAlignmentOffset( Control control, Control parent )
        {
            Rectangle pClientRect = parent.ClientRectangle;
            switch( this.horizontalAlign )
            {
                case HorizontalLayoutAlignment.Right:
                    return pClientRect.Width - control.Width - parent.Padding.Right;

                case HorizontalLayoutAlignment.Center:
                    return (pClientRect.Width - control.Width) / 2;
                    
                case HorizontalLayoutAlignment.Stretch:
                    control.Width = pClientRect.Width - parent.Padding.Horizontal;
                    return (pClientRect.Width - control.Width) / 2;
            }
            return parent.Padding.Left;
        }

        #endregion
    }
}
