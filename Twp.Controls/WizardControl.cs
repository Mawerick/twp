﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
    /// <summary>
    /// Description of WizardForm.
    /// </summary>
    [Designer( typeof( Design.WizardControlDesigner ), typeof( IDesigner ) )]
    [DefaultProperty( "Pages" )]
    [DefaultEvent( "Cancel" )]
    public partial class WizardControl : UserControl
    {
        #region Consts
        private const int FooterAreaHeight = 48;
        private readonly Point offsetCancel = new Point( 84, 36 );
        private readonly Point offsetNext = new Point( 168, 36 );
        private readonly Point offsetBack = new Point( 252, 36 );
        #endregion
        
        #region Fields
        private WizardPage selectedPage = null;
        private WizardPageCollection pages = null;
        private Image headerImage = null;
        private Image welcomeImage = null;
        private Font headerFont = null;
        private Font headerTitleFont = null;
        private Font welcomeFont = null;
        private Font welcomeTitleFont = null;
        #endregion
        
        #region Designer generated code
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button backButton;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        #endregion
        
        #region Constructor
        public WizardControl()
        {
            this.InitializeComponent();
            
            this.SetStyle( ControlStyles.AllPaintingInWmPaint |
                           ControlStyles.OptimizedDoubleBuffer |
                           ControlStyles.ResizeRedraw |
                           ControlStyles.UserPaint, true );
            
            base.Dock = DockStyle.Fill;
            this.pages = new WizardPageCollection( this );
        }
        
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if( this.components != null )
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion
        
        #region Designer generated code
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            //
            // cancelButton
            //
            this.cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            this.cancelButton.DialogResult = DialogResult.Cancel;
            this.cancelButton.Location = new Point( 344, 224 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new EventHandler( this.CancelButtonClick );
            //
            // nextButton
            //
            this.nextButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            this.nextButton.Location = new Point( 260, 224 );
            this.nextButton.Name = "nextButton";
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = "&Next >";
            this.nextButton.Click += new EventHandler( this.NextButtonClick );
            //
            // backButton
            //
            this.backButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            this.backButton.Location = new Point( 184, 224 );
            this.backButton.Name = "backButton";
            this.backButton.TabIndex = 6;
            this.backButton.Text = "< &Back";
            this.backButton.Click += new EventHandler( this.BackButtonClick );
            //
            // WizardControl
            //
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.nextButton );
            this.Controls.Add( this.backButton );
            this.Name = "WizardControl";
            this.Size = new Size( 428, 256 );
            this.ResumeLayout( false );
        }
        #endregion
        
        #region Properties
        [DefaultValue( DockStyle.Fill )]
        public override DockStyle Dock
        {
            get { return base.Dock; }
            set { base.Dock = value; }
        }
        
        [Category( "Data" )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        [Description( "Gets the collection of pages." )]
        public WizardPageCollection Pages
        {
            get { return this.pages; }
        }
        
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public WizardPage SelectedPage
        {
            get { return this.selectedPage; }
            set { this.ActivatePage( value ); }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        internal int SelectedIndex
        {
            get { return this.pages.IndexOf( this.selectedPage ); }
            set
            {
                if( this.pages.Count == 0 )
                {
                    this.ActivatePage( -1 );
                    return;
                }
                
                if( value < -1 || value >= this.pages.Count )
                {
                    throw new ArgumentOutOfRangeException( "SelectedIndex", value, "The page index must be between 0 and " + Convert.ToString( this.pages.Count - 1 ) );
                }
                
                this.ActivatePage( value );
            }
        }
        
        [DefaultValue( null )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the image displayed on the header of the standard pages." )]
        public Image HeaderImage
        {
            get { return this.headerImage; }
            set
            {
                if( this.headerImage != value )
                {
                    this.headerImage = value;
                    this.Invalidate();
                }
            }
        }

        [DefaultValue( null )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the image displayed on the header of the welcome and finish pages." )]
        public Image WelcomeImage
        {
            get { return this.welcomeImage; }
            set
            {
                if( this.welcomeImage != value )
                {
                    this.welcomeImage = value;
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "Gets or sets the font used to display the description of a standard page." )]
        public Font HeaderFont
        {
            get
            {
                if( this.headerFont != null )
                    return this.headerFont;
                else
                    return this.Font;
            }
            set
            {
                if( this.headerFont != value )
                {
                    this.headerFont = value;
                    this.Invalidate();
                }
            }
        }

        protected bool ShouldSerializeHeaderFont()
        {
            return this.headerFont != null;
        }
        [Category( "Appearance" )]
        [Description( "Gets or sets the font used to display the title of a standard page." )]
        public Font HeaderTitleFont
        {
            get
            {
                if( this.headerTitleFont != null )
                    return this.headerTitleFont;
                else
                    return new Font( this.Font.FontFamily, this.Font.Size + 2, FontStyle.Bold );
            }
            set
            {
                if( this.headerTitleFont != value )
                {
                    this.headerTitleFont = value;
                    this.Invalidate();
                }
            }
        }

        protected bool ShouldSerializeHeaderTitleFont()
        {
            return this.headerTitleFont != null;
        }

        [Category( "Appearance" )]
        [Description( "Gets or sets the font used to display the description of a welcome or finish page." )]
        public Font WelcomeFont
        {
            get
            {
                if( this.welcomeFont != null )
                    return this.welcomeFont;
                else
                    return this.Font;
            }
            set
            {
                if( this.welcomeFont != value )
                {
                    this.welcomeFont = value;
                    this.Invalidate();
                }
            }
        }

        protected bool ShouldSerializeWelcomeFont()
        {
            return this.welcomeFont != null;
        }

        [Category( "Appearance" )]
        [Description( "Gets or sets the font used to display the title of a welcome or finish page." )]
        public Font WelcomeTitleFont
        {
            get
            {
                if( this.welcomeTitleFont != null )
                    return this.welcomeTitleFont;
                else
                    return new Font( this.Font.FontFamily, this.Font.Size + 10, FontStyle.Bold );
            }
            set
            {
                if( this.welcomeTitleFont != value )
                {
                    this.welcomeTitleFont = value;
                    this.Invalidate();
                }
            }
        }

        protected bool ShouldSerializeWelcomeTitleFont()
        {
            return this.welcomeTitleFont != null;
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool NextEnabled
        {
            get { return this.nextButton.Enabled; }
            set { this.nextButton.Enabled = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool BackEnabled
        {
            get { return this.backButton.Enabled; }
            set { this.backButton.Enabled = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool CancelEnabled
        {
            get { return this.cancelButton.Enabled; }
            set { this.cancelButton.Enabled = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string NextText
        {
            get { return this.nextButton.Text; }
            set { this.nextButton.Text = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string BackText
        {
            get { return this.backButton.Text; }
            set { this.backButton.Text = value; }
        }
        
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string CancelText
        {
            get { return this.cancelButton.Text; }
            set { this.cancelButton.Text = value; }
        }
        #endregion
        
        #region Methods
        internal void HitTest( Point point, IntPtr handle )
        {
            if( handle == this.nextButton.Handle )
            {
                if( this.nextButton.Enabled && this.nextButton.ClientRectangle.Contains( point ) )
                    this.Next();
            }
            else if( handle == this.backButton.Handle )
            {
                if( this.backButton.Enabled && this.backButton.ClientRectangle.Contains( point ) )
                    this.Back();
            }
        }
        
        public void Next()
        {
            if( this.SelectedIndex == this.pages.Count - 1 )
                this.nextButton.Enabled = false;
            else
                this.OnBeforePageSwitched( new BeforePageSwitchedEventArgs( this.SelectedIndex, this.SelectedIndex + 1 ) );
        }

        public void Back()
        {
            if( this.SelectedIndex == 0 )
                this.backButton.Enabled = false;
            else
                this.OnBeforePageSwitched( new BeforePageSwitchedEventArgs( this.SelectedIndex, this.SelectedIndex - 1 ) );
        }
        
        private void ActivatePage( int index )
        {
            if( index < 0 || index >= this.pages.Count )
                return;
            
            WizardPage page = (WizardPage) this.pages[index];
            this.ActivatePage( page );
        }
        
        private void ActivatePage( WizardPage page )
        {
            if( !this.pages.Contains( page ) )
                return;
            
            if( this.selectedPage != null )
                this.selectedPage.Visible = false;
            
            this.selectedPage = page;
            
            if( this.selectedPage != null )
            {
                this.selectedPage.Parent = this;
                if( !this.Contains( this.selectedPage ) )
                    this.Container.Add( this.selectedPage );

                if( this.selectedPage.Style == WizardPageStyle.Finish )
                {
                    this.cancelButton.Text = "Ok";
                    this.cancelButton.DialogResult = DialogResult.OK;
                }
                else
                {
                    this.cancelButton.Text = "Cancel";
                    this.cancelButton.DialogResult = DialogResult.Cancel;
                }
                
                this.selectedPage.SetBounds( 0, 0, this.Width, this.Height - FooterAreaHeight);
                this.selectedPage.Visible = true;
                this.selectedPage.BringToFront();
                this.FocusFirstTabIndex( this.selectedPage );
            }
            
            if( this.SelectedIndex > 0 )
                this.backButton.Enabled = true;
            else
                this.backButton.Enabled = false;
            
            if( this.SelectedIndex < this.pages.Count - 1 )
                this.nextButton.Enabled = true;
            else
                this.nextButton.Enabled = false;
            
            if( this.selectedPage != null )
                this.selectedPage.Invalidate();
            else
                this.Invalidate();
        }
        
        private void FocusFirstTabIndex( Control container )
        {
            Control result = null;
            
            foreach( Control control in container.Controls )
            {
                if( control.CanFocus && (result == null || control.TabIndex < result.TabIndex) )
                    result = control;
            }
            
            if( result != null )
                result.Focus();
            else
                container.Focus();
        }
        
        protected virtual void OnBeforePageSwitched( BeforePageSwitchedEventArgs e )
        {
            if( this.BeforePageSwitched != null )
                this.BeforePageSwitched( this, e );
            
            if( e.Cancel )
                return;
            
            this.ActivatePage( e.NewIndex );
            
            this.OnAfterPageSwitched( e as AfterPageSwitchedEventArgs );
        }
        
        protected virtual void OnAfterPageSwitched( AfterPageSwitchedEventArgs e )
        {
            if( this.AfterPageSwitched != null )
                this.AfterPageSwitched( this, e );
        }
        
        protected virtual void OnCancel( CancelEventArgs e )
        {
            if( this.Cancel != null )
                this.Cancel( this, e );
            
            if( e.Cancel )
                this.ParentForm.DialogResult = DialogResult.None;
            else
                this.ParentForm.Close();
        }
        
        protected virtual void OnFinish( EventArgs e )
        {
            if( this.Finish != null )
                this.Finish( this, e );
            
            this.ParentForm.Close();
        }
        
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            
            if( this.pages.Count > 0 )
                this.ActivatePage( 0 );
        }
        
        protected override void OnResize( EventArgs e )
        {
            base.OnResize(e);
            
            if( this.selectedPage != null )
                this.selectedPage.SetBounds( 0, 0, this.Width, this.Height - FooterAreaHeight );
            
            this.cancelButton.Location = new Point( this.Width - this.offsetCancel.X, this.Height - this.offsetCancel.Y );
            this.nextButton.Location = new Point( this.Width - this.offsetNext.X, this.Height - this.offsetNext.Y );
            this.backButton.Location = new Point( this.Width - this.offsetBack.X, this.Height - this.offsetBack.Y );
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            
            // Draw line
            Rectangle rect = this.ClientRectangle;
            rect.Y = this.Height - FooterAreaHeight;
            rect.Height = FooterAreaHeight;
            ControlPaint.DrawBorder3D( e.Graphics, rect, Border3DStyle.Etched, Border3DSide.Top );
        }
        
        protected override void OnControlAdded( ControlEventArgs e )
        {
            if( e.Control is WizardPage == false &&
                e.Control != this.backButton &&
                e.Control != this.nextButton &&
                e.Control != this.cancelButton )
            {
                if( this.selectedPage != null )
                    this.selectedPage.Controls.Add( e.Control );
            }
            else
            {
                base.OnControlAdded(e);
            }
        }
        #endregion
        
        #region Events
        public event BeforePageSwitchedEventHandler BeforePageSwitched;
        public event AfterPageSwitchedEventHandler AfterPageSwitched;
        public event CancelEventHandler Cancel;
        public event EventHandler Finish;
        
        public delegate void BeforePageSwitchedEventHandler( object sender, BeforePageSwitchedEventArgs e );
        public delegate void AfterPageSwitchedEventHandler( object sender, AfterPageSwitchedEventArgs e );
        #endregion

        #region Event handlers
        private void NextButtonClick( object sender, EventArgs e )
        {
            this.Next();
        }
        
        private void BackButtonClick( object sender, EventArgs e )
        {
            this.Back();
        }
        
        private void CancelButtonClick( object sender, EventArgs e )
        {
            if( this.cancelButton.DialogResult == DialogResult.Cancel )
                this.OnCancel( new CancelEventArgs() );
            else if( this.cancelButton.DialogResult == DialogResult.OK )
                this.OnFinish( EventArgs.Empty );
        }
        #endregion
    }
    
    public class AfterPageSwitchedEventArgs : EventArgs
    {
        private int oldIndex;
        protected int newIndex;
        
        internal AfterPageSwitchedEventArgs( int oldIndex, int newIndex )
        {
            this.oldIndex = oldIndex;
            this.newIndex = newIndex;
        }
        
        public int NewIndex
        {
            get { return newIndex; }
        }

        public int OldIndex
        {
            get { return oldIndex; }
        }
    }
    
    public class BeforePageSwitchedEventArgs : AfterPageSwitchedEventArgs
    {
        private bool cancel = false;
        
        internal BeforePageSwitchedEventArgs( int oldIndex, int newIndex ) : base( oldIndex, newIndex )
        {
        }
        
        public bool Cancel
        {
            get { return this.cancel; }
            set { this.cancel = true; }
        }
        
        public new int NewIndex
        {
            get { return base.newIndex; }
            set { base.newIndex = value; }
        }
    }
}
