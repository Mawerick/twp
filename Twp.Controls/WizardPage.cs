﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
    public enum WizardPageStyle
    {
        Standard,
        Welcome,
        Finish,
        Custom
    }

    /// <summary>
    /// Description of WizardPage.
    /// </summary>
    [Designer( typeof( Design.WizardPageDesigner ) )]
    public class WizardPage : Panel
    {
        #region Consts
        internal const int HeaderAreaHeight = 64;
        internal const int HeaderGlyphSize = 48;
        internal const int HeaderTextPadding = 8;
        internal const int WelcomeGlyphSize = 168;
        #endregion
        
        #region Fields
        private WizardPageStyle style = WizardPageStyle.Standard;
        private string title = String.Empty;
        private string description = String.Empty;
        #endregion

        #region Constructor
        public WizardPage()
        {
            base.SetStyle( ControlStyles.AllPaintingInWmPaint |
                           ControlStyles.OptimizedDoubleBuffer |
                           ControlStyles.ResizeRedraw |
                           ControlStyles.UserPaint, true );
        }
        #endregion
        
        #region Properties
        [DefaultValue( WizardPageStyle.Standard )]
        [Category( "Appearance" )]
        [Description( "Gets or sets the style of the wizard page." )]
        public WizardPageStyle Style
        {
            get { return this.style; }
            set
            {
                if( this.style != value )
                {
                    this.style = value;
                    if( this.Parent != null && this.Parent is WizardControl )
                    {
                        WizardControl pWizard = (WizardControl) this.Parent;
                        if( pWizard.SelectedPage == this )
                            pWizard.SelectedPage = this;
                    }
                    else
                    {
                        this.Invalidate();
                    }
                }
            }
        }
        
        [DefaultValue( "" )]
        [Category( "Appearance" )]
        [Description( "Gets or set the title of the wizard page." )]
        public string Title
        {
            get { return this.title; }
            set
            {
                if( value == null )
                    value = String.Empty;
                if( this.title != value )
                {
                    this.title = value;
                    this.Invalidate();
                }
            }
        }

        public string Description
        {
            get { return this.description; }
            set
            {
                if( value == null )
                    value = String.Empty;
                if( this.description != value )
                {
                    this.description = value;
                    this.Invalidate();
                }
            }
        }
        #endregion
        
        #region Methods
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            
            if( this.style == WizardPageStyle.Custom )
                return;

            Rectangle headerRect = this.ClientRectangle;
            Rectangle glyphRect = Rectangle.Empty;
            Rectangle titleRect = Rectangle.Empty;
            Rectangle descriptionRect = Rectangle.Empty;
            
            StringFormat textFormat = StringFormat.GenericDefault;
            textFormat.LineAlignment = StringAlignment.Near;
            textFormat.Alignment = StringAlignment.Near;
            textFormat.Trimming = StringTrimming.EllipsisCharacter;
            
            switch( this.style )
            {
                case WizardPageStyle.Standard:
                    headerRect.Height = HeaderAreaHeight;
                    ControlPaint.DrawBorder3D( e.Graphics, headerRect, Border3DStyle.Etched, Border3DSide.Bottom );
                    headerRect.Height -= SystemInformation.Border3DSize.Height;
                    e.Graphics.FillRectangle( SystemBrushes.Window, headerRect );
                    
                    // measure header image rectangle
                    int headerPadding = (int) Math.Floor( (double)(HeaderAreaHeight - HeaderGlyphSize) / 2 );
                    glyphRect.Location = new Point( this.Width - HeaderGlyphSize - headerPadding, headerPadding );
                    glyphRect.Size = new Size( HeaderGlyphSize, HeaderGlyphSize );
                    
                    // determine the header content
                    Image headerImage = null;
                    Font headerFont = this.Font;
                    Font headerTitleFont = this.Font;
                    if( this.Parent != null && this.Parent is WizardControl )
                    {
                        WizardControl pWizard = (WizardControl) this.Parent;
                        headerImage = pWizard.HeaderImage;
                        headerFont = pWizard.HeaderFont;
                        headerTitleFont = pWizard.HeaderTitleFont;
                    }
                    
                    if( headerImage != null )
                        e.Graphics.DrawImage( headerImage, glyphRect );
                    else
                        ControlPaint.DrawFocusRectangle( e.Graphics, glyphRect );

                    int headerTitleHeight = (int) Math.Ceiling( e.Graphics.MeasureString( this.title, headerTitleFont, 0, textFormat ).Height );
                    titleRect.Location = new Point( HeaderTextPadding, HeaderTextPadding );
                    titleRect.Size = new Size( glyphRect.Left - HeaderTextPadding, headerTitleHeight );
                    descriptionRect.Location = titleRect.Location;
                    descriptionRect.Y += headerTitleHeight + HeaderTextPadding / 2;
                    descriptionRect.Size = new Size( titleRect.Width, HeaderAreaHeight - descriptionRect.Y );
                    
                    e.Graphics.DrawString( this.title, headerTitleFont, SystemBrushes.WindowText, titleRect, textFormat );
                    e.Graphics.DrawString( this.description, headerFont, SystemBrushes.WindowText, descriptionRect, textFormat );
                    break;

                case WizardPageStyle.Welcome:
                case WizardPageStyle.Finish:
                    e.Graphics.FillRectangle( SystemBrushes.Window, headerRect );
                    glyphRect.Location = Point.Empty;
                    glyphRect.Size = new Size( WelcomeGlyphSize, this.Height );
                    
                    Image welcomeImage = null;
                    Font welcomeFont = this.Font;
                    Font welcomeTitleFont = this.Font;
                    if( this.Parent != null && this.Parent is WizardControl )
                    {
                        WizardControl pWizard = (WizardControl) this.Parent;
                        welcomeImage = pWizard.WelcomeImage;
                        welcomeFont = pWizard.WelcomeFont;
                        welcomeTitleFont = pWizard.WelcomeTitleFont;
                    }
                   
                    if( welcomeImage != null )
                        e.Graphics.DrawImage( welcomeImage, glyphRect );
                    else
                        ControlPaint.DrawFocusRectangle( e.Graphics, glyphRect );

                    titleRect.Location = new Point( WelcomeGlyphSize + HeaderTextPadding, HeaderTextPadding );
                    titleRect.Width = this.Width - titleRect.Left - HeaderTextPadding;
                    int welcomeTitleHeight = (int) Math.Ceiling( e.Graphics.MeasureString( this.title, welcomeTitleFont, titleRect.Width, textFormat ).Height );
                    descriptionRect.Location = titleRect.Location;
                    descriptionRect.Y += welcomeTitleHeight + HeaderTextPadding;
                    descriptionRect.Size = new Size( this.Width - descriptionRect.Left - HeaderTextPadding, this.Height - descriptionRect.Y );
                    
                    e.Graphics.DrawString( this.title, welcomeTitleFont, SystemBrushes.WindowText, titleRect, textFormat );
                    e.Graphics.DrawString( this.description, welcomeFont, SystemBrushes.WindowText, descriptionRect, textFormat );
                    break;
            }
        }
        #endregion
    }
}
