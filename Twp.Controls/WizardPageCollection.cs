﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Collections;

namespace Twp.Controls
{
    /// <summary>
    /// Description of WizardPageCollection.
    /// </summary>
    public class WizardPageCollection : CollectionBase
    {
        private WizardControl owner;
        
        internal WizardPageCollection( WizardControl owner )
        {
            this.owner = owner;
        }
        
        public WizardPage this[int index]
        {
            get { return (WizardPage) base.List[index]; }
            set { base.List[index] = value; }
        }
        
        public int Add( WizardPage page )
        {
            return this.List.Add( page );
        }
        
        public void AddRange( WizardPage[] pages )
        {
            foreach( WizardPage page in pages )
            {
                this.List.Add( page );
            }
        }
        
        public int IndexOf( WizardPage page )
        {
            return this.List.IndexOf( page );
        }
        
        public void Insert( int index, WizardPage page )
        {
            this.List.Insert( index, page );
        }
        
        public void Remove( WizardPage page )
        {
            this.List.Remove( page );
        }
        
        public bool Contains( WizardPage page )
        {
            return this.List.Contains( page );
        }
        
        protected override void OnInsertComplete( int index, object value )
        {
            base.OnInsertComplete( index, value );
            this.owner.SelectedIndex = index;
        }
        
        protected override void OnRemoveComplete( int index, object value )
        {
            base.OnRemoveComplete( index, value );
            
            if( this.owner.SelectedIndex == index )
            {
                if( index < this.InnerList.Count )
                    this.owner.SelectedIndex = index;
                else
                    this.owner.SelectedIndex = this.InnerList.Count;
            }
        }
    }
}
