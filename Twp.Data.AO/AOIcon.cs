﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Twp.Data.AO
{
	/// <summary>
	///
	/// </summary>
	public class AOIcon : IDisposable
	{
		#region Constructors
		
		public AOIcon( SQLiteResult row, Database db )
		{
			this.id = Convert.ToInt32( row["_id"] );
			this.image = FromByteArray( (byte[]) row["bytes"] );
			this.db = db;
			this.db.Icons.Add( this.id, this );
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AOIcon"/> class with the specified ID and data blob.
		/// </summary>
		/// <param name="id">The database ID of the icon.</param>
		/// <param name="bytes">A byte array holding the icon's bitmap data.</param>
		public AOIcon( int id, byte[] bytes )
		{
			this.id = id;
			this.image = FromByteArray( bytes );
			this.db = null;
		}

		#endregion
		
		#region Private Fields

		private int id = -1;
		private Bitmap image = null;
		private Database db;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the database ID of the icon.
		/// </summary>
		public int Id
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets a Bitmap containing the icon.
		/// </summary>
		public Bitmap Image
		{
			get { return (Bitmap) this.image.Clone(); }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Attempts to load an <see cref="AOIcon"/> from the <see cref="DatabaseManager.ItemsDatabase"/>.
		/// </summary>
		/// <param name="id">The ID of the Icon to load.</param>
		/// <returns>A <see cref="AOIcon"/> representing the icon, or null if no icon was found.</returns>
		public static AOIcon Load( int id )
		{
			return Load( id, DatabaseManager.ItemsDatabase );
		}
		
		/// <summary>
		/// Attempts to load an <see cref="AOIcon"/> from the provided <see cref="SQLiteDatabase"/>.
		/// </summary>
		/// <param name="id">The ID of the Icon to load.</param>
		/// <param name="db">The <see cref="SQLiteDatabase"/> to query.</param>
		/// <returns>A <see cref="AOIcon"/> representing the icon, or null if no icon was found.</returns>
		public static AOIcon Load( int id, Database db )
		{
			if( db == null )
				throw new DatabaseException( "Database not initialized." );

			if( db.Icons.Contains( id ) )
			{
				return db.Icons[id];
			}

			SQLiteResult row = db.QuerySingle( "SELECT * FROM 'icons' WHERE _id='{0}' LIMIT 1", id.ToString() );
			if( row.Count == 0 )
				return null;

			AOIcon icon = new AOIcon( row, db );
			return icon;
		}

		/// <summary>
		/// Writes the content of the <see cref="AOIcon"/> to the <see cref="DatabaseManager.ItemsDatabase"/>.
		/// </summary>
		public static void Save( int id, byte[] bytes )
		{
			Save( id, bytes, DatabaseManager.ItemsDatabase );
		}

		public static void Save( int id, byte[] bytes, Database db )
		{
			if( db == null )
				throw new DatabaseException( "Database not initialized." );
			
			Bitmap image = FromByteArray( bytes );
			FixImage( ref image );

			SQLiteArgument[] args = new SQLiteArgument[] {
				new SQLiteArgument( "_id", DbType.Int32, id ),
				new SQLiteArgument( "bytes", DbType.Binary, ToByteArray( image ) )
			};
			db.Update( "icons", args );
		}

		public static int Count()
		{
			return Count( DatabaseManager.ItemsDatabase );
		}
		
		public static int Count( Database db )
		{
			if( db == null )
				throw new DatabaseException( "Database not initialized." );

			SQLiteResult row = db.QuerySingle( "SELECT COUNT(_id) FROM 'icons'" );
			return row.Count == 0 ? 0 : Convert.ToInt32( row["COUNT(_id)"] );
		}

		public static AOIcon[] GetList( int start, int limit )
		{
			return GetList( start, limit, DatabaseManager.ItemsDatabase );
		}

		public static AOIcon[] GetList( int start, int limit, Database db )
		{
			if( db == null )
				throw new DatabaseException( "Database not initialized." );

			List<AOIcon> icons = new List<AOIcon>();
			SQLiteResults rows = db.Query( "SELECT * FROM 'icons' LIMIT {0}, {1}", start.ToString(), limit.ToString() );
			foreach( SQLiteResult row in rows )
			{
				Application.DoEvents();
				AOIcon icon = new AOIcon( row, db );
				icons.Add( icon );
			}

			return icons.ToArray();
		}
		
		public override string ToString()
		{
			return string.Format( "[AOIcon Id={0}, Image={1}]", id, image );
		}

		#endregion

		#region IDisposable interface

		private bool disposed = false;

		public void Dispose()
		{
			this.Dispose( true );
		}

		private void Dispose( bool disposing )
		{
			if( !this.disposed )
			{
				if( this.db != null )
					db.Icons.Remove( this.id );

				if( disposing )
				{
					if( this.image != null )
						this.image.Dispose();
				}
				this.disposed = true;
			}
		}

		#endregion

		#region Image conversion helpers

		private static Bitmap FromByteArray( byte[] bytes )
		{
			Bitmap image = null;
			using( MemoryStream ms = new MemoryStream( bytes ) )
			{
				image = (Bitmap) System.Drawing.Image.FromStream( ms );
			}
			return image;
		}

		private static byte[] ToByteArray( Bitmap image )
		{
			byte[] bytes = null;
			using( MemoryStream ms = new MemoryStream() )
			{
				image.Save( ms, ImageFormat.Png );
				bytes = ms.ToArray();
			}
			return bytes;
		}
		
		private static bool FixImage( ref Bitmap image )
		{
			MakeTransparent( ref image );
			
			if( image.Width == 80 ) // NCU icon...
			{
				Rectangle cropRect = new Rectangle( 0, 0, 40, 24 );
				Bitmap cropped1 = image.Clone( cropRect, image.PixelFormat );
				cropRect.Offset( 40, 0 );
				Bitmap cropped2 = image.Clone( cropRect, image.PixelFormat );
				
				Bitmap newImg = new Bitmap( 40, 48, image.PixelFormat );
				using( Graphics g = Graphics.FromImage( newImg ) )
				{
					cropRect.Offset( -40, 0 );
					g.DrawImageUnscaledAndClipped( cropped1, cropRect );
					cropRect.Offset( 0, 24 );
					g.DrawImageUnscaledAndClipped( cropped2, cropRect );
				}
				image = newImg;
			}
			
			return true;
		}
		
		private static void MakeTransparent( ref Bitmap image )
		{
			Size size = image.Size;
			Bitmap result = null;
			Graphics graphics = null;
			Rectangle rect = new Rectangle( 0, 0, size.Width, size.Height );
			
			try
			{
				result = new Bitmap( size.Width, size.Height, PixelFormat.Format32bppArgb );
				try
				{
					graphics = Graphics.FromImage( result );
					graphics.Clear( Color.Transparent );
					
					ImageAttributes attributes = null;
					try
					{
						attributes = new ImageAttributes();
						attributes.SetColorKey( Color.FromArgb( 0, 250, 0 ), Color.FromArgb( 7, 255, 7 ) );
						graphics.DrawImage( image, rect, 0, 0, size.Width, size.Height, GraphicsUnit.Pixel, attributes, null, IntPtr.Zero );
					}
					finally
					{
						if( attributes != null )
							attributes.Dispose();
					}
				}
				finally
				{
					if( graphics != null )
						graphics.Dispose();
				}
				
				image.Dispose();
				image = result.Clone( rect, PixelFormat.Format32bppArgb );
			}
			finally
			{
				if( result != null )
					result.Dispose();
			}
		}

		#endregion
	}
}
