﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Data;

using Twp.Utilities;

namespace Twp.Data.AO
{
	/// <summary>
	///
	/// </summary>
	public class AOItem : IEquatable<AOItem>, IComparable<AOItem>, IEquatable<object>, IDisposable
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AOItem"/> class.
		/// </summary>
		public AOItem()
		{
			this.id = -1;
			this.ql = 0;
			this.name = String.Empty;
			this.description = String.Empty;
			this.icon = 0;
			this.type = ItemType.Misc;
			this.db = null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AOItem"/> class with data from a database.
		/// </summary>
		/// <param name="row">A Dictionary holding the database data.</param>
		private AOItem( SQLiteResult row, Database db )
		{
			this.id = Convert.ToInt32( row["_id"] );
			this.ql = Convert.ToInt32( row["ql"] );
			this.name = (string) row["name"];
			this.description = (string) row["description"];
			this.icon = Convert.ToInt32( row["icon"] );
			this.type = (ItemType) Convert.ToInt32( row["type"] );
			this.data = (byte[]) row["data"];
			this.db = db;
			this.db.Items.Add( this.id, this );
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AOItem"/> class with the specified ID and data blob.
		/// </summary>
		/// <param name="id">The database ID of the item.</param>
		/// <param name="bytes">A byte array representing the item data.</param>
		public AOItem( int id, byte[] bytes )
		{
			this.id = id;
			this.data = bytes;
			this.db = null;
			
			ItemInfo info = ItemParser.Parse( bytes, false );
			this.ql = info.QL;
			this.name = info.Name;
			this.description = info.Description;
			this.icon = info.Attributes[AttributeKey.Icon];
			this.type = info.Type;
		}

		#endregion
		
		#region Private fields
		
		protected int id;
		protected int ql;
		protected string name;
		protected string description;
		protected int icon;
		protected ItemType type;
		protected byte[] data;
		protected Database db;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the item ID.
		/// </summary>
		public int Id
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets or sets the quality level of the item.
		/// </summary>
		public int Ql
		{
			get { return this.ql; }
			set { this.ql = value; }
		}

		/// <summary>
		/// Gets or sets the name of the item.
		/// </summary>
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		/// <summary>
		/// Gets or sets the description of the item.
		/// </summary>
		public string Description
		{
			get { return this.description; }
			set { this.description = value; }
		}

		/// <summary>
		/// Gets or sets the icon ID of the item.
		/// </summary>
		public int Icon
		{
			get { return this.icon; }
			set { this.icon = value; }
		}
		
		/// <summary>
		/// Gets or sets the <see cref="ItemType"/> of the item.
		/// </summary>
		public ItemType Type
		{
			get { return this.type; }
			set { this.type = value; }
		}

		/// <summary>
		/// Gets or sets the Data byte array for the item. This is the unparsed raw data from the AORDb.
		/// </summary>
		public byte[] Data
		{
			get { return this.data; }
			set { this.data = value; }
		}

		#endregion

		#region Methods
		
		/// <summary>
		/// Attempts to load an <see cref="AOItem"/> from the <see cref="DatabaseManager.ItemsDatabase"/>.
		/// </summary>
		/// <param name="id">The ID of the item to load.</param>
		/// <returns>A <see cref="AOItem"/> representing the item, or null if no item was found.</returns>
		public static AOItem Load( int id )
		{
			return Load( id, DatabaseManager.ItemsDatabase );
		}

		/// <summary>
		/// Attempts to load an <see cref="AOItem"/> from the provided <see cref="SQLiteDatabase"/>.
		/// </summary>
		/// <param name="id">The ID of the item to load.</param>
		/// <returns>A <see cref="AOItem"/> representing the item, or null if no item was found.</returns>
		public static AOItem Load( int id, Database db )
		{
			if( db == null )
				throw new DatabaseException( "Database not initialized." );

			if( db.Items.Contains( id ) )
				return db.Items[id];

			SQLiteResult row = db.QuerySingle( "SELECT _id, ql, name, description, icon, type, data FROM 'items' WHERE _id='{0}' LIMIT 1", id.ToString() );
			if( row.Count == 0 )
				return null;
			
			AOItem item = new AOItem( row, db );
			return item;
		}

		/// <summary>
		/// Writes the content of the <see cref="AOItem"/> to the <see cref="DatabaseManager.ItemsDatabase"/>.
		/// </summary>
		public void Save()
		{
			this.Save( DatabaseManager.ItemsDatabase );
		}

		public virtual void Save( Database db )
		{
			if( db == null )
				throw new DatabaseException( "Database not initialized." );

			SQLiteArgument[] args = new SQLiteArgument[] {
				new SQLiteArgument( "_id", DbType.Int32, this.id ),
				new SQLiteArgument( "ql", DbType.Int32, this.ql ),
				new SQLiteArgument( "name", DbType.String, this.name ),
				new SQLiteArgument( "description", DbType.String, this.description ),
				new SQLiteArgument( "icon", DbType.Int32, this.icon ),
				new SQLiteArgument( "type", DbType.Int32, (int) this.type ),
				new SQLiteArgument( "data", DbType.Binary, this.data ),
			};
			db.Update( "items", args );
			this.db = db;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="args"></param>
		/// <returns></returns>
		public static List<AOItem> Search( string[] args )
		{
			return Search( args, DatabaseManager.ItemsDatabase );
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="args"></param>
		/// <param name="db"></param>
		/// <returns></returns>
		public static List<AOItem> Search( string[] args, Database db )
		{
			return Search( args, 0, 0, db );
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="args"></param>
		/// <param name="minQL"></param>
		/// <param name="maxQL"></param>
		/// <returns></returns>
		public static List<AOItem> Search( string[] args, int minQL, int maxQL )
		{
			return Search( args, minQL, maxQL, DatabaseManager.ItemsDatabase );
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="args"></param>
		/// <param name="minQL"></param>
		/// <param name="maxQL"></param>
		/// <param name="db"></param>
		/// <returns></returns>
		public static List<AOItem> Search( string[] args, int minQL, int maxQL, Database db )
		{
			if( db == null )
				return null;

			string searchText = SQLiteDatabase.Escape( String.Join( "%", args ) );
			string sql = String.Format( "SELECT _id, ql, name, description, icon, type, data FROM 'items' WHERE name LIKE '%{0}%'", searchText );
			if( minQL > 0 )
				sql += String.Format( " AND ql >= '{0}'", minQL );
			if( maxQL > 0 && maxQL >= minQL )
				sql += String.Format( " AND ql <= '{0}'", maxQL );

			List<AOItem> items = new List<AOItem>();
			SQLiteResults rows = db.Query( sql );
			foreach( SQLiteResult row in rows )
			{
				AOItem item = new AOItem( row, db );
				items.Add( item );
			}
			return items;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="args"></param>
		/// <returns></returns>
		public static List<AOItem> GetFromIcon( int icon )
		{
			return GetFromIcon( icon, DatabaseManager.ItemsDatabase );
		}
		
		/// <summary>
		///
		/// </summary>
		/// <param name="icon"></param>
		/// <param name="db"></param>
		/// <returns></returns>
		public static List<AOItem> GetFromIcon( int icon, Database db )
		{
			if( db == null )
				return null;

			List<AOItem> items = new List<AOItem>();
			SQLiteResults rows = db.Query( "SELECT _id, ql, name, description, icon, type, data FROM 'items' WHERE icon='{0}';", icon.ToString() );
			foreach( SQLiteResult row in rows )
			{
				AOItem item = new AOItem( row, db );
				items.Add( item );
			}
			return items;
		}

		public override string ToString()
		{
			if( String.IsNullOrEmpty( this.name ) )
				return base.ToString();
			return String.Format( "{0} (QL{1}) [ID::{2}]", this.name, this.ql, this.id );
		}

		#endregion

		#region IEquatable implementation

		public override int GetHashCode()
		{
			int hashCode = 0;
			unchecked {
				hashCode += 1000000007 * id.GetHashCode();
				hashCode += 1000000009 * ql.GetHashCode();
				if (name != null)
					hashCode += 1000000021 * name.GetHashCode();
				if (description != null)
					hashCode += 1000000033 * description.GetHashCode();
				hashCode += 1000000087 * icon.GetHashCode();
				hashCode += 1000000093 * type.GetHashCode();
				if (data != null)
					hashCode += 1000000097 * data.GetHashCode();
			}
			return hashCode;
		}

		public bool Equals( AOItem other )
		{
			if( other == null )
				return false;

			return this.id == other.id &&
				this.ql == other.ql &&
				this.name == other.name &&
				this.description == other.description &&
				this.icon == other.icon &&
				this.type == other.type &&
				Extensions.Equals( this.data, other.data );
		}

        bool IEquatable<AOItem>.Equals( AOItem other )
        {
            return this.Equals( other );
        }

        bool IEquatable<object>.Equals( object other )
        {
            return this.Equals( other as AOItem );
        }

		public override bool Equals( object other )
		{
			return this.Equals( other as AOItem );
		}

        public static bool operator ==( AOItem item1, AOItem item2 )
		{
			if( object.ReferenceEquals( item1, item2 ) )
				return true;
			if( object.ReferenceEquals( item1, null ) )
				return false;
			if( object.ReferenceEquals( item2, null ) )
				return false;

			return item1.Equals( item2 );
		}

		public static bool operator !=( AOItem item1, AOItem item2 )
		{
			if( object.ReferenceEquals( item1, item2 ) )
				return false;
			if( object.ReferenceEquals( item1, null ) )
				return true;
			if( object.ReferenceEquals( item2, null ) )
				return true;

			return !item1.Equals( item2 );
		}

		#endregion

		#region IComparable implementation

		public int CompareTo( AOItem other )
		{
		    if( this.ql == other.ql )
		        return this.name.CompareTo( other.name );
		    else
    			return this.ql.CompareTo( other.ql );
		}

		#endregion

		#region IDisposable interface

		protected bool disposed = false;

		public void Dispose()
		{
			this.Dispose( true );
		}

		protected virtual void Dispose( bool disposing )
		{
			if( !this.disposed )
			{
				if( this.db != null )
					db.Items.Remove( this.id );

				if( disposing )
				{
					// Add stuff here if needed...
				}
				this.disposed = true;
			}
		}

		#endregion

    }
}
