﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Data;

namespace Twp.Data.AO
{
    /// <summary>
    ///
    /// </summary>
    public class AONano : AOItem, IEquatable<AONano>, IEquatable<object>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AONano"/> class.
        /// </summary>
        public AONano() : base()
        {
            this.school = 0;
            this.type = ItemType.Nano;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AONano"/> class with data from a database.
        /// </summary>
        /// <param name="row">A Dictionary holding the database data.</param>
        private AONano( SQLiteResult row, Database db ) : this()
        {
            base.id = Convert.ToInt32( row["_id"] );
            this.name = (string) row["name"];
            this.description = (string) row["description"];
            this.school = Convert.ToInt32( row["school"] );
            this.icon = Convert.ToInt32( row["icon"] );
            this.data = (byte[]) row["data"];
            this.db = db;
            this.db.Nanos.Add( base.id, this );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AONano"/> class with the specified ID and data blob.
        /// </summary>
        /// <param name="id">The database ID of the nano.</param>
        /// <param name="bytes">A byte array representing the nano data.</param>
        public AONano( int id, byte[] bytes ) : this()
        {
            this.id = id;
            this.data = bytes;

            ItemInfo info = ItemParser.Parse( bytes, false );
            this.ql = info.QL;
            this.name = info.Name;
            this.description = info.Description;
            this.icon = info.Attributes[AttributeKey.Icon];
            this.school = info.Attributes[AttributeKey.NanoSchool];
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the school ID of the nano.
        /// </summary>
        public int School
        {
            get { return this.school; }
            set { this.school = value; }
        }
        private int school;

        #endregion

        #region Methods

        /// <summary>
        /// Attempts to load an <see cref="AONano"/> from the <see cref="DatabaseManager.nanosDatabase"/>.
        /// </summary>
        /// <param name="id">The ID of the nano to load.</param>
        /// <returns>A <see cref="AONano"/> representing the nano, or null if no nano was found.</returns>
        public new static AONano Load( int id )
        {
            return Load( id, DatabaseManager.ItemsDatabase );
        }

        /// <summary>
        /// Attempts to load an <see cref="AONano"/> from the provided <see cref="SQLiteDatabase"/>.
        /// </summary>
        /// <param name="id">The ID of the nano to load.</param>
        /// <param name="db">The <see cref="SQLiteDatabase"/> to query.</param>
        /// <returns>A <see cref="AONano"/> representing the nano, or null if no nano was found.</returns>
        public new static AONano Load( int id, Database db )
        {
            if( db == null )
                throw new DatabaseException( "Database not initialized." );

            if( db.Nanos.Contains( id ) )
            {
                return db.Nanos[id];
            }
            
            SQLiteResult row = db.QuerySingle( "SELECT _id, name, description, school, icon, data FROM 'nanos' WHERE _id='{0}' LIMIT 1", id.ToString() );
            if( row.Count == 0 )
                return null;
            
            AONano nano = new AONano( row, db );
            return nano;
        }

        /// <summary>
		/// Writes the content of the <see cref="AONano"/> to the provided <see cref="SQLiteDatabase"/>.
        /// </summary>
        public override void Save( Database db )
        {
            if( db == null )
                throw new DatabaseException( "Database not initialized." );

            SQLiteArgument[] args = new SQLiteArgument[] {
                new SQLiteArgument( "_id", DbType.Int32, this.id ),
                new SQLiteArgument( "name", DbType.String, this.name ),
                new SQLiteArgument( "description", DbType.String, this.description ),
                new SQLiteArgument( "school", DbType.Int32, this.school ),
                new SQLiteArgument( "icon", DbType.Int32, this.icon ),
                new SQLiteArgument( "data", DbType.Binary, this.data ),
            };
            db.Update( "nanos", args );
            this.db = db;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public new static List<AONano> Search( string[] args )
        {
            return Search( args, DatabaseManager.ItemsDatabase );
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public new static List<AONano> Search( string[] args, Database db )
        {
            if( db == null )
                return null;

            List<AONano> nanos = new List<AONano>();
            SQLiteResults rows = db.Query( "SELECT _id, name, description, school, icon, data FROM 'nanos' WHERE name LIKE '%{0}%';", SQLiteDatabase.Escape( String.Join( "%", args ) ) );
            foreach( SQLiteResult row in rows )
            {
                nanos.Add( new AONano( row, db ) );
            }
            return nanos;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public new static List<AONano> GetFromIcon( int icon )
        {
            return GetFromIcon( icon, DatabaseManager.ItemsDatabase );
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="icon"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public new static List<AONano> GetFromIcon( int icon, Database db )
        {
            if( db == null )
                return null;

            List<AONano> items = new List<AONano>();
            SQLiteResults rows = db.Query( "SELECT _id, name, description, school, icon, data FROM 'nanos' WHERE icon='{0}';", icon.ToString() );
            foreach( SQLiteResult row in rows )
            {
                items.Add( new AONano( row, db ) );
            }
            return items;
        }

        public override string ToString()
        {
            if( String.IsNullOrEmpty( this.name ) )
                return base.ToString();
            string school = ((NanoSchool) this.school).ToString();
            if( String.IsNullOrEmpty( school ) )
                school = this.school.ToString();
            return String.Format( "{0} ({1}) [ID::{2}]", this.name, school, this.id );
        }

        #endregion

        #region IDisposable interface

        protected override void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                if( this.db != null )
                    db.Nanos.Remove( this.id );

                if( disposing )
                {
                    // Add stuff here if needed...
                }
                this.disposed = true;
            }
        }

        #endregion
        
        #region IEquatable implementation
         
        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();
            unchecked {
                hashCode += 1000000007 * school.GetHashCode();
            }
            return hashCode;
        }
       
        public bool Equals( AONano other )
        {
            if( other == null )
                return false;

            return this.school == other.school && base.Equals( other );
        }

        bool IEquatable<AONano>.Equals( AONano other )
        {
            return this.Equals( other );
        }

        bool IEquatable<object>.Equals( object other )
        {
            return this.Equals( other as AONano );
        }

        public override bool Equals( object obj )
        {
            return this.Equals(obj as AONano);
        }

        public static bool operator ==( AONano nano1, AONano nano2 )
        {
            if( object.ReferenceEquals( nano1, nano2 ) )
                return true;
            if( object.ReferenceEquals( nano1, null ) )
                return false;
            if( object.ReferenceEquals( nano2, null ) )
                return false;

            return nano1.Equals( nano2 );
        }

        public static bool operator !=( AONano nano1, AONano nano2 )
        {
            if( object.ReferenceEquals( nano1, nano2 ) )
                return false;
            if( object.ReferenceEquals( nano1, null ) )
                return true;
            if( object.ReferenceEquals( nano2, null ) )
                return true;

            return !nano1.Equals( nano2 );
        }

        #endregion
    }
}
