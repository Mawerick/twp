﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of AtkDefList.
    /// </summary>
    public class AtkDefList : List<AtkDef>
    {
        public AtkDefList( string type, ItemInfo owner ) : base()
        {
            this.type = type;
            this.owner = owner;
        }

        public AtkDefList( AtkDefList other, ItemInfo owner ) : base()
        {
            this.type = other.type;
            this.AddRange( other );
            this.owner = owner;
        }

        private readonly string type;
        private ItemInfo owner;

        public string Type
        {
            get { return this.type; }
        }

        public string ToHtml( bool raw )
        {
            if( this.Count <= 0 )
                return null;
            string html = String.Format( "<div><span class='header'>{0}:</span> ", type );
            foreach( AtkDef item in this )
            {
                if( raw )
                    html += String.Format( "{0} ({1})&nbsp;{2}% ", (AttributeKey) item.Key, item.Key, item.Value );
                else
                    html += String.Format( "{0}&nbsp;{1}% ", this.owner.Mmdb.GetString( 2003, item.Key ), item.Value );
            }
            html += "</div>";
            return html;
        }
    }
}
