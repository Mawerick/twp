﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Data.AO
{
    public class AttributeList : Dictionary<int, int>
    {
        private ItemInfo owner;

        public AttributeList( ItemInfo owner ) : base()
        {
            this.owner = owner;
        }
        
        public AttributeList( AttributeList copy ) : base()
        {
            foreach( KeyValuePair<int, int> attr in copy )
                this.Add( attr.Key, attr.Value );
            this.owner = copy.owner;
        }
        
        public bool Contains( AttributeKey key )
        {
            return this.ContainsKey( (int) key );
        }
        
        public int this[AttributeKey key]
        {
            get
            {
                if( this.ContainsKey( (int) key ) )
                    return base[(int) key];
                else
                    return -1;
            }
            set
            {
                if( this.ContainsKey( (int) key ) )
                    base[(int) key] = value;
                else
                    base.Add( (int) key, value );
            }
        }
        
        public string ToHtml()
        {
            string html = String.Empty;
            foreach( KeyValuePair<int, int> kvp in this )
            {
                string val = ItemFormatter.GetAttrValue( kvp.Key, (int) OperatorKey.Default, kvp.Value, this.owner.Mmdb );
                if( val == kvp.Value.ToString() )
                    html += String.Format( "<div><span class='header'>{0} ({1}):</span> {2}</div>",
                                          ItemFormatter.GetAttr( kvp.Key, this.owner.Mmdb ), kvp.Key, val );
                else
                    html += String.Format( "<div><span class='header'>{0} ({1}):</span> {2} ({3})</div>",
                                          ItemFormatter.GetAttr( kvp.Key, this.owner.Mmdb ), kvp.Key, val , kvp.Value );
            }
            return html;
        }
    }
}
