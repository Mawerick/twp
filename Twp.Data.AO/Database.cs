﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

using Twp.Controls;
using Twp.Utilities;

namespace Twp.Data.AO
{
    [Flags]
    public enum DataType
    {
        None = 0,
        Items = 1,
        Nanos = 2,
        Icons = 4,
        All = Items | Nanos | Icons,
    }

    /// <summary>
    ///
    /// </summary>
    public class Database : SQLiteDatabase
    {
#region Constructor

        public Database()
        {
        }

        public Database( string fileName )
            : base( fileName )
        {
        }

#endregion

#region Fields

        Cache<AOIcon> m_icons = new Cache<AOIcon>();
        Cache<AOItem> m_items = new Cache<AOItem>();
        Cache<AONano> m_nanos = new Cache<AONano>();

#endregion

#region Properties

        internal Cache<AOIcon> Icons
        {
            get { return m_icons; }
        }

        internal Cache<AOItem> Items
        {
            get { return m_items; }
        }

        internal Cache<AONano> Nanos
        {
            get { return m_nanos; }
        }

#endregion

#region Events

        /// <summary>
        /// Occurs when a <see cref="Database"/> is created.
        /// </summary>
        public event EventHandler<DatabaseEventArgs> DatabaseCreated;

        /// <summary>
        /// Occurs when a <see cref="Database"/> is about to be closed.
        /// </summary>
        public event EventHandler<DatabaseEventArgs> DatabaseClosing;

        /// <summary>
        /// Occurs when an <see cref="AOItem"/> is extracted.
        /// </summary>
        public event EventHandler<ItemExtractedEventArgs> ItemExtracted;

        void RaiseDatabaseCreated( Database db )
        {
            if( DatabaseCreated != null )
                DatabaseCreated( this, new DatabaseEventArgs( db ) );
        }

        void RaiseDatabaseClosing( Database db )
        {
            if( DatabaseClosing != null )
                DatabaseClosing( this, new DatabaseEventArgs( db ) );
        }

        void RaiseItemExtracted( Database db, AOItem item )
        {
            if( ItemExtracted != null )
                ItemExtracted( this, new ItemExtractedEventArgs( db, item ) );
        }

#endregion

#region Methods

        /// <summary>
        /// Creates and opens the database.
        /// </summary>
        public new void Open()
        {
            if( !IsLoaded )
                base.Open();
            NonQuery( Properties.Resources.DatabaseSQL );
        }

        /// <summary>
        /// Creates and opens the database.
        /// </summary>
        /// <param name="fileName">The name of the database file.</param>
        public new void Open( string fileName )
        {
            if( !IsLoaded )
                base.Open( fileName );
            NonQuery( Properties.Resources.DatabaseSQL );
        }

        /// <summary>
        /// Closes the databases
        /// </summary>
        public new void Close()
        {
            base.Close();
            Log.DebugIf( Icons.Count > 0, "[Database.Close] AOIcons: {0}", m_icons.Count );
            m_icons.Clear();
            Log.DebugIf( Items.Count > 0, "[Database.Close] AOItems: {0}", m_items.Count );
            m_items.Clear();
            Log.DebugIf( Nanos.Count > 0, "[Database.Close] AONanos: {0}", m_nanos.Count );
            m_nanos.Clear();
        }

        /// <summary>
        /// Rips the AO database at the <paramref name="path"/> provided, storing it in the default location.
        /// </summary>
        /// <param name="path">The base path to a valid Anarchy Online installation.</param>
        /// <param name="refresh">The interval (int number of items) at which to refresh the progress dialog.</param>
        /// <param name="types">The <see cref="DataType"/>s that should be extracted.</param>
        public void Extract( string path, int refresh, DataType types )
        {
            if( types == DataType.None )
                return;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Log.Debug( "[Database.Extract] Creating progress form." );
            ProgressForm progressForm = new ProgressForm();
            progressForm.CanBeCancelled = true;
            progressForm.AutoCloseDelay = 5;
            progressForm.Show();

            Database db = null;
            string tempFileName = "tempdb.db3";
            if( !String.IsNullOrEmpty( DatabaseManager.DataPath ) )
                tempFileName = System.IO.Path.Combine( DatabaseManager.DataPath, tempFileName );

            ProgressTask task;
            try
            {
                task = progressForm.AddTask( "Preparing database...", 2, 1, false );
                Log.Debug( "[Database.Extract] Creating new database." );
                task.Text = "Creating new database...";
                Application.DoEvents();
                if( progressForm.Cancelled )
                    throw new OperationCanceledException();
                db = new Database( tempFileName );
                db.Open();
                RaiseDatabaseCreated( db );
                db.StartTransaction();
                task.Value++;

                Log.Debug( "[Database.Extract] Initializing database." );
                task.Text = "Initializing database...";
                Application.DoEvents();
                if( progressForm.Cancelled )
                    throw new OperationCanceledException();
                ResourceDatabase rdb = new ResourceDatabase( path );
                task.Finish();

                if( Flag.IsSet( types, DataType.Items ) )
                {
                    // Rip Items
                    int count = 0;
                    int total = 0;
                    int[] items = rdb.GetRecordIds( ResourceDatabase.ItemType );
                    Log.Debug( "[Database.Extract] Ripping items: {0}.", items.Length );
                    progressForm.Status = "Extracting items from the Database";
                    task = progressForm.AddTask( "Items extracted", items.Length, 84 );
                    Application.DoEvents();
                    if( progressForm.Cancelled )
                        throw new OperationCanceledException();
                    foreach( int id in items )
                    {
                        byte[] data = rdb.GetRecordData( ResourceDatabase.ItemType, id );
                        using( AOItem item = new AOItem( id, data ) )
                        {
                            db.Items.Add( item.Id, item );
                            item.Save( db );
                            RaiseItemExtracted( db, item );
                        }
                        Application.DoEvents();
                        if( progressForm.Cancelled )
                            throw new OperationCanceledException();
                        count++;
                        total++;
                        if( count >= refresh )
                        {
                            task.Value += count;
                            count = 0;
                        }
                    }
                    task.Finish();
                    Application.DoEvents();
                    if( progressForm.Cancelled )
                        throw new OperationCanceledException();
                }

                if( Flag.IsSet( types, DataType.Nanos ) )
                {
                    // Rip Nanos
                    int count = 0;
                    int total = 0;
                    int[] nanos = rdb.GetRecordIds( ResourceDatabase.NanoType );
                    Log.Debug( "[Database.Extract] Ripping nanos: {0}.", nanos.Length );
                    progressForm.Status = "Extracting Nano Programs from the Database";
                    task = progressForm.AddTask( "Nano Programs extracted", nanos.Length, 8 );
                    refresh /= 2;
                    Application.DoEvents();
                    if( progressForm.Cancelled )
                        throw new OperationCanceledException();
                    foreach( int id in nanos )
                    {
                        byte[] data = rdb.GetRecordData( ResourceDatabase.NanoType, id );
                        using( AONano nano = new AONano( id, data ) )
                        {
                            db.Nanos.Add( nano.Id, nano );
                            nano.Save( db );
                            RaiseItemExtracted( db, nano );
                        }
                        Application.DoEvents();
                        if( progressForm.Cancelled )
                            throw new OperationCanceledException();
                        count++;
                        total++;
                        if( count >= refresh )
                        {
                            task.Value += count;
                            count = 0;
                        }
                    }
                    task.Finish();
                    Application.DoEvents();
                    if( progressForm.Cancelled )
                        throw new OperationCanceledException();
                }

                if( Flag.IsSet( types, DataType.Icons ) )
                {
                    // Rip Icons
                    int count = 0;
                    int total = 0;
                    int[] icons = rdb.GetRecordIds( ResourceDatabase.IconType );
                    Log.Debug( "[Database.Extract] Ripping icons: {0}.", icons.Length );
                    progressForm.Status = "Extracting icons from Database";
                    task = progressForm.AddTask( "Icons extracted", icons.Length, 6 );
                    refresh /= 5;
                    Application.DoEvents();
                    if( progressForm.Cancelled )
                        throw new OperationCanceledException();
                    foreach( int id in icons )
                    {
                        byte[] data = rdb.GetRecordData( ResourceDatabase.IconType, id );
                        AOIcon.Save( id, data, db );
                        Application.DoEvents();
                        if( progressForm.Cancelled )
                            throw new OperationCanceledException();
                        count++;
                        total++;
                        if( count >= refresh )
                        {
                            task.Value += count;
                            count = 0;
                        }
                    }
                    task.Finish();
                    Application.DoEvents();
                    if( progressForm.Cancelled )
                        throw new OperationCanceledException();
                }

                Log.Debug( "[Database.Extract] Commiting transaction." );
                progressForm.Status = "Cleaning up...";
                task = progressForm.AddTask( "Finalizing...", 4, 1, false );
                Application.DoEvents();
                string version = DatabaseManager.GetVersion( path );
                db.Insert( "localdb", "version", version );
                db.Insert( "localdb", "path", path );
                Application.DoEvents();
                if( progressForm.Cancelled )
                    throw new OperationCanceledException();
                db.CommitTransaction();

                task.Value++;
                task.Text = "Replacing old database...";
                Application.DoEvents();
                if( progressForm.Cancelled )
                    throw new OperationCanceledException();
                progressForm.CanBeCancelled = false; // at this point canceling is pointless.
                if( IsLoaded )
                    Close();
                RaiseDatabaseClosing( db );
                db.Close();
                if( File.Exists( FileName ) )
                    File.Delete( FileName );
                File.Move( db.FileName, FileName );

                task.Value++;
                task.Text = "Reinitializing database...";
                Application.DoEvents();
                Open(); // reopen db...

                sw.Stop();
                Log.Debug( "[Database.Extract] Done. Time elapsed: {0}", sw.Elapsed );
                string elapsed = "Finished! (In ";
                if( sw.Elapsed.Hours > 0 )
                    elapsed += sw.Elapsed.Hours.ToString() + " Hours, ";
                int sec = sw.Elapsed.Seconds;
                if( sw.Elapsed.Milliseconds > 500 )
                    sec++;
                elapsed += String.Format( "{0} Minutes and {1} Seconds)", sw.Elapsed.Minutes, sec );
                task.Text = elapsed;
                task.Finish();
            }
            catch( OperationCanceledException )
            {
                sw.Stop();
                Log.Debug( "[Database.Extract] Aborted! Time elapsed: {0}", sw.Elapsed );
                if( db != null )
                {
                    RaiseDatabaseClosing( db );
                    db.Close();
                    File.Delete( db.FileName );
                }
            }
            progressForm.Finish();
        }

        /// <summary>
        /// Retrieves the value of the <paramref name="key"/> from the ripped AO database contained in the database.
        /// </summary>
        /// <param name="key">The key to find.</param>
        /// <returns>A string containing the value of the key, if found; otherwise an empty string.</returns>
        public string GetValue( string key )
        {
            return GetValue( key, FileName );
        }

        /// <summary>
        /// Retrieves the value of the <paramref name="key"/> from the ripped AO database contained in the database.
        /// </summary>
        /// <param name="key">The key to find.</param>
        /// <param name="fileName">The database filename.</param>
        /// <returns>A string containing the value of the key, if found; otherwise an empty string.</returns>
        public string GetValue( string key, string fileName )
        {
            bool loaded = false;
            if( !IsLoaded && File.Exists( fileName ) )
            {
                Open( fileName );
                loaded = true;
            }

            SQLiteResult row = QuerySingle( "SELECT value FROM 'localdb' WHERE key='{0}'", key );
            if( loaded )
                Close();

            return row.Count == 0 ? String.Empty : Convert.ToString( row["value"] );
        }

#endregion
    }

#region EventArgs classes

    public class DatabaseEventArgs : EventArgs
    {
        public DatabaseEventArgs( Database database )
        {
            m_database = database;
        }

        readonly Database m_database;

        public Database Database
        {
            get { return m_database; }
        }
    }

    public class ItemExtractedEventArgs : DatabaseEventArgs
    {
        public ItemExtractedEventArgs( Database database, AOItem item )
            : base( database )
        {
            m_item = item;
        }

        readonly AOItem m_item;

        public AOItem Item
        {
            get { return m_item; }
        }
    }

#endregion
}
