﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Runtime.Serialization;

namespace Twp.Data.AO
{
    /// <summary>
    /// Represents errors that occur during Database queries.
    /// </summary>
    [Serializable]
    public class DatabaseException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AOCrafter.Core.DatabaseException"/> class.
        /// </summary>
        public DatabaseException() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AOCrafter.Core.DatabaseException"/> class with a specified error message.
        /// </summary>
        public DatabaseException( string message ) : base( message )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AOCrafter.Core.DatabaseException"/> class with a specified error message
        /// and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        public DatabaseException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AOCrafter.Core.DatabaseException"/> class with serialized data.
        /// </summary>
        public DatabaseException( SerializationInfo info, StreamingContext context ) : base( info, context )
        {
        }
    }
}
