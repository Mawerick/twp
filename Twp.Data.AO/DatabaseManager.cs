﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Data.AO
{
    /// <summary>
    ///
    /// </summary>
    public static class DatabaseManager
    {
#region Fields

        private static string m_fileName = "AOItems.db3";
        private static string m_dataPath;
        private static Database m_itemsDatabase;

#endregion

#region Properties

        /// <summary>
        /// Gets or sets the path to the folder containing the database.
        /// </summary>
        public static string DataPath
        {
            get { return m_dataPath; }
            set { m_dataPath = value; }
        }

        /// <summary>
        /// Gets the database used for items and icons.
        /// </summary>
        public static Database ItemsDatabase
        {
            get
            {
                if( m_itemsDatabase == null )
                    m_itemsDatabase = new Database();
                return m_itemsDatabase;
            }
        }

        public static string FullPath
        {
            get
            {
                if( String.IsNullOrEmpty( m_dataPath ) )
                    return m_fileName;
                return System.IO.Path.Combine( m_dataPath, m_fileName );
            }
        }

#endregion

#region Methods

        /// <summary>
        /// Creates and opens the database.
        /// </summary>
        /// <param name="fileName">The name of the database file.</param>
        public static void Open( string fileName )
        {
            m_fileName = fileName;
            Open();
        }

        /// <summary>
        /// Creates and opens the database.
        /// </summary>
        public static void Open()
        {
            if( !ItemsDatabase.IsLoaded && File.Exists( FullPath ) )
                ItemsDatabase.Open( FullPath );
        }

        /// <summary>
        /// Closes the databases
        /// </summary>
        public static void Close()
        {
            if( m_itemsDatabase != null )
                m_itemsDatabase.Close();
        }

        /// <summary>
        /// Checks if the current database version matches the AO install at <paramref name="path"/>.
        /// If they don't, the user is asked to update.
        /// </summary>
        /// <param name="path">The base path to a valid Anarchy Online installation.</param>
        public static void UpdateIfOutdated( string path )
        {
            string message;
            if( IsDbOutdated( path, out message ) )
            {
                DialogResult dr = MessageBox.Show( message, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
                if( dr == DialogResult.Cancel )
                    Application.Exit();
                else if( dr == DialogResult.Yes )
                {
//                    Open();
                    ItemsDatabase.Extract( path, 1000, DataType.All );
//                    Close();
                }
            }
        }

        /// <summary>
        /// Checks if the current database version matches the AO install at <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The base path to a valid Anarchy Online installation.</param>
        /// <param name="message">A string containing a message that can be shown to the user, to indicate why the databases don't match.</param>
        /// <returns>true if the databases don't match; otherwise false.</returns>
        public static bool IsDbOutdated( string path, out string message )
        {
            FileInfo local = new FileInfo( FullPath );
            Log.Debug( "[DatabaseManager.IsDbOutdated] Local \"{0}\" Exists? {1}", local.FullName, local.Exists );
            if( !local.Exists )
            {
                message = "Your items database is missing. Would you like to create it now?";
                return true;
            }

            string dbPath = Twp.Utilities.Path.Combine( path, "cd_image", "data", "db" );
            DirectoryInfo dbInfo = new DirectoryInfo( dbPath );
            Log.Debug( "[DatabaseManager.IsDbOutdated] Db Path \"{0}\" Exists? {1}", dbInfo.FullName, dbInfo.Exists );
            if( !dbInfo.Exists )
                Log.Fatal( "Failed to locate AO database folder at {0}", path );

            string localVersion = ItemsDatabase.GetValue( "version", FullPath );
            string remoteVersion = GetVersion( path );
            message = String.Format( "Your items database is out of date. Would you like to update it now?\n\nYour version: {0}\nAO version: {1}", localVersion, remoteVersion );
            foreach( FileInfo aoDb in dbInfo.GetFiles( "ResearchDatabase.*" ) )
            {
                Log.Debug( "[DatabaseManager.IsDbOutdated] aoDb.LastWrite={0}, Local.LastWrite={1}", aoDb.LastWriteTimeUtc, local.LastWriteTimeUtc );
                if( aoDb.LastWriteTimeUtc > local.LastWriteTimeUtc )
                    return true;
            }

            Log.Debug( "[DatabaseManager.IsDbOutdated] localVersion={0}, remoteVersion={1}", localVersion, remoteVersion );
            if( localVersion != remoteVersion )
                return true;

            return false;
        }

        /// <summary>
        /// Retrieves the version of the AO installation found at <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The base path to a valid Anarchy Online installation.</param>
        /// <returns>A string containing the AO version.</returns>
        public static string GetVersion( string path )
        {
            string fileName = System.IO.Path.Combine( path, "version.id" );
            return File.Exists( fileName ) ? File.ReadAllText( fileName ).Trim() : String.Empty;
        }

#endregion
    }
}
