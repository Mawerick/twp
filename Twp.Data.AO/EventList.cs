﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Data.AO
{
    public class EventList : Dictionary<int, FunctionList>
    {
        private ItemInfo owner;
        
        public EventList( ItemInfo owner ) : base()
        {
            this.owner = owner;
        }

        public new FunctionList this[int key]
        {
            get
            {
                if( !this.ContainsKey( key ) )
                    base.Add( key, new FunctionList( this.owner ) );
                return base[key];
            }
            set
            {
                if( this.ContainsKey( key ) )
                    base[key] = value;
                else
                    base.Add( key, value );
            }
        }

        public string ToHtml( bool raw )
        {
            if( this.Count <= 0 )
                return null;

            string htmlEvent = String.Empty;
            foreach( KeyValuePair<int, FunctionList> evt in this )
            {
                if( evt.Value.Count <= 0 )
                    continue;

                string htmlTarget = String.Empty;
                foreach( KeyValuePair<int, List<Function>> target in evt.Value )
                {
                    if( target.Value.Count <= 0 )
                        continue;

                    string htmlFunc = String.Empty;
                    foreach( Function func in target.Value )
                    {
                        htmlFunc += func.ToHtml( raw );
                    }
                    if( !String.IsNullOrEmpty( htmlFunc ) )
                        htmlTarget += String.Format( "<div class='indent'>{0}{1}</div><br>",
                                                    ItemFormatter.GetTarget( target.Key, this.owner.Mmdb ), htmlFunc );
                }
                if( !String.IsNullOrEmpty( htmlTarget ) )
                    htmlEvent += String.Format( "<div class='normal'>{0}{1}</div>", ItemFormatter.GetEvent( evt.Key ), htmlTarget );
            }
            if( !String.IsNullOrEmpty( htmlEvent ) )
                return "<div class='header'>Modifiers:" + htmlEvent + "</div>";
            return null;
        }
    }
}
