﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using Twp.Utilities;

namespace Twp.Data.AO
{
    public class Function
    {
        #region Constructor

        public Function(ItemInfo owner)
        {
            mRequirements = new RequirementList(owner);
            mOwner = owner;
        }

        public Function(int key, int target, object[] arguments, ItemInfo owner)
            : this(owner)
        {
            mKey = key;
            mTarget = target;
            mArguments = arguments;
        }

        public Function(int key, int target, int tickCount, int tickInterval, object[] arguments, ItemInfo owner)
            : this(key, target, arguments, owner)
        {
            mTickCount = tickCount;
            mTickInterval = tickInterval;
        }

        #endregion

        #region Private fields

        private int mKey;
        private int mTarget;
        private int mTickCount;
        private int mTickInterval;
        private object[] mArguments;
        private RequirementList mRequirements;
        private ItemInfo mOwner;

        #endregion

        #region Properties

        public int Key
        {
            get { return mKey; }
            set { mKey = value; }
        }

        public int Target
        {
            get { return mTarget; }
            set { mTarget = value; }
        }

        public int TickCount
        {
            get { return mTickCount; }
            set { mTickCount = value; }
        }

        public int TickInterval
        {
            get { return mTickInterval; }
            set { mTickInterval = value; }
        }

        public object[] Arguments
        {
            get { return mArguments; }
            set { mArguments = value; }
        }

        public RequirementList Requirements
        {
            get { return mRequirements; }
        }

        #endregion

        #region Methods

        public string ToHtml(bool raw)
        {
            string args;
            if (raw)
            {
                args = String.Format("{0} ({1})", ItemFormatter.GetFunction(mKey), mKey);
                if (mArguments != null)
                {
                    args += String.Format(" Args({0}) ", mArguments.Length);
                    foreach (object arg in mArguments)
                    {
                        if (arg is byte[])
                        {
                            foreach (byte b in (byte[])arg)
                            {
                                args += Convert.ToString(b);
                            }
                            args += ", ";
                        }
                        else
                            args += Convert.ToString(arg) + ", ";
                    }
                    args = args.TrimEnd(' ', ',');
                }
            }
            else
            {
                args = FormatFunctionArgs("{0}<b> {1} </b><span class='headline'>{2}</span>");
                if (String.IsNullOrEmpty(args))
                    return null;
            }

            string html = "<div class='indent'>" + args;
            if (mTickCount > 1)
                html += String.Format("<span class='tick'> {0} times, once every {1}.</span>", mTickCount, ItemFormatter.FormatTime(Convert.ToDouble(mTickInterval) / 100));
            if (mRequirements.Count > 0)
            {
                html += " <i>if</i>";
                string htmlReqs = mRequirements.ToHtml(raw);
                if (!String.IsNullOrEmpty(htmlReqs))
                    html += String.Format("<div class='indent'>{0}</div>", htmlReqs);
            }
            html += "</div>";
            return html;
        }

        public override string ToString()
        {
            string args = FormatFunctionArgs("{0} {1} {2}");
            if (String.IsNullOrEmpty(args))
                return null;
            if (mTickCount > 1)
                args += String.Format(" {0} times, once every {1}.", mTickCount, ItemFormatter.FormatTime(Convert.ToDouble(mTickInterval) / 100));
            return Text.StripHtmlTags(args);
        }

        private string FormatFunctionArgs(string format)
        {
            switch ((FunctionKey)mKey)
            {
                case FunctionKey.Attractor_Effect:
                case FunctionKey.Attractor_Effect_2:
                case FunctionKey.Attractor_Effect_3:
                case FunctionKey.Attractor_Mesh:
                case FunctionKey.Back_Mesh:
                case FunctionKey.Head_Mesh:
                case FunctionKey.Shoulder_Mesh:
                case FunctionKey.Texture:
                case FunctionKey.Gfx_Effect:
                case FunctionKey.Monster_Shape:
                case FunctionKey.Restrict_Action:
                case FunctionKey.Change_Body_Mesh:
                    // We ignore these functions...
                    return null;

                case FunctionKey.Fear:
                case FunctionKey.Pacify:
                    // These have no arguments, and a readable function key.
                    return ItemFormatter.GetFunction(mKey);

                case FunctionKey.Can_Fly:
                    return "Enables flight.";

                case FunctionKey.NPC_Wipe_Hatelist:
                    return "Wipe Hatelist";

                case FunctionKey.Taunt_NPC:
                    return "Taunt " + Convert.ToInt32(mArguments[0]);

                case FunctionKey.Add_Skill:
                    //return String.Format( format, GetFunction( funcNum ), GetAttr( Convert.ToInt32( arguments[1] ) ), arguments[0] );
                    return null;

                case FunctionKey.Special_Hit:
                case FunctionKey.Hit:
                    int val1 = Convert.ToInt32(mArguments[1]);
                    switch ((AttributeKey)mArguments[0])
                    {
                        case AttributeKey.Credits:
                            if (val1 < 0)
                                return String.Format("Charge <span class='headline'>{0}</span> credits.", ItemFormatter.FormatCredits(val1));
                            else
                                return String.Format("Reward <span class='headline'>{0}</span> credits.", ItemFormatter.FormatCredits(val1));
                        case AttributeKey.MapUpgrades:
                            return String.Format("Add map upgrade <b>{0}</b>.", ItemFormatter.CleanEnum((MapUpgrade)val1));
                        case AttributeKey.MapFlags1:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapA)val1));
                        case AttributeKey.MapFlags2:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapB)val1));
                        case AttributeKey.MapFlags3:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapC)val1));
                        case AttributeKey.MapFlags4:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapD)val1));
                        default:
                            bool damage = false;
                            int val2 = Convert.ToInt32(mArguments[2]);
                            int val3 = Convert.ToInt32(mArguments[3]);
                            string func = ItemFormatter.GetFunction(mKey);
                            string attr = ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb);
                            if (val1 < 0 && val2 < 0)
                            {
                                val1 *= -1;
                                val2 *= -1;
                                damage = true;
                            }
                            string hit = val1.ToString();
                            if (val2 > val1)
                                hit += " - " + val2.ToString();
                            if (val2 == 0)
                                hit += '%';
                            if (val1 > 0 && !damage)
                            {
                                if (val3 > 0)
                                    hit += ' ' + val3.ToString() + " times";

                                switch ((AttributeKey)mArguments[0])
                                {
                                    case AttributeKey.Health:
                                        func = "";
                                        attr = mOwner.Mmdb.GetString(1005, 185789767);
                                        break;
                                    case AttributeKey.CurrentNano:
                                        func = "";
                                        attr = mOwner.Mmdb.GetString(1005, 75839476);
                                        break;
                                }
                            }
                            else
                            {
                                if (val3 > 0)
                                    hit += ' ' + mOwner.Mmdb.GetString(2003, val3);

                                switch ((AttributeKey)mArguments[0])
                                {
                                    case AttributeKey.Health:
                                        func = "";
                                        attr = mOwner.Mmdb.GetString(1005, 16669669);
                                        break;
                                    case AttributeKey.CurrentNano:
                                        func = "";
                                        attr = mOwner.Mmdb.GetString(1005, 76477381);
                                        break;
                                }
                            }

                            return String.Format(format, func, attr, hit);
                    }

                case FunctionKey.Set:
                    switch ((AttributeKey)Convert.ToInt32(mArguments[0]))
                    {
                        case AttributeKey.ShadowBreedTemplate:
                            return String.Format("Set <b>{0}</b> to <span class='headline'>{1}</span>",
                                                 ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb),
                                                 ItemFormatter.GetItemLink(mArguments[1], mOwner.Database));
                        default:
                            return String.Format(format, "Set",
                                                 ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb),
                                                 mArguments[1]);
                    }

                case FunctionKey.Skill:
                case FunctionKey.Modify:
                    switch ((AttributeKey)Convert.ToInt32(mArguments[0]))
                    {
                        case AttributeKey.CriticalIncrease:
                        case AttributeKey.HealEfficiency:
                        case AttributeKey.NanoCost:
                        case AttributeKey.XPModifier:
                        case AttributeKey.Scale:
                            return String.Format(format, "Modify",
                                                 ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb),
                                                 Convert.ToString(mArguments[1]) + "%");
                        default:
                            return String.Format(format, "Modify",
                                                 ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb),
                                                 mArguments[1]);
                    }

                case FunctionKey.Timed_Effect:
                    return String.Format("<b>{0}</b> temp set to <span class='headline'>{1}</span> for {2}",
                                         ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb), mArguments[1],
                                         ItemFormatter.FormatTime(Convert.ToInt32(mArguments[2])));

                case FunctionKey.Change_Variable:
                    return String.Format("<b>{0}</b> temp set to <span class='headline'>{1}</span>",
                                         ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb),
                                         Convert.ToInt32(mArguments[1]));

                case FunctionKey.Lock_Skill:
                    return String.Format("<b>{0}</b> skill locked for {1}.",
                                         ItemFormatter.GetAttr(Convert.ToInt32(mArguments[0]), mOwner.Mmdb),
                                         ItemFormatter.FormatTime(Convert.ToInt32(mArguments[1])));

                case FunctionKey.Head_Text:
                    return String.Format("Head Text: <b>&quot;{0}&quot;</b> for {1}", Convert.ToString(mArguments[0]),
                                         ItemFormatter.FormatTime(Convert.ToInt32(mArguments[1])));

                case FunctionKey.System_Text:
                case FunctionKey.System_Text_2:
                    return String.Format("System Text: <b>&quot;{0}&quot;</b>", Convert.ToString(mArguments[0]));

                case FunctionKey.Cast_Nano:
                case FunctionKey.Cast_Nano_2:
                    return String.Format("Cast {0}", ItemFormatter.GetNanoLink(mArguments[0], mOwner.Database));

                case FunctionKey.Team_Cast_Nano:
                    return String.Format("Cast {0} on team.", ItemFormatter.GetNanoLink(mArguments[0], mOwner.Database));

                case FunctionKey.Pets_Cast_Nano:
                    return String.Format("Cast {0} on pets.", ItemFormatter.GetNanoLink(mArguments[0], mOwner.Database));

                case FunctionKey.Area_Cast_Nano:
                    return String.Format("Cast {0}<b> in a {1}m radius.</b>",
                                         ItemFormatter.GetNanoLink(mArguments[0], mOwner.Database),
                                         Convert.ToString(mArguments[1]));

                case FunctionKey.Cast_Stun_Nano:
                    return String.Format("Cast stun {0}<b> {1}% chance.</b>",
                                         ItemFormatter.GetNanoLink(mArguments[0], mOwner.Database),
                                         Convert.ToString(mArguments[1]));

                case FunctionKey.Upload_Nano:
                    return String.Format("Upload {0}", ItemFormatter.GetNanoLink(mArguments[0], mOwner.Database));

                case FunctionKey.Resist_Nano_Strain:
                    return String.Format(format, "Resist", mOwner.Mmdb.GetString(2009, Convert.ToInt32(mArguments[0])),
                                         Convert.ToString(mArguments[1]) + "%");

                case FunctionKey.Remove_Nano_Strain:
                    return String.Format("Remove all <b>{0}</b> nanos",
                                         mOwner.Mmdb.GetString(2009, Convert.ToInt32(mArguments[0])));

                case FunctionKey.Reduce_Nano_Strain_Duration:
                    return String.Format("Reduce <b>{0}</b> nanos by {1}",
                                         mOwner.Mmdb.GetString(2009, Convert.ToInt32(mArguments[0])),
                                         ItemFormatter.FormatTime(Convert.ToInt32(mArguments[1])));

                case FunctionKey.Spawn_Item:
                    string duration = ItemFormatter.FormatTime(Convert.ToInt32(mArguments[2]));
                    if (!String.IsNullOrEmpty(duration))
                        duration = " for " + duration;
                    return String.Format("Spawn QL {0} <b>{1}</b>{2}", mArguments[1], mArguments[0], duration);

                case FunctionKey.Destroy_Item:
                    return "Destroy item.";

                case FunctionKey.Lock_Perk:
                    return String.Format("Lock perk <b>{0} {1}</b> for {2}", mArguments[1], mArguments[0],
                                         ItemFormatter.FormatTime(Convert.ToInt32(mArguments[2])));

                case FunctionKey.Add_Defense_Proc:
                    return String.Format("Defencive Proc {0} ({1}% chance to activate)",
                                         ItemFormatter.GetNanoLink(mArguments[1], mOwner.Database), mArguments[0]);

                case FunctionKey.Add_Action:
                    return String.Format("Add Action {0}", ItemFormatter.GetItemLink(mArguments[3], mOwner.Database));

                case FunctionKey.Set_Flag:
                    int attrKey = Convert.ToInt32(mArguments[0]);
                    int attrVal = Convert.ToInt32(mArguments[1]);
                    switch ((AttributeKey)attrKey)
                    {
                        case AttributeKey.MapUpgrades:
                            return String.Format("Add map upgrade <b>{0}</b>.", ItemFormatter.CleanEnum((MapUpgrade)(1 << attrVal)));
                        case AttributeKey.MapFlags1:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapA)(1 << attrVal)));
                        case AttributeKey.MapFlags2:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapB)(1 << attrVal)));
                        case AttributeKey.MapFlags3:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapC)(1 << attrVal)));
                        case AttributeKey.MapFlags4:
                            return String.Format("Add map <b>{0}</b>.", ItemFormatter.CleanEnum((MapD)(1 << attrVal)));
                        default:
                            return String.Format(format, "Set Flag",
                                                 ItemFormatter.GetAttr(attrKey, mOwner.Mmdb),
                                                 ItemFormatter.GetAttrValue(attrKey, (int)OperatorKey.Default, attrVal, mOwner.Mmdb));
                    }

                case FunctionKey.Summon_Pet:
                    return "Warp targeted pet to current position.";

                case FunctionKey.Summon_Pets:
                    return "Warp all pets to current position.";

                case FunctionKey.Summon_Player:
                    return "Warp targeted team mate to current position.";

                case FunctionKey.Summon_Team_Mates:
                    return "Warp all team mates to current position.";

                case FunctionKey.Teleport:
                    return String.Format("Teleport to <b>{0}</b>", Playfields.GetName(Convert.ToInt32(mArguments[3])));

                case FunctionKey.Teleport_Proxy:
                case FunctionKey.Teleport_Proxy_2:
                    return String.Format("Teleport to <b>{0}</b>", Playfields.GetName(Convert.ToInt32(mArguments[1])));

                case FunctionKey.Teleport_To_Save_Point:
                    return "Teleport to <b>Last save point</b>";

                case FunctionKey.Set_Anchor:
                    return "Set Anchor";

                default:
                    string text = ItemFormatter.GetFunction(mKey);
                    if (mArguments != null)
                    {
                        text += " Args( " + mArguments.Length.ToString() + " ) ";
                        foreach (object arg in mArguments)
                            text += Convert.ToString(arg) + ", ";
                        text = text.TrimEnd(' ', ',');
                    }
                    return text;
            }
        }

        #endregion
    }
}
