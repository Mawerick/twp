﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Data.AO
{
    public class FunctionList : Dictionary<int, List<Function>>
    {
        private ItemInfo owner;
        
        public FunctionList( ItemInfo owner ) : base()
        {
            this.owner = owner;
        }

        public new List<Function> this[int key]
        {
            get
            {
                if( !this.ContainsKey( key ) )
                    base.Add( key, new List<Function>() );
                return base[key];
            }
            set
            {
                if( this.ContainsKey( key ) )
                    base[key] = value;
                else
                    base.Add( key, value );
            }
        }
    }
}
