﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of ItemSet.
    /// </summary>
    public class IAItem : Item
    {
        #region Constructor

        public IAItem()
        {
            this.id = -1;
            this.owner = 0;
            this.parent = 0;
            this.children = 0;
            this.location = "";
        }

        public IAItem( SQLiteResult row )
        {
            this.id = Convert.ToInt32( row["itemidx"] );
            this.keyLow = Convert.ToInt32( row["keylow"] );
            this.keyHigh = Convert.ToInt32( row["keyhigh"] );
            this.ql = Convert.ToInt32( row["ql"] );
            this.owner = Convert.ToUInt32( row["owner"] );
            this.parent = Convert.ToInt32( row["parent"] );
            this.children = Convert.ToInt32( row["children"] );
            this.location = ItemAssistant.GetLocation( this.parent, Convert.ToInt32( row["slot"] ) );
        }
        
        #endregion
        
        #region Private fields

        private readonly int id;
        private readonly uint owner;
        private readonly int parent;
        private readonly int children;
        private readonly string location;

        #endregion

        #region Properties

        public int Id
        {
            get { return this.id; }
        }

        public uint Owner
        {
            get { return this.owner; }
        }

        public int Parent
        {
            get { return this.parent; }
        }

        public int Children
        {
            get { return this.children; }
        }

        public string Location
        {
            get { return this.location; }
        }

        #endregion
    }
}
