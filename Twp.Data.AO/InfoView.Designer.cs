﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace Twp.Data.AO
{
	partial class InfoView
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
		    this.toolStrip = new System.Windows.Forms.ToolStrip();
		    this.forwardButton = new System.Windows.Forms.ToolStripButton();
		    this.backButton = new System.Windows.Forms.ToolStripButton();
		    this.linkLabel = new System.Windows.Forms.ToolStripLabel();
		    this.qlLabel = new System.Windows.Forms.ToolStripLabel();
		    this.qlSlider = new Twp.Controls.ToolStripSlider();
		    this.htmlPanel = new Twp.Controls.HtmlRenderer.HtmlPanel();
		    this.toolStrip.SuspendLayout();
		    this.SuspendLayout();
		    // 
		    // toolStrip
		    // 
		    this.toolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
		    this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
		    this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
		    		    		    this.forwardButton,
		    		    		    this.backButton,
		    		    		    this.linkLabel,
		    		    		    this.qlLabel,
		    		    		    this.qlSlider});
		    this.toolStrip.Location = new System.Drawing.Point(0, 341);
		    this.toolStrip.Name = "toolStrip";
		    this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
		    this.toolStrip.Size = new System.Drawing.Size(384, 25);
		    this.toolStrip.Stretch = true;
		    this.toolStrip.TabIndex = 1;
		    this.toolStrip.Text = "toolStrip";
		    // 
		    // forwardButton
		    // 
		    this.forwardButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
		    this.forwardButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
		    this.forwardButton.Image = global::Twp.Data.AO.Properties.Resources.ForwardButton;
		    this.forwardButton.ImageTransparentColor = System.Drawing.Color.Magenta;
		    this.forwardButton.Name = "forwardButton";
		    this.forwardButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
		    this.forwardButton.Size = new System.Drawing.Size(23, 22);
		    this.forwardButton.Text = "&Forward";
		    this.forwardButton.Click += new System.EventHandler(this.ForwardButtonClick);
		    // 
		    // backButton
		    // 
		    this.backButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
		    this.backButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
		    this.backButton.Image = global::Twp.Data.AO.Properties.Resources.BackButton;
		    this.backButton.ImageTransparentColor = System.Drawing.Color.Magenta;
		    this.backButton.Name = "backButton";
		    this.backButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
		    this.backButton.Size = new System.Drawing.Size(23, 22);
		    this.backButton.Text = "&Back";
		    this.backButton.Click += new System.EventHandler(this.BackButtonClick);
		    // 
		    // linkLabel
		    // 
		    this.linkLabel.Name = "linkLabel";
		    this.linkLabel.Size = new System.Drawing.Size(0, 22);
		    // 
		    // qlLabel
		    // 
		    this.qlLabel.Name = "qlLabel";
		    this.qlLabel.Size = new System.Drawing.Size(25, 22);
		    this.qlLabel.Text = "QL:";
		    this.qlLabel.Visible = false;
		    // 
		    // qlSlider
		    // 
		    this.qlSlider.Maximum = new decimal(new int[] {
		    		    		    100,
		    		    		    0,
		    		    		    0,
		    		    		    0});
		    this.qlSlider.Minimum = new decimal(new int[] {
		    		    		    0,
		    		    		    0,
		    		    		    0,
		    		    		    0});
		    this.qlSlider.Name = "qlSlider";
		    this.qlSlider.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
		    this.qlSlider.PageStep = new decimal(new int[] {
		    		    		    10,
		    		    		    0,
		    		    		    0,
		    		    		    0});
		    this.qlSlider.Size = new System.Drawing.Size(270, 22);
		    this.qlSlider.Step = new decimal(new int[] {
		    		    		    1,
		    		    		    0,
		    		    		    0,
		    		    		    0});
		    this.qlSlider.TickFrequency = new decimal(new int[] {
		    		    		    5,
		    		    		    0,
		    		    		    0,
		    		    		    0});
		    this.qlSlider.Value = new decimal(new int[] {
		    		    		    0,
		    		    		    0,
		    		    		    0,
		    		    		    0});
		    this.qlSlider.Visible = false;
		    this.qlSlider.ValueChanged += new System.EventHandler(this.QlSliderValueChanged);
		    // 
		    // htmlPanel
		    // 
		    this.htmlPanel.AutoScroll = true;
		    this.htmlPanel.AvoidGeometryAntialias = false;
		    this.htmlPanel.AvoidImagesLateLoading = false;
		    this.htmlPanel.BackColor = System.Drawing.Color.Black;
		    this.htmlPanel.BaseStylesheet = null;
		    this.htmlPanel.Cursor = System.Windows.Forms.Cursors.IBeam;
		    this.htmlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
		    this.htmlPanel.Location = new System.Drawing.Point(0, 0);
		    this.htmlPanel.Name = "htmlPanel";
		    this.htmlPanel.Size = new System.Drawing.Size(384, 341);
		    this.htmlPanel.TabIndex = 2;
		    this.htmlPanel.LinkClicked += new System.EventHandler<Twp.Controls.HtmlRenderer.Entities.HtmlLinkClickedEventArgs>(this.HtmlPanelLinkClicked);
		    this.htmlPanel.RenderError += new System.EventHandler<Twp.Controls.HtmlRenderer.Entities.HtmlRenderErrorEventArgs>(this.HtmlPanelRenderError);
		    this.htmlPanel.ImageLoad += new System.EventHandler<Twp.Controls.HtmlRenderer.Entities.HtmlImageLoadEventArgs>(this.HtmlPanelImageLoad);
		    // 
		    // InfoView
		    // 
		    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		    this.Controls.Add(this.htmlPanel);
		    this.Controls.Add(this.toolStrip);
		    this.Name = "InfoView";
		    this.Size = new System.Drawing.Size(384, 366);
		    this.toolStrip.ResumeLayout(false);
		    this.toolStrip.PerformLayout();
		    this.ResumeLayout(false);
		    this.PerformLayout();
		}
		private System.Windows.Forms.ToolStripLabel qlLabel;
		private Twp.Controls.ToolStripSlider qlSlider;
		private Twp.Controls.HtmlRenderer.HtmlPanel htmlPanel;
		private System.Windows.Forms.ToolStripLabel linkLabel;
		private System.Windows.Forms.ToolStripButton backButton;
		private System.Windows.Forms.ToolStripButton forwardButton;
		private System.Windows.Forms.ToolStrip toolStrip;
	}
}
