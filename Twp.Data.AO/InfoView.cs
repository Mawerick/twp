﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Twp.Controls.HtmlRenderer.Entities;
using Twp.Utilities;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of InfoPreviewForm.
    /// </summary>
    public partial class InfoView : UserControl
    {
        public InfoView()
        {
            InitializeComponent();

            htmlPanel.BaseStylesheet = Properties.Resources.BaseCSS;
			mDatabase = DatabaseManager.ItemsDatabase;
        }

        List<object> mPages = new List<object>();
        int mCurrent = -1;
        Mmdb mMmdb;
        Database mDatabase;

        public Mmdb Mmdb
        {
            get { return mMmdb; }
            set { mMmdb = value; }
        }

        public Database Database
        {
            get { return mDatabase; }
            set { mDatabase = value; }
        }

        public void AddPage(object page)
        {
            if (!mPages.Contains(page))
            {
                if (mCurrent < 0 || mCurrent >= mPages.Count)
                    mPages.Add(page);
                else
                    mPages.Insert(mCurrent + 1, page);
            }
            mCurrent = mPages.IndexOf(page);
            UpdatePage();
        }

        public void Clear()
        {
            mCurrent = -1;
            mPages.Clear();
        }

        public void UpdatePage()
        {
            bool back = true;
            bool forward = true;
            if (mCurrent <= 0)
            {
                mCurrent = 0;
                back = false;
            }
            if (mCurrent >= mPages.Count - 1)
            {
                mCurrent = mPages.Count - 1;
                forward = false;
            }
            qlLabel.Visible = false;
            qlSlider.Visible = false;

            string content = String.Empty;
            if (mPages.Count > 0)
            {
                object page = mPages[mCurrent];
                ItemInfo info = null;
                if (page is ItemInfo)
                    info = page as ItemInfo;
                else if (page is AOItem)
                    info = ItemParser.Parse(page as AOItem);
                else if (page is AONano)
                    info = ItemParser.Parse(page as AONano);
                else if (page is Item)
                {
                    Item item = page as Item;
                    if (item.KeyLow != item.KeyHigh)
                    {
                        AOItem itemLow = AOItem.Load(item.KeyLow, mDatabase);
                        AOItem itemHigh = AOItem.Load(item.KeyHigh, mDatabase);
                        info = ItemParser.Interpolate(itemLow.Data, itemHigh.Data, item.Ql);

                        qlLabel.Visible = true;
                        qlSlider.Visible = true;
                        qlSlider.Minimum = itemLow.Ql;
                        qlSlider.Maximum = itemHigh.Ql;
                        qlSlider.Value = item.Ql;
                    }
                    else
                    {
                        AOItem itemLow = AOItem.Load(item.KeyLow, mDatabase);
                        info = ItemParser.Parse(itemLow);
                    }
                }

                if (info != null)
                {
                    info.Database = mDatabase;
                    info.Mmdb = mMmdb;
                    content = info.ToHtml();
                }
                else
                    content = page.ToString();
            }
            htmlPanel.Text = "<html><body>" + content + "</body></html>";
            backButton.Enabled = back;
            forwardButton.Enabled = forward;

            //Log.Debug( "[InfoPreviewForm.UpdatePage] Page:" );
            //Log.Debug( htmlPanel.GetHtml() );
        }

        void BackButtonClick(object sender, EventArgs e)
        {
            mCurrent--;
            UpdatePage();
        }

        void ForwardButtonClick(object sender, EventArgs e)
        {
            mCurrent++;
            UpdatePage();
        }

        void HtmlPanelRenderError(object sender, HtmlRenderErrorEventArgs e)
        {
            Twp.Utilities.Log.Debug("[InfoView.HtmlPanelRenderError] Type: {0} Message: {1}", e.Type, e.Message);
        }

        void HtmlPanelImageLoad(object sender, HtmlImageLoadEventArgs e)
        {
            int index = e.Src.IndexOf("://");
            if (index >= 0)
            {
                string scheme = e.Src.Substring(0, index).ToLower();
                string content = e.Src.Substring(index + 3).ToLower();
                if (scheme == "rdb")
                {
                    try
                    {
                        System.Drawing.Image image = AOIcon.Load(Convert.ToInt32(content), mDatabase).Image;
                        e.Callback(image, new System.Drawing.Rectangle(0, 0, image.Width, image.Height));
                        e.Handled = true;
                    }
                    catch (Exception)
                    {
                    }
                }
                else if (scheme == "resource" && content == "itembg")
                {
                    System.Drawing.Image image = Properties.Resources.BackgroundIcon;
                    e.Callback(image, new System.Drawing.Rectangle(0, 0, image.Width, image.Height));
                    e.Handled = true;
                }
            }
        }

        void HtmlPanelLinkClicked(object sender, HtmlLinkClickedEventArgs e)
        {
            int index = e.Link.IndexOf("://");
            if (index < 0)
                return;

            string scheme = e.Link.Substring(0, index).Trim();
            string content = e.Link.Substring(index + 3).Trim();

            if (String.IsNullOrEmpty(scheme) || String.IsNullOrEmpty(content))
                return;

            switch (scheme)
            {
                case "chatcmd":
                    string[] args = content.Split(' ');
                    if (args.Length < 2)
                        return;
                    if (args[0] == "/start")
                    {
                        e.Handled = true;
                        using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                        {
                            try
                            {
                                process.StartInfo.UseShellExecute = true;
                                process.StartInfo.FileName = String.Join(" ", args, 1, args.Length - 1);
                                process.Start();
                            }
                            catch (Exception ex)
                            {
                                Log.Debug("[InfoPreviewForm.HtmlPanelLinkClicked] {0}", ex.Message);
                            }
                        }
                    }
                    break;

                case "nanoref":
                    e.Handled = true;
                    int id = Convert.ToInt32(content);
                    AONano nano = AONano.Load(id, mDatabase);
                    if (nano != null)
                    {
                        ItemInfo info = ItemParser.Parse(nano);
                        info.Database = mDatabase;
                        info.Mmdb = mMmdb;
                        AddPage(info);
                    }
                    break;

                case "itemref":
                    e.Handled = true;
                    Item item = new Item(content);
                    AddPage(item);
                    break;
            }
        }

        void QlSliderValueChanged(object sender, EventArgs e)
        {
            Item item = mPages[mCurrent] as Item;
            if (item != null && item.KeyLow != item.KeyHigh)
            {
                AOItem itemLow = AOItem.Load(item.KeyLow, mDatabase);
                AOItem itemHigh = AOItem.Load(item.KeyHigh, mDatabase);
                ItemInfo info = ItemParser.Interpolate(itemLow.Data, itemHigh.Data, Decimal.ToInt32(qlSlider.Value));
                info.Database = mDatabase;
                info.Mmdb = mMmdb;
                htmlPanel.Text = "<html><body>" + info.ToHtml() + "</body></html>";
            }
            else
            {
                qlLabel.Visible = false;
                qlSlider.Visible = false;
            }
        }
    }
}
