﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of Item.
    /// </summary>
    public class Item
    {
        #region Constructors

        public Item()
        {
            this.ql = 0;
            this.keyLow = -1;
            this.keyHigh = -1;
            this.itemLow = null;
            this.itemHigh = null;
        }

        public Item( string itemref )
        {
            string[] parts = itemref.Split( '/' );
            this.keyLow = Convert.ToInt32( parts[0] );
            if( parts.Length != 3 )
            {
                this.keyHigh = this.KeyLow;
                this.ql = 0;
            }
            else
            {
                this.keyHigh = Convert.ToInt32( parts[1] );
                this.ql = Convert.ToInt32( parts[2] );
            }
            this.itemLow = null;
            this.itemHigh = null;
        }

        #endregion

        #region Private fields

        protected int ql;
        protected int keyLow;
        protected int keyHigh;
        protected AOItem itemLow;
        protected AOItem itemHigh;

        #endregion
        
        #region Properties
        
        public int Ql
        {
            get { return this.ql; }
            set { this.ql = value; }
        }

        public int KeyLow
        {
            get { return this.keyLow; }
            set { this.keyLow = value; }
        }

        public int KeyHigh
        {
            get { return this.keyHigh; }
            set { this.keyHigh = value; }
        }
        
        public AOItem ItemLow
        {
            get { return this.itemLow; }
            set { this.itemLow = value; }
        }
        
        public AOItem ItemHigh
        {
            get { return this.itemHigh; }
            set { this.itemHigh = value; }
        }

        #endregion

        #region Methods

        public AOItem Interpolate( Database db )
        {
            if( this.itemLow == null || this.itemLow.Id != this.keyLow )
                this.itemLow = AOItem.Load( this.keyLow, db );
            if( this.itemHigh == null || this.itemHigh.Id != this.keyHigh )
                this.itemHigh = AOItem.Load( this.keyHigh, db );

            if( itemLow == itemHigh )
                return itemLow;

            if( this.ql <= itemLow.Ql )
                return itemLow;

            if( this.ql >= itemHigh.Ql )
                return itemHigh;

            int midpoint = itemLow.Ql + ( ( itemHigh.Ql - itemLow.Ql ) / 2 );
            AOItem itemMid = ql >= midpoint ? itemHigh : itemLow;

            AOItem item = new AOItem();
            item.Ql = this.ql;
            item.Name = itemMid.Name;
            item.Icon = itemMid.Icon;
            item.Data = itemMid.Data;
            return item;
        }

        public override string ToString()
        {
            return String.Format( "itemref://{0}/{1}/{2}", this.keyLow, this.keyHigh, this.ql );
        }

        #endregion
    }
}
