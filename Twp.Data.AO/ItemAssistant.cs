﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Data.AO
{
	/// <summary>
	/// Description of ItemAssistant.
	/// </summary>
	public static class ItemAssistant
	{
		private static SQLiteDatabase itemDb = null;
		
		public static bool Open( string path )
		{
			if( itemDb != null )
				return true;
			
			if( String.IsNullOrEmpty( path ) )
				return false;
			
			Log.Debug( "[ItemAssistant.Open] Path: {0}", path );
			
			string fileName = Path.Combine( path, "ItemAssistant.db" );
			if( !System.IO.File.Exists( fileName ) )
				return false;
			
			itemDb = new SQLiteDatabase();
			itemDb.Open( fileName );
			return true;
		}
		
		public static void Close()
		{
			if( itemDb != null )
				itemDb.Close();
		}
		
		public static string GetCharacterName( uint id )
		{
			if( itemDb == null )
				throw new DatabaseException( "AOIA Database has not been initialized." );
			
			SQLiteResult row = itemDb.QuerySingle( "SELECT charname FROM `tToons` WHERE charid = '{0}' LIMIT 1", id.ToString() );
			if( row.Count <= 0 )
				return null;
			return Convert.ToString( row["charname"] );
		}
		
		private static string PageSql( IAPage page )
		{
			switch( page )
			{
				case IAPage.Bank:
					return " AND parent = 1";

				case IAPage.Weapons:
					return " AND parent = 2 AND slot < 16";

				case IAPage.Cloth:
					return " AND parent = 2 AND slot > 16 AND slot < 32";

				case IAPage.Implants:
					return " AND parent = 2 AND slot > 32 AND slot < 48";

				case IAPage.Social:
					return " AND parent = 2 AND slot > 48 AND slot < 64";

				case IAPage.Inventory:
					return " AND parent = 2 AND slot > 64";
					
				default:
					return "";
			}
		}
		
		internal static string GetLocation( int parent, int slot )
		{
			if( parent == 1 )
				return "Bank";
			else if( parent == 2 )
			{
				if( slot < 16 )
					return "Equiped (Weapons)";
				else if( slot < 32 )
					return "Equiped (Cloth)";
				else if( slot < 48 )
					return "Equiped (Implants)";
				else if( slot < 64 )
					return "Equiped (Social)";
				else
					return "Inventory";
			}
			return String.Format( "Unknown :: {0} :: {1}", parent, slot );
		}
		
		public static IAItem GetParent( int id )
		{
			if( itemDb == null )
				throw new DatabaseException( "AOIA Database has not been initialized." );

			SQLiteResult row = itemDb.QuerySingle( "SELECT itemidx, keylow, keyhigh, ql, owner, parent, children, slot FROM `tItems` WHERE children = '{0}'", id.ToString() );
			Application.DoEvents();
			return new IAItem( row );
		}
		
		public static List<IAItem> GetItems( uint charId, IAPage page )
		{
			if( itemDb == null )
				throw new DatabaseException( "AOIA Database has not been initialized." );

			List<IAItem> items = new List<IAItem>();
			SQLiteResults rows = itemDb.Query( "SELECT itemidx, keylow, keyhigh, ql, owner, parent, children, slot FROM `tItems` WHERE owner = '{0}'{1}", charId.ToString(), PageSql( page ) );
			Application.DoEvents();
			foreach( SQLiteResult row in rows )
			{
				items.Add( new IAItem( row ) );
				Application.DoEvents();
			}
			
			return items;
		}
		
		public static List<IAItem> GetItems( uint charId, int bagId )
		{
			if( itemDb == null )
				throw new DatabaseException( "AOIA Database has not been initialized." );

			List<IAItem> items = new List<IAItem>();
			SQLiteResults rows = itemDb.Query( "SELECT itemidx, keylow, keyhigh, ql, owner, parent, children, slot FROM `tItems` WHERE owner = '{0}' AND parent = '{1}'", charId.ToString(), bagId.ToString() );
			Application.DoEvents();
			foreach( SQLiteResult row in rows )
			{
				items.Add( new IAItem( row ) );
				Application.DoEvents();
			}
			
			return items;
		}

		public static List<IAItem> Search( int[] ids, int minQL, int maxQL, uint charId )
		{
			if( itemDb == null )
				throw new DatabaseException( "AOIA Database has not been initialized." );

			string sql = "SELECT itemidx, keylow, keyhigh, ql, owner, parent, children, slot FROM `tItems` WHERE (keylow = '{0}' OR keyhigh = '{0}')";
			if( minQL > 0 )
				sql += String.Format( " AND ql >= '{0}'", minQL );
			if( maxQL > 0 && maxQL >= minQL )
				sql += String.Format( " AND ql <= '{0}'", maxQL );
			if( charId > 0 )
				sql += String.Format( " AND owner = '{0}'", charId );

			List<IAItem> items = new List<IAItem>();
			foreach( int id in ids )
			{
				SQLiteResults rows = itemDb.Query( sql, id.ToString() );
				Application.DoEvents();
				foreach( SQLiteResult row in rows )
				{
					items.Add( new IAItem( row ) );
					Application.DoEvents();
				}
			}
			
			return items;
		}
	}
}
