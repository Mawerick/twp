﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of ItemFormatter.
    /// </summary>
    public class ItemFormatter
    {
        public static string FormatCredits(int credits)
        {
            if (credits < 0)
                credits *= -1;

            return credits.ToString("### ### ### ###").Trim();
        }

        public static string FormatTime(int seconds)
        {
            return FormatTime((double)seconds, "#0");
        }

        public static string FormatTime(double seconds)
        {
            return FormatTime(seconds, "#0.00");
        }

        public static string FormatTime(double seconds, string format)
        {
            string text = String.Empty;
            if (seconds > 60)
            {
                double minutes = Math.Floor(seconds / 60);
                seconds -= minutes * 60;

                if (minutes > 60)
                {
                    double hours = Math.Floor(minutes / 60);
                    minutes -= hours * 60;

                    text += hours.ToString("##0") + "h ";
                }
                if (minutes > 0)
                    text += minutes.ToString("#0") + "m ";
            }
            if (seconds > 0)
                text += seconds.ToString(format, System.Globalization.CultureInfo.InvariantCulture) + "s";
            return text.Trim();
        }

        public static string GetItemName(object id, Database db)
        {
            AOItem item = AOItem.Load(Convert.ToInt32(id), db);
            if (item != null)
                return item.Name;
            else
                return Convert.ToString(id);
        }

        public static string GetItemLink(object id, Database db)
        {
            AOItem item = AOItem.Load(Convert.ToInt32(id), db);
            if (item != null)
                return String.Format("<a href='itemref://{0}/{0}/{1}'>{2}</a>", item.Id, item.Ql, item.Name);
            else
                return String.Format("<span class='headline'>{0}</span>", Convert.ToString(id));
        }

        public static string GetNanoName(object id, Database db)
        {
            AONano nano = AONano.Load(Convert.ToInt32(id), db);
            if (nano != null)
                return nano.Name;
            else
                return Convert.ToString(id);
        }

        public static string GetNanoLink(object id, Database db)
        {
            AONano nano = AONano.Load(Convert.ToInt32(id), db);
            if (nano != null)
                return String.Format("<a href='nanoref://{0}'>{1}</a>", nano.Id, nano.Name);
            else
                return String.Format("<span class='headline'>{0}</span>", Convert.ToString(id));
        }

        public static string GetPerkLink(object id)
        {
            // TODO: Fix perk links...
            return id.ToString();
        }

        public static string GetAction(int key)
        {
            return CleanEnum((ActionKey)key) + ":";
        }

        public static string GetEvent(int key)
        {
            return CleanEnum((EventKey)key) + ":";
        }

        public static string GetFunction(int key)
        {
            return CleanEnum((FunctionKey)key);
        }

        public static string GetTarget(int key, Mmdb mmdb)
        {
            string text = null;
            if (key < 15)
                text = mmdb.GetString(506, key);
            else
                text = mmdb.GetString(1005, key + 1000);
            if (String.IsNullOrEmpty(text))
                text = String.Format("{0} ({1})", (TargetKey)key, key);
            return text;
        }

        public static string GetAttrOp(int op, ref int val)
        {
            if (op == (int)OperatorKey.GreaterThan)
            {
                val++;
                return "from";
            }
            else return GetAttrOp(op);
        }

        public static string GetAttrOp(int op)
        {
            switch ((OperatorKey)op)
            {
                case OperatorKey.Equal:
                    return "is";
                case OperatorKey.NotEqual:
                    return "is not";
                case OperatorKey.LessThan:
                    return "below";
                case OperatorKey.BitAnd:
                    return "is";
                case OperatorKey.NotBitAnd:
                    return "is not";
                case OperatorKey.Default:
                    return " ";
                default:
                    return CleanEnum((OperatorKey)op).ToLower();
            }
        }

        public static string GetSlot(int slot, ItemType page)
        {
            switch (page)
            {
                case ItemType.Weapon:
                    return CleanEnum((WeaponSlot)slot);
                case ItemType.Armor:
                    return CleanEnum((ArmorSlot)slot);
                case ItemType.Implant:
                case ItemType.Spirit:
                    return CleanEnum((ImplantSlot)slot);
            }
            return slot.ToString();
        }

        public static string GetAttr(int key, Mmdb mmdb)
        {
            switch ((AttributeKey)key)
            {
                case AttributeKey.NanoProgramming:
                    return "Nano Programming";
                default:
                    string text = mmdb.GetString(2003, key);
                    if (String.IsNullOrEmpty(text))
                        text = mmdb.GetString(2002, key);
                    if (String.IsNullOrEmpty(text))
                        text = CleanEnum((AttributeKey)key);
                    return text;
            }
        }

        public static string GetAttrValue(int key, int op, int val, Mmdb mmdb)
        {
            switch ((AttributeKey)key)
            {
                case AttributeKey.Flags:
                case AttributeKey.MoreFlags:
                    switch ((OperatorKey)op)
                    {
                        case OperatorKey.Default:
                            return CleanEnum((ItemFlag)val);
                    }
                    break;

                case AttributeKey.Can:
                    return CleanEnum((ItemCanFlag)val);

                case AttributeKey.Credits:
                    return FormatCredits(val);

                case AttributeKey.Breed:
                case AttributeKey.Breed2:
                case AttributeKey.VisualBreed:
                    return CleanEnum((Breed)val);

                case AttributeKey.Gender:
                case AttributeKey.VisualGender:
                    return CleanEnum((Gender)val);

                case AttributeKey.Girth:
                    return CleanEnum((CharacterGirth)val);

                case AttributeKey.Profession:
                case AttributeKey.VisualProfession:
                    return CleanEnum((Profession)val);

                case AttributeKey.Side:
                    return CleanEnum((Side)val);

                case AttributeKey.MonsterType:
                    return CleanEnum((MonsterType)val);

                case AttributeKey.Specialization:
                    return CleanEnum((Specialization)val);

                case AttributeKey.Expansion:
                    return CleanEnum((ExpansionFlag)val);

                case AttributeKey.ExpansionPlayfield:
                    return CleanEnum((ExpansionPlayfield)val);

                case AttributeKey.PlayfieldType:
                    return CleanEnum((PlayfieldType)val);

                case AttributeKey.CurrentPlayfield:
                    return Playfields.GetName(val);

                case AttributeKey.InitiativeType:
                    return mmdb.GetString(2002, val);

                case AttributeKey.DamageType:
                case AttributeKey.DamageType2:
                    return mmdb.GetString(2000, val);

                case AttributeKey.SelectedTargetType:
                    return CleanEnum((SelectedTargetType)val);

                case AttributeKey.MapUpgrades:
                    return CleanEnum((MapUpgrade)val);

                case AttributeKey.MapFlags1:
                    return CleanEnum((MapA)val);

                case AttributeKey.MapFlags2:
                    return CleanEnum((MapB)val);

                case AttributeKey.MapFlags3:
                    return CleanEnum((MapC)val);

                case AttributeKey.MapFlags4:
                    return CleanEnum((MapD)val);

                case AttributeKey.NanoSchool:
                    return CleanEnum((NanoSchool)val);

                case AttributeKey.SLZoneProtection:
                    return CleanEnum((SLZoneProtection)(1 << val));

                case AttributeKey.WornItem:
                    return CleanEnum((WornItem)(1 << val));
            }
            return val.ToString();
        }

        /// <summary>
        /// Converts an enum value to string, and cleans it up.
        /// </summary>
        /// <param name="value">An object reference of the enum value.</param>
        /// <returns>A cleaned up string.</returns>
        /// <remarks>_ becomes ' '<br/>_ _ becomes '-'<br/>_ _ _ becomes ' - '</remarks>
        public static string CleanEnum(object value)
        {
            string text = Convert.ToString(value);
            text = text.Replace("___", " - ");
            text = text.Replace("__", "-");
            text = text.Replace('_', ' ');
            return text;
        }
    }
}
