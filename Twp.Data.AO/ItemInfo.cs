﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace Twp.Data.AO
{
    public class ItemInfo : IComparable, IEquatable<ItemInfo>, IEquatable<object>
    {
        #region Fields

        public static readonly ItemInfo Empty = new ItemInfo();

        private int id = 0;
        private string name;
        private string description;
        private ItemType type;
        private int ql;
        private AttributeList attributes;
        private AtkDefList attack;
        private AtkDefList defense;
        private ActionList actions;
        private EventList events;
        private List<AnimSoundSet> animSets;
        private List<AnimSoundSet> soundSets;
        
        private Database database;
        private Mmdb mmdb;
        
        #endregion
        
        #region Properties
        
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public ItemType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public int QL
        {
            get { return this.ql; }
            set { this.ql = value; }
        }

        public AttributeList Attributes
        {
            get { return this.attributes; }
            set { this.attributes = value; }
        }

        public AtkDefList Attack
        {
            get { return this.attack; }
            set { this.attack = value; }
        }

        public AtkDefList Defense
        {
            get { return this.defense; }
            set { this.defense = value; }
        }

        public ActionList Actions
        {
            get { return this.actions; }
            set { this.actions = value; }
        }

        public EventList Events
        {
            get { return this.events; }
            set { this.events = value; }
        }

        public List<AnimSoundSet> AnimSets
        {
            get { return this.animSets; }
            set { this.animSets = value; }
        }

        public List<AnimSoundSet> SoundSets
        {
            get { return this.soundSets; }
            set { this.soundSets = value; }
        }

        public Database Database
        {
            get { return this.database; }
            set { this.database = value; }
        }

        public Mmdb Mmdb
        {
            get { return this.mmdb; }
            set { this.mmdb = value; }
        }

        #endregion

        #region Html Formatting

        public string ToHtml()
        {
            #if DEBUG
            return this.ToHtml( true );
        }

        public string ToHtml( bool debug )
        {
            #endif
            if( this.database == null )
            {
                Log.Error( "Unable to format item data: Database link missing!" );
                return null;
            }
            if( this.mmdb == null )
            {
                Log.Error( "Unable to format item data: MMDB link missing!" );
                return null;
            }

            string html = "<html><body>";

            if( this.type == ItemType.Nano || this.type == ItemType.Perk )
                html += FormatNanoAttributes();
            else
                html += FormatItemAttributes();

            html += this.attack.ToHtml( false );
            html += this.defense.ToHtml( false );
            html += "<br>";
            html += this.events.ToHtml( false );
            html += this.actions.ToHtml( false );
            if( !String.IsNullOrEmpty( this.description ) )
            {
                html += "<div class='header'>Description:</div>";
                html += "<div class='description'>" + this.description.Replace( "\r\n", "<br>" ).Replace( "\n", "<br>" ) + "</div>";
            }
            #if DEBUG
            if( debug )
            {
                html += "<hr><div class='headline'>Raw Data:</div>";
                if( this.id > 0 )
                    html += String.Format( "<div><span class='header'>ID:</span> {0}</div>", this.id );
                html += this.attributes.ToHtml();
                html += this.attack.ToHtml( true );
                html += this.defense.ToHtml( true );
                html += this.events.ToHtml( true );
                html += this.actions.ToHtml( true );
            }
            #endif
            html += "</body></html>";
            return html;
        }

        private string FormatNanoAttributes()
        {
            string html = String.Format( "<table><tr><td class='headline'>{0}</td>", this.name );

            AttributeList attributes = this.attributes;

            html += "<td rowspan='2' class='nanoIcon'>";
            if( attributes.Contains( AttributeKey.Icon ) )
                html += "<img src='rdb://" + Convert.ToString( attributes[AttributeKey.Icon] ) + "'>";
            if( attributes.Contains( AttributeKey.EffectIcon ) )
                html += "<img src='rdb://" + Convert.ToString( attributes[AttributeKey.EffectIcon] ) + "'>";
            html += "</td></tr><tr><td>";

            if( attributes.Contains( AttributeKey.OmniTokens ) )
            {
                string text = mmdb.GetString( 2009, attributes[AttributeKey.OmniTokens] );
                html += String.Format( "<div><span class='header'>Nano Strain:</span> {0}</div>", text );
            }

            html += this.FormatAttribute( AttributeKey.ClanTokens, "Alignment" );
            html += this.FormatAttribute( AttributeKey.StackingOrder, "Stacking Order" );
            html += this.FormatAttribute( AttributeKey.NanoSchool, "School" );

            html += this.FormatAttribute( AttributeKey.Duration, "Duration", AttributeFormat.Time );
            if( ( attributes.Contains( AttributeKey.AttackDelay ) && attributes[AttributeKey.AttackDelay] > 0 ) ||
               ( attributes.Contains( AttributeKey.RechargeDelay ) && attributes[AttributeKey.RechargeDelay] > 0 ))
            {
                html += String.Format( "<div><span class='header'>Speed:</span>" );
                html += this.FormatAttribute( "<br>&nbsp;&nbsp;{0}: {1}", AttributeKey.AttackDelay, "Attack", AttributeFormat.Time );
                html += this.FormatAttribute( "<br>&nbsp;&nbsp;{0}: {1}", AttributeKey.RechargeDelay, "Recharge", AttributeFormat.Time );
                html += "</div>";
            }
            html += this.FormatAttribute( attributeFormat, AttributeKey.EquipDelay, "Equip Delay", AttributeFormat.Time );
            html += this.FormatAttribute( attributeFormat, AttributeKey.AttackRange, "Range", AttributeFormat.Range );

            if( attributes.Contains( AttributeKey.MinDamage ) )
            {
                string dmg = String.Format( "{0} - {1} ({2})", attributes[AttributeKey.MinDamage],
                                           attributes[AttributeKey.MaxDamage],
                                           attributes[AttributeKey.CriticalBonus] );
                html += String.Format( "<div><span class='header'>Damage:</span> {0}</div>", dmg );
                html += this.FormatAttribute( attributeFormat, AttributeKey.DamageType, "Damage Type" );
                html += this.FormatAttribute( attributeFormat, AttributeKey.DamageType2, "Damage Type" );
            }
            
            html += this.FormatAttribute( attributeFormat, AttributeKey.InitiativeType, "Initiative skill" );
            html += "</td></tr></table>";
            return html;
        }
        
        private string FormatItemAttributes()
        {
            AttributeList attributes = this.attributes;
            int rarity = attributes[AttributeKey.Rarity];
            if( rarity < 0 )
                rarity = 0; // set to Unknown.
            
            string html = String.Format( "<table><tr><td class='rarity{0}'>{1}</td>", rarity, this.name );

            if( attributes.Contains( AttributeKey.Icon ) )
            {
                html += "<td rowspan='2' class='icon'>";
                html += "<img src='rdb://" + Convert.ToString( attributes[AttributeKey.Icon] ) + "'>";
                html += "</td>";
            }
            html += "</tr><tr><td>";

            if( Flag.IsSet( (ItemFlag) attributes[AttributeKey.Flags], ItemFlag.NoDrop ) )
                html += " NODROP";
            if( Flag.IsSet( (ItemFlag) attributes[AttributeKey.Flags], ItemFlag.Unique ) )
                html += " UNIQUE";

            string rarityTxt = mmdb.GetString( 2016, rarity );
            if( !String.IsNullOrEmpty( rarityTxt ) )
                html += "<div><span class='header'>Rarity: </span>" + rarityTxt + "</div>";
            html += "<div><span class='header'>QL: </span>";
            if( Flag.IsSet( (ItemFlag) attributes[AttributeKey.Flags], ItemFlag.Special ) )
                html += String.Format( "SPECIAL ({0})", this.ql );
            else
                html += this.ql;
            html += "</div>";

            if( this.type == ItemType.Weapon )
            {
                AmmoType ammoType = (AmmoType) attributes[AttributeKey.AmmoType];
                if( ammoType != AmmoType.No_Ammo )
                {
                    if( ammoType != AmmoType.Self__Supplied )
                        html += FormatAttribute( AttributeKey.MaxEnergy, "Ammo" );
                    html += String.Format( attributeFormat, "Ammo Type", ItemFormatter.CleanEnum( ammoType ) );
                }
            }
            else
            {
                html += FormatAttribute( AttributeKey.MaxEnergy, "Charges" );
            }

            if( attributes.Contains( AttributeKey.Can ) && attributes[AttributeKey.Can] > 0 )
                html += this.FormatAttribute( AttributeKey.Can, "Can" );
            if( this.type >= 0 )
                html += String.Format( attributeFormat, "Type", this.type );
            
            if( attributes.Contains( AttributeKey.Slot ) && attributes[AttributeKey.Slot] != 0 )
            {
                ItemType type = (ItemType) attributes[AttributeKey.EquipmentPage];
                string slot = ItemFormatter.GetSlot( attributes[AttributeKey.Slot], type );
                if( attributes.Contains( AttributeKey.DefaultPos ) )
                {
                    string dPos = ItemFormatter.GetSlot( 1 << attributes[AttributeKey.DefaultPos], type );
                    if( slot != dPos )
                        slot = slot.Replace( dPos, "<b>" + dPos + "</b>" );
                }
                html += String.Format( attributeFormat, "Location", slot );
            }

            if( ( attributes.Contains( AttributeKey.AttackDelay ) && attributes[AttributeKey.AttackDelay] > 0 ) ||
               ( attributes.Contains( AttributeKey.RechargeDelay ) && attributes[AttributeKey.RechargeDelay] > 0 ))
            {
                html += String.Format( "<div class='header'>Speed:</div>" );
                html += this.FormatAttribute( "<div class='normal indent'>{0}: {1}</div>",
                                             AttributeKey.AttackDelay, "Attack",
                                             AttributeFormat.Time );
                html += this.FormatAttribute( "<div class='normal indent'>{0}: {1}</div>",
                                             AttributeKey.RechargeDelay, "Recharge",
                                             AttributeFormat.Time );
            }
            html += this.FormatAttribute( AttributeKey.EquipDelay, "Equip Delay", AttributeFormat.Time );
            html += this.FormatAttribute( AttributeKey.AttackRange, "Range", AttributeFormat.Range );

            if( this.type == ItemType.Weapon )
            {
                string dmg = String.Format( "{0} - {1} ({2})", attributes[AttributeKey.MinDamage],
                                           attributes[AttributeKey.MaxDamage],
                                           attributes[AttributeKey.CriticalBonus] );
                html += String.Format( "<div><span class='header'>Damage:</span> {0}</div>", dmg );
                html += this.FormatAttribute( AttributeKey.DamageType, "Damage Type" );
                html += this.FormatAttribute( AttributeKey.DamageType2, "Damage Type" );
                
                string[] dualWield = new string[2];
                dualWield[0] = this.FormatAttribute( "{0} {1}", AttributeKey.MultiMelee, "Multi Melee" );
                dualWield[1] = this.FormatAttribute( "{0} {1}", AttributeKey.MultiRanged, "Multi Ranged" );
                html += String.Format( attributeFormat, "Dual wield", String.Join( " ", dualWield ) );
                html += this.FormatAttribute( AttributeKey.InitiativeType, "Initiative skill" );
                html += this.FormatAttribute( AttributeKey.BurstRecharge, "Burst Recharge" );
                html += this.FormatAttribute( AttributeKey.FullAutoRecharge, "Full Auto Recharge" );
                html += this.FormatAttribute( AttributeKey.AMSCap, "Attack Rating Cap" );
            }
            html += "</td></tr></table>";
            return html;
        }
        
        private static readonly string attributeFormat = "<div><span class='header'>{0}:</span> {1}</div>";
        
        private string FormatAttribute( AttributeKey attrKey, string label )
        {
            return this.FormatAttribute( attributeFormat, attrKey, label, AttributeFormat.Normal );
        }
        
        private string FormatAttribute( AttributeKey attrKey, string label, AttributeFormat attrFormat )
        {
            return this.FormatAttribute( attributeFormat, attrKey, label, attrFormat );
        }

        private string FormatAttribute( string format, AttributeKey attrKey, string label )
        {
            return this.FormatAttribute( format, attrKey, label, AttributeFormat.Normal );
        }

        private string FormatAttribute( string format, AttributeKey attrKey, string label, AttributeFormat attrFormat )
        {
            AttributeList attributes = this.attributes;
            if( !attributes.Contains( attrKey ) )
                return String.Empty;
            
            string attrVal;
            int val = attributes[attrKey];
            switch( attrFormat )
            {
                case AttributeFormat.Time:
                    attrVal = ItemFormatter.FormatTime( Convert.ToDouble( val ) / 100 );
                    break;
                case AttributeFormat.Range:
                    attrVal = val.ToString() + "m";
                    break;
                case AttributeFormat.Percent:
                    attrVal = val.ToString() + "%";
                    break;
                default:
                    attrVal = ItemFormatter.GetAttrValue( (int) attrKey, (int) OperatorKey.Default, val, mmdb ).ToString();
                    break;
            }
            return String.Format( format, label, attrVal );
        }
        
        #endregion

        #region Equals and GetHashCode implementation
        
        public override int GetHashCode()
        {
            int hashCode = 0;
            unchecked {
                if (name != null)
                    hashCode += 1000000007 * name.GetHashCode();
                if (description != null)
                    hashCode += 1000000009 * description.GetHashCode();
                hashCode += 1000000021 * type.GetHashCode();
                hashCode += 1000000033 * ql.GetHashCode();
                if (attributes != null)
                    hashCode += 1000000087 * attributes.GetHashCode();
                if (attack != null)
                    hashCode += 1000000093 * attack.GetHashCode();
                if (defense != null)
                    hashCode += 1000000097 * defense.GetHashCode();
                if (actions != null)
                    hashCode += 1000000103 * actions.GetHashCode();
                if (events != null)
                    hashCode += 1000000123 * events.GetHashCode();
                if (animSets != null)
                    hashCode += 1000000181 * animSets.GetHashCode();
                if (soundSets != null)
                    hashCode += 1000000207 * soundSets.GetHashCode();
            }
            return hashCode;
        }
        
        public bool Equals( ItemInfo other )
        {
            if( other == null )
                return false;
            return this.name == other.name &&
                this.description == other.description &&
                this.type == other.type &&
                this.ql == other.ql &&
                object.Equals( this.attributes, other.attributes ) &&
                object.Equals( this.attack, other.attack ) &&
                object.Equals( this.defense, other.defense ) &&
                object.Equals( this.actions, other.actions ) &&
                object.Equals( this.events, other.events ) &&
                object.Equals( this.animSets, other.animSets ) &&
                object.Equals( this.soundSets, other.soundSets );
        }

        bool IEquatable<ItemInfo>.Equals( ItemInfo other )
        {
            return this.Equals( other );
        }

        bool IEquatable<object>.Equals( object other )
        {
            return this.Equals( other as ItemInfo );
        }

        public override bool Equals( object other )
        {
            return this.Equals( other as ItemInfo );
        }
        
        public static bool operator ==( ItemInfo item1, ItemInfo item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return true;
            if( object.ReferenceEquals( item1, null ) )
                return false;
            if( object.ReferenceEquals( item2, null ) )
                return false;

            return item1.Equals( item2 );
        }

        public static bool operator !=( ItemInfo item1, ItemInfo item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return false;
            if( object.ReferenceEquals( item1, null ) )
                return true;
            if( object.ReferenceEquals( item2, null ) )
                return true;

            return !item1.Equals( item2 );
        }

        #endregion

        #region IComparable implementation

        public int CompareTo( object obj )
        {
            if( obj == null )
                return 1;

            ItemInfo other = obj as ItemInfo;
            if( other != null )
            {
                int ret = this.name.CompareTo( other.name );
                if( ret == 0 )
                    return this.ql.CompareTo( other.ql );
                return ret;
            }
            return -1;
        }

        #endregion

        public override string ToString()
        {
            return String.Format( "ItemInfo( {0}, {1} )", this.name, this.ql );
        }
    }
}
