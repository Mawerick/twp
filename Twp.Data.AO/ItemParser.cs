﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  Most of this code comes from XRDB4
//  Copyright (C) William "Xyphos" Scott <TheGreatXyphos@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Twp.Utilities;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of ItemParser.
    /// </summary>
    public static class ItemParser
    {
        #region Parser methods

        public static ItemInfo Parse( AOItem item )
        {
            ItemInfo info = Parse( item.Data, true );
            info.Id = item.Id;
            return info;
        }

        public static ItemInfo Parse( AONano nano )
        {
            ItemInfo info = Parse( nano.Data, true );
            info.Id = nano.Id;
            info.Type = ItemType.Nano;
            return info;
        }

        public static ItemInfo Parse( byte[] data )
        {
            return Parse( data, true );
        }

        public static ItemInfo Parse( byte[] data, bool everything )
        {
            FunctionSets.Init();
            BinFile binFile = new BinFile( data );
            ItemInfo item = new ItemInfo();
            item.Attributes = new AttributeList( item );
            item.Attack = new AtkDefList( "Attack", item );
            item.Defense = new AtkDefList( "Defense", item );
            item.Actions = new ActionList( item );
            item.Events = new EventList( item );
            item.AnimSets = new List<AnimSoundSet>();
            item.SoundSets = new List<AnimSoundSet>();

            //parse a bit of the data to get the item's name
            binFile.Position += 16; //skip pre-attributes
            long attrCount = binFile.Read3F1();   //get attr count
            for( long n = 0; n < attrCount; n++ )
            {
                int key = binFile.ReadInt32();
                int val = binFile.ReadInt32();
                if( key == (int) AttributeKey.Level )
                    item.QL = val;
                item.Attributes.Add( key, val );
            }
            binFile.Position += 8; //skip post-attributes
            short nameLen = binFile.ReadInt16();
            short descLen = binFile.ReadInt16();
            if( nameLen >= 0 )
                item.Name = binFile.ReadString( nameLen );
            if( descLen >= 0 )
                item.Description = binFile.ReadString( descLen );

            try
            {
                int type = item.Attributes[AttributeKey.EquipmentPage];
                if( type == -1 )
                    item.Type = ItemType.Misc;
                else
                    item.Type = (ItemType) type;
            }
            catch
            {
                item.Type = ItemType.Misc;
            }
            if( item.Name.Contains( "_" ) || item.Name.ToUpper().Contains( "BOSS" ) )
                item.Type = ItemType.NPC;
            else if( item.Attributes.Contains( AttributeKey.TowerType ) )
                item.Type = ItemType.Tower;
            else if( item.Attributes.Contains( AttributeKey.IsVehicle ) && item.Attributes[AttributeKey.IsVehicle] == 1 )
                item.Type = ItemType.Vehicle;
            else if( item.Attributes.Contains( AttributeKey.EquipmentPage ) &&
                    item.Attributes[AttributeKey.EquipmentPage] == (int) ItemType.Weapon )
            {
                int slot = item.Attributes[AttributeKey.Slot];
                if( Flag.IsSet( slot, 1 << 6 ) || Flag.IsSet( slot, 1 << 8 ) )
                    item.Type = ItemType.Weapon;
                else
                    item.Type = ItemType.Utility;
            }
            else if( item.Attributes.Contains( AttributeKey.EquipmentPage ) &&
                    item.Attributes[AttributeKey.EquipmentPage] == (int) ItemType.Armor )
            {
                if( item.Attributes[AttributeKey.Slot] == 0 && item.Attributes[AttributeKey.DefaultPos] == 0 )
                    item.Type = ItemType.Perk;
            }
            
            if( everything )
            {
                bool error = false;
                while( binFile.Position < binFile.Length - 0x8 && error == false )
                {
                    int keyNum = binFile.ReadInt32();
                    switch( keyNum )
                    {
                        case 0x2:
                            ParseFunctionSet( ref item, binFile );
                            break;
                            
                        case 0x4:
                            ParseAtkDefSet( ref item, binFile );
                            break;
                            
                        case 0x6:
                            binFile.Position += 4;
                            long cnum = binFile.Read3F1();
//                            Log.Debug( "[ItemParser.Parse] Reading unknown record type 0x6: {0} items.", cnum );
//                            for( long i = 0; i < cnum; i++ )
//                                Log.Debug( "[ItemParser.Parse] [{0,22}::0x{0:X8}]", binFile.ReadInt64() );
                            binFile.Position += (uint) cnum * 8;
                            break;
                            
                        case 0xE: // animset
                            ParseAnimSoundSet( ref item, 1, binFile );
                            break;
                            
                        case 0x14: // soundset
                            ParseAnimSoundSet( ref item, 2, binFile );
                            break;
                            
                        case 0x16: // actionset
                            ParseActionSet( ref item, binFile );
                            break;
                            
                        case 0x17:
                            ParseShopHash( ref item, binFile );
                            break;
                            
                        default:
                            string tmpFile = System.IO.Path.GetTempFileName();
                            using( System.IO.BinaryWriter bw = new System.IO.BinaryWriter( System.IO.File.Open( tmpFile, System.IO.FileMode.Create ) ) )
                            {
                                bw.Write( data );
                            }
                            Log.Debug( "[ItemParser.Parse] Invalid KeyNum {0} at position {1:X}, data written to {2}",
                                      keyNum, binFile.Position - 4, tmpFile );
                            error = true;
                            break;
                    }
                }
            }
            binFile.Close();
            binFile.Dispose();
            return item;
        }

        private static void ParseShopHash( ref ItemInfo info, BinFile binFile )
        {
            if( binFile == null )
                return;

            int eventNum = binFile.ReadInt32();
            int numFuncs = (int) binFile.Read3F1();
//            Log.Debug( "[ItemParser.ParseShopHash] ID: {0} Items: {1}", eventNum, numFuncs );

            for( int i = 0; i < numFuncs; i++ )
            {
                string strVal = binFile.ReadString( 4 );
//                Log.Debug( "[ItemParser.ParseShopHash] Str? {0}", strVal );
                int numA = (int) binFile.ReadByte();
                int numB = (int) binFile.ReadByte();
//                Log.Debug( "[ItemParser.ParseShopHash] A: {0} B: {1}", numA, numB );
                if( numA == 0 && numB == 0 )
                {
                    numA = binFile.ReadInt32();
                    numB = binFile.ReadInt32();
//                    Log.Debug( "[ItemParser.ParseShopHash] A: {0} B: {1}", numA, numB );
                }
                uint skip = (uint) Math.Min( 11, binFile.Length - binFile.Position );
                byte[] bytes = binFile.ReadBytes( (int) skip );
//                Log.Debug( "[ItemParser.ParseShopHash] Bytes: {0} ({1})", BitConverter.ToString( bytes ), bytes.Length );
//                binFile.Position += skip;
            }
        }

        private static void ParseActionSet( ref ItemInfo info, BinFile binFile )
        {
            if( binFile == null )
                return;

            if( binFile.ReadInt32() != 0x24 )
                throw new Exception( "Parser error!" );

            int maxSets = (int) binFile.Read3F1();
            for( int i = 0; i < maxSets; i++ )
            {
                int actionNum = binFile.ReadInt32();
                int numReqs = (int) binFile.Read3F1();
                if( numReqs > 0 )
                {
//                    Log.Debug( "[ItemParser.ParseActionSet] Reqs: {0}", numReqs );
                    for( int r = 0; r < numReqs; r++ )
                    {
                        Requirement req = new Requirement( info );
                        req.Key = binFile.ReadInt32();
                        req.Value = binFile.ReadInt32();
                        req.Op = binFile.ReadInt32();
//                        Log.Debug( "[ItemParser.ParseActionSet] Key: {0} Val: {1} Op: {2}", (AttributeKey) req.Key, req.Value, (OperatorKey) req.Op );
                        info.Actions[actionNum].Add( req );
                    }
                }
            }
        }
        
        private static void ParseAnimSoundSet( ref ItemInfo info, int type, BinFile binFile )
        {
            if( binFile == null )
                return;

            int setType = binFile.ReadInt32();
            int numFuncs = (int) binFile.Read3F1();
            
            for( int i = 0; i < numFuncs; i++ )
            {
                AnimSoundSet animSet = new AnimSoundSet();
                animSet.Key = binFile.ReadInt32();
                
                List<int> animList = new List<int>();
                int maxSets = (int) binFile.Read3F1();
                for( int j = 0; j < maxSets; j++ )
                    animList.Add( binFile.ReadInt32() );
                
                animSet.AnimList = animList.ToArray();
                
                switch( type )
                {
                    case 1:
                        info.AnimSets.Add( animSet );
                        break;
                    case 2:
                        info.SoundSets.Add( animSet );
                        break;

                    default:
                        Log.Debug( "[ItemParser.ParseAnimSoundSet] Invalid type {0}", type );
                        break;
                }
            }
        }
        
        private static void ParseAtkDefSet( ref ItemInfo info, BinFile binFile )
        {
            if( binFile == null )
                return;
            
            binFile.Position += 4;
            int maxSets = (int) binFile.Read3F1();
            for( int i = 0; i < maxSets; i++ )
            {
                int key = binFile.ReadInt32();
                int sets = (int) binFile.Read3F1();
                for( int j = 0; j < sets; j++ )
                {
                    AtkDef set = new AtkDef();
                    set.Key = binFile.ReadInt32();
                    set.Value = binFile.ReadInt32();
                    
                    switch( key )
                    {
                        case 12:
                            info.Attack.Add( set );
                            break;

                        case 13:
                            info.Defense.Add( set );
                            break;

                        default:
                            Log.Debug( "[ItemParser.ParseAtkDefSet] Unhandled set {0:X} in {1}", key, info.Name );
                            break;
                    }
                }
            }
        }

        private static void ParseFunctionSet( ref ItemInfo info, BinFile binFile )
        {
            if( binFile == null )
                return;

            int eventNum = binFile.ReadInt32();
            int numFuncs = (int) binFile.Read3F1();
            
            for( int i = 0; i < numFuncs; i++ )
            {
                Function func = new Function( info );
                func.Key = binFile.ReadInt32();
                binFile.Position += 8;
                int numReqs = binFile.ReadInt32();
                if( numReqs > 0 )
                {
//                    Log.Debug( "[ItemParser.ParseFunctionSet] Reqs: {0}", numReqs );
                    for( int r = 0; r < numReqs; r++ )
                    {
                        Requirement req = new Requirement( info );
                        req.Key = binFile.ReadInt32();
                        req.Value = binFile.ReadInt32();
                        req.Op = binFile.ReadInt32();
//                        Log.Debug( "[ItemParser.ParseFunctionSet] Key: {0} Val: {1} Op: {2}", (AttributeKey) req.Key, req.Value, (OperatorKey) req.Op );
                        func.Requirements.Add( req );
                    }
                }
                func.TickCount = binFile.ReadInt32();
                func.TickInterval = binFile.ReadInt32();
                func.Target = binFile.ReadInt32();
                binFile.Position += 4;
                func.Arguments = ParseFunctionArgs( func.Key, binFile );
                
                info.Events[eventNum][func.Target].Add( func );
            }
        }
        
        private static object[] ParseFunctionArgs( int key, BinFile binFile )
        {
            if( binFile == null )
                return null;

            if( !FunctionSets.Exists( (FunctionKey) key ) )
                return null;

            string[] args = FunctionSets.Get( (FunctionKey) key ).Split( ',' );
            List<object> argList = new List<object>();
            foreach( string arg in args )
            {
                string tArg = arg.Trim();
                int qty = Convert.ToInt32( tArg.Substring( 0, tArg.Length - 1 ) );
                string proc = tArg.Substring( tArg.Length - 1 );
                switch( proc )
                {
                    case "n":
                        for( int i = 0; i < qty; i++ )
                        {
                            int val = binFile.ReadInt32();
                            argList.Add( val );
                        }
                        break;
                        
                    case "h":
                        for( int i = 0; i < qty; i++ )
                        {
                            string val = binFile.ReadHash();
                            argList.Add( val );
                        }
                        break;
                        
                    case "s":
                        for( int i = 0; i < qty; i++ )
                        {
                            string val = "";
                            int strLen = binFile.ReadInt32();
                            if( strLen > 0 )
                            {
                                if( strLen > 1 )
                                    val = binFile.ReadString( strLen - 1 );
                                binFile.Position += 1;
                            }
                            argList.Add( val );
                        }
                        break;
                        
                    case "x":
                        binFile.Position += qty;
//                        byte[] bytes = binFile.ReadBytes( qty );
//                        Log.Debug( "[ItemParser.ParseArgs] Key: {0} Read bytes: {1}", key, BitConverter.ToString( bytes ) );
                        //argList.Add( bytes );
                        break;
                        
                    default:
                        Log.Debug( "[ItemParser.ParseArgs] Malformed FunctionSet!" );
                        break;
                }
            }
            
            return argList.ToArray();
        }

        #endregion

        #region Interpolation methods

        public static ItemInfo Interpolate( Item item )
        {
            if( item == null || item.ItemLow == null || item.ItemHigh == null )
                throw new ArgumentException( "Unable to interpolate. Missing data." );
            
            return Interpolate( item.ItemLow.Data, item.ItemHigh.Data, item.Ql );
        }

        public static ItemInfo Interpolate( byte[] lowData, byte[] highData, int ql )
        {
            ItemInfo low = Parse( lowData );
            if( lowData.Equals( highData ) )
                return low;
            ItemInfo high = Parse( highData );
            
            if( low.QL == high.QL )
                return low;
            
            if( ql <= low.QL )
                return low;
            
            if( ql >= high.QL )
                return high;

            /* Formula for non-static elements:
             *
             * m=(ql-lowQL)/(highQL-lowQL)
             * v=m*(highV-lowV)+lowV
             */
            int midpoint = low.QL + ( ( high.QL - low.QL ) / 2 );
            ItemInfo mid = ql >= midpoint ? high : low;

            double modifier = (double)(ql - low.QL) / (double)(high.QL - low.QL);
            
            ItemInfo item = new ItemInfo();
            item.QL = ql;
            item.Name = mid.Name;
            item.Description = mid.Description;
            item.Attributes = new AttributeList( item );
            foreach( int key in mid.Attributes.Keys )
            {
                if( !low.Attributes.ContainsKey( key ) || !high.Attributes.ContainsKey( key ) )
                    item.Attributes.Add( key, mid.Attributes[key] );
                else
                    item.Attributes.Add( key, InterpolateAttribute( key, low.Attributes[key], high.Attributes[key], mid.Attributes[key], modifier ) );
            }
            item.Actions = new ActionList( item );
            foreach( int act in mid.Actions.Keys )
            {
                RequirementList reqs = new RequirementList( item );
                for( int i = 0; i < mid.Actions[act].Count; i++ )
                {
                    Requirement req = new Requirement( item );
                    Requirement midReq = mid.Actions[act][i];
                    Requirement lowReq = low.Actions[act][i];
                    Requirement highReq = high.Actions[act][i];
                    req.Key = midReq.Key;
                    req.Value = InterpolateAttribute( req.Key, lowReq.Value, highReq.Value, midReq.Value, modifier );
                    req.Op = midReq.Op;
                    reqs.Add( req );
                }
                item.Actions.Add( act, reqs );
            }
            item.Events = new EventList( item );
            foreach( int eKey in mid.Events.Keys )
            {
                FunctionList funcList = new FunctionList( item );
                FunctionList midFuncList = mid.Events[eKey];
                FunctionList lowFuncList = low.Events[eKey];
                FunctionList highFuncList = high.Events[eKey];
                
                if( lowFuncList.Count != highFuncList.Count )
                    continue;
                
                foreach( int fKey in midFuncList.Keys )
                {
                    List<Function> funcs = new List<Function>();
                    List<Function> midFuncs = midFuncList[fKey];
                    List<Function> lowFuncs = lowFuncList[fKey];
                    List<Function> highFuncs = highFuncList[fKey];
                    
                    if( lowFuncs.Count != highFuncs.Count )
                        continue;
                    
                    for( int i = 0; i < midFuncs.Count; i++ )
                    {
                        Function func = new Function( item );
                        Function midFunc = midFuncs[i];
                        Function lowFunc = lowFuncs[i];
                        Function highFunc = highFuncs[i];
                        
                        func.Key = midFunc.Key;
                        func.Target = midFunc.Target;
                        func.TickCount = (int) Math.Round(modifier * (double)(highFunc.TickCount - lowFunc.TickCount)) + lowFunc.TickCount;
                        func.TickInterval = (int) Math.Round(modifier * (double)(highFunc.TickInterval - lowFunc.TickInterval)) + lowFunc.TickInterval;
                        
                        if( midFunc.Requirements != null )
                        {
                            for( int j = 0; j < midFunc.Requirements.Count; j++ )
                            {
                                Requirement req = new Requirement( item );
                                Requirement midReq = midFunc.Requirements[j];
                                Requirement lowReq = lowFunc.Requirements[j];
                                Requirement highReq = highFunc.Requirements[j];
                                req.Key = midReq.Key;
                                req.Value = InterpolateAttribute( req.Key, lowReq.Value, highReq.Value, midReq.Value, modifier );
                                req.Op = midReq.Op;
                                func.Requirements.Add( req );
                            }
                        }
                        
                        if( midFunc.Arguments != null )
                        {
                            if( highFunc.Arguments.Length != lowFunc.Arguments.Length )
                            {
                                func.Arguments = midFunc.Arguments;
                            }
                            else
                            {
                                List<object> args = new List<object>();
                                for( int j = 0; j < midFunc.Arguments.Length; j++ )
                                {
                                    object arg = midFunc.Arguments[j];
                                    if( arg is int && highFunc.Arguments[j] != lowFunc.Arguments[j] && highFunc.Key == lowFunc.Key )
                                        arg = (int) Math.Round(modifier * (double)((int)highFunc.Arguments[j] - (int)lowFunc.Arguments[j])) + (int)lowFunc.Arguments[j];
                                    args.Add( arg );
                                }
                                func.Arguments = args.ToArray();
                            }
                        }
                        funcs.Add( func );
                    }
                    funcList.Add( fKey, funcs );
                }
                item.Events.Add( eKey, funcList );
            }
            item.AnimSets = mid.AnimSets;
            item.SoundSets = mid.SoundSets;
            item.Attack = new AtkDefList( mid.Attack, item );
            item.Defense = new AtkDefList( mid.Defense, item );
            return item;
        }

        private static int InterpolateAttribute( int key, int low, int high, int mid, double modifier )
        {
            int val = mid;
            switch( (AttributeKey) key )
            {
                case AttributeKey.Flags:
                case AttributeKey.MoreFlags:
                case AttributeKey.Can:
                case AttributeKey.Icon:
                case AttributeKey.EffectIcon:
                case AttributeKey.Slot:
                case AttributeKey.DefaultPos:
                    break;

                default:
                    if( low != high )
                        val = (int) Math.Round( modifier * (double)(high - low) ) + low;
                    break;
            }
            return val;
        }
        
        #endregion

        #region Function parsing helper class
        
        private static class FunctionSets
        {
            public static void Init()
            {
                if( sets != null )
                    return;
                
                sets = new Dictionary<FunctionKey, string>();
                sets.Add( FunctionKey.Hit, "4n" );
                sets.Add( FunctionKey.Anim_Effect, "5n" );
                sets.Add( FunctionKey.Skill, "2n,4x" );
                sets.Add( FunctionKey.Timed_Effect, "2n,8x,1n" );
                sets.Add( FunctionKey.Teleport, "4n" );
                sets.Add( FunctionKey.Upload_Nano, "1n" );
                sets.Add( FunctionKey.Anim, "3n" );
                sets.Add( FunctionKey.Set, "4x,2n" );
                sets.Add( FunctionKey.Add_Skill, "2n,4x" );
                sets.Add( FunctionKey.Gfx_Effect, "1n,24x" );
                sets.Add( FunctionKey.Save_Char, "16x" );
                sets.Add( FunctionKey.Lock_Skill, "4x,2n" );
                sets.Add( FunctionKey.Head_Mesh, "4x,1n" );
                sets.Add( FunctionKey.Back_Mesh, "4x,1n" );
                sets.Add( FunctionKey.Shoulder_Mesh, "4x,1n" );
                sets.Add( FunctionKey.Texture, "2n,4x" );
                sets.Add( FunctionKey.System_Text, "1s,20x" );
                sets.Add( FunctionKey.Modify, "2n" );
                sets.Add( FunctionKey.Cast_Nano, "1n" );
                sets.Add( FunctionKey.Change_Body_Mesh, "1s" );
                sets.Add( FunctionKey.Attractor_Mesh, "4x,1n" );
                sets.Add( FunctionKey.Head_Text, "1s,1n" );
                sets.Add( FunctionKey.Monster_Shape, "1n,4x" );
                sets.Add( FunctionKey.Spawn_Monster_2, "1h,2n,28x" );
                sets.Add( FunctionKey.Spawn_Item, "1h,2n,20x" );
                sets.Add( FunctionKey.Attractor_Effect, "16x" );
                sets.Add( FunctionKey.Team_Cast_Nano, "1n" );
                sets.Add( FunctionKey.Change_Action_Restriction, "2n" );
                sets.Add( FunctionKey.Restrict_Action, "1n,4x" );
//                sets.Add( FunctionKey.NextHead, "0x" );
//                sets.Add( FunctionKey.PrevHead, "0x" );
                sets.Add( FunctionKey.Area_Hit, "5n" );
                sets.Add( FunctionKey.Attractor_Effect_2, "16x" );
                sets.Add( FunctionKey.Attractor_Effect_3, "16x" );
                sets.Add( FunctionKey.NPC_Social_Anim, "1n" );
                sets.Add( FunctionKey.Change_Effect, "1n,8x" );
                sets.Add( FunctionKey.Teleport_Proxy, "4n,8x" );
                sets.Add( FunctionKey.Teleport_Proxy_2, "4n,12x" );
//                sets.Add( FunctionKey.RefreshModel, "0x" );
                sets.Add( FunctionKey.Area_Cast_Nano, "2n" );
                sets.Add( FunctionKey.Cast_Stun_Nano, "2n" );
//                sets.Add( FunctionKey.OpenBank, "0x" );
                sets.Add( FunctionKey.Equip_Monster_Weapon, "1h" );
                sets.Add( FunctionKey.NPC_Say_Robot_Speech, "1s,4x" );
                sets.Add( FunctionKey.Remove_Nano_Effects, "3n" );
                sets.Add( FunctionKey.NPC_Push_Script, "1n" );
//                sets.Add( FunctionKey.EnterApartment, "0x" );
                sets.Add( FunctionKey.Change_Variable, "2n" );
                sets.Add( FunctionKey.Input_Box, "1s,8x" );
                sets.Add( FunctionKey.Taunt_NPC, "8x,1n,8x" );
//                sets.Add( FunctionKey.Pacify, "0x" );
//                sets.Add( FunctionKey.Fear, "0x" );
//                sets.Add( FunctionKey.Stun, "0x" );
                sets.Add( FunctionKey.Rnd_Spawn_Item, "4x,1h,2n,20x" );
//                sets.Add( FunctionKey.NPCWipeHatelist, "0x" );
//                sets.Add( FunctionKey.CharmNPC, "0x" );
//                sets.Add( FunctionKey.Daze, "0x" );
//                sets.Add( FunctionKey.DestroyItem, "0x" );
                sets.Add( FunctionKey.Generate_Name, "2s,4x" );
                sets.Add( FunctionKey.Set_Government_Type, "1n" );
                sets.Add( FunctionKey.Text, "2s,4x"  );
                sets.Add( FunctionKey.Create_Apartment, "4x" );
//                sets.Add( FunctionKey.CanFly, "0x" );
                sets.Add( FunctionKey.Set_Flag, "2n" );
                sets.Add( FunctionKey.Clear_Flag, "2n" );
                sets.Add( FunctionKey.Unknown_1, "12n" );
//                sets.Add( FunctionKey.TeleportSavePoint, "0x" );
//                sets.Add( FunctionKey.Mezz, "0x" );
//                sets.Add( FunctionKey.SummonPlayer, "0x" );
//                sets.Add( FunctionKey.SummonTeamMates, "0x" );
                sets.Add( FunctionKey.Resist_Nano_Strain, "2n" );
//                sets.Add( FunctionKey.SaveHere, "0x" );
//                sets.Add( FunctionKey.ComboNameGen, "0x" );
                sets.Add( FunctionKey.Summon_Pet, "1h,2n,52x" );
//                sets.Add( FunctionKey.LandControlCreate, "0x" );
                sets.Add( FunctionKey.Scaling_Modify, "2n" );
                sets.Add( FunctionKey.Reduce_Nano_Strain_Duration, "2n" );
//                sets.Add( FunctionKey.DisableDefenseShield, "0x" );
                sets.Add( FunctionKey.Summon_Pets, "1n" );
                sets.Add( FunctionKey.Add_Action, "1n,1h,2n" );
                sets.Add( FunctionKey.Modify_Percentage, "2n" );
                sets.Add( FunctionKey.Drain_Hit, "5n" );
                sets.Add( FunctionKey.Lock_Perk, "3n" );
                sets.Add( FunctionKey.Side, "2n" );
                sets.Add( FunctionKey.NPC_Movement_Action, "4x"  );
                sets.Add( FunctionKey.Spawn_Monster_Rot, "1h,10n" );
                sets.Add( FunctionKey.Polymorph_Attack, "2n" );
                sets.Add( FunctionKey.Special_Hit, "4n" );
                sets.Add( FunctionKey.Attractor_Gfx_Effect, "52x" );
                sets.Add( FunctionKey.Cast_Nano_If_Possible, "1n" );
//                sets.Add( FunctionKey.SetAnchor, "0x" );
//                sets.Add( FunctionKey.RecallToAnchor, "0x" );
                sets.Add( FunctionKey.Talk, "1s,1n" );
                sets.Add( FunctionKey.Control_Hate, "8n" );
                sets.Add( FunctionKey.Delayed_Spawn_NPC, "1h,8n" );
                sets.Add( FunctionKey.Run_Script, "3n,1s" );
                sets.Add( FunctionKey.Add_Battlestation_Queue, "1n" );
                sets.Add( FunctionKey.Register_Control_Point, "1n" );
                sets.Add( FunctionKey.Add_Defense_Proc, "2n" );
//                sets.Add( FunctionKey.DestroyAllHumans, "0x" );
                sets.Add( FunctionKey.Spawn_Quest, "1h,2n" );
                sets.Add( FunctionKey.Add_Offense_Proc, "2n" );
                sets.Add( FunctionKey.Playfield_Nano, "1n" );
                sets.Add( FunctionKey.Solve_Quest, "1h" );
                sets.Add( FunctionKey.Knockback, "3n" );
//                sets.Add( FunctionKey.InstanceLock, "0x" );
//                sets.Add( FunctionKey.MindControl, "0x" );
                sets.Add( FunctionKey.Instanced_Player_City, "15n,1x" );
//                sets.Add( FunctionKey.ResetAllPerks, "0x" );
                sets.Add( FunctionKey.Create_City_Guest_Key, "2n,1x" );
                sets.Add( FunctionKey.Remove_Nano_Strain, "1n" );
                sets.Add( FunctionKey.Unknown_2, "2n" );
                sets.Add( FunctionKey.Unknown_3, "2n" );
                sets.Add( FunctionKey.Unknown_4, "1n" );
                sets.Add( FunctionKey.Pets_Cast_Nano, "1n" );
                sets.Add( FunctionKey.Cast_Nano_2, "1n" );
                sets.Add( FunctionKey.Charge, "1n" );
                sets.Add( FunctionKey.System_Text_2, "1s,4n" );
                sets.Add( FunctionKey.Unknown_5, "1s,1n" );
                sets.Add( FunctionKey.Unknown_6, "2n" );
                sets.Add( FunctionKey.Pay_To_Org, "2n" );
                sets.Add( FunctionKey.Fail_Quest, "1n" );
                sets.Add( FunctionKey.Send_Mail, "3s,1h,2n,4x" );
                sets.Add( FunctionKey.Smoke_Bomb, "2n" );
                sets.Add( FunctionKey.Smoke_Bomb_2, "2n" );
            }
            
            private static Dictionary<FunctionKey, string> sets = null;
            
            public static bool Exists( FunctionKey key )
            {
                return sets.ContainsKey( key );
            }
            
            public static string Get( FunctionKey key )
            {
                return sets[key];
            }
        }
        
        #endregion
    }
    
    public class ItemParseException : Exception
    {
        public ItemParseException() : base() { }
        public ItemParseException( string message ) : base( message ) { }
        public ItemParseException( string message, Exception innerExection ) : base( message, innerExection ) { }
        public ItemParseException( SerializationInfo info, StreamingContext context ) : base( info, context ) { }
    }
}
