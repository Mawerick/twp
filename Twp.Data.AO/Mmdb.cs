﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Text;

using Twp.Utilities;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of Mmdb.
    /// </summary>
    public class Mmdb
    {
        public Mmdb()
        {
        }

        public Mmdb( string fileName )
        {
            this.fileName = fileName;
            if( !this.fileName.EndsWith( ".mdb", true, null ) )
                this.fileName = Path.Combine( this.fileName, "cd_image", "text", "text.mdb" );
        }

        private string fileName;
        private List<Mmdb.Category> categories;
        private bool isLoaded = false;

        public List<Mmdb.Category> Categories
        {
            get { return this.categories; }
        }

        public bool IsLoaded
        {
            get { return this.isLoaded; }
        }

        public string GetString( int catId, int entryId )
        {
            if( this.categories == null || this.isLoaded == false )
                return null;
            
            foreach( Mmdb.Category cat in this.categories )
            {
                if( cat.Id == catId )
                {
                    Mmdb.Entry entry = cat.GetEntry( entryId );
                    if( entry != null )
                        return entry.Text;
                }
            }
            return null;
        }

        public Mmdb.Category GetCategory( int catId )
        {
            if( this.categories == null || this.isLoaded == false )
                return null;

            foreach( Mmdb.Category cat in this.categories )
            {
                if( cat.Id == catId )
                    return cat;
            }

            return null;
        }

        public static Mmdb Open( string path )
        {
            Mmdb mmdb = new Mmdb( path );
            if( mmdb.Open() )
                return mmdb;
            return null;
        }

        public bool Open()
        {
            this.isLoaded = false;
            if( !System.IO.File.Exists( this.fileName ) )
            {
                Log.Debug( "[Mmdb.Open] File {0} does not exist.", this.fileName );
                return false;
            }

            BinFile binFile = new BinFile( this.fileName );
            string filetype = binFile.ReadString( 4 );
            if( filetype.ToUpper() != "MMDB" )
            {
                Log.Debug( "[Mmdb.Open] Incorrect file type." );
                return false;
            }

            binFile.Position += 4;

            this.categories = new List<Mmdb.Category>();
            uint endOfCats = 0;
            uint endOfEnts = 0;

            try
            {
                uint currentOffset = 8;
                bool finished = false;
                while( !finished )
                {
                    int catID = 0;
                    uint offset = 0;
                    if( !ReadKey( binFile, ref catID, ref offset ) )
                        throw new Exception();

                    currentOffset += 8;

                    if( catID == -1 )
                    {
                        endOfCats = currentOffset;
                        endOfEnts = offset;
                        finished = true;
                    }
                    else
                    {
                        Category cat = new Category( catID, offset );
                        this.categories.Add( cat );
                    }
                }

                finished = false;
                while( !finished )
                {
                    binFile.Position = currentOffset;
                    int entryID = 0;
                    uint offset = 0;
                    string text = String.Empty;

                    if( !ReadKey( binFile, ref entryID, ref offset ) )
                        throw new Exception();

                    if( !ReadString( binFile, offset, ref text ) )
                        throw new Exception();

                    Entry entry = new Entry( entryID, offset, text );

                    Category category = null;
                    foreach( Category cat in this.categories )
                    {
                        if( cat.Offset > currentOffset )
                            break;
                        category = cat;
                    }
                    if( category != null )
                        category.Add( entry );

                    currentOffset += 8;
                    if( currentOffset >= endOfEnts )
                        break;
                }
            }
            catch
            {
                Log.Debug( "[Mmdb.Open] Error while reading file {0}", fileName );
                return false;
            }

            binFile.Close();
            Log.Debug( "[Mmdb.Open] File {0} loaded ok.", fileName );
            this.isLoaded = true;
            return true;
        }

        private static bool ReadKey( BinFile binFile, ref int catID, ref uint offset )
        {
            try
            {
                catID = binFile.ReadInt32();
                offset = binFile.ReadUInt32();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool ReadString( BinFile binFile, uint offset, ref string text )
        {
            try
            {
                binFile.Position = offset;
                while( true )
                {
                    byte[] bytes = binFile.ReadBytes( 1 );
                    if( (int) bytes[0] == 0 )
                        break;

                    text += Encoding.UTF8.GetString( bytes );
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public class Category
        {
            public Category( int id, uint offset )
            {
                this.id = id;
                this.offset = offset;
                this.entries = new List<Mmdb.Entry>();
            }

            private readonly int id;
            public int Id
            {
                get { return this.id; }
            }

            private readonly uint offset;
            public uint Offset
            {
                get { return this.offset; }
            }

            private List<Mmdb.Entry> entries;
            public Mmdb.Entry[] Entries
            {
                get { return this.entries.ToArray(); }
                set
                {
                    this.entries = new List<Mmdb.Entry>();
                    foreach( Mmdb.Entry entry in value )
                        this.entries.Add( entry );
                }
            }

            public bool Add( Mmdb.Entry entry )
            {
                if( entry == null )
                    return false;

                if( this.GetEntry( entry.Id ) != null )
                    return false;

                if( this.entries == null )
                    return false;

                this.entries.Add( entry );
                return true;
            }

            public Mmdb.Entry GetEntry( int id )
            {
                if( this.entries != null )
                {
                    foreach( Mmdb.Entry entry in this.entries )
                    {
                        if( entry.Id == id )
                            return entry;
                    }
                }
                return null;
            }

            public override string ToString()
            {
                return String.Format( "[{0}] ({1} entries)", this.id, this.entries.Count );
            }
        }

        public class Entry
        {
            public Entry( int id, uint offset, string text )
            {
                this.id = id;
                this.offset = offset;
                this.text = text;
            }

            private readonly int id;
            public int Id
            {
                get { return this.id; }
            }

            private readonly uint offset;
            public uint Offset
            {
                get { return this.offset; }
            }

            private readonly string text;
            public string Text
            {
                get { return this.text; }
            }

            public override string ToString()
            {
                return String.Format( "[{0}] {1}", this.id, this.text );
            }
        }
    }
}
