﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of Playfields.
    /// </summary>
    public class Playfields
    {
        private static Dictionary<int, string> playfieldMap = new Dictionary<int, string>();
        public static Dictionary<int, string> PlayfieldMap
        {
            get { return playfieldMap; }
        }
        
        private static bool isLoaded = false;
        public static bool IsLoaded
        {
            get { return isLoaded; }
            set { isLoaded = value; }
        }
        
        public static string GetName( int key )
        {
            if( isLoaded && playfieldMap.ContainsKey( key ) )
                return playfieldMap[key];
            return key.ToString();
        }
        
        public static bool Open( string path )
        {
            isLoaded = false;
            playfieldMap.Clear();
            
            string fileName = Path.Combine( path, "cd_image", "data", "launcher", "pfnrmap.dat" );
            if( !System.IO.File.Exists( fileName ) )
            {
                Log.Debug( "[Playfields.Open] File {0} does not exist.", fileName );
                return false;
            }
            
            string[] lines = System.IO.File.ReadAllLines( fileName );
            if( lines.Length <= 0 )
            {
                Log.Debug( "[Playfields.Open] File {0} is empty.", fileName );
                return false;
            }

            bool ok = true;
            for( int i = 0; i < lines.Length; i++ )
            {
                string[] parts = lines[i].Split( ';' );
                if( parts.Length != 2 )
                {
                    Log.Debug( "[Playfields.Open] Error parsing line {0}: {1}", i, lines[i] );
                    ok = false;
                    continue;
                }
                
                try
                {
                    if( parts[0].Contains( "-" ) )
                    {
                        string[] range = parts[0].Split( '-' );
                        if( range.Length != 2 )
                        {
                            Log.Debug( "[Playfields.Open] Error parsing line {0}: Failed to split {1}", i, parts[0] );
                            ok = false;
                            continue;
                        }
//                        Log.Debug( "[Playfields.Open] PF range: {0}-{1} Name: {2}", range[0], range[1], parts[1] );
                    }
                    else
                        playfieldMap.Add( Convert.ToInt32( parts[0] ), parts[1] );
                }
                catch
                {
                    Log.Debug( "[Playfields.Open] Error parsing line {0}: Failed to convert {1} to an integer.", i, parts[0] );
                    ok = false;
                }
            }
            isLoaded = true;
            return ok;
        }
    }
}
