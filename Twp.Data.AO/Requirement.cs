﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using Twp.Utilities;

namespace Twp.Data.AO
{
    public class Requirement
    {
        #region Constructor

        public Requirement(ItemInfo owner)
        {
            this.owner = owner;
        }

        public Requirement(int key, int value, int op, ItemInfo owner)
            : this(owner)
        {
            this.Key = key;
            this.Value = value;
            this.Op = op;
        }

        #endregion

        #region Private fields

        private int key;
        private int value;
        private int op;
        private ItemInfo owner;

        #endregion

        #region Properties

        public int Key
        {
            get { return this.key; }
            set { this.key = value; }
        }

        public int Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public int Op
        {
            get { return this.op; }
            set { this.op = value; }
        }

        #endregion

        #region Methods

        public string ToHtml(bool raw)
        {
            if (raw)
            {
                string val = ItemFormatter.GetAttrValue(this.key, this.op, this.value, this.owner.Mmdb);
                if (val == this.value.ToString())
                    return String.Format("{0} ({1}) {2} {3}",
                                          ItemFormatter.GetAttr(this.key, this.owner.Mmdb),
                                          this.key, (OperatorKey)this.op, val);
                else
                    return String.Format("{0} ({1}) {2} {3} ({4})",
                                          ItemFormatter.GetAttr(this.key, this.owner.Mmdb),
                                          this.key, (OperatorKey)this.op, val, this.value);
            }
            else
            {
                //Log.Debug( "[Requirement.ToHtml] Checking attribute: {0} ({1})", (AttributeKey) this.key, this.key );
                switch ((AttributeKey)this.key)
                {
                    case AttributeKey.Profession:
                        if (this.op == (int)OperatorKey.Equal)
                            return String.Format("<b>{0}</b>", this.owner.Mmdb.GetString(2004, this.value));
                        else if (this.op == (int)OperatorKey.NotEqual)
                            return String.Format("Not <b>{0}</b>", this.owner.Mmdb.GetString(2004, this.value));
                        break;

                    case AttributeKey.VisualProfession:
                        if (this.op == (int)OperatorKey.Equal)
                            return String.Format("Visual Profession: <b>{0}</b>", this.owner.Mmdb.GetString(2004, this.value));
                        else if (this.op == (int)OperatorKey.NotEqual)
                            return String.Format("Visual Profession: Not <b>{0}</b>", this.owner.Mmdb.GetString(2004, this.value));
                        break;

                    case AttributeKey.EquippedWeaponType:
                        int[] bits = Flag.GetBits(this.value);
                        string weaponType = String.Empty;
                        foreach (int bit in bits)
                            weaponType += this.owner.Mmdb.GetString(2013, bit) + " ";
                        if (String.IsNullOrEmpty(weaponType))
                            weaponType = ItemFormatter.CleanEnum((WeaponType)this.value);
                        if (this.op == (int)OperatorKey.BitAnd)
                        {
                            string text = this.owner.Mmdb.GetString(1005, 162952846);
                            if (!String.IsNullOrEmpty(text))
                                return text.Replace("%s", weaponType);
                            else
                                return "Equiped " + weaponType;
                        }
                        else if (this.op == (int)OperatorKey.NotBitAnd)
                        {
                            string text = this.owner.Mmdb.GetString(1005, 226077982);
                            if (!String.IsNullOrEmpty(text))
                                return text.Replace("%s", weaponType);
                            else
                                return "Not Equiped " + weaponType;
                        }
                        break;

                    case AttributeKey.Expansion:
                        string expansion = ItemFormatter.CleanEnum((ExpansionFlag)this.value);
                        if (this.op == (int)OperatorKey.BitAnd)
                        {
                            string text = this.owner.Mmdb.GetString(1005, 146655790);
                            if (!String.IsNullOrEmpty(text))
                                return text.Replace("%s", expansion);
                            else
                                return expansion;
                        }
                        break;

                    case AttributeKey.ExpansionPlayfield:
                        switch ((ExpansionPlayfield)this.value)
                        {
                            case ExpansionPlayfield.RubiKa:
                                return this.owner.Mmdb.GetString(1005, 267085425);
                            case ExpansionPlayfield.ShadowLands:
                                return this.owner.Mmdb.GetString(1005, 114151651);
                        }
                        break;

                    case AttributeKey.PlayfieldType:
                        switch ((OperatorKey)this.op)
                        {
                            case OperatorKey.NotBitAnd:
                                if (this.value == (int)PlayfieldType.Indoors)
                                    return this.owner.Mmdb.GetString(1005, 66916441);
                                break;
                        }
                        break;

                    case AttributeKey.MonsterType:
                        switch ((MonsterType)this.value)
                        {
                            case MonsterType.NPC:
                                if (this.op == (int)OperatorKey.Equal)
                                    return this.owner.Mmdb.GetString(1005, 255731522);
                                else if (this.op == (int)OperatorKey.NotEqual)
                                    return this.owner.Mmdb.GetString(1005, 17281922);
                                break;
                            case MonsterType.LandControlTower:
                                if (this.op == (int)OperatorKey.Equal)
                                    return this.owner.Mmdb.GetString(1005, 247233266);
                                else if (this.op == (int)OperatorKey.NotEqual)
                                    return this.owner.Mmdb.GetString(1005, 174826242);
                                break;
                        }
                        break;

                    case AttributeKey.NumFightingOpponents:
                        if (this.op == (int)OperatorKey.Equal && this.value == 0)
                            return this.owner.Mmdb.GetString(1005, 179552215);
                        break;

                    case AttributeKey.ShapeShift:
                        if (this.op == (int)OperatorKey.Equal && this.value == 0)
                            return this.owner.Mmdb.GetString(1005, 139231573);
                        break;

                    case AttributeKey.Side:
                        if (this.op == (int)OperatorKey.Equal)
                            return this.owner.Mmdb.GetString(2005, this.value);
                        else if (this.op == (int)OperatorKey.NotEqual)
                            return "Not " + this.owner.Mmdb.GetString(2005, this.value);
                        break;

                    case AttributeKey.Specialization:
                        switch ((Specialization)this.value)
                        {
                            case Specialization.First:
                                return this.owner.Mmdb.GetString(1005, 104594638);
                            case Specialization.Second:
                                return this.owner.Mmdb.GetString(1005, 103152846);
                            case Specialization.Third:
                                return this.owner.Mmdb.GetString(1005, 102366414);
                            case Specialization.Fourth:
                                return this.owner.Mmdb.GetString(1005, 101432526);
                        }
                        break;

                    case AttributeKey.ShadowBreedTemplate:
                        return String.Format("<b>{0}</b> {1} <span class='headline'>{2}</span>",
                                              ItemFormatter.GetAttr(this.key, this.owner.Mmdb),
                                              ItemFormatter.GetAttrOp(this.op),
                                              ItemFormatter.GetItemLink(this.value, this.owner.Database));

                    case AttributeKey.WaitState:
                        if (this.op == (int)OperatorKey.Equal && this.value == 0)
                            return this.owner.Mmdb.GetString(1005, 116138407);
                        else if (this.op == (int)OperatorKey.Equal && this.value == 2)
                            return this.owner.Mmdb.GetString(1005, 239200100);
                        break;

                    case AttributeKey.WornItem:
                        int offset = -1;
                        if (this.op == (int)OperatorKey.NotBitAnd)
                            offset = 0;
                        if (this.op == (int)OperatorKey.BitAnd)
                            offset = 1;

                        if (offset >= 0)
                        {
                            switch ((WornItem)this.value)
                            {
                                case WornItem.Basic_CyberDeck:
                                    return this.owner.Mmdb.GetString(1005, 508 + offset);
                                case WornItem.Augmented_CyberDeck:
                                    return this.owner.Mmdb.GetString(1005, 506 + offset);
                                case WornItem.Jobe__Chipped_CyberDeck:
                                    return this.owner.Mmdb.GetString(1005, 504 + offset);
                                case WornItem.Izgimmer__Modified_CyberDeck:
                                    return this.owner.Mmdb.GetString(1005, 502 + offset);
                                case WornItem.Grid_Armor:
                                    return this.owner.Mmdb.GetString(1005, 500 + offset);
                                case WornItem.Profession_Nanodeck:
                                    return this.owner.Mmdb.GetString(1005, 510 + offset);
                            }
                        }
                        break;
                }
                //Log.Debug( "[Requirement.ToHtml] Checking operator: {0} ({1})", (OperatorKey) this.op, this.op );
                switch ((OperatorKey)this.op)
                {
                    case OperatorKey.HasWornItem:
                    case OperatorKey.HasWieldedItem:
                        string text = this.owner.Mmdb.GetString(1005, 237116148);
                        if (!String.IsNullOrEmpty(text))
                            return text.Replace("%s", ItemFormatter.GetItemLink(this.value, this.owner.Database));
                        else
                            return "Wearing " + ItemFormatter.GetItemLink(this.value, this.owner.Database);

                    case OperatorKey.HasNotWornItem:
                    case OperatorKey.HasNotWieldedItem:
                        text = this.owner.Mmdb.GetString(1005, 93771044);
                        if (!String.IsNullOrEmpty(text))
                            return text.Replace("%s", ItemFormatter.GetItemLink(this.value, this.owner.Database));
                        else
                            return "Not wearing " + ItemFormatter.GetItemLink(this.value, this.owner.Database);

                    case OperatorKey.HasFormula:
                        return String.Format("Must have uploaded {0}", ItemFormatter.GetNanoLink(this.value, this.owner.Database));

                    case OperatorKey.HasNotFormula:
                        return String.Format("Must not have uploaded {0}", ItemFormatter.GetNanoLink(this.value, this.owner.Database));

                    case OperatorKey.HasRunningNano:
                        return String.Format("Must have {0} running.", ItemFormatter.GetNanoLink(this.value, this.owner.Database));

                    case OperatorKey.HasNotRunningNano:
                        return String.Format("Must not have {0} running.", ItemFormatter.GetNanoLink(this.value, this.owner.Database));

                    case OperatorKey.HasRunningNanoLine:
                        return String.Format("Must have a <b>{0}</b> nano running", this.owner.Mmdb.GetString(2009, this.value));

                    case OperatorKey.HasNotRunningNanoLine:
                        return String.Format("Must not have a <b>{0}</b> nano running", this.owner.Mmdb.GetString(2009, this.value));

                    case OperatorKey.HasPerk:
                        return String.Format("{0} {1}", this.owner.Mmdb.GetString(100, 226935915), ItemFormatter.GetPerkLink(this.value));

                    case OperatorKey.HasNotPerk:
                        return String.Format("{0} {1}", this.owner.Mmdb.GetString(100, 39801003), ItemFormatter.GetPerkLink(this.value));

                    case OperatorKey.IsPerkLocked:
                        text = this.owner.Mmdb.GetString(1005, 175924644);
                        if (!String.IsNullOrEmpty(text))
                            return text.Replace("%s", ItemFormatter.GetPerkLink(this.value));
                        else
                            return String.Format("Perk {0} locked.", ItemFormatter.GetPerkLink(this.value));

                    case OperatorKey.IsPerkUnlocked:
                        text = this.owner.Mmdb.GetString(1005, 187467428);
                        if (!String.IsNullOrEmpty(text))
                            return text.Replace("%s", ItemFormatter.GetPerkLink(this.value));
                        else
                            return String.Format("Perk {0} unlocked.", ItemFormatter.GetPerkLink(this.value));

                    case OperatorKey.CanTeleport:
                        return this.owner.Mmdb.GetString(1005, 263089668);

                    case OperatorKey.FlyingIsAllowed:
                        return this.owner.Mmdb.GetString(100, 44660212);

                    case OperatorKey.IsPlayerOwnedPet:
                        return this.owner.Mmdb.GetString(1005, 38097604);

                    case OperatorKey.AlliedInCombat:
                        return this.owner.Mmdb.GetString(1005, 74285572);

                    case OperatorKey.SameAsSelectedTarget:
                        return String.Format("Must has the same <b>{0}</b> as the selected target.", ItemFormatter.GetAttr(this.key, this.owner.Mmdb));
                }
                int val = this.value;
                return String.Format("<b>{0}</b> {1} <span class='headline'>{2}</span>",
                                      ItemFormatter.GetAttr(this.key, this.owner.Mmdb),
                                      ItemFormatter.GetAttrOp(this.op, ref val),
                                      ItemFormatter.GetAttrValue(this.key, this.op, val, this.owner.Mmdb));
            }
        }

        public override string ToString()
        {
            return Text.StripHtmlTags(ToHtml(false));
        }

        #endregion
    }
}
