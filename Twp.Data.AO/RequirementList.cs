﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace Twp.Data.AO
{
    /// <summary>
    /// Description of RequirementList.
    /// </summary>
    public class RequirementList : List<Requirement>
    {
        private ItemInfo owner;

        public RequirementList( ItemInfo owner ) : base()
        {
            this.owner = owner;
        }

        public string ToHtml( bool raw )
        {
            if( this.Count <= 0 )
                return null;

            int lastOp = -1;
            int indent = 0;
            bool hasTarget = false;
            string html = String.Empty;
//            Log.Debug( "[RequirementList.ToHtml] Reqs: {0}", this.Count );
            for( int i = this.Count - 1; i >= 0; i-- )
            {
                Requirement req = this[i];
//                Log.Debug( "[RequirementList.ToHtml] Checking req.Op: {0} ({1})", (OperatorKey) req.Op, req.Op );
                switch( (OperatorKey) req.Op )
                {
                    case OperatorKey.And:
                    case OperatorKey.Or:
                        if( lastOp == -1 )
                            lastOp = req.Op;
                        else if( lastOp != req.Op )
                        {
                            lastOp = req.Op;
                            indent++;
                            html += "<div class='indent'>";
                        }
                        break;

                    case OperatorKey.Not:
                        if( i > 0 )
                        {
                            Requirement nextReq = this[i - 1];
                            if( nextReq.Op == (int) OperatorKey.Equal )
                                nextReq.Op = (int) OperatorKey.NotEqual;
                            else if( nextReq.Op == (int) OperatorKey.BitAnd )
                                nextReq.Op = (int) OperatorKey.NotBitAnd;
                            else if( nextReq.Op == (int) OperatorKey.HasWornItem )
                                nextReq.Op = (int) OperatorKey.HasNotWornItem;
                            else if( nextReq.Op == (int) OperatorKey.HasWieldedItem )
                                nextReq.Op = (int) OperatorKey.HasNotWieldedItem;
                            else if( nextReq.Op == (int) OperatorKey.HasFormula )
                                nextReq.Op = (int) OperatorKey.HasNotFormula;
                            else if( nextReq.Op == (int) OperatorKey.HasRunningNano )
                                nextReq.Op = (int) OperatorKey.HasNotRunningNano;
                            else if( nextReq.Op == (int) OperatorKey.HasRunningNanoLine )
                                nextReq.Op = (int) OperatorKey.HasNotRunningNanoLine;
                            else if( nextReq.Op == (int) OperatorKey.HasItem )
                                nextReq.Op = (int) OperatorKey.HasNotItem;
                            else if( nextReq.Op == (int) OperatorKey.HasPerk )
                                nextReq.Op = (int) OperatorKey.HasNotPerk;
                            this[i - 1] = nextReq;
                        }
                        break;

                    case OperatorKey.OnTarget:
                    case OperatorKey.OnSelf:
                    case OperatorKey.OnUser:
                    case OperatorKey.OnValidTarget:
                    case OperatorKey.OnInvalidTarget:
                    case OperatorKey.OnValidUser:
                    case OperatorKey.OnInvalidUser:
                    case OperatorKey.OnGeneralBeholder:
                    case OperatorKey.OnCaster:
                    case OperatorKey.SomethingOrOther:
                        while( indent-- > 0 )
                            html += "</div>";
                        html += ItemFormatter.GetTarget( req.Op, this.owner.Mmdb ) + "<br>";
                        hasTarget = true;
                        break;

                    default:
                        string htmlReq = req.ToHtml( raw );
                        if( !hasTarget )
                        {
                            html = ItemFormatter.GetTarget( (int) OperatorKey.OnSelf, this.owner.Mmdb ) + "<br>" + html;
                            hasTarget = true;
                        }
                        if( i > 0 && lastOp != -1 )
                            htmlReq += String.Format( " <i>{0}</i>", ItemFormatter.GetAttrOp( lastOp ) );
                        html += String.Format( "<div class='indent'>{0}</div>", htmlReq );
                        break;
                }
            }
            while( indent-- > 0 )
                html += "</div>";
            return html;
        }
    }
}
