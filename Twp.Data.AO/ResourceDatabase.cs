﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  Most of this code comes from XRDB4
//  Copyright (C) William "Xyphos" Scott <TheGreatXyphos@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using Twp.Utilities;

namespace Twp.Data.AO
{
    public class ResourceDatabase : IDisposable
    {
        private List<BinFile> dataFiles;
        private Dictionary<int, Dictionary<int, long>> records;

        private uint blockOffset;
        private uint dataFileSize;

        public const int ItemType = 0xF4254;
        public const int NanoType = 0xFDE85;
        public const int IconType = 0xF6958;

        private int count = 0;
        public int Count
        {
            get { return this.count; }
        }

        public ResourceDatabase( string path )
        {
            //grab the datafiles in their proper order
            this.records = new Dictionary<int, Dictionary<int, long>>();
            this.dataFiles = new List<BinFile>();

            Log.Debug( "[Database] Starting index parsing." );

            string dbPath = Path.Combine( path, "cd_image", "data", "db" );
            Regex regEx = new Regex( @"ResourceDatabase\.dat(\.\d{3})?$", RegexOptions.IgnoreCase );
            List<string> fileNames = new List<string>();

            foreach( string file in System.IO.Directory.GetFiles( dbPath ) )
            {
                if( regEx.IsMatch( file ) )
                {
                    fileNames.Add( file );
                    Log.Debug( "[Database] Adding datafile: {0}", file );
                }
            }
            fileNames.Sort();
            foreach( string dataFile in fileNames )
            {
                this.dataFiles.Add( new BinFile( dataFile ) );
            }
            fileNames = null;
            
            //grab the index file
            string indexFile = Path.Combine( dbPath, "ResourceDatabase.idx" );
            BinFile index = new BinFile( System.IO.File.ReadAllBytes( indexFile ) );

            //grab the block offsets
            this.blockOffset = index.ReadUInt32At( 0x0c );
            Log.Debug( "[Database] Block offset: {0}", this.blockOffset );

            //grab datafile size
            this.dataFileSize = index.ReadUInt32At( 0xB8 );
            if( dataFileSize < 0 )
                dataFileSize = this.dataFiles[0].Length;
            Log.Debug( "[Database] Datafile Size: {0}", this.dataFileSize );

            //goto first block
            index.Position = index.ReadUInt32At( 0x48 );

            //start reading
            uint forwardLink = index.ReadUInt32();
            while( forwardLink > 0 )
            {
                Application.DoEvents();

                //skip back link
                index.ReadInt32();

                // # of entries in block
                short entries = index.ReadInt16();
                index.Position += 18; // skip ahead
                this.count += entries;

                while( entries-- > 0 )
                {
                    Application.DoEvents();

                    UInt64ToUInt32 recordPosition = new UInt64ToUInt32();
                    recordPosition.RightUInt32 = index.ReadUInt32();
                    recordPosition.LeftUInt32 = index.ReadUInt32();
                    int recordType = index.ReadInt32Swapped();
                    int recordId = index.ReadInt32Swapped();
                    
                    if( !this.records.ContainsKey( recordType ) )
                    {
                        this.records.Add( recordType, new Dictionary<int, long>() );
                    }
                    this.records[recordType].Add( recordId, (long) recordPosition.UInt64Value );
                }

                forwardLink = index.ReadUInt32At( forwardLink );
            }

            index.Close();
        }
        
        /// <summary>
        ///
        /// </summary>
        ~ResourceDatabase()
        {
            this.Dispose( false );
        }
        
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }
        
        private void Dispose( bool disposing )
        {
            if( disposing )
            {
                foreach( BinFile bf in this.dataFiles )
                {
                    bf.Close();
                    bf.Dispose();
                }
            }
        }

        public int[] RecordTypes()
        {
            int[] types = new int[this.records.Keys.Count];
            this.records.Keys.CopyTo( types, 0 );
            return types;
        }

        public int[] GetRecordIds( int type )
        {
            int[] types = new int[this.records[type].Keys.Count];
            this.records[type].Keys.CopyTo( types, 0 );
            return types;
        }

        public bool IsValidType( int type )
        {
            return this.records.ContainsKey( type );
        }

        public bool IsValidId( int type, int id )
        {
            if( this.IsValidType( type ) )
                return this.records[type].ContainsKey( id );
            return false;
        }

        public byte[] GetRecordData( int type, int id )
        {
            return this.GetRecordData( type, id, false );
        }

        public byte[] GetRecordData( int type, int id, bool decode )
        {
            long pos = this.records[type][id];
            byte[] buffer = this.ReadSegment( 0x22, pos );
//            Log.Debug( "[Database.GetRecordData] Header:" );
//            Log.Debug( ByteArrayToString( buffer ) );
            BinFile header = new BinFile( buffer );
            int recType = header.ReadInt32At( 0x0A );
            if( recType != type )
                throw new System.IO.InvalidDataException( "Invalid Record Type" );

            int recId = header.ReadInt32();
            if( recId != id )
                throw new System.IO.InvalidDataException( "Invalid Record Id" );

            uint recLen = header.ReadUInt32At( 0x12 ) - 0x0c; // subtract header size;
            byte[] result = this.ReadSegment( recLen );

            if( decode )
            {
                ulong seed = (ulong) id;
                int i = 0;
                while( i < result.Length )
                {
                    seed *= 0x1012003;
                    seed %= 0x4e512dc8fUL;
                    result[i++] ^= (byte) seed;
                }
            }
            return result;
        }

        private int dataFileIndex;

        private byte[] ReadSegment( uint count )
        {
            return this.ReadSegment( count, long.MaxValue );
        }

        private byte[] ReadSegment( uint count, long position )
        {
            if( position != long.MaxValue )
            {
                //calc which datafile the record is in
                this.dataFileIndex = (int) Math.Floor( (double) position / (double) this.dataFileSize );
                if( this.dataFileIndex > 0 )
                    position -= (this.dataFileSize - this.blockOffset) * this.dataFileIndex;
                this.dataFiles[this.dataFileIndex].Position = position;
            }

            byte[] buffer = new byte[count];
            int read = this.dataFiles[this.dataFileIndex].Read( buffer, 0, (int) count );
            if( read == -1 )
                throw new System.IO.EndOfStreamException();

            if( read != count )
            {
                this.dataFiles[++this.dataFileIndex].Position = this.blockOffset;
                this.dataFiles[this.dataFileIndex].Read( buffer, read, (int) count - read );
            }

            return buffer;
        }
        
        private string ByteArrayToString( byte[] data )
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            int i = 0;
            foreach( byte b in data )
            {
                sb.AppendFormat( "{0:X2}", b );
                if( ++i == 8 )
                {
                    sb.AppendLine();
                    i = 0;
                }
                else if( i == 4 )
                    sb.Append( "  " );
                else
                    sb.Append( ' ' );
            }
            return sb.ToString();
        }
    }
}

