﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

using Twp.Utilities;

namespace Twp.Data.AO
{
	public class SQLiteDatabase
	{
		#region Constructors

		public SQLiteDatabase()
		{
		}

		public SQLiteDatabase( string fileName )
		{
			this.fileName = fileName;
		}

		#endregion
		
		#region Fields

		private SQLiteConnection connection;
		private SQLiteCommand command;
		private SQLiteDataReader reader;
		private string fileName;
		private bool isLoaded = false;

		#endregion
		
		#region Properties

		public string FileName
		{
			get { return this.fileName; }
		}
		
		public bool IsLoaded
		{
			get { return this.isLoaded; }
		}

		#endregion

		#region Methods

		public void Open()
		{
			this.Open( false );
		}

		public void Open( string fileName )
		{
			this.fileName = fileName;
			this.Open( false );
		}
		
		public void Open( string fileName, bool failIfMissing )
		{
			this.fileName = fileName;
			this.Open( failIfMissing );
		}

		public void Open( bool failIfMissing )
		{
			Log.Debug( "[SQLiteDatabase.Open] Opening database {0}", this.fileName );
			this.connection = new SQLiteConnection( String.Format( "Data Source={0}; Version=3; FailIfMissing={1};", this.fileName, failIfMissing  ) );
			this.connection.Open();
			if( this.connection.State == ConnectionState.Open )
			{
				this.command = this.connection.CreateCommand();
				this.isLoaded = true;
			}
		}

		public void Close()
		{
			Log.Debug( "[SQLiteDatabase.Close] Closing database {0}", this.fileName );
			if( this.reader != null )
			{
				this.reader.Close();
				this.reader.Dispose();
				this.reader = null;
			}
			if( this.command != null )
			{
				this.command.Dispose();
				this.command = null;
			}
			if( this.connection != null )
			{
				this.connection.Close();
				this.connection.Dispose();
				this.connection = null;
			}
			this.isLoaded = false;
		}

		public void StartTransaction()
		{
			this.NonQuery( "BEGIN TRANSACTION;" );
		}

		public void CommitTransaction()
		{
			this.NonQuery( "COMMIT TRANSACTION;" );
		}

		public void RollbackTransaction()
		{
			this.NonQuery( "ROLLBACK TRANSACTION;" );
		}

		public void Optimize()
		{
			this.NonQuery( "VACUUM;" );
		}

		public void NonQuery( string sql, params string[] args )
		{
			this.Execute( sql, args );
		}

		public void Insert( string table, params string[] args )
		{
			this.NonQuery( String.Format( "INSERT INTO {0} VALUES('{1}');", table, string.Join( "', '", args ) ) );
		}

		public int Insert( string table, params SQLiteArgument[] args )
		{
			if( args == null || args.Length == 0 )
				return -1;

			if( this.command == null )
				return -1;

			List<string> cols = new List<string>();
			List<string> ids = new List<string>();
			SQLiteParameter param;
			foreach( SQLiteArgument arg in args )
			{
				cols.Add( arg.Name );
				ids.Add( ":" + arg.Name );
				param = new SQLiteParameter( arg.Name, arg.Type );
				param.Value = arg.Value;
				this.command.Parameters.Add( param );
			}
			this.command.CommandText = String.Format( "INSERT INTO {0} ({1}) VALUES({2}); SELECT last_insert_rowid();",
													 table, String.Join( ", ", cols.ToArray() ), String.Join( ", ", ids.ToArray() ) );
			object rowid = this.command.ExecuteScalar();
			return rowid != null ? Convert.ToInt32( rowid ) : -1;
		}

		public void Update( string table, params SQLiteArgument[] args )
		{
			if( args == null || args.Length == 0 )
				return;

			if( this.command == null )
				return;

			List<string> cols = new List<string>();
			List<string> ids = new List<string>();
			SQLiteParameter param;
			foreach( SQLiteArgument arg in args )
			{
				cols.Add( arg.Name );
				ids.Add( ":" + arg.Name );
				param = new SQLiteParameter( arg.Name, arg.Type );
				param.Value = arg.Value;
				this.command.Parameters.Add( param );
			}
			this.command.CommandText = String.Format( "REPLACE INTO {0} ({1}) VALUES({2});",
													 table, String.Join( ", ", cols.ToArray() ), String.Join( ", ", ids.ToArray() ) );
			this.command.ExecuteNonQuery();
		}

		public SQLiteResults Query( string sql, params string[] args )
		{
			if( this.command == null )
				return null;

			SQLiteResults results = new SQLiteResults();
			lock( this.command )
			{
				this.Execute( sql, args );
				int count = reader.FieldCount;// -1;
				while( this.reader.Read() )
				{
					SQLiteResult result = new SQLiteResult();
					for( int i = 0; i < count; i++ )
					{
						string key = this.reader.GetName( i ).ToString();
						object value = this.reader.GetValue( i );
						result.Add( key, value );
					}
					results.Add( result );
				}
				this.reader.Close();
			}
			return results;
		}

		public SQLiteResult QuerySingle( string sql, params string[] args )
		{
			SQLiteResults row = this.Query( sql, args );
			return (row != null && row.Count > 0) ? row[0] : new SQLiteResult();
		}

		public static string Escape( string str )
		{
			return str.Replace( "'", "''" ).Replace( "\0", "" );
		}

		private void Execute( string sql, params string[] args )
		{
			if( this.reader != null && !this.reader.IsClosed )
				this.reader.Close();

			if( this.command == null )
				return;
			
			this.command.CommandText = String.Format( sql, args );

			if( ( sql.Trim().Substring( 0, 6 ).ToUpper() == "SELECT" ) )
				this.reader = this.command.ExecuteReader();
			else
				this.command.ExecuteNonQuery();
		}

		#endregion
	}

	public class SQLiteArgument
	{
		private readonly string name = string.Empty;
		public string Name
		{
			get { return this.name; }
		}

		private readonly DbType type = DbType.Object;
		public DbType Type
		{
			get { return this.type; }
		}

		private readonly object value = null;
		public object Value
		{
			get { return this.value; }
		}

		public SQLiteArgument( string name, DbType type, object value )
		{
			this.name = name;
			this.type = type;
			this.value = value;
		}
	}

	public class SQLiteResults : List<SQLiteResult>
	{
	}

	public class SQLiteResult : Dictionary<string, object>
	{
	}
}
