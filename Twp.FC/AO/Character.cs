﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace Twp.FC.AO
{
    public class Character
    {
        public Character( DirectoryInfo dirInfo )
        {
            this.topPath = dirInfo.Parent.Parent.Parent.FullName;
            this.dirInfo = dirInfo;
            string idStr = this.dirInfo.Name.Substring( 4 );
            this.id = Convert.ToUInt32( idStr );
        }

        private string topPath;
        private DirectoryInfo dirInfo;

        private readonly uint id = 0;
        public uint ID
        {
            get { return this.id; }
        }
        
        private string name;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        private Chat chat;
        public Chat Chat
        {
            get
            {
                if( this.chat == null )
                {
                    this.chat = new Chat();
                    this.chat.Read( Path.Combine( this.dirInfo.FullName, "Chat" ) );
                }
                return this.chat;
            }
        }

        private Containers containers;
        public Containers Containers
        {
            get
            {
                if( this.containers == null )
                {
                    this.containers = new Containers();
                    this.containers.Read( Path.Combine( this.dirInfo.FullName, "Containers" ) );
                }
                return this.containers;
            }
        }

        private DockAreas dockAreas;
        public DockAreas DockAreas
        {
            get
            {
                if( this.dockAreas == null )
                {
                    this.dockAreas = new DockAreas();
                    this.dockAreas.Read( Path.Combine( this.dirInfo.FullName, "DockAreas" ) );
                }
                return this.dockAreas;
            }
        }

        private XmlFile prefs;
        public XmlFile Prefs
        {
            get
            {
                if( this.prefs == null )
                {
                    string filePath = Path.Combine( this.dirInfo.FullName, "Prefs.xml" );
                    if( File.Exists( filePath ) )
                    {
                        this.prefs = new XmlFile( filePath );
                    }
                    else
                    {
                        this.prefs = Gui.Default( this.topPath ).CharPrefs;
                        this.prefs.FileName = filePath;
                    }
                }
                return this.prefs;
            }
        }

        private CfgFile config;
        public CfgFile Config
        {
            get
            {
                if( this.config == null )
                {
                    this.config = new CfgFile();
                    this.config.Read( Path.Combine( this.dirInfo.FullName, "Char.cfg" ) );
                }
                return this.config;
            }
        }

        private IgnoreList ignoreList;
        public IgnoreList IgnoreList
        {
            get
            {
                if( this.ignoreList == null )
                {
                    this.ignoreList = new IgnoreList();
                    this.ignoreList.Read( this.dirInfo.FullName );
                }
                return this.ignoreList;
            }
        }

        private TextMacros textMacros;
        public TextMacros TextMacros
        {
            get
            {
                if( this.textMacros == null )
                {
                    this.textMacros = new TextMacros();
                    this.textMacros.Read( this.dirInfo.FullName );
                }
                return this.textMacros;
            }
        }
        
        private int[] nanos;
        public int[] Nanos
        {
            get
            {
                if( this.nanos == null )
                    this.ParseNanos();
                return this.nanos;
            }
        }
        
        private static Regex nanoRegex = new Regex( @"Identity\(53019,(\d+)\)" );
        
        private void ParseNanos()
        {
            List<int> nanoList = new List<int>();
            
            XmlElement root = this.Prefs.DocumentElement;
            if( root == null )
                return;

            XmlNode items = root.SelectSingleNode( "/Root/Archive[@name='NanoViewConfig']/Archive[@name='item_position_map']/Array[@name='item_id']" );
            if( items != null )
            {
                foreach( XmlNode item in items.ChildNodes )
                {
                    string idValue = (item as XmlElement).GetAttribute( "value" );
                    int pos = idValue.IndexOf( ',' );
                    if( pos >= 0 )
                    {
                        idValue = idValue.Substring( pos + 1, idValue.Length - pos - 2 );
                        nanoList.Add( Convert.ToInt32( idValue ) );
                    }
                }
            }

            this.nanos = nanoList.ToArray();
        }

        public override string ToString()
        {
            if( String.IsNullOrEmpty( this.name ) )
            {
                if( this.id < 0 )
                    return base.ToString();
                else
                    return this.id.ToString();
            }
            else
                return this.name;
        }
    }
}
