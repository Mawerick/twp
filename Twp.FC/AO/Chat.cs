﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.FC.AO
{
	public class Chat
	{
		public Chat()
		{
		}
		
		public void Read( string path )
		{
			DirectoryInfo dirInfo = new DirectoryInfo( Path.Combine( path, "Windows" ) );
			DirectoryInfo[] winDirs = dirInfo.GetDirectories( "Window*" );
			foreach( DirectoryInfo winDir in winDirs )
			{
				ChatWindow window = new ChatWindow( winDir );
				this.windows.Add( window );
			}
		}

		private List<ChatWindow> windows = new List<ChatWindow>();
		public List<ChatWindow> Windows
		{
			get { return this.windows; }
		}
	}

	public class ChatWindow
	{
		public ChatWindow( DirectoryInfo dirInfo )
		{
			this.dirInfo = dirInfo;
			this.config.Load( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Config.xml" ) );
			this.inputHistory.Load( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "InputHistory.xml" ) );
		}

		private DirectoryInfo dirInfo;

		private XmlFile config = new XmlFile();
		public XmlFile Config
		{
			get { return this.config; }
		}

		private XmlFile inputHistory = new XmlFile();
		public XmlFile InputHistory
		{
			get { return this.inputHistory; }
		}
	}
}
