﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Twp.FC.AO
{
	public class Containers
	{
		public Containers()
		{
		}

		public void Read( string path )
		{
			DirectoryInfo dirInfo = new DirectoryInfo( path );
			this.bank = new Container( Twp.Utilities.Path.Combine( dirInfo.FullName, "Bank.xml" ) );
			this.inventory = new Container( Twp.Utilities.Path.Combine( dirInfo.FullName, "Inventory.xml" ) );
			this.specialActions = new ShortcutBar( Twp.Utilities.Path.Combine( dirInfo.FullName, "SpecialActions.xml" ) );

			this.bags.Clear();
            this.oldBags = 0;
            this.staleBags = 0;

			FileInfo[] bagFiles = dirInfo.GetFiles( "Container_51017x*" );
			foreach( FileInfo bagFile in bagFiles )
			{
				Container bag = new Container( bagFile.FullName );
				this.bags.Add( bag.Id, bag );
				if( bag.IsOld )
					this.oldBags++;
                if( bag.IsStale )
                    this.staleBags++;
			}

			this.lootBags.Clear();
			FileInfo[] lootBagFiles = dirInfo.GetFiles( "TempContainer_*" );
			foreach( FileInfo lootBagFile in lootBagFiles )
			{
				Container lootBag = new Container( lootBagFile.FullName );
				this.lootBags.Add( lootBag );
			}

			this.shortcutBars.Clear();
			FileInfo[] shortcutBarFiles = dirInfo.GetFiles( "ShortcutBar_*" );
			foreach( FileInfo shortcutBarFile in shortcutBarFiles )
			{
				ShortcutBar shortcutBar = new ShortcutBar( shortcutBarFile.FullName );
				this.shortcutBars.Add( shortcutBar );
			}
		}

		private Container bank;
		public Container Bank
		{
			get { return this.bank; }
		}

		private Container inventory;
		public Container Inventory
		{
			get { return this.inventory; }
		}

		private Dictionary<int, Container> bags = new Dictionary<int, Container>();
		public Dictionary<int, Container> Bags
		{
			get { return this.bags; }
		}

		private int oldBags = 0;
		public int OldBags
		{
			get { return this.oldBags; }
		}

        private int staleBags = 0;
        public int StaleBags
        {
            get { return this.staleBags; }
        }

		private List<Container> lootBags = new List<Container>();
		public List<Container> LootBags
		{
			get { return this.lootBags; }
		}

		private ShortcutBar specialActions;
		public ShortcutBar SpecialActions
		{
			get { return this.specialActions; }
		}

		private List<ShortcutBar> shortcutBars = new List<ShortcutBar>();
		public List<ShortcutBar> ShortcutBars
		{
			get { return this.shortcutBars; }
		}
	}

	public class ContainerBase
	{
		public ContainerBase( string fileName )
		{
			this.fileInfo = new FileInfo( fileName );
			this.xmlFile.Load( this.fileInfo.FullName );
		}

		protected FileInfo fileInfo;
		protected XmlFile xmlFile = new XmlFile();
	}

	public class Container : ContainerBase
	{
		public Container( string fileName ) : base( fileName )
		{
			this.Parse();
		}

		private static int oldAge = 30;
        public static int OldAge
        {
            get { return oldAge; }
            set { oldAge = value; }
        }

		private static int staleAge = 90;
        public static int StaleAge
        {
            get { return staleAge; }
            set { staleAge = value; }
        }

		protected virtual void Parse()
		{
            if( this.fileInfo.Name.StartsWith( "Container_51017x" ) )
    		    this.id = Convert.ToInt32( this.fileInfo.Name.Substring( 16, this.fileInfo.Name.Length - 20 ) );
            else if( this.fileInfo.Name.StartsWith( "TempContainer_" ) )
    		    this.id = Convert.ToInt32( this.fileInfo.Name.Substring( 14, this.fileInfo.Name.Length - 18 ) );
            else
                this.id = -1;
            
			// check age.
			TimeSpan span = DateTime.UtcNow - this.fileInfo.LastWriteTimeUtc;
			this.isOld = span.Days > oldAge;
			this.isStale = span.Days > staleAge;

			XmlElement root = this.xmlFile.DocumentElement;
			if( root == null )
				return;

            foreach( XmlElement element in root.ChildNodes )
            {
                if( element.HasAttributes )
                {
                    string key = element.GetAttribute( "name" );
                    string value = element.GetAttribute( "value" );
                    switch( key )
                    {
                        case "container_name":
                            this.name = XmlFile.StripQuotes( value );
                            break;

                        case "DockableViewDockName":
                            this.dockArea = XmlFile.StripQuotes( value );
                            break;

                        default:
                            break;
                    }
                }
            }
		}

        private int id;
		public int Id
		{
			get { return this.id; }
		}

		private string name;
		public string Name
		{
			get { return this.name; }
		}

		private string dockArea;
		public string DockArea
		{
			get { return this.dockArea; }
		}

		private bool isOld;
		public bool IsOld
		{
			get { return this.isOld; }
		}

		private bool isStale;
		public bool IsStale
		{
			get { return this.isStale; }
		}

        public override string ToString()
        {
            if( String.IsNullOrEmpty( this.name ) )
                return base.ToString();
            else
                return this.name;
        }
	}

	public class ShortcutBar : ContainerBase
	{
		public ShortcutBar( string fileName ) : base( fileName )
		{
		    string idStr = fileName.Substring( fileName.Length - 5, 1 );
		    if( Twp.Utilities.Extensions.IsNumeric( idStr ) )
    		    this.id = Convert.ToInt32( idStr );
		    else
		        this.id = -1;
		    this.Parse();
		}

		protected void Parse()
		{
		    this.items.Clear();
		    
			XmlElement root = this.xmlFile.DocumentElement;
			if( root == null )
				return;

			XmlNode itemID = root.SelectSingleNode( "/Archive//Array[@name='item_id']" );
			XmlNode itemPos = root.SelectSingleNode( "/Archive//Array[@name='item_pos']" );
			if( itemID != null && itemPos != null )
			{
    			for( int i = 0; i < itemID.ChildNodes.Count; i++ )
    			{
    			    string idValue = (itemID.ChildNodes[i] as XmlElement).GetAttribute( "value" );
    			    string posValue = (itemPos.ChildNodes[i] as XmlElement).GetAttribute( "value" );
    			    
    			    this.items.Add( new ShortcutItem( idValue, posValue ) );
    			}
    			this.items.Sort();
			}
		}
		
		private int id;
		public int ID
		{
		    get { return this.id; }
		}

		private List<ShortcutItem> items = new List<ShortcutItem>();
		public List<ShortcutItem> Items
		{
		    get { return this.items; }
		}
	}

	public enum ShortcutItemType : int
	{
	    Unknown = 0,
	    TextMacro = 51081,
	    NanoProgram = 53019,
	    Action = 57008,
	}

	public class ShortcutItem : IComparable<ShortcutItem>
	{
	    public ShortcutItem()
	    {
	    }
	    
	    public ShortcutItem( string idStr, string posStr )
	    {
	        string[] idArray = idStr.Substring( 9, idStr.Length - 10 ).Split( ',' );
	        string[] posArray = posStr.Substring( 7, posStr.Length - 8 ).Split( ',' );
	        this.bar = Convert.ToInt32( posArray[0] );
	        this.slot = Convert.ToInt32( posArray[1] );
	        this.type = Convert.ToInt32( idArray[0] );
	        this.id = Convert.ToInt32( idArray[1] );
	    }

	    private int bar;
	    public int Bar
	    {
            get { return this.bar; }
            set { this.bar = value; }
        }

	    private int slot;
        public int Slot
        {
            get { return this.slot; }
            set { this.slot = value; }
        }

	    private int type;
        public ShortcutItemType Type
        {
            get { return (ShortcutItemType) this.type; }
            set { this.type = (int) value; }
        }

	    private int id;
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

		#region IComparable implementation

        public int CompareTo( ShortcutItem other )
		{
            int comp = this.Bar.CompareTo( other.Bar );
            if( comp == 0 )
                return this.Slot.CompareTo( other.Slot );
            else
                return comp;
		}

		#endregion
	}
}
