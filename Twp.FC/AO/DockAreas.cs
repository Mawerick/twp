﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Twp.Utilities;

namespace Twp.FC.AO
{
	public class DockAreas
	{
		public DockAreas()
		{
		}

		public void Read( string path )
		{
			this.items.Clear();
			this.oldAreas = 0;
            this.staleAreas = 0;
            this.emptyAreas = 0;

			DirectoryInfo dirInfo = new DirectoryInfo( path );
			this.rollupArea = new DockArea( Twp.Utilities.Path.Combine( dirInfo.FullName, "RollupArea.xml" ) );
			this.planetMap = new DockArea( Twp.Utilities.Path.Combine( dirInfo.FullName, "PlanetMapViewConfig.xml" ) );
			FileInfo[] dockFiles = dirInfo.GetFiles( "DockArea*" );
			foreach( FileInfo dockFile in dockFiles )
			{
				DockArea dockArea = new DockArea( dockFile.FullName );
				this.items.Add( dockArea.Name, dockArea );
				if( dockArea.IsOld )
					this.oldAreas++;
                if( dockArea.IsStale )
                    this.staleAreas++;
                if( dockArea.Identities.Count == 0 )
                    this.emptyAreas++;
			}
		}

		private DockArea rollupArea;
		public DockArea RollupArea
		{
			get { return this.rollupArea; }
		}

		private DockArea planetMap;
		public DockArea PlanetMap
		{
			get { return this.planetMap; }
		}

		private Dictionary<string, DockArea> items = new Dictionary<string, DockArea>();
		public Dictionary<string, DockArea> Items
		{
			get { return this.items; }
		}

		private int oldAreas = 0;
		public int OldAreas
		{
			get { return this.oldAreas; }
		}

        private int staleAreas = 0;
        public int StaleAreas
        {
            get { return this.staleAreas; }
        }

        private int emptyAreas = 0;
        public int EmptyAreas
        {
            get { return this.emptyAreas; }
        }
	}

	public class DockArea : ContainerBase
	{
		public DockArea( string fileName ) : base( fileName )
		{
			this.Parse();
			// check age.
			TimeSpan span = DateTime.UtcNow - this.fileInfo.LastWriteTimeUtc;
			this.isOld = span.Days > Container.OldAge;
			this.isStale = span.Days > Container.StaleAge;
		}

		private void Parse()
		{
			XmlElement root = this.xmlFile.DocumentElement;
			if( root == null )
				return;

            foreach( XmlElement element in root.ChildNodes )
            {
                if( element.HasAttributes )
                {
                    string name = element.GetAttribute( "name" );
                    string value = element.GetAttribute( "value" );

                    switch( name )
                    {
                        case "dock_config":
                            this.ParseConfig( element );
                            break;

                        case "dock_name":
                            this.name = XmlFile.StripQuotes( value );
                            break;

                        case "dock_type":
                            this.type = XmlFile.StripQuotes( value );
                            break;

                        default:
                            Log.Debug( "[DockArea] Unknown element: {0}", name );
                            break;
                    }
//                    break;
                }
            }

            if( String.IsNullOrEmpty( this.name ) )
            {
                Log.Debug( "[DockArea] No name given for file {0}", this.fileInfo.FullName );
                this.name = this.fileInfo.Name;
            }
		}

		private void ParseConfig( XmlElement archive )
		{
			foreach( XmlElement element in archive.ChildNodes )
			{
                if( element.HasAttributes )
                {
                    string name = element.GetAttribute( "name" );

                    if( element.Name == "Array" )
                    {
                        switch( name )
                        {
                            case "dock_node_configs":
                                // used in rollup area
                                break;

                            case "docked_view_identities":
                                // used if there are multiple windows connected to this dockarea
                                this.identities.Clear();
                                foreach( XmlElement arrayItem in element.ChildNodes )
                                {
                                    if( arrayItem.HasAttributes )
                                    {
                                        string ident = arrayItem.GetAttribute( "value" );
                                        this.identities.Add( XmlFile.StripQuotes( ident ) );
                                    }
                                }
                                break;

                            default:
                                break;
                        }
                    }
                    else
                    {
                        string value = element.GetAttribute( "value" );
                        switch( name )
                        {
                            case "docked_view_identities":
                                // used if there's only one window connected to this dockarea
                                this.identities.Clear();
                                this.identities.Add( XmlFile.StripQuotes( value ) );
                                break;

                            default:
                                break;
                        }
                    }
                }
			}
		}

		private string name;
		public string Name
		{
			get { return this.name; }
		}

        private string type;
		public string Type
		{
			get { return this.type; }
		}

		private List<string> identities = new List<string>();
		public List<string> Identities
		{
			get { return this.identities; }
		}

		private bool isOld;
		public bool IsOld
		{
			get { return this.isOld; }
		}

		private bool isStale;
		public bool IsStale
		{
			get { return this.isStale; }
		}

        public override string ToString()
        {
            if( String.IsNullOrEmpty( this.name ) )
                return base.ToString();
            else
                return this.name;
        }
	}
}
