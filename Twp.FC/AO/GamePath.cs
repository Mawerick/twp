﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.FC.AO
{
	/// <summary>
	/// Provides static functions for AO path related tasks.
	/// </summary>
	public static class GamePath
	{
		/// <summary>
		/// Attempts to confirm the validity of an Anarchy Online installation.
		/// A Folder Browser dialog is presented repeatedly until the user either
		/// selects a valid path or clicks the "Cancel" button.
		/// </summary>
		/// <param name="path">A string representing the installation path. If this is not null orn an empty string,
		/// it is used to provide the dialog with a default selection.</param>
		/// <returns>true if a valid path is found, otherwise, false.</returns>
		public static bool Confirm( ref string path )
		{
            return GamePathBase.Confirm( ref path, Game.Name, Game.VerifyFile );
		}

		/// <summary>
		/// Displays a <see cref="System.Windows.Forms.FolderBrowserDialog"/> to the user.
		/// No path validation is performed.
		/// </summary>
		/// <param name="path">A string representing the installation path. If this is not null or an empty string,
		/// it is used to provide the dialog with a default selection.</param>
		/// <returns>true if a path was selected, otherwise, false.</returns>
		public static bool Browse( ref string path )
		{
            return GamePathBase.Browse( ref path, Game.Name );
        }

		/// <summary>
		/// Checks the path for the existance of the "version.id" file.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>true if the path is valid, otherwise, false.</returns>
		public static bool IsValid( string path )
		{
            return GamePathBase.IsValid( path, Game.VerifyFile );
		}
	}
}
