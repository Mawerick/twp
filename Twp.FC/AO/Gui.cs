﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Twp.FC.AO
{
	public class Gui
	{
		internal Gui()
		{
			this.xmlFile = null;
			this.dirInfo = null;
			this.name = null;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="Twp.FC.AO.Gui"/> class.
		/// </summary>
		/// <param name="xmlFile">The XmlFile containg the user's Prefs.xml data.</param>
		public Gui( string path, XmlFile xmlFile )
		{
			this.xmlFile = xmlFile;
			
            this.name = "Default";
            XmlElement element = this.xmlFile.GetElement( "Value", "GUIName" );
            if( element != null )
                this.name = XmlFile.StripQuotes( element.GetAttribute( "value" ) );

            FileInfo fi = new FileInfo( this.xmlFile.FileName );
			this.dirInfo = new DirectoryInfo( GetPath( path, this.name ) );
		}

		/// <summary>
		/// Creates a new instance of the <see cref="Twp.AO.Gui"/> class.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		public Gui( string path, string gui )
		{
			if( Gui.IsValid( path, gui ) )
			{
				this.xmlFile = Prefs.Read( path );
				this.dirInfo = new DirectoryInfo( GetPath( path, gui ) );
				this.name = gui;
			}
		}

		public static Gui Default( string path )
		{
			Gui gui = new Gui();
			gui.name = "Default";
			gui.dirInfo = new DirectoryInfo( GetPath( path, "Default" ) );
			return gui;
		}

		private XmlFile xmlFile;
//        private XmlElement nameElement;
		private DirectoryInfo dirInfo;
		public DirectoryInfo DirInfo
		{
			get { return this.dirInfo; }
		}

		/// <summary>
		/// Checks if the path of this <see cref="Gui"/> exists.
		/// </summary>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public bool IsValid()
		{
		    return this.dirInfo.Exists;
		}

		/// <summary>
		/// Checks if the path of <paramref name="gui"/> exists in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( string path, string gui )
		{
			return Directory.Exists( GetPath( path, gui ) );
		}

		/// <summary>
		/// Checks if the path of <paramref name="gui"/> exists.
		/// </summary>
		/// <param name="gui">A <see cref="Gui"/> containing information about a GUI folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( Gui gui )
		{
			return gui.dirInfo.Exists;
		}
		
		/// <summary>
		/// Returns a string representing the file [path]\\cd_image\\gui\\[gui]\\[file].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <param name="file">A string representing a file name.</param>
		/// <returns>a string representing the file [path]\\cd_image\\gui\\[gui]\\[file].</returns>
		public static string GetFile( string path, string gui, string file )
		{
			return Path.Combine( GetPath( path, gui ), file );
		}

		/// <summary>
		/// Returns a string representing the folder [path]\\cd_image\\gui\\[gui].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <returns>a string representing the folder [path]\\cd_image\\gui\\[gui].</returns>
		public static string GetPath( string path, string gui )
		{
		    if( gui == "Default" )
		        return Twp.Utilities.Path.Combine( path, "cd_image", "GUI", gui );
		    else
    		    return Path.Combine( GetBasePath( path ), gui );
		}
		
		private static string GetBasePath( string path )
		{
		    string prefsPath = Prefs.GetBasePath( path );
		    string guiPath = Path.Combine( prefsPath, "GUI" );
		    if( !Directory.Exists( guiPath ) )
		       guiPath = Twp.Utilities.Path.Combine( path, "cd_image", "GUI" );
		    
		    Twp.Utilities.Log.Debug( "[Gui.GetBasePath] GuiPath: {0}", guiPath );
		    return guiPath;
		}
		
		/// <summary>
		/// Gets a list of GUI's that exist in the GUI base dir in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>An array holding the GUI's, or null if no GUI's were found.</returns>
		public static string[] List( string path )
		{
			if( String.IsNullOrEmpty( path ) )
				return null;

			DirectoryInfo di = new DirectoryInfo( GetBasePath( path ) );
			if( !di.Exists )
				return null;

		    List<string> guiDirs = new List<string>();
			DirectoryInfo[] subs = di.GetDirectories();
			foreach( DirectoryInfo sub in subs )
			{
			    guiDirs.Add( sub.Name );
			}
			if( !guiDirs.Contains( "Default" ) )
			    guiDirs.Insert( 0, "Default" );
			return guiDirs.ToArray();
		}
		
		public static bool CreateDirectory( string path, string gui )
		{
		    if( String.IsNullOrEmpty( path ) )
		        throw new ArgumentNullException( "path" );
		    if( String.IsNullOrEmpty( gui ) )
		        throw new ArgumentNullException( "gui" );
            if( IsValid( path, gui ) )
                return true;
            
            string guiPath = GetPath( path, gui );
            Directory.CreateDirectory( guiPath );
            return true;
		}
		
		/// <summary>
		/// Copies the content of a GUI to another. Both GUIs should exist. Use <see cref="CreateDirectory"/> to create a new one if necessary.
		/// </summary>
		/// <param name="path">The base AO install path.</param>
		/// <param name="source">The name of the source GUI.</param>
		/// <param name="destination">The name of the destination GUI.</param>
		/// <returns>True if the operation was a success. Otherwise, false.</returns>
		public static bool Copy( string path, string source, string destination )
		{
		    return Copy( path, source, destination, null );
		}
		
		/// <summary>
		/// Copies the content of a GUI to another. Both GUIs should exist. Use <see cref="CreateDirectory"/> to create a new one if necessary.
		/// </summary>
		/// <param name="path">The base AO install path.</param>
		/// <param name="source">The name of the source GUI.</param>
		/// <param name="destination">The name of the destination GUI.</param>
		/// <param name="fileNames">A list of filenames to copy.</param>
		/// <returns>True if the operation was a success. Otherwise, false.</returns>
		public static bool Copy( string path, string source, string destination, string[] fileNames )
		{
		    string sourcePath = GetPath( path, source );
		    string destinationPath = GetPath( path, destination );
		    
		    if( fileNames == null || fileNames.Length < 1 )
		    {
		        DirectoryInfo sourceDI = new DirectoryInfo( sourcePath );
		        Copy( sourceDI, destinationPath );
		    }
		    else
		    {
		        foreach( string fileName in fileNames )
		        {
		            string sourceFileName = Path.Combine( sourcePath, fileName );
		            string destinationFileName = Path.Combine( destinationPath, fileName );
		            File.Copy( sourceFileName, destinationFileName, true );
		        }
		    }
		    return false;
		}
		
		private static void Copy( DirectoryInfo source, string destination )
		{
            FileInfo[] files = source.GetFiles();
            foreach( FileInfo file in files )
            {
                string destinationFileName = Path.Combine( destination, file.Name );
                file.CopyTo( destinationFileName, true );
            }
            
            DirectoryInfo[] directories = source.GetDirectories();
            foreach( DirectoryInfo dir in directories )
            {
                string destinationPath = Path.Combine( destination, dir.Name );
                Directory.CreateDirectory( destinationPath );
                Copy( dir, destinationPath );
            }
		}

		private XmlFile mainPrefs = null;
		public XmlFile MainPrefs
		{
			get
			{
				if( this.mainPrefs == null )
					this.mainPrefs = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "MainPrefs.xml" ) );
				return this.mainPrefs;
			}
		}

		private XmlFile loginPrefs = null;
		public XmlFile LoginPrefs
		{
			get
			{
				if( this.loginPrefs == null )
					this.loginPrefs = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "LoginPrefs.xml" ) );
				return this.loginPrefs;
			}
		}

		private XmlFile charPrefs = null;
		public XmlFile CharPrefs
		{
			get
			{
				if( this.charPrefs == null )
					this.charPrefs = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "CharPrefs.xml" ) );
				return this.charPrefs;
			}
		}

		private XmlFile textColors = null;
		public XmlFile TextColors
		{
			get
			{
				if( this.textColors == null )
					this.textColors = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "TextColors.xml" ) );
				return this.textColors;
			}
		}

		private string name;
		/// <summary>
		/// Gets or sets the selected GUI Name in a user's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string Name
		{
		    get { return this.name; }
            set { this.name = value; }
		}
 	}
}
