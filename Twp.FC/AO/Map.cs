﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Twp.Utilities;

namespace Twp.FC.AO
{
	public class Map
	{
        /// <summary>
        /// Creates a new instance of the <see cref="Twp.FC.AO.Map"/> class.
        /// </summary>
        public Map()
		{
            this.xmlFile = null;
			this.dirInfo = null;
		}
        
		/// <summary>
		/// Creates a new instance of the <see cref="Twp.FC.AO.Map"/> class.
		/// </summary>
		/// <param name="xmlFile">The XmlFile containg the account's Prefs.xml data.</param>
		public Map( XmlFile xmlFile )
		{
			this.xmlFile = xmlFile;
            this.dirInfo = null;
		}

		/// <summary>
		/// Creates a new instance of the <c ref="Twp.AO.Map">Map</c> class.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a Map folder.</param>
		public Map( string path, string map )
		{
            if( Map.IsValid( path, map ) )
            {
                this.dirInfo = new DirectoryInfo( GetPath( path, map ) );
            }
            else
                this.dirInfo = null;
            this.xmlFile = null;
		}

		public Map( DirectoryInfo dirInfo )
		{
			this.dirInfo = dirInfo;
            this.xmlFile = null;
		}

		public static Map DefaultRK( string path )
		{
			return new Map( path, "normal" );
		}

		public static Map DefaultSL( string path )
		{
			return new Map( path, "Shadowlands" );
		}

        private XmlFile xmlFile;
        private XmlElement rkElement;
        private XmlElement slElement;
		private DirectoryInfo dirInfo;
		public DirectoryInfo DirInfo
		{
			get { return this.dirInfo; }
		}

		/// <summary>
		/// Checks if the path of <paramref name="map"/> exists in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a Map folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( string path, string map )
		{
			return Directory.Exists( GetPath( path, map ) );
		}

		/// <summary>
		/// Checks if the path of <paramref name="map"/> exists.
		/// </summary>
		/// <param name="map">A <see cref="Map"/> containing information about a Map folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( Map map )
		{
			return map.dirInfo.Exists;
		}

		/// <summary>
		/// Returns a string representing the file [path]\\cd_image\\textures\\PlanetMap\\[map]\\[file].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a map folder.</param>
		/// <param name="file">A string representing a file name.</param>
		/// <returns>a string representing the file [path]\\cd_image\\textures\\PlanetMap\\[map]\\[file].</returns>
		public static string GetFile( string path, string map, string file )
		{
			return Twp.Utilities.Path.Combine( GetPath( path, map ), file );
		}

		/// <summary>
		/// Returns a string representing the folder [path]\\cd_image\\textures\\PlanetMap\\[map].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a map folder.</param>
		/// <returns>a string representing the folder [path]\\cd_image\\textures\\PlanetMap\\[map].</returns>
		public static string GetPath( string path, string map )
		{
			return Twp.Utilities.Path.Combine( path, "cd_image", "textures", "PlanetMap", map );
		}

		/// <summary>
		/// Gets a list of maps that exist in the map base dir in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>An array holding the maps, or null if no maps were found.</returns>
		public static List<Map> List( string path )
		{
			if( String.IsNullOrEmpty( path ) )
				return null;

			DirectoryInfo di = new DirectoryInfo( Twp.Utilities.Path.Combine( path, "cd_image", "textures", "PlanetMap" ) );
			if( !di.Exists )
				return null;

			DirectoryInfo[] subs = di.GetDirectories();
			if( subs.Length > 0 )
			{
				List<Map> maps = new List<Map>();
				foreach( DirectoryInfo mapDir in subs )
				{
					Map map = new Map( mapDir );
					maps.Add( map );
				}
				return maps;
			}
			else
				return null;
		}

		/// <summary>
		/// Gets a list of map definition files that exist in the map dir in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a map folder.</param>
		/// <returns>An array holding the map files, or null if no map files were found.</returns>
		public static string[] GetFiles( DirectoryInfo dirInfo )
		{
			if( !dirInfo.Exists )
				return null;

			FileInfo[] files = dirInfo.GetFiles( "*.txt" );
			if( files.Length > 0 )
			{
				string[] mapFiles = new string[files.Length];
				for( int i = 0; i < files.Length; ++i )
				{
					mapFiles[i] = files[i].Name;
				}
				return mapFiles;
			}
			else
				return null;
		}

		private List<MapFile> files = null;
		public List<MapFile> Files
		{
			get
			{
				if( this.files == null )
				{
					this.files = new List<MapFile>();
					foreach( FileInfo fi in this.dirInfo.GetFiles( "*.txt" ) )
					{
						MapFile mapFile = MapFile.Read( fi );
						if( mapFile != null && mapFile.IsValid )
							this.files.Add( mapFile );
					}
				}
				return this.files;
			}
		}

		/// <summary>
		/// Gets or sets the selected Rubi-Ka map in an account's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string RubiKa
		{
			get
			{
                if( this.RKElement != null )
                    return XmlFile.StripQuotes( this.RKElement.GetAttribute( "value" ) );
                else
                    return null;
			}
			set
			{
                if( this.RKElement != null )
                    this.RKElement.SetAttribute( "value", XmlFile.AddQuotes( value ) );
			}
		}

        private XmlElement RKElement
        {
            get
            {
                if( this.rkElement == null )
                {
                    if( this.xmlFile == null || this.xmlFile.DocumentElement == null )
                        return null;

                    this.rkElement = this.xmlFile.GetElement( "Value", "PlanetMapIndexFile" );
                }
                return this.rkElement;
            }
        }

		/// <summary>
		/// Gets or sets the selected Rubi-Ka map in an account's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string Shadowlands
		{
            get
            {
                if( this.SLElement != null )
                    return XmlFile.StripQuotes( this.SLElement.GetAttribute( "value" ) );
                else
                    return null;
            }
            set
            {
                if( this.SLElement != null )
                    this.SLElement.SetAttribute( "value", XmlFile.AddQuotes( value ) );
            }
        }

        private XmlElement SLElement
        {
            get
            {
                if( this.slElement == null )
                {
                    if( this.xmlFile == null || this.xmlFile.DocumentElement == null )
                        return null;

                    this.slElement = this.xmlFile.GetElement( "Value", "ShadowlandMapIndexFile" );
                }
                return this.slElement;
            }
        }
	}
}
