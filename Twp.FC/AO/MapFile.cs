﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Twp.FC.AO
{
	public class MapFile : IEquatable<MapFile>, IComparable<MapFile>
	{
		public MapFile()
		{
		}

		private FileInfo fileInfo;
		public FileInfo FileInfo
		{
			get { return this.fileInfo; }
		}

		public string FileName
		{
			get
			{
				return String.Format( "{0}/{1}", this.fileInfo.Directory.Name, this.fileInfo.Name );
			}
		}

		private string name;
		public string Name
		{
			get { return this.name; }
		}

		private string type;
		public string Type
		{
			get { return this.type; }
		}

		private CfgFile config;
		public CfgFile Config
		{
			get { return this.config; }
		}

		private XmlFile coordsFile;
		public XmlFile CoordFile
		{
			get { return this.coordsFile; }
		}

		private List<MapData> dataFiles = new List<MapData>();
		public List<MapData> DataFiles
		{
			get { return this.dataFiles; }
		}

		public bool IsValid
		{
			get
			{
				if( String.IsNullOrEmpty( this.name ) || String.IsNullOrEmpty( this.type ) )
					return false;
				else
					return true;
			}
		}

		public static MapFile Read( FileInfo fileInfo )
		{
			if( !fileInfo.Exists )
				return null;

			MapFile mapFile = new MapFile();
			mapFile.fileInfo = fileInfo;
			mapFile.config = new CfgFile();
			string[] lines = File.ReadAllLines( fileInfo.FullName );
			foreach( string line in lines )
			{
				string[] tokens = line.Split( new char[] { ' ', '=' }, 2 );
				if( tokens.Length != 2 )
					continue;

				MapData data = null;
				switch( tokens[0] )
				{
					case "Name":
						mapFile.name = XmlFile.StripQuotes( tokens[1] );
						break;

					case "Type":
						mapFile.type = tokens[1];
						break;

					case "CoordsFile":
						string path = Twp.Utilities.Path.Combine( fileInfo.Directory.Parent.FullName, tokens[1] );
						mapFile.coordsFile = new XmlFile( path );
						break;

					case "File":
						data = new MapData();
						mapFile.dataFiles.Add( data );
						break;

					case "TextureSize":
						if( data != null )
							data.TextureSize = Convert.ToInt32( tokens[1] );
						break;

					case "Size":
						if( data != null )
						{
							int[] ints = MapData.StringToIntArray( tokens[1] );
							if( ints.Length != 2 )
								break;
							data.Size = new Size( ints[0], ints[1] );
						}
						break;

					case "Tiles":
						if( data != null )
						{
							int[] ints = MapData.StringToIntArray( tokens[1] );
							if( ints.Length != 2 )
								break;
							data.Tiles = new Size( ints[0], ints[1] );
						}
						break;

					case "MapRect":
						if( data != null )
						{
							int[] ints = MapData.StringToIntArray( tokens[1] );
							if( ints.Length != 4 )
								break;
							data.Rectangle = new Rectangle( ints[0], ints[1], ints[2], ints[3] );
						}
						break;

					case "FilePos":
						if( data != null )
						{
							data.FilePos.Add( Convert.ToInt32( tokens[1] ) );
						}
						break;

					default:
						//Twp.Utilities.Log.Debug( "[MapFile.Read] Unknown token {0} in line {1}", tokens[0], line );
						return null;
				}
			}

			return mapFile;
		}

		public override string ToString()
		{
			return this.name;
		}

		#region IEquatable

		public override int GetHashCode()
		{
			return this.name.GetHashCode();
		}

		public bool Equals( MapFile other )
		{
			if( other == null )
				return false;
			else
				return this.name.Equals( other.name );
		}

		public override bool Equals( object obj )
		{
			MapFile other = obj as MapFile;
			if( other != null )
				return this.Equals( other );

			string text = obj as string;
			if( text != null )
			{
				if( this.name.Equals( text ) )
					return true;
				else
					return this.FileName.Equals( text );
			}

			return false;
		}

		public static bool operator ==( MapFile map1, MapFile map2 )
		{
			if( object.ReferenceEquals( map1, map2 ) )
				return true;
			if( object.ReferenceEquals( map1, null ) )
				return false;
			if( object.ReferenceEquals( map2, null ) )
				return false;
			return map1.Equals( map2 );
		}

		public static bool operator !=( MapFile map1, MapFile map2 )
		{
			if( object.ReferenceEquals( map1, map2 ) )
				return false;
			if( object.ReferenceEquals( map1, null ) )
				return true;
			if( object.ReferenceEquals( map2, null ) )
				return true;
			return !map1.Equals( map2 );
		}

		#endregion

		#region IComparable implementation

		public int CompareTo( MapFile other )
		{
			return this.name.CompareTo( other.name );
		}

		#endregion

		public class MapData
		{
			private string fileName;
			public string FileName
			{
				get { return this.fileName; }
				set { this.fileName = value; }
			}

			private int textureSize;
			public int TextureSize
			{
				get { return this.textureSize; }
				set { this.textureSize = value; }
			}

			private Size size;
			public Size Size
			{
				get { return this.size; }
				set { this.size = value; }
			}

			private Size tiles;
			public Size Tiles
			{
				get { return this.tiles; }
				set { this.tiles = value; }
			}

			private Rectangle rectangle;
			public Rectangle Rectangle
			{
				get { return this.rectangle; }
				set { this.rectangle = value; }
			}

			private List<int> filePos = new List<int>();
			public List<int> FilePos
			{
				get { return this.filePos; }
			}

			public static int[] StringToIntArray( string text )
			{
				string[] ts = text.Split( new char[] { ' ' } );
				List<int> ints = new List<int>();
				foreach( string t in ts )
				{
					ints.Add( Convert.ToInt32( t ) );
				}
				return ints.ToArray();
			}
		}
	}
}
