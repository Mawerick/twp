﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;

namespace Twp.FC.AO
{
    public class Prefs
    {
        /// <summary>
        /// Checks the path for the existance of the "Prefs" folder.
        /// </summary>
        /// <param name="path">A string representing an Anarchy Online installation path.</param>
        /// <returns>true if the path is valid, otherwise, false.</returns>
        public static bool IsValid( string path )
        {
            return File.Exists( Path.Combine( GetPath( path ), "Prefs.xml" ) );
        }

        /// <summary>
        /// Reads the content of the Prefs.xml file from the specified path.
        /// </summary>
        /// <param name="path">The path to look into.</param>
        /// <returns>An <see cref="Twp.FC.XmlFile"/> containing the content of the Prefs.xml file.</returns>
        public static XmlFile Read( string path )
        {
            XmlFile xmlFile = new XmlFile();
            string filePath = Path.Combine( GetPath( path ), "Prefs.xml" );
            if( File.Exists( filePath ) )
            {
                xmlFile.Load( filePath );
            }
            else
            {
                xmlFile = Gui.Default( path ).MainPrefs;
                xmlFile.FileName = filePath;
            }
            return xmlFile;
        }

        /// <summary>
        /// Returns a string representing the folder [path]\\Prefs.
        /// </summary>
        /// <param name="path">A string representing an Anarchy Online installation path.</param>
        /// <returns>a string representing the folder [path]\\Prefs.</returns>
        public static string GetPath( string path )
        {
            return Path.Combine( GetBasePath( path ), "Prefs" );
        }

        public static string GetBasePath( string path )
        {
            string basePath = Twp.Utilities.Path.GetLocalAppDataPath( "Funcom", "Anarchy Online" );
            if( Directory.Exists( basePath ) )
            {
                DirectoryInfo di = new DirectoryInfo( path );
                string parentPath = di.Parent.FullName.Replace( '\\', '/' );
                basePath = Path.Combine( basePath, Twp.Utilities.Path.GetHash( parentPath ) );
                basePath = Path.Combine( basePath, di.Name );
                Twp.Utilities.Log.Debug( "[Prefs.GetBasePath] Path: {0}", basePath );
                return basePath;
            }
            else
            {
                // Pre-18.6 fallback.
                Twp.Utilities.Log.Debug( "[Prefs.GetBasePath] Path: {0}", path );
                return path;
            }
        }
    }
}
