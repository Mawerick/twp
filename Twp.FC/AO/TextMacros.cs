﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace Twp.FC.AO
{
	public class TextMacros
	{
		public TextMacros()
		{
		}

		private List<TextMacro> macros = new List<TextMacro>();
		public List<TextMacro> Macros
		{
			get { return this.macros; }
		}

		public void Read( string path )
		{
			this.macros.Clear();
			string fileName = System.IO.Path.Combine( path, "TextMacro.bin" );
			if( !System.IO.File.Exists( fileName ) )
			    return;
			
			BinFile binFile = new BinFile( fileName );

			int len;
			int count = binFile.ReadInt32Swapped();
			for( int n = 0; n < count; n++ )
			{
				TextMacro macro = new TextMacro();
				macro.Id = binFile.ReadInt32Swapped();

				len = binFile.ReadInt32Swapped();
				macro.Name = binFile.ReadString( len );

				len = binFile.ReadInt32Swapped();
				macro.Command = binFile.ReadString( len );
				this.macros.Add( macro );
			}
		}
		
		public TextMacro GetMacro( int id )
		{
		    foreach( TextMacro macro in this.macros )
		    {
		        if( macro.Id == id )
		            return macro;
		    }
		    return default(TextMacro);
		}
	}

	public class TextMacro
	{
		private int id;
		public int Id
		{
			get { return this.id; }
			set { this.id = value; }
		}

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private string command;
		public string Command
		{
			get { return this.command; }
			set { this.command = value; }
		}

		public override string ToString()
		{
			return String.Format( "[ID::{0}] {1}: {2}", this.id, this.name, this.command );
		}
	}
}
