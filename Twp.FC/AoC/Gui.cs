﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using Twp.Utilities;

namespace Twp.FC.AoC
{
	public class Gui
	{
		internal Gui()
		{
			this.dirInfo = null;
		}
        
		/// <summary>
		/// Creates a new instance of the <see cref="Twp.FC.AoC.Gui"/> class.
		/// </summary>
		/// <param name="path">A string representing an Age of Conan installation path.</param>
		public Gui( string path )
		{
			if( Gui.IsValid( path ) )
			{
                Log.Debug( "[Gui] {0} is valid", path );
				this.dirInfo = new DirectoryInfo( GetPath( path ) );
			}
            else
                Log.Debug( "[Gui] {0} is invalid!", path );
		}

        public static Gui Default( string path )
        {
            Gui gui = new Gui();
            gui.dirInfo = new DirectoryInfo( GetDefaultPath( path ) );
            return gui;
        }

		private DirectoryInfo dirInfo;
		public DirectoryInfo DirInfo
		{
			get { return this.dirInfo; }
		}

		/// <summary>
		/// Checks if the folder "Customized" exists in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Age of Conan installation path.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( string path )
		{
			return Directory.Exists( GetPath( path ) );
		}

		/// <summary>
		/// Checks if the path of <paramref name="gui"/> exists.
		/// </summary>
		/// <param name="gui">A <see cref="Gui"/> containing information about a GUI folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( Gui gui )
		{
			return gui.dirInfo.Exists;
		}

		/// <summary>
		/// Returns a string representing the file [path]\\Data\\gui\\Customized\\[file].
		/// </summary>
		/// <param name="path">A string representing an Age of Conan installation path.</param>
		/// <param name="file">A string representing a file name.</param>
        /// <param name="copy">If true, and the file is missing, it is copied from the default GUI.</param>
		/// <returns>a string representing the file [path]\\Data\\gui\\Customized\\[file].</returns>
		public static string GetFile( string path, string file, bool copy )
		{
            string fileName = Twp.Utilities.Path.Combine( GetPath( path ), file );
            if( copy && !File.Exists( fileName ) )
            {
                string source = Twp.Utilities.Path.Combine( GetDefaultPath( path ), file );
                File.Copy( source, fileName );
            }
            return fileName;
		}

		/// <summary>
		/// Returns a string representing the file [path]\\Data\\Gui\\Default\\[file].
		/// </summary>
		/// <param name="path">A string representing an Age of Conan installation path.</param>
		/// <param name="file">A string representing a file name.</param>
		/// <returns>a string representing the file [path]\\Data\\Gui\\Default\\[file].</returns>
		public static string GetDefaultFile( string path, string file )
		{
            return Twp.Utilities.Path.Combine( GetDefaultPath( path ), file );
		}

		/// <summary>
		/// Returns a string representing the folder [path]\\Data\\gui\\Customized.
		/// </summary>
		/// <param name="path">A string representing an Age of Conan installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <returns>a string representing the folder [path]\\Data\\gui\\Customized.</returns>
		public static string GetPath( string path )
		{
			return Twp.Utilities.Path.Combine( path, "Data", "Gui", "Customized" );
		}

        /// <summary>
        /// Returns a string representing the folder [path]\\Data\\gui\\Default.
        /// </summary>
        /// <param name="path">A string representing an Age of Conan installation path.</param>
        /// <param name="gui">A string representing a GUI folder.</param>
        /// <returns>a string representing the folder [path]\\Data\\gui\\Default.</returns>
        public static string GetDefaultPath( string path )
        {
            return Twp.Utilities.Path.Combine( path, "Data", "Gui", "Default" );
        }

		private XmlFile mainPrefs = null;
		public XmlFile MainPrefs
		{
			get
			{
				if( this.mainPrefs == null )
					this.mainPrefs = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "MainPrefs.xml" ) );
				return this.mainPrefs;
			}
		}

		private XmlFile loginPrefs = null;
		public XmlFile LoginPrefs
		{
			get
			{
				if( this.loginPrefs == null )
					this.loginPrefs = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "LoginPrefs.xml" ) );
				return this.loginPrefs;
			}
		}

		private XmlFile charPrefs = null;
		public XmlFile CharPrefs
		{
			get
			{
				if( this.charPrefs == null )
					this.charPrefs = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "CharPrefs.xml" ) );
				return this.charPrefs;
			}
		}
		
		private XmlFile fonts = null;
		public XmlFile Fonts
		{
		    get
		    {
		        if( this.fonts == null )
		            this.fonts = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Fonts.xml" ) );
		        return this.fonts;
		    }
		}

		private XmlFile textColors = null;
		public XmlFile TextColors
		{
			get
			{
				if( this.textColors == null )
					this.textColors = new XmlFile( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "TextColors.xml" ) );
				return this.textColors;
			}
		}
	}
}
