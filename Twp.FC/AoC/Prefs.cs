﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Twp.FC.AoC
{
	public class Prefs
	{
		/// <summary>
		/// Checks the path for the existance of the "Prefs" folder.
		/// </summary>
		/// <returns>true if the path is valid, otherwise, false.</returns>
		public static bool IsValid()
		{
			return !String.IsNullOrEmpty( GetFile() );
		}

		public static XmlFile Read()
		{
			XmlFile xmlFile = new XmlFile();
            string filePath = GetFile();
            if( File.Exists( filePath ) )
            {
                xmlFile.Load( filePath );
            }
            //else
            //{
            //    xmlFile = Gui.Default( path ).MainPrefs;
            //    xmlFile.FileName = filePath;
            //}
			return xmlFile;
		}

		/// <summary>
		/// Returns a string representing the full path to the Prefs folder.
		/// </summary>
		/// <returns>a string representing the Prefs folder in the current user's Local AppData folder.</returns>
		public static string GetPath()
		{
            string path = Twp.Utilities.Path.GetLocalAppDataPath( "Funcom", "Conan" );
			return Twp.Utilities.Path.Combine( path, "Prefs" );
		}

        /// <summary>
        /// Returns the first matching file to the pattern "Prefs(_\d{1})?.xml".
        /// </summary>
        /// <returns>a string representing the full path to the Prefs file.</returns>
        private static string GetFile()
        {
            string path = String.Empty;
            Regex regEx = new Regex( @"Prefs(\_\d{1})?\.xml$", RegexOptions.IgnoreCase );
            foreach( string file in System.IO.Directory.GetFiles( GetPath() ) )
            {
                if( regEx.IsMatch( file ) )
                {
                    path = file;
                }
            }
            return path;
        }
	}
}
