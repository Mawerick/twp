﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using Twp.Utilities;

namespace Twp.FC
{
	public class CfgFile
	{
		public CfgFile()
		{
		}

		private Dictionary<string, string> nodes = new Dictionary<string, string>();
		public Dictionary<string, string> Nodes
		{
			get { return this.nodes; }
		}

		public bool Read( string fileName )
		{
			if( !File.Exists( fileName ) )
				return false;

			string[] lines = File.ReadAllLines( fileName );
			foreach( string line in lines )
			{
				string[] tokens = line.Split( new char[] { ' ', '=' }, 2 );
				if( tokens.Length != 2 )
					Log.Debug( "[CfgFile.Read] Unknown line format: {0}", line );
				else
					this.nodes.Add( tokens[0], tokens[1] );
			}
			return true;
		}

		public bool Write( string fileName )
		{
			List<string> lines = new List<string>();
			foreach( KeyValuePair<string, string> node in this.nodes )
			{
				string line = String.Format( "{0} {1}", node.Key, node.Value );
				lines.Add( line );
			}
			File.WriteAllLines( fileName, lines.ToArray() );
			return true;
		}
	}
}
