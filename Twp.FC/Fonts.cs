﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;

using Twp.Utilities;

namespace Twp.FC
{
    /// <summary>
    /// Description of Fonts.
    /// </summary>
    public class Fonts
    {
        #region Private fields

        private static List<string> files = new List<string>();
        private static Dictionary<string, Font> map = new Dictionary<string, Font>();

        #endregion
        
        #region Properties
        
        public static List<string> Files
        {
            get { return files; }
        }
        
        public static Dictionary<string, Font> Map
        {
            get { return map; }
        }

        #endregion
        
        #region Methods
        
        public static bool Load( string fileName )
        {
            if( fileName == null )
                throw new ArgumentNullException( "fileName" );

            return Load( new XmlFile( fileName ) );
        }

        public static bool Load( XmlFile fontsFile )
        {
            if( fontsFile == null )
                throw new ArgumentNullException( "fontsFile" );

            if( fontsFile.DocumentElement == null || fontsFile.DocumentElement.ChildNodes == null )
            {
                Log.Debug( "[Fonts.Load] Failed to load fonts from {0}", fontsFile.FileName );
                return false;
            }

            foreach( XmlNode node in fontsFile.DocumentElement.ChildNodes )
            {
                switch( node.Name )
                {
                    case "FontMemResource":
                        XmlAttribute nameAttr = node.Attributes["name"];
                        if( nameAttr != null && !files.Contains( nameAttr.Value ) )
                            files.Add( nameAttr.Value );
                        break;

                    case "Font":
                        nameAttr = node.Attributes["name"];
                        if( nameAttr != null && !map.ContainsKey( nameAttr.Value ) )
                        {
                            Font font = new Font();
                            font.Name = nameAttr.Value;

                            foreach( XmlAttribute attr in node.Attributes )
                            {
                                switch( attr.Name.ToLower() )
                                {
                                    case "face":
                                        font.Face = attr.Value;
                                        break;

                                    case "size":
                                        if( attr.Value.StartsWith( "Scaled", false, CultureInfo.InvariantCulture ) )
                                        {
                                            font.Scaled = true;
                                            int start = attr.Value.IndexOf( '(' ) + 1;
                                            int end = attr.Value.IndexOf( ')' ) - 1;
                                            string value = attr.Value.Substring( start, end - start ).Trim();
                                            font.Size = Convert.ToInt32( value );
                                        }
                                        else
                                            font.Size = Convert.ToInt32( attr.Value );
                                        break;

                                    case "weight":
                                        try
                                        {
                                            font.Weight = (FontWeight) Enum.Parse( typeof( FontWeight ), attr.Value, true );
                                        }
                                        catch( Exception )
                                        {
                                        }
                                        break;
                                        
                                    case "italic":
                                        if( attr.Value.ToLower() == "yes" )
                                            font.Italic = true;
                                        else
                                            font.Italic = false;
                                        break;
                                        
                                    case "antialiasing":
                                        try
                                        {
                                            font.AntiAliasing = (FontAntiAliasing) Enum.Parse( typeof( FontAntiAliasing ), attr.Value, true );
                                        }
                                        catch( Exception )
                                        {
                                        }
                                        break;
                                }
                            }
                            
                            map.Add( font.Name, font );
                        }
                        break;

                    default:
                        break;
                }
            }
            return true;
        }
        
        #endregion
    }
    
    public enum FontWeight
    {
        Thin,
        ExtraLight,
        Light,
        Normal,
        Medium,
        SemiBold,
        Bold,
        ExtraBold,
        Heavy
    }
    
    public enum FontAntiAliasing
    {
        None,
        Medium,
        High,
        Full,
        Auto
    }
    
    public class Font
    {
        public Font()
        {
            this.name = String.Empty;
            this.face = String.Empty;
            this.size = 0;
            this.scaled = false;
            this.weight = FontWeight.Normal;
            this.italic = false;
            this.antiAliasing = FontAntiAliasing.Auto;
        }

        private string name;
        private string face;
        private int size;
        private bool scaled;
        private FontWeight weight;
        private bool italic;
        private FontAntiAliasing antiAliasing;
        
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string Face
        {
            get { return this.face; }
            set { this.face = value; }
        }

        public int Size
        {
            get { return this.size; }
            set { this.size = value; }
        }

        public bool Scaled
        {
            get { return this.scaled; }
            set { this.scaled = value; }
        }

        public FontWeight Weight
        {
            get { return this.weight; }
            set { this.weight = value; }
        }

        public bool Italic
        {
            get { return this.italic; }
            set { this.italic = value; }
        }
        
        public FontAntiAliasing AntiAliasing
        {
            get { return this.antiAliasing; }
            set { this.antiAliasing = value; }
        }
    }
}
