﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.FC
{
    /// <summary>
    /// Description of Script.
    /// </summary>
    public class ScriptFile : IEquatable<ScriptFile>, IComparable<ScriptFile>
    {
        public ScriptFile( FileInfo file )
        {
            this.file = file;
        }
        
        public ScriptFile( string path, string name )
        {
            this.file = new FileInfo( Path.Combine( GetPath( path ), name ) );
        }
        
        private FileInfo file;
        
        public bool IsValid()
        {
            return this.file.Exists;
        }
        
        public static bool IsValid( string path, string script )
        {
            return File.Exists( Path.Combine( GetPath( path ), script ) );
        }
        
		public void Save()
		{
		    if( !this.file.Directory.Exists )
		        this.file.Directory.Create();
		    using( StreamWriter sw = new StreamWriter( this.file.FullName, false ) )
		    {
		        sw.Write( this.content );
		        sw.Flush();
		        sw.Close();
		    }
		}
		
		public void Delete()
		{
		    this.file.Delete();
		}

        public static List<ScriptFile> List( string path )
		{
			if( String.IsNullOrEmpty( path ) )
				return null;

			DirectoryInfo di = new DirectoryInfo( GetPath( path ) );
			Twp.Utilities.Log.Debug( "[Scipt.List] Path: {0}", di.FullName );
			if( !di.Exists )
				return null;

			FileInfo[] files = di.GetFiles();
			if( files.Length > 0 )
			{
				List<ScriptFile> scripts = new List<ScriptFile>();
				foreach( FileInfo file in files )
				{
					ScriptFile script = new ScriptFile( file );
					scripts.Add( script );
				}
				return scripts;
			}
			else
				return null;
		}

        public static string GetPath( string path )
        {
            return Path.Combine( path, "scripts" );
        }

		public string Name
		{
			get { return this.file.Name; }
		}
		
		public string[] Lines
		{
		    get { return this.content.Split( '\n' ); }
		}
		
		private string content = null;
		public string Content
		{
		    get
		    {
		        if( this.content == null )
		        {
		            this.ReadContent();
		        }
		        return this.content;
		    }
		    set
		    {
		        this.content = value;
		    }
		}

		public void ReadContent()
		{
            if( !this.file.Exists )
            {
                this.content = String.Empty;
                return;
            }

            using( StreamReader sr = new StreamReader( this.file.FullName ) )
            {
                this.content = sr.ReadToEnd();
            }
		}
		
		public override string ToString()
		{
			if( String.IsNullOrEmpty( this.Name ) )
				return base.ToString();
			else
				return this.Name;
		}

		#region IEquatable implementation

		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		public bool Equals( ScriptFile other )
		{
			if( other == null )
				return false;
			else
				return this.Name.Equals( other.Name );
		}

		public override bool Equals( object obj )
		{
			ScriptFile other = obj as ScriptFile;
			if( other != null )
				return this.Equals( other );
			else
				return false;
		}

		public static bool operator ==( ScriptFile script1, ScriptFile script2 )
		{
			if( object.ReferenceEquals( script1, script2 ) )
				return true;
			if( object.ReferenceEquals( script1, null ) )
				return false;
			if( object.ReferenceEquals( script2, null ) )
				return false;

			return script1.Equals( script2 );
		}

		public static bool operator !=( ScriptFile script1, ScriptFile script2 )
		{
			if( object.ReferenceEquals( script1, script2 ) )
				return false;
			if( object.ReferenceEquals( script1, null ) )
				return true;
			if( object.ReferenceEquals( script2, null ) )
				return true;

			return !script1.Equals( script2 );
		}

		#endregion

		#region IComparable implementation

		public int CompareTo( ScriptFile other )
		{
			return this.Name.CompareTo( other.Name );
		}

		#endregion
    }
}
