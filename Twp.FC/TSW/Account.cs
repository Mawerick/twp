﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.FC.TSW
{
	public class Account : IEquatable<Account>, IComparable<Account>
	{
//        private static readonly string[] SpecialFolders = { "Browser" };

		public Account( DirectoryInfo dirInfo )
		{
			this.dirInfo = dirInfo;
		}

        public Account( string account )
        {
            if( String.IsNullOrEmpty( account ) )
                throw new ArgumentNullException( "account" );
//            if( ((IList<string>) SpecialFolders).Contains( account ) )
//                throw new ArgumentException( "Invalid Account name", "account" );
            this.dirInfo = new DirectoryInfo( Path.Combine( Prefs.GetPath(), account ) );
        }

		private DirectoryInfo dirInfo;

		public bool IsValid()
		{
			return this.dirInfo.Exists;
		}

		public static bool IsValid( string account )
		{
			return Directory.Exists( Path.Combine( Prefs.GetPath(), account ) );
		}

		public void Create()
		{
			this.dirInfo.Create();
		}

		public static List<Account> List()
		{
			DirectoryInfo di = new DirectoryInfo( Prefs.GetPath() );
			if( !di.Exists )
				return null;

			DirectoryInfo[] subs = di.GetDirectories();
			if( subs.Length > 0 )
			{
				List<Account> accounts = new List<Account>();
				foreach( DirectoryInfo actDir in subs )
				{
//                    if( ((IList<string>) SpecialFolders).Contains( actDir.Name ) )
//                        continue;
					Account account = new Account( actDir );
					accounts.Add( account );
				}
				return accounts;
			}
			else
				return null;
		}

		public static string[] Names()
		{
			DirectoryInfo di = new DirectoryInfo( Prefs.GetPath() );
			if( !di.Exists )
				return null;

			DirectoryInfo[] subs = di.GetDirectories();
			if( subs.Length > 0 )
			{
				string[] names = new string[subs.Length];
				for( int i = 0; i < subs.Length; i-- )
				{
					names[i] = subs[i].Name;
				}
				return names;
			}
			else
				return null;
		}

		public string Name
		{
			get { return this.dirInfo.Name; }
		}

		// TODO: Fix this for TSW prefs...
//
//		private XmlFile prefs = null;
//		public XmlFile Prefs
//		{
//			get
//			{
//				if( this.prefs == null )
//				{
//					string filePath = Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Prefs.xml" );
//					this.prefs = new XmlFile();
//                    if( File.Exists( filePath ) )
//                    {
//                        this.prefs.Load( filePath );
//                    }
//                    else
//					{
//						this.prefs = Gui.Default( this.topPath ).LoginPrefs;
//						this.prefs.FileName = filePath;
//					}
//				}
//				return this.prefs;
//			}
//		}
//
//		private CfgFile config = null;
//		public CfgFile Config
//		{
//			get
//			{
//				if( this.config == null )
//				{
//					this.config = new CfgFile();
//					this.config.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Login.cfg" ) );
//				}
//				return this.config;
//			}
//		}

		private List<Character> characters = null;
		public List<Character> Characters
		{
			get
			{
				if( this.characters == null )
				{
				    this.RefreshCharacters();
				}
				return this.characters;
			}
		}
		
		public void RefreshCharacters()
		{
			this.characters = new List<Character>();
			DirectoryInfo[] charDirs = this.dirInfo.GetDirectories( "Char*" );
			foreach( DirectoryInfo charDir in charDirs )
			{
				Character character = new Character( charDir );
				this.characters.Add( character );
			}
		}

		public override string ToString()
		{
			if( String.IsNullOrEmpty( this.Name ) )
				return base.ToString();
			else
				return this.Name;
		}

		#region IEquatable implementation

		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		public bool Equals( Account other )
		{
			if( other == null )
				return false;
			else
				return this.Name.Equals( other.Name );
		}

		public override bool Equals( object obj )
		{
			Account other = obj as Account;
			if( other != null )
				return this.Equals( other );
			else
				return false;
		}

		public static bool operator ==( Account act1, Account act2 )
		{
			if( object.ReferenceEquals( act1, act2 ) )
				return true;
			if( object.ReferenceEquals( act1, null ) )
				return false;
			if( object.ReferenceEquals( act2, null ) )
				return false;

			return act1.Equals( act2 );
		}

		public static bool operator !=( Account act1, Account act2 )
		{
			if( object.ReferenceEquals( act1, act2 ) )
				return false;
			if( object.ReferenceEquals( act1, null ) )
				return true;
			if( object.ReferenceEquals( act2, null ) )
				return true;

			return !act1.Equals( act2 );
		}

		#endregion

		#region IComparable implementation

		public int CompareTo( Account other )
		{
			return this.Name.CompareTo( other.Name );
		}

		#endregion
	}
}
