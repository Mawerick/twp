﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Twp.Utilities;

namespace Twp.FC.TSW
{
	public class Chat
	{
		public Chat()
		{
		}
		
		public void Read( string path )
		{
			DirectoryInfo dirInfo = new DirectoryInfo( path );
			FileInfo[] winFiles = dirInfo.GetFiles( "Windows_*.xml" );
			foreach( FileInfo winFile in winFiles )
			{
			    XmlFile xmlFile = new XmlFile( winFile.FullName );
			    this.files.Add( xmlFile );
			    
			    this.ParseFile( xmlFile );
			}
		}
		
		private void ParseFile( XmlFile xmlFile )
		{
		    foreach( XmlNode node in xmlFile.DocumentElement )
		    {
		        if( node.Attributes["name"].Value == "windows" )
		        {
		            if( node.Name == "Array" )
		            {
            		    foreach( XmlNode childNode in node.ChildNodes )
            		    {
            		        ChatWindow window = new ChatWindow( childNode );
            		        this.windows.Add( window );
            		    }
		            }
		            else if( node.Name == "Archive" )
    		        {
        		        ChatWindow window = new ChatWindow( node );
        		        this.windows.Add( window );
    		        }
		            else
		                Log.Debug( "[Chat.ParseFile] Unknown windows node {0}", node.Name );
		        }
		    }
		}
		
		private List<XmlFile> files = new List<XmlFile>();
		public List<XmlFile> Files
		{
		    get { return this.files; }
		}

		private List<ChatWindow> windows = new List<ChatWindow>();
		public List<ChatWindow> Windows
		{
			get { return this.windows; }
		}
	}

	public class ChatWindow
	{
		public ChatWindow( XmlNode windowNode )
		{
			this.windowNode = windowNode;
		    foreach( XmlNode node in this.windowNode.ChildNodes )
		    {
		        if( node.Attributes["name"].Value == "group_view_array" )
		        {
		            if( node.Name == "Array" )
		            {
            		    foreach( XmlNode childNode in node.ChildNodes )
            		    {
            		        ChatTab chatTab = new ChatTab( childNode );
            		        this.tabs.Add( chatTab );
            		    }
    		        }
    		        else if( node.Name == "Archive" )
    		        {
        		        ChatTab chatTab = new ChatTab( node );
        		        this.tabs.Add( chatTab );
    		        }
    		        else
    		            Log.Debug( "[ChatWindow] Unknown group_view_array node {0}", node.Name );
		        }
		    }
		}

		private XmlNode windowNode;
		public XmlNode Node
		{
		    get { return this.windowNode; }
		}
		
		private List<ChatTab> tabs = new List<ChatTab>();
		public List<ChatTab> Tabs
		{
		    get { return this.tabs; }
		}
	}
	
	public class ChatTab : IEquatable<ChatTab>, IComparable<ChatTab>
	{
	    public ChatTab( XmlNode tabNode )
	    {
	        this.tabNode = tabNode;
	        this.Parse();
	    }
	    
	    private void Parse()
	    {
	        this.name = null;
	        this.isAutoSubscribe = false;
	        this.channelNames.Clear();
	        this.channelIDs.Clear();
	        
	        foreach( XmlNode node in this.tabNode.ChildNodes )
	        {
	            switch( node.Name )
	            {
	                case "String":
	                    switch( node.Attributes["name"].Value )
	                    {
	                        case "name":
                                this.name = node.Attributes["value"].Value;
//                                Log.Debug( "[ChatTab.Parse] Name: {0}", this.name );
                                break;
                                
                            case "selected_group_fixnames":
                                this.channelNames.Add( node.Attributes["value"].Value );
                                this.channelNamesNode = node;
//                                Log.Debug( "[ChatTab.Parse] ChannelName: {0}", node.Attributes["value"].Value );
                                break;
//                                
//                            default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled String Node: name={0}", node.Attributes["name"].Value );
//                                break;
	                    }
	                    break;
	                    
	                case "Bool":
	                    switch( node.Attributes["name"].Value )
	                    {
	                        case "is_autosubscribe_window":
    	                        this.isAutoSubscribe = Convert.ToBoolean( node.Attributes["value"].Value );
//                                Log.Debug( "[ChatTab.Parse] IsAutoSubscribe: {0}", this.isAutoSubscribe );
    	                        break;
//    	                        
//                            default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled Bool Node: name={0}", node.Attributes["name"].Value );
//                                break;
	                    }
	                    break;
//
//	                case "Float":
//	                    switch( node.Attributes["name"].Value )
//	                    {
//	                        default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled Float Node: name={0}", node.Attributes["name"].Value );
//                                break;
//	                    }
//	                    break;
//
//	                case "Int32":
//	                    switch( node.Attributes["name"].Value )
//	                    {
//	                        default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled Int32 Node: name={0}", node.Attributes["name"].Value );
//                                break;
//	                    }
//	                    break;
	                    
	                case "Int64":
	                    switch( node.Attributes["name"].Value )
	                    {
	                        case "selected_group_ids":
	                            this.channelIDs.Add( node.Attributes["value"].Value );
//                                Log.Debug( "[ChatTab.Parse] ChannelID: {0}", node.Attributes["value"].Value );
                                this.channelIDsNode = node;
	                            break;
//
//	                        default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled Int64 Node: name={0}", node.Attributes["name"].Value );
//                                break;
	                    }
	                    break;
//	                    
//	                case "Archive":
//	                    switch( node.Attributes["name"].Value )
//	                    {
//	                        default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled Archive Node: name={0}", node.Attributes["name"].Value );
//                                break;
//	                    }
//	                    break;
	                    
	                case "Array":
	                    switch( node.Attributes["name"].Value )
	                    {
	                        case "selected_group_fixnames":
                    	        foreach( XmlNode childNode in node.ChildNodes )
                    	        {
                    	            if( childNode.Name != "String" )
                    	                continue;
                    	            
                    	            this.channelNames.Add( childNode.Attributes["value"].Value );
//                                    Log.Debug( "[ChatTab.Parse] ChannelName: {0}", childNode.Attributes["value"].Value );
                    	        }
                                this.channelNamesNode = node;
                    	        break;

                    	    case "selected_group_ids":
                    	        foreach( XmlNode childNode in node.ChildNodes )
                    	        {
                    	            if( childNode.Name != "Int64" )
                    	                continue;
                    	            
                    	            this.channelIDs.Add( childNode.Attributes["value"].Value );
//                                    Log.Debug( "[ChatTab.Parse] ChannelID: {0}", childNode.Attributes["value"].Value );
                    	        }
                                this.channelIDsNode = node;
                    	        break;
//
//	                        default:
//                                Log.Debug( "[ChatTab.Parse] Unhandled Array Node: name={0}", node.Attributes["name"].Value );
//                                break;
	                    }
	                    break;
//
//	                default:
//                        Log.Debug( "[ChatTab.Parse] Unhandled Node: {0}", node.Name );
//                        break;
	            }
	        }
	    }

	    public void ToggleChannel( string name, string id, bool subscribed )
	    {
	        if( this.isAutoSubscribe )
	        {
	            if( subscribed )
	                this.RemoveChannel( name, id );
	            else
	                this.AddChannel( name, id );
	        }
	        else
	        {
	            if( subscribed )
	                this.AddChannel( name, id );
	            else
	                this.RemoveChannel( name, id );
	        }
	    }

	    public void AddChannel( string name, string id )
	    {
	        if( this.channelNames.Contains( name ) )
	            return;
	        
	        if( this.channelIDs.Contains( id ) )
	            return;
	        
	        this.channelNames.Add( name );
	        this.channelIDs.Add( id );
	        
	        this.UpdateChannels();
	    }
	    
	    public void RemoveChannel( string name, string id )
	    {
	        if( !this.channelNames.Contains( name ) )
	            return;
	        
	        if( !this.channelIDs.Contains( id ) )
	            return;
	        
	        this.channelNames.Remove( name );
	        this.channelIDs.Remove( id );
	        
	        this.UpdateChannels();
	    }
	    
	    private void UpdateChannels()
	    {
	        if( this.channelNamesNode.Name == "String" )
	        {
	            XmlNode sibling = this.channelNamesNode.PreviousSibling;
	            this.Node.RemoveChild( this.channelNamesNode );
	            this.channelNamesNode = this.Node.OwnerDocument.CreateElement( "Array" );
	            this.Node.InsertAfter( this.channelNamesNode, sibling );
	        }
	        else
                this.channelNamesNode.RemoveAll();
	        
            XmlAttribute attribute = this.Node.OwnerDocument.CreateAttribute( "name" );
            attribute.Value = "selected_group_fixnames";
            this.channelNamesNode.Attributes.Append( attribute );
	        
            foreach( string name in this.channelNames )
            {
                XmlElement element = this.Node.OwnerDocument.CreateElement( "String" );
                attribute = this.Node.OwnerDocument.CreateAttribute( "value" );
                attribute.Value = name;
                element.Attributes.Append( attribute );
                this.channelNamesNode.AppendChild( element );
            }
	        
	        if( this.channelIDsNode.Name == "Int64" )
	        {
	            XmlNode sibling = this.channelIDsNode.PreviousSibling;
	            this.Node.RemoveChild( this.channelIDsNode );
	            this.channelIDsNode = this.Node.OwnerDocument.CreateElement( "Array" );
	            this.Node.InsertAfter( this.channelIDsNode, sibling );
	        }
	        else
	            this.channelIDsNode.RemoveAll();

            attribute = this.Node.OwnerDocument.CreateAttribute( "name" );
            attribute.Value = "selected_group_ids";
            this.channelIDsNode.Attributes.Append( attribute );

            foreach( string id in this.channelIDs )
            {
                XmlElement element = this.Node.OwnerDocument.CreateElement( "Int64" );
                attribute = this.Node.OwnerDocument.CreateAttribute( "value" );
                attribute.Value = id;
                element.Attributes.Append( attribute );
                this.channelIDsNode.AppendChild( element );
            }
	    }
	    
	    private XmlNode tabNode;
	    public XmlNode Node
	    {
	        get { return this.tabNode; }
	    }
	    
	    private string name;
	    public string Name
	    {
	        get { return this.name; }
	    }
	    
	    private bool isAutoSubscribe = false;
	    public bool IsAutoSubscribe
	    {
	        get { return this.isAutoSubscribe; }
	    }
	    
	    private XmlNode channelNamesNode;
	    private List<string> channelNames = new List<string>();
        public List<string> ChannelNames
        {
            get { return this.channelNames; }
        }
        
	    private XmlNode channelIDsNode;
        private List<string> channelIDs = new List<string>();
        public List<string> ChannelIDs
        {
            get { return this.channelIDs; }
        }
	    
	    public override string ToString()
        {
	        string name = this.Name;
			if( String.IsNullOrEmpty( this.Name ) )
				name = base.ToString();
			if( this.isAutoSubscribe )
		        name += " (autosub)";
			return name;
        }

		#region IEquatable implementation

		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		public bool Equals( ChatTab other )
		{
			if( other == null )
				return false;
			else
				return this.Name.Equals( other.Name );
		}

		public override bool Equals( object obj )
		{
			ChatTab other = obj as ChatTab;
			if( other != null )
				return this.Equals( other );
			else
				return false;
		}

		public static bool operator ==( ChatTab tab1, ChatTab tab2 )
		{
			if( object.ReferenceEquals( tab1, tab2 ) )
				return true;
			if( object.ReferenceEquals( tab1, null ) )
				return false;
			if( object.ReferenceEquals( tab2, null ) )
				return false;

			return tab1.Equals( tab2 );
		}

		public static bool operator !=( ChatTab tab1, ChatTab tab2 )
		{
			if( object.ReferenceEquals( tab1, tab2 ) )
				return false;
			if( object.ReferenceEquals( tab1, null ) )
				return true;
			if( object.ReferenceEquals( tab2, null ) )
				return true;

			return !tab1.Equals( tab2 );
		}

		#endregion

		#region IComparable implementation

		public int CompareTo( ChatTab other )
		{
			return this.Name.CompareTo( other.Name );
		}

		#endregion
	}
}
