﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2013 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

namespace Twp.FC
{
    /// <summary>
    /// Description of Color.
    /// </summary>
    public static class TextColors
    {
        private static Dictionary<string, Color> map = new Dictionary<string, Color>();
        public static Dictionary<string, Color> Map
        {
            get { return map; }
        }

        public static bool Load( XmlFile colorsFile )
        {
            map.Clear();
            if( colorsFile == null || !colorsFile.Exists )
                return false;
            
            XmlNodeList nodes = colorsFile.GetElementsByTagName( "HTMLColor" );
            if( nodes == null || nodes.Count <= 0 )
                return false;
            
            foreach( XmlNode node in nodes )
            {
                XmlNode nameNode = node.Attributes.GetNamedItem( "name" );
                XmlNode colorNode = node.Attributes.GetNamedItem( "color" );
                Color color = ColorFromXml( colorNode.Value );
                if( color.IsEmpty && map.ContainsKey( colorNode.Value ) )
                    color = map[colorNode.Value];
                
                if( color != Color.Empty && !map.ContainsKey( nameNode.Value ) )
                    map.Add( nameNode.Value, color );
            }
            return true;
        }
        
        private static Color ColorFromXml( string color )
        {
            if( color.StartsWith( "0x" ) )
            {
                int r, g, b;
                string hex = color.Substring( 2 );
                if( hex.Length > 6 )
                {
                    int pad = hex.Length - 6;
                    r = Convert.ToByte( hex.Substring( pad, 2 ), 16 );
                    g = Convert.ToByte( hex.Substring( 2 + pad, 2 ), 16 );
                    b = Convert.ToByte( hex.Substring( 4 + pad, 2 ), 16 );
                }
                else
                {
                    r = Convert.ToByte( hex.Substring( 0, 2 ), 16 );
                    g = Convert.ToByte( hex.Substring( 2, 2 ), 16 );
                    b = Convert.ToByte( hex.Substring( 4, 2 ), 16 );
                }
                return Color.FromArgb( r, g, b );
            }
            else if( color.StartsWith( "#" ) )
            {
                int r, g, b;
                string hex = color.Substring( 1 );
                r = Convert.ToByte( hex.Substring( 0, 2 ), 16 );
                g = Convert.ToByte( hex.Substring( 2, 2 ), 16 );
                b = Convert.ToByte( hex.Substring( 4, 2 ), 16 );
                return Color.FromArgb( r, g, b );
            }
            return Color.Empty;
        }
    }
}
