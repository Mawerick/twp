﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Xml;

namespace Twp.AO.Xml
{
	public class TextLineElement : IAOElement
	{
		public TextLineElement()
		{
			this.text = String.Empty;
			this.name = String.Format( "Line{0}", ++lineIndex );
		}

		public TextLineElement( string text )
		{
			this.text = text;
			this.name = String.Format( "Line{0}", ++lineIndex );
		}

		private static int lineIndex = 0;

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		public string Value
		{
			get { return this.text; }
			set { this.text = value; }
		}

		public bool HasChildren
		{
			get { return false; }
		}

		private string text;
		public string Text
		{
			get { return this.text; }
			set { this.text = value; }
		}

		public void ReadXml( XmlReader reader )
		{
			if( !reader.HasAttributes )
				return;

			this.text = reader.GetAttribute( "text" );
		}

		public void WriteXml( XmlWriter writer )
		{
			writer.WriteStartElement( "TextLine" );
			writer.WriteAttributeString( "text", this.text );
			writer.WriteEndElement();
		}

		public IAOElement GetElement( string name )
		{
			if( this.name == name )
				return this;
			return null;
		}

		public override string ToString()
		{
			return String.Format( "[TextLine] {0}", this.text );
		}
	}
}
