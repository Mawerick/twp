﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

using Twp.Utilities;

namespace Twp.FC
{
    public class XmlFile : XmlDocument
    {
        public XmlFile()
        {
        }
        
        public XmlFile( string fileName )
        {
            this.fileName = fileName;
            this.Load();
        }
        
        private string fileName;
        public string FileName
        {
            get { return this.fileName; }
            set { this.fileName = value; }
        }

        public bool Exists
        {
            get
            {
                return System.IO.File.Exists( this.fileName );
            }
        }
        
        public XmlNode Root
        {
        	get
        	{
        	    return this.DocumentElement.FirstChild;
        	}
        }

        public override void Load( string fileName )
        {
            this.fileName = fileName;
            this.Load();
        }
        
        public void Load()
        {
            if( this.Exists )
            {
                try
                {
                    using( StreamReader reader = new StreamReader( fileName, Encoding.UTF8 ) )
                    {
                        base.Load( reader );
                    }
                }
                catch( Exception ex )
                {
                    Log.Debug( "[XmlFile.Load] FileName: {0} Exception: {1}", fileName, ex );
                }
            }
        }

        public void Save()
        {
            base.Save( this.fileName );
            string bxml = this.fileName.Substring( 0, this.fileName.Length - 3 ) + "bxml";
            Log.Debug( "[XmlFile.Save] Checking for binary Xml file: {0}", bxml );
            System.IO.FileInfo fi = new System.IO.FileInfo( bxml );
            if( fi.Exists )
            {
                Log.Debug( "[XmlFile.Save] File found. Deleting it." );
                fi.Delete();
            }
            else
                Log.Debug( "[XmlFile.Save] File not found." );
        }
        
		[Conditional("DEBUG")]
        public void Debug()
        {
        	Log.Debug( "[XmlFile.Debug] XmlFile Content:" );
        	Log.Debug( this.InnerXml );
        }

        public XmlElement GetElement( string type, string name )
        {
            XmlElement element = null;
            XmlNodeList nodes = this.GetElementsByTagName( type );
            foreach( XmlElement current in nodes )
            {
                if( current.HasAttributes )
                {
                    if( current.GetAttribute( "name" ) == name )
                    {
                        element = current;
                        break;
                    }
                }
            }
            return element;
        }

        public static string StripQuotes( string text )
        {
            if( String.IsNullOrEmpty( text ) )
                return String.Empty;
            return text.Trim( new char[] { '"', '\'' } );
        }

        public static string AddQuotes( string text )
        {
            return '"' + StripQuotes( text ) + '"';
        }
    }
}
