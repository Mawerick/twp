﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Text;

namespace Twp.Utilities
{
	public class BinFile : BinaryReader
	{
		public BinFile( string fileName )
			: base( new FileStream( fileName, FileMode.Open, FileAccess.Read, FileShare.Read ) )
		{
		}

		public BinFile( byte[] buffer )
			: base( new MemoryStream( buffer ) )
		{
		}

		public long Position
		{
			get { return this.BaseStream.Position; }
			set { this.BaseStream.Position = value; }
		}

		public uint Length
		{
		    get { return (uint) this.BaseStream.Length; }
		}

		public string ReadString( int len )
		{
			return Encoding.ASCII.GetString( this.ReadBytes( len ) );
		}

		public int ReadInt32At( uint offset )
		{
			this.Position = offset;
			return this.ReadInt32();
		}

		public uint ReadUInt32At( uint offset )
		{
			this.Position = offset;
			return this.ReadUInt32();
		}

		public int ReadInt32Swapped()
		{
			byte[] bytes = BitConverter.GetBytes( this.ReadInt32() );
			Array.Reverse( bytes );
			return BitConverter.ToInt32( bytes, 0 );
		}
		
		public static int SwapEndian( int value )
		{
		    if( !BitConverter.IsLittleEndian )
		        return value;

		    byte[] bytes = BitConverter.GetBytes( value );
			Array.Reverse( bytes );
			return BitConverter.ToInt32( bytes, 0 );
		}

		public static uint SwapEndian( uint value )
		{
		    if( !BitConverter.IsLittleEndian )
		        return value;

		    byte[] bytes = BitConverter.GetBytes( value );
			Array.Reverse( bytes );
			return BitConverter.ToUInt32( bytes, 0 );
		}
		
		public static ulong SwapEndian( ulong value )
		{
		    if( !BitConverter.IsLittleEndian )
		        return value;
		    
		    byte[] bytes = BitConverter.GetBytes( value );
		    Array.Reverse( bytes );
		    return BitConverter.ToUInt64( bytes, 0 );
		}
		
		public string ReadHash()
		{
		    byte[] bytes = this.ReadBytes( 4 );
		    Array.Reverse( bytes );
		    return Encoding.ASCII.GetString( bytes );
		}

		public long Read3F1()
		{
		    return Convert.ToInt64( Math.Round( ( Convert.ToDouble( this.ReadInt32() ) / 1009f ) - 1f ) );
		}
		
		public void Dispose()
		{
			this.Dispose( true );
			GC.SuppressFinalize( this );
		}
	}
}
