﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2010 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Utilities
{
	public class Cache<T>
	{
		System.Timers.Timer timer = new System.Timers.Timer();

		public Cache()
		{
			timer.Interval = 60000;
			timer.AutoReset = true;
			timer.Elapsed += new System.Timers.ElapsedEventHandler( timer_Elapsed );
			timer.Start();
		}

		void timer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
		{
			foreach( KeyValuePair<int, CacheNode<T>> kvp in this.items )
			{
				if( kvp.Value.Uses <= 0 )
				{
					Log.Debug( "[Cache.timer_Elapsed] Not freed: {0}", kvp.Value.Item );
					this.items.Remove( kvp.Key );
				}
			}
		}

		private Dictionary<int, CacheNode<T>> items = new Dictionary<int, CacheNode<T>>();

		public T this[int key]
		{
			get
			{
				this.items[key].Uses++;
				return this.items[key].Item;
			}
		}

		public int Count
		{
			get { return this.items.Count; }
		}

		public void Add( int id, T item )
		{
			if( !this.items.ContainsKey( id ) )
			{
				this.items.Add( id, new CacheNode<T>( item ) );
			}
			this.items[id].Uses++;
		}

		public bool Remove( int id )
		{
			if( !this.items.ContainsKey( id ) )
				return false;

			this.items[id].Uses--;

			if( this.items[id].Uses <= 0 )
			{
				this.items.Remove( id );
			}
			return true;
		}

		public bool Contains( int key )
		{
			return this.items.ContainsKey( key );
		}

		public int Uses( int key )
		{
			if( this.Contains( key ) )
				return this.items[key].Uses;
			else
				return 0;
		}

		public void Clear()
		{
			this.items.Clear();
		}
	}

	internal class CacheNode<T>
	{
		public CacheNode( T item )
		{
			this.item = item;
		}

		private int uses = 0;
		public int Uses
		{
			get { return this.uses; }
			set { this.uses = value; }
		}

		private T item = default( T );
		public T Item
		{
			get { return this.item; }
		}
	}
}
