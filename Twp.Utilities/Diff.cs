﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of Diff.
    /// </summary>
    public static class Diff
    {
        public static int[,] GetLongestCommonSubsequenceMatrix( string s1, string s2 )
        {
            int[,] lcsMatrix = new int[s1.Length,s2.Length];
            char l1, l2;
            
            for( int i = 0; i < s1.Length; i++ )
            {
                for( int j = 0; j < s2.Length; j++ )
                {
                    l1 = s1[i];
                    l2 = s2[j];
                    
                    if( l1 == l2 )
                    {
                        if( i == 0 || j == 0 )
                            lcsMatrix[i, j] = 1;
                        else
                            lcsMatrix[i, j] = 1 + lcsMatrix[i - 1, j - 1];
                    }
                    else
                    {
                        if( i == 0 && j == 0 )
                            lcsMatrix[i, j] = 0;
                        else if( i == 0 && j != 0 )
                            lcsMatrix[i, j] = Math.Max( 0, lcsMatrix[i, j - 1] );
                        else if( i != 0 && j == 0 )
                            lcsMatrix[i, j] = Math.Max( lcsMatrix[i - 1, j], 0 );
                        else if( i != 0 && j != 0 )
                            lcsMatrix[i, j] = Math.Max( lcsMatrix[i - 1, j], lcsMatrix[i, j - 1] );
                    }
                }
            }
            
            return lcsMatrix;
        }
        
        public static void GetDiffTreeFromBacktrackMatrix( int[,] lcsMatrix, string s1, string s2, int i, int j )
        {
            if( i > 0 && j > 0 && s1[i - 1] == s2[j - 1] )
            {
                GetDiffTreeFromBacktrackMatrix( lcsMatrix, s1, s2, i - 1, j - 1 );
                Console.WriteLine( "  " + s1[i - 1] );
            }
            else
            {
                if( j > 0 && (i == 0 || lcsMatrix[i, j - 1] >= lcsMatrix[i - 1, j]) )
                {
                    GetDiffTreeFromBacktrackMatrix( lcsMatrix, s1, s2, i, j - 1 );
                    Console.WriteLine( "+ " + s2[j - 1] );
                }
                else if( i > 0 && (j == 0 || lcsMatrix[i, j - 1] < lcsMatrix[i - 1, j]) )
                {
                    GetDiffTreeFromBacktrackMatrix( lcsMatrix, s1, s2, i - 1, j );
                    Console.WriteLine( "- " + s1[i - 1] );
                }
            }
        }
    }
}
