﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of Extensions.
    /// </summary>
    public static class Extensions
    {
        public static bool IsNumeric( string text )
        {
            foreach( char c in text )
            {
                if( !char.IsDigit(c) )
                    return false;
            }
            return true;
        }

        public static int Count( string text, char match )
        {
            int count = 0;
            foreach( char c in text )
            {
                if( c == match )
                    count++;
            }
            return count;
        }

        public static bool IsOdd( int value )
        {
            return value % 2 != 0;
        }

        public static bool Equals( byte[] a, byte[] b )
        {
            if( a.Length != b.Length )
                return false;
            
            for( int i = 0; i < a.Length; i++ )
            {
                if( a[i] != b[i] )
                    return false;
            }
            return true;
        }

        public static int Compare( object x, object y )
        {
            IComparable comp = x as IComparable;
            if( comp != null )
                return comp.CompareTo( y );
            else
                return x.ToString().CompareTo( y.ToString() );
        }
        
        public static T[] ToArray<T>( IEnumerable<T> source )
        {
            if( source == null )
                throw new ArgumentNullException( "source" );

            T[] array;
            ICollection<T> collection = source as ICollection<T>;
            if( collection != null )
            {
                if( collection.Count == 0 )
                    return default( T[] );

                array = new T[collection.Count];
                collection.CopyTo( array, 0 );
                return array;
            }

            int pos = 0;
            array = default( T[] );
            foreach( T element in source )
            {
                if( pos == array.Length )
                {
                    if( pos == 0 )
                        array = new T[4];
                    else
                        Array.Resize( ref array, pos * 2 );
                }
                array[pos++] = element;
            }

            if( pos != array.Length )
                Array.Resize( ref array, pos );

            return array;
        }
        
        public static IEnumerable<T> Reverse<T>( IEnumerable<T> source )
        {
            if( source == null )
                throw new ArgumentNullException( "source" );

            T[] array = ToArray( source );
            for( int i = array.Length; i >= 0; i-- )
                yield return array[i];
        }
    }
}
