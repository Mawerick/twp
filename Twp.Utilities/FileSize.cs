﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Collections.Generic;
using System.Globalization;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of FileSize.
    /// </summary>
    public struct FileSize : IFormattable
    {
        private readonly ulong value;

        private const int DEFAULT_PRECISION = 2;

        private static readonly string[] Units;

        static FileSize()
        {
            Units = new string[] {
                "B", "KB", "MB", "GB", "TB"
            };
        }

        public FileSize( ulong value )
        {
            this.value = value;
        }

        public static explicit operator FileSize( ulong value )
        {
            return new FileSize( value );
        }

        public static explicit operator FileSize( long value )
        {
            return new FileSize( (ulong) value );
        }

        override public string ToString()
        {
            return this.ToString( null, null );
        }

        public string ToString( string format )
        {
            return this.ToString( format, null );
        }

        public string ToString( string format, IFormatProvider formatProvider )
        {
            int precision;

            if( String.IsNullOrEmpty( format ) )
                return this.ToString( DEFAULT_PRECISION );
            else if( int.TryParse( format, out precision ) )
                return this.ToString( precision );
            else
                return this.value.ToString( format, formatProvider );
        }

        /// <summary>
        /// Formats the FileSize using the given number of decimals.
        /// </summary>
        public string ToString( int precision )
        {
            double pow = Math.Floor( ( this.value > 0 ? Math.Log( this.value ) : 0 ) / Math.Log( 1024 ) );
            pow = Math.Min( pow, Units.Length - 1 );
            double dValue = (double) value / Math.Pow( 1024, pow );
            return dValue.ToString( pow == 0 ? "F0" : "F" + precision.ToString() ) + " " + Units[(int) pow];
        }
    }
}
