﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Utilities
{
    /// <summary>
    /// Flag helper functions.
    /// </summary>
    public static class Flag
    {
        /// <summary>
        /// A simple helper function for checking if a flag is set.
        /// </summary>
        /// <example>
        /// Instead of:
        /// <code>
        ///  bool selected = ((item.Flags & ItemFlags.ImportantItem) == ItemFlags.ImportantItem);
        /// </code>
        /// you can do:
        /// <code>
        ///  bool selected = Flag.IsSet(item.Flags, ItemFlags.ImportantItem);
        /// </code>
        /// </example>
        /// <param name="container">The "container" for the flag.</param>
        /// <param name="flag">The flag to check for.</param>
        /// <returns>True if the flag is set in container, otherwise false.</returns>
        public static bool IsSet( int container, int flag )
        {
            return ((container & flag) == flag);
        }

        /// <summary>
        /// A simple helper function for checking if a flag is set.
        /// </summary>
        /// <example>
        /// Instead of:
        /// <code>
        ///  bool selected = ((item.Flags & ItemFlags.ImportantItem) == ItemFlags.ImportantItem);
        /// </code>
        /// you can do:
        /// <code>
        ///  bool selected = Flag.IsSet(item.Flags, ItemFlags.ImportantItem);
        /// </code>
        /// </example>
        /// <param name="container">The "container" for the flag.</param>
        /// <param name="flag">The flag to check for.</param>
        /// <returns>True if the flag is set in container, otherwise false.</returns>
        public static bool IsSet<T>( T container, T flag )
        {
            if( !typeof( T ).IsEnum )
                throw new ArgumentException( String.Format( Properties.Resources.FlagTypeNotEnum, typeof( T ).FullName ) );
            if( !Attribute.IsDefined( typeof( T ), typeof( FlagsAttribute ) ) )
                throw new ArgumentException( String.Format( Properties.Resources.FlagTypeNotFlags, typeof( T ).FullName ) );
            long lcontainer = Convert.ToInt64( container );
            long lflag = Convert.ToInt64( flag );
            return ((lcontainer & lflag) == lflag);
        }
        
        public static int[] GetBits( int flag )
        {
            List<int> bits = new List<int>();
            for( int i = 0; i < 32; i++ )
                if( IsSet( flag, 1 << i ) )
                    bits.Add( i );
            return bits.ToArray();
        }
    }
}
