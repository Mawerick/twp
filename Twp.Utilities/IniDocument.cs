﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.Utilities
{
    /// <summary>
    /// Represents an Ini-style settings file.
    /// </summary>
    public class IniDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IniDocument"/> class.
        /// </summary>
        public IniDocument() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="IniDocument"/> class with a specified file name and location.
        /// </summary>
        /// <param name="fileName">The name of the settings file.</param>
        /// <param name="path">The location of the settings file.</param>
        public IniDocument( string fileName, string path )
        {
            this.fileName = fileName;
            this.path = path;
            this.SetFullFileName();
        }

        /// <summary>
        /// Gets or sets the name of the settings file.
        /// </summary>
        public string FileName
        {
            get { return this.fileName; }
            set
            {
                this.fileName = value;
                this.SetFullFileName();
            }
        }
        private string fileName;

        /// <summary>
        /// Gets or sets the name of the path where the settings file is located.
        /// </summary>
        public string Path
        {
            get { return this.path; }
            set
            {
                this.path = value;
                this.SetFullFileName();
            }
        }
        private string path;

        private string fullFileName;

        private void SetFullFileName()
        {
            if( !String.IsNullOrEmpty( this.path ) && !String.IsNullOrEmpty( this.fileName ) )
                this.fullFileName = System.IO.Path.Combine( this.path, this.fileName );
            else
                this.fullFileName = this.fileName;
        }

        /// <summary>
        /// Gets the <see cref="IniSectionCollection"/> that holds the settings sections of this ini document.
        /// </summary>
        public IniSectionCollection Sections
        {
            get { return this.sections; }
        }
        private IniSectionCollection sections = new IniSectionCollection();

        /// <summary>
        /// Gets or sets the <see cref="IniSection"/> associated with the key <paramref name="name"/>.
        /// This is equivalent of using Sections[name] and is provided for convenience.
        /// </summary>
        /// <param name="name">The name of the sections.</param>
        /// <returns>An IniSection representing the named section.</returns>
        public IniSection this[string name]
        {
            get
            {
                if( !this.sections.ContainsKey( name ) )
                    this.sections.Add( name, new IniSection() );
                return this.sections[name];
            }
            set
            {
                if( this.sections[name] != value )
                    this.sections[name] = value;
            }
        }

        /// <summary>
        /// Sets the value of a section setting to a default value if no value is set.
        /// </summary>
        /// <param name="section">The name of the section.</param>
        /// <param name="key">The name of the setting.</param>
        /// <param name="value">The default value to set.</param>
        public void SetDefault( string section, string key, string value )
        {
            this[section].SetDefault( key, value );
        }
        
        /// <summary>
        /// Updates an old key to a new key in the same section.
        /// This is an overloaded method, provided for convenience.
        /// </summary>
        /// <param name="section">The name of the section.</param>
        /// <param name="oldKey">The name of the old key.</param>
        /// <param name="newKey">The name of the new key.</param>
        public void Update( string section, string oldKey, string newKey )
        {
            if( this.sections.ContainsKey( section ) )
            {
                IniSection sect = this.sections[section];
                if( sect.ContainsKey( oldKey ) )
                {
                    sect[newKey] = sect[oldKey];
                    sect.Remove( oldKey );
                }
            }
        }

        /// <summary>
        /// Updates an old section/key to a new section/key.
        /// </summary>
        /// <param name="oldSection">The name of the old section.</param>
        /// <param name="oldKey">The name of the old key.</param>
        /// <param name="newSection">The name of the new section.</param>
        /// <param name="newKey">The name of the new key.</param>
        public void Update( string oldSection, string oldKey, string newSection, string newKey )
        {
            if( this.sections.ContainsKey( oldSection ) )
            {
                if( this.sections[oldSection].ContainsKey( oldKey ) )
                {
                    this.sections[newSection][newKey] = this.sections[oldSection][oldKey];
                    this.sections[oldSection].Remove( oldKey );
                }
                if( this.sections[oldSection].Count == 0 )
                    this.sections.Remove( oldSection );
            }
        }

        /// <summary>
        /// Reads an ini-style settings file.
        /// </summary>
        /// <param name="fileName">The name of the settings file.</param>
        /// <exception cref="ArgumentException">fileName is an empty string ("").</exception>
        /// <exception cref="ArgumentNullException">fileName is null.</exception>
        /// <exception cref="FileNotFoundException">The file cannot be found.</exception>
        /// <exception cref="DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
        /// <exception cref="IOException">An I/O error occurs.</exception>
        /// <exception cref="OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
        /// <exception cref="IniParseException">A Parsing error occured.</exception>
        public void Read( string fileName )
        {
            this.FileName = fileName;
            this.Read();
        }

        /// <summary>
        /// Reads an ini-style settings file.
        /// </summary>
        /// <param name="fileName">The name of the settings file.</param>
        /// <param name="path">The location of the settings file.</param>
        /// <exception cref="ArgumentException">fileName is an empty string ("").</exception>
        /// <exception cref="ArgumentNullException">fileName is null.</exception>
        /// <exception cref="FileNotFoundException">The file cannot be found.</exception>
        /// <exception cref="DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
        /// <exception cref="IOException">An I/O error occurs.</exception>
        /// <exception cref="OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
        /// <exception cref="IniParseException">A Parsing error occured.</exception>
        public void Read( string fileName, string path )
        {
            this.FileName = fileName;
            this.Path = path;
            this.Read();
        }

        /// <summary>
        /// Reads an ini-style settings file.
        /// </summary>
        /// <exception cref="System.ArgumentException">FileName is an empty string ("").</exception>
        /// <exception cref="System.ArgumentNullException">FileName is null.</exception>
        /// <exception cref="System.IO.FileNotFoundException">The file cannot be found.</exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
        /// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
        /// <exception cref="System.OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
        /// <exception cref="Twp.Utilities.IniParseException">A Parsing error occured.</exception>
        public void Read()
        {
            if( String.IsNullOrEmpty( this.fullFileName ) )
                this.SetFullFileName();

            if( !File.Exists( this.fullFileName ) )
                return;

            using( StreamReader sr = new StreamReader( this.fullFileName ) )
            {
                this.sections.Clear();
                string line;
                string sectionName = null;
                IniSection section = null;
                int lineNumber = 0;
                while( ( line = sr.ReadLine() ) != null )
                {
                    ++lineNumber;

                    if( line.StartsWith( "#", StringComparison.Ordinal ) || line.Trim() == String.Empty )
                        continue;

                    if( line.StartsWith( "[", StringComparison.Ordinal ) )
                    {
                        if( sectionName != null && section != null )
                            this.sections.Add( sectionName, section );

                        int end = line.LastIndexOf( ']' );
                        if( end == -1 )
                            throw new IniParseException( String.Format( Properties.Resources.IniParseSectionError, lineNumber ), line );

                        sectionName = line.Substring( 1, end - 1 ).Trim();
                        if( sectionName == string.Empty )
                            sectionName = null;
                        else
                            section = new IniSection();
                    }
                    else if( line.Contains( "=" ) )
                    {
                        if( sectionName == null || section == null )
                            throw new IniParseException( String.Format( Properties.Resources.IniParseSectionMissing, lineNumber ), line );

                        string[] parts = line.Split( new char[] { '=' } );
                        section.Add( parts[0].Trim(), parts[1].Trim() );
                    }
                    else
                        throw new IniParseException( String.Format( Properties.Resources.IniParseFormatError, lineNumber ), line );
                }
                if( sectionName != null && section != null )
                    this.sections.Add( sectionName, section );
            }
        }

        /// <summary>
        /// Writes an ini-style settings file.
        /// </summary>
        /// <exception cref="UnauthorizedAccessException">Access is denied.</exception>
        /// <exception cref="ArgumentException">FileName is en empty string ("").</exception>
        /// <exception cref="ArgumentNullException">FileName is null.</exception>
        /// <exception cref="FileNotFoundException">The file cannot be found.</exception>
        /// <exception cref="DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
        /// <exception cref="IOException">An I/O error occurs.</exception>
        /// <exception cref="OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
        public void Write()
        {
            if( !String.IsNullOrEmpty( this.path ) && !Directory.Exists( this.path ) )
                Directory.CreateDirectory( this.path );

            if( String.IsNullOrEmpty( this.fullFileName ) )
                this.SetFullFileName();

            using( StreamWriter sw = new StreamWriter( this.fullFileName ) )
            {
                bool firstLine = true;
                foreach( KeyValuePair<string, IniSection> group in sections )
                {
                    if( firstLine )
                        firstLine = false;
                    else
                        sw.WriteLine();
                    sw.WriteLine( "[{0}]", group.Key );
                    foreach( KeyValuePair<string, string> item in group.Value )
                    {
                        sw.WriteLine( "{0} = {1}", item.Key, item.Value );
                    }
                }
            }
        }
    }
}
