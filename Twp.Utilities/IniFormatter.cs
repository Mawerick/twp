﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace Twp.Utilities
{
    public class IniFormatter : IFormatter
    {
        #region Constructor

        public IniFormatter()
        {
            this.context = new StreamingContext( StreamingContextStates.All );
            this.commentRegex = new Regex( @";.[^\r^\n]*" );
            this.sectionRegex = new Regex( @"(\s*?)\[{1}\s*[_\.\-\w\d\s]+\s*\](\s*?)" );
            this.keyValueRegex = new Regex( @"(\s*[_\.\d\w]*\s*)=([\s\d\w\W]*)" );
        }

        #endregion

        #region Properties

        public SerializationBinder Binder
        {
            get { return this.binder; }
            set { this.binder = value; }
        }
        private SerializationBinder binder;

        public StreamingContext Context
        {
            get { return this.context; }
            set { this.context = value; }
        }
        private StreamingContext context;

        public ISurrogateSelector SurrogateSelector
        {
            get { return this.surrogateSelector; }
            set { this.surrogateSelector = value; }
        }
        private ISurrogateSelector surrogateSelector;

        #endregion

        #region Methods

        private Regex commentRegex;
        private Regex sectionRegex;
        private Regex keyValueRegex;

        private IniSectionCollection sections;
        private IniSection currentSection;
        private string currentSectionName;

        public object Deserialize( Stream serializationStream )
        {
            if( serializationStream == null )
                throw new ArgumentNullException( "serializationStream" );
            if( !serializationStream.CanRead )
                throw new ArgumentException( "Stream is not readable." );

            this.sections = new IniSectionCollection();

            Log.Debug( "[IniFormatter.Deserialize] Parsing stream." );
            using( StreamReader reader = new StreamReader( serializationStream ) )
            {
                while( !reader.EndOfStream )
                {
                    ParseLine( reader.ReadLine() );
                }
                reader.Close();
            }

            Log.Debug( "[IniFormatter.Deserialize] Attempting to create objects from parsed data." );
            List<object> objects = this.CreateObjects();
            if( objects.Count == 1 )
                return objects[0];
            else
                return objects;
        }

        private void ParseLine( string line )
        {
            Log.Debug( "[IniFormatter.ParseLine] Line: [{0}]", line );
            if( String.IsNullOrEmpty( line ) )
            {
                Log.Debug( "[IniFormatter.ParseLine] (Empty line)" );
                return;
            }

            if( this.commentRegex.Match( line ).Success )
            {
                Log.Debug( "[IniFormatter.ParseLine] (Comment)" );
                return;
            }

            if( this.sectionRegex.Match( line ).Success )
            {
                this.ParseSection( line );
            }
            else if( this.keyValueRegex.Match( line ).Success )
            {
                this.ParseKeyValue( line );
            }
            else
            {
                throw new SerializationException( "Failed to parse line: " + line );
            }
        }

        private void ParseSection( string line )
        {
            string name = this.sectionRegex.Match( line ).Value.Trim();

            if( String.IsNullOrEmpty( name ) )
                throw new SerializationException( "Failed to parse section. Line: " + line );

            // remove delimeters
            name = name.Substring( 1, name.Length - 2 ).Trim();
            Log.Debug( "[IniFormatter.ParseSection] Name: {0}", name );

            if( this.sections.ContainsKey( name ) )
                throw new SerializationException( "Failed to parse section. Another section already with the same name exists: " + name );

            this.currentSectionName = name;
            this.currentSection = new IniSection();
            this.sections.Add( this.currentSectionName, this.currentSection );
        }

        private void ParseKeyValue( string line )
        {
            if( String.IsNullOrEmpty( this.currentSectionName ) )
                throw new SerializationException( "Parser error. Key/value not inside a section! Line: " + line );

            string tmp = this.keyValueRegex.Match( line ).Value.Trim();
            if( String.IsNullOrEmpty( tmp ) )
                throw new SerializationException( "Failed to parse key/value. Line: " + line );

            int index = tmp.IndexOf( '=' );
            string key = tmp.Substring( 0, index ).Trim();
            string value = tmp.Substring( index + 1, tmp.Length - index - 1 ).Trim();
            if( value.StartsWith( "\"" ) && value.EndsWith( "\"" ) )
                value = value.Substring( 1, value.Length - 2 );
            Log.Debug( "[IniFormatter.ParseKeyValue] Key: {0} Value: {1}", key, value );

            if( this.currentSection.ContainsKey( key ) )
                throw new SerializationException( "Failed to parse section. Another value with the same name already exists in section: "
                    + this.currentSectionName + ". Line: " + line );

            this.currentSection.Add( key, value );
        }

        private List<object> CreateObjects()
        {
            List<object> list = new List<object>();
            foreach( KeyValuePair<string, IniSection> section in this.sections )
            {
                Log.Debug( "[IniFormatter.CreateObjects] Processing section: {0}", section.Key );
                object obj = this.CreateObject( section.Key, section.Value, String.Empty );
                if( obj != null )
                    list.Add( obj );
            }
            return list;
        }

        private object CreateObject( string key, IniSection section, string prefix )
        {
            if( section.Count == 0 )
                return null;

            if( key.Contains( prefix ) )
                key = key.Substring( prefix.Length );

            Assembly assembly = Assembly.GetEntryAssembly();
            string name = assembly.GetName().Name + "." + key;
            Type t = assembly.GetType( name, true );

            // Create object of just found type
            Object obj = FormatterServices.GetUninitializedObject( t );

            // Get type members
            PropertyInfo[] properties = obj.GetType().GetProperties();
            MemberInfo[] members = FormatterServices.GetSerializableMembers( obj.GetType(), this.Context );

            // Create data array for each member
            object[] data = new object[properties.Length];

            // Store for each member its value, converted from string to its type.
            for( int i = 0; i < properties.Length; ++i )
            {
                //FieldInfo fi = ((FieldInfo) members[i]);
                PropertyInfo pi = properties[i];
                string sub = prefix + key + "." + pi.PropertyType.Name;
                if( section.ContainsKey( pi.Name ) )
                    data[i] = Convert.ChangeType( section[pi.Name], pi.PropertyType );
                else if( this.sections.ContainsKey( sub ) )
                {
                    data[i] = this.CreateObject( sub, this.sections[sub], prefix + key + "." );
                    this.sections[sub].Clear();
                }
                //else
                    //throw new SerializationException( "Missing field value: " + fi.Name );
            }

            // Populate object members with their values and return object.
            return FormatterServices.PopulateObjectMembers( obj, members, data );
        }

        public void Serialize( Stream serializationStream, object graph )
        {
            if( serializationStream == null )
                throw new ArgumentNullException( "serializationStream" );
            if( graph == null )
                throw new ArgumentNullException( "graph" );
            if( !serializationStream.CanWrite )
                throw new ArgumentException( "Stream is not writable." );

            IniSectionCollection sections = new IniSectionCollection();
            this.Serialize( ref sections, graph, String.Empty );

            StreamWriter writer = new StreamWriter( serializationStream );
            bool first = true;
            foreach( KeyValuePair<string, IniSection> section in sections )
            {
                if( first )
                    first = false;
                else
                    writer.WriteLine();

                writer.WriteLine( "[{0}]", section.Key );
                foreach( KeyValuePair<string, string> line in section.Value )
                {
                    writer.WriteLine( "{0} = {1}", line.Key, line.Value );
                }
            }
            writer.Close();
        }

        private void Serialize( ref IniSectionCollection sections, object graph, string prefix )
        {
            // Get fields that are to be serialized.
            PropertyInfo[] properties = graph.GetType().GetProperties();
            MemberInfo[] members = FormatterServices.GetSerializableMembers( graph.GetType(), this.Context );

            // Get fields data.
            object[] objMembersData = FormatterServices.GetObjectData( graph, members );

            // Write class name and all fields & values to file.
            IniSection section = new IniSection();
            sections.Add( prefix + graph.GetType().Name, section );
            for( int i = 0; i < members.Length; ++i )
            {
                if( HasINIIgnore( properties[i] ) )
                    continue;

                if( objMembersData[i] is ValueType )
                {
                    section.Add( properties[i].Name, objMembersData[i].ToString() );
                }
                else if( objMembersData[i] is String )
                {
                    section.Add( properties[i].Name, "\"" + objMembersData[i].ToString() + "\"" );
                }
                else if( objMembersData[i] is System.Collections.IList )
                {
                    throw new SerializationException( "Arrays are not yet supported." );
                }
                else if( objMembersData[i] is System.Collections.IDictionary )
                {
                    throw new SerializationException( "Dictionaries are not yet supported." );
                }
                else
                {
                    this.Serialize( ref sections, objMembersData[i], prefix + graph.GetType().Name + "." );
                }
            }
        }

        private bool HasINIIgnore( PropertyInfo info )
        {
            object[] attr = info.GetCustomAttributes( true );
            bool found = false;
            if( attr.Length != 0 )
            {
#if DEBUG
                string attrDebug = String.Empty;
#endif
                foreach( object a in attr )
                {
#if DEBUG
                    attrDebug += a.ToString() + " ";
#endif
                    INIIgnoreAttribute iia = a as INIIgnoreAttribute;
                    if( iia != null )
                    {
                        found = true;
                        break;
                    }
                }
#if DEBUG
                Log.Debug( "[IniFormatter.IgnoreINI] {0} {1} attributes: {2} (found={3})", info.MemberType.ToString(), info.Name, attrDebug, found );
#endif
            }
            return found;
        }

        #endregion
    }

    [AttributeUsage( AttributeTargets.Property )]
    public class INIIgnoreAttribute : Attribute
    {
    }
}
