﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Collections.Generic;

namespace Twp.Utilities
{
	/// <summary>
	/// Represents a single Ini-styled section.
	/// </summary>
	public class IniSection : Dictionary<string, string>
	{
		/// <summary>
		/// Gets or sets the value associated with the specified key.
		/// </summary>
		/// <param name="key">The name of the setting.</param>
		/// <value>
		/// A <see cref="System.String"/> containing the value of the named setting.
		/// If no setting of that name exists, a get operation will create a new item for it.
		/// </value>
		public new string this[string key]
		{
			get
			{
				if( !base.ContainsKey( key ) )
					base.Add( key, null );
				return base[key];
			}
			set
			{
				if( this[key] != value )
					base[key] = value;
			}
		}

        /// <summary>
        /// Sets the value of a setting to a default value if no value is set.
        /// </summary>
        /// <param name="key">The name of the setting.</param>
        /// <param name="value">The default value to set.</param>
        public void SetDefault( string key, string value )
        {
            if( this[key] == null )
                this[key] = value;
        }
    }
}
