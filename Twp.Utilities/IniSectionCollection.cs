﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Collections.Generic;

namespace Twp.Utilities
{
	/// <summary>
	/// Represents a table of Ini-style groups.
	/// </summary>
	public class IniSectionCollection : Dictionary<string, IniSection>
	{
		/// <summary>
		/// Gets or sets the group associated with the specified name.
		/// </summary>
		/// <param name="name">The name of the group.</param>
		/// <value>
		/// An <see cref="IniSection"/> containing the settings for the named group.
		/// If no group of that name exists, a get operation will create a new empty <see cref="IniSection"/>.
		/// </value>
		public new IniSection this[string name]
		{
			get
			{
				if( !base.ContainsKey( name ) )
					base.Add( name, new IniSection() );
				return base[name];
			}
			set
			{
				if( base[name] != value )
					base[name] = value;
			}
		}
	}
}
