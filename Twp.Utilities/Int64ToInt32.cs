﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Runtime.InteropServices;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of Int64ToInt32.
    /// </summary>
    [StructLayout( LayoutKind.Explicit )]
    public struct Int64ToInt32
    {
        [FieldOffset(0)]
        public long Int64Value;
        
        [FieldOffset(0)]
        public int LeftInt32;
        
        [FieldOffset(4)]
        public int RightInt32;
    }

    /// <summary>
    /// Description of UInt64ToUInt32.
    /// </summary>
    [StructLayout( LayoutKind.Explicit )]
    public struct UInt64ToUInt32
    {
        [FieldOffset(0)]
        public ulong UInt64Value;
        
        [FieldOffset(0)]
        public uint LeftUInt32;
        
        [FieldOffset(4)]
        public uint RightUInt32;
    }
}
