﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Twp.Utilities
{
    /// <summary>
    /// Provides methods for writing information and event messages
    /// to a log file, as well as displaying them to the user.
    /// </summary>
    /// <example>
    /// <code>
    /// private static void Main(string[] args)
    /// {
    /// 	Log.Start("log.txt");
    /// 	Log.Warning("This is a warning message!");
    /// }
    /// </code>
    /// </example>
    public static class Log
    {
        private static string LogFile;

        /// <summary>
        /// Gets a value indicating whether the Log system has been started.
        /// </summary>
        public static bool Started
        {
            get { return started; }
        }
        private static bool started;

        /// <summary>
        /// Gets or sets a value indicating how debug info will be displayed.
        /// </summary>
        public static OutputType DebugOutput
        {
            get { return debugOutput; }
            set { debugOutput = value; }
        }
        private static OutputType debugOutput = OutputType.Trace | OutputType.Console;

        /// <summary>
        /// Gets or sets a value indicating how debug info will be displayed.
        /// </summary>
        public static OutputType InformationOutput
        {
            get { return informationOutput; }
            set { informationOutput = value; }
        }
        private static OutputType informationOutput = OutputType.Dialog;

        /// <summary>
        /// Gets or sets a value indicating how debug info will be displayed.
        /// </summary>
        public static OutputType WarningOutput
        {
            get { return warningOutput; }
            set { warningOutput = value; }
        }
        private static OutputType warningOutput = OutputType.Dialog;

        /// <summary>
        /// Gets or sets a value indicating how debug info will be displayed.
        /// </summary>
        public static OutputType ErrorOutput
        {
            get { return errorOutput; }
            set { errorOutput = value; }
        }
        private static OutputType errorOutput = OutputType.Dialog;

        /// <summary>
        /// Specifies how additional output of log messages should be handled.
        /// </summary>
        [Flags]
        public enum OutputType
        {
            /// <summary>If set, nothing will be output.</summary>
            None = 0,
            /// <summary>If set, System.Diagnostics.Trace.WriteLine will be called.</summary>
            Trace = 1,
            /// <summary>If set, System.Console.WriteLine will be called.</summary>
            Console = 2,
            /// <summary>If set, System.Windows.Forms.MessageBox.Show will be called.</summary>
            Dialog = 4,
        }

        /// <summary>
        /// Specifies the type of the message.
        /// </summary>
        public enum LogLevel
        {
            /// <summary>The message is a debugging message.</summary>
            Debug,
            /// <summary>The message is an informational message.</summary>
            Information,
            /// <summary>The message is a warning message.</summary>
            Warning,
            /// <summary>The message is an error message.</summary>
            Error,
            /// <summary>The message is a fatal error message.</summary>
            Fatal,
        }

        /// <summary>
        /// 
        /// </summary>
        public static event LoggedEventHandler Logged;

        // Triggers the Logged event.
        private static bool DoLogged( LogLevel level, string message )
        {
            if( Logged == null )
                return false;

            LoggedEventArgs e = new LoggedEventArgs( level, message );
            Logged( e );
            return e.Cancel;
        }

        /// <summary>
        /// Sets the file name to which the Log sysstem should write to to [AppDataPath]\[Company]\[Product]\[Product].log, and writes a start messsage to it.
        /// </summary>
        public static void Start()
        {
            Start(true);
        }

        /// <summary>
        /// Sets the file name to which the Log sysstem should write to to [AppDataPath]\[Company]\[Product]\[Product].log, and writes a start messsage to it.
        /// </summary>
        /// <param name="truncate">true if the file should be truncated, otherwise, false.</param>
        public static void Start(bool truncate)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            string product = Path.GetAssemblyAttributeValue(asm, typeof(AssemblyProductAttribute), "Product");
            Start(product + ".log", Path.GetAppDataPath());
        }

        /// <summary>
        /// Sets the file name to which the Log system should write to, and writes a start messsage to it.
        /// </summary>
        /// <param name="fileName">The name of the file used for writing.</param>
        /// <param name="path">The location of the file.</param>
        public static void Start( string fileName, string path )
        {
            Start( fileName, path, true );
        }

        /// <summary>
        /// Sets the file name to which the Log system should write to, and writes a start messsage to it.
        /// </summary>
        /// <param name="fileName">The name of the file used for writing.</param>
        /// <param name="path">The location of the file.</param>
        /// <param name="truncate">true if the file should be truncated, otherwise, false.</param>
        public static void Start( string fileName, string path, bool truncate )
        {
            if( !String.IsNullOrEmpty( path ) )
            {
                if( !Directory.Exists( path ) )
                    Directory.CreateDirectory( path );
                LogFile = Path.Combine( path, fileName );
            }
            else
            {
                LogFile = fileName;
            }
            if( truncate )
            {
                if( File.Exists( LogFile ) )
                    File.Delete( LogFile );
            }
            started = true;
            //Add( LogLevel.Information, "Logger started." );
        }

        // The base Add method.
        // If Log.Start has not been called, a LogFile has not been specified, which means we have no file to add to.
        private static void Add( LogLevel level, string message )
        {
            // Check if the log system has started.
            if( !started )
                return;

            // Lock LogFile so Multi-threading doesn't fail here.
            lock( LogFile )
            {
                // Open a stream for the logfile, appending to the end of the file.
                using( StreamWriter log = File.AppendText( LogFile ) )
                {
                    // Write a date/time stamp.
                    log.Write( "[{0} {1}] ", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString() );
                    // Write a LogLevel stamp (unless it's an informational message).
                    if( level != LogLevel.Information )
                        log.Write( "[{0}] ", level );
                    // Write the message.
                    log.WriteLine( message );
                    // Flush and close the stream.
                    log.Flush();
                    log.Close();
                }
            }
        }

        // The base Show method.
        // No Log.Start check here, as these kinds of messages tends to be important.
        private static void Show( LogLevel level, string message )
        {
            // Get the proper icon for the MessageBox.
            MessageBoxIcon icon = MessageBoxIcon.None;
            switch( level )
            {
                case LogLevel.Debug:
                    icon = MessageBoxIcon.None;
                    break;
                case LogLevel.Information:
                    icon = MessageBoxIcon.Information;
                    break;
                case LogLevel.Warning:
                    icon = MessageBoxIcon.Warning;
                    break;
                case LogLevel.Error:
                    icon = MessageBoxIcon.Exclamation;
                    break;
                case LogLevel.Fatal:
                    icon = MessageBoxIcon.Error;
                    break;
            }

            // Show the MessageBox.
            MessageBox.Show( message, level.ToString(), MessageBoxButtons.OK, icon );
        }
        
        /// <summary>
        /// Writes the text representation of the object to the Log file,
        /// as well as to the standard output stream.
        /// </summary>
        /// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
        [Conditional("DEBUG")]
        public static void Debug( object value )
        {
            Debug( value.ToString() );
        }

        /// <summary>
        /// Writes the text representation of the specified array of objects to the Log file,
        /// as well as the standard output stream, using the specified format information.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        [Conditional("DEBUG")]
        public static void Debug( string format, params object[] args )
        {
            Debug( String.Format( format, args ) );
        }

        /// <summary>
        /// Writes the text representation of the object to the Log file,
        /// as well as to the standard output stream if a condition is true.
        /// </summary>
        /// <param name="condition">true to cause the message to be written; otherwise, false.</param>
        /// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
        [Conditional( "DEBUG" )]
        public static void DebugIf( bool condition, object value )
        {
            if( condition )
                Debug( value.ToString() );
        }

        /// <summary>
        /// Writes the text representation of the specified array of objects to the Log file,
        /// as well as the standard output stream, using the specified format information,
        /// if a condition is true.
        /// </summary>
        /// <param name="condition">true to cause the message to be written; otherwise, false.</param>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        [Conditional( "DEBUG" )]
        public static void DebugIf( bool condition, string format, params object[] args )
        {
            if( condition )
                Debug( String.Format( format, args ) );
        }

        /// <summary>
        /// Writes a text message to the Log file, as well as the standard output stream,
        /// using the specified format information.
        /// </summary>
        /// <param name="message">The message to write.</param>
        [Conditional( "DEBUG" )]
        public static void Debug( string message )
        {
            Add( LogLevel.Debug, message );
            if( Flag.IsSet( debugOutput, OutputType.Trace ) )
                System.Diagnostics.Trace.WriteLine( message );
            if( Flag.IsSet( debugOutput, OutputType.Console ) )
                Console.WriteLine( message );
            if( DoLogged( LogLevel.Debug, message ) )
                return;
            if( Flag.IsSet( debugOutput, OutputType.Dialog ) )
                Show( LogLevel.Debug, message );
        }

        /// <summary>
        /// Writes the text representation of the object to the Log file.
        /// </summary>
        /// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
        public static void Information( object value )
        {
            Information( value.ToString() );
        }

        /// <summary>
        /// Writes the text representation of the specified array of objects
        /// to the Log file, as well as the standard output stream, using the specified format information.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        public static void Information( string format, params object[] args )
        {
            Information( String.Format( format, args ) );
        }

        /// <summary>
        /// Writes a message to the Log file.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public static void Information( string message )
        {
            Add( LogLevel.Information, message );
            if( Flag.IsSet( informationOutput, OutputType.Trace ) )
                System.Diagnostics.Trace.WriteLine( message );
            if( Flag.IsSet( informationOutput, OutputType.Console ) )
                Console.WriteLine( message );
            if( DoLogged( LogLevel.Information, message ) )
                return;
            if( Flag.IsSet( informationOutput, OutputType.Dialog ) )
                Show( LogLevel.Information, message );
        }

        /// <summary>
        /// Writes the text representation of the object to the Log file.
        /// </summary>
        /// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
        public static void Warning( object value )
        {
            Warning( value.ToString() );
        }

        /// <summary>
        /// Writes the text representation of the specified array of objects
        /// to the Log file, as well as the standard output stream, using the specified format information.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        public static void Warning( string format, params object[] args )
        {
            Warning( String.Format( format, args ) );
        }

        /// <summary>
        /// Writes a message to the Log file.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public static void Warning( string message )
        {
            Add( LogLevel.Warning, message );
            if( Flag.IsSet( warningOutput, OutputType.Trace ) )
                System.Diagnostics.Trace.WriteLine( message );
            if( Flag.IsSet( warningOutput, OutputType.Console ) )
                Console.WriteLine( message );
            if( DoLogged( LogLevel.Warning, message ) )
                return;
            if( Flag.IsSet( warningOutput, OutputType.Dialog ) )
                Show( LogLevel.Warning, message );
        }

        /// <summary>
        /// Writes the text representation of the object to the Log file.
        /// </summary>
        /// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
        public static void Error( object value )
        {
            Error( value.ToString() );
        }

        /// <summary>
        /// Writes the text representation of the specified array of objects
        /// to the Log file, as well as the standard output stream, using the specified format information.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        public static void Error( string format, params object[] args )
        {
            Error( String.Format( format, args ) );
        }

        /// <summary>
        /// Writes a message to the Log file.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public static void Error( string message )
        {
            Add( LogLevel.Error, message );
            if( Flag.IsSet( errorOutput, OutputType.Trace ) )
                System.Diagnostics.Trace.WriteLine( message );
            if( Flag.IsSet( errorOutput, OutputType.Console ) )
                Console.WriteLine( message );
            if( DoLogged( LogLevel.Error, message ) )
                return;
            if( Flag.IsSet( errorOutput, OutputType.Dialog ) )
                Show( LogLevel.Error, message );
        }

        /// <summary>
        /// Writes the text representation of the object to the Log file.
        /// The Application is then terminated.
        /// </summary>
        /// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
        public static void Fatal( object value )
        {
            Fatal( value.ToString() );
        }

        /// <summary>
        /// Writes the text representation of the specified array of objects
        /// to the Log file, as well as the standard output stream,
        /// using the specified format information.
        /// The Application is then terminated.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An array of objects to write using format.</param>
        public static void Fatal( string format, params object[] args )
        {
            Fatal( String.Format( format, args ) );
        }

        /// <summary>
        /// Writes a message to the Log file. The Application is then terminated.
        /// </summary>
        /// <param name="message">The message to write.</param>
        public static void Fatal( string message )
        {
            Add( LogLevel.Fatal, message );
            if( Flag.IsSet( errorOutput, OutputType.Trace ) )
                System.Diagnostics.Trace.WriteLine( message );
            if( Flag.IsSet( errorOutput, OutputType.Console ) )
                Console.WriteLine( message );
            if( !DoLogged( LogLevel.Fatal, message ) )
                Show( LogLevel.Fatal, message );
            Process.GetCurrentProcess().Kill();
        }
    }
}
