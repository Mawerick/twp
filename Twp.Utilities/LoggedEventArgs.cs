﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Utilities
{
    public delegate void LoggedEventHandler( LoggedEventArgs e );
    public class LoggedEventArgs : EventArgs
    {
        public LoggedEventArgs( Log.LogLevel level, string message )
        {
            this.level = level;
            this.message = message;
            this.cancel = false;
        }

        public LoggedEventArgs( Log.LogLevel level, string message, bool cancel )
        {
            this.level = level;
            this.message = message;
            this.cancel = cancel;
        }

        private readonly string message;
        private readonly Log.LogLevel level;
        private bool cancel;

        public Log.LogLevel Level
        {
            get { return this.level; }
        }

        public string Message
        {
            get { return this.message; }
        }

        public bool Cancel
        {
            get { return this.cancel; }
            set { this.cancel = true; }
        }
    }
}
