﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Utilities
{
    public static class KeyStates
    {
        public const int LMB   = 1 << 0;
        public const int RMB   = 1 << 1;
        public const int Shift = 1 << 2;
        public const int Ctrl  = 1 << 3;
        public const int MMB   = 1 << 4;
        public const int Alt   = 1 << 5;
    }

    public delegate void Action();
}
