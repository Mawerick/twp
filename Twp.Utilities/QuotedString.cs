﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of QuotedString.
    /// </summary>
    public static class QuotedString
    {
        public static string[] Split( string input, params char[] delimiters )
        {
            List<string> strings = new List<string>();
            StringBuilder sb = new StringBuilder();
            bool inQuotes = false;
            char quoteChar = '\0';

            foreach( char c in input )
            {
                if( c == '"' || c == '\'' )
                {
                    inQuotes = !inQuotes;
                    quoteChar = c;
                }
                else if( !inQuotes && Array.IndexOf( delimiters, c ) >= 0 )
                {
                    strings.Add( sb.ToString() );
                    sb = new StringBuilder();
                }
                else
                    sb.Append( c );
            }

            if( sb.Length > 0 )
                strings.Add( sb.ToString() );

            return strings.ToArray();
        }

        public static string Join( string separator, params string[] input )
        {
            if( input == null || input.Length == 0 )
                throw new ArgumentException( "input" );

            string result = String.Empty;
            for( int i = 0; i < input.Length; i++ )
            {
                if( i > 0 )
                    result += separator;

                if( input[i].Contains( " " ) )
                    result += '"' + input[i] + '"';
                else
                    result += input[i];
            }
            return result;
        }
    }
}
