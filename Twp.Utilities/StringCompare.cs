﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Text;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of StringComparison.
    /// </summary>
    public static class StringCompare
    {
        public static string StartingSubstring( string source, string target )
        {
            if( String.IsNullOrEmpty( source ) || String.IsNullOrEmpty( target ) )
                return null;

            string[] srcWords = source.Split( ' ' );
            string[] tgtWords = target.Split( ' ' );
            string substring = String.Empty;

            for( int i = 0; i < srcWords.Length; i++ )
            {
                if( i >= tgtWords.Length || srcWords[i] != tgtWords[i] )
                    break;

                substring += srcWords[i] + " ";
            }

            return substring.Trim();
        }
    }
}
