﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Twp.Utilities
{
    public static class Text
    {
        static Text()
        {
            _specialChars["nbsp"] = ' ';
            _specialChars["rdquo"] = '"';
            _specialChars["lsquo"] = '\'';
            _specialChars["apos"] = '\'';
            _specialChars["lt"] = '<';
            _specialChars["gt"] = '>';
            _specialChars["quot"] = '"';
            _specialChars["amp"] = '&';

            // ISO 8859-1 Symbols
            _specialChars["iexcl"] = Convert.ToChar(161);
            _specialChars["cent"] = Convert.ToChar(162);
            _specialChars["pound"] = Convert.ToChar(163);
            _specialChars["curren"] = Convert.ToChar(164);
            _specialChars["yen"] = Convert.ToChar(165);
            _specialChars["brvbar"] = Convert.ToChar(166);
            _specialChars["sect"] = Convert.ToChar(167);
            _specialChars["uml"] = Convert.ToChar(168);
            _specialChars["copy"] = Convert.ToChar(169);
            _specialChars["ordf"] = Convert.ToChar(170);
            _specialChars["laquo"] = Convert.ToChar(171);
            _specialChars["not"] = Convert.ToChar(172);
            _specialChars["shy"] = Convert.ToChar(173);
            _specialChars["reg"] = Convert.ToChar(174);
            _specialChars["macr"] = Convert.ToChar(175);
            _specialChars["deg"] = Convert.ToChar(176);
            _specialChars["plusmn"] = Convert.ToChar(177);
            _specialChars["sup2"] = Convert.ToChar(178);
            _specialChars["sup3"] = Convert.ToChar(179);
            _specialChars["acute"] = Convert.ToChar(180);
            _specialChars["micro"] = Convert.ToChar(181);
            _specialChars["para"] = Convert.ToChar(182);
            _specialChars["middot"] = Convert.ToChar(183);
            _specialChars["cedil"] = Convert.ToChar(184);
            _specialChars["sup1"] = Convert.ToChar(185);
            _specialChars["ordm"] = Convert.ToChar(186);
            _specialChars["raquo"] = Convert.ToChar(187);
            _specialChars["frac14"] = Convert.ToChar(188);
            _specialChars["frac12"] = Convert.ToChar(189);
            _specialChars["frac34"] = Convert.ToChar(190);
            _specialChars["iquest"] = Convert.ToChar(191);
            _specialChars["times"] = Convert.ToChar(215);
            _specialChars["divide"] = Convert.ToChar(247);

            // ISO 8859-1 Characters
            _specialChars["Agrave"] = Convert.ToChar(192);
            _specialChars["Aacute"] = Convert.ToChar(193);
            _specialChars["Acirc"] = Convert.ToChar(194);
            _specialChars["Atilde"] = Convert.ToChar(195);
            _specialChars["Auml"] = Convert.ToChar(196);
            _specialChars["Aring"] = Convert.ToChar(197);
            _specialChars["AElig"] = Convert.ToChar(198);
            _specialChars["Ccedil"] = Convert.ToChar(199);
            _specialChars["Egrave"] = Convert.ToChar(200);
            _specialChars["Eacute"] = Convert.ToChar(201);
            _specialChars["Ecirc"] = Convert.ToChar(202);
            _specialChars["Euml"] = Convert.ToChar(203);
            _specialChars["Igrave"] = Convert.ToChar(204);
            _specialChars["Iacute"] = Convert.ToChar(205);
            _specialChars["Icirc"] = Convert.ToChar(206);
            _specialChars["Iuml"] = Convert.ToChar(207);
            _specialChars["ETH"] = Convert.ToChar(208);
            _specialChars["Ntilde"] = Convert.ToChar(209);
            _specialChars["Ograve"] = Convert.ToChar(210);
            _specialChars["Oacute"] = Convert.ToChar(211);
            _specialChars["Ocirc"] = Convert.ToChar(212);
            _specialChars["Otilde"] = Convert.ToChar(213);
            _specialChars["Ouml"] = Convert.ToChar(214);
            _specialChars["Oslash"] = Convert.ToChar(216);
            _specialChars["Ugrave"] = Convert.ToChar(217);
            _specialChars["Uacute"] = Convert.ToChar(218);
            _specialChars["Ucirc"] = Convert.ToChar(219);
            _specialChars["Uuml"] = Convert.ToChar(220);
            _specialChars["Yacute"] = Convert.ToChar(221);
            _specialChars["THORN"] = Convert.ToChar(222);
            _specialChars["szlig"] = Convert.ToChar(223);
            _specialChars["agrave"] = Convert.ToChar(224);
            _specialChars["aacute"] = Convert.ToChar(225);
            _specialChars["acirc"] = Convert.ToChar(226);
            _specialChars["atilde"] = Convert.ToChar(227);
            _specialChars["auml"] = Convert.ToChar(228);
            _specialChars["aring"] = Convert.ToChar(229);
            _specialChars["aelig"] = Convert.ToChar(230);
            _specialChars["ccedil"] = Convert.ToChar(231);
            _specialChars["egrave"] = Convert.ToChar(232);
            _specialChars["eacute"] = Convert.ToChar(233);
            _specialChars["ecirc"] = Convert.ToChar(234);
            _specialChars["euml"] = Convert.ToChar(235);
            _specialChars["igrave"] = Convert.ToChar(236);
            _specialChars["iacute"] = Convert.ToChar(237);
            _specialChars["icirc"] = Convert.ToChar(238);
            _specialChars["iuml"] = Convert.ToChar(239);
            _specialChars["eth"] = Convert.ToChar(240);
            _specialChars["ntilde"] = Convert.ToChar(241);
            _specialChars["ograve"] = Convert.ToChar(242);
            _specialChars["oacute"] = Convert.ToChar(243);
            _specialChars["ocirc"] = Convert.ToChar(244);
            _specialChars["otilde"] = Convert.ToChar(245);
            _specialChars["ouml"] = Convert.ToChar(246);
            _specialChars["oslash"] = Convert.ToChar(248);
            _specialChars["ugrave"] = Convert.ToChar(249);
            _specialChars["uacute"] = Convert.ToChar(250);
            _specialChars["ucirc"] = Convert.ToChar(251);
            _specialChars["uuml"] = Convert.ToChar(252);
            _specialChars["yacute"] = Convert.ToChar(253);
            _specialChars["thorn"] = Convert.ToChar(254);
            _specialChars["yuml"] = Convert.ToChar(255);

            // Math Symbols Supported by HTML
            _specialChars["forall"] = Convert.ToChar(8704);
            _specialChars["part"] = Convert.ToChar(8706);
            _specialChars["exist"] = Convert.ToChar(8707);
            _specialChars["empty"] = Convert.ToChar(8709);
            _specialChars["nabla"] = Convert.ToChar(8711);
            _specialChars["isin"] = Convert.ToChar(8712);
            _specialChars["notin"] = Convert.ToChar(8713);
            _specialChars["ni"] = Convert.ToChar(8715);
            _specialChars["prod"] = Convert.ToChar(8719);
            _specialChars["sum"] = Convert.ToChar(8721);
            _specialChars["minus"] = Convert.ToChar(8722);
            _specialChars["lowast"] = Convert.ToChar(8727);
            _specialChars["radic"] = Convert.ToChar(8730);
            _specialChars["prop"] = Convert.ToChar(8733);
            _specialChars["infin"] = Convert.ToChar(8734);
            _specialChars["ang"] = Convert.ToChar(8736);
            _specialChars["and"] = Convert.ToChar(8743);
            _specialChars["or"] = Convert.ToChar(8744);
            _specialChars["cap"] = Convert.ToChar(8745);
            _specialChars["cup"] = Convert.ToChar(8746);
            _specialChars["int"] = Convert.ToChar(8747);
            _specialChars["there4"] = Convert.ToChar(8756);
            _specialChars["sim"] = Convert.ToChar(8764);
            _specialChars["cong"] = Convert.ToChar(8773);
            _specialChars["asymp"] = Convert.ToChar(8776);
            _specialChars["ne"] = Convert.ToChar(8800);
            _specialChars["equiv"] = Convert.ToChar(8801);
            _specialChars["le"] = Convert.ToChar(8804);
            _specialChars["ge"] = Convert.ToChar(8805);
            _specialChars["sub"] = Convert.ToChar(8834);
            _specialChars["sup"] = Convert.ToChar(8835);
            _specialChars["nsub"] = Convert.ToChar(8836);
            _specialChars["sube"] = Convert.ToChar(8838);
            _specialChars["supe"] = Convert.ToChar(8839);
            _specialChars["oplus"] = Convert.ToChar(8853);
            _specialChars["otimes"] = Convert.ToChar(8855);
            _specialChars["perp"] = Convert.ToChar(8869);
            _specialChars["sdot"] = Convert.ToChar(8901);

            // Greek Letters Supported by HTML
            _specialChars["Alpha"] = Convert.ToChar(913);
            _specialChars["Beta"] = Convert.ToChar(914);
            _specialChars["Gamma"] = Convert.ToChar(915);
            _specialChars["Delta"] = Convert.ToChar(916);
            _specialChars["Epsilon"] = Convert.ToChar(917);
            _specialChars["Zeta"] = Convert.ToChar(918);
            _specialChars["Eta"] = Convert.ToChar(919);
            _specialChars["Theta"] = Convert.ToChar(920);
            _specialChars["Iota"] = Convert.ToChar(921);
            _specialChars["Kappa"] = Convert.ToChar(922);
            _specialChars["Lambda"] = Convert.ToChar(923);
            _specialChars["Mu"] = Convert.ToChar(924);
            _specialChars["Nu"] = Convert.ToChar(925);
            _specialChars["Xi"] = Convert.ToChar(926);
            _specialChars["Omicron"] = Convert.ToChar(927);
            _specialChars["Pi"] = Convert.ToChar(928);
            _specialChars["Rho"] = Convert.ToChar(929);
            _specialChars["Sigma"] = Convert.ToChar(931);
            _specialChars["Tau"] = Convert.ToChar(932);
            _specialChars["Upsilon"] = Convert.ToChar(933);
            _specialChars["Phi"] = Convert.ToChar(934);
            _specialChars["Chi"] = Convert.ToChar(935);
            _specialChars["Psi"] = Convert.ToChar(936);
            _specialChars["Omega"] = Convert.ToChar(937);
            _specialChars["alpha"] = Convert.ToChar(945);
            _specialChars["beta"] = Convert.ToChar(946);
            _specialChars["gamma"] = Convert.ToChar(947);
            _specialChars["delta"] = Convert.ToChar(948);
            _specialChars["epsilon"] = Convert.ToChar(949);
            _specialChars["zeta"] = Convert.ToChar(950);
            _specialChars["eta"] = Convert.ToChar(951);
            _specialChars["theta"] = Convert.ToChar(952);
            _specialChars["iota"] = Convert.ToChar(953);
            _specialChars["kappa"] = Convert.ToChar(954);
            _specialChars["lambda"] = Convert.ToChar(955);
            _specialChars["mu"] = Convert.ToChar(956);
            _specialChars["nu"] = Convert.ToChar(957);
            _specialChars["xi"] = Convert.ToChar(958);
            _specialChars["omicron"] = Convert.ToChar(959);
            _specialChars["pi"] = Convert.ToChar(960);
            _specialChars["rho"] = Convert.ToChar(961);
            _specialChars["sigmaf"] = Convert.ToChar(962);
            _specialChars["sigma"] = Convert.ToChar(963);
            _specialChars["tau"] = Convert.ToChar(964);
            _specialChars["upsilon"] = Convert.ToChar(965);
            _specialChars["phi"] = Convert.ToChar(966);
            _specialChars["chi"] = Convert.ToChar(967);
            _specialChars["psi"] = Convert.ToChar(968);
            _specialChars["omega"] = Convert.ToChar(969);
            _specialChars["thetasym"] = Convert.ToChar(977);
            _specialChars["upsih"] = Convert.ToChar(978);
            _specialChars["piv"] = Convert.ToChar(982);

            // Other Entities Supported by HTML
            _specialChars["OElig"] = Convert.ToChar(338);
            _specialChars["oelig"] = Convert.ToChar(339);
            _specialChars["Scaron"] = Convert.ToChar(352);
            _specialChars["scaron"] = Convert.ToChar(353);
            _specialChars["Yuml"] = Convert.ToChar(376);
            _specialChars["fnof"] = Convert.ToChar(402);
            _specialChars["circ"] = Convert.ToChar(710);
            _specialChars["tilde"] = Convert.ToChar(732);
            _specialChars["ndash"] = Convert.ToChar(8211);
            _specialChars["mdash"] = Convert.ToChar(8212);
            _specialChars["lsquo"] = Convert.ToChar(8216);
            _specialChars["rsquo"] = Convert.ToChar(8217);
            _specialChars["sbquo"] = Convert.ToChar(8218);
            _specialChars["ldquo"] = Convert.ToChar(8220);
            _specialChars["rdquo"] = Convert.ToChar(8221);
            _specialChars["bdquo"] = Convert.ToChar(8222);
            _specialChars["dagger"] = Convert.ToChar(8224);
            _specialChars["Dagger"] = Convert.ToChar(8225);
            _specialChars["bull"] = Convert.ToChar(8226);
            _specialChars["hellip"] = Convert.ToChar(8230);
            _specialChars["permil"] = Convert.ToChar(8240);
            _specialChars["prime"] = Convert.ToChar(8242);
            _specialChars["Prime"] = Convert.ToChar(8243);
            _specialChars["lsaquo"] = Convert.ToChar(8249);
            _specialChars["rsaquo"] = Convert.ToChar(8250);
            _specialChars["oline"] = Convert.ToChar(8254);
            _specialChars["euro"] = Convert.ToChar(8364);
            _specialChars["trade"] = Convert.ToChar(153);
            _specialChars["larr"] = Convert.ToChar(8592);
            _specialChars["uarr"] = Convert.ToChar(8593);
            _specialChars["rarr"] = Convert.ToChar(8594);
            _specialChars["darr"] = Convert.ToChar(8595);
            _specialChars["harr"] = Convert.ToChar(8596);
            _specialChars["crarr"] = Convert.ToChar(8629);
            _specialChars["lceil"] = Convert.ToChar(8968);
            _specialChars["rceil"] = Convert.ToChar(8969);
            _specialChars["lfloor"] = Convert.ToChar(8970);
            _specialChars["rfloor"] = Convert.ToChar(8971);
            _specialChars["loz"] = Convert.ToChar(9674);
            _specialChars["spades"] = Convert.ToChar(9824);
            _specialChars["clubs"] = Convert.ToChar(9827);
            _specialChars["hearts"] = Convert.ToChar(9829);
            _specialChars["diams"] = Convert.ToChar(9830);
        }

        private static readonly Dictionary<string, char> _specialChars = new Dictionary<string, char>(StringComparer.InvariantCultureIgnoreCase);

        public static string StripHtmlTags(string source)
        {
            if (source == null)
                return null;

            char[] array = new char[source.Length];
            char[] special = null;
            int j = 0;
            int s = 0;
            bool insideTag = false;
            bool insideSpecial = false;

            for (int i = 0; i < source.Length; i++)
            {
                char c = source[i];
                if (c == '<')
                {
                    insideTag = true;
                    continue;
                }
                if (c == '>')
                {
                    insideTag = false;
                    continue;
                }
                if (c == '&')
                {
                    int len = source.IndexOf(';', i);
                    if (len > 0)
                    {
                        special = new char[len];
                        s = 0;
                        insideSpecial = true;
                        continue;
                    }
                }
                if (c == ';' && insideSpecial)
                {
                    if (special[0] == '#')
                    {
                        int code = Convert.ToInt32(new string(special, 1, s - 1));
                        c = Convert.ToChar(code);
                    }
                    else
                    {
                        string str = new String(special, 0, s);
                        Log.Debug("[Text.StripHtmlTags] Special: {0}", str);
                        if (_specialChars.ContainsKey(str))
                            c = _specialChars[str];
                        else
                            c = '#';
                    }
                    special = null;
                    insideSpecial = false;
                }
                if (!insideTag && !insideSpecial)
                {
                    array[j++] = c;
                }
                if (insideSpecial)
                {
                    special[s++] = c;
                }
            }

            return new string(array, 0, j);
        }
    }
}
