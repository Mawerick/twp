﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of AbstractAction.
    /// </summary>
    public abstract class AbstractAction : IAction
    {
        protected int DoCount;

        public virtual void Do()
        {
            if( !this.CanDo )
                return;

            this.DoCore();
            this.DoCount++;
        }

        public virtual void Undo()
        {
            if( !this.CanUndo )
                return;

            this.UndoCore();
            this.DoCount--;
        }

        /// <summary>
        /// Override DoCore to provide your logic that actually performs the action.
        /// </summary>
        protected abstract void DoCore();

        /// <summary>
        /// Override UndoCore to provide your logic that undoes the action.
        /// </summary>
        protected abstract void UndoCore();

        public virtual bool CanDo
        {
            get { return this.DoCount == 0; }
        }

        public virtual bool CanUndo
        {
            get { return !this.CanDo; }
        }

        /// <summary>
        /// Attempts to take a new incoming action and instead of recording that one
        /// as a new action, just modify the current one so that it's summary effect is
        /// a combination of both.
        /// </summary>
        /// <param name="nextAction">the action to merge</param>
        /// <returns>
        /// true if the action agreed to merge, false if we want the nextAction to be
        /// tracked separately.
        /// </returns>
        public virtual bool TryToMerge( IAction nextAction )
        {
            return false;
        }

        /// <summary>
        /// Defines if the action can be merged with the previous one in the undo history.
        /// This is useful for long chains of consecutive operations of the same type,
        /// i.e. dragging something or typing some text.
        /// </summary>
        public bool AllowToMergeWithPrevious
        {
            get { return this.allowToMergeWithPrevious; }
            set { this.allowToMergeWithPrevious = value; }
        }
        private bool allowToMergeWithPrevious;
    }
}
