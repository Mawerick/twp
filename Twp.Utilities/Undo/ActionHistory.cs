﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Collections.Generic;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of ActionHistory.
    /// </summary>
    internal class ActionHistory : IActionHistory
    {
        public ActionHistory()
        {
            this.Init();
        }

        #region Events

        public event EventHandler CollectionChanged;
        protected void RaiseCollectionChanged()
        {
            if( this.CollectionChanged != null )
                this.CollectionChanged( this, EventArgs.Empty );
        }

        #endregion

        #region Private fields

        private ActionHistoryNode currentNode;
        private ActionHistoryNode firstNode;
        private IAction lastAction;
        private int length;

        #endregion

        #region Properties

        public ActionHistoryNode CurrentNode
        {
            get { return this.currentNode; }
            set
            {
                if( value != null )
                    this.currentNode = value;
                else
                    throw new ArgumentNullException( "CurrentNode" );
            }
        }
        
        public ActionHistoryNode First
        {
            get { return this.firstNode; }
        }

        public IAction LastAction
        {
            get { return this.lastAction; }
            set { this.lastAction = value; }
        }

        public bool CanMoveBack
        {
            get
            {
                return this.currentNode.PreviousAction != null &&
                    this.currentNode.PreviousNode != null;
            }
        }

        public bool CanMoveForward
        {
            get
            {
                return this.currentNode.NextAction != null &&
                    this.currentNode.NextNode != null;
            }
        }

        public int Length
        {
            get { return this.length; }
        }

        #endregion

        #region Methods

        public bool AppendAction( IAction action )
        {
            if( this.currentNode.PreviousAction != null && this.currentNode.PreviousAction.TryToMerge( action ) )
            {
                this.RaiseCollectionChanged();
                return false;
            }
            this.currentNode.NextAction = action;
            this.currentNode.NextNode = new ActionHistoryNode( action, this.currentNode );
            return true;
        }

        public void Clear()
        {
            this.Init();
            this.RaiseCollectionChanged();
        }
        
        private void Init()
        {
            this.currentNode = new ActionHistoryNode();
            this.firstNode = this.currentNode;
            this.length = 0;
        }

        public void MoveBack()
        {
            if( !this.CanMoveBack )
            {
                throw new InvalidOperationException( "ActionHistory.MoveBack() cannot execute because"
                                                    + " CanMoveBack returned false (the current node"
                                                    + " is the first node in the undo history)." );
            }
            this.currentNode.PreviousAction.Undo();
            this.currentNode = this.currentNode.PreviousNode;
            this.length -= 1;
            this.RaiseCollectionChanged();
        }

        public void MoveForward()
        {
            if( !this.CanMoveForward )
            {
                throw new InvalidOperationException( "ActionHistory.MoveForward() cannot execute because"
                                                    + " CanMoveForward returned false (the current node"
                                                    + " is the last node in the undo history)." );
            }
            this.currentNode.NextAction.Do();
            this.currentNode = this.currentNode.NextNode;
            this.length += 1;
            this.RaiseCollectionChanged();
        }

        public IEnumerable<IAction> EnumUndoableActions()
        {
            ActionHistoryNode node = this.firstNode;
            while( node != null && node != this.currentNode && node.NextAction != null )
            {
                yield return node.NextAction;
                node = node.NextNode;
            }
        }

        public IEnumerator<IAction> GetEnumerator()
        {
            return this.EnumUndoableActions().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}
