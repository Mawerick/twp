﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of ActionHistoryNode.
    /// </summary>
    internal class ActionHistoryNode
    {
        public ActionHistoryNode()
        {
        }

        public ActionHistoryNode( IAction lastAction, ActionHistoryNode lastNode )
        {
            this.previousAction = lastAction;
            this.previousNode = lastNode;
        }

        private IAction previousAction;
        private IAction nextAction;
        private ActionHistoryNode previousNode;
        private ActionHistoryNode nextNode;

        public IAction PreviousAction
        {
            get { return this.previousAction; }
            set { this.previousAction = value; }
        }

        public IAction NextAction
        {
            get { return this.nextAction; }
            set { this.nextAction = value; }
        }

        public ActionHistoryNode PreviousNode
        {
            get { return this.previousNode; }
            set { this.previousNode = value; }
        }

        public ActionHistoryNode NextNode
        {
            get { return this.nextNode; }
            set { this.nextNode = value; }
        }
    }
}
