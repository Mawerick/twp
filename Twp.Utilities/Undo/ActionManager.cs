﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// ActionManager is the central component of the Undo framework.
    /// </summary>
    [DefaultEvent( "CollectionChanged" )]
    [DefaultProperty( "ExecuteImmediately" )]
    public class ActionManager : Component
    {
        public ActionManager()
        {
            this.History = new ActionHistory();
            this.savedNode = this.history.CurrentNode;
        }

        #region Events

        /// <summary>
        /// Occurs whenever an action is added, executed, undone or redone.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Occurs when an action is added, executed, undone or redone." )]
        public event EventHandler CollectionChanged;

        protected void RaiseCollectionChanged( object sender, EventArgs e )
        {
            if( this.CollectionChanged != null )
                this.CollectionChanged( this, e );
        }

        #endregion

        #region Private fields

        private IActionHistory history = null;
        private IAction currentAction = null;
        private bool executeImmediately = false;
        private ActionHistoryNode savedNode = null;

        private Stack<Transaction> transactions = new Stack<Transaction>();

        #endregion

        #region Properties

        internal IActionHistory History
        {
            get { return this.history; }
            set
            {
                if( this.history != null )
                {
                    this.history.CollectionChanged -= this.RaiseCollectionChanged;
                }
                this.history = value;
                if( this.history != null )
                {
                    this.history.CollectionChanged += this.RaiseCollectionChanged;
                }
            }
        }

        /// <summary>
        /// The currently running action (during an undo or redo operation).
        /// </summary>
        /// <remarks>null if no operation is running.</remarks>
        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public IAction CurrentAction
        {
            get { return this.currentAction; }
        }

        /// <summary>
        /// Checks if we're inside a redo/undo action.
        /// </summary>
        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool ActionIsExecuting
        {
            get { return this.currentAction != null; }
        }

        /// <summary>
        /// Defines whether we should record an action to the Undo buffer and then excecute,
        /// or just execute it without it becoming a part of the history.
        /// </summary>
        [Category( "Behavior" )]
        [Description( "Defines whether we should record an action, or just executed it immediately" )]
        [DefaultValue( false )]
        public bool ExecuteImmediately
        {
            get { return this.executeImmediately; }
            set { this.executeImmediately = value; }
        }

        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Stack<Transaction> Transactions
        {
            get { return this.transactions; }
        }

        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Transaction RecordingTransaction
        {
            get
            {
                if( this.transactions.Count > 0 )
                    return this.transactions.Peek();
                return null;
            }
        }

        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool CanUndo
        {
            get { return this.history.CanMoveBack; }
        }

        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool CanRedo
        {
            get { return this.history.CanMoveForward; }
        }

        [BrowsableAttribute( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public IEnumerable<IAction> EnumUndoableActions
        {
            get { return this.history.EnumUndoableActions(); }
        }

        public bool IsSaved
        {
            get { return this.savedNode == this.history.CurrentNode; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Main method to add and execute a new action.
        /// </summary>
        /// <param name="action">The action to be recorded in the buffer and executed.</param>
        public void RecordAction( IAction action )
        {
            if( action == null )
                throw new ArgumentNullException( "action" );

            // make sure we're not already executing an action...
            if( this.ActionIsExecuting )
                throw new InvalidOperationException( "ActionManager is busy" );

            // if we don't want to record actions, just execute it.
            if( this.ExecuteImmediately && action.CanDo )
            {
                action.Do();
                return;
            }

            // check if we're inside a transaction that is being recorded
            Transaction currentTransaction = this.RecordingTransaction;
            if( currentTransaction != null )
            {
                // if we're inside a transaction, just add the action to the transaction's list
                currentTransaction.Add( action );
                if( !currentTransaction.IsDelayed )
                    action.Do();
            }
            else
            {
                this.RunActionDirectly( action );
            }
        }

        /// <summary>
        /// Adds the action to the buffer and executes it.
        /// </summary>
        /// <param name="action">The action to store and execute.</param>
        private void RunActionDirectly( IAction action )
        {
            // make sure we're not already executing an action...
            if( this.ActionIsExecuting )
                throw new InvalidOperationException( "ActionManager is busy" );

            this.currentAction = action;
            try
            {
                if( History.AppendAction( action ) )
                    History.MoveForward();
            }
            finally
            {
                this.currentAction = null;
            }
        }

        public Transaction CreateTransaction()
        {
            return Transaction.Create( this );
        }

        public Transaction CreateTransaction( bool delayed )
        {
            return Transaction.Create( this, delayed );
        }

        public void OpenTransaction( Transaction transaction )
        {
            this.transactions.Push( transaction );
        }

        public void CommitTransaction()
        {
            if( this.transactions.Count == 0 )
                throw new ArgumentNullException( "Transaction stack is empty!" );

            Transaction transaction = this.transactions.Pop();
            if( transaction.HasActions )
            {
                this.RecordAction( transaction );
            }
        }

        public void RollbackTransaction()
        {
            if( this.transactions.Count != 0 )
            {
                Transaction transaction = this.transactions.Peek();
                if( transaction != null )
                    transaction.Undo();

                this.transactions.Clear();
            }
        }

        public void Undo()
        {
            if( !this.CanUndo )
                return;

            if( this.ActionIsExecuting )
                throw new InvalidOperationException( "ActionManager is busy." );

            this.currentAction = this.history.CurrentNode.PreviousAction;
            this.history.MoveBack();
            this.currentAction = null;
        }

        public void Redo()
        {
            if( !this.CanRedo )
                return;

            if( this.ActionIsExecuting )
                throw new InvalidOperationException( "ActionManager is busy." );

            this.currentAction = this.history.CurrentNode.NextAction;
            this.history.MoveForward();
            this.currentAction = null;
        }

        public void Clear()
        {
            this.history.Clear();
            this.savedNode = this.history.CurrentNode;
            this.currentAction = null;
        }

        public void Save()
        {
            this.savedNode = this.history.CurrentNode;
        }

        #endregion
    }
}
