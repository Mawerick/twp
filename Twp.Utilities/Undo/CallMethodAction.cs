﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of CallMethodAction.
    /// </summary>
    public class CallMethodAction : AbstractAction
    {
        public CallMethodAction( Action doMethod, Action undoMethod )
        {
            this.doMethod = doMethod;
            this.undoMethod = undoMethod;
        }

        private Action doMethod;
        private Action undoMethod;

        protected override void DoCore()
        {
            if( this.doMethod != null )
                this.doMethod();
        }

        protected override void UndoCore()
        {
            if( this.undoMethod != null )
                this.undoMethod();
        }
    }
}
