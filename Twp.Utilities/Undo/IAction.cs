﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of IAction.
    /// </summary>
    public interface IAction
    {
        /// <summary>
        /// Apply changes encapsulated by this object.
        /// </summary>
        void Do();
        
        /// <summary>
        /// Undo changes by a previous call.
        /// </summary>
        void Undo();
        
        /// <summary>
        /// Defines if an action can be executed.
        /// </summary>
        bool CanDo { get; }
        
       
        /// <summary>
        /// Defines if an action can be undone.
        /// </summary>
        bool CanUndo { get; }
        
        /// <summary>
        /// Attempts to take a new incoming action and instead of recording that one
        /// as a new action, just modify the current one so that it's summary effect is
        /// a combination of both.
        /// </summary>
        /// <param name="nextAction">the action to merge</param>
        /// <returns>
        /// true if the action agreed to merge, false if we want the nextAction to be
        /// tracked separately.
        /// </returns>
        bool TryToMerge( IAction nextAction );
        
        /// <summary>
        /// Defines if the action can be merged with the previous one in the undo history.
        /// This is useful for long chains of consecutive operations of the same type,
        /// i.e. dragging something or typing some text.
        /// </summary>
        bool AllowToMergeWithPrevious { get; set; }
    }
}
