﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Reflection;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of SetPropertyAction.
    /// </summary>
    public class SetPropertyAction : AbstractAction
    {
        public SetPropertyAction( object parent, string propertyName, object value )
        {
            this.parent = parent;
            this.property = this.parent.GetType().GetProperty( propertyName );
            this.value = value;
        }

        private readonly object parent;
        private readonly PropertyInfo property;
        private object value;
        private object oldValue;

        protected override void DoCore()
        {
            this.oldValue = this.property.GetValue( this.parent, null );
            this.property.SetValue( this.parent, this.value, null );
        }

        protected override void UndoCore()
        {
            this.property.SetValue( this.parent, this.oldValue, null );
        }

        public override bool TryToMerge( IAction nextAction )
        {
            SetPropertyAction next = nextAction as SetPropertyAction;
            if( next != null && next.parent == this.parent && next.property == this.property && this.value.GetType() != typeof( bool ) )
            {
                this.value = next.value;
                this.property.SetValue( this.parent, this.value, null );
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return String.Format( "{0}.{1} set to {2}", parent, property.Name, value );
        }

    }
}
