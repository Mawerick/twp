﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Utilities.Undo
{
    /// <summary>
    /// Description of Transaction.
    /// </summary>
    public sealed class Transaction : IAction, IDisposable
    {
        #region Constructors

        public Transaction( ActionManager manager, bool delayed )
        {
            this.actions = new List<IAction>();
            this.manager = manager;
            this.manager.OpenTransaction( this );
            this.delayed = delayed;
        }

        public static Transaction Create( ActionManager manager, bool delayed )
        {
            if( manager == null )
                throw new ArgumentNullException( "manager" );
            return new Transaction( manager, delayed );
        }
        
        public static Transaction Create( ActionManager manager )
        {
            return Create( manager, true );
        }

        #endregion

        #region Private fields

        private readonly List<IAction> actions;
        private readonly ActionManager manager;
        private bool allowToMergeWithPrevious;
        private bool delayed;
        private bool aborted;

        #endregion

        #region Properties

        public bool AllowToMergeWithPrevious
        {
            get { return this.allowToMergeWithPrevious; }
            set { this.allowToMergeWithPrevious = value; }
        }

        public bool IsDelayed
        {
            get { return this.delayed; }
            set { this.delayed = value; }
        }

        public bool CanDo
        {
            get
            {
                foreach( IAction action in this.actions )
                {
                    if( !action.CanDo )
                        return false;
                }
                return true;
            }
        }

        public bool CanUndo
        {
            get
            {
                foreach( IAction action in Extensions.Reverse( this.actions ) )
                {
                    if( !action.CanUndo )
                        return false;
                }
                return true;
            }
        }

        public bool HasActions
        {
            get { return this.actions.Count != 0; }
        }

        #endregion

        #region Methods

        public void Commit()
        {
            this.manager.CommitTransaction();
        }

        public void Rollback()
        {
            this.manager.RollbackTransaction();
            this.aborted = true;
        }

        public void Add( IAction action )
        {
            if( action == null )
                throw new ArgumentNullException( "action" );
            this.actions.Add( action );
        }
        
        public void Remove( IAction action )
        {
            if( action == null )
                throw new ArgumentNullException( "action" );
            this.actions.Remove( action );
        }

        public void Do()
        {
            if( !this.delayed )
            {
                this.delayed = true;
                return;
            }
            foreach( IAction action in this.actions )
            {
                action.Do();
            }
        }

        public void Undo()
        {
            foreach( IAction action in Extensions.Reverse( this.actions ) )
            {
                action.Undo();
            }
        }

        public bool TryToMerge( IAction nextAction )
        {
            return false;
        }

        public void Dispose()
        {
            if( !this.aborted )
                this.Commit();
        }

        #endregion
    }
}
