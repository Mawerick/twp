﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Xml;

namespace Twp.Utilities
{
    /// <summary>
    /// Description of Updates.
    /// </summary>
    public class Updates
    {
        public static bool Check( string url, Version version )
        {
            XmlDocument projectXml = new XmlDocument();
            try
            {
                projectXml.Load( url );
                XmlNodeList packages = projectXml.GetElementsByTagName( "Package" );
                if( packages.Count > 0 )
                {
                    foreach( XmlNode package in packages )
                    {
                        Version packageVersion = new Version( package.Attributes["Name"].Value );
                        Log.Debug( "[Updates.Check] Remote version found: {0}", packageVersion );
                        if( packageVersion > version )
                            return true;
                    }
                }
            }
            catch( Exception ex )
            {
                Log.Debug( "[Updates.Check] Caught Exception: {0}", ex.Message );
                Log.Debug( ex.StackTrace );
            }
            
            return false;
        }
    }
}
