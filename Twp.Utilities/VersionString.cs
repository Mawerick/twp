﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Reflection;
namespace Twp.Utilities
{
    /// <summary>
    /// Version string format used by VersionString.Format
    /// </summary>
    public enum VersionFormat
    {
        /// <summary>Shows full version string, i.e. 1.0.0.0</summary>
        Full,
        /// <summary>Shows simple version string, i.e. 1.0.0</summary>
        Simple,
        /// <summary>Shows detailed version string, i.e. 1.0.0 (rev 1)</summary>
        Detailed
    }

    public static class VersionString
    {
        public static string Format( VersionFormat format )
        {
            Assembly assembly = Assembly.GetCallingAssembly();
            return Format( assembly, format );
        }

        public static string Format( Assembly assembly, VersionFormat format )
        {
            if( assembly == null )
                throw new ArgumentNullException( "assembly" );

            Version version = assembly.GetName().Version;

            switch( format )
            {
                case VersionFormat.Full:
                    return version.ToString();
                case VersionFormat.Simple:
                    return String.Format( "{0}.{1}.{2}", version.Major, version.Minor, version.Build );
                case VersionFormat.Detailed:
                    return String.Format( "{0}.{1}.{2} (r{3})", version.Major, version.Minor, version.Build, version.Revision );
                default:
                    throw new Exception( "Unknown format value" );
            }
        }
    }
}


